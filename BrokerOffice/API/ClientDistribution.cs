﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BrokerOffice.API
{
    public class ClientDistribution
    {
        public string ClientType { get; set; }
        public int total { get; set; }

        public string color { get; set; }
    }

    public class Orders
    {
        public string Type { get; set; }
        public int total { get; set; }

        public string color { get; set; }
    }
}