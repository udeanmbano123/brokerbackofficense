using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace BrokerOffice.DealNotes
{

    /*
     *  Added = 0,
        Deleted = 1,
        Modified = 2,
        SoftDeleted = 3,
        UnDeleted = 4
     */
     [TrackChanges]
    public class TransactionCharges
    {
        [Key]
        public int Id { get; set; }

        [StringLength(50)]
        public string Transcode { get; set; }

        [StringLength(50)]
        public string ChargeCode { get; set; }

        [Column(TypeName = "money")]
        public decimal? BuyCharges { get; set; }

        [Column(TypeName = "money")]
        public decimal? SellCharges { get; set; }

        [Column(TypeName = "date")]
        public DateTime? Date { get; set; }
    }
}
