﻿using BrokerOffice.DAO;
using BrokerOffice.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebMatrix.WebData;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Data;
using Json.NET;
using System.Collections.ObjectModel;
using System.Net.Sockets;
using System.Net;
using System.Runtime.Remoting.Contexts;

namespace BrokerOffice.PrimaryMarket
{
    public partial class DailyPrices : System.Web.UI.Page
    {
        public string id = "";
        private SBoardContext db = new SBoardContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                id = Request.QueryString["id"];
                if (this.IsPostBack)
                {
                    // TabName.Value = Request.Form[TabName.UniqueID];
                    trades();
                }
                else
                {
                    // loadCustomers();
                    trades();


                    if (id == " " || id == null)
                    {

                    }
                    else
                    {
                        // getDetails(id);
                    }
                }

            }
            catch (Exception)
            {
                Response.Redirect("~/User/index", false);        //write redirect
                Context.ApplicationInstance.CompleteRequest(); // end response

            }

        }


        public async void trades()
        {


            try
            {
                string n = "";
                string responJsonText = "";

                string updatedjson = "";
                int d = 0;
                int x = 0;
                string carryjson = "";
                JArray v2 = null;

                Tbl_MatchedDeals c1 = null;
                var jsonObjects = new List<string>();
                var jsonObjectsArray = "";
                string date = txtSearch.Date.ToString("yyyy-MM-dd");
                Uri requestUri = null;
                if (IsAddressAvailable("http://192.168.3.245/EscrowWebService/EscrowSoapWebService.asmx?op=Createnewaccount?op=Createnewaccount") == true)
                {
                    requestUri = new Uri("http://192.168.3.245/BrokerService/api/Deals/ATSPrice?date=" + date + "");//replace your Url 
                                                                                                                        //  dynamic dynamicJson = new ExpandoObject();
                }
               else
                {

               requestUri = new Uri("http://197.155.235.78/BrokerService/api/Deals/ATSPrice?date=" + date + ""); 
                }

                //string json = "";
                // json = Newtonsoft.Json.JsonConvert.SerializeObject(dynamicJson);
                var objClint = new System.Net.Http.HttpClient();
                HttpResponseMessage respon = await objClint.PostAsync(requestUri, new StringContent("", System.Text.Encoding.UTF8, "application/json"));

                responJsonText = await respon.Content.ReadAsStringAsync();


                GridView1.DataSource = DerializeDataTable(responJsonText);
                GridView1.DataBind();

            }
            catch (Exception)
            {

                msgbox("Mo connection");
            }




        }
       
        public DataTable DerializeDataTable(string data)
        {
            string json = data; //"data" should contain your JSON 
            var table = JsonConvert.DeserializeObject<DataTable>(json);
            return table;
        }
        public string checkNull(string s)
        {
            if (s == null)
            {
                return "0";
            }

            return s;
            
        }

      
        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
     


        }
        public void msgbox(string strMessage)
        {
            string strScript = "<script language=JavaScript>";
            strScript += "window.alert(\"" + strMessage + "\");";
            strScript += "</script>";
            System.Web.UI.WebControls.Label lbl = new System.Web.UI.WebControls.Label();
            lbl.Text = strScript;
            Page.Controls.Add(lbl);
        }
        protected void grdData_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            trades();
        }

        protected void btnSearch_Click1(object sender, EventArgs e)
        {

            trades();
           

        }
        public bool IsAddressAvailable(string address)
        {
            try
            {
                System.Net.WebClient client = new WebClient();
                client.DownloadData(address);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("Local IP Address Not Found!");
        }
    }
}
