﻿<%@ Page Language="C#" Async="true" MaintainScrollPositionOnPostback="true" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="DealSelector.aspx.cs" Inherits="BrokerOffice.Reporting.DealSelector" %>

<%@ Register Assembly="DevExpress.Web.v18.2, Version=18.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.XtraReports.v18.2.Web.WebForms, Version=18.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraReports.Web" TagPrefix="dx" %>


<asp:content id="Content1" contentplaceholderid="ContentPlaceHolder1" runat="server">
   
    <div>


  <div class="panel panel-default" style=" padding: 10px; margin: 10px">
    <div class="panel-heading" style="background-color:#428bca">
        <i style="color:white">Broker`s Note</i>
    </div>
        <div id="Tabs" role="tabpanel">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li class="active"><a href="#personal" aria-controls="personal" role="tab" data-toggle="tab">
                   Individual Broker Note </a></li>
                <li ><a href="#Action" aria-controls="Action" role="tab" data-toggle="tab">
                    Day Order Broker Note</a></li>
                    <li ><a href="#Action2" aria-controls="Action2" role="tab" data-toggle="tab">
                   Consolidated Broker Note</a></li>
          
            </ul>
            <!-- Tab panes -->
            <div class="tab-content" style="padding-top: 20px">
                  <div role="tabpanel" class="tab-pane active" id="personal">
                                          <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
            <div class="form-group">
       <asp:Label ID="SLbl" runat="server" Font-Size="Small" CssClass="control-label col-md-6" Text="  Search Name/(ATP/CSD Number/Deal)"></asp:Label>

            <div class="col-md-10">
                <table class="table table-responsive" style="margin-left:0%;">
                    <tr>
                        
                           
                       
                        <td style="width:170px">
<dx:ASPxTextBox ID="txtSearch" runat="server" CssClass="form-control" Width="250px"></dx:ASPxTextBox>
                </td>
                      <td style="width:170px">
<dx:ASPxButton ID="btnSearch" runat="server" CssClass="btn btn-primary" Text="Search" OnClick="btnSearch_Click" style="height: 23px"></dx:ASPxButton>
         
                        </td>
                       
                       
                    </tr>
                    <tr>
                        
                           
                       
                        <td style="width:170px">
                            <asp:Label ID="Label3" runat="server" Font-Size="Small" CssClass="control-label col-md-6" Text="  Search (Deal Number)"></asp:Label>
                            <br />
<dx:ASPxTextBox ID="ASPxTextBox1" runat="server" CssClass="form-control" Width="250px"></dx:ASPxTextBox>
                </td>
                      <td style="width:170px">
                          <br />
<dx:ASPxButton ID="ASPxButton6" runat="server" CssClass="btn btn-primary" Text="Search" OnClick="ASPxButton6_Click"></dx:ASPxButton>
         
                        </td>
                       
                       
                    </tr>
      
                </table>
              </div>
        </div>

       <br />
       <br />

          <dx:ASPxGridView ID="ASPxGridView1" KeyFieldName="Deal"  AutoGenerateColumns="false"  runat="server"  EnableCallBacks="False" Theme="Glass"   OnSelectionChanged="ASPxGridView1_SelectionChanged">
                        <SettingsPager AlwaysShowPager="True" PageSize="30">
                            <PageSizeItemSettings Visible="True">
                            </PageSizeItemSettings>
                        </SettingsPager>
                        <SettingsBehavior AllowSelectSingleRowOnly="True"   ProcessSelectionChangedOnServer="True" AllowSelectByRowClick="True" ConfirmDelete="True" />

                    
                        <SettingsCommandButton>
                            <SelectButton ButtonType="Button">
                            </SelectButton>
                        </SettingsCommandButton>
                       <Columns>

            <dx:GridViewCommandColumn ShowSelectCheckbox="True" Caption="Select"  VisibleIndex="0">
                </dx:GridViewCommandColumn>
                      <dx:GridViewDataTextColumn Width="200px" FieldName="DealNumber" ReadOnly="True" VisibleIndex="1">
            
                </dx:GridViewDataTextColumn>
                           <dx:GridViewDataTextColumn Width="200px" FieldName="Security" ReadOnly="True" VisibleIndex="2">
            
                </dx:GridViewDataTextColumn>
                     <dx:GridViewDataTextColumn Width="100px" FieldName="Deal" VisibleIndex="3">
                </dx:GridViewDataTextColumn>
                  <dx:GridViewDataTextColumn Width="100px" FieldName="Quantity" VisibleIndex="4">
                </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Width="100px" FieldName="Price" VisibleIndex="5">
                </dx:GridViewDataTextColumn>
                                       
                                   <dx:GridViewDataTextColumn Width="200px" FieldName="BuyerClient" VisibleIndex="6">
                </dx:GridViewDataTextColumn>
                                       
                              <dx:GridViewDataTextColumn Width="200px" FieldName="SellerClient" VisibleIndex="7">
                </dx:GridViewDataTextColumn>
                                         
                              <dx:GridViewDataTextColumn Width="200px" FieldName="DatePosted" VisibleIndex="8">
                </dx:GridViewDataTextColumn>
                                          
                              <dx:GridViewDataTextColumn Width="200px" FieldName="SettlementDate" VisibleIndex="9">
                </dx:GridViewDataTextColumn>
                                         
           
            </Columns>

                      

                       

                    </dx:ASPxGridView>
   
       <div class="form-group">
               <table class="table table-reponsive" style="margin-left:2%;">
                      <tr>
             <td style="width:170px">
                 <dx:ASPxLabel ID="ASPxLabel1" runat="server" Theme="Moderno" Width="170px" Text="Account"></dx:ASPxLabel>
                     <dx:ASPxTextBox ID="txtAcc" CssClass="form-control" runat="server" Width="170px"></dx:ASPxTextBox>
               
             </td>
                          <td style="width:170px">
                              <dx:ASPxLabel ID="ASPxLabel2" runat="server" Theme="Moderno" Width="170px" Text="OrderNo"></dx:ASPxLabel>
  <dx:ASPxTextBox ID="txtOrder" CssClass="form-control" runat="server" Width="170px"></dx:ASPxTextBox>
            
                          </td> 
                          </tr>
                   <tr>  
                          <td style="width:170px">
                                   <asp:RadioButtonList ID="RadioButtonList4" runat="server" RepeatDirection="Horizontal" AutoPostBack="True" OnSelectedIndexChanged="RadioButtonList4_SelectedIndexChanged" Font-Bold="True" Height="22px" Width="173px" Font-Size="Small">
                                          <asp:ListItem Text="BUYER" Value="BUYER"></asp:ListItem> 
                                          <asp:ListItem Text="SELLER" Value="SELLER"></asp:ListItem>
                                       
                                      </asp:RadioButtonList>
                         </td>
                       <td style="width:170px">
       <asp:Label  runat="server" Visible="false" Text="" Width="170px"></asp:Label>
                              <dx:ASPxButton ID="txtView" runat="server" CssClass="btn btn-primary" Text="View" OnClick="txtView_Click"></dx:ASPxButton>
                     
                          </td>
                          <asp:Label ID="Label2" runat="server" Visible="False" Text="Label"></asp:Label>
                          </tr>
                   <tr>
                       <td>
                <asp:Label ID="Label5" runat="server" Visible="False" Text="Ammend Executed Date"></asp:Label>
                         
                       </td>
                       <td>
                           <dx:ASPxDateEdit ID="ASPxDateEdit4" CssClass="form-control" runat="server" Visible="False"></dx:ASPxDateEdit>
                       </td>
                       <td>
                            <dx:ASPxButton ID="ASPxButton5" runat="server" CssClass="btn btn-primary" Text="UPDATE" OnClick="ASPxButton5_Click" Visible="False" ></dx:ASPxButton>
                     
                       </td>
                   </tr>
        </table>
  
       
   </div>
                             
                </div>
                <div role="tabpanel" class="tab-pane" id="Action">
                    <div class="form-group">
                            <table  style="margin-left:2%;">
                
                            <tr style="width:400px"">
                                <td style="width:200px">
                                         
                                    From<dx:ASPxDateEdit ID="ASPxDateEdit1" CssClass="form-control" Width="200px" runat="server"></dx:ASPxDateEdit>
                                </td>
                            
                                <td style="width:200px">
                                   
                                    To<dx:ASPxDateEdit ID="ASPxDateEdit3" CssClass="form-control" Width="200px" runat="server"></dx:ASPxDateEdit>
                                </td>
                               
                                <td style="width:170px">
                                    <br />
                                        <asp:RadioButtonList ID="RadioButtonList1" runat="server" RepeatDirection="Horizontal" AutoPostBack="false" Font-Bold="True" Height="22px" Width="173px" Font-Size="Small" OnSelectedIndexChanged="RadioButtonList1_SelectedIndexChanged">
                                          <asp:ListItem Text="BUYER" Value="BUY"></asp:ListItem> 
                                          <asp:ListItem Text="SELLER" Value="SELL"></asp:ListItem>
                                       
                                      </asp:RadioButtonList>
                                </td>
                                <td style="width:170px">
                                    <br />
                              <dx:ASPxButton ID="ASPxButton2" runat="server" CssClass="btn btn-primary" Text="View Report" OnClick="ASPxButton2_Click" ></dx:ASPxButton>
                     
                                </td>
                           
                            </tr>
                                <tr>
                                  
                                             <td>
                                                 <br />
                                             <asp:Label  runat="server" Visible="false" Text="" Width="170px"></asp:Label>
                              <dx:ASPxButton ID="ASPxButton4" runat="server" CssClass="btn btn-primary" Text="Print All Day Order Notes" OnClick="ASPxButton4_Click" ></dx:ASPxButton>
                     
                                </td>
                                   
                                </tr>

                                 <tr>
                                  
                                             <td>
                                                 <br />
                                             <asp:Label  runat="server" Visible="false" Text="" Width="170px"></asp:Label>
                              <dx:ASPxButton ID="btnGenerateZip" runat="server" CssClass="btn btn-primary" Text="Download All Day Order Notes" OnClick="btnGenerateZip_Click"  ></dx:ASPxButton>
                     
                                </td>
                                   
                                </tr>
                            </table>
                        </div>
                </div>
         <div role="tabpanel" class="tab-pane" id="Action2">
             <table  style="margin-left:0%;">

           <tr>
               <td style="width:170px">
             
           Select Investor <dx:ASPxComboBox ID="drpname" runat="server" ValueType="System.String"  AutoPostBack="false" Height="17px" TextField="FullNames" Theme="BlackGlass" ValueField="id" Width="200px"></dx:ASPxComboBox>
                                         
               </td>
               <td style="width:170px">
                      <br />
                   <asp:RadioButtonList ID="RadioButtonList2" runat="server" RepeatDirection="Horizontal" AutoPostBack="false" Font-Bold="True" Height="22px" Width="173px" Font-Size="Small">
                                          <asp:ListItem Text="BUYER" Value="BUY"></asp:ListItem> 
                                          <asp:ListItem Text="SELLER" Value="SELL"></asp:ListItem>
                                       
                                      </asp:RadioButtonList>
               </td>
              
               <td style="width:170px">
                
                   Select Date <dx:ASPxDateEdit ID="ASPxDateEdit2" CssClass="form-control" Width="170px" runat="server"></dx:ASPxDateEdit>
               </td>
               </tr>
                 <tr>
               <td>
                                                              <asp:HiddenField ID="TabName" runat="server" />
                                           <asp:Label ID="Label44" runat="server" Visible="false" Text=""></asp:Label>
         
                   <dx:ASPxButton ID="ASPxButton1" runat="server" CssClass="btn btn-primary" Text="Search" OnClick="ASPxButton1_Click" ></dx:ASPxButton>
       
               </td>
           </tr>
                 
                 </table>
               <dx:ASPxGridView ID="ASPxGridView2" KeyFieldName="Key"  AutoGenerateColumns="false"  runat="server" CssClass="nav-tabs" EnableCallBacks="False" Theme="Glass"   OnSelectionChanged="ASPxGridView2_SelectionChanged">
                        <SettingsPager AlwaysShowPager="True" PageSize="10">
                        </SettingsPager>
                        <SettingsBehavior AllowSelectSingleRowOnly="True"   ProcessSelectionChangedOnServer="True" AllowSelectByRowClick="True" ConfirmDelete="True" />

                    
                        <SettingsCommandButton>
                            <SelectButton ButtonType="Button">
                            </SelectButton>
                        </SettingsCommandButton>
                       <Columns>

            <dx:GridViewCommandColumn ShowSelectCheckbox="True" Caption="Select"  VisibleIndex="0">
                </dx:GridViewCommandColumn>
                <dx:GridViewDataTextColumn FieldName="AccountName" ReadOnly="True" VisibleIndex="1">
            
                </dx:GridViewDataTextColumn>
                     <dx:GridViewDataTextColumn FieldName="ATPCSD" VisibleIndex="2">
                </dx:GridViewDataTextColumn>
                  <dx:GridViewDataTextColumn FieldName="TotalQuantity" VisibleIndex="3">
                </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Price" VisibleIndex="4">
                </dx:GridViewDataTextColumn>
                                       
                                   <dx:GridViewDataTextColumn FieldName="TotalBrokerAmount" VisibleIndex="5">
                </dx:GridViewDataTextColumn>
                                       
                              <dx:GridViewDataTextColumn FieldName="DatePosted" VisibleIndex="6">
                </dx:GridViewDataTextColumn>
                                         
                              <dx:GridViewDataTextColumn FieldName="Deals" VisibleIndex="7">
                </dx:GridViewDataTextColumn>

                             <dx:GridViewDataTextColumn FieldName="Security" VisibleIndex="8">
                </dx:GridViewDataTextColumn>
                                       
           
            </Columns>

                      

                       

                    </dx:ASPxGridView>
             <table class="table table-responsive">
                 <tr>
                     <td style="width:170px">
                         <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="AccountName" Theme="Glass"></dx:ASPxLabel>
                       <dx:ASPxTextBox ID="AccName" runat="server" Width="170px" ReadOnly="true" Theme="Glass"></dx:ASPxTextBox>
                   
                     </td>
                     <td style="width:170px">
                           <dx:ASPxLabel ID="ASPxLabel4" runat="server" Text="Account" Theme="Glass"></dx:ASPxLabel>
                      <dx:ASPxTextBox ID="AccNum" runat="server" Width="170px" ReadOnly="true" Theme="Glass"></dx:ASPxTextBox>
                   
                     </td>
                     <td style="width:170px">
                           <dx:ASPxLabel ID="ASPxLabel5" runat="server" Text="TotalQuantity" Theme="Glass"></dx:ASPxLabel>
                     
                          <dx:ASPxTextBox ID="AccQ" runat="server" Width="170px" ReadOnly="true" Theme="Glass"></dx:ASPxTextBox>
                   
                     </td>
                     </tr>
                 <tr>
                      <td style="width:170px">
                           <dx:ASPxLabel ID="ASPxLabel8" runat="server" Text="Price" Theme="Glass"></dx:ASPxLabel>
                      <dx:ASPxTextBox ID="Price" runat="server" Width="170px" ReadOnly="true" Theme="Glass"></dx:ASPxTextBox>
                   
                     </td>
                     <td style="width:170px">
                           <dx:ASPxLabel ID="ASPxLabel6" runat="server" Text="TotalBrokerAmount" Theme="Glass"></dx:ASPxLabel>
                      <dx:ASPxTextBox ID="AccBR" runat="server" Width="170px" ReadOnly="true" Theme="Glass"></dx:ASPxTextBox>
                   
                     </td>
                       <td style="width:170px">
                           <dx:ASPxLabel ID="ASPxLabel7" runat="server" Text="DatePosted" Theme="Glass"></dx:ASPxLabel>
                      <dx:ASPxTextBox ID="DTePost" runat="server" Width="170px" ReadOnly="true" Theme="Glass"></dx:ASPxTextBox>
                   
                     </td>
                     </tr>
                 <tr>
                  <td style="width:170px">
                           <dx:ASPxLabel ID="ASPxLabel9" runat="server" Text="Security" Theme="Glass"></dx:ASPxLabel>
                      <dx:ASPxTextBox ID="txtSec" runat="server" Width="170px" ReadOnly="true" Theme="Glass"></dx:ASPxTextBox>
                   
                     </td>
                     </tr>
                 <tr>
                     <td>
                         <br />
                         <dx:ASPxButton ID="ASPxButton3" runat="server" CssClass="btn btn-primary" Text="View Report" OnClick="ASPxButton3_Click"></dx:ASPxButton>
                     </td>
                 </tr>
             </table>
             </div>
            </div>
        </div>
    </div>
      
    </div>
    <script type="text/javascript">
        $(function () {
            var tabName = $("[id*=TabName]").val() != "" ? $("[id*=TabName]").val() : "personal";
            $('#Tabs a[href="#' + tabName + '"]').tab('show');
            $("#Tabs a").click(function () {
                $("[id*=TabName]").val($(this).attr("href").replace("#", ""));
            });
        });
</script>

</asp:content>
