﻿<%@ Page Language="C#" MaintainScrollPositionOnPostback="true" MasterPageFile="~/Site1.Master"  AutoEventWireup="true" CodeBehind="Batch.aspx.cs" Inherits="BrokerOffice.PrimaryMarket.Batch" %>
<%@ Register Assembly="DevExpress.Web.v18.2, Version=18.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>


<asp:content id="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   
    <div>


  <div class="panel panel-default" style=" padding: 10px; margin: 10px">
    <div class="panel-heading" style="background-color:#428bca">
        <i style="color:white">Batch Application Form</i>
    </div>
        <div id="Tabs" role="tabpanel">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li class="active"><a href="#personal" aria-controls="personal" role="tab" data-toggle="tab">
                   Batch Number ,    <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
</a></li>
                       
            </ul>
            <!-- Tab panes -->
            <div class="tab-content" style="padding-top: 20px">
                  <div role="tabpanel" class="tab-pane active" id="personal">
                               <table class="table table-striped">
                                     <tr>
                                 <td>
                                     <asp:Label runat="server"  Text="Investor" Font-Bold="True"></asp:Label>
                                         <br />
                                       <dx:ASPxComboBox ID="drpname" runat="server" ValueType="System.String" Height="19px" TextField="FullNames" Theme="BlackGlass" ValueField="id" Width="258px"></dx:ASPxComboBox>
                                         
                                   </td>
                                      <td>
                                     <asp:Label runat="server"  Text="Number Of Shares" Font-Bold="True"></asp:Label>  
                                   <br />
<asp:TextBox id="qty" CssClass="form-control" runat="server" Width="250px" OnTextChanged="qty_TextChanged" AutoPostBack="True" ></asp:TextBox>
                                     
                                 </td>
                                            

                  
                                        </tr>
                                   <tr>
                                       <td>
     <asp:Label runat="server"  Text="Batch Price" Font-Bold="True"></asp:Label>  
                                   <br />
<asp:TextBox id="TextBox2" CssClass="form-control" runat="server" Width="250px" ReadOnly="true"></asp:TextBox>
                                    
                                       </td>
                                       <td>
 <asp:Label runat="server"  Text="Client Amount" Font-Bold="True"></asp:Label>  
                                   <br />
<asp:TextBox id="TextBox1" CssClass="form-control" runat="server" Width="250px" AutoPostBack="True" ReadOnly="True"  ></asp:TextBox>
                                    
                                       </td>
                                   </tr>
                                       <tr>

                                           <td>
                                               <br />
<asp:Button ID="Submit" runat="server" CssClass="btn btn-primary" Text="Submit" OnClick="Submit_Click" style="width:150px" ></asp:Button>

                    
                                            
                    
                                           </td>
                                             <td>
                                               <br />

                                            
                    
                                           </td>
                                
                                   </tr>
                             </table>
                           
                </div>
                <div role="tabpanel" class="tab-pane" id="Action">
                  <table class="table table-striped">
                      <tr>
                          <td>
                              &nbsp;</td>
                          <td>
                              &nbsp;</td>
                      </tr>
                      <tr>
                          <td>
                              &nbsp;</td>
                          <td>
                              <asp:TextBox ID="clcValue" ReadOnly="true" Width="150px"  CssClass="form-control" runat="server"></asp:TextBox>
                          </td>
                      </tr>
            <tr>
                          <td>
                              &nbsp;</td>
              
                               <td>
                                           <asp:HiddenField ID="TabName" runat="server" />
                                     
                                       </td>
                          
                      </tr>
                  </table>
                  
                </div>
      
            </div>
        </div>
    </div>
      
    </div>
    <script type="text/javascript">
$(function () {
    var tabName = $("[id*=TabName]").val() != "" ? $("[id*=TabName]").val() : "personal";
    $('#Tabs a[href="#' + tabName + '"]').tab('show');
    $("#Tabs a").click(function () {
        $("[id*=TabName]").val($(this).attr("href").replace("#", ""));
    });
});
</script>

</asp:content>