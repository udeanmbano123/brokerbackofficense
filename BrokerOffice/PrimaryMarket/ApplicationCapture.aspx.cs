﻿using BrokerOffice.DAO;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebMatrix.WebData;

namespace BrokerOffice.PrimaryMarket
{

    public partial class ApplicationCapture : System.Web.UI.Page
    {
        public string id = "";
        private SBoardContext db = new SBoardContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            id = Request.QueryString["id"];
            if (this.IsPostBack)
            {
                TabName.Value = Request.Form[TabName.UniqueID];

            }
            else
            {
                loadBatch();
                loadClient();
                if (id == " " || id == null)
                {

                }
                else
                {
                    getDetails(id);
                }
            }
        }
        protected void getDetails(string x)
        {

            string per = "";
            if (x != "")
            {
                Submit.Text = "Update";
                txtFkey.Text = x;

                var acc = db.ApplicationCaptures.ToList().Where(a => a.ApplicationCaptureID == Convert.ToInt32(x));

                foreach (var my in acc)
                {
                  batchID.Text=my.BatchID.ToString();
                    batchT.Text=my.BatchTotal.ToString() ;
                    batchref.Text=my.BatchRef;
                    oldBatch.Text= my.BatchID.ToString();
                    txtClientID.Text=my.ClientID ;
                    xtClientNumber.Text=my.ClientNumber;
                   txtFirst.Text=my.ClientNames.ToString();
                   txtUnits.Text=my.ClientUnits.ToString();
                    hldBatch.Text= my.ClientUnits.ToString();
                    txtValue.Text=my.value.ToString();
                    //my.tempstatus = "PENDING";
                    var br = db.Batches.ToList().Where(a => a.BatchID == my.BatchID);
                    foreach(var b in br)
                    {
                        drpBatch.Items.Insert(0, new ListItem("No." + b.BatchID + "Ref" + b.Batchref + "Units" + b.BatchUnits, b.BatchID.ToString()));
                    }

                    var cr = db.Account_Creations.ToList().Where(a => a.CDSC_Number == my.ClientNumber);
                    foreach(var p in cr)
                    {
                  drpname.Items.Insert(0, new ListItem("Firstname: " + p.OtherNames + " Surname:" + p.Surname_CompanyName , p.CDSC_Number));
                    }
            
                    ;
                }
            }
          
            return;
        }

        protected void loadBatch()
        {
            drpBatch.Items.Clear();
            drpBatch.DataSource = null;
            var ds = from c in db.Batches
                     select new

                     {

                         ID = c.BatchID,

                         Batch= "No." + c.BatchID + "Ref" + c.Batchref + "Units" + c.BatchUnits,

                     };
            drpBatch.DataSource = ds.ToList();
            drpBatch.DataTextField = "Batch";
            // text field name of table dispalyed in dropdown
            drpBatch.DataValueField = "ID"; // to retrive specific  textfield name 
            //assigning datasource to the dropdownlist
            drpBatch.DataBind(); //binding dropdownlist
                                 // allocate.Items.Insert(0, "Select");
            drpBatch.Items.Insert(0, new ListItem("Please Select a batch", "0"));
        }
        protected void loadClient()
        {
            drpname.Items.Clear();
            drpname.DataSource = null;
            var ds = from c in db.Account_Creations
                     select new

                     {

                         CDSC_Number = c.CDSC_Number,

                         CustomerNames = "Firstname: " + c.OtherNames + " Surname:" + c.Surname_CompanyName ,

                     };
            drpname.DataSource = ds.ToList();
            drpname.DataTextField = "CustomerNames";
            // text field name of table dispalyed in dropdown
            drpname.DataValueField = "CDSC_Number"; // to retrive specific  textfield name 
            //assigning datasource to the dropdownlist
            drpname.DataBind(); //binding dropdownlist
                                // allocate.Items.Insert(0, "Select");
            drpname.Items.Insert(0, new ListItem("Please Select a customer", "0"));
        }

        protected void Submit_Click(object sender, EventArgs e)
        {

            if (batchID.Text=="" || batchT.Text=="" || batchref.Text=="")
            {
                msgbox("Batch details are required");
                return;
            }else if(txtClientID.Text == "" || xtClientNumber.Text == "" || txtFirst.Text == "" )
            {
                msgbox("Customer details are required");
                return;
            }
   
            double n;
            bool isNumeric = double.TryParse(txtUnits.Text, out n);

            if (isNumeric == false)
            {
                msgbox("Must be a figure");
                txtUnits.Text = "";
                return;

            }
        
        


            if (Submit.Text == "Submit")
            {
                BrokerOffice.Models.ApplicationCapture my = new BrokerOffice.Models.ApplicationCapture();
                my.BatchID = Convert.ToInt32(batchID.Text);
                my.BatchTotal = Convert.ToDouble(batchT.Text);
                my.BatchRef = batchref.Text;

                my.ClientID = txtClientID.Text;
                my.ClientNumber = xtClientNumber.Text;
                my.ClientNames = txtFirst.Text;
                my.ClientUnits = Convert.ToDouble(txtUnits.Text);
                my.value = Convert.ToInt32(txtValue.Text);
                my.tempstatus = "PENDING";
                db.ApplicationCaptures.Add(my);

                //Update table  
                db.SaveChanges(WebSecurity.CurrentUserName.ToString());
            
                //Update reduce the batch
                var x = db.Batches.Find(Convert.ToInt32(batchID.Text));
               
                x.BatchUnits = Convert.ToDouble(my.BatchTotal-my.ClientUnits);
              
                //Update Row in Table  
                db.Batches.AddOrUpdate(x);
                db.SaveChanges(WebSecurity.CurrentUserName.ToString());
                System.Web.HttpContext.Current.Session["NOT"] = "You have successfully added the application";
                Response.Redirect("~/Applications/Index");
            }
            else if (Submit.Text == "Update")
            {
                var my = db.ApplicationCaptures.Find(Convert.ToInt32(txtFkey.Text));
                my.BatchID = Convert.ToInt32(batchID.Text);
                my.BatchTotal = Convert.ToDouble(batchT.Text);
                my.BatchRef = batchref.Text;

                my.ClientID = txtClientID.Text;
                my.ClientNumber = xtClientNumber.Text;
                my.ClientNames = txtFirst.Text;
                my.ClientUnits = Convert.ToDouble(txtUnits.Text);
                my.value = Convert.ToInt32(txtValue.Text);
                my.tempstatus = "PENDING";

                //Update Row in Table  
                db.ApplicationCaptures.AddOrUpdate(my);
                db.SaveChanges(WebSecurity.CurrentUserName.ToString());
                // UpdateModel the batch
                double hidb = Convert.ToDouble(hldBatch.Text);
                    var x = db.Batches.Find(Convert.ToInt32(batchID.Text));
                if(oldBatch.Text== batchID.Text)
                {
               x.BatchUnits = Convert.ToDouble((my.BatchTotal+ hidb)- my.ClientUnits);

                }else
                {
                    x.BatchUnits = Convert.ToDouble(my.BatchTotal - my.ClientUnits);
                }
                
                //Update Row in Table  
                db.Batches.AddOrUpdate(x);
                db.SaveChanges(WebSecurity.CurrentUserName.ToString());
                System.Web.HttpContext.Current.Session["NOT"] = "You have successfully updated the application";
                Response.Redirect("~/Applications/Index");
            }
        }
        public void msgbox(string strMessage)
        {
            string strScript = "<script language=JavaScript>";
            strScript += "window.alert(\"" + strMessage + "\");";
            strScript += "</script>";
            System.Web.UI.WebControls.Label lbl = new System.Web.UI.WebControls.Label();
            lbl.Text = strScript;
            Page.Controls.Add(lbl);
        }
        protected void Clear()
        {
            Response.Redirect("~/Applications/Index");
        }

        protected void txtUnits_TextChanged(object sender, EventArgs e)
        {
            double n;
            bool isNumeric = double.TryParse(txtUnits.Text, out n);

            if (isNumeric == false)
            {
                msgbox("Must be a figure");
                txtUnits.Text = "";
                return;

            }

           
            txtValue.Text = ((Convert.ToDouble(batchT.Text)) * Convert.ToDouble(txtUnits.Text)).ToString();
        }

        protected void drpBatch_SelectedIndexChanged(object sender, EventArgs e)
        {
            var db2 = db.Batches.ToList().Where(a => a.BatchID==Convert.ToInt32(drpBatch.SelectedValue.ToString()));

            foreach(var c in db2)
            {
                batchID.Text = c.BatchID.ToString();
                batchT.Text = c.BatchUnits.ToString();
                batchref.Text = c.Batchref.ToString();
            }
        }

        protected void drpname_SelectedIndexChanged(object sender, EventArgs e)
        {
            var db2 = db.Account_Creations.ToList().Where(a => a.CDSC_Number==drpname.SelectedValue.ToString());

            foreach (var c in db2)
            {
                txtClientID.Text = c.Identification.ToString();
               xtClientNumber.Text = c.CDSC_Number.ToString();
                txtFirst.Text = c.Surname_CompanyName.ToString()+" " + c.OtherNames.ToString();
            }
        }

        protected void dxAction_Click(object sender, EventArgs e)
        {
            Label44.Text = "Action";
            TabName.Value = Label44.Text;
            return;
        }

        protected void dxPersonal_Click(object sender, EventArgs e)
        {
            Label44.Text = "personal";
            TabName.Value = Label44.Text;
            return;
        }
    }
}