﻿
<%@ Page Language="C#" MaintainScrollPositionOnPostback="true" MasterPageFile="~/Site1.Master" Async="true"    AutoEventWireup="true" CodeBehind="Trades.aspx.cs" Inherits="BrokerOffice.PrimaryMarket.Trades" %>
<%@ Register Assembly="DevExpress.Web.v18.2, Version=18.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>


<asp:content id="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   
  <div style="background:white" class="form-horizontal">
    <div class="panel-heading" style="background-color:#428bca">
        <i style="color:white">Matched Orders</i>
       
    </div> 

      <dx:ASPxGridView ID="GridView1" CssClass="table table-striped" onselectedindexchanged="GridView1_SelectedIndexChanged"
    AllowPaging="True"  OnPageIndexChanging="grdData_PageIndexChanging" runat="server" Theme="Glass" EnableTheming="True">
          <SettingsPager AlwaysShowPager="True" PageSize="10" EnableAdaptivity="True">
          </SettingsPager>
          
      </dx:ASPxGridView>
   
  
    </div>
    <script type="text/javascript">
$(function () {
    var tabName = $("[id*=TabName]").val() != "" ? $("[id*=TabName]").val() : "personal";
    $('#Tabs a[href="#' + tabName + '"]').tab('show');
    $("#Tabs a").click(function () {
        $("[id*=TabName]").val($(this).attr("href").replace("#", ""));
    });
});
</script>

</asp:content>