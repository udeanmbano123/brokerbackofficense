﻿<%@ Page Language="C#" MaintainScrollPositionOnPostback="true" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="PortfolioCreation.aspx.cs" Inherits="BrokerOffice.PrimaryMarket.PortfolioCreation" %>


<%@ Register Assembly="DevExpress.Web.v18.2, Version=18.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<asp:content id="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   
   <div style="margin-left:15%;margin-right:10%;background:white" class="form-horizontal">
        <div class="panel-heading" style="background-color:#428bca">
            <i style="color:white">Client Portfolios Form</i>
        </div>
        <hr />
       <dx:ASPxLabel ID="ASPxLabel4" runat="server" Text="Search Customer" Width="170px"></dx:ASPxLabel>
       <dx:ASPxTextBox ID="txtSearch" CssClass="form-control" runat="server" Width="170px"></dx:ASPxTextBox>
       <dx:ASPxButton ID="btnSearch" runat="server" Theme="Moderno" Text="Search" OnClick="btnSearch_Click"></dx:ASPxButton>  
       <table class="table table-responsive">
          
    <tr>
                  <asp:Label ID="txtFkey" runat="server" Visible="False" Text=""></asp:Label>
               <td>
                    <asp:GridView ID="GridView1" CssClass="table table-striped" runat="server"   EmptyDataText="No records has been added."
    AutoGenerateSelectButton="true" onselectedindexchanged="GridView1_SelectedIndexChanged"
    AllowPaging="True" PageSize="5" OnPageIndexChanging="grdData_PageIndexChanging">  
                         <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                        </asp:GridView>

               </td>
           </tr>
       </table>
          
       <table class="table table-striped">
  
           <tr>

               <td>
                   <dx:ASPxLabel ID="ASPxLabel1" CssClass="control-label col-md-6" runat="server" Text="ClientNumber"></dx:ASPxLabel>
                   <dx:ASPxTextBox ID="txtClient" CssClass="form-control" ReadOnly="true" runat="server" Width="200px"></dx:ASPxTextBox>
               </td>

               <td>
                   <dx:ASPxLabel ID="ASPxLabel2" CssClass="control-label col-md-4" runat="server" Text="Stock"></dx:ASPxLabel>
                   <asp:DropDownList ID="drpStock" CssClass="form-control" runat="server" Width="200px"></asp:DropDownList>
                 
               </td>


               <td>
                   <dx:ASPxLabel ID="ASPxLabel3" CssClass="control-label col-md-4" runat="server" Text="Holdings"></dx:ASPxLabel>
                   <dx:ASPxTextBox ID="txtHoldings" CssClass="form-control" runat="server" Theme="Moderno" Width="200px"></dx:ASPxTextBox>
               </td>
           </tr>
           <tr>
               <td>
                   <dx:ASPxButton ID="btnSubmit" CssClass="btn btn-primary" runat="server" Theme="Moderno" Text="Submit" OnClick="btnSubmit_Click"></dx:ASPxButton>
               </td>
               <td>

               </td>
               <td>

               </td>
           </tr>
           </table>

       </div>

</asp:content>