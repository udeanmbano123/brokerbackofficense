﻿<%@ Page Language="C#" MaintainScrollPositionOnPostback="true" MasterPageFile="~/Site1.Master"  AutoEventWireup="true" CodeBehind="ApplicationCapture.aspx.cs" Inherits="BrokerOffice.PrimaryMarket.ApplicationCapture" %>
<%@ Register Assembly="DevExpress.Web.v18.2, Version=18.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:content id="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   
    <div>


  <div class="panel panel-default" style=" padding: 10px; margin: 10px">
    <div class="panel-heading" style="background-color:#428bca">
        <i style="color:white">Application Capture Form</i>
    </div>
        <div id="Tabs" role="tabpanel">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li class="active"><a href="#personal" aria-controls="personal" role="tab" data-toggle="tab">
                    Application </a></li>
                <li ><a href="#Action" aria-controls="Action" role="tab" data-toggle="tab">
                    Cost</a></li>
          
            </ul>
            <!-- Tab panes -->
            <div class="tab-content" style="padding-top: 20px">
                  <div role="tabpanel" class="tab-pane active" id="personal">
                               <table class="table table-striped">
                                     <asp:Label ID="txtFkey" runat="server" Visible="False" Text=""></asp:Label>
                                   <asp:Label ID="hldBatch" runat="server" Text="Label" Visible="False"></asp:Label>
                                   <asp:Label ID="oldBatch" runat="server" Text="Label" Visible="False"></asp:Label>
               <tr>
                   <td>
                       <asp:Label ID="Label1" CssClass="control-label col-md-6" runat="server" Text="Select Batch"></asp:Label>
                        <br />
                       <asp:DropDownList ID="drpBatch" Width="150px" AutoPostBack="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="drpBatch_SelectedIndexChanged"></asp:DropDownList>
                   </td>
                  
               </tr>
                   <tr>
                       <td>
                           <asp:Label ID="Label2" CssClass="control-label col-md-6" runat="server" Text="Batch ID"></asp:Label>
                           <br />
                           <asp:TextBox ID="batchID" CssClass="form-control" runat="server" Width="150px" ReadOnly="True"></asp:TextBox>
                       </td>

                       <td>
                           <asp:Label ID="Label3" CssClass="control-label col-md-6" runat="server" Text="Batch Total"></asp:Label>
                           <br />
                           <asp:TextBox ID="batchT" CssClass="form-control" runat="server" Width="150px"  ReadOnly="True"></asp:TextBox>
                       </td>
                       <td>
                           <asp:Label ID="Label4" CssClass="control-label col-md-6" runat="server" Text="Batch Ref"></asp:Label>
                           <br />
                           <asp:TextBox ID="batchref" CssClass="form-control" runat="server" Width="150px"  ReadOnly="True"></asp:TextBox>
                       </td>
                    </tr>
                                   <tr>
                                       <td>
                                           <asp:Label ID="Label5" CssClass="control-label col-md-6" runat="server" Text="Select Client"></asp:Label>
                                            <br />
                                           <asp:DropDownList id="drpname" AutoPostBack="true" CssClass="form-control" Width="150px"   runat="server" OnSelectedIndexChanged="drpname_SelectedIndexChanged"></asp:DropDownList>
                                       </td>
                                   </tr>
                                   <tr>
                                       <td>
                                           <asp:Label ID="Label7" runat="server" Text="Client ID"></asp:Label>
                                            <br />
                                           <asp:TextBox ID="txtClientID" CssClass="form-control"  runat="server" Width="150px"  ReadOnly="True"></asp:TextBox>
                                       </td>
                                       <td>
                                           <asp:Label ID="Label6" runat="server" Text="Client Number"></asp:Label>
                                            <br />
                                           <asp:TextBox ID="xtClientNumber" CssClass="form-control" runat="server" Width="150px"  ReadOnly="True"></asp:TextBox>
                                        </td>
                                       <td>
    <asp:Label ID="Label8" runat="server" Text="Names"></asp:Label>
                                            <br />
                                           <asp:TextBox ID="txtFirst" CssClass="form-control" runat="server" Width="150px"  ReadOnly="True"></asp:TextBox>
                                      
                                       </td>
                                   </tr>
                                   <tr>
                                       <td>
                                           &nbsp;</td>
                                       <td>
                                           &nbsp;</td>
                                   </tr>
                             </table>
                          <dx style="float:right"><dx:ASPxButton ID="dxAction" runat="server" CssClass="btn btn-primary" Theme="Moderno" Text="Next" OnClick="dxAction_Click"  ></dx:ASPxButton></dx>
                        
                </div>
                <div role="tabpanel" class="tab-pane" id="Action">
                  <table class="table table-striped">
                    <tr>
                        <td>
                            <asp:Label ID="Label10" runat="server" Text="Enter Units"></asp:Label>
                             <br />
                            <asp:TextBox ID="txtUnits"  CssClass="form-control" runat="server" AutoPostBack="True" Width="150px"  OnTextChanged="txtUnits_TextChanged"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Label ID="Label11" runat="server" Text="Value"></asp:Label>
                            <br />
                            <asp:TextBox ID="txtValue" CssClass="form-control" runat="server" Width="150px"  ReadOnly="True"></asp:TextBox>
                        </td>
                    </tr>
                     
                      <tr>
                          <td>
<asp:Button ID="Submit" runat="server" CssClass="btn btn-primary" Text="Submit" OnClick="Submit_Click"></asp:Button>
                          </td>
              
                               <td>
                                           <asp:HiddenField ID="TabName" runat="server" />
                                      <asp:Label ID="Label44" runat="server" Visible="false" Text=""></asp:Label>
                                     
                                       </td>
                          
                      </tr>
                  </table>
                     <dx style="float:right"><dx:ASPxButton ID="dxPersonal" runat="server" CssClass="btn btn-primary" Theme="Moderno" Text="Previous" OnClick="dxPersonal_Click" ></dx:ASPxButton></dx>
              
                </div>
      
            </div>
        </div>
    </div>
      
    </div>
      <script type="text/javascript">
$(function () {
    var tabName = $("[id*=TabName]").val() != "" ? $("[id*=TabName]").val() : "personal";
    $('#Tabs a[href="#' + tabName + '"]').tab('show');
    $("#Tabs a").click(function () {
        $("[id*=TabName]").val($(this).attr("href").replace("#", ""));
    });
});
</script>

</asp:content>