﻿using BrokerOffice.DAO;
using BrokerOffice.DealClass;
using BrokerOffice.DealNotes;
using BrokerOffice.Models;
using DevExpress.XtraReports.UI;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Linq.SqlClient;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Text;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Sockets;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebMatrix.WebData;
using System.IO.Compression;

namespace BrokerOffice.Reporting
{
    public partial class DealSelector : System.Web.UI.Page
    {
        public static string GetDataFilePath() => HttpRuntime.AppDomainAppVirtualPath != null ?
      Path.Combine(HttpRuntime.AppDomainAppPath, "DownloadFolder") :
      Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

        private SBoardContext db = new SBoardContext();
        protected void Page_Load(object sender, EventArgs e)
        {

            if (this.IsPostBack == true)
            {

                TabName.Value = Request.Form[TabName.UniqueID];

            }
            if (!(this.IsPostBack))
            {
                drpname.DataSource = GetDataSource();
                drpname.DataBind();
            }

            if (txtSearch.Text != "")
            {
                //ASPxTextBox1.Text = "";
                loadCustomers();
            }
            if (ASPxTextBox1.Text != "")
            {
                //txtSearch.Text = "";
                loadCustomerss();
            }
            try
            {

                if (RadioButtonList2.SelectedIndex < 0 && drpname.SelectedItem.Text.ToString() != "Select Investor" && ASPxDateEdit2.Text != "")
                {
                    loadCon();
                }
            }
            catch (Exception)
            {

            }



        }

        private DataTable GetDataSource()
        {
            //DataTable defined
            var dtSource = new DataTable();
            dtSource.Columns.Add("id", typeof(string));
            dtSource.Columns.Add("FullNames", typeof(string));
            string x = "";

            string name = "";
            dtSource.Rows.Add("0", "Select Investor");
            var dd = from c in db.Account_Creations
                     where c.StatuSActive == true
                     select c;
            foreach (var p in dd)
            {
                if (p.OtherNames == null)
                {
                    name = p.Surname_CompanyName + " ," + p.CDSC_Number;
                }
                else if (p.OtherNames == "")
                {
                    name = p.Surname_CompanyName + " ," + p.CDSC_Number;
                }
                else
                {
                    name = p.OtherNames + " " + p.Surname_CompanyName + " ," + p.CDSC_Number;
                }

                dtSource.Rows.Add(p.CDSC_Number, name);
            }





            return dtSource;
        }
        protected void loadCon()
        {
            ASPxGridView2.DataSource = GetEarners(RadioButtonList2.SelectedValue.ToString(), drpname.SelectedItem.Value.ToString(), ASPxDateEdit2.Text);
            ASPxGridView2.DataBind();
        }
        protected async void loadCustomers()
        {

            string nme = "NSE";
      


            var dbsel = from s in db.DealerDGs
                        join v in db.Account_Creations on s.Account1 equals v.CDSC_Number
                        let DealNumber = s.DealNumber
                        let Security = s.Security
                        let Deal = s.Deal
                        let Quantity = s.Quantity
                        let Price = s.Price
                        let BuyerClient = s.AccountName1
                        let SellerClient = s.AccountName2
                        let DatePosted = s.PostDate
                        let SettlementDate = s.SettlementDate
                        let fullNames = v.Surname_CompanyName + " " + v.OtherNames + " " + v.CDSC_Number
                        where (v.Surname_CompanyName + " " + v.OtherNames + " " + v.CDSC_Number + " " + Deal).ToLower().Contains((txtSearch.Text).ToLower()) || v.Broker == nme

                        select new { DealNumber, Security, Deal, Quantity, Price, BuyerClient, SellerClient, DatePosted, SettlementDate };

            var dbsel2 = from s in db.DealerDGs
                         join v in db.Account_Creations on s.Account2 equals v.CDSC_Number
                         let DealNumber = s.DealNumber
                         let Security = s.Security
                         let Deal = s.Deal
                         let Quantity = s.Quantity
                         let Price = s.Price
                         let BuyerClient = s.AccountName1
                         let SellerClient = s.AccountName2
                         let DatePosted = s.PostDate
                         let SettlementDate = s.SettlementDate
                         let fullNames = v.Surname_CompanyName + " " + v.OtherNames + " " + v.CDSC_Number
                         where (v.Surname_CompanyName + " " + v.OtherNames + " " + v.CDSC_Number + " " + Deal).ToLower().Contains((txtSearch.Text).ToLower()) || v.Broker == nme

                         select new { DealNumber, Security, Deal, Quantity, Price, BuyerClient, SellerClient, DatePosted, SettlementDate };

            var allresults = dbsel.Union(dbsel2).ToList().OrderByDescending(a => a.DatePosted);

            ASPxGridView1.DataSource = null;

            ASPxGridView1.DataSource = allresults;

            ASPxGridView1.DataBind();


        }
        protected async void loadCustomerss()
        {

            string nme = "";
            var users = db.Users.ToList().Where(a => a.Email == WebSecurity.CurrentUserName);

            foreach (var dd in users)
            {
                nme = dd.BrokerCode;
            }


            var dbsel = from s in db.DealerDGs
                        join v in db.Account_Creations on s.Account1 equals v.CDSC_Number
                        let DealNumber = s.DealNumber
                        let Security = s.Security
                        let Deal = s.Deal
                        let Quantity = s.Quantity
                        let Price = s.Price
                        let BuyerClient = s.AccountName1
                        let SellerClient = s.AccountName2
                        let DatePosted = s.PostDate
                        let SettlementDate = s.SettlementDate
                        let fullNames = v.Surname_CompanyName + " " + v.OtherNames + " " + v.CDSC_Number
                        where (DealNumber.ToString()) == ASPxTextBox1.Text

                        select new { DealNumber, Security, Deal, Quantity, Price, BuyerClient, SellerClient, DatePosted, SettlementDate };

            var dbsel2 = from s in db.DealerDGs
                         join v in db.Account_Creations on s.Account2 equals v.CDSC_Number
                         let DealNumber = s.DealNumber
                         let Security = s.Security
                         let Deal = s.Deal
                         let Quantity = s.Quantity
                         let Price = s.Price
                         let BuyerClient = s.AccountName1
                         let SellerClient = s.AccountName2
                         let DatePosted = s.PostDate
                         let SettlementDate = s.SettlementDate
                         let fullNames = v.Surname_CompanyName + " " + v.OtherNames + " " + v.CDSC_Number
                         where (DealNumber.ToString()) == ASPxTextBox1.Text
                         select new { DealNumber, Security, Deal, Quantity, Price, BuyerClient, SellerClient, DatePosted, SettlementDate };

            var allresults = dbsel.Union(dbsel2).ToList().OrderByDescending(a => a.DatePosted);

            ASPxGridView1.DataSource = null;

            ASPxGridView1.DataSource = allresults;

            ASPxGridView1.DataBind();


        }

        public string Name(string s)
        {
            string uv = "";
            var pp = db.Account_Creations.ToList().Where(a => a.CDSC_Number == s);

            foreach (var k in pp)
            {
                uv = k.Surname_CompanyName + " " + k.OtherNames;
            }
            if (uv == "")
            {
                uv = "No Name";
            }
            return uv;
        }


        protected void btnSearch_Click(object sender, EventArgs e)
        {
            ASPxTextBox1.Text = "";
            if (txtSearch.Text == "")
            {
                msgbox("Please enter keyword to search");
                return;
            }
            else
            {
                loadCustomers();
            }

        }

        protected void txtView_Click(object sender, EventArgs e)
        {
            if (txtAcc.Text == "")
            {
                msgbox("Account Number must be selected");
                return;
            }
            else if (txtOrder.Text == "")
            {
                msgbox("Order number selected");
                return;
            }
            else if (RadioButtonList4.SelectedIndex < 0)
            {
                msgbox("Buyer Or Seller  must be selected");
                return;
            }
            else
            {

                if (RadioButtonList4.SelectedValue == "BUYER")
                {


                    HttpContext.Current.Session["Type"] = "BUY";
                    HttpContext.Current.Session["Type2"] = "PURCHASE";

                }
                else if (RadioButtonList4.SelectedValue == "SELLER")
                {


                    HttpContext.Current.Session["Type"] = "SELL";
                    HttpContext.Current.Session["Type2"] = "SALE";
                }
                string strscript = "<script>window.location.href='../Reporting/DealReport.aspx'</script>";

                if (!ClientScript.IsClientScriptBlockRegistered("clientscript"))
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "clientscript", strscript);
                }
                //Response.Redirect("~/");
            }
        }

        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("Local IP Address Not Found!");
        }

        protected void ASPxGridView1_SelectionChanged(object sender, EventArgs e)
        {
            object key = null;
            string deal = "";
            for (int i = 0; i < ASPxGridView1.VisibleRowCount; i++)
            {
                if (ASPxGridView1.Selection.IsRowSelected(i))
                {
                    key = ASPxGridView1.GetRowValues(i, "Deal");
                    deal = ASPxGridView1.GetRowValues(i, "Quantity").ToString();
                }

            }
            string nm = deal;
            string my = "";
            try
            {
                my = key.ToString();
            }
            catch (Exception)
            {

                my = "";
            }


            List<object> keys = ASPxGridView1.GetSelectedFieldValues(ASPxGridView1.KeyFieldName);

            foreach (var p in keys)
            {
                my = p.ToString();
            }

            Label2.Text = my;

            var dbsel = db.DealerDGs.ToList().Where(a => a.Deal == my);
            string Buyer = "";
            string Seller = "";
            string sid = "";

            foreach (var b in dbsel)
            {



                HttpContext.Current.Session["SID"] = b.Deal;
                HttpContext.Current.Session["Company"] = b.Security;
                HttpContext.Current.Session["max"] = b.Price;
                HttpContext.Current.Session["qty"] = b.Quantity;
                HttpContext.Current.Session["Dte"] = b.SettlementDate;
                Buyer = b.Account1;
                Seller = b.Account2;
                sid = b.Deal.ToString();

            }
            int s = 0;
            int s2 = 0;
            try
            {
                s = db.Account_Creations.ToList().Where(a => a.CDSC_Number == Buyer).Take(1).Count();
            }
            catch (Exception)
            {

                s = 0;
            }
            try
            {
                s2 = db.Account_Creations.ToList().Where(a => a.CDSC_Number == Seller).Take(1).Count();
            }
            catch (Exception)
            {

                s2 = 0;
            }
            if (s == 1)
            {
                HttpContext.Current.Session["Ord"] = sid;
                txtOrder.Text = sid;
                txtAcc.Text = Buyer;
                HttpContext.Current.Session["Acc"] = Buyer;
                HttpContext.Current.Session["Type"] = "BUY";
                HttpContext.Current.Session["Type2"] = "PURCHASE";
                RadioButtonList4.SelectedValue = "BUYER";
            }
            else if (s2 == 1)
            {
                HttpContext.Current.Session["Ord"] = sid;
                txtOrder.Text = sid;
                txtAcc.Text = Seller;
                HttpContext.Current.Session["Acc"] = Seller;
                HttpContext.Current.Session["Type"] = "SELL";
                HttpContext.Current.Session["Type2"] = "SALE";

                RadioButtonList4.SelectedValue = "SELLER";
            }



            //  db.SubmitChanges();





        }

        protected void RadioButtonList4_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string my = Label2.Text;

                List<object> keys = ASPxGridView1.GetSelectedFieldValues(ASPxGridView1.KeyFieldName);

                foreach (var p in keys)
                {
                    my = p.ToString();
                }


                var dbsel = db.DealerDGs.ToList().Where(a => a.Deal == my);

                string Buyer = "";
                string Seller = "";
                string sid = "";

                foreach (var b in dbsel)
                {



                    HttpContext.Current.Session["SID"] = b.Deal;
                    HttpContext.Current.Session["Company"] = b.Security;
                    HttpContext.Current.Session["max"] = b.Price;
                    HttpContext.Current.Session["qty"] = b.Quantity;
                    HttpContext.Current.Session["Dte"] = b.SettlementDate;
                    Buyer = b.Account1;
                    Seller = b.Account2;
                    sid = b.Deal.ToString();

                }
                int s = 0;
                int s2 = 0;
                try
                {
                    s = db.Account_Creations.ToList().Where(a => a.CDSC_Number == Buyer).Count();
                }
                catch (Exception)
                {

                    s = 0;
                }
                try
                {
                    s2 = db.Account_Creations.ToList().Where(a => a.CDSC_Number == Seller).Count();
                }
                catch (Exception)
                {

                    s2 = 0;
                }
                if (s == 1 && RadioButtonList4.SelectedValue == "BUYER")
                {
                    HttpContext.Current.Session["Ord"] = sid;
                    txtOrder.Text = sid;
                    txtAcc.Text = Buyer;
                    HttpContext.Current.Session["Acc"] = Buyer;
                    HttpContext.Current.Session["Type"] = "BUY";
                    HttpContext.Current.Session["Type2"] = "PURCHASE";

                }
                else if (s2 == 1 && RadioButtonList4.SelectedValue == "SELLER")
                {
                    HttpContext.Current.Session["Ord"] = sid;
                    txtOrder.Text = sid;
                    txtAcc.Text = Seller;
                    HttpContext.Current.Session["Acc"] = Seller;
                    HttpContext.Current.Session["Type"] = "SELL";
                    HttpContext.Current.Session["Type2"] = "SALE";
                }
                else if (s2 == 0 && RadioButtonList4.SelectedValue == "SELLER")
                {
                    msgbox("The seller does not exsist in the Broker Back Office");
                    return;
                }
                else if (s == 0 && RadioButtonList4.SelectedValue == "BUYER")
                {
                    msgbox("The Buyer does not exsist in the Broker Back Office");
                    return;
                }

            }
            catch (Exception)
            {

                // throw;
            }

        }

        protected void ASPxButton2_Click(object sender, EventArgs e)
        {
            if (ASPxDateEdit1.Text == "")
            {
                msgbox("Trade From date is required");
                return;
            }
            else if (ASPxDateEdit3.Text == "")
            {
                msgbox("Trade To date is required");
                return;
            }
            else if (RadioButtonList1.SelectedIndex < 0)
            {
                msgbox("Trade date is required");
                return;
            }
            else
            {

                HttpContext.Current.Session["Type"] = RadioButtonList1.SelectedValue.ToString();
                HttpContext.Current.Session["Trade"] = Convert.ToDateTime(ASPxDateEdit1.Text).ToString("dd-MMM-yyyy");
                HttpContext.Current.Session["Trade2"] = Convert.ToDateTime(ASPxDateEdit3.Text).ToString("dd-MMM-yyyy");
                string strscript = "<script>window.location.href='../Reporting/PagedDealReport.aspx'</script>";

                if (!ClientScript.IsClientScriptBlockRegistered("clientscript"))
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "clientscript", strscript);
                }
                //Response.Redirect("~/");
            }

        }

        protected void ASPxGridView2_SelectionChanged(object sender, EventArgs e)
        {
            object key = null;
            string deal = "";
            for (int i = 0; i < ASPxGridView2.VisibleRowCount; i++)
            {
                if (ASPxGridView2.Selection.IsRowSelected(i))
                {
                    key = ASPxGridView2.GetRowValues(i, "Key");
                    AccName.Text = ASPxGridView2.GetRowValues(i, "AccountName").ToString();
                    AccNum.Text = ASPxGridView2.GetRowValues(i, "ATPCSD").ToString();
                    AccQ.Text = ASPxGridView2.GetRowValues(i, "TotalQuantity").ToString();
                    AccBR.Text = ASPxGridView2.GetRowValues(i, "TotalBrokerAmount").ToString();
                    DTePost.Text = ASPxGridView2.GetRowValues(i, "DatePosted").ToString();
                    Price.Text = ASPxGridView2.GetRowValues(i, "Price").ToString();
                    txtSec.Text = ASPxGridView2.GetRowValues(i, "Security").ToString();
                }

            }

        }

        public static List<Consolidateds> GetEarners(string type, string account, string dte)
        {
            string s1 = "Select ROW_NUMBER() OVER (ORDER BY Account1) As 'Key', AccountName1 As 'AccountName',Account1 As 'ATPCSD',sum(CAST(Quantity As Decimal(18,4))) As 'TotalQuantity',(CAST(Price As Decimal(18,4))) As 'Price',sum((CAST(Quantity As Decimal(18,2))*CAST(Price As Decimal(18,2)))) As 'TotalBrokerAmount',Security,CAST(PostDate as Date) as 'DatePosted',count((CAST(Quantity As Decimal(18,2))*CAST(Price As Decimal(18,2))))  As 'Deals' from DealerDG where Account1='" + account + "' and CAST(PostDate  AS date)=CAST('" + dte + "' as date) group by Account1,Price,CAST(PostDate as Date),AccountName1,Security having count(CAST(Price As Decimal(18,6))) >1";
            string s2 = "Select ROW_NUMBER() OVER (ORDER BY Account2) As 'Key', AccountName2 As 'AccountName',Account2 As 'ATPCSD',sum(CAST(Quantity As Decimal(18,4))) As 'TotalQuantity',(CAST(Price As Decimal(18,4))) As 'Price',sum((CAST(Quantity As Decimal(18,2))*CAST(Price As Decimal(18,2)))) As 'TotalBrokerAmount',Security,CAST(PostDate as Date) as 'DatePosted',count((CAST(Quantity As Decimal(18,2))*CAST(Price As Decimal(18,2))))  As 'Deals' from DealerDG where Account2='" + account + "' and CAST(PostDate  AS date)=CAST('" + dte + "' as date) group by Account2,Price,CAST(PostDate as Date),AccountName2,Security having count(CAST(Price As Decimal(18,6))) >1";
            string finals = "";

            //string s2 = "Select ROW_NUMBER() OVER (ORDER BY Account2) As 'Key', AccountName2 As 'AccountName',Account2 As 'ATPCSD',sum(CAST(Quantity As Decimal(18,4))) As 'TotalQuantity',(CAST(Price As Decimal(18,4))) As 'Price',sum((CAST(Quantity As Decimal(18,2))*CAST(Price As Decimal(18,2)))) As 'TotalBrokerAmount',DatePosted,count((CAST(Quantity As Decimal(18,2))*CAST(Price As Decimal(18,2))))  As 'Deals' from DealerDG where Account2='" + account + "' and CAST(DatePosted AS date)=CAST('" + dte + "' as date) group by Account2,Price,DatePosted,AccountName2 having count(CAST(Price As Decimal(18,6))) >1";
            //string finals = "";

            if (type == "BUY")
            {
                finals = s1;
            }
            else
            {
                finals = s2;
            }


            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["SBoardConnection"].ConnectionString;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = finals;
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<Consolidateds>();
            while (reader.Read())
            {
                var accountDetails = new Consolidateds
                {
                    Key = Convert.ToInt32(reader.GetValue(0).ToString()),
                    AccountName = reader.GetValue(1).ToString(),
                    ATPCSD = reader.GetValue(2).ToString(),
                    TotalQuantity = Convert.ToDecimal(reader.GetValue(3).ToString()),
                    Price = Convert.ToDecimal(reader.GetValue(4).ToString()),
                    TotalBrokerAmount = Convert.ToDecimal(reader.GetValue(5).ToString()),
                    DatePosted = Convert.ToDateTime(reader.GetValue(7).ToString()),
                    Deals = Convert.ToInt32(reader.GetValue(8).ToString()),
                    Security = reader.GetValue(6).ToString(),


                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();

            return accDetails;
        }

        protected void ASPxButton3_Click(object sender, EventArgs e)
        {
            if (AccName.Text != "" && AccNum.Text != "" && AccQ.Text != "" && AccBR.Text != "" && DTePost.Text != "" && Price.Text != "")
            {

                HttpContext.Current.Session["AccName"] = AccName.Text;
                HttpContext.Current.Session["AccNum"] = AccNum.Text;
                HttpContext.Current.Session["AccQ"] = AccQ.Text;
                HttpContext.Current.Session["AccBR"] = AccBR.Text;
                HttpContext.Current.Session["Price"] = Price.Text;
                HttpContext.Current.Session["DTePost"] = DTePost.Text;
                HttpContext.Current.Session["Sec"] = txtSec.Text;

                if (RadioButtonList2.SelectedValue.ToString() == "BUY")
                {
                    string strscript = "<script>window.location.href='../Reporting/PagedDealReport1.aspx'</script>";

                    if (!ClientScript.IsClientScriptBlockRegistered("clientscript"))
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "clientscript", strscript);
                    }

                }
                else if (RadioButtonList2.SelectedValue.ToString() == "SELL")
                {
                    string strscript = "<script>window.location.href='../Reporting/PagedDealReport2.aspx'</script>";

                    if (!ClientScript.IsClientScriptBlockRegistered("clientscript"))
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "clientscript", strscript);
                    }
                }
            }
            else
            {
                msgbox("They are missing fields");
                return;
            }


        }

        protected void ASPxButton1_Click(object sender, EventArgs e)
        {
            if (RadioButtonList2.SelectedIndex < 0)
            {

                msgbox("Buy or Sell is required");
                return;
            }
            else if (drpname.SelectedItem.Text.ToString() == "Select Investor")
            {
                msgbox("Investor is required");
                return;
            }
            else if (ASPxDateEdit2.Text == "")
            {
                msgbox("Date posted is required");
                return;
            }
            else
            {
                loadCon();
            }

        }

        protected void ASPxButton4_Click(object sender, EventArgs e)
        {
            HttpContext.Current.Session["Trade"] = ASPxDateEdit1.Text;
            HttpContext.Current.Session["Trade2"] = ASPxDateEdit3.Text;

            if (ASPxDateEdit1.Text != "")
            {
                msgbox("The dates must be selected");
            }
            if (ASPxDateEdit1.Text != ASPxDateEdit3.Text)
            {
                msgbox("The dates must be selected and be the same for From and To");
            }
            PagedDeal2 report1 = new PagedDeal2();
            //parameters
            report1.Parameters["Type"].Value = "BUY";
            report1.Parameters["Trade"].Value = Convert.ToDateTime(ASPxDateEdit1.Text).ToString("dd-MMM-yyyy");
            report1.Parameters["Trade2"].Value = Convert.ToDateTime(ASPxDateEdit3.Text).ToString("dd-MMM-yyyy");
            report1.Parameters["Stamp"].Value = "Zim";
            foreach (XRControlStyle style in report1.StyleSheet)
            {
                style.Font = new Font(FontCollection.Families[0], 8F, GraphicsUnit.Point);
            }
            report1.xrLabel6.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);

            report1.xrLabel22.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);

            report1.xrLabel15.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);

            report1.xrLabel2.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);

            report1.xrLabel1.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);

            report1.xrLabel11.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
            report1.xrLabel23.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);


            report1.xrLabel18.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
            report1.xrLabel46.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
            report1.xrLabel21.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
            report1.xrLabel3.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
            report1.xrLabel31.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
            report1.xrLabel34.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
            report1.xrLabel35.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
            report1.xrLabel5.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);


            report1.xrLabel36.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
            report1.xrLabel26.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
            report1.xrLabel33.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
            report1.xrLabel13.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
            report1.xrLabel42.Font = new Font(FontCollection.Families[0], 8F, GraphicsUnit.Point);
            report1.xrLabel44.Font = new Font(FontCollection.Families[0], 8F, GraphicsUnit.Point);



            report1.xrLabel22.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
            report1.xrLabel17.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
            report1.xrLabel41.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);


            report1.CreateDocument();

            // Create the 2nd report and generate its document. 
            PagedDeal2 report2 = new PagedDeal2();
            //parameters

            report2.Parameters["Type"].Value = "SELL";
            report2.Parameters["Trade"].Value = Convert.ToDateTime(ASPxDateEdit1.Text).ToString("dd-MMM-yyyy");
            report2.Parameters["Trade2"].Value = Convert.ToDateTime(ASPxDateEdit3.Text).ToString("dd-MMM-yyyy");
            report2.Parameters["Stamp"].Value = "Stamp duty";
            foreach (XRControlStyle style in report2.StyleSheet)
            {
                style.Font = new Font(FontCollection.Families[0], 8F, GraphicsUnit.Point);
            }
            report2.xrLabel6.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);

            report2.xrLabel22.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);

            report2.xrLabel15.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);

            report2.xrLabel2.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);

            report2.xrLabel1.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);

            report2.xrLabel11.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
            report2.xrLabel23.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);


            report2.xrLabel18.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
            report2.xrLabel46.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
            report2.xrLabel21.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
            report2.xrLabel3.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
            report2.xrLabel31.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
            report2.xrLabel34.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
            report2.xrLabel35.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
            report2.xrLabel5.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);


            report2.xrLabel36.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
            report2.xrLabel26.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
            report2.xrLabel33.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
            report2.xrLabel13.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
            report2.xrLabel42.Font = new Font(FontCollection.Families[0], 8F, GraphicsUnit.Point);
            report2.xrLabel44.Font = new Font(FontCollection.Families[0], 8F, GraphicsUnit.Point);



            report2.xrLabel22.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
            report2.xrLabel17.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
            report2.xrLabel41.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);

            report2.CreateDocument();

            // Merge pages of two reports, page-by-page. 
            int minPageCount = Math.Min(report1.Pages.Count, report2.Pages.Count);
            for (int i = 0; i < minPageCount; i++)
            {
                report1.Pages.Insert(i * 2 + 1, report2.Pages[i]);
            }
            if (report2.Pages.Count != minPageCount)
            {
                for (int i = minPageCount; i < report2.Pages.Count; i++)
                {
                    report1.Pages.Add(report2.Pages[i]);
                }
            }

            // Reset all page numbers in the resulting document. 
            report1.PrintingSystem.ContinuousPageNumbering = true;
            string nme = DateTime.Now.ToString("dd-MMM-yyy");
            // Create a Print Tool and show the Print Preview form. 
            //report1.ExportToPdf();


            

            ExportReport(report1, "AllDayOrderNote" + nme + ".pdf", "pdf", false);



        }
        static PrivateFontCollection fontCollection;
        public static FontCollection FontCollection
        {
            get
            {
                if (fontCollection == null)
                {
                    fontCollection = new PrivateFontCollection();
                    fontCollection.AddFontFile(HttpContext.Current.Server.MapPath("~/Fonts/Gotham-Light.ttf"));
                }
                return fontCollection;
            }
        }

        public void ExportPageByPage(XtraReport report, string tradeType)
        {
           

            string subPath = GetDataFilePath(); // your code goes here

            bool exists = System.IO.Directory.Exists(subPath);

            if (!exists)
                System.IO.Directory.CreateDirectory(subPath);

          

            string nme = DateTime.Now.ToString("dd-MMM-yyy");
            string thePath = GetDataFilePath();
            var rep = report;
            rep.CreateDocument();
            XtraReport r = new XtraReport();
            for (int i = 0; i < rep.Pages.Count; i++)
            {

                r.Pages.Add(rep.Pages[i]);
               
                string path = thePath +"\\"+ tradeType + "-Report" + i.ToString() + ".pdf";
                r.ExportToPdf(path);
                r.Pages.Clear();

             

            }
        }

        private void generateZip(string startPath)
        {
            ZipFile.CreateFromDirectory(startPath, startPath+".zip");
        }

        public void ExportReport(XtraReport report, string fileName, string fileType, bool inline)
        {
            MemoryStream stream = new MemoryStream();

            Response.Clear();

            if (fileType == "xls")
                report.ExportToXls(stream);
            if (fileType == "pdf")
                report.ExportToPdf(stream);
            if (fileType == "rtf")
                report.ExportToRtf(stream);
            if (fileType == "csv")
                report.ExportToCsv(stream);

            Response.ContentType = "application/" + fileType;
            Response.AddHeader("Accept-Header", stream.Length.ToString());
            Response.AddHeader("Content-Disposition", (inline ? "Inline" : "Attachment") + "; filename=" + fileName + "." + fileType);
            Response.AddHeader("Content-Length", stream.Length.ToString());
            //Response.ContentEncoding = System.Text.Encoding.Default;
            Response.BinaryWrite(stream.ToArray());
            Response.End();
        }

        public void WriteDocumentToResponse(byte[] documentData, string format, bool isInline, string fileName)
        {
            string contentType;
            string disposition = (isInline) ? "inline" : "attachment";

            switch (format.ToLower())
            {
                case "xls":
                    contentType = "application/vnd.ms-excel";
                    break;
                case "xlsx":
                    contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    break;
                case "mht":
                    contentType = "message/rfc822";
                    break;
                case "html":
                    contentType = "text/html";
                    break;
                case "txt":
                case "csv":
                    contentType = "text/plain";
                    break;
                case "png":
                    contentType = "image/png";
                    break;
                default:
                    contentType = String.Format("application/{0}", format);
                    break;
            }

            Response.Clear();
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Disposition", String.Format("{0}; filename={1}", disposition, fileName));
            Response.BinaryWrite(documentData);
            Response.End();
        }

        protected void ASPxButton5_Click(object sender, EventArgs e)
        {
            //update date added
            if (ASPxDateEdit4.Text == "")
            {
                msgbox("You must select the date to ammend");
                return;
            }
            else if (txtAcc.Text == "")
            {
                msgbox("Account number must be selected");
                return;
            }
            else
            {
                //else updating deals
                List<DealerDG> results = (from p in db.DealerDGs
                                          where p.Deal == txtOrder.Text
                                          select p).ToList();
                foreach (DealerDG p in results)
                {
                    p.DatePosted = ASPxDateEdit4.Text;
                    p.SettlementDate = AddBusinessDays(Convert.ToDateTime(ASPxDateEdit4.Text), 3).ToString();
                    //var edc2 = escorw.SubmitDocuments(p.doc_generated,Convert.ToBase64String(p.Data),p.Name,p.ContentType);

                }
                db.SaveChanges();
                List<ATS.Tbl_MatchedDeals> results2 = (from p in db.TblDealss
                                                       where p.Deal == txtOrder.Text
                                                       select p).ToList();
                foreach (ATS.Tbl_MatchedDeals p in results2)
                {
                    p.Trade = Convert.ToDateTime(ASPxDateEdit4.Text);
                    //var edc2 = escorw.SubmitDocuments(p.doc_generated,Convert.ToBase64String(p.Data),p.Name,p.ContentType);

                }
                db.SaveChanges();

                msgbox("Date to ammended");
                loadCustomers();
            }
        }
        public DateTime AddBusinessDays(DateTime dateTime, int nDays)
        {
            var wholeWeeks = nDays / 5; //since nDays does not include weekdays every week is considered as 5 days
            var absDays = Math.Abs(nDays);
            var remaining = absDays % 5; //results in the number remaining days to add or substract excluding the whole weeks
            var direction = nDays / absDays;//results in 1 if nDays is posisive or -1 if it's negative
            while (dateTime.DayOfWeek == DayOfWeek.Saturday || dateTime.DayOfWeek == DayOfWeek.Sunday)
                dateTime = dateTime.AddDays(direction); //If we are already in a weekend, get out of it
            while (remaining-- > 0)
            {//add remaining days...
                dateTime = dateTime.AddDays(direction);
                if (dateTime.DayOfWeek == DayOfWeek.Saturday)
                    dateTime = dateTime.AddDays(direction * 2);//...skipping weekends
            }
            return dateTime.AddDays(wholeWeeks * 7); //Finally add the whole weeks as 7 days, thus skipping the weekends without checking for DayOfWeek
        }
        public DateTime cAddBusinessDays(DateTime date, int days)
        {
            if (days == 0) return date;
            int i = 0;
            while (i < days)
            {
                if (!(date.DayOfWeek == DayOfWeek.Saturday || date.DayOfWeek == DayOfWeek.Sunday)) i++;
                date = date.AddDays(1);
            }
            return date;
        }
        public void msgbox(string strMessage)
        {
            string strScript = "<script language=JavaScript>";
            strScript += "window.alert(\"" + strMessage + "\");";
            strScript += "</script>";
            System.Web.UI.WebControls.Label lbl = new System.Web.UI.WebControls.Label();
            lbl.Text = strScript;
            Page.Controls.Add(lbl);
        }

        protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void ASPxButton6_Click(object sender, EventArgs e)
        {
            txtSearch.Text = "";
            if (ASPxTextBox1.Text == "")
            {
                msgbox("Please Deal ID to search");
                return;
            }
            else
            {
                loadCustomerss();
            }

        }

        protected void btnGenerateZip_Click(object sender, EventArgs e)
        {
            HttpContext.Current.Session["Trade"] = ASPxDateEdit1.Text;
            HttpContext.Current.Session["Trade2"] = ASPxDateEdit3.Text;

            try
            {
                if (ASPxDateEdit1.Text != "" || ASPxDateEdit3.Text != "")
                {
                    PagedDeal2 report1 = new PagedDeal2();
                    //parameters
                    report1.Parameters["Type"].Value = "BUY";
                    report1.Parameters["Trade"].Value = Convert.ToDateTime(ASPxDateEdit1.Text).ToString("dd-MMM-yyyy");
                    report1.Parameters["Trade2"].Value = Convert.ToDateTime(ASPxDateEdit3.Text).ToString("dd-MMM-yyyy");
                    report1.Parameters["Stamp"].Value = "Zim";
                    foreach (XRControlStyle style in report1.StyleSheet)
                    {
                        style.Font = new Font(FontCollection.Families[0], 8F, GraphicsUnit.Point);
                    }
                    report1.xrLabel6.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);

                    report1.xrLabel22.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);

                    report1.xrLabel15.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);

                    report1.xrLabel2.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);

                    report1.xrLabel1.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);

                    report1.xrLabel11.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                    report1.xrLabel23.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);


                    report1.xrLabel18.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                    report1.xrLabel46.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                    report1.xrLabel21.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                    report1.xrLabel3.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                    report1.xrLabel31.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                    report1.xrLabel34.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                    report1.xrLabel35.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                    report1.xrLabel5.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);


                    report1.xrLabel36.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                    report1.xrLabel26.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                    report1.xrLabel33.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                    report1.xrLabel13.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                    report1.xrLabel42.Font = new Font(FontCollection.Families[0], 8F, GraphicsUnit.Point);
                    report1.xrLabel44.Font = new Font(FontCollection.Families[0], 8F, GraphicsUnit.Point);



                    report1.xrLabel22.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                    report1.xrLabel17.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                    report1.xrLabel41.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);


                    report1.CreateDocument();

                    // Create the 2nd report and generate its document. 
                    PagedDeal2 report2 = new PagedDeal2();
                    //parameters

                    report2.Parameters["Type"].Value = "SELL";
                    report2.Parameters["Trade"].Value = Convert.ToDateTime(ASPxDateEdit1.Text).ToString("dd-MMM-yyyy");
                    report2.Parameters["Trade2"].Value = Convert.ToDateTime(ASPxDateEdit3.Text).ToString("dd-MMM-yyyy");
                    report2.Parameters["Stamp"].Value = "Stamp duty";
                    foreach (XRControlStyle style in report2.StyleSheet)
                    {
                        style.Font = new Font(FontCollection.Families[0], 8F, GraphicsUnit.Point);
                    }
                    report2.xrLabel6.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);

                    report2.xrLabel22.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);

                    report2.xrLabel15.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);

                    report2.xrLabel2.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);

                    report2.xrLabel1.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);

                    report2.xrLabel11.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                    report2.xrLabel23.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);


                    report2.xrLabel18.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                    report2.xrLabel46.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                    report2.xrLabel21.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                    report2.xrLabel3.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                    report2.xrLabel31.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                    report2.xrLabel34.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                    report2.xrLabel35.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                    report2.xrLabel5.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);


                    report2.xrLabel36.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                    report2.xrLabel26.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                    report2.xrLabel33.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                    report2.xrLabel13.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                    report2.xrLabel42.Font = new Font(FontCollection.Families[0], 8F, GraphicsUnit.Point);
                    report2.xrLabel44.Font = new Font(FontCollection.Families[0], 8F, GraphicsUnit.Point);



                    report2.xrLabel22.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                    report2.xrLabel17.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                    report2.xrLabel41.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);

                    report2.CreateDocument();

                    // Merge pages of two reports, page-by-page. 
                    int minPageCount = Math.Min(report1.Pages.Count, report2.Pages.Count);
                    for (int i = 0; i < minPageCount; i++)
                    {
                        report1.Pages.Insert(i * 2 + 1, report2.Pages[i]);
                    }
                    if (report2.Pages.Count != minPageCount)
                    {
                        for (int i = minPageCount; i < report2.Pages.Count; i++)
                        {
                            report1.Pages.Add(report2.Pages[i]);
                        }
                    }

                    // Reset all page numbers in the resulting document. 
                    report1.PrintingSystem.ContinuousPageNumbering = true;
                    string nme = DateTime.Now.ToString("dd-MMM-yyy");
                    // Create a Print Tool and show the Print Preview form. 
                    //report1.ExportToPdf();

                    if (System.IO.File.Exists(GetDataFilePath() + ".zip"))
                    {
                        System.IO.File.Delete(GetDataFilePath() + ".zip");
                    }
                    System.IO.DirectoryInfo di = new DirectoryInfo(GetDataFilePath());
                    foreach (FileInfo file in di.GetFiles())
                    {
                        file.Delete();
                    }

                    ExportPageByPage(report1, "Buy");
                    ExportPageByPage(report2, "Sell");
                    generateZip(GetDataFilePath());



                    WebClient req = new WebClient();
                    System.Web.HttpResponse response = HttpContext.Current.Response;
                    string filePath = GetDataFilePath() + ".zip";
                    response.Clear();
                    response.ClearContent();
                    response.ClearHeaders();
                    response.Buffer = true;
                    response.AddHeader("Content-Disposition", "attachment;filename=DownloadFolder.zip");
                    byte[] data = req.DownloadData(filePath);
                    response.BinaryWrite(data);
                    response.End();
                }
                else
                {
                    msgbox("The dates must be selected");
                }
            }
            catch (Exception)
            {

            }
           


         
        }


    }
}