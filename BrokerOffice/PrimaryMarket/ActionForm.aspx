﻿<%@ Page Language="C#" MaintainScrollPositionOnPostback="true" MasterPageFile="~/Site1.Master"   AutoEventWireup="true" CodeBehind="ActionForm.aspx.cs" Inherits="BrokerOffice.PrimaryMarket.ActionForm" %>
<%@ Register Assembly="DevExpress.Web.v18.2, Version=18.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>


<asp:content id="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   
    <div>


  <div class="panel panel-default" style=" padding: 10px; margin: 10px">
    <div class="panel-heading" style="background-color:#428bca">
        <i style="color:white">Portfolio Management</i>
    </div>
      <div class="form-group">
          <div class="col md-4">
              <asp:Label ID="Label6" CssClass="control-label col-md-2" runat="server" Text="Search Customer"></asp:Label>
              <asp:TextBox ID="txtSearch" CssClass="form-control" PlaceHolder="Search name......." runat="server"></asp:TextBox>
              <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-primary" Text="Search" OnClick="btnSearch_Click" />
              <dx:ASPxButton ID="btnMarket" runat="server" Theme="Moderno" Text="Market Calculator" OnClick="btnMarket_Click"></dx:ASPxButton>

          </div>
                             <asp:GridView ID="GridView1" CssClass="table table-striped" runat="server"   EmptyDataText="No records has been added."
    AutoGenerateSelectButton="true" onselectedindexchanged="GridView1_SelectedIndexChanged"
    AllowPaging="True" PageSize="5" OnPageIndexChanging="grdData_PageIndexChanging"
    >   <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />

</asp:GridView>

      <br />
        <div id="Tabs" role="tabpanel">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li class="active"><a href="#personal" aria-controls="personal" role="tab" data-toggle="tab">
                    Search Customer </a></li>
                <li ><a href="#Action" aria-controls="Action" role="tab" data-toggle="tab">
                  Portfolio Details</a></li>
          
            </ul>
            <!-- Tab panes -->
            <div class="tab-content" style="padding-top: 20px">
                  <div role="tabpanel" class="tab-pane active" id="personal">
                               <table class="table table-striped">
                                      <asp:Label ID="txtFkey" runat="server" Visible="False" Text=""></asp:Label>
<asp:Label ID="txtID" Visible="false" runat="server" Text="Label"></asp:Label>
                                    <tr>
                                <td>
                                    <asp:Label ID="Label3" CssClass="control-label col-md-4" runat="server" Text="Customer Number"></asp:Label>
                                    <asp:TextBox ID="txtCustomerNumber" CssClass="form-control" runat="server" ReadOnly="True" width="155px"></asp:TextBox>
                                 </td>
                                       <td>
                                           <asp:Label ID="Label4" CssClass="control-label col-md-4" runat="server" Text="Customer Names"></asp:Label>
                                           <asp:TextBox ID="txtCustomerNames" CssClass="form-control" runat="server" ReadOnly="True" width="150px"></asp:TextBox>
                                       </td>

                                       <td>
                                           <asp:Label ID="Label5" CssClass="control-label col-md-4" runat="server" Text="Custmer Holdings"></asp:Label>
                                           <asp:TextBox ID="txtHoldings" CssClass="form-control" runat="server" ReadOnly="True" width="150px"></asp:TextBox>
                                       </td>
                                   </tr>

                                   
                             </table>
                              <dx style="float:right"><dx:ASPxButton ID="dxAction" runat="server" CssClass="btn btn-primary" Theme="Moderno" Text="Next" OnClick="dxAction_Click" ></dx:ASPxButton></dx>
                                     
                     </div>
                <div role="tabpanel" class="tab-pane" id="Action">
                  <table class="table table-striped">
                        <tr>
                          <td>
<asp:Label runat="server" Text="Method"></asp:Label>
                          </td>
                          <td>
<asp:RadioButtonList id="Method" runat="server" RepeatDirection="Horizontal" AutoPostBack="True" OnSelectedIndexChanged="Method_SelectedIndexChanged1">
      <asp:ListItem Text="Contact" Value="Contact"></asp:ListItem>
      <asp:ListItem Text="Advice" Value="Advice"></asp:ListItem>
    </asp:RadioButtonList>
                          </td>


                      </tr>
                      <tr>
                          <td>
                   <asp:Label runat="server" CssClass="control-label col-md-4" Text="Date" ID="lbldate"></asp:Label>
                              </td>
                          <td>
<dx:ASPxDateEdit ID="txtDate" CssClass="form-control" runat="server" width="150px"></dx:ASPxDateEdit>
                          </td>
                          <td id="lblhour">
<asp:Label runat="server" CssClass="control-label col-md-2" Text="Hour" ID="lblhour"></asp:Label>
                              </td>
                          <td>
<asp:TextBox ID="txthr" CssClass="form-control" runat="server"></asp:TextBox>
                              </td>
                          <td>
<asp:Label runat="server" CssClass="control-label col-md-2" Text="Minutes" ID="lblmin"></asp:Label>
                              </td>
                          <td>
<asp:TextBox id="txtMin" CssClass="form-control" runat="server"></asp:TextBox>
                              </td>
                          <td>
<asp:Label runat="server" CssClass="control-label col-md-2" ID="lblsett"></asp:Label>
                              </td>
                          <td>
<asp:RadioButtonList id="txtSetting" runat="server" RepeatDirection="Horizontal">
      <asp:ListItem Text="AM" Value="AM"></asp:ListItem>
                              <asp:ListItem Text="PM" Value="PM"></asp:ListItem>

</asp:RadioButtonList>
                          </td>
                      </tr>
                      
                      <tr>
                          <td>
                              <asp:Label ID="lblcontact" runat="server" CssClass="control-label col-md-4" Text="Contact Notes"></asp:Label>
                          </td>
                          <td>
                              <asp:TextBox ID="txtContact" CssClass="form-control"  TextMode="MultiLine" runat="server" width="150px"></asp:TextBox>
                          </td>
                          <td></td>
                          <td></td>
                           <td></td>
                          <td></td>
                           <td></td>
                          <td></td>
                      </tr>
                                <tr>
                          <td>
                              <asp:Label ID="lblAdv" runat="server" CssClass="control-label col-md-4" Text="Advice"></asp:Label>
                          </td>
                          <td>
                              <asp:TextBox ID="txtAdvice" CssClass="form-control" TextMode="MultiLine" runat="server" width="150px"></asp:TextBox>
                          </td>
                                    <td></td>
                          <td></td>
                           <td></td>
                          <td></td>
                           <td></td>
                          <td></td>
                      </tr>


                      <tr>
                          <td>
<asp:Button runat="server" CssClass="btn btn-primary" ID="Submit" Text="Submit" OnClick="Submit_Click" ></asp:Button>
                          </td>
              
                               <td>
                                           <asp:HiddenField ID="TabName" runat="server" />
                                      <asp:Label ID="Label44" runat="server" Visible="false" Text=""></asp:Label>
                                     
                                       </td>
                          <td></td>
                          <td></td>
                           <td></td>
                          <td></td>
                           <td></td>
                          <td></td>
                      </tr>
                  </table>
               <dx style="float:right"><dx:ASPxButton ID="dxPersonal" runat="server" CssClass="btn btn-primary" Theme="Moderno" Text="Previous" OnClick="dxPersonal_Click" style="height: 29px" ></dx:ASPxButton></dx>
              
                </div>
       <div class="form-group">
          <div class="col md-4"
         
                       <asp:Label ID="impD" CssClass="control-label col-md-2" runat="server" ForeColor="White" Text="Search Client"></asp:Label>
           </div>
      
                             <asp:GridView ID="GridView2" CssClass="table table-striped" runat="server"   EmptyDataText="No records has been added."
   
    AllowPaging="True" PageSize="5" OnPageIndexChanging="grdData_PageIndexChanging2"
    >   <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />

</asp:GridView>

              <dx:ASPxPopupControl ID="ASPxPopupControl1" runat="server" Theme="Moderno" FooterText="Market Calculator" HeaderText="Market Calculator" Width="1258px" Height="700px" Modal="True" ScrollBars="Auto" AllowDragging="True" AllowResize="True" AutoUpdatePosition="True" CloseAction="CloseButton" >

             <ContentCollection>
                <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                                          <h2>Market Calculator</h2>
                                     <div class="form-horizontal">
                                              <div class="form-group">
                                             
                                                  <dx:ASPxRadioButtonList ID="ASPxRadioButtonList1" runat="server" ValueType="System.String" AutoPostBack="True" EnableTheming="True" OnSelectedIndexChanged="ASPxRadioButtonList1_SelectedIndexChanged" RepeatDirection="Horizontal" SelectedIndex="0" Theme="Moderno">
                                                      <Items>
                                                          <dx:ListEditItem  Text="Buy" Value="Buy" />
                                                          <dx:ListEditItem Text="Sell" Value="Sell" />
                                                          <dx:ListEditItem Text="Both" Value="Both" />
                                                      </Items>
                                                  </dx:ASPxRadioButtonList>
                                         </div>
                                         <div class="form-group">
                                             <div class="col-md-2">
                                      <dx:ASPxLabel ID="ASPxLabel1" Theme="Moderno" runat="server" Text="Quantity"></dx:ASPxLabel>
                                             </div>
                                             <div class="col-md-4">
                                                 <dx:ASPxTextBox ID="qty" Theme="Moderno" runat="server" Width="150px"></dx:ASPxTextBox>
                                             </div>
                                         </div>
                                          <div class="form-group">
                                             <div class="col-md-2">
                                                 <dx:ASPxLabel ID="ASPxLabel2" Theme="Moderno" runat="server" Text="Price"></dx:ASPxLabel>

                                             </div>
                                             <div class="col-md-4">
                                                 <dx:ASPxTextBox ID="max" Theme="Moderno"  runat="server" Width="150px"></dx:ASPxTextBox>
                                             </div>
                                         </div>
                                          <div class="form-group">
                                             <div class="col-md-2">
                                                 <dx:ASPxLabel ID="ASPxLabel4" Theme="Moderno" runat="server" Text="Result"></dx:ASPxLabel>

                                             </div>
                                             <div class="col-md-4">
                                                 <dx:ASPxTextBox ID="txtAnswr" Theme="Moderno" ReadOnly="true"  runat="server" Width="150px"></dx:ASPxTextBox>
                                             </div>
                                         </div>
                                            <div class="form-group">
                                             <div class="col-md-2">
                                                 <dx:ASPxLabel ID="ASPxLabel3" Theme="Moderno" runat="server" Text="Total"></dx:ASPxLabel>

                                             </div>
                                             <div class="col-md-4">
                                                 <dx:ASPxTextBox ID="result" Theme="Moderno" ReadOnly="true"  runat="server" Width="150px"></dx:ASPxTextBox>
                                             </div>
                                         </div>
                                         <div class="form-group">
                                           <div class="col-md-8">
                                               <dx:ASPxButton ID="btnCalc" Theme="Moderno" Width="50px" runat="server" Text="Calculate" OnClick="btnCalc_Click"></dx:ASPxButton>
                                               <dx:ASPxButton ID="btnClear" Theme="Moderno" Width="50px" runat="server" Text="Clear" OnClick="btnClear_Click"></dx:ASPxButton>
                                             </div>
                                         </div>
                                     </div>
                    <center>
 <dx:ASPxGridView ID="ASPxGridView1" runat="server" Theme="Glass" Width="675px">
     <SettingsPager Mode="ShowAllRecords">
     </SettingsPager>
                        </dx:ASPxGridView>
                         <table class="table table-responsive">
                            <tr>
                                <td style="width:100px">
  <dx:ASPxLabel id="totalL" runat="server" CssClass="control-label md-2" Theme="Moderno" Text="" Font-Bold="True"></dx:ASPxLabel>
                                 </td>
                                  <td style="width:254px">
       <dx:ASPxMemo id="memo" runat="server" Theme="Moderno" HorizontalAlign="Center" Width="253px" Height="57px" Font-Bold="True" ReadOnly="True"></dx:ASPxMemo>
                            
                                </td>

                            </tr>
                        </table>
                    </center>
                    </dx:PopupControlContentControl>
            </ContentCollection>
     
            
      </dx:ASPxPopupControl>
           </div>
      </div>
        </div>
    </div>
      </div>
  
    </div>
      <script type="text/javascript">
$(function () {
    var tabName = $("[id*=TabName]").val() != "" ? $("[id*=TabName]").val() : "personal";
    $('#Tabs a[href="#' + tabName + '"]').tab('show');
    $("#Tabs a").click(function () {
        $("[id*=TabName]").val($(this).attr("href").replace("#", ""));
    });
});
</script>

</asp:content>