﻿using BrokerOffice.DAO;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebMatrix.WebData;

namespace BrokerOffice.PrimaryMarket
{
    public partial class Batch : System.Web.UI.Page
    {
        public string id = "";
        public string id2 = "";
        private SBoardContext db = new SBoardContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            id = Request.QueryString["id"];
            id2 = Request.QueryString["idd"];
            if (this.IsPostBack)
            {

                TabName.Value = Request.Form[TabName.UniqueID];


            }
            else
            {

               
                drpname.DataSource = GetDataSource();
                drpname.DataBind();
                try
                {
                    Label1.Text = Request.QueryString["id"].ToString();
                }
                catch (Exception)
                {

                  
                }
                if (id2 == " " || id2 == null)
                {

                }
                else
                {
                  
                    getDetails(id2);
                }
            }
        }

        public void getDetails(string idd)
        {
            int my = Convert.ToInt32(idd);
            var d = db.BatchApplications.ToList().Where(a=>a.BatchApplicationID==my);

            foreach (var q in d)
            {
                qty.Text = q.numberofshares.ToString();
                TextBox1.Text = q.ClientAmount.ToString();
                TextBox2.Text = ((q.ClientAmount) / (q.numberofshares)).ToString();
                drpname.Text = q.ClientName;
                drpname.Value =q.Clientnumber;
                Label1.Text = q.BatchNo.ToString();
            }
            Submit.Text = "Update";
        }
        private DataTable GetDataSource()
        {
            //DataTable defined
            var dtSource = new DataTable();
            dtSource.Columns.Add("id", typeof(string));
            dtSource.Columns.Add("FullNames", typeof(string));
            string x = "";

            try
            {
                x = Request.QueryString["id"];
            }
            catch (Exception)
            {

                x = "";
            }

            string name = "";
            string nn= Request.QueryString["idd"];
            if (nn!="")
            {
                var d = db.BatchApplications.ToList().Where(a => a.BatchApplicationID.ToString() == nn);

                foreach (var q in d)
                {

                    //dtSource.Rows.Add(q.Clientnumber,q.ClientName);

                }

                dtSource.Rows.Add("2", "Select Investor");
            }
            else
            {
            dtSource.Rows.Add("1", "Select Investor");
            }
                
                var dd = from c in db.Account_Creations
                         where c.StatuSActive == true
                         select c;
                foreach (var p in dd)
                {
                    if (p.OtherNames == null)
                    {
                        name = p.Surname_CompanyName + " ," + p.CDSC_Number;
                    }
                    else if (p.OtherNames == "")
                    {
                        name = p.Surname_CompanyName + " ," + p.CDSC_Number;
                    }
                    else
                    {
                        name = p.OtherNames + " " + p.Surname_CompanyName + " ," + p.CDSC_Number;
                    }

                    dtSource.Rows.Add(p.CDSC_Number, name);
                }

            //Fill rows



            return dtSource;
        }

        protected void Submit_Click(object sender, EventArgs e)
        {
            //check parameters


            double n;
            bool isNumericT = double.TryParse(qty.Text, out n);

            if (isNumericT == false)
            {
                msgbox("Number of shares is required");
                return;
            }

        

            if (drpname.SelectedItem.Text=="Select Investor")
            {
                msgbox("Investor name is required");
                return;
            }
          Label1.Text= Request.QueryString["id"];
            if (Submit.Text == "Submit")
            {
                var d = db.BatchApplications.ToList().Where(a=>a.BatchNo.ToString()==Label1.Text).Count();
                var f = db.batch_mast.ToList().Where(a => a.batch_no== Label1.Text);
                int minn = 0;
                foreach (var k in f)
                {
                    minn = Convert.ToInt32(k.apps);
                }
                if (d < minn)
                {


                    BrokerOffice.Models.BatchApplication my = new BrokerOffice.Models.BatchApplication();
                    my.BatchNo = Convert.ToInt32(Label1.Text);
                    my.ClientName = drpname.SelectedItem.Text;
                    my.Clientnumber = drpname.SelectedItem.Value.ToString();
                    my.numberofshares = Convert.ToDecimal(qty.Text);
                    my.ClientAmount = Convert.ToDecimal(TextBox1.Text);
                    db.BatchApplications.Add(my);
                    db.SaveChanges(WebSecurity.CurrentUserName.ToString());
                    //update batch counters
                    //var p = db.batch_mast.ToList().Where(a => a.batch_no == Label1.Text);

                    //int myD = 0;

                    //foreach (var k in p)
                    //{
                    //    myD = k.ID;
                    //}
                    //var counterC = db.BatchApplications.ToList().Where(a => a.BatchNo.ToString() == Label1.Text).Count();
                    //var counterCC = db.BatchApplications.ToList().Where(a => a.BatchNo.ToString() == Label1.Text).Sum(a => a.ClientAmount);
                    //var counterShares= db.BatchApplications.ToList().Where(a => a.BatchNo.ToString() == Label1.Text).Sum(a => a.numberofshares);
                    //var myc = db.batch_mast.Find(myD);
                    //myc.apps = counterC.ToString();
                    //myc.app_cash = counterCC;
                    //myc.app_shares = counterShares;
                    //db.batch_mast.AddOrUpdate(myc);
                    //db.SaveChanges(WebSecurity.CurrentUserName.ToString());
                    System.Web.HttpContext.Current.Session["NOT"] = "You have successfully updated the Investor batch application";
                    Response.Redirect("~/batch_mast/Index");
                }else
                {
                    msgbox("The maximum number of applications for the batch has been reached");
                    return;
                }
                } else if (Submit.Text=="Update")
                {
                    int myc = Convert.ToInt32(Request.QueryString["idd"]);
                    BrokerOffice.Models.BatchApplication my = db.BatchApplications.Find(myc);
                    my.BatchNo = my.BatchNo;
                    my.ClientName = drpname.SelectedItem.Text;
                    my.Clientnumber = drpname.SelectedItem.Value.ToString();
                    my.numberofshares = Convert.ToDecimal(qty.Text);
                    my.ClientAmount = Convert.ToDecimal(TextBox1.Text);
                    db.BatchApplications.AddOrUpdate(my);
                    db.SaveChanges(WebSecurity.CurrentUserName.ToString());
                    System.Web.HttpContext.Current.Session["NOT"] = "You have successfully updated the Investor batch application number "+my.BatchNo;
                    Response.Redirect("~/batch_mast/Index");
                }
            }
        
        
        protected void Clear()
        {
            Response.Redirect("~/Batch/Index");
        }
        public void msgbox(string strMessage)
        {
            string strScript = "<script language=JavaScript>";
            strScript += "window.alert(\"" + strMessage + "\");";
            strScript += "</script>";
            System.Web.UI.WebControls.Label lbl = new System.Web.UI.WebControls.Label();
            lbl.Text = strScript;
            Page.Controls.Add(lbl);
        }

     

        protected void qty_TextChanged(object sender, EventArgs e)
        {
            //calculate amount

            decimal n;
            bool isNumericT = decimal.TryParse(qty.Text, out n);

            if (isNumericT == false)
            {
                msgbox("Number of shares must be a numeric");
                return;
            }
            decimal p = Convert.ToDecimal(qty.Text);
            //
            string my =Request.QueryString["id"];
            if (Submit.Text=="Update")
            {my= Request.QueryString["idd"];
                var cz = db.BatchApplications.ToList().Where(a=>a.BatchApplicationID.ToString()==my);
                foreach (var k in cz )
                {
                    my = k.BatchNo.ToString();
                }
            }
            var pp = db.batch_mast.ToList().Where(a=>a.batch_no==my);
            string com = "";
            foreach (var z in pp)
            {
                com = z.Company;
            }

            //GetDataItem price
            var client = new RestClient("http://localhost/BrokerService");
            var request = new RestRequest("IPPCompany/{f}", Method.GET);
            request.AddUrlSegment("f", com);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
    
            var Jsonobject = JsonConvert.DeserializeObject<string>(validate);
            validate = Jsonobject.ToString();
            TextBox1.Text = (Convert.ToDecimal(validate) * Convert.ToDecimal(qty.Text)).ToString();
            TextBox2.Text = validate;
        }
    }
}