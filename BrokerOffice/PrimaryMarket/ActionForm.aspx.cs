﻿using BrokerOffice.DAO;
using BrokerOffice.Models;
using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Data.Entity.SqlServer;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebMatrix.WebData;

namespace BrokerOffice.PrimaryMarket
{
    public partial class ActionForm : System.Web.UI.Page
    {
        public string id = "";
        private SBoardContext db = new SBoardContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            id = Request.QueryString["id"];
            if (this.IsPostBack)
            {
                TabName.Value = Request.Form[TabName.UniqueID];
                if (result.Text!=null){
                   loadgrid();
                }
               
            }
            else
            {
                loadCustomers();


                if (id == " " || id == null)
                {

                }
                else
                {
                    getDetails(id);
                }
            }


        }
        protected void getDetails(string x)
        {

            string per = "";
            if (x != "")
            {
                Submit.Text = "Update";
                txtFkey.Text = x;

                var acc = db.ActionForm.ToList().Where(a => a.ActionFormID == Convert.ToInt32(x));

                foreach (var my in acc)
                {
                    txtID.Text = my.ClientPortfoliosID.ToString();
                    txtCustomerNumber.Text = my.ClientNumber;
                    txtCustomerNames.Text = my.ClientNames;
                    txtHoldings.Text = my.clientholdings.ToString();
                    Method.SelectedValue = my.contacttype;
                    txtDate.Value = my.Date;
                    txthr.Text = my.hour;
                    txtMin.Text = my.minutes;
                    txtSetting.Text = my.setting;
                    txtContact.Text = my.contactnotes;
                    txtAdvice.Text = my.adviceclient;

                }
            }
            if (Method.SelectedIndex == 0)
            {
                lbldate.Visible = true;
                lblhour.Visible = true;
                lblmin.Visible = true;
                lblsett.Visible = true;
                lblcontact.Visible = true;
                txtDate.Visible = true;
                txthr.Visible = true;
                txtMin.Visible = true;
                txtSetting.Visible = true;
                txtContact.Visible = true;
                lblAdv.Visible = false;
                txtAdvice.Visible = false;
                //no.Visible = false;

            }
            else if (Method.SelectedIndex == 1)
            {
                lbldate.Visible = false;
                lblhour.Visible = false;
                lblmin.Visible = false;
                lblsett.Visible = false;
                lblcontact.Visible = false;
                txtDate.Visible = false;
                txthr.Visible = false;
                txtMin.Visible = false;
                txtSetting.Visible = false;
                //   no.Visible = false;
                txtContact.Visible = false;
                lblAdv.Visible = true;
                txtAdvice.Visible = true;
                //   no2.Visible = true;
            }
            return;
        }
        public void msgbox(string strMessage)
        {
            string strScript = "<script language=JavaScript>";
            strScript += "window.alert(\"" + strMessage + "\");";
            strScript += "</script>";
            System.Web.UI.WebControls.Label lbl = new System.Web.UI.WebControls.Label();
            lbl.Text = strScript;
            Page.Controls.Add(lbl);
        }
        protected void loadCustomers()
        {
            //var action = from s in db.ClientPortfolioss
            //             join v in db.Account_Creations on s.ClientNumber equals v.CDSC_Number
            //             let CDSNUMBER = v.CDSC_Number let Surname = v.Surname_CompanyName let Firstname = v.OtherNames
            //             let Holdings = s.Holdings let stockISIN = s.Stock let Issuer = s.Stockf
            //             group s by new {s.Stock,s.Stockf,v.ID_} into g

            //             select new { v.ID_=, CDSNUMBER=g.Select(s=>s.ClientNumber), Surname, Firstname, Holdings, stockISIN, Issuer };

            //  select Account_Creation.ID_,Account_Creation.CDSC_Number, Account_Creation.Surname_CompanyName,Account_Creation.OtherNames,IsNull(sum(ClientPortfolios.Holdings),0),ClientPortfolios.Stock , ClientPortfolios.Stockf from ClientPortfolios inner join Account_Creation On ClientPortfolios.ClientNumber=Account_Creation.CDSC_Number group by ClientPortfolios.Stock , ClientPortfolios.Stockf,Account_Creation.ID_,Account_Creation.CDSC_Number, Account_Creation.Surname_CompanyName,Account_Creation.OtherNames
            string constr = ConfigurationManager.ConnectionStrings["SBoardConnection"].ToString(); // connection string

            using (SqlConnection con = new SqlConnection(constr))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("select Account_Creation.ID_ As 'SYSID',Account_Creation.CDSC_Number as 'Customer Number', Account_Creation.Surname_CompanyName As 'Surname',Account_Creation.OtherNames As 'FirstName',IsNull(sum(ClientPortfolios.Holdings),0) As 'Holdings',ClientPortfolios.Stock As 'StockISIN', ClientPortfolios.Stockf As 'Issuer' from ClientPortfolios inner join Account_Creation On ClientPortfolios.ClientNumber=Account_Creation.CDSC_Number group by ClientPortfolios.Stock , ClientPortfolios.Stockf,Account_Creation.ID_,Account_Creation.CDSC_Number, Account_Creation.Surname_CompanyName,Account_Creation.OtherNames", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds); // fill dataset
                GridView1.DataSource = ds;
                GridView1.DataBind();
                con.Close();

            }
            //GridView1.DataSource = action.ToList();
            //GridView1.DataBind();
        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //     public int ClientPortfoliosID { get; set; }
            //public string ClientNumber { get; set; }
            //public string ClientNames { get; set; }
            //public double clientholdings { get; set; }
            String pathname = GridView1.SelectedRow.Cells[1].Text;
            txtID.Text = pathname;
            txtCustomerNumber.Text = GridView1.SelectedRow.Cells[2].Text;
            txtCustomerNames.Text = GridView1.SelectedRow.Cells[3].Text + " " + GridView1.SelectedRow.Cells[4].Text;
            txtHoldings.Text = GridView1.SelectedRow.Cells[5].Text;
            impD.Text = GridView1.SelectedRow.Cells[6].Text;
            loadkillergrid();


        }
        protected void loadkillergrid()
        {

            DateTime MyNewDateValue = DateTime.Now.AddDays(-8);
            decimal outf = Convert.ToDecimal(txtHoldings.Text);
            var actions = from s in db.StockPrices
                          let Day = SqlFunctions.DateName("day", s.daterecorded).Trim() + "/" + SqlFunctions.StringConvert((double)s.daterecorded.Value.Month).TrimStart() + "/" + SqlFunctions.DateName("year", s.daterecorded)

                          let CustomerHoldings = txtHoldings.Text
                          let Value = outf * s.LastTradePrice let Price = s.LastTradePrice
                          where s.daterecorded <= DateTime.Now && s.daterecorded >= MyNewDateValue
                          select new { Day, CustomerHoldings, Price, Value };

            GridView2.DataSource = actions.ToList();
            GridView2.DataBind();
            //  msgbox(MyNewDateValue.ToString());
        }
        protected void grdData_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            loadCustomers();
        }
        protected void grdData_PageIndexChanging2(object sender, GridViewPageEventArgs e)
        {
            GridView2.PageIndex = e.NewPageIndex;
            loadkillergrid();
        }

        protected void Submit_Click(object sender, EventArgs e)
        {
            if (Method.SelectedIndex == 0)
            {
                if (txtContact.Text == "")
                {
                    msgbox("Contact notes must be  entered");
                    return;
                }
                else if (txtDate.Text == "")
                {
                    msgbox("Date must be selected");
                    return;
                }
                else if (txthr.Text == "")
                {
                    msgbox("Hour must be selected");
                    return;
                }
                else if (txtMin.Text == "")
                {
                    msgbox("Minutes must be selected");
                    return;
                }
                else if (txtSetting.SelectedIndex < 0)
                {
                    msgbox("Setting must be selected");
                    return;
                }
            }
            else if (Method.SelectedIndex == 1)

            {
                if (txtAdvice.Text == "")
                {
                    msgbox("Advice must be  entered");
                    return;
                }
            }

            if (Submit.Text == "Submit")
            {
                ActionFormcs my = new ActionFormcs();
                my.ClientPortfoliosID = Convert.ToInt32(txtID.Text);
                my.ClientNumber = txtCustomerNumber.Text;
                my.ClientNames = txtCustomerNames.Text;
                my.clientholdings = Convert.ToDouble(txtHoldings.Text);
                my.contacttype = Method.SelectedValue.ToString();
                if (txtDate.Text == "" || txtDate.Text == null)
                {
                    my.Date = DateTime.Now;
                } else
                {
                    my.Date = Convert.ToDateTime(txtDate.Value);
                }

                my.hour = txthr.Text;
                my.minutes = txtMin.Text;
                my.setting = txtSetting.Text;
                my.contactnotes = txtContact.Text;
                my.adviceclient = txtAdvice.Text;

                db.ActionForm.Add(my);

                //Update table  
                db.SaveChanges(WebSecurity.CurrentUserName.ToString());
                System.Web.HttpContext.Current.Session["NOT"] = "You have successfully added the action";
                Response.Redirect("~/Actions/Index");
            }
            else if (Submit.Text == "Update")
            {
                var my = db.ActionForm.Find(Convert.ToInt32(txtFkey.Text));
                my.ClientPortfoliosID = Convert.ToInt32(txtID.Text);
                my.ClientNumber = txtCustomerNumber.Text;
                my.ClientNames = txtCustomerNames.Text;
                my.clientholdings = Convert.ToDouble(txtHoldings.Text);
                my.contacttype = Method.SelectedValue.ToString();
                if (txtDate.Text == "" || txtDate.Text == null)
                {
                    my.Date = DateTime.Now;
                }
                else
                {
                    my.Date = Convert.ToDateTime(txtDate.Value);
                }
                my.hour = txthr.Text;
                my.minutes = txtMin.Text;
                my.setting = txtSetting.Text;
                my.contactnotes = txtContact.Text;
                my.adviceclient = txtAdvice.Text;
                //Update Row in Table  
                db.ActionForm.AddOrUpdate(my);
                db.SaveChanges(WebSecurity.CurrentUserName.ToString());
                System.Web.HttpContext.Current.Session["NOT"] = "You have successfully updated the action";
                Response.Redirect("~/Actions/Index");
            }

        }
        protected void Clear()
        {
            Response.Redirect("~/Actions/Index");
        }

        protected void Method_SelectedIndexChanged1(object sender, EventArgs e)
        {
            if (Method.SelectedIndex == 0)
            {
                lbldate.Visible = true;
                lblhour.Visible = true;
                lblmin.Visible = true;
                lblsett.Visible = true;
                lblcontact.Visible = true;
                txtDate.Visible = true;
                txthr.Visible = true;
                txtMin.Visible = true;
                txtSetting.Visible = true;
                txtContact.Visible = true;
                lblAdv.Visible = false;
                txtAdvice.Visible = false;
                //no.Visible = false;

            }
            else if (Method.SelectedIndex == 1)
            {
                lbldate.Visible = false;
                lblhour.Visible = false;
                lblmin.Visible = false;
                lblsett.Visible = false;
                lblcontact.Visible = false;
                txtDate.Visible = false;
                txthr.Visible = false;
                txtMin.Visible = false;
                txtSetting.Visible = false;
                //   no.Visible = false;
                txtContact.Visible = false;
                lblAdv.Visible = true;
                txtAdvice.Visible = true;
                //   no2.Visible = true;
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (txtSearch.Text != "")
            {
                //var action = from s in db.ClientPortfolioss
                //             join v in db.Account_Creations on s.ClientNumber equals v.CDSC_Number
                //             let CDSNUMBER = v.CDSC_Number
                //             let Surname = v.Surname_CompanyName
                //             let Firstname = v.OtherNames
                //             let Holdings = s.Holdings
                //             let stockISIN = s.Stock
                //             let Issuer = s.Stockf
                //             where (v.Surname_CompanyName + v.OtherNames + "" + v.CDSC_Number).Contains(txtSearch.Text)
                //             select new { v.ID_, CDSNUMBER, Surname, Firstname, Holdings, stockISIN, Issuer };

                //GridView1.DataSource = action.ToList();
                //GridView1.DataBind();

                string constr = ConfigurationManager.ConnectionStrings["SBoardConnection"].ToString(); // connection string

                using (SqlConnection con = new SqlConnection(constr))
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("select Account_Creation.ID_ As 'SYSID',Account_Creation.CDSC_Number as 'Customer Number', Account_Creation.Surname_CompanyName As 'Surname',Account_Creation.OtherNames As 'FirstName',IsNull(sum(ClientPortfolios.Holdings),0) As 'Holdings',ClientPortfolios.Stock As 'StockISIN', ClientPortfolios.Stockf As 'Issuer' from ClientPortfolios inner join Account_Creation On ClientPortfolios.ClientNumber=Account_Creation.CDSC_Number  where Account_Creation.Surname_CompanyName+' '+Account_Creation.OtherNames+' '+Account_Creation.CDSC_Number LIKE '%'"+ txtSearch.Text + "'' group by ClientPortfolios.Stock , ClientPortfolios.Stockf,Account_Creation.ID_,Account_Creation.CDSC_Number, Account_Creation.Surname_CompanyName,Account_Creation.OtherNames ", con);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    da.Fill(ds); // fill dataset
                    GridView1.DataSource = ds;
                    GridView1.DataBind();
                    con.Close();

                }
            }
            else
            {
                loadCustomers();
            }

        }

        protected void btnMarket_Click(object sender, EventArgs e)
        {
            ASPxPopupControl1.PopupHorizontalAlign = PopupHorizontalAlign.WindowCenter;
            ASPxPopupControl1.PopupVerticalAlign = PopupVerticalAlign.WindowCenter;
            ASPxPopupControl1.AllowDragging = true;
            ASPxPopupControl1.ShowCloseButton = true;
            ASPxPopupControl1.ShowOnPageLoad = true;
            ASPxPopupControl1.Modal = false;
            ASPxPopupControl1.ShowCloseButton = true;
            ASPxPopupControl1.ShowRefreshButton = true;
            ASPxPopupControl1.ShowShadow = true;
        }

        protected void btnCalc_Click(object sender, EventArgs e)
        {
            if (max.Text == "")
            {
                max.Text = "0.0";
                return;
            }
            else if(qty.Text == "")
            {
               qty.Text = "0.0";
                return;
            }

            decimal s;
            bool isNumericS = decimal.TryParse(max.Text, out s);

            if (isNumericS == false)
            {
                max.Text = "0";
                return;
            }

            decimal f;
            bool isNumeriF = decimal.TryParse(qty.Text, out f);

            if (isNumeriF == false)
            {
                qty.Text = "0";
                return;
            }


            decimal maxprice = Convert.ToDecimal(max.Text);
            decimal quantity = Convert.ToDecimal(qty.Text);
         decimal prod = maxprice * quantity;
            decimal overallT = 0;
            decimal cumT = 0;
           
            var tr = db.TradingChargess.ToList();
            foreach (var c in tr)
            {
                if (c.ChargedAs == "Flat")
                {
                    cumT +=Convert.ToDecimal(Convert.ToDecimal(c.chargevalue));
                }
                else if (c.ChargedAs == "Percentage")
                {
                    cumT += ((Convert.ToDecimal(c.chargevalue) / 100) * (maxprice * quantity));
                }
            }

            overallT = cumT + (maxprice * quantity);
          //result.Text= overallT.ToString()
            //loading aspx gidview
            loadgrid();

        }

        public void loadgrid()
        {
        

            if (max.Text == "")
            {
                max.Text = "0.0";

            }
            else if (qty.Text == "")
            {
                qty.Text = "0.0";

            }

            decimal s;
            bool isNumericS = decimal.TryParse(max.Text, out s);

            if (isNumericS == false)
            {
                max.Text = "0";
                return;
            }

            decimal f;
            bool isNumeriF = decimal.TryParse(qty.Text, out f);

            if (isNumeriF == false)
            {
                qty.Text = "0";
                return;
            }
            decimal maxprice = Convert.ToDecimal(max.Text);
            decimal quantity = Convert.ToDecimal(qty.Text);
            decimal prod = maxprice * quantity;

            txtAnswr.Text = prod.ToString();
            string constr = ConfigurationManager.ConnectionStrings["SBoardConnection"].ToString(); // connection string

            string sql1 = "select ChargeName AS 'Type of Charge',(CASE when tradeside='Both' Then CAST(CAST((chargevalue/100)*CAST('" + prod.ToString() + "' as decimal(10,4) ) as Decimal(10,4)) as nvarchar)  when  tradeside='Buy' Then CAST(CAST((chargevalue/100)*CAST('" + prod.ToString() + "' as Decimal(10,4) ) as Decimal(10,4))  as nvarchar)  Else '' End) As 'Buying($)',(CASE when tradeside='Both' Then CAST(CAST((chargevalue/100)*CAST('" + prod.ToString() + "' as Decimal(10,4) ) as Decimal(10,4)) as nvarchar)  when  tradeside='Sell' Then CAST(CAST((chargevalue/100)*CAST('" + prod.ToString() + "' as Decimal(10,4) ) as Decimal(10,4)) as nvarchar)  Else '' End) As 'Selling($)' from tradingcharges union all ";
            string sql2 = "(select distinct  'Total transaction costs' AS 'Type of Charge', CAST((select Isnull(sum(CAST((chargevalue/100)*CAST('" + prod.ToString() + "' as Decimal(10,4) ) as Decimal(10,4))), 0) from tradingcharges where tradeside = 'Buy' or tradeside = 'Both') as nvarchar) As 'Buy',CAST((select Isnull(sum(CAST((chargevalue/100)*CAST('" + prod.ToString() + "' as Decimal(10,4) ) as Decimal(10,4))), 0) from tradingcharges where tradeside = 'Sell' or tradeside = 'Both') as nvarchar) As 'Sell' from tradingcharges group by tradeside)";
            string full = sql1 + sql2;
            using (SqlConnection con = new SqlConnection(constr))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand(full, con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds); // fill dataset
                ASPxGridView1.DataSource = ds;
                ASPxGridView1.DataBind();
                con.Close();

            }
            decimal buy = 0;
            decimal sell = 0;
           
            string constr2 = ConfigurationManager.ConnectionStrings["SBoardConnection"].ToString(); // connection string
            bool check;
            string col1Value = "";
            using (SqlConnection con2 = new SqlConnection(constr2))
            {
                con2.Open();
                SqlCommand cmd =
                    new SqlCommand("select distinct  'Total transaction costs' AS 'Type of Charge', CAST((select Isnull(sum(CAST((chargevalue/100)*CAST('" + prod.ToString() + "' as decimal) as Decimal(10,4))), 0) from tradingcharges where tradeside = 'Buy' or tradeside = 'Both') as nvarchar) As 'Buy',CAST((select Isnull(sum(CAST((chargevalue/100)*CAST('" + prod.ToString() + "' as decimal) as Decimal(10,4))), 0) from tradingcharges where tradeside = 'Sell' or tradeside = 'Both') as nvarchar) As 'Sell' from tradingcharges group by tradeside", con2);
                SqlDataReader dr = cmd.ExecuteReader();
                check = dr.HasRows;
                while (dr.Read())
                {

                    buy = Convert.ToDecimal(dr["Buy"].ToString());
                    sell = Convert.ToDecimal(dr["Sell"].ToString());
                }
                con2.Close();

               
                HttpContext.Current.Session["Buy"] = buy;
                HttpContext.Current.Session["Sell"] = sell;

                if (ASPxRadioButtonList1.SelectedIndex == 0)
                {
                    result.Text = ((Convert.ToDecimal(HttpContext.Current.Session["Buy"].ToString()) + (maxprice * quantity))).ToString();
                    ASPxGridView1.Columns[2].Visible = false;
                    ASPxGridView1.Columns[1].Visible = true;
                    totalL.Text = "Total costs for buying";
                    memo.Text = (buy).ToString();
                }
                else if (ASPxRadioButtonList1.SelectedIndex == 1)
                {
                    result.Text = ((Convert.ToDecimal(HttpContext.Current.Session["Sell"].ToString()) + (maxprice * quantity))).ToString();
                    ASPxGridView1.Columns[1].Visible = false;
                    ASPxGridView1.Columns[2].Visible = true;
                    totalL.Text = "Total costs for selling";
                    memo.Text = (sell).ToString();
                }
                else if (ASPxRadioButtonList1.SelectedIndex == 2)
                {
                    result.Text = ((Convert.ToDecimal(HttpContext.Current.Session["Buy"].ToString()) + (Convert.ToDecimal(HttpContext.Current.Session["Sell"].ToString())) + (maxprice * quantity))).ToString();
                    ASPxGridView1.Columns[1].Visible = true;
                    ASPxGridView1.Columns[2].Visible = true;
                    totalL.Text = "Total costs for buying and selling";
                    memo.Text = (buy + sell).ToString();
                }
            }
        }
        protected void btnClear_Click(object sender, EventArgs e)
        {
           max.Text="";
           qty.Text="";
            result.Text = "";
        }

        protected void ASPxRadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                decimal maxprice = Convert.ToDecimal(max.Text);
                decimal quantity = Convert.ToDecimal(qty.Text);
                decimal prod = maxprice * quantity;

                txtAnswr.Text = prod.ToString();
                if (ASPxRadioButtonList1.SelectedIndex == 0)
                {
                    result.Text = ((Convert.ToDecimal(HttpContext.Current.Session["Buy"].ToString()) + (maxprice * quantity))).ToString();
                    ASPxGridView1.Columns[2].Visible = false;
                    ASPxGridView1.Columns[1].Visible = true;
                }
                else if (ASPxRadioButtonList1.SelectedIndex == 1)
                {
                    result.Text = ((Convert.ToDecimal(HttpContext.Current.Session["Sell"].ToString()) + (maxprice * quantity))).ToString();
                    ASPxGridView1.Columns[1].Visible = false;
                    ASPxGridView1.Columns[2].Visible = true;
                }
                else if (ASPxRadioButtonList1.SelectedIndex == 2)
                {
                    result.Text = ((Convert.ToDecimal(HttpContext.Current.Session["Buy"].ToString()) + (Convert.ToDecimal(HttpContext.Current.Session["Sell"].ToString())) + (maxprice * quantity))).ToString();
                    ASPxGridView1.Columns[1].Visible = true;
                    ASPxGridView1.Columns[2].Visible = true;
                }
            }
            catch (Exception)
            {

                result.Text = "";
            }

        }

    
        protected void dxAction_Click(object sender, EventArgs e)
        {
            Label44.Text = "Action";
            TabName.Value = Label44.Text;
            return;
        }

        protected void dxPersonal_Click(object sender, EventArgs e)
        {
            Label44.Text = "personal";
            TabName.Value = Label44.Text;
            return;
        }
    }
}