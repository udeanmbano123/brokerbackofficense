﻿using BrokerOffice.DAO;
using BrokerOffice.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebMatrix.WebData;

namespace BrokerOffice.PrimaryMarket
{
    public partial class PortfolioCreation : System.Web.UI.Page
    {
        public string id = "";
        private SBoardContext db = new SBoardContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            id = Request.QueryString["id"];
            if (this.IsPostBack)
            {
               

            }
            else
            {
                loadCustomers();
                loadStock();

                if (id == " " || id == null)
                {

                }
                else
                {
                    GridView1.Visible = false;
                    btnSearch.Visible = false;
                    txtSearch.Visible = false;
                    ASPxLabel4.Visible = false;
                   getDetails(id);
                }
            }

        }

        protected void getDetails(string x)
        {

            string per = "";
            if (x != "")
            {
                btnSubmit.Text = "Update";
                txtFkey.Text = x;

                var acc = db.ClientPortfolioss.ToList().Where(a => a.ClientPortfoliosID == Convert.ToInt32(x));

                foreach (var my in acc)
                {
                    txtClient.Text= my.ClientNumber;
                    txtHoldings.Text = my.Holdings.ToString();
                    drpStock.Items.Insert(0, new ListItem(my.Stockf.ToString(), my.Stock));

                }
            }
            
            return;
        }
        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //     public int ClientPortfoliosID { get; set; }
            //public string ClientNumber { get; set; }
            //public string ClientNames { get; set; }
            //public double clientholdings { get; set; }
           // String pathname = GridView1.SelectedRow.Cells[1].Text;
           
            txtClient.Text= GridView1.SelectedRow.Cells[1].Text;

        }

        protected void grdData_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            loadCustomers();
        }
        protected void loadStock()
        {
           drpStock.Items.Clear();
            drpStock.DataSource = null;
            var ds = from c in db.StockSecuritiess
                     select new

                     {

                         Bankcode = c.StockISIN,

                         Bankname = c.StockISIN,

                     };
            drpStock.DataSource = ds.ToList();
            drpStock.DataTextField = "Bankname";
            // text field name of table dispalyed in dropdown
            drpStock.DataValueField = "Bankcode"; // to retrive specific  textfield name 
                                                  //assigning datasource to the dropdownlist
            drpStock.DataBind(); //binding dropdownlist
                                 // allocate.Items.Insert(0, "Select");
            drpStock.Items.Insert(0, new ListItem("Select Stock", "0"));
        }
        protected void loadCustomers()
        {
            var action = from v in db.Account_Creations
                         let CDSNUMBER = v.CDSC_Number
                         let Surname = v.Surname_CompanyName
                         let Firstname = v.OtherNames
                      

                         select new { CDSNUMBER,Surname,Firstname };

      
            GridView1.DataSource = action.ToList();
            GridView1.DataBind();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            var action = from v in db.Account_Creations
                         let CDSNUMBER = v.CDSC_Number
                         let Surname = v.Surname_CompanyName
                         let Firstname = v.OtherNames
                         where (v.CDSC_Number + "" + v.Surname_CompanyName + "" + v.OtherNames).Contains(txtSearch.Text)

                         select new { CDSNUMBER, Surname, Firstname };


            GridView1.DataSource = action.ToList();
            GridView1.DataBind();
        }
        public void msgbox(string strMessage)
        {
            string strScript = "<script language=JavaScript>";
            strScript += "window.alert(\"" + strMessage + "\");";
            strScript += "</script>";
            System.Web.UI.WebControls.Label lbl = new System.Web.UI.WebControls.Label();
            lbl.Text = strScript;
            Page.Controls.Add(lbl);
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            bool holdistrue = false;
            int holdistrueid = 0;
            decimal currh = 0;

            decimal n;
            bool isNumericT = decimal.TryParse(txtHoldings.Text, out n);

            if (isNumericT == false)
            {
                msgbox("Holdings must be numeric");
                return;
            }
            if (txtClient.Text=="")
            {
                msgbox("Client is required to be selected");
                return;
            }

            if (drpStock.SelectedItem.Text=="Select Stock")
            {
                msgbox("Please select a stock");
                return;
            }

            if (btnSubmit.Text == "Submit")
            {
            string fellow = drpStock.SelectedValue.ToString();
            string fellow2 = txtClient.Text;
            var br = db.StockSecuritiess.ToList().Where(a => a.StockISIN == fellow);
            var br2 = db.Account_Creations.ToList().Where(a => a.CDSC_Number == fellow2);
            string rm = "";
            string code = "";
            string rm2 = "";
            string code2 = "";
            foreach (var q in br)
            {
                rm = q.StockISIN;
                code = q.Issuer;
            }
      
                ClientPortfolios my = new ClientPortfolios();
                my.Stock = rm;
                my.Stockf = code;

                foreach (var c in br2)
                {
                    rm2 = "Names: " + c.OtherNames + "," + c.Surname_CompanyName + " Customer Number" + c.CDSC_Number;
                    code2 = c.CDSC_Number;
                }
               my.Clientf = rm2;
               my.ClientNumber = code2;
                my.Holdings=Convert.ToDecimal(txtHoldings.Text);
                var checkn = db.ClientPortfolioss.ToList().Where(a => a.ClientNumber == my.ClientNumber && a.Stock == my.Stock && a.Stockf == my.Stockf);
                foreach (var c in checkn)
                {
                    holdistrue = true;
                    holdistrueid = c.ClientPortfoliosID;
                    currh = c.Holdings;
                }

                if (holdistrue == true && holdistrueid != null)
                {
                    //update the customers holdings
                    var my2 = db.ClientPortfolioss.Find(Convert.ToInt32(holdistrueid));

                    my2.Holdings = currh + my.Holdings;
                    //Update Row in Table  
                    db.ClientPortfolioss.AddOrUpdate(my2);
                    db.SaveChanges(WebSecurity.CurrentUserName.ToString());
                }
                else
                {
                 db.ClientPortfolioss.Add(my);
                 db.SaveChangesAsync(WebSecurity.CurrentUserName.ToString());
                }
                System.Web.HttpContext.Current.Session["NOT"] = "You have successfully added the portfolio";
                Response.Redirect("~/ClientPortfolios/Index");
                
            }
      else if (btnSubmit.Text == "Update")
            {
                var my = db.ClientPortfolioss.Find(Convert.ToInt32(txtFkey.Text));
                string fellow =drpStock.SelectedValue.ToString();
                string fellow2 = txtClient.Text;
                var br = db.StockSecuritiess.ToList().Where(a => a.StockISIN == fellow);
                var br2 = db.Account_Creations.ToList().Where(a => a.CDSC_Number == fellow2);
                string rm = "";
                string code = "";
                string rm2 = "";
                string code2 = "";
                foreach (var q in br)
                {
                    rm = q.StockISIN;
                    code = q.Issuer;
                }
                my.Stock = rm;
                my.Stockf = code;

                foreach (var c in br2)
                {
                    rm2 = "Names: " + c.OtherNames + "," + c.Surname_CompanyName + " Customer Number" + c.CDSC_Number;
                    code2 = c.CDSC_Number;
                }
                my.Holdings = Convert.ToDecimal(txtHoldings.Text);
                my.Clientf = rm2;
                my.ClientNumber = code2;
                db.ClientPortfolioss.AddOrUpdate(my);
                db.SaveChanges(WebSecurity.CurrentUserName.ToString());

                System.Web.HttpContext.Current.Session["NOT"] = "You have successfully updated the portfolio";
                Response.Redirect("~/ClientPortfolios/Index");
            }
        }

        protected void Clear()
        {
            Response.Redirect("~/ClientPortfolios/Index");
        }

    }
}