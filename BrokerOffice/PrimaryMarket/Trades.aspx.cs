﻿
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebMatrix.WebData;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Data;
using BrokerOffice.DAO;
using BrokerOffice.Models;
using Json.NET;
using System.Collections.ObjectModel;
using System.Net;
using System.Net.Sockets;
using RestSharp;

namespace BrokerOffice.PrimaryMarket
{
    public partial class Trades : System.Web.UI.Page
    {
        public string id = "";
        private SBoardContext db = new SBoardContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                id = Request.QueryString["id"];
                if (this.IsPostBack)
                {
                    // TabName.Value = Request.Form[TabName.UniqueID];
                    trades();
                }
                else
                {
                    trades2();
                    // loadCustomers();
                    trades();



                    if (id == " " || id == null)
                    {

                    }
                    else
                    {
                        // getDetails(id);
                    }
                }

            }
            catch (Exception)
            {
                Response.Redirect("~/User/Index", false);        //write redirect
                Context.ApplicationInstance.CompleteRequest(); // end response


            }

        }

        public bool IsAddressAvailable(string address)
        {
            try
            {
                System.Net.WebClient client = new WebClient();
                client.DownloadData(address);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public async void trades2()
        {

            
                var dd = db.Users.ToList().Where(a => a.Email == WebSecurity.CurrentUserName);

                string vx = "";

                foreach (var p in dd)
                {
                    vx = p.BrokerCode;
                }
                string n = "";
                string responJsonText = "";
                var user = db.Account_Creations.ToList();
                string updatedjson = "";
                int d = 0;
                int x = 0;
                string carryjson = "";
                JArray v2 = null;

                Tbl_MatchedDeals c1 = null;
                var jsonObjects = new List<string>();
                var jsonObjectsArray = "";
                

            var client = new RestClient("http://localhost/BrokerService");
            var request = new RestRequest("Matcheds", Method.GET);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            List<ATS.Tbl_MatchedDeals> dataList = JsonConvert.DeserializeObject<List<ATS.Tbl_MatchedDeals>>(validate);
           //   GridView1.DataSource = DerializeDataTable(responJsonText);
            var dbsel = from s in dataList
                            join v in db.Account_Creations on s.Buyercdsno equals v.CDSC_Number
                            let SID = s.ID
                            let Deal = s.Deal
                            let BuyCompany = s.BuyCompany
                            let SellCompany = s.SellCompany
                            let Buyer = s.Buyercdsno
                            let Seller = s.Sellercdsno
                            let Quantity = s.Quantity
                            let Trade = s.Trade
                            let DealPrice = s.DealPrice
                            let DealFlag = s.DealFlag
                            let Instrument = s.Instrument
                            let affirmation = s.Affirmation
                            let buybroker = s.buybroker
                            let sellbroker = s.sellbroker
                            //where s.Buyercdsno==bss || s.Sellercdsno==bss
                            select new { SID, Deal, BuyCompany, SellCompany, Buyer, Seller, Quantity, Trade, DealPrice, DealFlag, Instrument, affirmation, buybroker, sellbroker };
                var dbsel2 = from s in dataList
                             join v in db.Account_Creations on s.Sellercdsno equals v.CDSC_Number
                             let SID = s.ID
                             let Deal = s.Deal
                             let BuyCompany = s.BuyCompany
                             let SellCompany = s.SellCompany
                             let Buyer = s.Buyercdsno
                             let Seller = s.Sellercdsno
                             let Quantity = s.Quantity
                             let Trade = s.Trade
                             let DealPrice = s.DealPrice
                             let DealFlag = s.DealFlag
                             let Instrument = s.Instrument
                             let affirmation = s.Affirmation
                             let buybroker = s.buybroker
                             let sellbroker = s.sellbroker
                             //where s.Buyercdsno==bss || s.Sellercdsno==bss
                             select new { SID, Deal, BuyCompany, SellCompany, Buyer, Seller, Quantity, Trade, DealPrice, DealFlag, Instrument, affirmation, buybroker, sellbroker };


            int my44= dbsel.ToList().Count();
            int my45= dbsel2.ToList().Count();
            int sel = 0;
            var bag= dbsel.ToList();
            var bag2 = dbsel2.ToList();
            foreach (var c in bag)
                {
                var ss = 0;

                try
                {
                    ss = db.TblDealss.ToList().Where(a => a.RefID == c.SID).Count();
                }
                catch (Exception)
                {

                    ss = 0;
                }


                    if (ss == 0)
                    {
                    try
                    {
                        sel = db.Account_Creations.ToList().Where(a => a.CDSC_Number == c.Seller).Count();


                    }
                    catch (Exception)
                    {

                        sel = 0;
                    }
                    if (sel == 0)
                    {
                        ATS.Tbl_MatchedDeals my = new ATS.Tbl_MatchedDeals();
                        my.Deal = c.Deal;
                        my.BuyCompany = c.BuyCompany;
                        my.SellCompany = c.SellCompany;
                        my.Buyercdsno = c.Buyer;
                        my.Sellercdsno = c.Seller;
                        my.Quantity = c.Quantity;
                        my.Trade = c.Trade;
                        my.DealPrice = c.DealPrice;
                        my.DealFlag = c.DealFlag;
                        my.Instrument = c.Instrument;
                        my.Affirmation = c.affirmation;
                        my.buybroker = c.buybroker;
                        my.sellbroker = c.sellbroker;
                        my.RefID = Convert.ToInt32(c.SID);
                        my.BrokerCode = vx;
                        db.TblDealss.Add(my);
                        db.SaveChanges(WebSecurity.CurrentUserName);
                    }
                        updateBuyTrade(c.Buyer, c.Seller, c.DealPrice.ToString(), Convert.ToInt32(c.SID), Convert.ToInt32(c.Quantity));

                    }

                }


                //update all sell

                foreach (var c in bag2)
                {
                var ss = 0;
                try
                {
                    ss = db.TblDealss.ToList().Where(a => a.RefID == c.SID).Count();

                }
                catch (Exception)
                {

                    ss = 0;
                }

                    if (ss == 0)
                    {
                        ATS.Tbl_MatchedDeals my = new ATS.Tbl_MatchedDeals();
                        my.Deal = c.Deal;
                        my.BuyCompany = c.BuyCompany;
                        my.SellCompany = c.SellCompany;
                        my.Buyercdsno = c.Buyer;
                        my.Sellercdsno = c.Seller;
                        my.Quantity = c.Quantity;
                        my.Trade = c.Trade;
                        my.DealPrice = c.DealPrice;
                        my.DealFlag = c.DealFlag;
                        my.Instrument = c.Instrument;
                        my.Affirmation = c.affirmation;
                        my.buybroker = c.buybroker;
                        my.sellbroker = c.sellbroker;
                        my.RefID = Convert.ToInt32(c.SID);
                        my.BrokerCode = vx;
                        db.TblDealss.Add(my);
                        db.SaveChanges(WebSecurity.CurrentUserName);
                        updateSellTrade(c.Buyer, c.Seller, c.DealPrice.ToString(), Convert.ToInt32(c.SID), Convert.ToInt32(c.Quantity));
                    }
                }

          

            
            }
        public async void trades()
        {
            var dd = db.Users.ToList().Where(a => a.Email == WebSecurity.CurrentUserName);

            string vx = "";

            foreach (var p in dd)
            {
                vx = p.BrokerCode;
            }

            
                string n = "";
                string responJsonText = "";
                var user = db.Account_Creations.ToList();
                string updatedjson = "";
                int d = 0;
                int x = 0;
                string carryjson = "";
                JArray v2 = null;

                Tbl_MatchedDeals c1 = null;
                var jsonObjects = new List<string>();
                var jsonObjectsArray = "";
                Uri requestUri = null;
               
                    requestUri = new Uri("http://192.168.3.245/BrokerService/api/Deals/Deals");//replace your Url 
                                                                                                   //  dynamic dynamicJson = new ExpandoObject();
                
           

                //string json = "";
                // json = Newtonsoft.Json.JsonConvert.SerializeObject(dynamicJson);
                var objClint = new System.Net.Http.HttpClient();
                HttpResponseMessage respon = await objClint.PostAsync(requestUri, new StringContent("", System.Text.Encoding.UTF8, "application/json"));

                responJsonText = await respon.Content.ReadAsStringAsync();

                List<Tbl_MatchedDeals> dataList = JsonConvert.DeserializeObject<List<Tbl_MatchedDeals>>(responJsonText);


                //   GridView1.DataSource = DerializeDataTable(responJsonText);
                var dbsel = from s in dataList
                            join v in db.Account_Creations on s.Buyer equals v.CDSC_Number

                            let Deal = s.Deal
                            let BuyCompany = s.BuyCompany
                            let SellCompany = s.SellCompany
                            let Buyer = s.Buyer
                            let Seller = s.Seller
                            let Quantity = s.Quantity
                            let Trade = s.Trade
                            let DealPrice = s.DealPrice
                            select new { Deal, BuyCompany, SellCompany, Buyer, Seller, Quantity, Trade, DealPrice };
                var dbsel2 = from s in dataList
                             join v in db.Account_Creations on s.Seller equals v.CDSC_Number

                             let Deal = s.Deal
                             let BuyCompany = s.BuyCompany
                             let SellCompany = s.SellCompany
                             let Buyer = s.Buyer
                             let Seller = s.Seller
                             let Quantity = s.Quantity
                             let Trade = s.Trade
                             let DealPrice = s.DealPrice
                             select new { Deal, BuyCompany, SellCompany, Buyer, Seller, Quantity, Trade, DealPrice };

                var allresults = dbsel.Concat(dbsel2).ToList().Distinct();
                GridView1.DataSource = allresults;
                GridView1.DataBind();

            
        }
        public void postTradeCharges(string type, string b, string p, int d, string deal)
        {
            BrokerOffice.Models.Trans my2 = new BrokerOffice.Models.Trans();
            string maxid = deal;

            decimal maxprice = Convert.ToDecimal(p);
            decimal quantity = Convert.ToDecimal(d);
            int num = 0;
            //Cash Deposits
            var acc = db.Accounts_Masters.ToList().Where(a => a.AccountName.Contains("Accounts Receivable"));


            foreach (var c in acc)
            {
                num = c.AccountNumber;
            }
            ////trading charges
            var tr = db.TradingChargess.ToList();
            BrokerOffice.Models.Trans my3 = new BrokerOffice.Models.Trans();
            //Debit
            decimal total = 0;
            foreach (var q in tr)
            {
                if (type == "BUY")
                {
                    if (q.ChargedAs == "Flat")
                    {
                        my3.Account = q.chargeaccountcode.ToString();
                        my3.Category = "Order Posting";
                        my3.Credit = Convert.ToDecimal(q.chargevalue);
                        my3.Debit = 0;
                        my3.Narration = q.chartaccount;
                        my3.Reference_Number = maxid.ToString();
                        my3.TrxnDate = DateTime.Now;
                        my3.Post = DateTime.Now;
                        my3.Type = q.chartaccount;
                        my3.PostedBy = WebSecurity.CurrentUserName;
                        db.Transs.Add(my3);

                        //Update table  
                        db.SaveChanges(WebSecurity.CurrentUserName.ToString());
                        ////debit account number
                        //my3.Account = b;
                        //my3.Category = q.chartaccount;
                        //my3.Credit = 0;
                        //my3.Debit = Convert.ToDecimal(q.chargevalue);
                        //my3.Narration = q.chartaccount;
                        //my3.Reference_Number = maxid.ToString();
                        //my3.TrxnDate = DateTime.Now;
                        //my3.Post = DateTime.Now;
                        //my3.Type = q.chartaccount;
                        //my3.PostedBy = WebSecurity.CurrentUserName;
                        //db.Transs.Add(my3);

                        ////Update table  
                        //db.SaveChanges(WebSecurity.CurrentUserName.ToString());

                        //debit
                        my3.Account = num.ToString();
                        my3.Category = "Accounts Receivable";
                        my3.Credit = 0;
                        my3.Debit = Convert.ToDecimal(q.chargevalue);
                        my3.Narration = q.chartaccount;
                        my3.Reference_Number = maxid.ToString();
                        my3.TrxnDate = DateTime.Now;
                        my3.Post = DateTime.Now;
                        my3.Type = "Accounts Receivable";
                        my3.PostedBy = WebSecurity.CurrentUserName;
                        db.Transs.Add(my3);

                        //Update table  
                        db.SaveChanges(WebSecurity.CurrentUserName.ToString());
                        total += Convert.ToDecimal(q.chargevalue);
                    }
                    else if (q.ChargedAs == "Percentage")
                    {
                        my3.Account = q.chargeaccountcode.ToString();
                        my3.Category = "Order Posting";
                        my3.Credit = ((Convert.ToDecimal(q.chargevalue) / 100) * (maxprice * quantity));
                        my3.Debit = 0;
                        my3.Narration = q.chartaccount;
                        my3.Reference_Number = maxid.ToString();
                        my3.TrxnDate = DateTime.Now;
                        my3.Post = DateTime.Now;
                        my3.Type = q.chartaccount;
                        my3.PostedBy = WebSecurity.CurrentUserName;
                        db.Transs.Add(my3);

                        //Update table  
                        db.SaveChanges(WebSecurity.CurrentUserName.ToString());
                        //debit account number
                        //my3.Account = b;
                        //my3.Category = q.chartaccount;
                        //my3.Credit = 0;
                        //my3.Debit = ((Convert.ToDecimal(q.chargevalue) / 100) * (maxprice * quantity));
                        //my3.Narration = q.chartaccount;
                        //my3.Reference_Number = maxid.ToString();
                        //my3.TrxnDate = DateTime.Now;
                        //my3.Post = DateTime.Now;
                        //my3.Type = q.chartaccount;
                        //my3.PostedBy = WebSecurity.CurrentUserName;
                        //db.Transs.Add(my3);

                        ////Update table  
                        //db.SaveChanges(WebSecurity.CurrentUserName.ToString());

                        //debit cashdeposits
                        my3.Account = num.ToString();
                        my3.Category = "Accounts Receivable";
                        my3.Credit = 0;
                        my3.Debit = ((Convert.ToDecimal(q.chargevalue) / 100) * (maxprice * quantity));
                        my3.Narration = q.chartaccount;
                        my3.Reference_Number = maxid.ToString();
                        my3.TrxnDate = DateTime.Now;
                        my3.Post = DateTime.Now;
                        my3.Type = "Accounts Receivable";
                        my3.PostedBy = WebSecurity.CurrentUserName;
                        db.Transs.Add(my3);

                        //Update table  
                        db.SaveChanges(WebSecurity.CurrentUserName.ToString());
                        total += Convert.ToDecimal(q.chargevalue);
                    }
                }
                else
                {
                    if (q.ChargedAs == "Flat")
                    {
                        my3.Account = q.chargeaccountcode.ToString();
                        my3.Category = "Order Posting";
                        my3.Debit = Convert.ToDecimal(q.chargevalue);
                        my3.Credit = 0;
                        my3.Narration = q.chartaccount;
                        my3.Reference_Number = maxid.ToString();
                        my3.TrxnDate = DateTime.Now;
                        my3.Post = DateTime.Now;
                        my3.Type = q.chartaccount;
                        my3.PostedBy = WebSecurity.CurrentUserName;
                        db.Transs.Add(my3);

                        //Update table  
                        db.SaveChanges(WebSecurity.CurrentUserName.ToString());
                        //debit account number
                        //my3.Account = b;
                        //my3.Category = q.chartaccount;
                        //my3.Debit = 0;
                        //my3.Credit = Convert.ToDecimal(q.chargevalue);
                        //my3.Narration = q.chartaccount;
                        //my3.Reference_Number = maxid.ToString();
                        //my3.TrxnDate = DateTime.Now;
                        //my3.Post = DateTime.Now;
                        //my3.Type = q.chartaccount;
                        //my3.PostedBy = WebSecurity.CurrentUserName;
                        //db.Transs.Add(my3);

                        ////Update table  
                        //db.SaveChanges(WebSecurity.CurrentUserName.ToString());

                        //debit
                        my3.Account = num.ToString();
                        my3.Category = "Accounts Receivable";
                        my3.Debit = 0;
                        my3.Credit = Convert.ToDecimal(q.chargevalue);
                        my3.Narration = q.chartaccount;
                        my3.Reference_Number = maxid.ToString();
                        my3.TrxnDate = DateTime.Now;
                        my3.Post = DateTime.Now;
                        my3.Type = "Accounts Receivable";
                        my3.PostedBy = WebSecurity.CurrentUserName;
                        db.Transs.Add(my3);

                        //Update table  
                        db.SaveChanges(WebSecurity.CurrentUserName.ToString());
                        total += Convert.ToDecimal(q.chargevalue);
                    }
                    else if (q.ChargedAs == "Percentage")
                    {
                        my3.Account = q.chargeaccountcode.ToString();
                        my3.Category = "Order Posting";
                        my3.Debit = ((Convert.ToDecimal(q.chargevalue) / 100) * (maxprice * quantity));
                        my3.Credit = 0;
                        my3.Narration = q.chartaccount;
                        my3.Reference_Number = maxid.ToString();
                        my3.TrxnDate = DateTime.Now;
                        my3.Post = DateTime.Now;
                        my3.Type = q.chartaccount;
                        my3.PostedBy = WebSecurity.CurrentUserName;
                        db.Transs.Add(my3);

                        //Update table  
                        db.SaveChanges(WebSecurity.CurrentUserName.ToString());
                        //debit account number
                        //my3.Account = b;
                        //my3.Category = q.chartaccount;
                        //my3.Debit = 0;
                        //my3.Credit = ((Convert.ToDecimal(q.chargevalue) / 100) * (maxprice * quantity));
                        //my3.Narration = q.chartaccount;
                        //my3.Reference_Number = maxid.ToString();
                        //my3.TrxnDate = DateTime.Now;
                        //my3.Post = DateTime.Now;
                        //my3.Type = q.chartaccount;
                        //my3.PostedBy = WebSecurity.CurrentUserName;
                        //db.Transs.Add(my3);

                        ////Update table  
                        //db.SaveChanges(WebSecurity.CurrentUserName.ToString());

                        //debit cashdeposits
                        my3.Account = num.ToString();
                        my3.Category = "Accounts Receivable";
                        my3.Debit = 0;
                        my3.Credit = ((Convert.ToDecimal(q.chargevalue) / 100) * (maxprice * quantity));
                        my3.Narration = q.chartaccount;
                        my3.Reference_Number = maxid.ToString();
                        my3.TrxnDate = DateTime.Now;
                        my3.Post = DateTime.Now;
                        my3.Type = "Accounts Receivable";
                        my3.PostedBy = WebSecurity.CurrentUserName;
                        db.Transs.Add(my3);

                        //Update table  
                        db.SaveChanges(WebSecurity.CurrentUserName.ToString());
                        total += Convert.ToDecimal(q.chargevalue);
                    }
                }


            }

            if (type == "BUY")
            {
                //debit account number
                my2.Account = b;
                my2.Category = "Deal";
                my2.Credit = 0;
                my2.Debit = (maxprice * quantity) + total;
                my2.Narration = "Deal";
                my2.Reference_Number = maxid.ToString();
                my2.TrxnDate = DateTime.Now;
                my2.Post = DateTime.Now;
                my2.Type = "Purchase Deal";
                my2.PostedBy = WebSecurity.CurrentUserName;
                db.Transs.Add(my2);

                //Update table  
                db.SaveChanges(WebSecurity.CurrentUserName.ToString());
            }
            else if (type == "SELL")
            {
                //debit account number
                my2.Account = b;
                my2.Category = "Deal";
                my2.Credit = (maxprice * quantity) - total;
                my2.Debit = 0;
                my2.Narration = "Deal";
                my2.Reference_Number = maxid.ToString();
                my2.TrxnDate = DateTime.Now;
                my2.Post = DateTime.Now;
                my2.Type = "Sell Deal";
                my2.PostedBy = WebSecurity.CurrentUserName;
                db.Transs.Add(my2);

                //Update table  
                db.SaveChanges(WebSecurity.CurrentUserName.ToString());
            }








            //insert into chart of accounts

            //Liability account
            var liab = db.Accounts_Masters.ToList().Where(a => a.AccountName.Contains("Liability"));
            int lacc = 0;
            foreach (var c in liab)
            {
                lacc = c.AccountNumber;
            }

            //Debit
            my2.Account = lacc.ToString();
            my2.Category = "Liability";
            my2.Credit = (maxprice * quantity) + total;
            my2.Debit = 0;
            my2.Narration = "Deal";
            my2.Reference_Number = maxid.ToString();
            my2.TrxnDate = DateTime.Now;
            my2.Post = DateTime.Now;
            my2.Type = "Order Posting";
            my2.PostedBy = WebSecurity.CurrentUserName;
            db.Transs.Add(my2);

            //Update table  
            db.SaveChanges(WebSecurity.CurrentUserName.ToString());



            my2.Account = num.ToString();
            my2.Category = "Deal";
            my2.Credit = 0;
            my2.Debit = (maxprice * quantity) + total;
            my2.Narration = "Deal";
            my2.Reference_Number = maxid.ToString();
            my2.TrxnDate = DateTime.Now;
            my2.Post = DateTime.Now;
            my2.Type = "Order Posting";
            my2.PostedBy = WebSecurity.CurrentUserName;
            db.Transs.Add(my2);

            db.SaveChanges(WebSecurity.CurrentUserName.ToString());

        }


        public void updateSellTrade(string b, string s, string p, int d, int q)
        {
           


                
                postTradeCharges("SELL", s, p, q, d.ToString());
            
        }

        public void updateBuyTrade(string b, string s, string p, int d, int q)
        {
            
                postTradeCharges("BUY", b, p, q, d.ToString());
            
        }

        public DataTable DerializeDataTable(string data)
        {
            string json = data; //"data" should contain your JSON 
            var table = JsonConvert.DeserializeObject<DataTable>(json);
            return table;
        }
        public string checkNull(string s)
        {
            if (s == null)
            {
                return "0";
            }

            return s;
            
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
//            if (txtSearch.Text != "")
//            {
//var action = from s in db.ClientPortfolioss
//                         join v in db.Account_Creations on s.ClientNumber equals v.CDSC_Number
//                         let CDSNUMBER = v.CDSC_Number
//                         let Surname = v.Surname_CompanyName
//                         let Firstname = v.OtherNames
//                         let Holdings = s.Holdings
//                         let stockISIN = s.Stock
//                         let Issuer = s.Stockf
//             where (v.Surname_CompanyName + v.OtherNames + "" + v.CDSC_Number).Contains(txtSearch.Text)
//             select new { v.ID_, CDSNUMBER, Surname, Firstname, Holdings, stockISIN, Issuer };

//            GridView1.DataSource = action.ToList();
//            GridView1.DataBind();
//            }
//            else
//            {
//                trades();
//            }
            
        }
        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
     


        }
        public void msgbox(string strMessage)
        {
            string strScript = "<script language=JavaScript>";
            strScript += "window.alert(\"" + strMessage + "\");";
            strScript += "</script>";
            System.Web.UI.WebControls.Label lbl = new System.Web.UI.WebControls.Label();
            lbl.Text = strScript;
            Page.Controls.Add(lbl);
        }
        protected void grdData_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            trades();
        }
        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("Local IP Address Not Found!");
        }
    }
}
