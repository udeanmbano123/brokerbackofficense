namespace BrokerOffice.Migrations
{
    using Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Security.Cryptography;
    using System.Text;
    using StoredProcedures;
    internal sealed class Configuration : DbMigrationsConfiguration<BrokerOffice.DAO.SBoardContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(BrokerOffice.DAO.SBoardContext context)
        {
            Str my = new StoredProcedures.Str();
            context.Database.ExecuteSqlCommand("DROP PROCEDURE IF EXISTS [dbo].[NewAccounts]");
            context.Database.ExecuteSqlCommand("DROP PROCEDURE IF EXISTS [dbo].[NewAccountsAuth]");
            context.Database.ExecuteSqlCommand(my.newaccount());
            context.Database.ExecuteSqlCommand(my.newaccountAuth());

        }

    }
    
    }

