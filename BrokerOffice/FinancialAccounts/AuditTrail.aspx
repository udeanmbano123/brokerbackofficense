﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AuditTrail.aspx.cs" MasterPageFile="~/Site1.Master" Inherits="BrokerOffice.FinancialAccounts.AuditTrail" %>

<%@ Register Assembly="DevExpress.Web.v18.2, Version=18.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div>


        <div class="panel panel-default" style="padding: 10px; margin: 10px">
            <div class="panel-heading" style="background-color: #428bca">
                <i style="color: white">AuditTrail</i>
            </div>

            <br />
            <div id="Tabs" role="tabpanel">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li class="active"><a href="#personal" aria-controls="personal" role="tab" data-toggle="tab">AuditTrail</a></li>
                  

                </ul>
                <!-- Tab panes -->
                <div class="tab-content" style="padding-top: 20px">
                    <div role="tabpanel" class="tab-pane active" id="personal">
                        <table class="table table-striped">
                            <asp:Label ID="txtFkey" runat="server" Visible="False" Text=""></asp:Label>
                            <asp:Label ID="txtID" Visible="false" runat="server" Text="Label"></asp:Label>
                            
                       
                            <tr>
                                <td>
<asp:Label ID="from" runat="server"  Text="From" Font-Bold="True"></asp:Label>
<dx:ASPxDateEdit ID="txtFromDate" CssClass="form-control" runat="server"></dx:ASPxDateEdit>
                          </td>
                          <td>
<asp:Label ID="to" runat="server"  Text="To" Font-Bold="True"></asp:Label>
<dx:ASPxDateEdit ID="txtToDate" CssClass="form-control"  runat="server"></dx:ASPxDateEdit>
                          </td>
                            </tr>
                        </table>
                        <dx style="float: right"><dx:ASPxButton ID="dxAction" runat="server" CssClass="btn btn-primary" Theme="Moderno" Text="Generate Report" OnClick="dxAction_Click"  ></dx:ASPxButton></dx>

                    </div>
                    
                </div>
            </div>
        </div>

    </div>
    <script type="text/javascript">
        $(function () {
            var tabName = $("[id*=TabName]").val() != "" ? $("[id*=TabName]").val() : "personal";
            $('#Tabs a[href="#' + tabName + '"]').tab('show');
            $("#Tabs a").click(function () {
                $("[id*=TabName]").val($(this).attr("href").replace("#", ""));
            });
        });
    </script>

</asp:Content>