﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerOffice.DAO;
using System.Data;
using WebMatrix.WebData;


namespace BrokerOffice.FinancialAccounts
{
    public partial class DetailedGeneralLedger : System.Web.UI.Page
    {
        public string id = "";
        private SBoardContext db = new SBoardContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            id = Request.QueryString["id"];
           
        }



        
        public void msgbox(string strMessage)
        {
            string strScript = "<script language=JavaScript>";
            strScript += "window.alert(\"" + strMessage + "\");";
            strScript += "</script>";
            System.Web.UI.WebControls.Label lbl = new System.Web.UI.WebControls.Label();
            lbl.Text = strScript;
            Page.Controls.Add(lbl);
        }


      
        protected void dxAction_Click(object sender, EventArgs e)
        {

            Response.Redirect("~/Reporting/GeneralLedgerDetailed.aspx?From=" + Convert.ToDateTime(txtDateFrom.Text).ToString("dd-MMM-yyyy") + "&to=" + Convert.ToDateTime(txtDateTo.Text).ToString("dd-MMM-yyyy"));
            msgbox("Report Generated");
        }

     
    }
}