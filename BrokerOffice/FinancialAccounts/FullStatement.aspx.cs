﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BrokerOffice.DAO;
using System.Data;
using WebMatrix.WebData;

namespace BrokerOffice.FinancialAccounts
{
    public partial class FullStatement : System.Web.UI.Page
    {
        public string id = "";
        private SBoardContext db = new SBoardContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            id = Request.QueryString["id"];
            Debit.SelectedValue = "GL";


            try
            {
                string dr = Debit.SelectedValue.ToString();
            }
            catch (Exception)
            {


            }
            drpDebit.DataSource = GetDataSource();
            drpDebit.DataBind();

            txtAcc.Text = "1100";

        }





        protected void Debit_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string dr = Debit.SelectedValue.ToString();
            }
            catch (Exception)
            {


            }
            drpDebit.DataSource = GetDataSource();
            drpDebit.DataBind();

        }
        private DataTable GetDataSource()
        {
            //DataTable defined
            var dtSource = new DataTable();
            dtSource.Columns.Add("id", typeof(string));
            dtSource.Columns.Add("FullNames", typeof(string));
            string x = "";

            string name = "";
            int num = 0;



            string pia = "";

            //Fill rows
            if (Debit.SelectedIndex == 0)
            {
                dtSource.Rows.Add("0", "Please Select a group");
                var dd = from c in db.tblFinancialAccounts
                         where c.AccountNumber != num
                         select c;
                foreach (var p in dd)
                {

                    dtSource.Rows.Add(p.AccountNumber, p.AccountName);
                }

            }
            else if (Debit.SelectedIndex == 1)
            {
                dtSource.Rows.Add("0", "Please Select a client");
                var dd = from c in db.Account_Creations
                         where c.StatuSActive == true && c.CDSC_Number != pia
                         select c;
                foreach (var p in dd)
                {
                    if (p.OtherNames == null)
                    {
                        name = p.Surname_CompanyName + " ," + p.CDSC_Number;
                    }
                    else if (p.OtherNames == "")
                    {
                        name = p.Surname_CompanyName + " ," + p.CDSC_Number;
                    }
                    else
                    {
                        name = p.OtherNames + " " + p.Surname_CompanyName + " ," + p.CDSC_Number;
                    }

                    dtSource.Rows.Add(p.CDSC_Number, name);
                }

            }//Fill rows



            return dtSource;
        }

        private DataTable GetDataSource2()
        {
            //DataTable defined
            var dtSource = new DataTable();
            dtSource.Columns.Add("id", typeof(string));
            dtSource.Columns.Add("FullNames", typeof(string));
            string x = "";

            string name = "";
            int num = 0;

            try
            {
                num = Convert.ToInt32(drpDebit.SelectedItem.Value);
            }
            catch (Exception)
            {

                num = 0;
            }
            string pia = "";
            try
            {
                pia = drpDebit.SelectedItem.Value.ToString();
            }
            catch (Exception)
            {

                pia = "0";
            }
            //Fill rows

            return dtSource;
        }

        public void msgbox(string strMessage)
        {
            string strScript = "<script language=JavaScript>";
            strScript += "window.alert(\"" + strMessage + "\");";
            strScript += "</script>";
            System.Web.UI.WebControls.Label lbl = new System.Web.UI.WebControls.Label();
            lbl.Text = strScript;
            Page.Controls.Add(lbl);
        }


        protected void Clear()
        {
            System.Web.HttpContext.Current.Session["NOT"] = "You have successfully added the transaction";
            msgbox("You have successfully added the transaction");
            Response.Redirect(Request.RawUrl);
        }

        protected void dxAction_Click(object sender, EventArgs e)
        {

            Response.Redirect("~/Reporting/AccStatementFull.aspx?Acco=" + txtAcc.Text + "&From=" + Convert.ToDateTime(begindate.Text).ToString("dd-MMM-yyyy") + "&to=" + Convert.ToDateTime(expirydate.Text).ToString("dd-MMM-yyyy"));
            msgbox("Report Generated");
        }

        protected void dxPersonal_Click(object sender, EventArgs e)
        {

        }

        protected void drpDebit_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtAcc.Text = drpDebit.SelectedItem.Value.ToString();
        }

        protected void bhatan_Click(object sender, EventArgs e)
        {

        }
    }
}