﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BrokerOffice.Models;
using RestSharp;
using Newtonsoft.Json;
using WebMatrix.WebData;
using BrokerOffice.DAO;
using PagedList;
using BrokerOffice.AccountCreation;
using BrokerOffice.DAO.security;

namespace BrokerOffice.Controllers
{// [CustomAuthorize(RolesConfigKey = "RolesConfigKey")]
 // [CustomAuthorize(UsersConfigKey = "UsersConfigKey")]
    [CustomAuthorize(Roles = "Approver", NotifyUrl = "/UnauthorizedPage")]
    // [CustomAuthorize(Users = "1,2")]

    public class Accounts_AuditController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private SBoardContext db2 = new SBoardContext();

        // GET: Accounts_Audit
        public ActionResult Index(string sortOrder, string currentFilter, string
  searchString, string Date1String, string Date2String, int? page)
        {
           
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.IdentificationSortParm = String.IsNullOrEmpty(sortOrder) ? "identification" : "";
            ViewBag.SurnameSortParm = String.IsNullOrEmpty(sortOrder) ? "surname" : "";
            ViewBag.CDSCSortParm = String.IsNullOrEmpty(sortOrder) ? "cdsc" : "";
           var c = db2.Users.ToList().Where(a=>a.Email==WebSecurity.CurrentUserName);
            string vx = "";
            foreach (var d in c)
            {
                vx = d.BrokerCode;
            }
            var client = new RestClient("http://localhost/BrokerService");
            var request = new RestRequest("AccountsList/{s}", Method.GET);
            request.AddUrlSegment("s",vx);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            List<Accounts_Audit> dataList = JsonConvert.DeserializeObject<List<Accounts_Audit>>(validate);

            
            var per = from s in dataList
                      where s.BrokerCode==vx
                      select s;
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;
            if (!String.IsNullOrEmpty(searchString))
            {
                per = per.Where(s =>
                (s.Surname).ToUpper().Contains(searchString.ToUpper())
                 ||
                 (s.Forenames).ToUpper().Contains(searchString.ToUpper())
                 ||
                (s.CDS_Number).ToUpper().Contains(searchString.ToUpper()));
            }
      
            switch (sortOrder)
            {
                case "name_desc":
                    per = per.OrderByDescending(s => s.Forenames);
                    break;
                case "identification":
                    per = per.OrderBy(s => s.IDNoPP);
                    break;
                case "surname":
                    per = per.OrderBy(s => s.Surname);
                    break;
                case "cdsc":
                    per = per.OrderBy(s => s.CDS_Number);
                    break;
                default:
                    per = per.OrderBy(s => s.ID);
                    break;
            }
            int pageSize = 20;
            int pageNumber = (page ?? 1);
            return View(per.ToPagedList(pageNumber, pageSize));
         }

        // GET: Accounts_Audit/Details/5
        public async Task<ActionResult> Details(int? id)
        {
           
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var c = db2.Users.ToList().Where(a => a.Email == WebSecurity.CurrentUserName);
            string vx = "";
            foreach (var d in c)
            {
                vx = d.BrokerCode;
            }
            var client = new RestClient("http://localhost/BrokerService");
            var request = new RestRequest("AccountsList/{s}", Method.GET);
            request.AddUrlSegment("s", vx);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            List<Accounts_Audit> dataList = JsonConvert.DeserializeObject<List<Accounts_Audit>>(validate);

           var p = dataList.ToList().Where(a=>a.ID==id);
            Accounts_Audit accounts_Audit = new Accounts_Audit();
            foreach(var t in p)
            {
                accounts_Audit.ID = t.ID;
                accounts_Audit.CDS_Number = t.CDS_Number;
                accounts_Audit.BrokerCode = t.BrokerCode;
                accounts_Audit.AccountType = t.AccountType;
                accounts_Audit.Surname = t.Surname;
                accounts_Audit.Middlename = t.Middlename;
                accounts_Audit.Forenames = t.Forenames;
                accounts_Audit.Initials = t.Initials;
                accounts_Audit.Title = t.Title;
                accounts_Audit.IDNoPP = t.IDNoPP;
                accounts_Audit.IDtype = t.IDtype;
                accounts_Audit.Nationality = t.Nationality;
                accounts_Audit.DOB = t.DOB;
                accounts_Audit.Gender = t.Gender;
                accounts_Audit.Add_1 = t.Add_1;
                accounts_Audit.Add_2 = t.Add_2;
                accounts_Audit.Add_3 = t.Add_3;
                accounts_Audit.Add_4 = t.Add_4;
                accounts_Audit.Country = t.Country;
                accounts_Audit.City = t.City;
                accounts_Audit.Tel = t.Tel;
                accounts_Audit.Mobile = t.Mobile;
                accounts_Audit.Email = t.Email;
                accounts_Audit.Category = t.Category;
                accounts_Audit.Custodian = t.Custodian;
                accounts_Audit.TradingStatus = t.TradingStatus;
                accounts_Audit.Industry = t.Industry;
                accounts_Audit.Tax = t.Tax;
                accounts_Audit.Div_Bank = t.Div_Bank;
                accounts_Audit.Div_Branch = t.Div_Branch;
                accounts_Audit.Div_AccountNo = t.Div_AccountNo;
                accounts_Audit.Cash_Bank = t.Cash_Bank;
                accounts_Audit.Cash_Branch = t.Cash_Branch;
                accounts_Audit.Cash_AccountNo = t.Cash_AccountNo;
                accounts_Audit.Client_Image = t.Client_Image;
                accounts_Audit.Documents = t.Documents;
                accounts_Audit.BioMatrix = t.BioMatrix;
                accounts_Audit.Attached_Documents = t.Attached_Documents;
                accounts_Audit.Date_Created = t.Date_Created;
                accounts_Audit.Update_Type = t.Update_Type;
                accounts_Audit.Created_By = t.Created_By;
                accounts_Audit.AuthorizationState = t.AuthorizationState;
                accounts_Audit.Comments = t.Comments;
                accounts_Audit.VerificationCode = t.VerificationCode;
                accounts_Audit.DivPayee = t.DivPayee;
                accounts_Audit.SettlementPayee = t.SettlementPayee;
                accounts_Audit.idnopp2 = t.idnopp2;
                accounts_Audit.idtype2 = t.idtype2;
                accounts_Audit.client_image2 = t.client_image2;
                accounts_Audit.documents2 = t.documents2;
                accounts_Audit.isin = t.isin;
                accounts_Audit.company_code = t.company_code;
                accounts_Audit.mobile_money = t.mobile_money;
                accounts_Audit.mobile_number = t.mobile_number;
                accounts_Audit.currency = t.currency;
                accounts_Audit.Indegnous = t.Indegnous;
                accounts_Audit.Race = t.Race;
                accounts_Audit.Disadvantaged = t.Disadvantaged;
                accounts_Audit.NationalityBy = t.NationalityBy;
                accounts_Audit.Custody = t.Custody;
                accounts_Audit.Trading = t.Trading;
    }
            if (p == null)
            {
                return HttpNotFound();
            }
            return View(accounts_Audit);
        }

        

       

        // GET: Accounts_Audit/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Accounts_Audit accounts_Audit = await db.Accounts_Audit.FindAsync(id);
            if (accounts_Audit == null)
            {
                return HttpNotFound();
            }
            return View(accounts_Audit);
        }

        // POST: Accounts_Audit/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Accounts_Audit accounts_Audit = await db.Accounts_Audit.FindAsync(id);
            db.Accounts_Audit.Remove(accounts_Audit);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }


        // GET: Accounts_Audit/Delete/5
        public async Task<ActionResult> Download(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var c = db2.Users.ToList().Where(a => a.Email == WebSecurity.CurrentUserName);
            string vx = "";
            foreach (var d in c)
            {
                vx = d.BrokerCode;
            }
            var client = new RestClient("http://localhost/BrokerService");
            var request = new RestRequest("AccountsListUpdate/{s}/{p}", Method.GET);
            request.AddUrlSegment("s", vx);
            request.AddUrlSegment("p", id.ToString());
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            List<Accounts_Audit> dataList = JsonConvert.DeserializeObject<List<Accounts_Audit>>(validate);
            var p = dataList.ToList().Where(a => a.ID == id);
            Accounts_Audit accounts_Audit = new Accounts_Audit();
            foreach (var t in p)
            {
                accounts_Audit.ID = t.ID;
                accounts_Audit.CDS_Number = t.CDS_Number;
                accounts_Audit.BrokerCode = t.BrokerCode;
                accounts_Audit.AccountType = t.AccountType;
                accounts_Audit.Surname = t.Surname;
                accounts_Audit.Middlename = t.Middlename;
                accounts_Audit.Forenames = t.Forenames;
                accounts_Audit.Initials = t.Initials;
                accounts_Audit.Title = t.Title;
                accounts_Audit.IDNoPP = t.IDNoPP;
                accounts_Audit.IDtype = t.IDtype;
                accounts_Audit.Nationality = t.Nationality;
                accounts_Audit.DOB = t.DOB;
                accounts_Audit.Gender = t.Gender;
                accounts_Audit.Add_1 = t.Add_1;
                accounts_Audit.Add_2 = t.Add_2;
                accounts_Audit.Add_3 = t.Add_3;
                accounts_Audit.Add_4 = t.Add_4;
                accounts_Audit.Country = t.Country;
                accounts_Audit.City = t.City;
                accounts_Audit.Tel = t.Tel;
                accounts_Audit.Mobile = t.Mobile;
                accounts_Audit.Email = t.Email;
                accounts_Audit.Category = t.Category;
                accounts_Audit.Custodian = t.Custodian;
                accounts_Audit.TradingStatus = t.TradingStatus;
                accounts_Audit.Industry = t.Industry;
                accounts_Audit.Tax = t.Tax;
                accounts_Audit.Div_Bank = t.Div_Bank;
                accounts_Audit.Div_Branch = t.Div_Branch;
                accounts_Audit.Div_AccountNo = t.Div_AccountNo;
                accounts_Audit.Cash_Bank = t.Cash_Bank;
                accounts_Audit.Cash_Branch = t.Cash_Branch;
                accounts_Audit.Cash_AccountNo = t.Cash_AccountNo;
                accounts_Audit.Client_Image = t.Client_Image;
                accounts_Audit.Documents = t.Documents;
                accounts_Audit.BioMatrix = t.BioMatrix;
                accounts_Audit.Attached_Documents = t.Attached_Documents;
                accounts_Audit.Date_Created = t.Date_Created;
                accounts_Audit.Update_Type = t.Update_Type;
                accounts_Audit.Created_By = t.Created_By;
                accounts_Audit.AuthorizationState = t.AuthorizationState;
                accounts_Audit.Comments = t.Comments;
                accounts_Audit.VerificationCode = t.VerificationCode;
                accounts_Audit.DivPayee = t.DivPayee;
                accounts_Audit.SettlementPayee = t.SettlementPayee;
                accounts_Audit.idnopp2 = t.idnopp2;
                accounts_Audit.idtype2 = t.idtype2;
                accounts_Audit.client_image2 = t.client_image2;
                accounts_Audit.documents2 = t.documents2;
                accounts_Audit.isin = t.isin;
                accounts_Audit.company_code = t.company_code;
                accounts_Audit.mobile_money = t.mobile_money;
                accounts_Audit.mobile_number = t.mobile_number;
                accounts_Audit.currency = t.currency;
                accounts_Audit.Indegnous = t.Indegnous;
                accounts_Audit.Race = t.Race;
                accounts_Audit.Disadvantaged = t.Disadvantaged;
                accounts_Audit.NationalityBy = t.NationalityBy;
                accounts_Audit.Custody = t.Custody;
                accounts_Audit.Trading = t.Trading;
            }

            if (p== null)
            {
                return HttpNotFound();
            }
            return View(accounts_Audit);
        }

        // POST: Accounts_Audit/Delete/5
        [HttpPost, ActionName("Download")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DownloadConfirmed(int id)
        {
            var c = db2.Users.ToList().Where(a => a.Email == WebSecurity.CurrentUserName);
            string vx = "";
            foreach (var d in c)
            {
                vx = d.BrokerCode;
            }
            var client = new RestClient("http://localhost/BrokerService");
            var request = new RestRequest("AccountsListUpdate/{s}/{p}", Method.GET);
            request.AddUrlSegment("s", vx);
            request.AddUrlSegment("p", id.ToString());
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            List<Accounts_Audit> dataList = JsonConvert.DeserializeObject<List<Accounts_Audit>>(validate);
            var p = dataList.ToList().Where(a => a.ID == id);
            Accounts_Audit accounts_Audit = new Accounts_Audit();
            foreach (var t in p)
            {
                accounts_Audit.ID = t.ID;
                accounts_Audit.CDS_Number = t.CDS_Number;
                accounts_Audit.BrokerCode = t.BrokerCode;
                accounts_Audit.AccountType = t.AccountType;
                accounts_Audit.Surname = t.Surname;
                accounts_Audit.Middlename = t.Middlename;
                accounts_Audit.Forenames = t.Forenames;
                accounts_Audit.Initials = t.Initials;
                accounts_Audit.Title = t.Title;
                accounts_Audit.IDNoPP = t.IDNoPP;
                accounts_Audit.IDtype = t.IDtype;
                accounts_Audit.Nationality = t.Nationality;
                accounts_Audit.DOB = t.DOB;
                accounts_Audit.Gender = t.Gender;
                accounts_Audit.Add_1 = t.Add_1;
                accounts_Audit.Add_2 = t.Add_2;
                accounts_Audit.Add_3 = t.Add_3;
                accounts_Audit.Add_4 = t.Add_4;
                accounts_Audit.Country = t.Country;
                accounts_Audit.City = t.City;
                accounts_Audit.Tel = t.Tel;
                accounts_Audit.Mobile = t.Mobile;
                accounts_Audit.Email = t.Email;
                accounts_Audit.Category = t.Category;
                accounts_Audit.Custodian = t.Custodian;
                accounts_Audit.TradingStatus = t.TradingStatus;
                accounts_Audit.Industry = t.Industry;
                accounts_Audit.Tax = t.Tax;
                accounts_Audit.Div_Bank = t.Div_Bank;
                accounts_Audit.Div_Branch = t.Div_Branch;
                accounts_Audit.Div_AccountNo = t.Div_AccountNo;
                accounts_Audit.Cash_Bank = t.Cash_Bank;
                accounts_Audit.Cash_Branch = t.Cash_Branch;
                accounts_Audit.Cash_AccountNo = t.Cash_AccountNo;
                accounts_Audit.Client_Image = t.Client_Image;
                accounts_Audit.Documents = t.Documents;
                accounts_Audit.BioMatrix = t.BioMatrix;
                accounts_Audit.Attached_Documents = t.Attached_Documents;
                accounts_Audit.Date_Created = t.Date_Created;
                accounts_Audit.Update_Type = t.Update_Type;
                accounts_Audit.Created_By = t.Created_By;
                accounts_Audit.AuthorizationState = t.AuthorizationState;
                accounts_Audit.Comments = t.Comments;
                accounts_Audit.VerificationCode = t.VerificationCode;
                accounts_Audit.DivPayee = t.DivPayee;
                accounts_Audit.SettlementPayee = t.SettlementPayee;
                accounts_Audit.idnopp2 = t.idnopp2;
                accounts_Audit.idtype2 = t.idtype2;
                accounts_Audit.client_image2 = t.client_image2;
                accounts_Audit.documents2 = t.documents2;
                accounts_Audit.isin = t.isin;
                accounts_Audit.company_code = t.company_code;
                accounts_Audit.mobile_money = t.mobile_money;
                accounts_Audit.mobile_number = t.mobile_number;
                accounts_Audit.currency = t.currency;
                accounts_Audit.Indegnous = t.Indegnous;
                accounts_Audit.Race = t.Race;
                accounts_Audit.Disadvantaged = t.Disadvantaged;
                accounts_Audit.NationalityBy = t.NationalityBy;
                accounts_Audit.Custody = t.Custody;
                accounts_Audit.Trading = t.Trading;
            }
            
            int x = db2.Account_Creations.ToList().Where(a => a.CDSC_Number == accounts_Audit.CDS_Number).Count();
            if (x<=0)
            {
                Account_CreationPending my = new Account_CreationPending();
                my.CDSC_Number=accounts_Audit.CDS_Number;
                my.Broker=accounts_Audit.BrokerCode;
                my.accountcategory=accounts_Audit.AccountType;
                my.Surname_CompanyName=accounts_Audit.Surname;
                my.Middlename=accounts_Audit.Middlename;
                my.OtherNames=accounts_Audit.Forenames;
                my.Initials=accounts_Audit.Initials;
                my.Title= accounts_Audit.Title;
                my.Identification=accounts_Audit.IDNoPP;
                my.idtype=accounts_Audit.IDtype;
                my.Nationality=accounts_Audit.Nationality;
                my.DateofBirth_Incorporation=accounts_Audit.DOB;
                my.Gender=accounts_Audit.Gender;
                my.Address1=accounts_Audit.Add_1;
                my.Address2=accounts_Audit.Add_2;
                my.Address3=accounts_Audit.Add_3;
                my.country = accounts_Audit.Country;
                my.Town=accounts_Audit.City;
                my.TelephoneNumber=accounts_Audit.Tel;
                my.MobileNumber=accounts_Audit.Mobile;
                my.Emailaddress=accounts_Audit.Email;
                //my.accountcategory=accounts_Audit.Category;
                my.TaxBracket = Convert.ToDecimal(accounts_Audit.Tax);
               //accounts_Audit.Industry = t.Industry;
                //accounts_Audit.Tax = t.Tax;
                my.divbank=accounts_Audit.Div_Bank;
                my.divbankcode = accounts_Audit.Div_Bank;
                my.divbranch=accounts_Audit.Div_Branch;
                my.divbranchcode = accounts_Audit.Div_Branch;
                my.divacc=accounts_Audit.Div_AccountNo;
                my.Bankname=accounts_Audit.Cash_Bank;
                my.Bankcode = accounts_Audit.Cash_Bank;
                my.Branch=accounts_Audit.Cash_Branch;
                my.BranchCode= accounts_Audit.Cash_Branch;
                my.Accountnumber=accounts_Audit.Cash_AccountNo;
               
               my.Date_Created= accounts_Audit.Date_Created;
                my.CreatedBy = accounts_Audit.Created_By;
              my.divpayee= accounts_Audit.DivPayee;
               my.idtype2=accounts_Audit.idtype2;
                
               my.mobile_money= accounts_Audit.mobile_money;
                my.MobileNumber = accounts_Audit.mobile_number;
                my.currency=accounts_Audit.currency;
                
                my.Nationality=accounts_Audit.NationalityBy;
                my.Resident = accounts_Audit.Country;
                my.dividnumber = accounts_Audit.idnopp2;
                my.update_type = "NEW";
                db2.Account_CreationPendingss.Add(my);
                await db2.SaveChangesAsync(WebSecurity.CurrentUserName);

                db2.Database.ExecuteSqlCommand("Exec NewAccounts @cust_no='" + my.CDSC_Number + "'");

                System.Web.HttpContext.Current.Session["NOT"] = accounts_Audit.CDS_Number + ". You have successfully uploaded the account from CDS";
             }
            else
            {
                System.Web.HttpContext.Current.Session["NOT"] = accounts_Audit.CDS_Number + ". Account exsists in Broker Office";

            }
            //db.Accounts_Audit.Remove(accounts_Audit);
            //await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
