﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BrokerOffice.DAO;
using BrokerOffice.Models;
using BrokerOffice.DAO.security;
using WebMatrix.WebData;
using PagedList;

namespace BrokerOffice.Controllers
{// [CustomAuthorize(RolesConfigKey = "RolesConfigKey")]
 // [CustomAuthorize(UsersConfigKey = "UsersConfigKey")]
    [CustomAuthorize(Roles = "User,Approver,BrokerAdmin,BrokerAdmin2", NotifyUrl = " /UnauthorizedPage")]
    // [CustomAuthorize(Users = "1,2")]

    public class BatchHeadersApproveController : Controller
    {
        private SBoardContext db = new SBoardContext();

        // GET: BatchHeaders
        public async Task<ActionResult> Index(string sortOrder, string currentFilter, string
  searchString,int? page)
        {
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.ModuleSortParm = String.IsNullOrEmpty(sortOrder) ? "module" : "";
            var per = from s in db.BatchHeaders
                      select s;
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;
            if (!String.IsNullOrEmpty(searchString))
            {
                per = per.Where(s =>
                ((s.BatchNo).ToUpper().Contains(searchString.ToUpper())
                 ||
                 (s.BatchName).ToUpper().Contains(searchString.ToUpper())) && s.CreatedBy == WebSecurity.CurrentUserName
               );
            }
            switch (sortOrder)
            {
                case "name_desc":
                    per = per.OrderByDescending(s => s.BatchNo);
                    break;

                case "module":
                    per = per.OrderBy(s =>s.DateCreated);
                    break;

                default:
                    per = per.OrderBy(s => s.id);
                    break;
            }
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            per = per.Where(a=>a.Status==false && a.Auth==true);
            return View(per.ToPagedList(pageNumber, pageSize));

        }

        // GET: BatchHeaders/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BatchHeader batchHeader = await db.BatchHeaders.FindAsync(id);
            if (batchHeader == null)
            {
                return HttpNotFound();
            }
            return View(batchHeader);
        }

        // GET: BatchHeaders/Create
        public ActionResult Create()
        {
            ViewBag.Batch = regionS();
            return View();
        }

        // POST: BatchHeaders/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "id,BatchNo,BatchType,Amount,NumberOfTrxns,CreatedBy,DateCreated,Status,BatchName,BatchDate")] BatchHeader batchHeader)
        {
            int max = 0;
            try
            {
                max = db.BatchHeaders.ToList().Max(a => a.id);
            }
            catch (Exception)
            {

                max=0;
            }
            batchHeader.BatchNo = (max+1).ToString() + "B";
            batchHeader.DateCreated = DateTime.Now;
            batchHeader.CreatedBy =WebSecurity.CurrentUserName;
            batchHeader.Status = false;
            if (ModelState.IsValid)
            {
                db.BatchHeaders.Add(batchHeader);
                await db.SaveChangesAsync(WebSecurity.CurrentUserName);
                return RedirectToAction("Index");
            }
            ViewBag.Batch = regionS(batchHeader.BatchType,batchHeader.BatchType);
            return View(batchHeader);
        }

        // GET: BatchHeaders/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BatchHeader batchHeader = await db.BatchHeaders.FindAsync(id);
            if (batchHeader == null)
            {
                return HttpNotFound();
            }
            ViewBag.Batch = regionS(batchHeader.BatchType, batchHeader.BatchType);
            return View(batchHeader);
        }

        // POST: BatchHeaders/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "id,BatchNo,BatchType,Amount,NumberOfTrxns,CreatedBy,DateCreated,Status,BatchName,BatchDate")] BatchHeader batchHeader)
        {
            if (ModelState.IsValid)
            {
                db.Entry(batchHeader).State = EntityState.Modified;
                await db.SaveChangesAsync(WebSecurity.CurrentUserName);
                return RedirectToAction("Index");
            }
            ViewBag.Batch = regionS(batchHeader.BatchType, batchHeader.BatchType);
            return View(batchHeader);
        }

        // GET: BatchHeaders/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BatchHeader batchHeader = await db.BatchHeaders.FindAsync(id);
            if (batchHeader == null)
            {
                return HttpNotFound();
            }
            return View(batchHeader);
        }

        // POST: BatchHeaders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            BatchHeader batchHeader = await db.BatchHeaders.FindAsync(id);
            db.BatchHeaders.Remove(batchHeader);
            await db.SaveChangesAsync(WebSecurity.CurrentUserName);
            return RedirectToAction("Index");
        }
        public List<SelectListItem> regionS()
        {


            List<SelectListItem> phy = new List<SelectListItem>();
            var p = db.para_issuers.ToList();
            phy.Add(new SelectListItem { Text = "Receipt", Value = "Receipt" });
            phy.Add(new SelectListItem { Text = "Journal", Value = "Journal" });
            phy.Add(new SelectListItem { Text = "Cashbook", Value = "Cashbook" });
            return phy;
        }
        public List<SelectListItem> regionS(string m, string n)
        {


            List<SelectListItem> phy = new List<SelectListItem>();
            /*
             Receipt
             Journal
             Cashbook
             */
            phy.Add(new SelectListItem { Text = m, Value = n });
            phy.Add(new SelectListItem { Text = "Receipt", Value = "Receipt" });
            phy.Add(new SelectListItem { Text = "Journal", Value = "Journal" });
            phy.Add(new SelectListItem { Text = "Cashbook", Value = "Cashbook" });

            return phy;
        }
        public async Task<ActionResult> Add(string id)
        {
            System.Web.HttpContext.Current.Session["Batch"] = id.ToString();
           return Redirect("~/BatchTransactionsApprove/Index");
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
