﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BrokerOffice.DAO;
using BrokerOffice.Models;
using BrokerOffice.DAO.security;
using WebMatrix.WebData;
using PagedList;

namespace BrokerOffice.Controllers
{//[CustomAuthorize(RolesConfigKey = "RolesConfigKey")]
 // [CustomAuthorize(UsersConfigKey = "UsersConfigKey")]
    [CustomAuthorize(Roles = "User", NotifyUrl = "/UnauthorizedPage")]
    // [CustomAuthorize(Users = "1")]
    public class TasksUserController : Controller
    {
        private SBoardContext db = new SBoardContext();

        // GET: Tasks
        public async Task<ActionResult> Index(string sortOrder, int? page)
        {
            var users = db.Users.ToList().Where(a=>a.Email==WebSecurity.CurrentUserName);
            int userid = 0;

            foreach(var x in users)
            {
                userid = x.UserId;
            }

            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.ModuleSortParm = String.IsNullOrEmpty(sortOrder) ? "module" : "";
            var per = from s in db.Tasks
                      where s.TaskUserID==userid
                      select s;
            switch (sortOrder)
            {
                case "name_desc":
                    per = per.OrderByDescending(s => s.TaskTitle);
                    break;

                case "module":
                    per = per.OrderBy(s => s.TaskDue);
                    break;

                default:
                    per = per.OrderBy(s => s.TaskID);
                    break;
            }
            int pageSize = 5;
            int pageNumber = (page ?? 1);
            return View(per.ToPagedList(pageNumber, pageSize));
          //  return View(await db.Tasks.ToListAsync());
        }

        // GET: Tasks/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Models.Task task = await db.Tasks.FindAsync(id);
            if (task == null)
            {
                return HttpNotFound();
            }
            return View(task);
        }

        // GET: Tasks/Create
        public ActionResult Create()
        {
            var list2 = db.Users.ToList();
            //Create List of SelectListItem
            List<SelectListItem> selectlist2 = new List<SelectListItem>();
            selectlist2.Add(new SelectListItem() { Text = "", Value = "" });
            foreach (var row in list2)
            {
                //Adding every record to list  
                selectlist2.Add(new SelectListItem { Text = row.Email, Value = row.UserId.ToString() });
            }

            ViewBag.Category = selectlist2;
            return View();
        }

        // POST: Tasks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "TaskID,TaskTitle,TaskDescription,TaskUser,TaskUserID,percentagedone,status,fromUser,fromUserID,TaskDue")] Models.Task task)
        {
            if (ModelState.IsValid)
            {
                int myid = Convert.ToInt32(Request["TaskUser"].ToString());

                task.TaskUserID = myid;

                var tasksusers = db.Users.ToList().Where(a => a.UserId == myid);
                foreach(var c in tasksusers)
                {
                    task.TaskUser = c.Email;
                }
                var users = db.Users.ToList().Where(a => a.Email == WebSecurity.CurrentUserName);

                foreach(var d in users)
                {
                    task.fromUser = d.Email;
                    task.fromUserID = d.UserId.ToString();
                }
               
                db.Tasks.Add(task);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            var list2 = db.Users.ToList();
            //Create List of SelectListItem
            List<SelectListItem> selectlist2 = new List<SelectListItem>();
            selectlist2.Add(new SelectListItem() { Text = Request[""], Value = Request[""] });
            foreach (var row in list2)
            {
                //Adding every record to list  
                selectlist2.Add(new SelectListItem { Text = row.Email, Value = row.UserId.ToString() });
            }

            ViewBag.Category = selectlist2;
            return View(task);
        }

        // GET: Tasks/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Models.Task task = await db.Tasks.FindAsync(id);
            var list2 = db.Users.ToList();
            //Create List of SelectListItem
            List<SelectListItem> selectlist2 = new List<SelectListItem>();
            selectlist2.Add(new SelectListItem() { Text = task.TaskUser, Value = task.TaskUserID.ToString() });
            foreach (var row in list2)
            {
                //Adding every record to list  
                selectlist2.Add(new SelectListItem { Text = row.Email, Value = row.UserId.ToString() });
            }

            ViewBag.Category = selectlist2;
            if (task == null)
            {
                return HttpNotFound();
            }
            return View(task);
        }

        // POST: Tasks/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "TaskID,TaskTitle,TaskDescription,TaskUser,TaskUserID,percentagedone,status,fromUser,fromUserID,TaskDue")] Models.Task task)
        {
            if (ModelState.IsValid)
            {
                int myid = Convert.ToInt32(Request["TaskUser"].ToString());

                task.TaskUserID = myid;

                var tasksusers = db.Users.ToList().Where(a => a.UserId == myid);
                foreach (var c in tasksusers)
                {
                    task.TaskUser = c.Email;
                }
                var users = db.Users.ToList().Where(a => a.Email == WebSecurity.CurrentUserName);

                foreach (var d in users)
                {
                    task.fromUser = d.Email;
                    task.fromUserID = d.UserId.ToString();
                }

                db.Entry(task).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            var list2 = db.Users.ToList();
            //Create List of SelectListItem
            List<SelectListItem> selectlist2 = new List<SelectListItem>();
            selectlist2.Add(new SelectListItem() { Text = Request[""], Value = Request[""] });
            foreach (var row in list2)
            {
                //Adding every record to list  
                selectlist2.Add(new SelectListItem { Text = row.Email, Value = row.UserId.ToString() });
            }

            ViewBag.Category = selectlist2;
            return View(task);
        }

        // GET: Tasks/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Models.Task task = await db.Tasks.FindAsync(id);
            if (task == null)
            {
                return HttpNotFound();
            }
            return View(task);
        }

        // POST: Tasks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Models.Task task = await db.Tasks.FindAsync(id);
            db.Tasks.Remove(task);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
