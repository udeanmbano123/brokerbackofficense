﻿using DevExpress.Web.Mvc;
using BrokerOffice.DAO.security;
using DevExpress.XtraReports.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BrokerOffice.Models;
using BrokerOffice.DAO;

using System.IO;

namespace BrokerOffice.Controllers
{// [CustomAuthorize(RolesConfigKey = "RolesConfigKey")]
 // [CustomAuthorize(UsersConfigKey = "UsersConfigKey")]
    [CustomAuthorize(Roles = "User,Approver,BrokerAdmin,BrokerAdmin2", NotifyUrl = " /UnauthorizedPage")]
    // [CustomAuthorize(Users = "1,2")]
    public class ReportingController : Controller
    {

        private SBoardContext db = new SBoardContext();
        // GET: Reporting
        public ActionResult ClientList()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ClientList([Bind(Include = "Asat,To")] ClientList registerreport)
        {

            if (ModelState.IsValid)
            {
                return Redirect("~/Reporting/ClientList.aspx?From=" + registerreport.Asat.ToString("dd-MMM-yyyy") + "&To=" + registerreport.To.ToString("dd-MMM-yyyy"));
            }
            return View();
        }

        public ActionResult TradesL()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult TradesL([Bind(Include = "Asat,To")] ClientList registerreport)
        {

            if (ModelState.IsValid)
            {
                return Redirect("~/Reporting/TradesList.aspx?From=" + registerreport.Asat.ToString("dd-MMM-yyyy") + "&To=" + registerreport.To.ToString("dd-MMM-yyyy"));
            }
            return View();
        }
        public ActionResult ClientListTop()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ClientListTop([Bind(Include = "Asat,To")] ClientList registerreport)
        {

            if (ModelState.IsValid)
            {
                return Redirect("~/Reporting/ClientListTop.aspx?From=" + registerreport.Asat.ToString("dd-MMM-yyyy") + "&To=" + registerreport.To.ToString("dd-MMM-yyyy"));
            }
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Posting([Bind(Include = "From,To")] Posting registerreport)
        {

            if (ModelState.IsValid)
            {
                return Redirect("~/Reporting/PostingOrders.aspx?From=" + registerreport.From.ToString("dd-MMM-yyyy") + "&To="+registerreport.To.ToString("dd-MMM-yyyy"));
            }
            return View();
        }

        public ActionResult Posting()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PostingPrint([Bind(Include = "From,To")] PostingL registerreport)
        {

            if (ModelState.IsValid)
            {

                foreach (var q in registerreport.Questions)
                {
                  

                }
                }
            var evalVM = new PostingL();
            var cp = db.Accounts_Masters.ToList().OrderBy(a => a.AccountName);

            foreach (var d in cp)
            {
                evalVM.Questions.Add(new Accountss_Master { AccountName = d.AccountName, AccountNumber = d.AccountNumber.ToString() });
            }

            return View();
        }
        public void ExportReport(XtraReport report, string fileName, string fileType, bool inline)
        {
            MemoryStream stream = new MemoryStream();

            Response.Clear();

            if (fileType == "xls")
                report.ExportToXls(stream);
            if (fileType == "pdf")
                report.ExportToPdf(stream);
            if (fileType == "rtf")
                report.ExportToRtf(stream);
            if (fileType == "csv")
                report.ExportToCsv(stream);

            Response.ContentType = "application/" + fileType;
            Response.AddHeader("Accept-Header", stream.Length.ToString());
            Response.AddHeader("Content-Disposition", (inline ? "Inline" : "Attachment") + "; filename=" + fileName + "." + fileType);
            Response.AddHeader("Content-Length", stream.Length.ToString());
            //Response.ContentEncoding = System.Text.Encoding.Default;
            Response.BinaryWrite(stream.ToArray());
            Response.End();
        }

        public ActionResult PostingPrint()
        {
            var evalVM = new PostingL();
            var cp = db.Accounts_Masters.ToList().OrderBy(a => a.AccountName);
            foreach (var d in cp)
            {
                evalVM.Questions.Add(new Accountss_Master { AccountName=d.AccountName+","+d.AccountNumber, AccountNumber =d.AccountNumber.ToString() });
            }
            return View(evalVM);
        }

        public ActionResult Portfolio()
        {
            return View();
        }

        public ActionResult TrialBalance()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult TrialBalance([Bind(Include = "DateHeld")] TrialBalance registerreport)
        {

            if (ModelState.IsValid)
            {
                return Redirect("~/Reporting/TrialBalance.aspx?id="+registerreport.DateHeld.ToString("dd-MMM-yyyy"));
            }
                return View();
        }

        public ActionResult IncomeStatement()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult IncomeStatement([Bind(Include = "DateHeld")] TrialBalance registerreport)
        {

            if (ModelState.IsValid)
            {
                return Redirect("~/Reporting/IncomeStatement.aspx?id=" + registerreport.DateHeld.ToString("dd-MMM-yyyy"));
            }
            return View();
        }

        public ActionResult IncomeStatementM()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult IncomeStatementM([Bind(Include = "DateHeld")] TrialBalance registerreport)
        {

            if (ModelState.IsValid)
            {
                DateTime date = Convert.ToDateTime(registerreport.DateHeld);
                var firstDayOfMonth = new DateTime(date.Year, date.Month, 1);
                var lastDayOfMonth = new DateTime(date.Year, date.Month, DateTime.DaysInMonth(date.Year, date.Month));


                return Redirect("~/Reporting/IncomeStatementM.aspx?From=" + firstDayOfMonth.ToString("dd-MMM-yyyy") + "&To=" + lastDayOfMonth.ToString("dd-MMM-yyyy"));
            }
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ExecutedOrder([Bind(Include = "From,To")] Posting registerreport)
        {

            if (ModelState.IsValid)
            {
                return Redirect("~/Reporting/ExecutedOrders.aspx?date=" + registerreport.From.ToString("dd-MMM-yyyy") + "&date2=" + registerreport.To.ToString("dd-MMM-yyyy"));
            }
            return View();
        }

        public ActionResult ExecutedOrder()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult OrderBook([Bind(Include = "From,To")] Posting registerreport)
        {

            if (ModelState.IsValid)
            {
                return Redirect("~/Reporting/OrderBookReport.aspx?date=" + registerreport.From.ToString("dd-MMM-yyyy") + "&date2=" + registerreport.To.ToString("dd-MMM-yyyy"));
            }
            return View();
        }
        public ActionResult OrderBook()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult OutStanding([Bind(Include = "From,To")] Posting registerreport)
        {

            if (ModelState.IsValid)
            {
                return Redirect("~/Reporting/OutStandingOrders.aspx?date=" + registerreport.From.ToString("dd-MMM-yyyy") + "&date2=" + registerreport.To.ToString("dd-MMM-yyyy"));
            }
            return View();
        }

        public ActionResult OutStanding()
        {
            return View();
        }

        public ActionResult BalanceSheet()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult BalanceSheet([Bind(Include = "DateHeld")] TrialBalance registerreport)
        {

            if (ModelState.IsValid)
            {
                return Redirect("~/Reporting/BalanceSheetReport.aspx?id=" + registerreport.DateHeld.ToString("dd-MMM-yyyy"));
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AccountAuditR([Bind(Include = "From,To")] Posting registerreport)
        {

            if (ModelState.IsValid)
            {
                return Redirect("~/Reporting/AccountAudit.aspx?From=" + registerreport.From.ToString("dd-MMM-yyyy") + "&To=" + registerreport.To.ToString("dd-MMM-yyyy"));
            }
            return View();
        }

        public ActionResult AccountAuditR()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AccountAuditB([Bind(Include = "From,To")] Posting registerreport)
        {

            if (ModelState.IsValid)
            {
                return Redirect("~/Reporting/BrokerAudit.aspx?From=" + registerreport.From.ToString("dd-MMM-yyyy") + "&To=" + registerreport.To.ToString("dd-MMM-yyyy"));
            }
            return View();
        }

        public ActionResult AccountAuditB()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult JournalAuditB([Bind(Include = "From,To")] Posting registerreport)
        {

            if (ModelState.IsValid)
            {
                return Redirect("~/Reporting/JournalAuditss.aspx?From=" + registerreport.From.ToString("dd-MMM-yyyy") + "&To=" + registerreport.To.ToString("dd-MMM-yyyy"));
            }
            return View();
        }

        public ActionResult JournalAuditB()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult MatchedDealsR([Bind(Include = "From,To,DT")] PostingD registerreport)
        {

            if (ModelState.IsValid)
            {
                if (registerreport.DT=="Date Posted")
                {
                    return Redirect("~/Reporting/MatchedDeals.aspx?From=" + registerreport.From.ToString("dd-MMM-yyyy") + "&To=" + registerreport.To.ToString("dd-MMM-yyyy") + "&DT=" + registerreport.DT.ToString());

                }else
                {
                    return Redirect("~/Reporting/MatchedDealss.aspx?From=" + registerreport.From.ToString("dd-MMM-yyyy") + "&To=" + registerreport.To.ToString("dd-MMM-yyyy") + "&DT=" + registerreport.DT.ToString());

                }
            }
            ViewBag.DT = regionD(registerreport.DT, registerreport.DT);
            return View();
        }

        public ActionResult MatchedDealsR()
        {
            ViewBag.DT = regionD();
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult MatchedDealsN([Bind(Include = "From,To")] Posting registerreport)
        {
            string m=System.Web.HttpContext.Current.Session["csd"].ToString();

            if (ModelState.IsValid)
            {
                return Redirect("~/Reporting/MatchedDealsN.aspx?From=" + registerreport.From.ToString("dd-MMM-yyyy") + "&To=" + registerreport.To.ToString("dd-MMM-yyyy") + "&csd="+m);
            }
            ViewBag.CSD = System.Web.HttpContext.Current.Session["csd"].ToString();
            return View();
        }

        public ActionResult MatchedDealsN()
        {
            ViewBag.CSD = System.Web.HttpContext.Current.Session["csd"].ToString();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CashF([Bind(Include = "From,To")] Posting registerreport)
        {

            if (ModelState.IsValid)
            {
                return Redirect("~/Reporting/CashFlowReport.aspx?From=" + registerreport.From.ToString("dd-MMM-yyyy") + "&To=" + registerreport.To.ToString("dd-MMM-yyyy"));
            }
            return View();
        }

        public ActionResult CashF()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChargesScheduleR([Bind(Include = "From,To")] Posting registerreport)
        {

            if (ModelState.IsValid)
            {
                return Redirect("~/Reporting/ChargesSchedule.aspx?From=" + registerreport.From.ToString("dd-MMM-yyyy") + "&To=" + registerreport.To.ToString("dd-MMM-yyyy"));
            }
            return View();
        }

        public ActionResult ChargesScheduleR()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult TransStatement([Bind(Include = "From,To")] Posting registerreport)
        {

            if (ModelState.IsValid)
            {
                return Redirect("~/Reporting/FullStatement.aspx?Begin=" + registerreport.From.ToString("dd-MMM-yyyy") + "&End=" + registerreport.To.ToString("dd-MMM-yyyy"));
            }
            return View();
        }

        public ActionResult TransStatement()
        {
            return View();
        }

        //Tax Returns
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult TaxReturns([Bind(Include = "From,To")] Posting registerreport)
        {

            if (ModelState.IsValid)
            {
                return Redirect("~/Reporting/TaxReturnss.aspx?Begin=" + registerreport.From.ToString("dd-MMM-yyyy") + "&End=" + registerreport.To.ToString("dd-MMM-yyyy"));
            }
            return View();
        }

        public ActionResult TaxReturns()
        {
            return View();
        }

        // Chart of Accounts
        public ActionResult ChartAccounts()
        {
            return Redirect("~/Reporting/ChartAccounts.aspx");

        }

        //Journal Audit
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult JournalAudit([Bind(Include = "From,To,DT")] PostingD registerreport)
        {

            if (ModelState.IsValid)
            {
                if (registerreport.DT == "All")
                {
                    return Redirect("~/Reporting/JournalAudit.aspx?From=" + registerreport.From.ToString("dd-MMM-yyyy") + "&To=" + registerreport.To.ToString("dd-MMM-yyyy") + "&DT=" + registerreport.DT.ToString());

                }
                else
                {
                    return Redirect("~/Reporting/JournalAuditJ.aspx?From=" + registerreport.From.ToString("dd-MMM-yyyy") + "&To=" + registerreport.To.ToString("dd-MMM-yyyy") + "&Account=" + registerreport.DT.ToString());

                }
            }
            ViewBag.DT = regionF(registerreport.DT, registerreport.DT);
            return View();
        }

        public ActionResult JournalAudit()
        {
            ViewBag.DT = regionF();
            return View();
        }


        //New Income Statement



        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AgeReport([Bind(Include = "From,To")] Posting registerreport)
        {

            if (ModelState.IsValid)
            {
                return Redirect("~/Reporting/AgeReports.aspx?From=" + registerreport.From.ToString("dd-MMM-yyyy"));
            }
            return View();
        }

        public ActionResult AgeReport()
        {
            return View();
        }

        //Create Charges Report 

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChargesReport([Bind(Include = "From,To")] Posting registerreport)
        {

            if (ModelState.IsValid)
            {
                return Redirect("~/Reporting/ChargesReport.aspx?From=" + registerreport.From.ToString("dd-MMM-yyyy") + "&To=" + registerreport.To.ToString("dd-MMM-yyyy"));
            }
            return View();
        }

        public ActionResult ChargesReport()
        {
            return View();
        }

        //General Ledger Statement

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GeneralLedger([Bind(Include = "From,To")] Posting registerreport)
        {

            if (ModelState.IsValid)
            {
                return Redirect("~/Reporting/GeneralLedger.aspx?From=" + registerreport.From.ToString("dd-MMM-yyyy") + "&To=" + registerreport.To.ToString("dd-MMM-yyyy"));
            }
            return View();
        }

        public ActionResult GeneralLedger()
        {
            return View();
        }




        //Audit Trail
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FinAuditTrailer([Bind(Include = "From,To")] Posting registerreport)
        {

            if (ModelState.IsValid)
            {
                return Redirect("~/Reporting/FinAuditTrail.aspx?From=" + registerreport.From.ToString("dd-MMM-yyyy") + "&To=" + registerreport.To.ToString("dd-MMM-yyyy"));
            }
            return View();
        }

        public ActionResult FinAuditTrailer()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GeneralLedgerDetailed([Bind(Include = "From,To")] Posting registerreport)
        {

            if (ModelState.IsValid)
            {
                return Redirect("~/Reporting/GeneralLedgerUn.aspx?From=" + registerreport.From.ToString("dd-MMM-yyyy") + "&To=" + registerreport.To.ToString("dd-MMM-yyyy"));
            }
            return View(); 
        }

        public ActionResult GeneralLedgerDetailed()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AccountAudit([Bind(Include = "From,To")] Posting registerreport)
        {

            if (ModelState.IsValid)
            {
                return Redirect("~/Reporting/AccAudit.aspx?From=" + registerreport.From.ToString("dd-MMM-yyyy") + "&To=" + registerreport.To.ToString("dd-MMM-yyyy"));
            }
            return View();
        }

        public ActionResult AccountAudit()
        {
            return View();
        }

        //New Trial Balance
        public ActionResult FinTrialBalance()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FinTrialBalance([Bind(Include = "DateHeld")] TrialBalance registerreport)
        {

            if (ModelState.IsValid)
            {
                return Redirect("~/Reporting/FinTrialBalance.aspx?id=" + registerreport.DateHeld.ToString("dd-MMM-yyyy"));
            }
            return View();
        }

        public ActionResult FinIncomeStatement()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FinIncomeStatement([Bind(Include = "From,To")] Posting registerreport)
        {

            if (ModelState.IsValid)
            {
                return Redirect("~/Reporting/FinIncomeStatement.aspx?date=" + registerreport.From.ToString("dd-MMM-yyyy") + "&date2=" + registerreport.To.ToString("dd-MMM-yyyy"));
            }
            return View();
        }

        public ActionResult FinBalanceSheet()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FinBalanceSheet([Bind(Include = "DateHeld")] TrialBalance registerreport)
        {

            if (ModelState.IsValid)
            {
                return Redirect("~/Reporting/FinBalanceSheet.aspx?id=" + registerreport.DateHeld.ToString("dd-MMM-yyyy"));
            }
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UploadedOrder([Bind(Include = "From,To")] Posting registerreport)
        {

            if (ModelState.IsValid)
            {
                return Redirect("~/Reporting/UploadedDeals.aspx?date=" + registerreport.From.ToString("dd-MMM-yyyy") + "&date2=" + registerreport.To.ToString("dd-MMM-yyyy"));
            }
            return View();
        }

        public ActionResult UploadedOrder()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult MatchedDealsM([Bind(Include = "From,To")] Posting registerreport)
        {

            if (ModelState.IsValid)
            {
                return Redirect("~/Reporting/MatchedDealsm.aspx?From=" + registerreport.From.ToString("dd-MMM-yyyy") + "&To=" + registerreport.To.ToString("dd-MMM-yyyy"));
            }
            return View();
        }

        public ActionResult MatchedDealsM()
        {
            return View();
        }

        public ActionResult AuditMaster()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AuditMaster([Bind(Include = "From,To")] Posting registerreport)
        {

            if (ModelState.IsValid)
            {
                return Redirect("~/Reporting/AuditMasterPage.aspx?From=" + registerreport.From.ToString("dd-MMM-yyyy") + "&To=" + registerreport.To.ToString("dd-MMM-yyyy"));
            }
            return View();
        }

      

        public List<SelectListItem> regionD()
        {


            List<SelectListItem> phy = new List<SelectListItem>();
            phy.Add(new SelectListItem { Text = "Date Posted", Value = "Date Posted" });

            phy.Add(new SelectListItem { Text = "Settlement Date", Value = "Settlement Date" });

            return phy;
        }
        public List<SelectListItem> regionD(string m, string n)
        {
        

        List<SelectListItem> phy = new List<SelectListItem>();
            phy.Add(new SelectListItem { Text = m, Value = n });

            phy.Add(new SelectListItem { Text = "Date Posted", Value = "Date Posted" });

            phy.Add(new SelectListItem { Text = "Settlement Date", Value = "Settlement Date" });



            return phy;
        }

        public List<SelectListItem> regionF()
        {


            List<SelectListItem> phy = new List<SelectListItem>();
            phy.Add(new SelectListItem { Text = "All", Value = "All" });

            var c = db.Accounts_Masters.ToList();
            foreach (var p in c)
            {
                phy.Add(new SelectListItem { Text = p.AccountName + ":" + p.AccountNumber.ToString(), Value = p.AccountNumber.ToString() });

            }

            return phy;
        }
        public List<SelectListItem> regionF(string m, string n)
        {


            List<SelectListItem> phy = new List<SelectListItem>();
            phy.Add(new SelectListItem { Text = m, Value = n });

            phy.Add(new SelectListItem { Text = "All", Value = "All" });
            var c = db.Accounts_Masters.ToList();
            foreach (var p in c)
            {
                phy.Add(new SelectListItem { Text = p.AccountName + ":" + p.AccountNumber.ToString(), Value = p.AccountNumber.ToString() });

            }


            return phy;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult BankRec([Bind(Include = "To,DT")] PostingW registerreport)
        {

            if (ModelState.IsValid)
            {

                return Redirect("~/Reporting/BankRecJ.aspx?To=" + registerreport.To.ToString("dd-MMM-yyyy") + "&Account=" + registerreport.DT.ToString());

            }
            ViewBag.DT = regionQ(registerreport.DT, registerreport.DT);
            return View();
        }

        public ActionResult BankRec()
        {
            ViewBag.DT = regionQ();
            return View();
        }
        public List<SelectListItem> regionQ()
        {


            List<SelectListItem> phy = new List<SelectListItem>();
            var c = db.Accounts_Masters.ToList();
            foreach (var p in c)
            {
                phy.Add(new SelectListItem { Text = p.AccountName + ":" + p.AccountNumber.ToString(), Value = p.AccountNumber.ToString() });

            }

            return phy;
        }
        public List<SelectListItem> regionQ(string m, string n)
        {


            List<SelectListItem> phy = new List<SelectListItem>();
            phy.Add(new SelectListItem { Text = m, Value = n });


            var c = db.Accounts_Masters.ToList();
            foreach (var p in c)
            {
                phy.Add(new SelectListItem { Text = p.AccountName + ":" + p.AccountNumber.ToString(), Value = p.AccountNumber.ToString() });

            }


            return phy;
        }
        public ActionResult MonetaryBalance()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult MonetaryBalance([Bind(Include = "DateHeld")] TrialBalance registerreport)
        {

            if (ModelState.IsValid)
            {
                return Redirect("~/Reporting/MonetaryBalance.aspx?id=" + registerreport.DateHeld.ToString("dd-MMM-yyyy"));
            }
            return View();
        }
    }
}