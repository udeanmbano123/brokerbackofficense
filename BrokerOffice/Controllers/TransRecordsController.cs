﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BrokerOffice.DAO;
using BrokerOffice.Models;
using BrokerOffice.DAO.security;
using PagedList;

namespace BrokerOffice.Controllers
{// [CustomAuthorize(RolesConfigKey = "RolesConfigKey")]
 // [CustomAuthorize(UsersConfigKey = "UsersConfigKey")]
    [CustomAuthorize(Roles = "User,Approver,BrokerAdmin,BrokerAdmin2", NotifyUrl = " /UnauthorizedPage")]
    // [CustomAuthorize(Users = "1,2")]
    public class TransRecordsController : Controller
    {
        private SBoardContext db = new SBoardContext();

        // GET: TransRecords
        public async Task<ActionResult> Index(string sortOrder, int? page)
        {
            ViewBag.TransSortParm = String.IsNullOrEmpty(sortOrder) ? "Date" : "";
            ViewBag.AccountSortParm = String.IsNullOrEmpty(sortOrder) ? "module" : "";
            var per = from s in db.Transs
                      select s;
            switch (sortOrder)
            {
                case "Date":
                    per = per.OrderByDescending(s => s.TrxnDate);
                    break;

                case "module":
                    per = per.OrderBy(s => s.Account);
                    break;

                default:
                    per = per.OrderBy(s => s.TransID);
                    break;
            }
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(per.ToPagedList(pageNumber, pageSize));
          //return View(await db.Transs.ToListAsync());
        }

        // GET: TransRecords/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Trans trans = await db.Transs.FindAsync(id);
            if (trans == null)
            {
                return HttpNotFound();
            }
            return View(trans);
        }

      

        // GET: TransRecords/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Trans trans = await db.Transs.FindAsync(id);
            if (trans == null)
            {
                return HttpNotFound();
            }
            return View(trans);
        }

        // POST: TransRecords/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Trans trans = await db.Transs.FindAsync(id);
            db.Transs.Remove(trans);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
