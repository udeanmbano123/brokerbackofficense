﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BrokerOffice.DAO;
using BrokerOffice.DealClass;
using BrokerOffice.DAO.security;
using WebMatrix.WebData;
using PagedList;
using System.Data.Entity.Migrations;
using System.Transactions;
using BrokerOffice.DealNotes;
using BrokerOffice.Models;
using System.Globalization;

namespace BrokerOffice.Controllers
{
    [CustomAuthorize(Roles = "User,Approver,BrokerAdmin", NotifyUrl = " /UnauthorizedPage")]

    public class BrokersNotesAuthController : Controller
    {
        private SBoardContext db = new SBoardContext();

        // GET: BrokersNotesAuth
        public async Task<ActionResult> Index(string sortOrder, string currentFilter, string
  searchString, string Date1String, string Date2String, int? page)
        {
            var per = from s in db.DealerDGs2
                      where s.Status=="TOUPDATE" ||s.Status=="DELETE"|| s.Status=="TOPRINT" || s.Status == "TOPRINTU"
                      select s;
            try
            {
                
                ViewBag.CurrentSort = sortOrder;
                ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "accountname1" : "";
                ViewBag.DealSortParm = String.IsNullOrEmpty(sortOrder) ? "deal" : "";
                ViewBag.NameSortParam2 = String.IsNullOrEmpty(sortOrder) ? "accountname2" : "";
                ViewBag.DateSortParm = String.IsNullOrEmpty(sortOrder) ? "dateposted" : "";
                ViewBag.CDSCSortParm = String.IsNullOrEmpty(sortOrder) ? "cdsc" : "";
                ViewBag.CDSCSortParm2 = String.IsNullOrEmpty(sortOrder) ? "cdsc2" : "";
                per = from s in db.DealerDGs2
                      where s.Status == "TOUPDATE" || s.Status == "DELETE" || s.Status == "TOPRINT" || s.Status == "TOPRINTU"
                      select s;
                if (searchString != null)
                {
                    page = 1;
                }
                else
                {
                    searchString = currentFilter;
                }
                ViewBag.CurrentFilter = searchString;
                if (!String.IsNullOrEmpty(searchString))
                {
                    per = per.Where(s =>
                    (s.AccountName1).ToUpper().Contains(searchString.ToUpper())
                     ||
                      (s.AccountName2).ToUpper().Contains(searchString.ToUpper())
                      ||
                       (s.Account1).ToUpper().Contains(searchString.ToUpper())
                       ||
                     (s.Account2).ToUpper().Contains(searchString.ToUpper())
                     ||
                    (s.Deal).ToUpper().Contains(searchString.ToUpper())
               );
                }

                try
                {
                    Date1String = Request.QueryString["Date1String"].ToString();
                    Date2String = Request.QueryString["Date2String"].ToString();
                    if (!String.IsNullOrEmpty(Date1String) && !String.IsNullOrEmpty(Date2String))
                    {
                        System.Web.HttpContext.Current.Session["Date1"] = Date1String;
                        System.Web.HttpContext.Current.Session["Date2"] = Date2String;
                        ViewBag.dt1 = Date1String;
                        ViewBag.dt2 = Date2String;
                    }
                }
                catch (Exception)
                {


                }
                try
                {
                    if (System.Web.HttpContext.Current.Session["Date1"].ToString() != "" && System.Web.HttpContext.Current.Session["Date2"].ToString() != "")
                    {

                        Date1String = System.Web.HttpContext.Current.Session["Date1"].ToString();
                        Date2String = System.Web.HttpContext.Current.Session["Date2"].ToString();
                        ViewBag.dt1 = Date1String;
                        ViewBag.dt2 = Date2String;
                    }
                }
                catch (Exception)
                {


                }
                if (!String.IsNullOrEmpty(Date1String) && !String.IsNullOrEmpty(Date2String) && String.IsNullOrEmpty(searchString))
                {
                    System.Web.HttpContext.Current.Session["Date1"] = Date1String;
                    System.Web.HttpContext.Current.Session["Date2"] = Date2String;
                    DateTime date = Convert.ToDateTime(Date1String);
                    DateTime date2 = Convert.ToDateTime(Date2String);
                    DateTime date3 = date2.AddDays(1);
                    per = from s in db.DealerDGs2

                          where (s.DatePosted > date && s.DatePosted < date3 && (s.Status == "TOUPDATE" || s.Status == "DELETE"))
                          select s;

                    var cc = per.Count();
                }
                ViewBag.dt1 = Date1String;
                ViewBag.dt2 = Date2String;

                switch (sortOrder)
                {

                    case "accountname1":
                        per = per.OrderByDescending(s => s.AccountName1);
                        break;
                    case "accountname2":
                        per = per.OrderBy(s => s.AccountName2);
                        break;
                    case "deal":
                        per = per.OrderBy(s => s.Deal);
                        break;
                    case "dateposted":
                        per = per.OrderBy(s => s.DatePosted);
                        break;
                    case "cdsc":
                        per = per.OrderBy(s => s.Account1);
                        break;
                  case "cdsc2":
                        per = per.OrderBy(s => s.Account2);
                         break;
                default:
                        per = per.OrderBy(s => s.ID);
                        break;
                }



            }
            catch (Exception)
            {


            }
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            per = per.OrderByDescending(a => a.DatePosted);
            return View(per.ToPagedList(pageNumber, pageSize));
        }

        // GET: BrokersNotesAuth/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DealerDG2 dealerDG2 = await db.DealerDGs2.FindAsync(id);
            if (dealerDG2 == null)
            {
                return HttpNotFound();
            }
            return View(dealerDG2);
        }

        // GET: BrokersNotesAuth/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: BrokersNotesAuth/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,Security,Deal,Quantity,Price,Account1,Account2,DatePosted,SettlementDate,Ack,AccountName1,AccountName2,SecurityName,Exchange,Printed,Status")] DealerDG2 dealerDG2)
        {
            if (ModelState.IsValid)
            {
                db.DealerDGs2.Add(dealerDG2);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(dealerDG2);
        }

        // GET: BrokersNotesAuth/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DealerDG2 dealerDG2 = await db.DealerDGs2.FindAsync(id);
            if (dealerDG2 == null)
            {
                return HttpNotFound();
            }
            return View(dealerDG2);
        }

        // POST: BrokersNotesAuth/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,Security,Deal,Quantity,Price,Account1,Account2,DatePosted,SettlementDate,Ack,AccountName1,AccountName2,SecurityName,Exchange,Printed,Status")] DealerDG2 dealerDG2)
        {
            if (ModelState.IsValid)
            {
                db.Entry(dealerDG2).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(dealerDG2);
        }

        // GET: BrokersNotesAuth/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DealerDG2 dealerDG2 = await db.DealerDGs2.FindAsync(id);
            if (dealerDG2 == null)
            {
                return HttpNotFound();
            }
            return View(dealerDG2);
        }

        // POST: BrokersNotesAuth/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            // using (TransactionScope transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            //{
            //try
            //{
                DealerDG2 dealerDG2 = await db.DealerDGs2.FindAsync(id);

                //remove from DealerDG
                //var remove = db.DealerDGs.ToList().Where(a => a.Deal == dealerDG2.Deal);
                //foreach (var detail in remove)
                //{
                //    DealerDG user = db.DealerDGs.Find(detail.ID);
                //    db.DealerDGs.Remove(user);
                //    db.SaveChanges(WebSecurity.CurrentUserName);
                //}
            int shrub = db.Database.ExecuteSqlCommand("Delete from DealerDG where Deal='" + dealerDG2.Deal + "' ");

            //remove tblmatched deals
            //var remove2 = db.TblDealss.ToList().Where(a => a.Deal == dealerDG2.Deal);
            //    foreach (var detail in remove2)
            //    {
            //        ATS.Tbl_MatchedDeals user = db.TblDealss.Find(detail.ID);
            //        db.TblDealss.Remove(user);
            //        db.SaveChanges(WebSecurity.CurrentUserName);
            //    }
            int shrub2 = db.Database.ExecuteSqlCommand("Delete from Tbl_MatchedDeals where Deal='" + dealerDG2.Deal + "' ");

            //remove from transactioncharges
            //var remove3 = db.TransactionCharges.ToList().Where(a => a.Transcode == dealerDG2.Deal);
            //    foreach (var detail in remove3)
            //    {
            //        TransactionCharges user = db.TransactionCharges.Find(detail.Id);
            //        db.TransactionCharges.Remove(user);
            //        db.SaveChanges(WebSecurity.CurrentUserName);
            //    }
            int shrub3 = db.Database.ExecuteSqlCommand("Delete from TransactionCharges where transcode='" + dealerDG2.Deal + "' ");

            //remove from trans
            //var remove4 = db.Transs.ToList().Where(a => a.Reference_Number == dealerDG2.Deal);
            //    foreach (var detail in remove4)
            //    {
            //        Trans user = db.Transs.Find(detail.TransID);
            //        db.Transs.Remove(user);
            //        db.SaveChanges(WebSecurity.CurrentUserName);
            //    }
            int shrub4 = db.Database.ExecuteSqlCommand("Delete from Trans where Reference_Number='" + dealerDG2.Deal + "' ");



            //db.DealerDGs2.Remove(dealerDG2);
            //  await db.SaveChangesAsync(WebSecurity.CurrentUserName);
            int shrub5 = db.Database.ExecuteSqlCommand("Delete from DealerDG2 where Deal='" + dealerDG2.Deal + "' ");

            System.Web.HttpContext.Current.Session["NOT"] = "You have successfully deleted the Deal from Broker Office " + dealerDG2.Deal;

                // transactionScope.Complete();

                //  }
                //catch (TransactionException ex)
                //{
                //  transactionScope.Dispose();
                //}
            //}
              //    }
               
            return RedirectToAction("Index");
        }

        // GET: Discard Information
        public async Task<ActionResult> Discard(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DealerDG2 dealerDG2 = await db.DealerDGs2.FindAsync(id);
            if (dealerDG2 == null)
            {
                return HttpNotFound();
            }
            return View(dealerDG2);
        }

        // POST: BrokersNotesAuth/Delete/5
        [HttpPost, ActionName("Discard")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DiscardConfirmed(int id)
        {
            //using (TransactionScope transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            //{
              //  try
                //{
                    DealerDG2 dealerDG2 = await db.DealerDGs2.FindAsync(id);

                    //grab details from DealerDG
                    var dd = db.DealerDGs.ToList().Where(a=>a.Deal== dealerDG2.OldDeal);
                    foreach (var p in dd)
                    {
                        dealerDG2.Account1 = p.Account1;
                        dealerDG2.Account2 = p.Account2;
                        dealerDG2.AccountName1 = p.AccountName1;
                        dealerDG2.AccountName2 = p.AccountName2;
                        dealerDG2.Ack = p.Ack;
                        dealerDG2.DatePosted = Convert.ToDateTime(p.DatePosted);
                        dealerDG2.SettlementDate = p.SettlementDate;
                        dealerDG2.Deal = p.Deal;
                        dealerDG2.Price = p.Price;
                        dealerDG2.Quantity = p.Quantity;
                        dealerDG2.Security = p.Security;
                        dealerDG2.SecurityName = p.SecurityName;
                        dealerDG2.Exchange = p.Exchange;
                        dealerDG2.Printed = p.Printed;
                        dealerDG2.Status = "DONE";
                        dealerDG2.OldDeal = p.Deal;
                        dealerDG2.SettDate = p.SettDate;
                        dealerDG2.PostDate = dealerDG2.DatePosted;
                    }


                    db.DealerDGs2.Remove(dealerDG2);
                    await db.SaveChangesAsync(WebSecurity.CurrentUserName);
                    System.Web.HttpContext.Current.Session["NOT"] = "You have successfully reverted changes for Broker Note " + dealerDG2.Deal;

                  //  transactionScope.Complete();

                //}
                //catch (TransactionException ex)
                //{
                  //  transactionScope.Dispose();
                //}
            //}


            return RedirectToAction("Index");
        }

        public async Task<ActionResult> Update(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DealerDG2 dealerDG2 = await db.DealerDGs2.FindAsync(id);
            if (dealerDG2 == null)
            {
                return HttpNotFound();
            }
            return View(dealerDG2);
        }
        public DateTime AddBusinessDays(DateTime dateTime, int nDays)
        {
            var wholeWeeks = nDays / 5; //since nDays does not include weekdays every week is considered as 5 days
            var absDays = Math.Abs(nDays);
            var remaining = absDays % 5; //results in the number remaining days to add or substract excluding the whole weeks
            var direction = nDays / absDays;//results in 1 if nDays is posisive or -1 if it's negative
            while (dateTime.DayOfWeek == DayOfWeek.Saturday || dateTime.DayOfWeek == DayOfWeek.Sunday)
                dateTime = dateTime.AddDays(direction); //If we are already in a weekend, get out of it
            while (remaining-- > 0)
            {//add remaining days...
                dateTime = dateTime.AddDays(direction);
                if (dateTime.DayOfWeek == DayOfWeek.Saturday)
                    dateTime = dateTime.AddDays(direction * 2);//...skipping weekends
            }
            return dateTime.AddDays(wholeWeeks * 7); //Finally add the whole weeks as 7 days, thus skipping the weekends without checking for DayOfWeek
        }
        // POST: BrokersNotesAuth/Delete/5
        [HttpPost, ActionName("Update")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> UpdateConfirmed(int id)
        {
            //using (TransactionScope transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            //{
              //  try
                //{
                    DealerDG2 dealerDG2 = await db.DealerDGs2.FindAsync(id);

                    //update dealerDG
                    var dg = db.DealerDGs.ToList().Where(a=>a.Deal== dealerDG2.Deal);
            //update tblMatchedDeals
            int old = 0;
            foreach (var pp in dg)
                    {
                        DealerDG myy = db.DealerDGs.Find(pp.ID);
                old = Convert.ToInt32(pp.DealNumber);
                        myy.Account1 = dealerDG2.Account1;
                        myy.Account2 = dealerDG2.Account2;
                        myy.AccountName1 = dealerDG2.AccountName1;
                        myy.AccountName2 = dealerDG2.AccountName2;
                        myy.Ack = dealerDG2.Ack;
                        myy.DatePosted = dealerDG2.DatePosted.ToString("yyyy-MM-dd");
                        myy.SettlementDate = AddBusinessDays(dealerDG2.DatePosted, 3).ToString("yyyy-MM-dd");
                        myy.Deal = dealerDG2.Deal;
                        myy.Price = dealerDG2.Price;
                        myy.Quantity = dealerDG2.Quantity;
                        myy.Security = dealerDG2.Security;
                        myy.SecurityName = dealerDG2.SecurityName;
                        myy.Exchange = dealerDG2.Exchange;
                        myy.Printed = dealerDG2.Printed;
                        myy.PostDate = dealerDG2.DatePosted;
                        myy.SettDate = AddBusinessDays(dealerDG2.DatePosted, 3);
                        myy.Status = myy.Status;
                myy.OrderNumber = myy.OrderNumber;
                        db.DealerDGs.AddOrUpdate(myy);
                        db.SaveChanges(WebSecurity.CurrentUserName);
                    }
                    //remove to transaction charges and trans
                    //update tblmatcheddeals
                    var mg = db.TblDealss.ToList().Where(a => a.Deal == dealerDG2.Deal);
                    //update tblMatchedDeals
                    foreach (var pp in mg)
                    {
                        ATS.Tbl_MatchedDeals my = db.TblDealss.Find(pp.ID);
                        my.Deal = dealerDG2.Deal;
                        my.BuyCompany = dealerDG2.Security;
                        my.SellCompany = dealerDG2.Security;
                        my.Buyercdsno = dealerDG2.Account1;
                        my.Sellercdsno = dealerDG2.Account2;
                        my.Quantity = Convert.ToDecimal(dealerDG2.Quantity);
                        my.Trade = dealerDG2.DatePosted;
                        my.DealPrice = Convert.ToDecimal(dealerDG2.Price);
                        my.DealFlag = "C";
                        my.Instrument = "NULL";
                        my.Affirmation = false;
                        my.buybroker = dealerDG2.AccountName1;
                        my.sellbroker = dealerDG2.AccountName2;
                        try
                        {
                            my.RefID = my.RefID;
                        }
                        catch (Exception)
                        {

                            my.RefID = 0;
                        }
                        my.BrokerCode = my.BrokerCode;
                        my.exchange =dealerDG2.Exchange;
                        db.TblDealss.AddOrUpdate(my);
                        db.SaveChanges(WebSecurity.CurrentUserName);

                    }

            //remove from transactioncharges
            int shrub = db.Database.ExecuteSqlCommand("Delete from TransactionCharges where transcode='" + dealerDG2.Deal + "' ");



            //remove from trans
            int minev = db.Database.ExecuteSqlCommand("Delete from Trans where Reference_Number='" + dealerDG2.Deal + "' ");

            //repost to transaction charges and trans
            int? qty =Convert.ToInt32(dealerDG2.Quantity); int? qty2 = Convert.ToInt32(dealerDG2.Quantity);
                    decimal? buys = Convert.ToDecimal(dealerDG2.Price)*qty; decimal? sells = Convert.ToDecimal(dealerDG2.Price)*qty2;
                    string buyc = dealerDG2.AccountName1; string sellc = dealerDG2.AccountName2; string buyer = dealerDG2.Account1; string seller = dealerDG2.Account2;
                    string sec =dealerDG2.Security;
                    string sec2 =dealerDG2.Security;
                    decimal? s = 0;
            decimal? buysP = Convert.ToDecimal(dealerDG2.Price);

            try
            {
                        s = (decimal.Parse(sells.ToString(), CultureInfo.InvariantCulture));

                    }
                    catch (Exception)
                    {

                        s = 0;
                    }
                    decimal? b = s;
                    var p = db.TradingChargess.ToList();
                    foreach (var z in p)
                    {
                        TransactionCharges myyc = new TransactionCharges();

                        myyc.Transcode = dealerDG2.Deal;
                        myyc.ChargeCode = z.chargecode;
                        if (z.chargecode.ToLower().Replace(" ", "") == ("Capital gains with holding Tax").ToLower().Replace(" ", ""))
                        {

                            myyc.BuyCharges = 0;
                        }
                        else
                        {
                            if (z.chargecode.ToLower().Replace(" ", "") == ("Capital gains with holding Tax").ToLower().Replace(" ", ""))
                            {
                                myyc.BuyCharges = 0;
                            }
                            else
                            {
                                myyc.BuyCharges = b * Convert.ToDecimal((z.chargevalue / 100));

                            }



                        }
                        if (z.chargecode.ToLower().Replace(" ", "") == ("Stamp duty").ToLower().Replace(" ", ""))
                        {
                            myyc.SellCharges = 0;

                        }
                        else
                        {
                            if (isTaxable(seller) == false && z.chargecode.ToLower().Replace(" ", "") == ("Capital gains with holding Tax").ToLower().Replace(" ", ""))
                            {
                                myyc.SellCharges = 0;
                            }
                            else
                            {

                                myyc.SellCharges = b * Convert.ToDecimal((z.chargevalue / 100));
                            }
                         }
                        string dgg = qty + " " + sec + " @ " + buysP;
                        myyc.Date = dealerDG2.DatePosted;
                if (chkclient(buyer) == true && chkclient(seller) == false)
                {
                    myyc.SellCharges = 0;
                }

                if (chkclient(seller) == true && chkclient(buyer) == false)
                {
                    myyc.BuyCharges = 0;
                }
                      db.TransactionCharges.Add(myyc);
                        db.SaveChanges(WebSecurity.CurrentUserName);
                  
                postTransCharge(myyc.BuyCharges, myyc.SellCharges, myyc.ChargeCode, myyc.Transcode, myyc.Date.ToString(), dgg);


                    }
                    //trans
                    //Post to accounts
                    string desc = qty + " " + sec + " @ " + b;
                    decimal? total = 0;
                    decimal? total2 = 0;
                    //insert charge to Trans
                    var zzz = db.TransactionCharges.ToList().Where(a => a.Transcode == dealerDG2.Deal);
                    foreach (var f in zzz)
                    {

                        total += f.BuyCharges;


                        total2 += f.SellCharges;




                    }
            string buy = "";
            string buyaacount = "";
            string sell = "";
            string sellacount = "";
            string buyA = "";
            string buyaacountA = "";
            string sellA = "";
            string sellacountA = "";
            var pk = db.WindowsServices.ToList().Where(a => a.Action == "BUY");
            foreach (var ppc in pk)
            {
                buy = ppc.AccountName;
                buyaacount = ppc.AccountNumber;
            }
            var tk = db.WindowsServices.ToList().Where(a => a.Action == "SELL");
            foreach (var u in tk)
            {
                sell = u.AccountName;
                sellacount = u.AccountNumber;
            }
            var pkA = db.WindowsServices.ToList().Where(a => a.Action == "BUYA");
            foreach (var ppc in pkA)
            {
                buyA = ppc.AccountName;
                buyaacountA = ppc.AccountNumber;
            }
            var tkA = db.WindowsServices.ToList().Where(a => a.Action == "SELLA");
            foreach (var u in tkA)
            {
                sellA = u.AccountName;
                sellacountA = u.AccountNumber;
            }

                    decimal deals = 0;
                    decimal? bb = 0;
                    decimal? ss = 0;

                    //deals = Convert.ToDecimal((Math.Round(Convert.ToDouble(ppp.Price), 4) * Math.Round(Convert.ToDouble(ppp.Quantity), 4)));
                    deals = (decimal.Parse(buys.ToString(), CultureInfo.InvariantCulture));
                    bb = b + total;
                    ss = b - total2;
                    buys= (decimal.Parse(dealerDG2.Price.ToString(), CultureInfo.InvariantCulture));

            if (chkclient(buyer)==true && chkclient(seller)==true)
            {
                Trans my2 = new Trans();


                //debit account number


                my2.Account = buyer;
                if (my2.Account == null || my2.Account == "")
                {
                    my2.Account = "NSE";
                }
                my2.Category = "Purchase " + qty + " " + sec + " @ " + buys;
                my2.Credit = 0;
                my2.Debit = Buy(dealerDG2.Deal);
                my2.Narration = "Purchase " + qty + " " + sec + " @ " + buys;
                my2.Reference_Number = dealerDG2.Deal;
                my2.TrxnDate = dealerDG2.DatePosted;
                my2.Post = dealerDG2.DatePosted;
                my2.Type = "Purchase " + qty + " " + sec + " @ " + buysP;
                my2.PostedBy = "Service/Total Buy Charges" + total.ToString() + "," + deals;
                db.Transs.Add(my2);
                //Update table  
                db.SaveChanges(WebSecurity.CurrentUserName);

                //update Acc Receivable
                my2.Account = buyaacount;
                my2.Category = "Purchase " + qty + " " + sec + " @ " + buysP;
                my2.Credit = 0;
                my2.Debit = Buy(dealerDG2.Deal);
                my2.Narration = "Purchase " + qty + " " + sec + " @ " + buysP;
                my2.Reference_Number = dealerDG2.Deal;
                my2.TrxnDate = dealerDG2.DatePosted;
                my2.Post = dealerDG2.DatePosted;
                my2.Type = "Purchase " + qty + " " + sec + " @ " + buysP;
                my2.PostedBy = "Service /Total Buy Charges" + total.ToString() + "," + deals;
                db.Transs.Add(my2);


                db.SaveChanges(WebSecurity.CurrentUserName);

                Trans my4 = new Trans();
                //debit account number

                my4.Account = seller;

                if (my4.Account == "")
                {
                    my4.Account = "NSE";
                }
                my4.Category = "Sell " + qty + " " + sec + " @ " + buysP;
                my4.Credit = Sell(dealerDG2.Deal);
                my4.Debit = 0;
                my4.Narration = "Sell " + qty + " " + sec + " @ " + buysP;
                my4.Reference_Number = dealerDG2.Deal;
                my4.TrxnDate = dealerDG2.DatePosted;
                my4.Post = dealerDG2.DatePosted;
                // my2.Type = "Sell Deal";
                my4.Type = "Sell " + qty + " " + sec + " @ " + buysP;
                my4.PostedBy = "Service/Total Sell Charges" + total2.ToString() + "," + deals;
                db.Transs.Add(my4);
                db.SaveChanges(WebSecurity.CurrentUserName);


                my4.Account = sellacount;
                my4.Category = "Sell " + qty + " " + sec + " @ " + buysP;
                my4.Credit = Sell(dealerDG2.Deal);
                my4.Debit = 0;
                my4.Narration = "Sell " + qty + " " + sec + " @ " + buysP;
                my4.Reference_Number = dealerDG2.Deal;
                my4.TrxnDate = dealerDG2.DatePosted;
                my4.Post = dealerDG2.DatePosted;
                // my2.Type = "Sell Deal";
                my4.Type = "Sell " + qty + " " + sec + " @ " + buysP;
                my4.PostedBy = "Service/Total Sell Charges" + total2.ToString() + "," + deals;
                db.Transs.Add(my4);
                db.SaveChanges(WebSecurity.CurrentUserName);

            }
            if (chkclient(buyer)==true && chkclient(seller)==false)
            {
                Trans my2 = new Trans();


                //debit account number


                my2.Account = buyer;
                if (my2.Account == null || my2.Account == "")
                {
                    my2.Account = "NSE";
                }
                my2.Category = "Purchase " + qty + " " + sec + " @ " + buysP;
                my2.Credit = 0;
                my2.Debit = Buy(dealerDG2.Deal);
                my2.Narration = "Purchase " + qty + " " + sec + " @ " + buysP;
                my2.Reference_Number = dealerDG2.Deal;
                my2.TrxnDate = dealerDG2.DatePosted;
                my2.Post = dealerDG2.DatePosted;
                my2.Type = "Purchase " + qty + " " + sec + " @ " + buysP;
                my2.PostedBy = "Service/Total Buy Charges" + total.ToString() + "," + deals;
                db.Transs.Add(my2);
                //Update table  
                db.SaveChanges(WebSecurity.CurrentUserName);

                //update Acc Receivable
                my2.Account = buyaacount;
                my2.Category = "Purchase " + qty + " " + sec + " @ " + buysP;
                my2.Credit = 0;
                my2.Debit = Buy(dealerDG2.Deal);
                my2.Narration = "Purchase " + qty + " " + sec + " @ " + buysP;
                my2.Reference_Number = dealerDG2.Deal;
                my2.TrxnDate = dealerDG2.DatePosted;
                my2.Post = dealerDG2.DatePosted;
                my2.Type = "Purchase " + qty + " " + sec + " @ " + buysP;
                my2.PostedBy = "Service /Total Buy Charges" + total.ToString() + "," + deals;
                db.Transs.Add(my2);


                db.SaveChanges(WebSecurity.CurrentUserName);
                //credit bramount
                my2.Account = buyaacountA;
                my2.Category = "Purchase " + qty + " " + sec + " @ " + buysP;
                my2.Credit = BR(dealerDG2.Deal);
                my2.Debit = 0;
                my2.Narration = "Purchase " + qty + " " + sec + " @ " + buysP;
                my2.Reference_Number = dealerDG2.Deal;
                my2.TrxnDate = dealerDG2.DatePosted;
                my2.Post = dealerDG2.DatePosted;
                my2.Type = "Purchase " + qty + " " + sec + " @ " + buysP;
                my2.PostedBy = "Service /Total Buy Charges" + total.ToString() + "," + deals;
                db.Transs.Add(my2);


                db.SaveChanges(WebSecurity.CurrentUserName);
            }
            //debit account number
            if (chkclient(seller)==true && chkclient(buyer)==false)
             {
                Trans my4 = new Trans();

                //debit account number

                my4.Account = seller;

                if (my4.Account == "")
                {
                    my4.Account = "NSE";
                }
                my4.Category = "Sell " + qty + " " + sec + " @ " + buysP;
                my4.Credit = Sell(dealerDG2.Deal);
                my4.Debit = 0;
                my4.Narration = "Sell " + qty + " " + sec + " @ " + buysP;
                my4.Reference_Number = dealerDG2.Deal;
                my4.TrxnDate = dealerDG2.DatePosted;
                my4.Post = dealerDG2.DatePosted;
                // my2.Type = "Sell Deal";
                my4.Type = "Sell " + qty + " " + sec + " @ " + buysP;
                my4.PostedBy = "Service/Total Sell Charges" + total2.ToString() + "," + deals;
                db.Transs.Add(my4);
                db.SaveChanges(WebSecurity.CurrentUserName);
                ////accounts payable
                //debit account number
                my4.Account = sellacount;
                my4.Category = "Sell " + qty + " " + sec + " @ " + buysP;
                my4.Credit = Sell(dealerDG2.Deal);
                my4.Debit = 0;
                my4.Narration = "Sell " + qty + " " + sec + " @ " + buysP;
                my4.Reference_Number = dealerDG2.Deal;
                my4.TrxnDate = dealerDG2.DatePosted;
                my4.Post = dealerDG2.DatePosted;
                // my2.Type = "Sell Deal";
                my4.Type = "Sell " + qty + " " + sec + " @ " + buysP;
                my4.PostedBy = "Service/Total Sell Charges" + total2.ToString() + "," + deals;
                db.Transs.Add(my4);
                db.SaveChanges(WebSecurity.CurrentUserName);

                my4.Account = sellacountA;
                my4.Category = "Sell " + qty + " " + sec + " @ " + buysP;
                my4.Credit = 0;
                my4.Debit = BR(dealerDG2.Deal);
                my4.Narration = "Sell " + qty + " " + sec + " @ " + buysP;
                my4.Reference_Number = dealerDG2.Deal;
                my4.TrxnDate = dealerDG2.DatePosted;
                my4.Post = dealerDG2.DatePosted;
                // my2.Type = "Sell Deal";
                my4.Type = "Sell " + qty + " " + sec + " @ " + buysP;
                my4.PostedBy = "Service/Total Sell Charges" + total2.ToString() + "," + deals;
                db.Transs.Add(my4);
                db.SaveChanges(WebSecurity.CurrentUserName);
            }

            dealerDG2.OrderNumber = dealerDG2.OrderNumber;

            dealerDG2.Status = "DONE";
                    dealerDG2.OldDeal=old.ToString();
                    dealerDG2.SettlementDate=AddBusinessDays(dealerDG2.DatePosted, 3).ToString();
                    dealerDG2.PostDate = dealerDG2.DatePosted;
                    dealerDG2.SettDate = AddBusinessDays(dealerDG2.DatePosted, 3);
                    db.DealerDGs2.AddOrUpdate(dealerDG2);
                    await db.SaveChangesAsync(WebSecurity.CurrentUserName);
                    System.Web.HttpContext.Current.Session["NOT"] = "You have successfully updated the Deal from Broker Office " + dealerDG2.Deal;

                  //  transactionScope.Complete();
                //}
                //atch (TransactionException ex)
                //{
                  //  transactionScope.Dispose();
                //}

            //}
            return RedirectToAction("Index");
        }

        public Decimal Buy(string max)
        {
            string vf = "";
            var sf = db.Database.SqlQuery<TR>("SELECT (select cast(Price as numeric(18,4))*cast(Quantity as  numeric(18,4)) from DealerDg where Deal='" + max + "')+sum(BuyCharges) as 'Mo'  FROM TransactionCharges where transcode = '" + max + "'");
            foreach (var p in sf)
            {
                vf = p.Mo.ToString();
            }
            return decimal.Parse(vf, CultureInfo.InvariantCulture);
        }
        public Decimal Sell(string max)
        {
            string vf = "";
            var sf = db.Database.SqlQuery<TR>("SELECT (select cast(Price as numeric(18,4))*cast(Quantity as  numeric(18,4)) from DealerDg where Deal='" + max + "')-sum(SellCharges) as 'Mo'  FROM TransactionCharges where transcode = '" + max + "'");
            foreach (var p in sf)
            {
                vf = p.Mo.ToString();
            }

            return decimal.Parse(vf, CultureInfo.InvariantCulture);
        }
        public Decimal BR(string max)
        {
            string vf = "";
            var sf = db.Database.SqlQuery<TR>("SELECT top 1 (select cast(Price as numeric(18,4))*cast(Quantity as  numeric(18,4)) from DealerDg where Deal='" + max + "') as 'Mo'  FROM TransactionCharges where transcode='" + max + "'");
            foreach (var p in sf)
            {
                vf = p.Mo.ToString();
            }


            return decimal.Parse(vf, CultureInfo.InvariantCulture);
        }
        public Decimal? postT(string max,string trade)
        {
            string vf = "";
            var sf = db.Database.SqlQuery<TR>("select top 1 IsNull(sum([BuyCharges]+[SellCharges]),0) as 'Mo' from TransactionCharges where transcode='" + max + "' and   REPLACE(LOWER(ChargeCode),' ','')=REPLACE(LOWER((select  ChargeCode from tradingcharges where chargeaccountcode=CAST('" + trade +"' as nvarchar))),' ','')");
            foreach (var p in sf)
            {
                vf = p.Mo.ToString();
            }


            return decimal.Parse(vf, CultureInfo.InvariantCulture);
        }
        public bool chkclient(string m)
        {
            bool tru = true;
            var ff = 0;

            try
            {
                ff = db.Account_Creations.ToList().Where(a => a.CDSC_Number == m).Count();

            }
            catch (Exception)
            {

                ff = 0;
                tru = false;
            }
            if (m.ToLower() == "NSE")
            {
                tru = false;
            }
            if (m.ToLower() == "finsec")
            {
                tru = false;
            }

            return tru;

        }

        public Boolean isTaxable(string s)
        {
            Boolean test = false;
            string nm = "";
            var pace = db.Account_Creations.ToList().Where(a => a.CDSC_Number == s);
            foreach (var z in pace)
            {
                nm = z.ClientType;
            }
            if (nm == "Taxable")
            {
                test = true;
            }
            return test;
        }
        public void postTransCharge(decimal? p, decimal? k, string c, string transcode, string st, string vv)
        {
            string buy = "";
            string buyaacount = "";
            var pk = db.WindowsServices.ToList().Where(a => a.Action == "BUY");

            foreach (var t in pk)
            {
                buy = t.AccountName;
                buyaacount = t.AccountNumber;
            }

            var dz = db.TradingChargess.ToList().Where(a => a.ChargeName.ToLower().Replace(" ", "") == c.ToLower().Replace(" ", ""));
            double pin = Math.Round(Convert.ToDouble((p + k)), 4);
            foreach (var q in dz)
            {

                Trans my3 = new Trans();

                my3.Account = q.chargeaccountcode.ToString();
                my3.Category = "Order Posting";
               // my3.Credit = postT(transcode,my3.Account);
                my3.Debit = 0;
                my3.Narration = vv;
                my3.Reference_Number = transcode;
                my3.Credit = postT(my3.Reference_Number, my3.Account);
                my3.TrxnDate = Convert.ToDateTime(st);
                my3.Post = Convert.ToDateTime(st);
                my3.Type = q.chartaccount;
                my3.PostedBy = WebSecurity.CurrentUserName;
                db.Transs.Add(my3);
                db.SaveChanges(WebSecurity.CurrentUserName);
            }


        }

        public async Task<ActionResult> Print(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DealerDG2 dealerDG2 = await db.DealerDGs2.FindAsync(id);
            if (dealerDG2 == null)
            {
                return HttpNotFound();
            }
            return View(dealerDG2);
        }

        // POST: BrokerNotes/Delete/5
        [HttpPost, ActionName("Print")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> PrintConfirmed(int id)
        {
            DealerDG2 dealerDG2 = await db.DealerDGs2.FindAsync(id);
            dealerDG2.Printed = true;
            dealerDG2.Status = "DONE";
            //updatedealerdg
            var dg = db.DealerDGs.ToList().Where(a => a.Deal == dealerDG2.OldDeal);
            //update tblMatchedDeals
            foreach (var pp in dg)
            {
                DealerDG myy = db.DealerDGs.Find(pp.ID);
                myy.Account1 = dealerDG2.Account1;
                myy.Account2 = dealerDG2.Account2;
                myy.AccountName1 = dealerDG2.AccountName1;
                myy.AccountName2 = dealerDG2.AccountName2;
                myy.Ack = dealerDG2.Ack;
                myy.DatePosted = dealerDG2.DatePosted.ToString();
                myy.SettlementDate = AddBusinessDays(dealerDG2.DatePosted, 3).ToString();
                myy.Deal = dealerDG2.Deal;
                myy.Price = dealerDG2.Price;
                myy.Quantity = dealerDG2.Quantity;
                myy.Security = dealerDG2.Security;
                myy.SecurityName = dealerDG2.SecurityName;
                myy.Exchange = dealerDG2.Exchange;
                myy.Printed = dealerDG2.Printed;
                myy.Status = myy.Status;
                db.DealerDGs.AddOrUpdate(myy);
                db.SaveChanges(WebSecurity.CurrentUserName);
            }
            db.DealerDGs2.AddOrUpdate(dealerDG2);
            await db.SaveChangesAsync(WebSecurity.CurrentUserName);
            System.Web.HttpContext.Current.Session["NOT"] = "Broker Note has been marked as printed successfully" + dealerDG2.Deal;

            return RedirectToAction("Index");
        }


        public async Task<ActionResult> UnPrint(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DealerDG2 dealerDG2 = await db.DealerDGs2.FindAsync(id);
            if (dealerDG2 == null)
            {
                return HttpNotFound();
            }
            return View(dealerDG2);
        }

        // POST: BrokerNotes/Delete/5
        [HttpPost, ActionName("UnPrint")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> UnPrintConfirmed(int id)
        {
            DealerDG2 dealerDG2 = await db.DealerDGs2.FindAsync(id);
            dealerDG2.Printed = false;
            dealerDG2.Status = "DONE";
            //updatedealerdg
            var dg = db.DealerDGs.ToList().Where(a => a.Deal == dealerDG2.OldDeal);
            //update tblMatchedDeals
            foreach (var pp in dg)
            {
                DealerDG myy = db.DealerDGs.Find(pp.ID);
                myy.Account1 = dealerDG2.Account1;
                myy.Account2 = dealerDG2.Account2;
                myy.AccountName1 = dealerDG2.AccountName1;
                myy.AccountName2 = dealerDG2.AccountName2;
                myy.Ack = dealerDG2.Ack;
                myy.DatePosted = dealerDG2.DatePosted.ToString();
                myy.SettlementDate = AddBusinessDays(dealerDG2.DatePosted, 3).ToString();
                myy.Deal = dealerDG2.Deal;
                myy.Price = dealerDG2.Price;
                myy.Quantity = dealerDG2.Quantity;
                myy.Security = dealerDG2.Security;
                myy.SecurityName = dealerDG2.SecurityName;
                myy.Exchange = dealerDG2.Exchange;
                myy.Printed = dealerDG2.Printed;
                myy.Status = myy.Status;
                db.DealerDGs.AddOrUpdate(myy);
                db.SaveChanges(WebSecurity.CurrentUserName);
            }
            db.DealerDGs2.AddOrUpdate(dealerDG2);
            await db.SaveChangesAsync(WebSecurity.CurrentUserName);
            System.Web.HttpContext.Current.Session["NOT"] = "Broker Note has been marked as not printed successfully" + dealerDG2.Deal;

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
