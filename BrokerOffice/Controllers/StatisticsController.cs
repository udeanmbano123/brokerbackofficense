﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BrokerOffice.DAO.security;
using BrokerOffice.Models;
using BrokerOffice.DAO;
using WebMatrix.WebData;
using Newtonsoft.Json;
using RestSharp;
using BrokerOffice.CDSMOdel;

namespace BrokerOffice.Controllers
{
    [CustomAuthorize(Roles = "User,Approver,BrokerAdmin", NotifyUrl = " /UnauthorizedPage")]

    public class StatisticsController : Controller
    {
        private SBoardContext db = new SBoardContext();

        public SqlConnection myConnection = new SqlConnection();
        public SqlDataAdapter adp;
        public SqlCommand cmd;
        public SBoardContext vv = new SBoardContext();
        // GET: Statistics

        public ActionResult Index()
        {
            
            ViewBag.Role = Secs();
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index([Bind(Include = "Company")] Company registerreport)
        {

            if (ModelState.IsValid && registerreport.company != "--Select--" && registerreport.company != "All")
            {
               return Redirect("~/Statistics/Company?sec=" + registerreport.company);
            }else
            {
                return Redirect("~/Statistics/CompanyAll?sec=" + registerreport.company);

            }
            ViewBag.Role = null;
            ViewBag.Role = Secs();
            return View();
        }
        public ActionResult Company(string sec)
        {
            string gen = sec;
            var client = new RestClient("http://localhost/BrokerService");
            var request = new RestRequest("Security", Method.GET);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            List<Securities> dataList = JsonConvert.DeserializeObject<List<Securities>>(validate);
            string topo = "";
            var bb = dataList.ToList().Where(a=>a.Company==sec);
            foreach (var po in bb)
            {
                ViewBag.Com = po.Fnam;
                topo = po.Fnam;
            }
          
            var p = vv.Users.ToList().Where(a=>a.Email==WebSecurity.CurrentUserName);
            string nme = "";
            foreach (var c in p)
            {
                nme = c.BrokerCode;
            }
         
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["connpath"].ConnectionString;

            DataSet ds = new DataSet();
            int i = 0;
            cmd = new SqlCommand("declare @custodian nvarchar(50)='"+ nme +"';declare @company nvarchar(50)='"+ sec +"'; select (select count(*) from accounts_clients where custodian=@custodian) AS [TotalRegistration] ,ISNULL((select sum(amount) from tbl_cashbalance where clientid in (select cds_number from accounts_clients where custodian=@custodian)),0) as [totalfunds], (select sum(shares) from trans where cds_number in (select cds_number from accounts_clients where custodian=@custodian) and company=@company) as [totalholdings], 0 AS CDCHOLDINGS, ISNULL((SELECT SUM(quantity) FROM TESTCDS.DBO.LIVETRADINGMASTER  where cds_ac_no in (select cds_number from accounts_clients where custodian=@custodian) and company=@company and OrderType='SELL'),0) AS [Incomingsells],ISNULL((SELECT SUM(quantity) FROM TESTCDS.DBO.LIVETRADINGMASTER  where cds_ac_no in (select cds_number from accounts_clients where custodian=@custodian) and company=@company and OrderType='BUY'),0) AS [Incomingbuys],(  select isnull(sum(tradeqty),0) from tblmatchedorders where banksent='0' and (account1 in (select cds_number from accounts_clients where custodian=@custodian) or account2 in (select cds_number from accounts_clients where custodian=@custodian)) and company=@company) as [Pendingsettlement],(  select isnull(sum(tradeqty),0) from tblmatchedorders where ack='SETTLED' and (account1 in (select cds_number from accounts_clients where custodian=@custodian) or account2 in (select cds_number from accounts_clients where custodian=@custodian)) and company=@company and convert(date,SettlementDate)=convert(date,getdate()))  as [todaysettlement]", myConnection);
            adp = new SqlDataAdapter(cmd);
            adp.Fill(ds, "stats");

            foreach (DataRow myRow in ds.Tables[0].Rows)
            {
                ViewBag.lbltotal_registrations = myRow["TotalRegistration"].ToString();
                ViewBag.lbltotal_funds = "$" + myRow["totalfunds"].ToString();
                ViewBag.lbldepositoryholdings = myRow["CDCHOLDINGS"].ToString();
                ViewBag.lbltotal_holdings = myRow["totalholdings"].ToString();
                ViewBag.lblincomingsells = myRow["Incomingsells"].ToString();
                ViewBag.lblincomingbuys = myRow["Incomingbuys"].ToString();
                ViewBag.lblpendingsettlement = myRow["Pendingsettlement"].ToString();
                ViewBag.lbltodaysettled = myRow["todaysettlement"].ToString();

            }

          
            ViewBag.ListB = db.Account_Creations.ToList().OrderByDescending(a=>a.ID_).Take(7);
            string n1 = "";
            string n2 = "";
            try
            {
                string name = WebSecurity.CurrentUserName;
                var user = db.Users.ToList().Where(a => a.Email == name);
                ViewBag.Users = "";
                foreach (var row in user)
                {
                    ViewBag.Users = row.FirstName + " " + row.LastName;
                }
                var pstord = db.Order_Lives.ToList().Count();

                ViewBag.Orders = pstord.ToString();
                var c = db.Users.ToList().Where(a => a.Email == WebSecurity.CurrentUserName);
                string vx = "";
                foreach (var d in c)
                {
                    vx = d.BrokerCode;
                }
                var list3 = "";
                var client77 = new RestClient("http://localhost/BrokerService");
                var request77 = new RestRequest("BrokersList", Method.GET);
                IRestResponse response77 = client.Execute(request77);
                string validate77 = response77.Content;
                List<Broker> dataList77 = JsonConvert.DeserializeObject<List<Broker>>(validate77);


                var dbsel = from s in dataList77
                            select s;
                //updatebilling();
                foreach (var d in dbsel)
                {
                    if (vx == d.Company_Code)
                    {
                        n1 = d.Company_name;
                        n2 = d.Company_Code;
                    }
                }
            }
            catch (Exception)
            {


            }
            ViewBag.Broker = n1;
            ViewBag.BrokerCode = n2;
            n2 = "AKRI";
            ViewBag.RegC = db.Account_Creations.ToList().Where(a => a.Broker == n2).Count();
            ViewBag.RegA = db.Account_CreationPendingss.ToList().Where(a=>a.update_type == "TOUPDATE").Count();
            int MB1= db.PreOrders.ToList().Where(a => a.OrderType == "BUY"  && a.Company.ToLower().Replace(" ", "") == gen.ToLower().Replace(" ", "") && a.OrderStatus == "MATCHED").Count();

            int MB2 = db.PreOrders.ToList().Where(a => a.OrderType == "BUY"  && a.Company.ToLower().Replace(" ", "") == topo.ToLower().Replace(" ", "") && a.OrderStatus== "MATCHED").Count();
            ViewBag.MB =MB1+MB2;
            int MS1 = db.PreOrders.ToList().Where(a => a.OrderType == "SELL" && a.Company.ToLower().Replace(" ", "") == gen.ToLower().Replace(" ", "") && a.OrderStatus == "MATCHED").Count();

            int MS2 = db.PreOrders.ToList().Where(a => a.OrderType == "SELL" && a.Company.ToLower().Replace(" ", "") == topo.ToLower().Replace(" ", "") && a.OrderStatus == "MATCHED").Count();
            ViewBag.MS = MS1+MS2;
            int SB1 = db.PreOrders.ToList().Where(a => a.OrderType == "BUY" && a.Company.ToLower().Replace(" ", "") == gen.ToLower().Replace(" ", "") && a.OrderStatus == "Settled").Count();

            int SB2 = db.PreOrders.ToList().Where(a => a.OrderType == "BUY" && a.Company.ToLower().Replace(" ", "") == topo.ToLower().Replace(" ", "") && a.OrderStatus == "Settled").Count();

            ViewBag.SB = SB1 + SB2;
            int SS1 = db.PreOrders.ToList().Where(a => a.OrderType == "SELL" && a.Company.ToLower().Replace(" ", "") == gen.ToLower().Replace(" ", "") && a.OrderStatus == "Settled").Count();

            int SS2 = db.PreOrders.ToList().Where(a => a.OrderType == "SELL" && a.Company.ToLower().Replace(" ", "") == topo.ToLower().Replace(" ", "") && a.OrderStatus == "Settled").Count();

            ViewBag.SS = SS1 + SS2;
            int PB1=db.PreOrders.ToList().Where(a => a.OrderType == "BUY" && a.Company.ToLower().Replace(" ", "") == gen.ToLower().Replace(" ", "")).Count();
           
                int PB2= db.PreOrders.ToList().Where(a => a.OrderType == "BUY" && a.Company.ToLower().Replace(" ", "") == topo.ToLower().Replace(" ", "")).Count();

            ViewBag.PB = PB1 + PB2;
            int PS1 = db.PreOrders.ToList().Where(a => a.OrderType == "SELL" && a.Company.ToLower().Replace(" ", "") == gen.ToLower()).Count();

            int PS2 = db.PreOrders.ToList().Where(a => a.OrderType == "SELL" && a.Company.ToLower().Replace(" ", "") == topo.ToLower()).Count();

            ViewBag.PS = PS1 + PS2;

            return View();
        }

        public ActionResult CompanyAll(string sec)
        {
            sec = "All";
            string gen = sec;
            string topo ="All";
            ViewBag.Com = "All";

             var p = vv.Users.ToList().Where(a => a.Email == WebSecurity.CurrentUserName);
            string nme = "";
            foreach (var c in p)
            {
                nme = c.BrokerCode;
            }

            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["connpath"].ConnectionString;

            DataSet ds = new DataSet();
            int i = 0;
            cmd = new SqlCommand("declare @custodian nvarchar(50)='" + nme + "'; select (select count(*) from accounts_clients where custodian=@custodian) AS [TotalRegistration] ,ISNULL((select sum(amount) from tbl_cashbalance where clientid in (select cds_number from accounts_clients where custodian=@custodian)),0) as [totalfunds], (select sum(shares) from trans where cds_number in (select cds_number from accounts_clients where custodian=@custodian)) as [totalholdings], 0 AS CDCHOLDINGS, ISNULL((SELECT SUM(quantity) FROM TESTCDS.DBO.LIVETRADINGMASTER  where cds_ac_no in (select cds_number from accounts_clients where custodian=@custodian) and OrderType='SELL'),0) AS [Incomingsells],ISNULL((SELECT SUM(quantity) FROM TESTCDS.DBO.LIVETRADINGMASTER  where cds_ac_no in (select cds_number from accounts_clients where custodian=@custodian) and OrderType='BUY'),0) AS [Incomingbuys],(  select isnull(sum(tradeqty),0) from tblmatchedorders where banksent='0' and (account1 in (select cds_number from accounts_clients where custodian=@custodian) or account2 in (select cds_number from accounts_clients where custodian=@custodian))) as [Pendingsettlement],(  select isnull(sum(tradeqty),0) from tblmatchedorders where ack='SETTLED' and (account1 in (select cds_number from accounts_clients where custodian=@custodian) or account2 in (select cds_number from accounts_clients where custodian=@custodian)) and convert(date,SettlementDate)=convert(date,getdate()))  as [todaysettlement]", myConnection);
            adp = new SqlDataAdapter(cmd);
            adp.Fill(ds, "stats");

            foreach (DataRow myRow in ds.Tables[0].Rows)
            {
                ViewBag.lbltotal_registrations = myRow["TotalRegistration"].ToString();
                ViewBag.lbltotal_funds = "$" + myRow["totalfunds"].ToString();
                ViewBag.lbldepositoryholdings = myRow["CDCHOLDINGS"].ToString();
                ViewBag.lbltotal_holdings = myRow["totalholdings"].ToString();
                ViewBag.lblincomingsells = myRow["Incomingsells"].ToString();
                ViewBag.lblincomingbuys = myRow["Incomingbuys"].ToString();
                ViewBag.lblpendingsettlement = myRow["Pendingsettlement"].ToString();
                ViewBag.lbltodaysettled = myRow["todaysettlement"].ToString();

            }


            ViewBag.ListB = db.Account_Creations.ToList().OrderByDescending(a => a.ID_).Take(7);
            string n1 = "";
            string n2 = "";
            try
            {
                string name = WebSecurity.CurrentUserName;
                var user = db.Users.ToList().Where(a => a.Email == name);
                ViewBag.Users = "";
                foreach (var row in user)
                {
                    ViewBag.Users = row.FirstName + " " + row.LastName;
                }
                var pstord = db.Order_Lives.ToList().Count();

                ViewBag.Orders = pstord.ToString();
                var c = db.Users.ToList().Where(a => a.Email == WebSecurity.CurrentUserName);
                string vx = "";
                foreach (var d in c)
                {
                    vx = d.BrokerCode;
                }
                var list3 = "";
                

               
                //updatebilling();
                
                        n1 = "Old Mutual Securities";
                        n2 = "OMSEC";
                
                
            }
            catch (Exception)
            {


            }
            ViewBag.Broker = n1;
            ViewBag.BrokerCode = n2;
            n2 = "AKRI";
            ViewBag.RegC = db.Account_Creations.ToList().Where(a => a.Broker == n2).Count();
            ViewBag.RegA = db.Account_CreationPendingss.ToList().Where(a => a.update_type == "TOUPDATE").Count();
            int MB1 = db.PreOrders.ToList().Where(a => a.OrderType == "BUY" && a.OrderStatus == "MATCHED").Count();

            int MB2 = db.PreOrders.ToList().Where(a => a.OrderType == "BUY" && a.OrderStatus == "MATCHED").Count();
            ViewBag.MB = MB1 + MB2;
            int MS1 = db.PreOrders.ToList().Where(a => a.OrderType == "SELL" && a.OrderStatus == "MATCHED").Count();

            int MS2 = db.PreOrders.ToList().Where(a => a.OrderType == "SELL" && a.OrderStatus == "MATCHED").Count();
            ViewBag.MS = MS1 + MS2;
            int SB1 = db.PreOrders.ToList().Where(a => a.OrderType == "BUY" && a.OrderStatus == "Settled").Count();

            int SB2 = db.PreOrders.ToList().Where(a => a.OrderType == "BUY" && a.OrderStatus == "Settled").Count();

            ViewBag.SB = SB1 + SB2;
            int SS1 = db.PreOrders.ToList().Where(a => a.OrderType == "SELL" && a.OrderStatus == "Settled").Count();

            int SS2 = db.PreOrders.ToList().Where(a => a.OrderType == "SELL" && a.OrderStatus == "Settled").Count();

            ViewBag.SS = SS1 + SS2;
            int PB1 = db.PreOrders.ToList().Where(a => a.OrderType == "BUY" ).Count();

            int PB2 = db.PreOrders.ToList().Where(a => a.OrderType == "BUY").Count();

            ViewBag.PB = PB1 + PB2;
            int PS1 = db.PreOrders.ToList().Where(a => a.OrderType == "SELL").Count();

            int PS2 = db.PreOrders.ToList().Where(a => a.OrderType == "SELL").Count();

            ViewBag.PS = PS1 + PS2;

            return View();
        }

        public ActionResult MarketWatch()
        {
            //var client = new RestClient("http://localhost/BrokerService/");
            //var request = new RestRequest("MarketWatch", Method.GET);
            //IRestResponse response = client.Execute(request);
            //string validate = response.Content;
            //List<marketwatch> dataList = JsonConvert.DeserializeObject<List<marketwatch>>(validate);
            //ViewBag.ListB = dataList;
            return Redirect("~/MarketWatch/marketwatcher.aspx");
        }
        public ActionResult MarketWatchDrill(string sec)
        {
           
            return View();
        }
        public List<Brokers> GetEarners(string p)
        {
            //return listEmp.First(e => e.ID == id); 

            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["connpath"].ConnectionString;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = "select cds_number, brokercode,  left(isnull(surname,'')+' '+isnull(forenames,''),20) as [fullname], idnopp, custodian, mobile,CASE WHEN IDtype='NID' Then 'NATIONAL ID' WHEN IDType='National ID' Then 'NATIONAL ID' Else IDtype End as 'IDType',CASE WHEN AccountType='I' THEN 'INDIVIDUAL' WHEN AccountType='J'  THEN 'JOINT' WHEN AccountType='C' THEN 'CORPORATE' ELSE AccountType END AS 'AccountType',TradingStatus  from accounts_clients where BrokerCode='" + p +"' ORDER BY ID DESC";
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<Brokers>();
            while (reader.Read())
            {
                var accountDetails = new Brokers
                {

                    Name = reader.GetValue(2).ToString(),
                    CDSNo = reader.GetValue(0).ToString(),
                    Phone = reader.GetValue(5).ToString(),
                    Broker = reader.GetValue(1).ToString(),
                    IDNumber = reader.GetValue(3).ToString(),
                    IDType = reader.GetValue(6).ToString(),
                    AccountType = reader.GetValue(7).ToString(),
                    TradingStatus = reader.GetValue(8).ToString()
                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();

            return accDetails;
        }

        public List<SelectListItem> Secs()
        {
            var client = new RestClient("http://localhost/BrokerService");
            var request = new RestRequest("Security", Method.GET);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            List<Securities> dataList = JsonConvert.DeserializeObject<List<Securities>>(validate);
            List<SelectListItem> phy = new List<SelectListItem>();

            phy.Add(new SelectListItem { Text = "All", Value = "All" });

            foreach (var b in dataList)
            {
                phy.Add(new SelectListItem { Text = b.Fnam, Value = b.Company });

            }

            return phy;
        }

    }
}