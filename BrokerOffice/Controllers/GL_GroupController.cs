﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BrokerOffice.DAO;
using BrokerOffice.Models;
using System.Web.Security;
using BrokerOffice.DAO.security;
using PagedList;

namespace BrokerOffice.Controllers
{
   // [CustomAuthorize(RolesConfigKey = "RolesConfigKey")]
        // [CustomAuthorize(UsersConfigKey = "UsersConfigKey")]
        [CustomAuthorize(Roles = "User,Approver,BrokerAdmin", NotifyUrl = "/UnauthorizedPage")]
        // [CustomAuthorize(Users = "1,2")]
    public class GL_GroupController : Controller
    {
        private SBoardContext db = new SBoardContext();

        // GET: GL_Group
        public async Task<ActionResult> Index(string sortOrder, int? page)
        {
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.StaringSortParm = String.IsNullOrEmpty(sortOrder) ? "starting" : "";
            ViewBag.EndingSortParm = String.IsNullOrEmpty(sortOrder) ? "ending" : "";
            var per = from s in db.GL_Groups
                      select s;
            switch (sortOrder)
            {
                case "name_desc":
                    per = per.OrderByDescending(s => s.GroupName);
                    break;

                case "starting":
                    per = per.OrderBy(s => s.StartingAccount);
                    break;
                case "ending":
                    per = per.OrderBy(s => s.EndingAccount);
                    break;
                default:
                    per = per.OrderBy(s => s.GL_GroupID);
                    break;
            }
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(per.ToPagedList(pageNumber, pageSize));
          //  return View(await db.GL_Groups.ToListAsync());
        }

        // GET: GL_Group/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GL_Group gL_Group = await db.GL_Groups.FindAsync(id);
            if (gL_Group == null)
            {
                return HttpNotFound();
            }
            return View(gL_Group);
        }

        // GET: GL_Group/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: GL_Group/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "GL_GroupID,GroupName,StartingAccount,EndingAccount")] GL_Group gL_Group)
        {
            if (ModelState.IsValid)
            {
                db.GL_Groups.Add(gL_Group);
                await db.SaveChangesAsync();
                System.Web.HttpContext.Current.Session["NOT"] = "You have successfully added the GL Group";

                return RedirectToAction("Index");
            }

            return View(gL_Group);
        }

        // GET: GL_Group/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GL_Group gL_Group = await db.GL_Groups.FindAsync(id);
            if (gL_Group == null)
            {
                return HttpNotFound();
            }
            return View(gL_Group);
        }

        // POST: GL_Group/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "GL_GroupID,GroupName,StartingAccount,EndingAccount")] GL_Group gL_Group)
        {
            if (ModelState.IsValid)
            {
                db.Entry(gL_Group).State = EntityState.Modified;
                await db.SaveChangesAsync();
                System.Web.HttpContext.Current.Session["NOT"] = "You have successfully updated the GL Group";

                return RedirectToAction("Index");
            }
            return View(gL_Group);
        }

        // GET: GL_Group/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GL_Group gL_Group = await db.GL_Groups.FindAsync(id);
            if (gL_Group == null)
            {
                return HttpNotFound();
            }
            return View(gL_Group);
        }

        // POST: GL_Group/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            GL_Group gL_Group = await db.GL_Groups.FindAsync(id);
            db.GL_Groups.Remove(gL_Group);
            await db.SaveChangesAsync();
            System.Web.HttpContext.Current.Session["NOT"] = "You have successfully deleted the GL Group";

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
