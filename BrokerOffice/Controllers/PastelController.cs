﻿using BrokerOffice.DAO;
using BrokerOffice.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Web;
using System.Web.Mvc;
using NsExcel = Microsoft.Office.Interop.Excel;

namespace BrokerOffice.Controllers
{
    public class PastelController : Controller
    {

        private SBoardContext db = new SBoardContext();

        // GET: Pastel
        public ActionResult Pastel()
        {
            return View();
        }

        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Pastel([Bind(Include = "From,To")] Posting registerreport)
        {

            if (ModelState.IsValid)
            {
                try
                {
                    List<PastelData> pastelList = new List<PastelData>();

                    //return Redirect("~/Reporting/GeneralLedger.aspx?From=" + registerreport.From.ToString("dd-MMM-yyyy") + "&To=" + registerreport.To.ToString("dd-MMM-yyyy"));
                    //var myDate = registerreport.From;

                    DateTime fDate = registerreport.From;
                    DateTime tDate = registerreport.To;

                    List<string> accountsToUpload = new List<string>();
                    accountsToUpload.Add("2003");
                    accountsToUpload.Add("6001");
                    accountsToUpload.Add("4008");
                    accountsToUpload.Add("4010");
                    accountsToUpload.Add("4012");
                    accountsToUpload.Add("4014");
                    accountsToUpload.Add("4009");
                    accountsToUpload.Add("4013");
                    accountsToUpload.Add("4011");
                    accountsToUpload.Add("4001");

                    var myTrans = db.Transs.Where(x => x.TrxnDate >= fDate && x.TrxnDate <= tDate &&   accountsToUpload.Contains(x.Account)).ToList();

                    foreach (var item in myTrans)
                    {
                        PastelData pastelData = new PastelData();
                        var myDate = item.TrxnDate;
                        pastelData.TransDate = myDate.Date.ToShortDateString();
                        pastelData.Account = item.Account;
                        pastelData.Module = "AP";
                        pastelData.TransCode = "IN";
                        pastelData.Reference = item.Reference_Number;
                        pastelData.Description = item.Narration;
                        pastelData.AmountExcl = Convert.ToDecimal(item.Debit + item.Credit);
                        if (item.Debit > 0)
                        {
                            pastelData.IsDebit = "1";
                        }
                        else
                        {
                            pastelData.IsDebit = "0";
                        }

                        pastelData.SplitGLAccount = item.Account;
                        try
                        {
                            var myAccMasters = db.Accounts_Masters.Where(xy => xy.AccountNumber.ToString() == item.Account).FirstOrDefault();
                            pastelData.SplitDescription = myAccMasters.AccountName;
                        }
                        catch (Exception)
                        {
                            var myAccClients = db.Account_Creations.Where(xy => xy.CDSC_Number.ToString() == item.Account).FirstOrDefault();
                            pastelData.SplitDescription = myAccClients.Surname_CompanyName + " " + myAccClients.OtherNames;
                        }



                        //pastelData.SplitDescription = item.Narration;
                        pastelData.SplitAmount = Convert.ToDecimal(item.Debit + item.Credit);

                        pastelList.Add(pastelData);
                    }

                    Response.Clear();
                    Response.AddHeader("Content-Disposition", "attachment; filename=TransactionData.csv");
                    Response.ContentType = "text/csv";
                    //write the header

                    Response.Write(String.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10}\r\n", "TransDate", "Account", "Module", "TransCode", "Reference", "Description", "AmountExcl", "IsDebit", "SplitGLAccount", "SplitDescription", "SplitAmount"));

                    //write every subscriber to the file
                    var resourceManager = new ResourceManager(typeof(PastelData));



                    foreach (var pastelData in pastelList)
                    {
                        Response.Write(String.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10}\r\n", pastelData.TransDate, pastelData.Account, pastelData.Module, pastelData.TransCode, pastelData.Reference, pastelData.Description, pastelData.AmountExcl, pastelData.IsDebit, pastelData.SplitGLAccount, pastelData.SplitDescription, pastelData.SplitAmount));
                        //Response.Write(Environment.NewLine);
                    }

                    Response.End();

                    return Redirect(Request.RawUrl);
                }
                catch (Exception)
                {

                   
                }
               

            }
            return View();


          


        }

        //public void WriteLineToResponse(string format, params object[] args)
        //{
        //    Response.Write(String.Format(format, args));
        //    Response.Write(Environment.NewLine);
        //}

        class PastelData
        {
            public string TransDate { get; set; }
            public string Account { get; set; }
            public string Module { get; set; }
            public string TransCode { get; set; }
            public string Reference { get; set; }
            public string Description { get; set; }
            public string OrderNumber { get; set; }
            public decimal AmountExcl { get; set; }
            public string IsDebit { get; set; }
            public string SplitGLAccount { get; set; }
            public string SplitDescription { get; set; }
            public decimal SplitAmount { get; set; }

        }

    }
}