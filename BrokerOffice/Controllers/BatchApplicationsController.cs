﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BrokerOffice.DAO;
using BrokerOffice.Models;

namespace BrokerOffice.Controllers
{
    public class BatchApplicationsController : Controller
    {
        private SBoardContext db = new SBoardContext();

        // GET: BatchApplications
        public async Task<ActionResult> Index()
        {
            //session variable 
            string nme = System.Web.HttpContext.Current.Session["BA"].ToString();
            ViewBag.nm = nme;
            ViewBag.shrs = "";
            ViewBag.amt = "";
            ViewBag.apps = "";
            ViewBag.stat = "";
            try
            {
                decimal fr = 0;
                ViewBag.apps = db.BatchApplications.ToList().Where(a => a.BatchNo.ToString() == nme).Count();
                ViewBag.amt = db.BatchApplications.ToList().Where(a => a.BatchNo.ToString() == nme).Sum(a=>a.ClientAmount);
                fr= db.BatchApplications.ToList().Where(a => a.BatchNo.ToString() == nme).Sum(a => a.numberofshares);
                
                ViewBag.amt = ViewBag.amt.ToString("#,##0");
                ViewBag.shrs = Convert.ToInt32(fr).ToString();
                var e = db.batch_mast.ToList().Where(a => a.batch_no.ToString() == nme);
               foreach (var p in e)
                {
                    ViewBag.stat = p.OldBatchRef;
                }
            
                return View(await db.BatchApplications.Where(a => a.BatchNo.ToString() == nme).ToListAsync());
            }
            catch (Exception)
            {

                return View(await db.BatchApplications.Where(a => a.BatchNo.ToString() == nme).ToListAsync());

            }
        }

        // GET: BatchApplications/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BatchApplication batchApplication = await db.BatchApplications.FindAsync(id);
            if (batchApplication == null)
            {
                return HttpNotFound();
            }
            return View(batchApplication);
        }

        // GET: BatchApplications/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: BatchApplications/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "BatchApplicationID,ClientName,ClientAmount,numberofshares,Clientnumber,BatchNo")] BatchApplication batchApplication)
        {
            if (ModelState.IsValid)
            {
                db.BatchApplications.Add(batchApplication);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(batchApplication);
        }

        // GET: BatchApplications/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BatchApplication batchApplication = await db.BatchApplications.FindAsync(id);
            if (batchApplication == null)
            {
                return HttpNotFound();
            }
            return View(batchApplication);
        }

        // POST: BatchApplications/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "BatchApplicationID,ClientName,ClientAmount,numberofshares,Clientnumber,BatchNo")] BatchApplication batchApplication)
        {
            if (ModelState.IsValid)
            {
                db.Entry(batchApplication).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(batchApplication);
        }

        // GET: BatchApplications/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BatchApplication batchApplication = await db.BatchApplications.FindAsync(id);
            if (batchApplication == null)
            {
                return HttpNotFound();
            }
            return View(batchApplication);
        }

        // POST: BatchApplications/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            BatchApplication batchApplication = await db.BatchApplications.FindAsync(id);
            db.BatchApplications.Remove(batchApplication);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
