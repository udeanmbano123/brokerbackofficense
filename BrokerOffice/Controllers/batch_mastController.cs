﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BrokerOffice.DAO;
using BrokerOffice.Models;
using WebMatrix.WebData;
using RestSharp;
using Newtonsoft.Json;
using PagedList;
using BrokerOffice.DAO.security;
using System.Data.Entity.Migrations;

namespace BrokerOffice.Controllers
{// [CustomAuthorize(RolesConfigKey = "RolesConfigKey")]
 // [CustomAuthorize(UsersConfigKey = "UsersConfigKey")]
    [CustomAuthorize(Roles = "User", NotifyUrl = "/UnauthorizedPage")]
    // [CustomAuthorize(Users = "1,2")]
    public class batch_mastController : Controller
    {
        private SBoardContext db = new SBoardContext();

        // GET: batch_mast
        public async Task<ActionResult> Index(string sortOrder, int? page)
        {
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.ModuleSortParm = String.IsNullOrEmpty(sortOrder) ? "module" : "";
            var per = from s in db.batch_mast
                      where s.CreatedBy==WebSecurity.CurrentUserName
                      select s;
            switch (sortOrder)
            {
                case "name_desc":
                    per = per.OrderByDescending(s => s.batch_no);
                    break;

                case "module":
                    per = per.OrderBy(s => s.datecreated);
                    break;

                default:
                    per = per.OrderBy(s => s.ID);
                    break;
            }
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(per.ToPagedList(pageNumber, pageSize));

        }
        // GET: Batch/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            batch_mast batch_mast = await db.batch_mast.FindAsync(id);
            if (batch_mast == null)
            {
                return HttpNotFound();
            }
            return View(batch_mast);
        }
        // GET: batch_mast/Create
        public ActionResult Create()
        {
            ViewBag.AgentList = Agent();
            ViewBag.Company = Company();
            ViewBag.Counter = "";
            int mm = 0;
           try 
	{
                mm = db.batch_mast.ToList().Max(a => a.ID);
                mm += 1;
         ViewBag.Counter = mm;
	}
	catch (Exception e)
	{

                ViewBag.Counter = 1;

    }
            return View();
        }

        // POST: batch_mast/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,batch_no,batch_type,app_shares,app_cash,status,Company,Agent,BrokerBatchRef,Verify,CreatedBy,apps,branch,datecreated,PayMode,BoxNo,OldBatchRef,Source,WebResponse")] batch_mast batch_mast)
        {
           // batch_mast.app_cash = 0;
          //  batch_mast.app_shares = 0;
            batch_mast.batch_type = "BBO";
            batch_mast.status = "C";
            batch_mast.BrokerBatchRef = batch_mast.Agent + "//" + batch_mast.batch_no;
            batch_mast.Verify = false;
            batch_mast.CreatedBy = WebSecurity.CurrentUserName;
            if (batch_mast.CreatedBy == "")
            {
                batch_mast.CreatedBy = "LoggedInUser";
            }
            batch_mast.branch = "N/A";
            batch_mast.datecreated = DateTime.Now;
            batch_mast.PayMode = "N/A";
            batch_mast.BoxNo = 0;
            batch_mast.OldBatchRef = "N/A";
            batch_mast.Source = "BBO";
           // batch_mast.ID = 0;

            //check if number exists
            var pz = db.batch_mast.ToList().Where(a => a.batch_no == batch_mast.batch_no).Count();

            if (ModelState.IsValid && pz == 0)
            {


                //send to IPO lols
                //webservice
                int mm2 = 0;
                try
                {                     mm2= db.batch_mast.ToList().Max(a => a.ID);
                    mm2 += 1;
                    ViewBag.Counter = mm2;
                }
                catch (Exception e)
                {

                    ViewBag.Counter = 1;

                }
              
                batch_mast.batch_no =(mm2 + 1).ToString();
                db.batch_mast.Add(batch_mast);
                System.Web.HttpContext.Current.Session["NOT"] = "Batch created successsfully";
                
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            if (pz > 0)
            {
                var mod = ModelState.First(c => c.Key == "batch_no");  // this
                mod.Value.Errors.Add("Duplicate batch numbers are not allowed");
            }
            int mm = 0;
            try
            {
                mm = db.batch_mast.ToList().Max(a => a.ID);
                ViewBag.Counter = mm + 1;
            }
            catch (Exception e)
            {

                ViewBag.Counter = 1;

            }
            ViewBag.AgentList = Agent();
            ViewBag.Company = Company();
            return View(batch_mast);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public List<SelectListItem> Agent(string s1,string s2)
        {

            var p = db.Users.ToList().Where(a => a.Email == WebSecurity.CurrentUserName);
            string mm = "";
            foreach (var z in p)
            {
                mm = z.BrokerCode;
            }
            var client = new RestClient("http://localhost/BrokerService");
            var request = new RestRequest("IPAgents", Method.GET);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            List<IPAgents> dataList = JsonConvert.DeserializeObject<List<IPAgents>>(validate);


            var dbsel = from s in dataList
                        where s.code == mm
                        select s;

            List<SelectListItem> phy = new List<SelectListItem>();

            var query = from u in dbsel select u;
            var d = query.ToList().Where(a => a.code== s1);
            foreach (var b in d)
            {
                phy.Add(new SelectListItem { Text = b.desc, Value = b.code });

            }
            if (query.Count() > 0)
            {
                foreach (var v in query)
                {
                    phy.Add(new SelectListItem { Text = v.desc, Value = v.code });
                }
            }
            return phy;
        }

        public List<SelectListItem> Agent()
        {

            var p = db.Users.ToList().Where(a => a.Email == WebSecurity.CurrentUserName);
            string mm = "";
            foreach (var z in p)
            {
                mm = z.BrokerCode;
            }
            var client = new RestClient("http://localhost/BrokerService");
            var request = new RestRequest("IPAgents", Method.GET);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            List<IPAgents> dataList = JsonConvert.DeserializeObject<List<IPAgents>>(validate);


            var dbsel = from s in dataList
                        where s.code==mm
                        select s;

            List<SelectListItem> phy = new List<SelectListItem>();

            var query = from u in dbsel select u;
            if (query.Count() > 0)
            {
                foreach (var v in query)
                {
                    phy.Add(new SelectListItem { Text = v.desc, Value = v.code });
                }
            }
            return phy;
        }

        public List<SelectListItem> Company()
        {

          
            var client = new RestClient("http://localhost/BrokerService");
            var request = new RestRequest("IPCompany", Method.GET);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            List<IPCompany> dataList = JsonConvert.DeserializeObject<List<IPCompany>>(validate);


            var dbsel = from s in dataList
               
                        select s;

            List<SelectListItem> phy = new List<SelectListItem>();

            var query = from u in dbsel select u;
            if (query.Count() > 0)
            {
                foreach (var v in query)
                {
                    phy.Add(new SelectListItem { Text = v.company_name, Value = v.company });
                }
            }
            return phy;
        }
        public async Task<ActionResult> SubmitBatch(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            batch_mast batch = await db.batch_mast.FindAsync(id);
            if (batch == null)
            {
                return HttpNotFound();
            }

            return View(batch);
        }
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            batch_mast batch_mast = await db.batch_mast.FindAsync(id);
            if (batch_mast == null)
            {
                return HttpNotFound();
            }
            if (batch_mast.OldBatchRef=="SUBMITTED")
            {
                System.Web.HttpContext.Current.Session["NOT"] = "The batch has been committed";

                return RedirectToAction("Index");
            }
            var d = db.Users.ToList().Where(a=>a.Email==WebSecurity.CurrentUserName);
            string br = ""; string bn = "" ;
            foreach(var p in d)
            {
                br = p.BrokerName;
                bn = p.BrokerCode;
            }
            ViewBag.AgentList = Agent(br,bn);
            ViewBag.Company = Company(batch_mast.Company, batch_mast.Company);

            return View(batch_mast);
        }

        // POST: batch_mast1/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,batch_no,batch_type,app_shares,app_cash,status,Company,Agent,BrokerBatchRef,Verify,CreatedBy,apps,branch,datecreated,PayMode,BoxNo,OldBatchRef,Source,WebResponse")] batch_mast batch_mast)
        {
            if (ModelState.IsValid)
            {
                batch_mast.batch_type = "BBO";
                batch_mast.status = "C";
                batch_mast.BrokerBatchRef = batch_mast.Agent + "//" + batch_mast.batch_no;
                batch_mast.Verify = false;
                batch_mast.CreatedBy = WebSecurity.CurrentUserName;
                if (batch_mast.CreatedBy == "")
                {
                    batch_mast.CreatedBy = "LoggedInUser";
                }
                batch_mast.branch = "N/A";
                batch_mast.datecreated = batch_mast.datecreated;
                batch_mast.PayMode = "N/A";
                batch_mast.BoxNo = 0;
                batch_mast.OldBatchRef = "N/A";
                batch_mast.Source = "BBO";
                //batch_mast.ID = 0;
                batch_mast.WebResponse = "N/A";
                db.Entry(batch_mast).State = EntityState.Modified;
                await db.SaveChangesAsync();
                System.Web.HttpContext.Current.Session["NOT"] = "The batch number "+ batch_mast.batch_no +" was successfully updated.";
                return RedirectToAction("Index");
            }
            ViewBag.AgentList = Agent(batch_mast.Agent,batch_mast.Agent);
            ViewBag.Company = Company(batch_mast.Company, batch_mast.Company);

            return View(batch_mast);
        }
        // POST: Batch/Delete/5
        [HttpPost, ActionName("SubmitBatch")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SubmitBatchConfirmed(int id)
        {
            batch_mast batch = await db.batch_mast.FindAsync(id);

            var client = new RestClient("http://localhost/BrokerService");
            var request = new RestRequest("IPPPCompany/{f}", Method.GET);
            request.AddUrlSegment("f", batch.Company);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;

            var Jsonobject = JsonConvert.DeserializeObject<string>(validate);
            validate = Jsonobject.ToString();

            decimal minshares = Convert.ToDecimal(batch.app_shares);
            decimal appshr = Convert.ToDecimal(validate);
            //batch applications
            var e= db.BatchApplications.ToList().Where(a => a.BatchNo.ToString() == batch.batch_no).Count();
            var f= db.BatchApplications.ToList().Where(a => a.BatchNo.ToString() == batch.batch_no).Sum(a=>a.numberofshares);
            //batch sares
            if (minshares < appshr)
            {
                System.Web.HttpContext.Current.Session["NOT"] = "The minimum required shares for the batch " + appshr;

            }
            else if (batch.OldBatchRef == "SUBMITTED")
            {
                System.Web.HttpContext.Current.Session["NOT"] = "The batch was already submitted";

            }else if (Convert.ToDecimal(f)!=batch.app_shares)
            {
                System.Web.HttpContext.Current.Session["NOT"] = "The batch application share total of "+ f +" does not match with batch share total of "+ batch.app_shares;

            }
            else if (e != Convert.ToInt32(batch.apps))
            {
                System.Web.HttpContext.Current.Session["NOT"] = "The batch application total of "+ e +" does not match with batch header total of"+ batch.apps +" applications";

            }
            else
            {
               
                batch.OldBatchRef = "SUBMITTED";
               
                db.batch_mast.AddOrUpdate(batch);
                await db.SaveChangesAsync();
         
                System.Web.HttpContext.Current.Session["NOT"] = "The batch was successfully created and is waiting for authorization";

            }

            return RedirectToAction("Index");
        }

        public async Task<ActionResult> Apps(string id)
        {
            System.Web.HttpContext.Current.Session["BA"] = id;

            return Redirect("~/BatchApplications/Index");
        }

    
        public List<SelectListItem> Company(string s1,string s2)
        {


            var client = new RestClient("http://localhost/BrokerService");
            var request = new RestRequest("IPCompany", Method.GET);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            List<IPCompany> dataList = JsonConvert.DeserializeObject<List<IPCompany>>(validate);


            var dbsel = from s in dataList

                        select s;

            List<SelectListItem> phy = new List<SelectListItem>();

            var query = from u in dbsel select u;
            var d = query.ToList().Where(a=>a.company==s1);
            foreach(var b in d)
            {
        phy.Add(new SelectListItem { Text = b.company_name, Value = b.company });
           
            }
            if (query.Count() > 0)
            {
                foreach (var v in query)
                {
                    phy.Add(new SelectListItem { Text = v.company_name, Value = v.company });
                }
            }
            return phy;
        }
    }
}


