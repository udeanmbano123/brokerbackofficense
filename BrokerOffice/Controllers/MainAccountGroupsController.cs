﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BrokerOffice.DAO;
using BrokerOffice.Models;

namespace BrokerOffice.Controllers
{
    public class MainAccountGroupsController : Controller
    {
        private SBoardContext db = new SBoardContext();

        // GET: MainAccountGroups
        public async Task<ActionResult> Index()
        {
            var tblFinAccountGroups = db.tblFinAccountGroups.Include(t => t.tblBalanceType);
            return View(await tblFinAccountGroups.ToListAsync());
        }

        // GET: MainAccountGroups/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblFinAccountGroups tblFinAccountGroups = await db.tblFinAccountGroups.FindAsync(id);
            if (tblFinAccountGroups == null)
            {
                return HttpNotFound();
            }
            return View(tblFinAccountGroups);
        }

        // GET: MainAccountGroups/Create
        public ActionResult Create()
        {
            ViewBag.BalanceTypeId = new SelectList(db.tblBalanceTypes, "BalanceTypeId", "BalanceTypeName");
            return View();
        }

        // POST: MainAccountGroups/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "FinAccountGroupId,AccountTypeName,BalanceTypeId,FirstNumber,LastNumber")] tblFinAccountGroups tblFinAccountGroups)
        {
            if (ModelState.IsValid)
            {
                db.tblFinAccountGroups.Add(tblFinAccountGroups);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.BalanceTypeId = new SelectList(db.tblBalanceTypes, "BalanceTypeId", "BalanceTypeName", tblFinAccountGroups.BalanceTypeId);
            return View(tblFinAccountGroups);
        }

        // GET: MainAccountGroups/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblFinAccountGroups tblFinAccountGroups = await db.tblFinAccountGroups.FindAsync(id);
            if (tblFinAccountGroups == null)
            {
                return HttpNotFound();
            }
            ViewBag.BalanceTypeId = new SelectList(db.tblBalanceTypes, "BalanceTypeId", "BalanceTypeName", tblFinAccountGroups.BalanceTypeId);
            return View(tblFinAccountGroups);
        }

        // POST: MainAccountGroups/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "FinAccountGroupId,AccountTypeName,BalanceTypeId,FirstNumber,LastNumber")] tblFinAccountGroups tblFinAccountGroups)
        {
            //if (ModelState.IsValid)
            //{

            //}

            var savedGroup = db.tblFinAccountGroups.Where(x => x.FinAccountGroupId == tblFinAccountGroups.FinAccountGroupId).FirstOrDefault();

            if (checkAccountNumbers(tblFinAccountGroups)==false)
            {
                savedGroup.FirstNumber = tblFinAccountGroups.FirstNumber;
                savedGroup.LastNumber = tblFinAccountGroups.LastNumber;
                await db.SaveChangesAsync();

                System.Web.HttpContext.Current.Session["NOT"] = "Changes saved successfully";
            }
            else
            {
                System.Web.HttpContext.Current.Session["NOT"] = "Changes were not saved please review your accounts numbers";
            }
            


            

            //db.Entry(tblFinAccountGroups).State = EntityState.Modified;
          
            //return RedirectToAction("Index");

            //System.Web.HttpContext.Current.Session["NOT"] = "Changes saved successfully";
            //Response.Redirect(Request.RawUrl, false);

            //ViewBag.BalanceTypeId = new SelectList(db.tblBalanceTypes, "BalanceTypeId", "BalanceTypeName", tblFinAccountGroups.BalanceTypeId);
            return RedirectToAction("Index");
           //return View();
        }

        // GET: MainAccountGroups/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblFinAccountGroups tblFinAccountGroups = await db.tblFinAccountGroups.FindAsync(id);
            if (tblFinAccountGroups == null)
            {
                return HttpNotFound();
            }
            return View(tblFinAccountGroups);
        }

        // POST: MainAccountGroups/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            tblFinAccountGroups tblFinAccountGroups = await db.tblFinAccountGroups.FindAsync(id);
            db.tblFinAccountGroups.Remove(tblFinAccountGroups);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool checkAccountNumbers(tblFinAccountGroups finGroup)
        {
            bool isAccountNumbersTaken = true;
            List<tblFinAccountGroups> finGroupList = db.tblFinAccountGroups.Where( a =>a.FinAccountGroupId != finGroup.FinAccountGroupId).ToList();

            if (finGroupList != null)
            {
                List<int> requestedAccounts = new List<int>();
                List<int> takenAccounts = new List<int>();

                foreach (var item in finGroupList)
                {
                    //Get Max and Min Account numbers for each account group that is in the database
                    //These should be treated as account numbers already taken
                    int min = item.FirstNumber;
                    int max = item.LastNumber;

                    for (int i = min; i <= max; i++)
                    {
                        takenAccounts.Add(i);
                    }

                }

                //for (int i = finGroup.FirstNumber; i < finGroup.LastNumber; i++)
                //{
                //    requestedAccounts.Add(i);
                //}

                if (!takenAccounts.Contains(finGroup.FirstNumber) && !takenAccounts.Contains(finGroup.LastNumber))
                {
                    if (finGroup.FirstNumber > 0 && finGroup.LastNumber> finGroup.FirstNumber)
                    {
                        isAccountNumbersTaken = false;

                        for (int i = finGroup.FirstNumber; i < finGroup.LastNumber; i++)
                        {
                            if (takenAccounts.Contains(i))
                            {
                                isAccountNumbersTaken = true;
                                break;
                            }
                        }

                    }
                    else
                    {
                        isAccountNumbersTaken = true;
                    }
                                        
                }
                else
                {
                    isAccountNumbersTaken = true;
                }

                
            }

            return isAccountNumbersTaken;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
