﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BrokerOffice.DAO;
using BrokerOffice.Models;
using PagedList;
using BrokerOffice.DAO.security;
using System.Data.OleDb;
using System.Configuration;
using System.Text.RegularExpressions;
using WebMatrix.WebData;
using Database = WebMatrix.Data.Database;
using OfficeOpenXml;
using System.Data.Entity.Migrations;
using RestSharp;

namespace BrokerOffice.Controllers
{// [CustomAuthorize(RolesConfigKey = "RolesConfigKey")]
 // [CustomAuthorize(UsersConfigKey = "UsersConfigKey")]
    [CustomAuthorize(Roles = "User,Approver,BrokerAdmin,BrokerAdmin2", NotifyUrl = " /UnauthorizedPage")]
    // [CustomAuthorize(Users = "1,2")]
    public class Account_CreationPController : Controller
    {
        private SBoardContext db = new SBoardContext();

        // GET: Account_Creation
        public ViewResult Index(string sortOrder, string currentFilter, string
  searchString, string Date1String, string Date2String, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.IdentificationSortParm = String.IsNullOrEmpty(sortOrder) ? "identification" : "";
            ViewBag.SurnameSortParm = String.IsNullOrEmpty(sortOrder) ? "surname" : "";
            ViewBag.CDSCSortParm = String.IsNullOrEmpty(sortOrder) ? "cdsc" : "";
            var per = from s in db.Account_CreationPendingss
                      where s.update_type == "TOUPDATE" || s.update_type == "INSERT"
                      select s;
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;
            if (!String.IsNullOrEmpty(searchString))
            {
                per = per.Where(s =>
                (s.Surname_CompanyName).ToUpper().Contains(searchString.ToUpper())
                 ||
                 (s.OtherNames).ToUpper().Contains(searchString.ToUpper())
                 ||
                (s.CDSC_Number).ToUpper().Contains(searchString.ToUpper()));
            }

            try
            {
                Date1String = Request.QueryString["Date1String"].ToString();
                Date2String = Request.QueryString["Date2String"].ToString();
                if (!String.IsNullOrEmpty(Date1String) && !String.IsNullOrEmpty(Date2String))
                {
                    System.Web.HttpContext.Current.Session["Date1"] = Date1String;
                    System.Web.HttpContext.Current.Session["Date2"] = Date2String;
                    ViewBag.dt1 = Date1String;
                    ViewBag.dt2 = Date2String;
                }
            }
            catch (Exception)
            {


            }
            try
            {
                if (System.Web.HttpContext.Current.Session["Date1"].ToString() != "" && System.Web.HttpContext.Current.Session["Date2"].ToString() != "")
                {

                    Date1String = System.Web.HttpContext.Current.Session["Date1"].ToString();
                    Date2String = System.Web.HttpContext.Current.Session["Date2"].ToString();
                    ViewBag.dt1 = Date1String;
                    ViewBag.dt2 = Date2String;
                }
            }
            catch (Exception)
            {


            }
            if (!String.IsNullOrEmpty(Date1String) && !String.IsNullOrEmpty(Date2String) && String.IsNullOrEmpty(searchString))
            {
                System.Web.HttpContext.Current.Session["Date1"] = Date1String;
                System.Web.HttpContext.Current.Session["Date2"] = Date2String;
                DateTime date = Convert.ToDateTime(Date1String);
                DateTime date2 = Convert.ToDateTime(Date2String);
                DateTime date3 = date2.AddDays(1);
                per = from s in db.Account_CreationPendingss
                     where (s.Date_Created > date && s.Date_Created < date3)
                     && (s.update_type == "TOUPDATE" || s.update_type == "INSERT")
                      select s;

                var cc = per.Count();
            }
            ViewBag.dt1 = Date1String;
            ViewBag.dt2 = Date2String;
            switch (sortOrder)
            {
                case "name_desc":
                    per = per.OrderByDescending(s => s.OtherNames);
                    break;
                case "identification":
                    per = per.OrderBy(s => s.Identification);
                    break;
                case "surname":
                    per = per.OrderBy(s => s.Surname_CompanyName);
                    break;
                case "cdsc":
                    per = per.OrderBy(s => s.CDSC_Number);
                    break;
                default:
                    per = per.OrderBy(s => s.ID_);
                    break;
            }
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(per.ToPagedList(pageNumber, pageSize));

        }

        // GET: Account_Creation/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Account_CreationPending account_Creation = await db.Account_CreationPendingss.FindAsync(id);
            if (account_Creation == null)
            {
                return HttpNotFound();
            }
            return View(account_Creation);
        }



        // POST: Account_Creation/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.

        // GET: Account_Creation/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Account_CreationPending account_Creation = await db.Account_CreationPendingss.FindAsync(id);
            if (account_Creation == null)
            {
                return HttpNotFound();
            }
            return View(account_Creation);
        }

        // POST: Account_Creation/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Account_CreationPending account_Creation = await db.Account_CreationPendingss.FindAsync(id);
            db.Account_CreationPendingss.Remove(account_Creation);
            await db.SaveChangesAsync();
            System.Web.HttpContext.Current.Session["NOT"] = "The account has been discarded successfully";

            return RedirectToAction("Index");
        }

        public ActionResult UploadExcel()
        {

            ViewBag.Sucess = "";
            return View();
        }

        [HttpPost]
        public ActionResult UploadExcel(FormCollection formCollection)
        {


            string fileLocation = "";

            

            int total = 0;
            if (Request != null)
            {
                HttpPostedFileBase file = Request.Files["UploadedFile"];
                if ((file != null) && (file.ContentLength != 0) && !string.IsNullOrEmpty(file.FileName))
                {
                    string fileName = file.FileName;
                    string fileContentType = file.ContentType;
                    byte[] fileBytes = new byte[file.ContentLength];
                    var data = file.InputStream.Read(fileBytes, 0, Convert.ToInt32(file.ContentLength));

                    using (var package = new ExcelPackage(file.InputStream))
                    {
                        var currentSheet = package.Workbook.Worksheets;
                        var workSheet = currentSheet.First();
                        var noOfCol = workSheet.Dimension.End.Column;
                        var noOfRow = workSheet.Dimension.End.Row;
                        int n = noOfRow;
                        total = n - 1;
                        for (int rowIterator = 2; rowIterator < (n + 1); rowIterator++)
                        {
                            db.Database.Connection.Open();
                            // string query = "Insert into Person(Name,Email,Mobile) Values('" + ds.Tables[0].Rows[i][0].ToString() + "','" + ds.Tables[0].Rows[i][1].ToString() + "','" + ds.Tables[0].Rows[i][2].ToString() + "')";
                            //EF Query
                            Account_Creation my = new Account_Creation();
                            var con2 = Database.Open("SBoardConnection");
                            var audit = "select max(ID_) as ID from Account_Creation";
                            var list13 = con2.Query(audit).ToList();
                            int code25 = 0;
                            try
                            {
                                foreach (var row in list13)
                                {
                                    code25 = row.ID;
                                }

                            }

                            catch (Exception f)
                            {
                                code25 = 0;
                            }

                            int size = 5;


                            var agent = "select top 1 AgentCode from Agent";
                            var list14 = con2.Query(agent).ToList();
                            string code22 = "";
                            foreach (var row in list14)
                            {
                                code22 = row.AgentCode;
                            }
                            string concode25 = Convert.ToString((code25 + 1));
                            string cdsNo = concode25.PadLeft(10, '0') + code22 + concode25;
                            //my.CDSC_Number = cdsNo;
                            try
                            {
                                my.Title = workSheet.Cells[rowIterator, 4].Value.ToString();
                            }
                            catch (Exception)
                            {

                                my.Title = "TBA";
                            }
                            try
                            {
                                my.OtherNames = workSheet.Cells[rowIterator, 6].Value.ToString();
                            }
                            catch (Exception)
                            {

                                my.OtherNames = "TBA";
                            }
                            my.Middlename = "TBA";
                            try
                            {
                                my.Surname_CompanyName = workSheet.Cells[rowIterator, 5].Value.ToString();
                            }
                            catch (Exception)
                            {

                                my.Surname_CompanyName = "TBA";
                            }
                            try
                            {
                                my.Nationality = workSheet.Cells[rowIterator, 8].Value.ToString();
                            }
                            catch (Exception)
                            {

                                my.Nationality = "TBA";
                            }
                            try
                            {
                                my.Resident = workSheet.Cells[rowIterator, 8].Value.ToString();
                            }
                            catch (Exception)
                            {

                                my.Resident = "TBA";
                            }
                            my.DateofBirth_Incorporation = DateTime.Now;

                            my.Gender = "O";
                            my.Initials = "TBA";
                            //contact
                            try
                            {
                                my.Address1 = workSheet.Cells[rowIterator, 7].Value.ToString();
                            }
                            catch (Exception)
                            {

                                my.Address1 = "TBA";
                            }
                            my.Address2 = "TBA";
                            my.Address3 = "TBA";
                            my.Town = "TBA";
                            my.PostCode = "TBA";
                            my.FaxNumber = "TBA";
                            try
                            {
                                my.TelephoneNumber = workSheet.Cells[rowIterator, 14].Value.ToString();
                            }
                            catch (Exception)
                            {

                                my.TelephoneNumber = "TBA";

                            }
                            try
                            {
                                my.Emailaddress = workSheet.Cells[rowIterator, 13].Value.ToString();
                            }
                            catch (Exception)
                            {

                                my.Emailaddress = "TBA";
                            }
                            //ident
                            my.ClientSuffix = "LI";
                            my.RNum = "TBA";
                            try
                            {
                                my.Identification = workSheet.Cells[rowIterator, 2].Value.ToString();
                            }
                            catch (Exception)
                            {

                                my.Identification = "TBA";
                            }

                            //banking
                            int? x = 100;
                            my.TaxBracket = x;
                            my.Bank = "TBA";
                            try
                            {
                                my.Branch = workSheet.Cells[rowIterator, 11].Value.ToString();
                            }
                            catch (Exception)
                            {

                                my.Branch = "TBA";
                            }
                            my.Bankname = "TBA";
                            my.BranchName = "TBA";
                            try
                            {
                                my.Accountnumber = workSheet.Cells[rowIterator, 12].Value.ToString();
                            }
                            catch (Exception)
                            {

                                my.Accountnumber = "TBA";
                            }
                            my.idtype = "TBA";
                            my.accountcategory = "TBA";
                            try
                            {
                                my.country = workSheet.Cells[rowIterator, 8].Value.ToString();
                            }
                            catch (Exception)
                            {

                                my.country = "TBA";
                            }
                            my.currency = "US";
                            my.divpayee = "TBA";
                            my.divbank = "TBA";
                            my.divbankcode = "TBA";
                            try
                            {
                                my.divbranch = workSheet.Cells[rowIterator, 11].Value.ToString();
                            }
                            catch (Exception)
                            {

                                my.divbranch = "TBA";
                            }
                            try
                            {
                                my.divbranchcode = workSheet.Cells[rowIterator, 11].Value.ToString();
                            }
                            catch (Exception)
                            {

                                my.divbranchcode = "TBA";
                            }
                            try
                            {
                                my.divacc = workSheet.Cells[rowIterator, 12].Value.ToString();
                            }
                            catch (Exception)
                            {

                                my.divacc = "TBA";
                            }
                            my.divaccounttype = "TBA";
                            my.dividnumber = "TBA";

                            my.idtype2 = "0";

                            my.Date_Created = DateTime.Now;
                            try
                            {
                                my.MobileNumber = workSheet.Cells[rowIterator, 14].Value.ToString();
                            }
                            catch (Exception)
                            {

                                my.MobileNumber = "TBA";
                            }
                            my.mobile_money = "TBA";
                            my.depname = "TBA";
                            my.depcode = "TBA";

                            //Insert Blank Row in Table  
                            code22 = "BEAR";
                            my.currency = "USD";

                            if (my.MNO_ == "GLOBAL")
                            {
                                try
                                {
                                    my.Address3 = workSheet.Cells[rowIterator, 3].Value.ToString();
                                }
                                catch (Exception)
                                {

                                    my.Address3 = "";
                                }



                            }

                            db.Account_Creations.Add(my);

                            try
                            {
                                db.SaveChanges(WebSecurity.CurrentUserName.ToString());
                            }
                            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                            {

                                Exception raise = dbEx;
                                foreach (var validationErrors in dbEx.EntityValidationErrors)
                                {
                                    foreach (var validationError in validationErrors.ValidationErrors)
                                    {
                                        string message = string.Format("{0}:{1}",
                                            validationErrors.Entry.Entity.ToString(),
                                            validationError.ErrorMessage);
                                        raise = new InvalidOperationException(message, raise);
                                    }
                                }
                                throw raise;
                            }
                            db.Database.Connection.Close();
                            con2.Close();
                        }
                    }
                }


            }
            else
            {

            }
            ViewBag.Sucess = "Total of " + total + "records were uploaded";
            return View();
        }
        public bool IsAddressAvailable(string address)
        {
            try
            {
                System.Net.WebClient client = new WebClient();
                client.DownloadData(address);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public async Task<ActionResult> Update(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Account_CreationPending account_Creation = await db.Account_CreationPendingss.FindAsync(id);
            if (account_Creation == null)
            {
                return HttpNotFound();
            }
            return View(account_Creation);
        }

        // POST: Account_Creation/Delete/5
        [HttpPost, ActionName("Update")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> UpdateConfirmed(int id)
        {
            string acctype = "";
            Account_CreationPending my = await db.Account_CreationPendingss.FindAsync(id);
            try
            {
                my.PIN_No = my.ID_.ToString();
            }
            catch (Exception)
            {

               
            }
            if (my.update_type == "INSERT")
            {
                my.update_type = "NEW";
                string cds_raw = "";
                if (my.MNO_ == "GLOBAL")
                {
                    if (my.accountcategory == "LI")
                    {
                        acctype = "C";
                        my.Identification = my.RNum;
                    }
                    else if (my.accountcategory == "LE")
                    {
                        acctype = "I";
                    }
                    else if (my.accountcategory == "LJ")
                    {
                        acctype = "J";
                        my.Identification = "J" + my.ID_.ToString();
                    }

                   
                }
                if (my.MNO_ == "GLOBAL")
                {
                    if (!cds_raw.Contains("investor_pin"))
                    {
                        System.Web.HttpContext.Current.Session["NOT"] = "Error on authorization " + my.CDSC_Number;
                    }
                    else
                    {
                        db.Account_CreationPendingss.AddOrUpdate(my);
                        //stored procedure

                        System.Web.HttpContext.Current.Session["NOT"] = my.CDSC_Number + " has been processed successfully";
                        await db.SaveChangesAsync();
                        db.Database.ExecuteSqlCommand("Exec NewAccounts @cust_no='" + my.ID_ + "'");
                        UpdateMember(my.CDSC_Number, my.ID_.ToString());
                    }
                }
                else
                {
                    db.Account_CreationPendingss.AddOrUpdate(my);
                    //stored procedure

                    System.Web.HttpContext.Current.Session["NOT"] = my.CDSC_Number + " has been processed successfully";
                    await db.SaveChangesAsync();
                    db.Database.ExecuteSqlCommand("Exec NewAccounts @cust_no='" + my.ID_ + "'");
                    UpdateMember(my.CDSC_Number, my.ID_.ToString());
                }
                
            }
            else if (my.update_type == "TOUPDATE")
            {

                my.update_type = "NEW";

                db.Account_CreationPendingss.AddOrUpdate(my);
                //stored procedure
                System.Web.HttpContext.Current.Session["NOT"] = my.CDSC_Number + " Updated changes have been processed successfully";
                await db.SaveChangesAsync();
                db.Database.ExecuteSqlCommand("Exec NewAccountsAuth @cust_no='" + my.CDSC_Number + "'");
                //afterupdate
                UpdateMember(my.CDSC_Number, my.ID_.ToString());
                //
                var cc = db.LogDetails.ToList().Where(a => a.PropertyName=="CDSC_Number" && a.NewValue==my.CDSC_Number).OrderByDescending(a=>a.Id).Take(1);

                string val = "";
                string val2 = "";
                foreach (var p in cc)
                {
                    val = p.OriginalValue;
                    val2 = p.NewValue;
                }

                if (val != "")
                {
              db.Database.ExecuteSqlCommand("Exec OverallUpdate @pnew='" + my.CDSC_Number + "',@prev='" + val + "'");
                    UpdateMemberSS(val, val2);
                }
                //update account_creation

                
            }
            else
            {

                System.Web.HttpContext.Current.Session["NOT"] = my.CDSC_Number + " No details have been updated for authorization";

            }
            return RedirectToAction("Index");
        }


        protected void UpdateMemberSS(string c, string z)
        {
           List<Account_Creation> results = (from p in db.Account_Creations
                                                where p.CDSC_Number==c
                                                select p).ToList();
            WebReference.EscrowService escorw = new WebReference.EscrowService();
            foreach (Account_Creation p in results)
            {
                p.CDSC_Number = z;
                
            }
            db.SaveChanges(WebSecurity.CurrentUserName);

        }

        protected void UpdateMember(string c, string z)
        {




            List<Accounts_Documents> results = (from p in db.Accounts_Documentss
                                                where p.doc_generated == z
                                                select p).ToList();
            WebReference.EscrowService escorw = new WebReference.EscrowService();
            foreach (Accounts_Documents p in results)
            {
                p.doc_generated = c;
                try
                {
                    //var edc2 = escorw.SubmitDocuments(p.doc_generated, Convert.ToBase64String(p.Data), p.Name, p.ContentType);

                }
                catch (Exception)
                {

                }
               
            }

            db.SaveChanges(WebSecurity.CurrentUserName);

            List<Accounts_Joint> results2 = (from p in db.Accounts_Joints
                                             where p.CDSNo == z
                                             select p).ToList();
            foreach (Accounts_Joint p in results2)
            {
                p.CDSNo = c;
            
            }
            db.SaveChanges(WebSecurity.CurrentUserName);

            //send to CDS

            var dbz = db.Accounts_Joints.ToList().Where(a => a.CDSNo == c);
            string validate = "";
            foreach (var p in dbz)
            {
                try
                {
                    /*DateTime? dt1 = p.DateOfBirth;
                    string dd1 = dt1.Value.Year.ToString() + "-" + dt1.Value.Month.ToString() + "-" + dt1.Value.Day.ToString();
                    var client = new RestClient("http://localhost/BrokerService");
                    var request = new RestRequest("Joint/{Surname}/{Forenames}/{IDNo}/{IDType}/{Nationality}/{DateOfBirth}/{Gender}/{CDSNo}/{email}", Method.POST);
                    request.AddUrlSegment("Surname", p.Surname.ToString().Replace(" ", string.Empty).Replace(":", string.Empty));
                    request.AddUrlSegment("Forenames", p.Forenames.ToString().Replace(" ", string.Empty).Replace(":", string.Empty));
                    request.AddUrlSegment("IDNo", p.IDNo.ToString().Replace(" ", string.Empty).Replace(":", string.Empty));
                    request.AddUrlSegment("IDType", p.IDType.ToString().Replace(" ", string.Empty).Replace(":", string.Empty));
                    request.AddUrlSegment("Nationality", p.Nationality.ToString().Replace(" ", string.Empty).Replace(":", string.Empty));
                    request.AddUrlSegment("DateOfBirth", dd1.ToString().Replace(" ", string.Empty).Replace(":", string.Empty));
                    request.AddUrlSegment("Gender", p.Gender);
                    request.AddUrlSegment("CDSNo", p.CDSNo);
                    request.AddUrlSegment("email", p.email.ToString().Replace(" ", string.Empty).Replace(":", string.Empty).Replace(".", "-"));
                    IRestResponse response = client.Execute(request);
                    validate = response.Content;*/
                }
                catch (Exception)
                {


                }
            }


        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
