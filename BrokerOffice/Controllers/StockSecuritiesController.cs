﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BrokerOffice.DAO;
using BrokerOffice.Models;
using BrokerOffice.DAO.security;

namespace BrokerOffice.Controllers
{
    [CustomAuthorize(Roles = "Admin", NotifyUrl = "/UnauthorizedPage")]
    // [CustomAuthorize(Users = "1,2")]
    public class StockSecuritiesController : Controller
    {
        private SBoardContext db = new SBoardContext();

        // GET: StockSecurities
        public async Task<ActionResult> Index()
        {
            return View(await db.StockSecuritiess.ToListAsync());
        }

        // GET: StockSecurities/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StockSecurities stockSecurities = await db.StockSecuritiess.FindAsync(id);
            if (stockSecurities == null)
            {
                return HttpNotFound();
            }
            return View(stockSecurities);
        }

        // GET: StockSecurities/Create
        public ActionResult Create()
        {
            var list2 = db.para_issuers.ToList();
            //Create List of SelectListItem
            List<SelectListItem> selectlist2 = new List<SelectListItem>();
            selectlist2.Add(new SelectListItem() { Text ="", Value = "" });
            foreach (var row in list2)
            {
                //Adding every record to list  
                selectlist2.Add(new SelectListItem { Text = row.Company, Value = row.Company.ToString() });
            }

            ViewBag.Category = selectlist2;
            var list3 = db.StockTypes.ToList();
            //Create List of SelectListItem
            List<SelectListItem> selectlist3 = new List<SelectListItem>();
            selectlist3.Add(new SelectListItem() { Text = "", Value = "" });
            foreach (var row in list3)
            {
                //Adding every record to list  
                selectlist3.Add(new SelectListItem { Text = row.stockname, Value = row.stockname.ToString() });
            }

            ViewBag.StockType = selectlist3;
            return View();
        }

        // POST: StockSecurities/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "StockSecuritiesID,StockISIN,Issuer,StockType,description,stockQty,StockPrice,Stockstatus,Numberofshares,StockDetails")] StockSecurities stockSecurities)
        {
            if (ModelState.IsValid)
            {
                //var e = db.para_issuers.ToList().Where(a => a.Company == stockSecurities.Issuer);
                //foreach (var k in e)
                //{
                //    stockSecurities.StockISIN = k.ISIN;
                //}
                stockSecurities.Stockstatus = "Pending";
                db.StockSecuritiess.Add(stockSecurities);
                await db.SaveChangesAsync();
                System.Web.HttpContext.Current.Session["NOT"] = "You have successfully added the Stock Security";

                return RedirectToAction("Index");
            }
            var list2 = db.para_issuers.ToList();
            //Create List of SelectListItem
            List<SelectListItem> selectlist2 = new List<SelectListItem>();
            selectlist2.Add(new SelectListItem() { Text = Request["Issuer"], Value = Request["Issuer"] });
            foreach (var row in list2)
            {
                //Adding every record to list  
                selectlist2.Add(new SelectListItem { Text = row.Company, Value = row.Company.ToString() });
            }

            ViewBag.Category = selectlist2;

            var list3 = db.StockTypes.ToList();
            //Create List of SelectListItem
            List<SelectListItem> selectlist3 = new List<SelectListItem>();
            selectlist3.Add(new SelectListItem() { Text = Request["StockType"], Value =Request ["StockType"] });
            foreach (var row in list3)
            {
                //Adding every record to list  
                selectlist3.Add(new SelectListItem { Text = row.stockname, Value = row.stockname.ToString() });
            }

            ViewBag.StockType = selectlist3;
            return View(stockSecurities);
        }

        // GET: StockSecurities/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StockSecurities stockSecurities = await db.StockSecuritiess.FindAsync(id);
            if (stockSecurities == null)
            {
                return HttpNotFound();
            }
            var list2 = db.para_issuers.ToList();
            //Create List of SelectListItem
            List<SelectListItem> selectlist2 = new List<SelectListItem>();
            selectlist2.Add(new SelectListItem() { Text =stockSecurities.Issuer, Value = stockSecurities.Issuer });
            foreach (var row in list2)
            {
                //Adding every record to list  
                selectlist2.Add(new SelectListItem { Text = row.Company, Value = row.Company.ToString() });
            }

            ViewBag.Category = selectlist2;
            var list3 = db.StockTypes.ToList();
            //Create List of SelectListItem
            List<SelectListItem> selectlist3 = new List<SelectListItem>();
            selectlist3.Add(new SelectListItem() { Text =stockSecurities.StockType, Value = stockSecurities.StockType });
            foreach (var row in list3)
            {
                //Adding every record to list  
                selectlist3.Add(new SelectListItem { Text = row.stockname, Value = row.stockname.ToString() });
            }

            ViewBag.StockType = selectlist3;
            return View(stockSecurities);
        }

        // POST: StockSecurities/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "StockSecuritiesID,StockISIN,Issuer,StockType,description,stockQty,StockPrice,Stockstatus,Numberofshares,StockDetails")] StockSecurities stockSecurities)
        {
            if (ModelState.IsValid)
            {
                //var e = db.para_issuers.ToList().Where(a => a.Company == stockSecurities.Issuer);
                //foreach (var k in e)
                //{
                //    stockSecurities.StockISIN = k.ISIN;
                //}
                db.Entry(stockSecurities).State = EntityState.Modified;
                await db.SaveChangesAsync();
                System.Web.HttpContext.Current.Session["NOT"] = "You have successfully updated the Stock Security";

                return RedirectToAction("Index");
            }
            var list2 = db.para_issuers.ToList();
            //Create List of SelectListItem
            List<SelectListItem> selectlist2 = new List<SelectListItem>();
            selectlist2.Add(new SelectListItem() { Text = Request["Issuer"], Value = Request["Issuer"] });
            foreach (var row in list2)
            {
                //Adding every record to list  
                selectlist2.Add(new SelectListItem { Text = row.Company, Value = row.Company.ToString() });
            }

            ViewBag.Category = selectlist2;
            var list3 = db.StockTypes.ToList();
            //Create List of SelectListItem
            List<SelectListItem> selectlist3 = new List<SelectListItem>();
            selectlist3.Add(new SelectListItem() { Text = Request["StockType"], Value = "StockType" });
            foreach (var row in list3)
            {
                //Adding every record to list  
                selectlist3.Add(new SelectListItem { Text = row.stockname, Value = row.stockname.ToString() });
            }

            ViewBag.StockType = selectlist3;
            return View(stockSecurities);
        }

        // GET: StockSecurities/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StockSecurities stockSecurities = await db.StockSecuritiess.FindAsync(id);
            if (stockSecurities == null)
            {
                return HttpNotFound();
            }
            return View(stockSecurities);
        }

        // POST: StockSecurities/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            StockSecurities stockSecurities = await db.StockSecuritiess.FindAsync(id);
            db.StockSecuritiess.Remove(stockSecurities);
            await db.SaveChangesAsync();
            System.Web.HttpContext.Current.Session["NOT"] = "You have successfully deleted the Stock Security";

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
