﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BrokerOffice.DAO;
using BrokerOffice.Models;
using BrokerOffice.DAO.security;

namespace BrokerOffice.Controllers
{// [CustomAuthorize(RolesConfigKey = "RolesConfigKey")]
 // [CustomAuthorize(UsersConfigKey = "UsersConfigKey")]
    [CustomAuthorize(Roles = "User,Approver,BrokerAdmin", NotifyUrl = " /UnauthorizedPage")]
    // [CustomAuthorize(Users = "1,2")]

    public class BrokerClassesController : Controller
    {
        private SBoardContext db = new SBoardContext();

        // GET: BrokerClasses
        public async Task<ActionResult> Index()
        {
            ViewBag.csd = System.Web.HttpContext.Current.Session["csd"].ToString();

            ViewBag.nme = System.Web.HttpContext.Current.Session["nme"].ToString();
            string nme = System.Web.HttpContext.Current.Session["csd"].ToString();


            return View(await db.BrokerClasss.Where(a => a.ATPCSD == nme).ToListAsync());
        }

        // GET: BrokerClasses/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BrokerClass brokerClass = await db.BrokerClasss.FindAsync(id);
            if (brokerClass == null)
            {
                return HttpNotFound();
            }
            return View(brokerClass);
        }

        // GET: BrokerClasses/Create
        public ActionResult Create()
        {
            ViewBag.nme = System.Web.HttpContext.Current.Session["csd"].ToString();

            //ViewBag.Exchange = regionD();
            return View();
        }

        // POST: BrokerClasses/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "BrokerClassID,BrokerName,ATPCSD")] BrokerClass brokerClass)
        {
            if (ModelState.IsValid)
            {
                db.BrokerClasss.Add(brokerClass);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            //ViewBag.Exchange = regionD(brokerClass.BrokerName, brokerClass.BrokerName);
            ViewBag.nme = System.Web.HttpContext.Current.Session["csd"].ToString();

            return View(brokerClass);
        }

        // GET: BrokerClasses/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BrokerClass brokerClass = await db.BrokerClasss.FindAsync(id);
            if (brokerClass == null)
            {
                return HttpNotFound();
            }
            //ViewBag.Exchange = regionD(brokerClass.BrokerName,brokerClass.BrokerName);
            ViewBag.nme = System.Web.HttpContext.Current.Session["csd"].ToString();

            return View(brokerClass);
        }

        // POST: BrokerClasses/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "BrokerClassID,BrokerName,ATPCSD")] BrokerClass brokerClass)
        {
            if (ModelState.IsValid)
            {
                db.Entry(brokerClass).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            // ViewBag.Exchange = regionD(brokerClass.BrokerName, brokerClass.BrokerName);
            ViewBag.nme = System.Web.HttpContext.Current.Session["nme"].ToString();

            return View(brokerClass);
        }

        // GET: BrokerClasses/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BrokerClass brokerClass = await db.BrokerClasss.FindAsync(id);
            if (brokerClass == null)
            {
                return HttpNotFound();
            }
            return View(brokerClass);
        }

        // POST: BrokerClasses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            BrokerClass brokerClass = await db.BrokerClasss.FindAsync(id);
            db.BrokerClasss.Remove(brokerClass);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }
        public List<SelectListItem> regionD()
        {


            List<SelectListItem> phy = new List<SelectListItem>();
            var p = (from v in db.Account_Creations
                     where v.manAccount != ""
                     select new { v.manAccount }).Distinct();
            //phy.Add(new SelectListItem { Text = m, Value = n });

            foreach (var z in p)
            {
                phy.Add(new SelectListItem { Text = z.manAccount, Value = z.manAccount });

            }
            return phy;
        }
        public List<SelectListItem> regionD(string m, string n)
        {


            List<SelectListItem> phy = new List<SelectListItem>();
            var p = (from v in db.Account_Creations
                     where v.manAccount != ""
                     select new { v.manAccount}).Distinct();
            phy.Add(new SelectListItem { Text = m, Value = n });

            foreach (var z in p)
            {
                phy.Add(new SelectListItem { Text = z.manAccount, Value = z.manAccount });

            }
            return phy;
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
