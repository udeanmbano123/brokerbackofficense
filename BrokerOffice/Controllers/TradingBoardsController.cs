﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BrokerOffice.DAO;
using BrokerOffice.Models;
using BrokerOffice.DAO.security;

namespace BrokerOffice.Controllers
{// [CustomAuthorize(RolesConfigKey = "RolesConfigKey")]
        // [CustomAuthorize(UsersConfigKey = "UsersConfigKey")]
        [CustomAuthorize(Roles = "Admin", NotifyUrl = "/UnauthorizedPage")]
        // [CustomAuthorize(Users = "1,2")]
    public class TradingBoardsController : Controller
    {
        private SBoardContext db = new SBoardContext();

        // GET: TradingBoards
        public async Task<ActionResult> Index()
        {
            var tradingBoards = db.TradingBoards;
            return View(await tradingBoards.ToListAsync());
        }

        // GET: TradingBoards/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TradingBoard tradingBoard = await db.TradingBoards.FindAsync(id);
            if (tradingBoard == null)
            {
                return HttpNotFound();
            }
            return View(tradingBoard);
        }

        // GET: TradingBoards/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TradingBoards/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "TradingBoardID,BoardCode,BoardName,BoardDescription")] TradingBoard tradingBoard)
        {
            if (ModelState.IsValid)
            {
                db.TradingBoards.Add(tradingBoard);
                await db.SaveChangesAsync();
                System.Web.HttpContext.Current.Session["NOT"] = "You have successfully added the Trading Board";

                return RedirectToAction("Index");
            }

            return View(tradingBoard);
        }

        // GET: TradingBoards/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TradingBoard tradingBoard = await db.TradingBoards.FindAsync(id);
            if (tradingBoard == null)
            {
                return HttpNotFound();
            }
            return View(tradingBoard);
        }

        // POST: TradingBoards/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "TradingBoardID,BoardCode,BoardName,BoardDescription")] TradingBoard tradingBoard)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tradingBoard).State = EntityState.Modified;
                await db.SaveChangesAsync();
                System.Web.HttpContext.Current.Session["NOT"] = "You have successfully updated the Trading Board";

                return RedirectToAction("Index");
            }
            return View(tradingBoard);
        }

        // GET: TradingBoards/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TradingBoard tradingBoard = await db.TradingBoards.FindAsync(id);
            if (tradingBoard == null)
            {
                return HttpNotFound();
            }
            return View(tradingBoard);
        }

        // POST: TradingBoards/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            TradingBoard tradingBoard = await db.TradingBoards.FindAsync(id);
            db.TradingBoards.Remove(tradingBoard);
            await db.SaveChangesAsync();
            System.Web.HttpContext.Current.Session["NOT"] = "You have successfully deleted the Trading Board";

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
