﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BrokerOffice.DAO;
using BrokerOffice.Models;
using System.Web.Security;
using BrokerOffice.DAO.security;
using WebMatrix.WebData;
using System.Data.Entity.Migrations;
using System.Data.OleDb;
using System.IO;
using DevExpress.DashboardCommon.Native;
using OfficeOpenXml;

namespace BrokerOffice.Controllers
{
    // [CustomAuthorize(RolesConfigKey = "RolesConfigKey")]
    // [CustomAuthorize(UsersConfigKey = "UsersConfigKey")]
    [CustomAuthorize(Roles = "User,General,BrokerAdmin2,BrokerAdmin,Approver,ApproverUser", NotifyUrl = "/UnauthorizedPage")]
    // [CustomAuthorize(Users = "1,2")]

    public class BatchTransactionsController : Controller
    {
        private SBoardContext db = new SBoardContext();

        // GET: BatchTransactions
        public async Task<ActionResult> Index()
        {
         ViewBag.Batch = System.Web.HttpContext.Current.Session["Batch"].ToString();
            bool nc = false;
            // check batch status
            var pp = db.BatchHeaders.ToList().Where(a=>a.BatchNo== ViewBag.Batch);
            ViewBag.Status = "";
            foreach (var c in pp)
            {
                ViewBag.Status = c.Status;
                nc = c.Auth;
            }
            string n = System.Web.HttpContext.Current.Session["Batch"].ToString();
            if (nc==false)
            {
              return Redirect("~/Accounting/TransJ.aspx?id="+ViewBag.Batch+"&&ff=New");

            }
          
            return View(await db.BatchTransactions.Where(a => a.BatchNo == n).ToListAsync());
        }


        public async Task<ActionResult> Commit(string batch)
        {
            decimal? sum = 0;
            decimal? p = 0;
            sum = db.BatchTransactions.ToList().Where(a => a.BatchNo == batch).Sum(a => a.Amount);

            p = db.BatchHeaders.ToList().Where(a => a.BatchNo == batch).Sum(a => a.Amount);
            // check for incomplete transactions
            var sums = db.BatchTransactions.ToList().Where(a => a.BatchNo == batch);

            foreach (var d in sums)
            {
                if (d.Account=="N/A")
                {
                    System.Web.HttpContext.Current.Session["NOT"] = "Batch transactions are incomplete";
                    return RedirectToAction("Index");
                }

                if (d.AccountD=="N/A")
                {
                    System.Web.HttpContext.Current.Session["NOT"] = "Batch transactions are incomplete";
                    return RedirectToAction("Index");}
            }

           
                var upp = db.BatchHeaders.ToList().Where(a => a.BatchNo == batch);
                foreach (var e in upp)
                {
                    BatchHeader see = db.BatchHeaders.Find(e.id);
                    e.Auth = true;
                    db.BatchHeaders.AddOrUpdate(see);
                    db.SaveChanges(WebSecurity.CurrentUserName.ToString());

                }
                var uppz = db.BatchTransactions.ToList().Where(a => a.BatchNo == batch);
                foreach (var e in uppz)
                {
                    BatchTransaction see = db.BatchTransactions.Find(e.ID);
                    e.Auth = true;
                    db.BatchTransactions.AddOrUpdate(see);
                    db.SaveChanges(WebSecurity.CurrentUserName.ToString());

                }
                System.Web.HttpContext.Current.Session["NOT"] = "The batch was processed successfully, awaiting authorization.";

            
            
            return Redirect("~/BatchHeaders/Index");
        }
        // GET: BatchTransactions/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BatchTransaction batchTransaction = await db.BatchTransactions.FindAsync(id);
            if (batchTransaction == null)
            {
                return HttpNotFound();
            }
            return View(batchTransaction);
        }

        // GET: BatchTransactions/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: BatchTransactions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,TrxnDate,Account,Reference,Description,Debit,Credit,CostCode,BankID,BankAccName,BatchNo,AccountName,trantype")] BatchTransaction batchTransaction)
        {
            if (ModelState.IsValid)
            {
                db.BatchTransactions.Add(batchTransaction);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(batchTransaction);
        }

        // GET: BatchTransactions/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BatchTransaction batchTransaction = await db.BatchTransactions.FindAsync(id);
            if (batchTransaction == null)
            {
                return HttpNotFound();
            }
            return View(batchTransaction);
        }

        // POST: BatchTransactions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,TrxnDate,Account,Reference,Description,Debit,Credit,CostCode,BankID,BankAccName,BatchNo,AccountName,trantype")] BatchTransaction batchTransaction)
        {
            if (ModelState.IsValid)
            {
                db.Entry(batchTransaction).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(batchTransaction);
        }

        // GET: BatchTransactions/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BatchTransaction batchTransaction = await db.BatchTransactions.FindAsync(id);
            if (batchTransaction == null)
            {
                return HttpNotFound();
            }
            return View(batchTransaction);
        }

        // POST: BatchTransactions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            BatchTransaction batchTransaction = await db.BatchTransactions.FindAsync(id);
            db.BatchTransactions.Remove(batchTransaction);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult Upload(FormCollection formCollection)
        {
            int total = 0;

            var chk = 0;
            int me = 0;
            string fileExtension = System.IO.Path.GetExtension(Request.Files["FileUpload"].FileName);
        
            try
            {
                chk = db.BatchTransactions.ToList().Where(a => a.filename == Request.Files["FileUpload"].FileName).Count();

            }
            catch (Exception)
            {

                chk = 0;
            }
            if (chk > 0)
            {
                System.Web.HttpContext.Current.Session["NOT"] = "File with name has already been uploaded " + Request.Files["FileUpload"].FileName;

                return RedirectToAction("Index");
            }
            try
            {

                if (fileExtension == ".xls" || fileExtension == ".xlsx" || fileExtension == ".csv")
                {

                    // Create a folder in App_Data named ExcelFiles because you need to save the file temporarily location and getting data from there. 
                    string fileLocation = Server.MapPath("~/File/" + Request.Files["FileUpload"].FileName + DateTime.Now.ToString("ddMMyyyyhhmmss"));

                    if (System.IO.File.Exists(fileLocation))
                        System.IO.File.Delete(fileLocation);

                    Request.Files["FileUpload"].SaveAs(fileLocation);
                    string excelConnectionString = string.Empty;

                    excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileLocation +
                                            ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                    //connection String for xls file format.
                    if (fileExtension == ".xls")
                    {
                        excelConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + fileLocation +
                                                ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
                    }
                    //connection String for xlsx file format.
                    else if (fileExtension == ".xlsx")
                    {

                        excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileLocation +
                                                ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                    }
                    else if (fileExtension == ".txt")
                    {
                        excelConnectionString = "Provider = Microsoft.Jet.OLEDB.4.0; Data Source=" + fileLocation +
                                                ";Extended Properties=\"text;HDR=Yes;FMT=Delimited\"";
                    }
                    else if (fileExtension == ".csv")
                    {
                        string fileName = Path.Combine(Server.MapPath("~/CSV"), Guid.NewGuid().ToString() + ".csv");

                        try
                        {
                            System.IO.File.Delete(fileName);
                            System.IO.File.Delete(fileLocation);

                        }
                        catch (Exception)
                        {


                        }
                        System.Web.HttpContext.Current.Session["NOT"] = "Only xls,xlsx may be uploaded";

                        return RedirectToAction("Index");
                    }

                    //Create Conneon to Excel work book and add oledb namespace
                    OleDbConnection excelConnection = new OleDbConnection(excelConnectionString);

                    excelConnection.Open();

                    DataTable dt = new DataTable();

                    dt = excelConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    if (dt == null)
                    {
                        return null;
                    }

                    String[] excelSheets = new String[dt.Rows.Count];
                    int t = 0;
                    //excel data saves in temp file here.
                    foreach (DataRow row in dt.Rows)
                    {
                        excelSheets[t] = row["TABLE_NAME"].ToString();
                        t++;
                    }
                    OleDbConnection excelConnection1 = new OleDbConnection(excelConnectionString);

                    DataSet ds = new DataSet();

                    string query = string.Format("Select * from [{0}]", excelSheets[0]);
                    using (OleDbDataAdapter dataAdapter = new OleDbDataAdapter(query, excelConnection1))
                    {
                        dataAdapter.Fill(ds);
                    }


                    if (Request != null)
                    {
                        HttpPostedFileBase file = Request.Files["FileUpload"];
                        if ((file != null) && (file.ContentLength != 0) && !string.IsNullOrEmpty(file.FileName))
                        {
                            string fileName = file.FileName;
                            string fileContentType = file.ContentType;
                            byte[] fileBytes = new byte[file.ContentLength];
                            var data = file.InputStream.Read(fileBytes, 0, Convert.ToInt32(file.ContentLength));
                            var nm = System.Web.HttpContext.Current.Session["Batch"].ToString();
                            using (var package = new ExcelPackage(file.InputStream))
                            {
                                var currentSheet = package.Workbook.Worksheets;
                                var workSheet = currentSheet.First();
                                var noOfCol = workSheet.Dimension.End.Column;
                                var noOfRow = workSheet.Dimension.End.Row;
                                int n = noOfRow;
                                total = n - 1;
                                for (int rowIterator = 2; rowIterator < (n + 1); rowIterator++)
                                {

                                    try
                                    {

                                        BatchTransaction my = new BatchTransaction();

                                        try
                                        {
                                            string ne = workSheet.Cells[rowIterator, 1].Value.ToString();
                                            my.TrxnDate = Convert.ToDateTime(ne);

                                        }
                                        catch (Exception)
                                        {

                                        }
                                        my.Reference=workSheet.Cells[rowIterator, 2].Value.ToString();
                                        my.BankID=workSheet.Cells[rowIterator, 3].Value.ToString();
                                        my.BankAccName= workSheet.Cells[rowIterator, 4].Value.ToString();
                                        my.Account= workSheet.Cells[rowIterator, 5].Value.ToString();
                                        my.AccountD = workSheet.Cells[rowIterator, 6].Value.ToString();
                                        try
                                        {
                                            my.Amount = Convert.ToDecimal(workSheet.Cells[rowIterator, 7].Value.ToString());

                                        }
                                        catch (Exception)
                                        {

                                            my.Amount = 0;
                                        }
                                        my.Description = "Batch Transaction";
                                        my.Commit = false;
                                        my.Auth = false;
                                        my.AccountName = chkclientNN(my.Account);
                                        my.AccountNameD = chkclientNN(my.AccountD);
                                        my.trantype= chkclientNNT(my.Account);
                                        my.trantypeD = chkclientNNT(my.AccountD);
                                        my.BatchNo =nm;
                                        my.filename = file.FileName;
                                        if (my.trantype == "")
                                        {
                                            System.Web.HttpContext.Current.Session["NOT"] = "File has an invalid account " + my.Account.ToString();

                                            break;
                                        }
                                        if (my.trantypeD == "")
                                        {
                                            System.Web.HttpContext.Current.Session["NOT"] = "File has an invalid account " + my.AccountD.ToString();
                                            break;
                                        }
                                        if (my.TrxnDate == null)
                                        {
                                            System.Web.HttpContext.Current.Session["NOT"] = "Missing date on row " + (me-1).ToString();
                                            break;
                                        }

                                        try
                                        {
                                            db.Database.Connection.Close();
                                        }
                                        catch (Exception)
                                        {


                                        }
                                        try
                                        {
                                            db.Database.Connection.Open();
                                        }
                                        catch (Exception)
                                        {


                                        }

                                        db.BatchTransactions.Add(my);
                                        db.SaveChanges(WebSecurity.CurrentUserName.ToString());
                                        me += 1;
                                    }
                                    catch (Exception)
                                    {

                                        throw;
                                    }


                                }

                                System.Web.HttpContext.Current.Session["NOT"] = "You have successfully uploaded " + (me - 1).ToString() + " transactions for the batch";


                                try
                                {

                                }
                                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                                {

                                    Exception raise = dbEx;
                                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                                    {
                                        foreach (var validationError in validationErrors.ValidationErrors)
                                        {
                                            string message = string.Format("{0}:{1}",
                                                validationErrors.Entry.Entity.ToString(),
                                                validationError.ErrorMessage);
                                            // raise a new exception nesting
                                            // the current instance as InnerException
                                            raise = new InvalidOperationException(message, raise);
                                        }
                                    }
                                    throw raise;
                                }

                            }
                        }
                    }



                }
                else
                {

                }

            }
            catch (Exception f)
            {

                System.Web.HttpContext.Current.Session["NOT"] = "You have successfully uploaded " + (me - 1).ToString() + " transactions for the batch";
                // System.Web.HttpContext.Current.Session["NOT"] = f.Message;



                throw;

            }

            try
            {
                var fileLocation = Server.MapPath("~/File/" + Request.Files["FileUpload"].FileName);
                System.IO.File.Delete(fileLocation);
            }
            catch (Exception)
            {


            }
            return RedirectToAction("Index");
        }


        public bool chkclient(string m)
        {
            bool tru = true;
            var ff = 0;

            try
            {
                ff = db.Account_Creations.ToList().Where(a => a.CDSC_Number == m).Count();

            }
            catch (Exception)
            {

                ff = 0;
                tru = false;
            }
            if (ff == 0)
            {
                tru = false;
            }

            return tru;

        }
        public string chkclientN(string m)
        {
            string tru = "";
            var ff = db.Account_Creations.ToList().Where(a => a.CDSC_Number == m).ToList();
                
            foreach (var d in ff)
            {
                tru = d.Surname_CompanyName + "" + d.OtherNames;
            }

            return tru;

        }

        public string chkclientNN(string m)
        {
            string tru = "";
            var ff = db.Account_Creations.ToList().Where(a => a.CDSC_Number == m).ToList();

            foreach (var d in ff)
            {
                tru = d.Surname_CompanyName + "" + d.OtherNames;
            }
            //check GL

            if(tru==""){

            }
            var mm = db.Accounts_Masters.ToList().Where(a => a.AccountNumber.ToString() == m).ToList();

            foreach (var d in mm)
            {
                tru = d.AccountName;
            }
            return tru;

        }
        public string chkclientNNT(string m)
        {
            string tru = "";
            string tho = "";
            var ff = db.Account_Creations.ToList().Where(a => a.CDSC_Number == m).ToList();

            foreach (var d in ff)
            {
                tru = d.Surname_CompanyName + "" + d.OtherNames;
                tho= "Client";
            }
            //check GL

            if (tru == "")
            {

            }
            var mm = db.Accounts_Masters.ToList().Where(a => a.AccountNumber.ToString() == m).ToList();

            foreach (var d in mm)
            {
                tru = d.AccountName;
                tho = "GL";
            }
            return tho;

        }
    }
}