﻿using BrokerOffice.CDSMOdel;
using BrokerOffice.DAO;
using BrokerOffice.DAO.security;
using BrokerOffice.Models;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMatrix.WebData;

namespace BrokerOffice.Controllers
{ // [CustomAuthorize(RolesConfigKey = "RolesConfigKey")]
        // [CustomAuthorize(UsersConfigKey = "UsersConfigKey")]
        [CustomAuthorize(Roles = "User", NotifyUrl = "/UnauthorizedPage")]
        // [CustomAuthorize(Users = "1,2")]
    public class UserController : Controller
    {
       

        private SBoardContext db = new SBoardContext();


        public ActionResult Index()
        {
            string n1 = "";
            string n2 = "";
            try
            {
                string name = WebSecurity.CurrentUserName;
                var user = db.Users.ToList().Where(a => a.Email == name);
                ViewBag.Users = "";
                foreach (var row in user)
                {
                    ViewBag.Users = row.FirstName + " " + row.LastName;
                }
                var pstord = db.Order_Lives.ToList().Count();

                ViewBag.Orders = pstord.ToString();
                var c = db.Users.ToList().Where(a => a.Email == WebSecurity.CurrentUserName);
                string vx = "";
                foreach (var d in c)
                {
                    vx = d.BrokerCode;
                }
                var list3 = "";
                var client = new RestClient("http://localhost/BrokerService");
                var request = new RestRequest("BrokersList", Method.GET);
                IRestResponse response = client.Execute(request);
                string validate = response.Content;
                List<Broker> dataList = JsonConvert.DeserializeObject<List<Broker>>(validate);


                var dbsel = from s in dataList
                            select s;
                //updatebilling();
                foreach (var d in dbsel)
                {
                    if (vx == d.Company_Code)
                    {
                        n1 = d.Company_name;
                        n2 = d.Company_Code;
                    }
                }
            }
            catch (Exception)
            {


            }
            ViewBag.Broker = n1;
            ViewBag.BrokerCode = n2;
            ViewBag.RegC = db.Account_Creations.ToList().Where(a=>a.Broker==n2).Count();
            ViewBag.RegA = db.Account_CreationPendingss.ToList().Where(a=>a.Broker==n2 && a.update_type== "TOUPDATE").Count();
            ViewBag.MB = db.TblDealss.ToList().Where(a => a.buybroker == n2).Count();
            ViewBag.MS= db.TblDealss.ToList().Where(a => a.sellbroker == n2).Count();
            ViewBag.SB = (from s in db.DealerDGs
                          join v in db.Account_Creations on s.Account1 equals v.CDSC_Number
                          where v.Broker ==n2
                          select s).Count();
            ViewBag.SS= (from s in db.DealerDGs
                         join v in db.Account_Creations on s.Account2 equals v.CDSC_Number
                         where v.Broker == n2
                         select s).Count();
            ViewBag.PB = db.Order_Lives.ToList().Where(a => a.OrderType == "Buy" && a.Broker_Code == n2).Count();
            ViewBag.PS= db.Order_Lives.ToList().Where(a => a.OrderType == "Sell" && a.Broker_Code == n2).Count();
            return View();
        }
        public void reoveaccount(string s)
        {
            int my = 0;
            var c = db.Account_Creations.ToList().Where(a => a.CDSC_Number == s);
            foreach (var p in c)
            {
                my = p.ID_;
            }

            try
            {
                Account_Creation account_Creation = db.Account_Creations.Find(my);
                account_Creation.StatuSActive = true;
                db.Account_Creations.AddOrUpdate(account_Creation);
                db.SaveChangesAsync();
            }
            catch (Exception)
            {


            }
        }

        public void updatebilling()
        {
            var client = new RestClient("http://localhost/BrokerService");
            var request = new RestRequest("Billing", Method.GET);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            List<para_Billing> dataList = JsonConvert.DeserializeObject<List<para_Billing>>(validate);


            var pers = from s in dataList
                       select s;

            foreach(var k in pers)
            {
                List<TradingCharges> p= (from s in  db.TradingChargess
                                        where s.ChargeName == k.ChargeName
                                        select s).ToList();

                foreach(TradingCharges c in p)
                {
                    TradingCharges my = db.TradingChargess.Find(c.TradingChargesID);
                    c.chargevalue = k.PercentageOrValue;
                    c.chargecode = k.ChargeCode;
                    if (k.ApplyTo == "BOTH")
                    {
                        c.tradeside = "Both";
                    }else if (k.ApplyTo == "BUYER")
                    {
                        c.tradeside = "Buy";
                    }
                    else if (k.ApplyTo == "SELLER")
                    {
                        c.tradeside = "Sell";
                    }
                    if (k.Indicator == "PERCENT")
                    {
                        c.ChargedAs = "Percentage";
                    }
                    else if (k.Indicator == "FLAT")
                    {
                        c.ChargedAs = "Flat";
                    }
                    db.TradingChargess.AddOrUpdate(my);
                    db.SaveChanges(WebSecurity.CurrentUserName);
                }
            }
            
        }

        
        //// GET: Report
        //public ActionResult UserReport()
        //{
        //    var list = from u in db.Saccoes
        //               join v in db.saccauths on u.ID equals v.SaccoId
        //               where v.status == "APPROVED"
        //               select u;
        //    //Create List of SelectListItem
        //    List<SelectListItem> selectlist = new List<SelectListItem>();
        //    selectlist.Add(new SelectListItem() { Text = "", Value = "" });
        ////    foreach (var row in list)
        ////    {
        ////        //Adding every record to list  
        ////        selectlist.Add(new SelectListItem { Text = row.Sacconame, Value = row.Sacconame.ToString() });
        ////    }

        ////    ViewBag.Role = selectlist;
        ////    return View();
        ////}

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult UserReport([Bind(Include = "ReportName,AsAt")] Report registerreport)
        //{
        //    if (ModelState.IsValid)
        //    {

        //    }
        //    else
        //    {

        //    }
        //    var list = db.Saccoes.ToList();
        //    //Create List of SelectListItem
        //    List<SelectListItem> selectlist = new List<SelectListItem>();
        //    selectlist.Add(new SelectListItem() { Text = "", Value = "" });
        //    foreach (var row in list)
        //    {
        //        //Adding every record to list  
        //        selectlist.Add(new SelectListItem { Text = row.Sacconame, Value = row.Sacconame.ToString() });
        //    }
        //    ViewBag.Role = selectlist;
        //    return View();
        //}
    }
}