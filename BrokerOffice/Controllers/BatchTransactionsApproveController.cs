﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BrokerOffice.DAO;
using BrokerOffice.Models;
using System.Web.Security;
using BrokerOffice.DAO.security;
using WebMatrix.WebData;
using System.Data.Entity.Migrations;

namespace BrokerOffice.Controllers
{
    // [CustomAuthorize(RolesConfigKey = "RolesConfigKey")]
    // [CustomAuthorize(UsersConfigKey = "UsersConfigKey")]
    [CustomAuthorize(Roles = "User,Approver,BrokerAdmin,BrokerAdmin2", NotifyUrl = " /UnauthorizedPage")]
    // [CustomAuthorize(Users = "1,2")]

    public class BatchTransactionsApproveController : Controller
    {
        private SBoardContext db = new SBoardContext();

        // GET: BatchTransactions
        public async Task<ActionResult> Index()
        {
         ViewBag.Batch = System.Web.HttpContext.Current.Session["Batch"].ToString();
            // check batch status
            var pp = db.BatchHeaders.ToList().Where(a=>a.BatchNo== ViewBag.Batch);
            ViewBag.Status = "";
            foreach (var c in pp)
            {
                ViewBag.Status = c.Status;
            }
            string n = System.Web.HttpContext.Current.Session["Batch"].ToString();
            return View(await db.BatchTransactions.Where(a=>a.BatchNo==n).ToListAsync());
        }

        public async Task<ActionResult> Commit(string batch)
        {
            decimal? sum = 0;
            decimal? p = 0;
            sum = db.BatchTransactions.ToList().Where(a => a.BatchNo == batch).Sum(a => a.Amount);

            p = db.BatchHeaders.ToList().Where(a => a.BatchNo == batch).Sum(a => a.Amount);

           
                //commiting
                var z = db.BatchTransactions.ToList().Where(a => a.BatchNo == batch);
                foreach (var t in z)
                {
                    BrokerOffice.Models.Trans my = new BrokerOffice.Models.Trans();
                    //Debit
                    my.Account = t.AccountD;
                    my.Category = t.trantypeD;
                    my.Credit = 0;
                    my.Debit = t.Amount;
                    my.Narration = t.Description;
                    my.Reference_Number = t.BatchNo;
                    my.TrxnDate = t.TrxnDate;
                    my.Post = DateTime.Now;
                    my.Type = "Journl Entry";
                    my.PostedBy = WebSecurity.CurrentUserName;
                    db.Transs.Add(my);

                    //Update table  
                    db.SaveChanges(WebSecurity.CurrentUserName.ToString());
                    // Credit//
                    my.Account = t.Account;
                    my.Category = t.trantype;
                    my.Credit = t.Amount;
                    my.Debit = 0;
                    my.Narration = t.Description;
                    my.Reference_Number = t.BatchNo;
                    my.TrxnDate = t.TrxnDate;
                    my.Post = DateTime.Now;
                    my.Type = "Journl Entry";
                    my.PostedBy = WebSecurity.CurrentUserName;
                    db.Transs.Add(my);

                    //Update table  
                    db.SaveChanges(WebSecurity.CurrentUserName.ToString());

                    //Update reduce the batch

                    if (t.Account.Length>4)
                    {
                        string nme = "Accounts recievables";
                        var acc = db.Accounts_Masters.ToList().Where(a => a.AccountName.ToLower().Replace(" ", "").Contains(nme.ToLower().Replace(" ", "")));

                        int num = 0;
                        foreach (var c in acc)
                        {
                            num = c.AccountNumber;
                        }


                        //submit
                        BrokerOffice.Models.Trans my2 = new BrokerOffice.Models.Trans();
                        //Debit
                        my2.Account = num.ToString();
                        my2.Category = "Journl Entry";
                        my2.Credit = t.Amount;
                        my2.Debit = 0;
                        my2.Narration = t.Description;
                        my2.Reference_Number = t.BatchNo;
                        my2.TrxnDate = t.TrxnDate;
                        my2.Post = DateTime.Now;
                        my2.Type = "Journl Entry";
                        my2.PostedBy = WebSecurity.CurrentUserName;
                        db.Transs.Add(my2);

                        //Update table  
                        db.SaveChanges(WebSecurity.CurrentUserName.ToString());



                    }
                    else if(t.AccountD.Length > 4)
                    {
                        string nme = "Accounts payables";

                        var acc = db.Accounts_Masters.ToList().Where(a => a.AccountName.ToLower().Replace(" ", "").Contains(nme.ToLower().Replace(" ", "")));

                        int num = 0;
                        foreach (var c in acc)
                        {
                            num = c.AccountNumber;
                        }


                        //submit
                        BrokerOffice.Models.Trans my2 = new BrokerOffice.Models.Trans();
                        //Debit
                        my2.Account = num.ToString();
                        my2.Category = "Journl Entry";
                        my2.Credit = 0;
                        my2.Debit = t.Amount;
                        my2.Narration = t.Description;
                        my2.Reference_Number = t.BatchNo;
                        my2.TrxnDate = t.TrxnDate;
                        my2.Post = DateTime.Now;
                        my2.Type = "Journl Entry";

                        db.Transs.Add(my2);

                        //Update table  
                        db.SaveChanges(WebSecurity.CurrentUserName.ToString());
                        BatchTransaction see = db.BatchTransactions.Find(t.ID);
                        t.Commit = true;
                        db.BatchTransactions.AddOrUpdate(see);
                        db.SaveChanges(WebSecurity.CurrentUserName.ToString());

                    }
             

                  
                   
                }
                //update batch to commit
                var upp = db.BatchHeaders.ToList().Where(a=>a.BatchNo==batch);
                foreach (var e in upp)
                {
                    BatchHeader see = db.BatchHeaders.Find(e.id);
                    e.Status = true;
                    db.BatchHeaders.AddOrUpdate(see);
                    db.SaveChanges(WebSecurity.CurrentUserName.ToString());

                }
                var uppz = db.BatchTransactions.ToList().Where(a => a.BatchNo == batch);
                foreach (var e in uppz)
                {
                    BatchTransaction see = db.BatchTransactions.Find(e.ID);
                    e.Commit = true;
                    db.BatchTransactions.AddOrUpdate(see);
                    db.SaveChanges(WebSecurity.CurrentUserName.ToString());

                }
                System.Web.HttpContext.Current.Session["NOT"] = "The batch was approved and committed successfully";

           
            return Redirect("~/BatchHeadersApprove/Index");
        }
        // GET: BatchTransactions/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BatchTransaction batchTransaction = await db.BatchTransactions.FindAsync(id);
            if (batchTransaction == null)
            {
                return HttpNotFound();
            }
            return View(batchTransaction);
        }

        // GET: BatchTransactions/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: BatchTransactions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,TrxnDate,Account,Reference,Description,Debit,Credit,CostCode,BankID,BankAccName,BatchNo,AccountName,trantype")] BatchTransaction batchTransaction)
        {
            if (ModelState.IsValid)
            {
                db.BatchTransactions.Add(batchTransaction);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(batchTransaction);
        }

        // GET: BatchTransactions/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BatchTransaction batchTransaction = await db.BatchTransactions.FindAsync(id);
            if (batchTransaction == null)
            {
                return HttpNotFound();
            }
            return View(batchTransaction);
        }

        // POST: BatchTransactions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,TrxnDate,Account,Reference,Description,Debit,Credit,CostCode,BankID,BankAccName,BatchNo,AccountName,trantype")] BatchTransaction batchTransaction)
        {
            if (ModelState.IsValid)
            {
                db.Entry(batchTransaction).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(batchTransaction);
        }

        // GET: BatchTransactions/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BatchTransaction batchTransaction = await db.BatchTransactions.FindAsync(id);
            if (batchTransaction == null)
            {
                return HttpNotFound();
            }
            return View(batchTransaction);
        }

        // POST: BatchTransactions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            BatchTransaction batchTransaction = await db.BatchTransactions.FindAsync(id);
            db.BatchTransactions.Remove(batchTransaction);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        // GET: BatchTransactions/Delete/5
        public async Task<ActionResult> Discard(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BatchTransaction batchTransaction = await db.BatchTransactions.FindAsync(id);
            if (batchTransaction == null)
            {
                return HttpNotFound();
            }
            return View(batchTransaction);
        }

        // POST: BatchTransactions/Delete/5
        [HttpPost, ActionName("Discard")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DiscardConfirmed(int id)
        {
            BatchTransaction batchTransaction = await db.BatchTransactions.FindAsync(id);
            //db.BatchTransactions.Remove(batchTransaction);
            //await db.SaveChangesAsync();
            decimal? sum = 0;
            decimal? p = 0;
            string batch = batchTransaction.BatchNo;
            sum = db.BatchTransactions.ToList().Where(a => a.BatchNo == batch).Sum(a => a.Amount);

            p = db.BatchHeaders.ToList().Where(a => a.BatchNo == batch).Sum(a => a.Amount);

            if (p == sum)
            {
                //commiting
                var z = db.BatchTransactions.ToList().Where(a => a.BatchNo == batch);
                foreach (var t in z)
                {
                    BrokerOffice.Models.Trans my = new BrokerOffice.Models.Trans();
                    //Debit
                    my.Account = t.AccountD;
                    my.Category = t.trantypeD;
                    my.Credit = 0;
                    my.Debit = t.Amount;
                    my.Narration = t.Description;
                    my.Reference_Number = t.BatchNo;
                    my.TrxnDate = t.TrxnDate;
                    my.Post = DateTime.Now;
                    my.Type = "Journl Entry";
                    my.PostedBy = WebSecurity.CurrentUserName;
                    db.Transs.Add(my);

                    //Update table  
                    db.SaveChanges(WebSecurity.CurrentUserName.ToString());
                    // Credit//
                    my.Account = t.Account;
                    my.Category = t.trantype;
                    my.Credit = t.Amount;
                    my.Debit = 0;
                    my.Narration = t.Description;
                    my.Reference_Number = t.BatchNo;
                    my.TrxnDate = t.TrxnDate;
                    my.Post = DateTime.Now;
                    my.Type = "Journl Entry";
                    my.PostedBy = WebSecurity.CurrentUserName;
                    db.Transs.Add(my);

                    //Update table  
                    db.SaveChanges(WebSecurity.CurrentUserName.ToString());

                    //Update reduce the batch

                    if (t.trantype == "Client")
                    {
                        string nme = "Accounts recievables";
                        var acc = db.Accounts_Masters.ToList().Where(a => a.AccountName.ToLower().Replace(" ", "").Contains(nme.ToLower().Replace(" ", "")));

                        int num = 0;
                        foreach (var c in acc)
                        {
                            num = c.AccountNumber;
                        }


                        //submit
                        BrokerOffice.Models.Trans my2 = new BrokerOffice.Models.Trans();
                        //Debit
                        my2.Account = num.ToString();
                        my2.Category = "Journl Entry";
                        my2.Credit = t.Amount;
                        my2.Debit = 0;
                        my2.Narration = t.Description;
                        my2.Reference_Number = t.BatchNo;
                        my2.TrxnDate = t.TrxnDate;
                        my2.Post = DateTime.Now;
                        my2.Type = "Journl Entry";
                        my2.PostedBy = WebSecurity.CurrentUserName;
                        db.Transs.Add(my2);

                        //Update table  
                        db.SaveChanges(WebSecurity.CurrentUserName.ToString());



                    }
                    else if (t.trantypeD == "Client")
                    {
                        string nme = "Accounts payables";

                        var acc = db.Accounts_Masters.ToList().Where(a => a.AccountName.ToLower().Replace(" ", "").Contains(nme.ToLower().Replace(" ", "")));

                        int num = 0;
                        foreach (var c in acc)
                        {
                            num = c.AccountNumber;
                        }


                        //submit
                        BrokerOffice.Models.Trans my2 = new BrokerOffice.Models.Trans();
                        //Debit
                        my2.Account = num.ToString();
                        my2.Category = "Journl Entry";
                        my2.Credit = 0;
                        my2.Debit = t.Amount;
                        my2.Narration = t.Description;
                        my2.Reference_Number = t.BatchNo;
                        my2.TrxnDate = t.TrxnDate;
                        my2.Post = DateTime.Now;
                        my2.Type = "Journl Entry";

                        db.Transs.Add(my2);

                        //Update table  
                        db.SaveChanges(WebSecurity.CurrentUserName.ToString());
                        BatchTransaction see = db.BatchTransactions.Find(t.ID);
                        t.Commit = true;
                        db.BatchTransactions.AddOrUpdate(see);
                        db.SaveChanges(WebSecurity.CurrentUserName.ToString());

                    }




                }
                //update batch to commit
                var upp = db.BatchHeaders.ToList().Where(a => a.BatchNo == batch);
                foreach (var e in upp)
                {
                    BatchHeader see = db.BatchHeaders.Find(e.id);
                    e.Status = true;
                    db.BatchHeaders.AddOrUpdate(see);
                    db.SaveChanges(WebSecurity.CurrentUserName.ToString());

                }
                var uppz = db.BatchTransactions.ToList().Where(a => a.BatchNo == batch);
                foreach (var e in uppz)
                {
                    BatchTransaction see = db.BatchTransactions.Find(e.ID);
                    e.Commit = true;
                    db.BatchTransactions.AddOrUpdate(see);
                    db.SaveChanges(WebSecurity.CurrentUserName.ToString());

                }
                System.Web.HttpContext.Current.Session["NOT"] = "The batch was committed successfully";

            }
            else
            {
                System.Web.HttpContext.Current.Session["NOT"] = "The batch totals do not match";

            }
            return RedirectToAction("Index");
        }
        public async Task<ActionResult> Discarded(string batch)
        {
            decimal? sum = 0;
            decimal? p = 0;
            sum = db.BatchTransactions.ToList().Where(a => a.BatchNo == batch).Sum(a => a.Amount);

            p = db.BatchHeaders.ToList().Where(a => a.BatchNo == batch).Sum(a => a.Amount);
            //update batch to commit
            var upp = db.BatchHeaders.ToList().Where(a => a.BatchNo == batch);
            foreach (var e in upp)
            {
                BatchHeader see = db.BatchHeaders.Find(e.id);
                e.Auth = false;
                db.BatchHeaders.AddOrUpdate(see);
                db.SaveChanges(WebSecurity.CurrentUserName.ToString());

            }
            var uppz = db.BatchTransactions.ToList().Where(a => a.BatchNo == batch);
            foreach (var e in uppz)
            {
                BatchTransaction see = db.BatchTransactions.Find(e.ID);
                e.Auth = false;
                db.BatchTransactions.AddOrUpdate(see);
                db.SaveChanges(WebSecurity.CurrentUserName.ToString());

            }
            System.Web.HttpContext.Current.Session["NOT"] ="The batch authprixatiom was discarded.";

            return Redirect("~/BatchTransactions/Index");
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
