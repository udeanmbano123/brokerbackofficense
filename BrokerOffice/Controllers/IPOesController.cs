﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BrokerOffice.DAO;
using BrokerOffice.Models;

namespace BrokerOffice.Controllers
{
    public class IPOesController : Controller
    {
        private SBoardContext db = new SBoardContext();

        // GET: IPOes
        public async Task<ActionResult> Index()
        {
            return View(await db.IPOs.ToListAsync());
        }

        // GET: IPOes/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IPO iPO = await db.IPOs.FindAsync(id);
            if (iPO == null)
            {
                return HttpNotFound();
            }
            return View(iPO);
        }

        // GET: IPOes/Create
        public ActionResult Create()
        {
            ViewBag.Issu = Countries();
            return View();
        }

        // POST: IPOes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "IPOID,issuer,targetedvalue,OpenningDate,ClosingDate,ListingDate,openningprice")] IPO iPO)
        {
            if (ModelState.IsValid)
            {
                db.IPOs.Add(iPO);
                await db.SaveChangesAsync();
                System.Web.HttpContext.Current.Session["NOT"] = "You have successfully added the IPO";

                return RedirectToAction("Index");
            }
            ViewBag.Issu = CountriesM(iPO.issuer,iPO.issuer);
            return View(iPO);
        }

        // GET: IPOes/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IPO iPO = await db.IPOs.FindAsync(id);
            if (iPO == null)
            {
                return HttpNotFound();
            }
            ViewBag.Issu = CountriesM(iPO.issuer, iPO.issuer);
            return View(iPO);
        }

        // POST: IPOes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "IPOID,issuer,targetedvalue,OpenningDate,ClosingDate,ListingDate,openningprice")] IPO iPO)
        {
            if (ModelState.IsValid)
            {
                db.Entry(iPO).State = EntityState.Modified;
                await db.SaveChangesAsync();
                System.Web.HttpContext.Current.Session["NOT"] = "You have successfully updated the IPO";

                return RedirectToAction("Index");
            }
            ViewBag.Issu = CountriesM(iPO.issuer, iPO.issuer);
            return View(iPO);
        }

        // GET: IPOes/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IPO iPO = await db.IPOs.FindAsync(id);
            if (iPO == null)
            {
                return HttpNotFound();
            }
            return View(iPO);
        }

        // POST: IPOes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            IPO iPO = await db.IPOs.FindAsync(id);
            db.IPOs.Remove(iPO);
            await db.SaveChangesAsync();
            System.Web.HttpContext.Current.Session["NOT"] = "You have successfully deleted the IPO";

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public List<SelectListItem> Countries()
        {
            List<SelectListItem> dept = new List<SelectListItem>();
            var query = from u in db.para_issuers select u;
            if (query.Count() > 0)
            {
                foreach (var v in query)
                {
                    dept.Add(new SelectListItem { Text = v.Company, Value = v.Company });
                }
            }
            return dept;
        }
        public List<SelectListItem> CountriesM(string s,string v2)
        {
            List<SelectListItem> dept = new List<SelectListItem>();
            var query = from u in db.para_issuers select u;
            dept.Add(new SelectListItem { Text = s, Value = v2});
            if (query.Count() > 0)
            {
                foreach (var v in query)
                {
                    dept.Add(new SelectListItem { Text = v.Company, Value = v.Company });
                }
            }
            return dept;
        }
    }
}
