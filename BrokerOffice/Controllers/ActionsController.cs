﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BrokerOffice.DAO;
using BrokerOffice.Models;

namespace BrokerOffice.Controllers
{
    public class ActionsController : Controller
    {
        private SBoardContext db = new SBoardContext();

        // GET: Actions
        public async Task<ActionResult> Index()
        {
            return View(await db.ActionForm.ToListAsync());
        }

        // GET: Actions/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ActionFormcs actionFormcs = await db.ActionForm.FindAsync(id);
            if (actionFormcs == null)
            {
                return HttpNotFound();
            }
            return View(actionFormcs);
        }

      
        // GET: Actions/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ActionFormcs actionFormcs = await db.ActionForm.FindAsync(id);
            if (actionFormcs == null)
            {
                return HttpNotFound();
            }
            return View(actionFormcs);
        }

        // POST: Actions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            ActionFormcs actionFormcs = await db.ActionForm.FindAsync(id);
            db.ActionForm.Remove(actionFormcs);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
