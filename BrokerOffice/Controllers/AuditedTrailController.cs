﻿using BrokerOffice.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BrokerOffice.Controllers
{
    public class AuditedTrailController : Controller
    {
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FinAuditTrailer([Bind(Include = "From,To")] Posting registerreport)
        {

            if (ModelState.IsValid)
            {
                return Redirect("~/Reporting/FinAuditTrail.aspx?From=" + registerreport.From.ToString("dd-MMM-yyyy") + "&To=" + registerreport.To.ToString("dd-MMM-yyyy"));
            }
            return View();
        }

        public ActionResult FinAuditTrailer()
        {
            return View();
        }
    }
}