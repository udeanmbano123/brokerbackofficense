﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;

using WebMatrix.WebData;

using BrokerOffice.DAO.security;
using BrokerOffice.DAO;
using BrokerOffice.Models;

namespace BrokerOffice.Controllers
{ // GET: Admin
  //[CustomAuthorize(RolesConfigKey = "RolesConfigKey")]
  // [CustomAuthorize(UsersConfigKey = "UsersConfigKey")]
    [CustomAuthorize(Roles = "Admin")]
    // [CustomAuthorize(Users = "1")]
    public class UserAccountsController : Controller
    {
        private SBoardContext db = new SBoardContext();

        // GET: UserAccounts
        public async Task<ActionResult> Index()
        {
            string name2 = WebSecurity.CurrentUserName;
            var user2 = db.Users.ToList().Where(a => a.Email == name2);
            ViewBag.Users = "";
            foreach (var row in user2)
            {
                ViewBag.Users = row.FirstName + " " + row.LastName;
            }
            string name = WebSecurity.CurrentUserName;
            var user = db.Users.ToList().Where(a => a.Email == name);
            return View(user.ToList());
        }

        // GET: SystemUsers/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            string name2 = WebSecurity.CurrentUserName;
            var user2 = db.Users.ToList().Where(a => a.Email == name2);
            ViewBag.Users = "";
            foreach (var row in user2)
            {
                ViewBag.Users = row.FirstName + " " + row.LastName;
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = await db.Users.FindAsync(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // GET: Users/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            string name2 = WebSecurity.CurrentUserName;
            var user2 = db.Users.ToList().Where(a => a.Email == name2);
            ViewBag.Users = "";
            foreach (var row in user2)
            {
                ViewBag.Users = row.FirstName + " " + row.LastName;
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = await db.Users.FindAsync(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: SystemUsers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "UserId,Username,Email,Password,ConfirmPassword,FirstName,LastName,IsActive,LockCount,CreateDate,role,BrokerName,BrokerCode")] User user)
        {
            if (ModelState.IsValid)
            {
                user.BrokerCode = "NSE";
                user.BrokerName = "Nairobi Stock Exchange";
                user.Password = Request["Password"].ToString();
                user.ConfirmPassword = Request["ConfirmPassword"].ToString();
                user.Password = ComputeHash(user.Password, new SHA256CryptoServiceProvider());
                user.ConfirmPassword = ComputeHash(user.ConfirmPassword, new SHA256CryptoServiceProvider());
                db.Entry(user).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            string name2 = WebSecurity.CurrentUserName;
            var user2 = db.Users.ToList().Where(a => a.Email == name2);
            ViewBag.Users = "";
            foreach (var row in user2)
            {
                ViewBag.Users = row.FirstName + " " + row.LastName;
            }
            return View(user);
        }



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        //password  hashing function
        public string ComputeHash(string input, HashAlgorithm algorithm)
        {
            Byte[] inputBytes = Encoding.UTF8.GetBytes(input);

            Byte[] hashedBytes = algorithm.ComputeHash(inputBytes);

            return BitConverter.ToString(hashedBytes);
        }
    }
}
