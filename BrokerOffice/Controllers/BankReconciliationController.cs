﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BrokerOffice.DAO;
using BrokerOffice.DAO.security;
using BrokerOffice.Models;
using OfficeOpenXml;
using PagedList;
using WebMatrix.WebData;
using WebMatrix.Data;
using System.Data.Entity.Migrations;

namespace BrokerOffice.Controllers
{// [CustomAuthorize(RolesConfigKey = "RolesConfigKey")]
 // [CustomAuthorize(UsersConfigKey = "UsersConfigKey")]
    [CustomAuthorize(Roles = "User,Approver,BrokerAdmin,BrokerAdmin2", NotifyUrl = " /UnauthorizedPage")]
    // [CustomAuthorize(Users = "1,2")]

    public class BankReconciliationController : Controller
    {
        private SBoardContext db = new SBoardContext();

        // GET: BankReconciliation
        public async Task<ActionResult> Index(string sortOrder, int? page)
        {
            ViewBag.Acc = System.Web.HttpContext.Current.Session["Rec"].ToString();
            string num = System.Web.HttpContext.Current.Session["Rec"].ToString();
            ViewBag.AccN = System.Web.HttpContext.Current.Session["RecN"].ToString();
            var per = from v in db.bhtrans
                      where v.clientnumber == num
                      select v;
            ViewBag.CurrentSort = sortOrder;

            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.AccountNumberSortParm = String.IsNullOrEmpty(sortOrder) ? "account" : "";
            ViewBag.GroupNameSortParm = String.IsNullOrEmpty(sortOrder) ? "group" : "";

            switch (sortOrder)
            {
                case "name_desc":
                    per = per.OrderByDescending(s => s.Reference);
                    break;
                case "account":
                    per = per.OrderBy(s => s.UploadedDate);
                    break;
                case "group":
                    per = per.OrderBy(s => s.TransactionDate);
                    break;
                default:
                    per = per.OrderBy(s => s.TransactionDate);
                    break;
            }
            int pageSize = 20;
            int pageNumber = (page ?? 1);
            //per = per.OrderBy(a => a.TransactionDate);
            return View(per.ToPagedList(pageNumber, pageSize));

        }

        // GET: BankReconciliation/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            ViewBag.Acc = System.Web.HttpContext.Current.Session["Rec"].ToString();
            ViewBag.AccN = System.Web.HttpContext.Current.Session["RecN"].ToString();

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UplodedTrans uplodedTrans = await db.bhtrans.FindAsync(id);
            if (uplodedTrans == null)
            {
                return HttpNotFound();
            }
            return View(uplodedTrans);
        }

        // GET: BankReconciliation/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: BankReconciliation/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "UplodedTransID,TransactionDate,ValueDate,Reference,Description,Debit,Credit,Balance,clientnumber,fileName,UploadedBy,UploadedDate,Batch")] UplodedTrans uplodedTrans)
        {
            if (ModelState.IsValid)
            {
                db.bhtrans.Add(uplodedTrans);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(uplodedTrans);
        }

        // GET: BankReconciliation/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UplodedTrans uplodedTrans = await db.bhtrans.FindAsync(id);
            if (uplodedTrans == null)
            {
                return HttpNotFound();
            }
            return View(uplodedTrans);
        }

        // POST: BankReconciliation/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "UplodedTransID,TransactionDate,ValueDate,Reference,Description,Debit,Credit,Balance,clientnumber,fileName,UploadedBy,UploadedDate,Batch")] UplodedTrans uplodedTrans)
        {
            if (ModelState.IsValid)
            {
                db.Entry(uplodedTrans).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(uplodedTrans);
        }

        // GET: BankReconciliation/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            ViewBag.Acc = System.Web.HttpContext.Current.Session["Rec"].ToString();
            ViewBag.AccN = System.Web.HttpContext.Current.Session["RecN"].ToString();

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UplodedTrans uplodedTrans = await db.bhtrans.FindAsync(id);
            if (uplodedTrans == null)
            {
                return HttpNotFound();
            }
            return View(uplodedTrans);
        }

        // POST: BankReconciliation/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            ViewBag.Acc = System.Web.HttpContext.Current.Session["Rec"].ToString();
            ViewBag.AccN = System.Web.HttpContext.Current.Session["RecN"].ToString();

            UplodedTrans uplodedTrans = await db.bhtrans.FindAsync(id);
            db.bhtrans.Remove(uplodedTrans);
            await db.SaveChangesAsync(WebSecurity.CurrentUserName);
            System.Web.HttpContext.Current.Session["NOT"] = "You have successfully removed the transaction";
            return RedirectToAction("Index");
        }

        public ActionResult Report(FormCollection formCollection)
        {
            var batch = System.Web.HttpContext.Current.Session["Rec"].ToString();
            var ff = Request.Form["DD2"].ToString();
            var ff2 = Request.Form["DD3"].ToString();
            if (ff == null || ff == "")
            {
                System.Web.HttpContext.Current.Session["NOT"] = "THe from date is required";

                return RedirectToAction("Index");

            }

            if (ff2 == null || ff2 == "")
            {
                System.Web.HttpContext.Current.Session["NOT"] = "The To date is required";

                return RedirectToAction("Index");

            }
            return Redirect("~/Reporting/BatchReconciliations.aspx?date=" + Convert.ToDateTime(ff).ToString("dd-MMM-yyyy") + "&date2=" + Convert.ToDateTime(ff2).ToString("dd-MMM-yyyy") + "&acc=" + batch);

        }

        public ActionResult Upload(FormCollection formCollection)
        {
            var batch = System.Web.HttpContext.Current.Session["Rec"].ToString();
            int total = 0;

            var chk = 0;
            int me = 0;
            string fileExtension = System.IO.Path.GetExtension(Request.Files["FileUpload"].FileName);
            var ff = Request.Form["DD"].ToString();
            if (ff == null || ff == "")
            {
                System.Web.HttpContext.Current.Session["NOT"] = "THe upload date is required";

                return RedirectToAction("Index");

            }

            try
            {
                chk = db.bhtrans.ToList().Where(a => a.fileName == Request.Files["FileUpload"].FileName).Count();

            }
            catch (Exception)
            {

                chk = 0;
            }
            if (chk > 0)
            {
                System.Web.HttpContext.Current.Session["NOT"] = "File with name has already been uploaded " + Request.Files["FileUpload"].FileName;

                return RedirectToAction("Index");
            }
            try
            {

                if (fileExtension == ".xls" || fileExtension == ".xlsx" || fileExtension == ".csv")
                {

                    // Create a folder in App_Data named ExcelFiles because you need to save the file temporarily location and getting data from there. 
                    string fileLocation = Server.MapPath("~/File/" + Request.Files["FileUpload"].FileName+DateTime.Now.ToString("ddMMyyyyhhmmss"));

                    if (System.IO.File.Exists(fileLocation))
                        System.IO.File.Delete(fileLocation);

                    Request.Files["FileUpload"].SaveAs(fileLocation);
                    string excelConnectionString = string.Empty;

                    excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileLocation +
                                            ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                    //connection String for xls file format.
                    if (fileExtension == ".xls")
                    {
                        excelConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + fileLocation +
                                                ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
                    }
                    //connection String for xlsx file format.
                    else if (fileExtension == ".xlsx")
                    {

                        excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileLocation +
                                                ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                    }
                    else if (fileExtension == ".txt")
                    {
                        excelConnectionString = "Provider = Microsoft.Jet.OLEDB.4.0; Data Source=" + fileLocation +
                                                ";Extended Properties=\"text;HDR=Yes;FMT=Delimited\"";
                    }
                    else if (fileExtension == ".csv")
                    {
                        string fileName = Path.Combine(Server.MapPath("~/CSV"), Guid.NewGuid().ToString() + ".csv");

                        try
                        {
                            System.IO.File.Delete(fileName);
                            System.IO.File.Delete(fileLocation);

                        }
                        catch (Exception)
                        {


                        }
                        System.Web.HttpContext.Current.Session["NOT"] = "Only xls,xlsx may be uploaded";

                        return RedirectToAction("Index");
                    }

                    //Create Conneon to Excel work book and add oledb namespace
                    OleDbConnection excelConnection = new OleDbConnection(excelConnectionString);

                    excelConnection.Open();
                   
                    DataTable dt = new DataTable();

                    dt = excelConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    if (dt == null)
                    {
                        return null;
                    }

                    String[] excelSheets = new String[dt.Rows.Count];
                    int t = 0;
                    //excel data saves in temp file here.
                    foreach (DataRow row in dt.Rows)
                    {
                        excelSheets[t] = row["TABLE_NAME"].ToString();
                        t++;
                    }
                    OleDbConnection excelConnection1 = new OleDbConnection(excelConnectionString);
                    
                    DataSet ds = new DataSet();

                    string query = string.Format("Select * from [{0}]", excelSheets[0]);
                    using (OleDbDataAdapter dataAdapter = new OleDbDataAdapter(query, excelConnection1))
                    {
                        dataAdapter.Fill(ds);
                    }


                    if (Request != null)
                    {
                        HttpPostedFileBase file = Request.Files["FileUpload"];
                        if ((file != null) && (file.ContentLength != 0) && !string.IsNullOrEmpty(file.FileName))
                        {
                            string fileName = file.FileName;
                            string fileContentType = file.ContentType;
                            byte[] fileBytes = new byte[file.ContentLength];
                            var data = file.InputStream.Read(fileBytes, 0, Convert.ToInt32(file.ContentLength));

                            using (var package = new ExcelPackage(file.InputStream))
                            {
                                var currentSheet = package.Workbook.Worksheets;
                                var workSheet = currentSheet.First();
                                var noOfCol = workSheet.Dimension.End.Column;
                                var noOfRow = workSheet.Dimension.End.Row;
                                int n = noOfRow;
                                total = n - 1;
                                for (int rowIterator = 2; rowIterator < (n + 1); rowIterator++)
                                {
                                  
                                    try
                                    {

                                        UplodedTrans my = new UplodedTrans();
                                        my.TransactionDate = Convert.ToDateTime(workSheet.Cells[rowIterator, 1].Value.ToString());
                                        my.ValueDate = Convert.ToDateTime(workSheet.Cells[rowIterator, 2].Value.ToString());
                                        my.Reference = workSheet.Cells[rowIterator, 3].Value.ToString();
                                        my.Description = workSheet.Cells[rowIterator, 4].Value.ToString();
                                        try
                                        {
                                            my.Debit = Convert.ToDecimal(workSheet.Cells[rowIterator, 5].Value.ToString());

                                        }
                                        catch (Exception)
                                        {

                                            my.Debit = 0;
                                        }
                                        try
                                        {
                                            my.Credit = Convert.ToDecimal(workSheet.Cells[rowIterator, 6].Value.ToString());

                                        }
                                        catch (Exception)
                                        {

                                            my.Credit = 0;
                                        }
                                        try
                                        {
                                            my.Balance = Convert.ToDecimal(workSheet.Cells[rowIterator, 7].Value.ToString());

                                        }
                                        catch (Exception)
                                        {

                                            my.Balance = 0;
                                        }
                                        my.clientnumber = System.Web.HttpContext.Current.Session["Rec"].ToString();
                                        my.UploadedBy = WebSecurity.CurrentUserName;
                                        my.fileName = fileName;
                                        my.Batch = recons(my.TransactionDate.ToString("dd-MMM-yy"), workSheet.Cells[rowIterator, 5].Value.ToString(), workSheet.Cells[rowIterator, 6].Value.ToString(), my.clientnumber);
                                        if (my.Batch == "Reconciled")
                                        {
                                          my.transID= reconsKey(my.TransactionDate.ToString("dd-MMM-yy"), workSheet.Cells[rowIterator, 5].Value.ToString(), workSheet.Cells[rowIterator, 6].Value.ToString(), my.clientnumber);
                                            my.reason = "Found";
                                        }
                                        if (my.Batch == "UnReconciled")
                                        {
                                            my.Batch = recons2(my.TransactionDate.ToString("dd-MMM-yy"), workSheet.Cells[rowIterator, 5].Value.ToString(), workSheet.Cells[rowIterator, 6].Value.ToString(), my.clientnumber);
                                            my.transID = recons2Key(my.TransactionDate.ToString("dd-MMM-yy"), workSheet.Cells[rowIterator, 5].Value.ToString(), workSheet.Cells[rowIterator, 6].Value.ToString(), my.clientnumber);
                                            my.reason = "Not found in GL.";
                                            if (my.transID == "0")
                                            {
                                                my.reason = "Not found in GL.";
                                            }
                                        }
                                        if (my.transID == null)
                                        {
                                            my.transID = "0";
                                        }
                                            my.UploadedBy = WebSecurity.CurrentUserName;

                                        my.UploadedDate = Convert.ToDateTime(ff);
                                        try
                                        {
                                            db.Database.Connection.Close();
                                        }
                                        catch (Exception)
                                        {


                                        }
                                        try
                                        {
                                            db.Database.Connection.Open();
                                        }
                                        catch (Exception)
                                        {


                                        }

                                        db.bhtrans.Add(my);
                                        db.SaveChanges(WebSecurity.CurrentUserName.ToString());
                                        me += 1;
                                    }
                                    catch (Exception)
                                    {

                                        continue;
                                    }


                                }

                                System.Web.HttpContext.Current.Session["NOT"] = "You have successfully uploaded " + (me-1).ToString() + " transactions for the batch";


                                try
                                {

                                }
                                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                                {

                                    Exception raise = dbEx;
                                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                                    {
                                        foreach (var validationError in validationErrors.ValidationErrors)
                                        {
                                            string message = string.Format("{0}:{1}",
                                                validationErrors.Entry.Entity.ToString(),
                                                validationError.ErrorMessage);
                                            // raise a new exception nesting
                                            // the current instance as InnerException
                                            raise = new InvalidOperationException(message, raise);
                                        }
                                    }
                                    throw raise;
                                }

                            }
                        }
                    }



                }
                else
                {

                }

            }
            catch (Exception f)
            {

                System.Web.HttpContext.Current.Session["NOT"] = "You have successfully uploaded " + (me-1).ToString() + " transactions for the batch";
               // System.Web.HttpContext.Current.Session["NOT"] = f.Message;



               // throw;

            }

            try
            {
                var fileLocation = Server.MapPath("~/File/" + Request.Files["FileUpload"].FileName);
                System.IO.File.Delete(fileLocation);
            }
            catch (Exception)
            {


            }
            return RedirectToAction("Index");
        }

        public string recons2(string txndate, string debit, string credit, string acc)
        {
            string neh = "UnReconciled";
            string sql = "Select * from Trans where CAST(TrxnDate as Date)=CAST('" + txndate + "' as Date) and CAST(Debit as numeric(18,2))=CAST('" + debit + "' as numeric(18,2)) and CAST(Credit as numeric(18,2))=CAST('" + credit + "' as numeric(18,2)) and Account='" + acc + "'";
            var db2 = WebMatrix.Data.Database.Open("SBoardConnection");
            var fy = 0;
            try
            {
                fy = db2.Query(sql).ToList().Count();

            }
            catch (Exception)
            {

                fy = 0;
            }
            if (fy > 0)
            {
                neh = "UnReconciled";
            }
            else
            {
                neh = "UnReconciled";
            }
            return neh;
        }
        public string recons2Key(string txndate, string debit, string credit, string acc)
        {
            string neh = "UnReconciled";
            string sql = "Select IsNull(TransID,0) as 'Mo' from Trans where CAST(TrxnDate as Date)=CAST('" + txndate + "' as Date) and CAST(Debit as numeric(18,2))=CAST('" + debit + "' as numeric(18,2)) and CAST(Credit as numeric(18,2))=CAST('" + credit + "' as numeric(18,2)) and Account='" + acc + "'";
            string vf = "";
            var sf = db.Database.SqlQuery<TRP>(sql);
            foreach (var p in sf)
            {
                vf = p.Mo.ToString();
            }
            return vf;
        }
        public string recons(string txndate, string debit, string credit, string acc)
        {
            string neh = "Reconciled";
            string sql = "Select * from Trans where CAST(TrxnDate as Date)=CAST('" + txndate + "' as Date) and CAST(Debit as numeric(18,2))=CAST('" + credit + "' as numeric(18,2)) and CAST(Credit as numeric(18,2))=CAST('" + debit + "' as numeric(18,2)) and Account='" + acc + "'";
            var db2 = WebMatrix.Data.Database.Open("SBoardConnection");
            var fy = 0;
            try
            {
                fy = db2.Query(sql).ToList().Count();

            }
            catch (Exception)
            {

                fy = 0;
            }
            if (fy > 0)
            {
                neh = "Reconciled";
            }
            else
            {
                neh = "UnReconciled";
            }
            return neh;
        }
        public string reconsKey(string txndate, string debit, string credit, string acc)
        {
            string neh = "Reconciled";
            string sql = "Select IsNull(TransID,0) as 'Mo' from Trans where CAST(TrxnDate as Date)=CAST('" + txndate + "' as Date) and CAST(Debit as numeric(18,2))=CAST('" + credit + "' as numeric(18,2)) and CAST(Credit as numeric(18,2))=CAST('" + debit + "' as numeric(18,2)) and Account='" + acc + "'";

            string vf = "";
            var sf = db.Database.SqlQuery<TRP>(sql);
            foreach (var p in sf)
            {
                vf = p.Mo.ToString();
            }
            return vf;
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        // GET: BankReconciliation/Delete/5
        public async Task<ActionResult> Recon(int? id)
        {
            ViewBag.Acc = System.Web.HttpContext.Current.Session["Rec"].ToString();
            ViewBag.AccN = System.Web.HttpContext.Current.Session["RecN"].ToString();

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UplodedTrans uplodedTrans = await db.bhtrans.FindAsync(id);
            if (uplodedTrans == null)
            {
                return HttpNotFound();
            }
            return View(uplodedTrans);
        }

        // POST: BankReconciliation/Delete/5
        [HttpPost, ActionName("Recon")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ReconConfirmed(int id)
        {
            ViewBag.Acc = System.Web.HttpContext.Current.Session["Rec"].ToString();
            ViewBag.AccN = System.Web.HttpContext.Current.Session["RecN"].ToString();

            UplodedTrans uplodedTrans = await db.bhtrans.FindAsync(id);
            uplodedTrans.Batch = "Reconciled";
            db.bhtrans.AddOrUpdate(uplodedTrans);
            await db.SaveChangesAsync();
            System.Web.HttpContext.Current.Session["NOT"] = "The transaction has been reconciled";

            return RedirectToAction("Index");
        }

        // GET: BankReconciliation/Delete/5
        public async Task<ActionResult> UnRecon(int? id)
        {
            ViewBag.Acc = System.Web.HttpContext.Current.Session["Rec"].ToString();
            ViewBag.AccN = System.Web.HttpContext.Current.Session["RecN"].ToString();

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UplodedTrans uplodedTrans = await db.bhtrans.FindAsync(id);
            if (uplodedTrans == null)
            {
                return HttpNotFound();
            }
            return View(uplodedTrans);
        }

        // POST: BankReconciliation/Delete/5
        [HttpPost, ActionName("UnRecon")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> UnReconConfirmed(int id)
        {
            ViewBag.Acc = System.Web.HttpContext.Current.Session["Rec"].ToString();
            ViewBag.AccN = System.Web.HttpContext.Current.Session["RecN"].ToString();

            UplodedTrans uplodedTrans = await db.bhtrans.FindAsync(id);
            uplodedTrans.Batch = "UnReconciled";
            db.bhtrans.AddOrUpdate(uplodedTrans);
            await db.SaveChangesAsync();
            System.Web.HttpContext.Current.Session["NOT"] = "The transaction has been unreconciled";

            return RedirectToAction("Index");

        }
    }
}