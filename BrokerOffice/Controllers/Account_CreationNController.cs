﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BrokerOffice.DAO;
using BrokerOffice.Models;
using PagedList;
using BrokerOffice.DAO.security;
using System.Data.OleDb;
using System.Configuration;
using System.Text.RegularExpressions;
using WebMatrix.WebData;
using Database = WebMatrix.Data.Database;
using OfficeOpenXml;
using System.Data.Entity.Migrations;
using RestSharp;
using Newtonsoft.Json;

namespace BrokerOffice.Controllers
{// [CustomAuthorize(RolesConfigKey = "RolesConfigKey")]
 // [CustomAuthorize(UsersConfigKey = "UsersConfigKey")]
    [CustomAuthorize(Roles = "User,Approver,BrokerAdmin,BrokerAdmin2", NotifyUrl = " /UnauthorizedPage")]
    // [CustomAuthorize(Users = "1,2")]
    public class Account_CreationNController : Controller
    {
        private SBoardContext db = new SBoardContext();

        // GET: Account_Creation
        public ViewResult Index(string sortOrder, string currentFilter, string
  searchString, string Date1String, string Date2String, int? page)
        {
            var per = from s in db.Account_Creations
                      select s;
            try
            {
           
               
      
                ViewBag.CurrentSort = sortOrder;
                ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
                ViewBag.IdentificationSortParm = String.IsNullOrEmpty(sortOrder) ? "identification" : "";
                ViewBag.SurnameSortParm = String.IsNullOrEmpty(sortOrder) ? "surname" : "";
                ViewBag.CDSCSortParm = String.IsNullOrEmpty(sortOrder) ? "cdsc" : "";
                per = from s in db.Account_Creations
                          select s;
                if (searchString != null)
                {
                    page = 1;
                }
                else
                {
                    searchString = currentFilter;
                }
                ViewBag.CurrentFilter = searchString;
                if (!String.IsNullOrEmpty(searchString))
                {
                    per = per.Where(s =>
                    (s.Surname_CompanyName).ToUpper().Contains(searchString.ToUpper())
                     ||
                     (s.OtherNames).ToUpper().Contains(searchString.ToUpper())
                     ||
                    (s.CDSC_Number).ToUpper().Contains(searchString.ToUpper()));
                }
                
                try
                {
                    Date1String = Request.QueryString["Date1String"].ToString();
                    Date2String = Request.QueryString["Date2String"].ToString();
                    if (!String.IsNullOrEmpty(Date1String) && !String.IsNullOrEmpty(Date2String))
                    {
                        System.Web.HttpContext.Current.Session["Date1"] = Date1String;
                        System.Web.HttpContext.Current.Session["Date2"] = Date2String;
                        ViewBag.dt1 = Date1String;
                        ViewBag.dt2 = Date2String;
                    }
                }
                catch (Exception)
                {


                }
                try
                {
                    if (System.Web.HttpContext.Current.Session["Date1"].ToString() != "" && System.Web.HttpContext.Current.Session["Date2"].ToString() != "")
                    {

                        Date1String = System.Web.HttpContext.Current.Session["Date1"].ToString();
                        Date2String = System.Web.HttpContext.Current.Session["Date2"].ToString();
                        ViewBag.dt1 = Date1String;
                        ViewBag.dt2 = Date2String;
                    }
                }
                catch (Exception)
                {


                }
                if (!String.IsNullOrEmpty(Date1String) && !String.IsNullOrEmpty(Date2String) && String.IsNullOrEmpty(searchString))
                {
                    System.Web.HttpContext.Current.Session["Date1"] = Date1String;
                    System.Web.HttpContext.Current.Session["Date2"] = Date2String;
                    DateTime date = Convert.ToDateTime(Date1String);
                    DateTime date2 = Convert.ToDateTime(Date2String);
                    DateTime date3 = date2.AddDays(1);
                    per = from s in db.Account_Creations
                          
                          where (s.Date_Created>date && s.Date_Created<date3)
                          select s;

                    var cc = per.Count();
                }
                ViewBag.dt1 = Date1String;
                ViewBag.dt2 = Date2String;
             
                switch (sortOrder)
                {
                    case "name_desc":
                        per = per.OrderByDescending(s => s.OtherNames);
                        break;
                    case "identification":
                        per = per.OrderBy(s => s.Identification);
                        break;
                    case "surname":
                        per = per.OrderBy(s => s.Surname_CompanyName);
                        break;
                    case "cdsc":
                        per = per.OrderBy(s => s.CDSC_Number);
                        break;
                    default:
                        per = per.OrderBy(s => s.ID_);
                        break;
                }
              
        

            }
            catch (Exception)
            {

              
            }
            int pageSize = 10;
                int pageNumber = (page ?? 1);
            return View(per.ToPagedList(pageNumber, pageSize));
        }

        // GET: Account_Creation/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Account_Creation account_Creation = await db.Account_Creations.FindAsync(id);
            if (account_Creation == null)
            {
                return HttpNotFound();
            }
            return View(account_Creation);
        }

     

        // POST: Account_Creation/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
       
        // GET: Account_Creation/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Account_Creation account_Creation = await db.Account_Creations.FindAsync(id);
            if (account_Creation == null)
            {
                return HttpNotFound();
            }
            return View(account_Creation);
        }

        // POST: Account_Creation/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Account_Creation account_Creation = await db.Account_Creations.FindAsync(id);
            db.Account_Creations.Remove(account_Creation);
            await db.SaveChangesAsync();
            System.Web.HttpContext.Current.Session["NOT"] = account_Creation.CDSC_Number + " No details have been deleted";


            return RedirectToAction("Index");
        }

        
        public ActionResult UploadExcel()
        {

            ViewBag.Sucess = "";
           return View();
        }

        [HttpPost]
        public ActionResult UploadExcel(FormCollection formCollection)
        {


            string fileLocation = "";
         
                //string fileExtension =
                //    System.IO.Path.GetExtension(Request.Files["FileUpload"].FileName);

                //if (fileExtension == ".xls" || fileExtension == ".xlsx" || fileExtension == ".csv")
                //{

                //    // Create a folder in App_Data named ExcelFiles because you need to save the file temporarily location and getting data from there. 
                //    fileLocation = Server.MapPath("~/File/" + Request.Files["FileUpload"].FileName);

                //    if (System.IO.File.Exists(fileLocation))
                //        System.IO.File.Delete(fileLocation);

                //    Request.Files["FileUpload"].SaveAs(fileLocation);
                //    string excelConnectionString = string.Empty;

                //    excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileLocation +
                //                            ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                //    //connection String for xls file format.
                //    if (fileExtension == ".xls")
                //    {
                //        excelConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + fileLocation +
                //                                ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
                //    }
                //    //connection String for xlsx file format.
                //    else if (fileExtension == ".xlsx")
                //    {

                //        excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileLocation +
                //                                ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                //    }
                //    else if (fileExtension == ".txt")
                //    {
                //        excelConnectionString = "Provider = Microsoft.Jet.OLEDB.4.0; Data Source=" + fileLocation +
                //                                ";Extended Properties=\"text;HDR=Yes;FMT=Delimited\"";
                //    }
                //    else if (fileExtension == ".csv")
                //    {
                //        excelConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;DataSource=" +
                //                                fileLocation +
                //                                ";Extended Properties='text;HDR=Yes;'FMT=Delimited";
                //    }

                //    //Create Connection to Excel work book and add oledb namespace
                //    OleDbConnection excelConnection = new OleDbConnection(excelConnectionString);

                //    excelConnection.Open();
                //    DataTable dt = new DataTable();

                //    dt = excelConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                //    if (dt == null)
                //    {
                //        return null;
                //    }

                //    String[] excelSheets = new String[dt.Rows.Count];
                //    int t = 0;
                //    //excel data saves in temp file here.
                //    foreach (DataRow row in dt.Rows)
                //    {
                //        excelSheets[t] = row["TABLE_NAME"].ToString();
                //        t++;
                //    }
                //    OleDbConnection excelConnection1 = new OleDbConnection(excelConnectionString);
                //    DataSet ds = new DataSet();

                //    string query = string.Format("Select * from [{0}]", excelSheets[0]);
                //    using (OleDbDataAdapter dataAdapter = new OleDbDataAdapter(query, excelConnection1))
                //    {
                //        dataAdapter.Fill(ds);
                //    }


            int total=0;
                if (Request != null)
                {
                    HttpPostedFileBase file = Request.Files["UploadedFile"];
                    if ((file != null) && (file.ContentLength!=0) && !string.IsNullOrEmpty(file.FileName))   
        {
                        string fileName = file.FileName;
                        string fileContentType = file.ContentType;
                        byte[] fileBytes = new byte[file.ContentLength];
                        var data = file.InputStream.Read(fileBytes, 0, Convert.ToInt32(file.ContentLength));
                      
                        using (var package = new ExcelPackage(file.InputStream))
                        {
                            var currentSheet = package.Workbook.Worksheets;
                            var workSheet = currentSheet.First();
                            var noOfCol = workSheet.Dimension.End.Column;
                            var noOfRow = workSheet.Dimension.End.Row;
                        int n =noOfRow;
                        total =n - 1;
                            for (int rowIterator = 2; rowIterator<(n+1); rowIterator++)   
                {
                            db.Database.Connection.Open();
                            // string query = "Insert into Person(Name,Email,Mobile) Values('" + ds.Tables[0].Rows[i][0].ToString() + "','" + ds.Tables[0].Rows[i][1].ToString() + "','" + ds.Tables[0].Rows[i][2].ToString() + "')";
                            //EF Query
                            Account_Creation my = new Account_Creation();
                                var con2 = Database.Open("SBoardConnection");
                                var audit = "select max(ID_) as ID from Account_Creation";
                                var list13 = con2.Query(audit).ToList();
                                int code25 = 0;
                                try
                                {
                                    foreach (var row in list13)
                                    {
                                        code25 = row.ID;
                                    }

                                }

                                catch (Exception f)
                                {
                                    code25 = 0;
                                }

                                int size = 5;


                                var agent = "select top 1 AgentCode from Agent";
                                var list14 = con2.Query(agent).ToList();
                                string code22 = "";
                                foreach (var row in list14)
                                {
                                    code22 = row.AgentCode;
                                }
                                string concode25 = Convert.ToString((code25 + 1));
                                string cdsNo = concode25.PadLeft(10, '0') + code22 + concode25;
                                //my.CDSC_Number = cdsNo;
                                try
                                {
                                    my.Title = workSheet.Cells[rowIterator, 4].Value.ToString();
                                }
                                catch (Exception)
                                {

                                    my.Title ="TBA";
                                }
                                try
                                {
                                    my.OtherNames = workSheet.Cells[rowIterator, 6].Value.ToString();
                                }
                                catch (Exception)
                                {

                                    my.OtherNames ="TBA";
                                }
                                my.Middlename ="TBA";
                                try
                                {
                                    my.Surname_CompanyName = workSheet.Cells[rowIterator, 5].Value.ToString();
                                }
                                catch (Exception)
                                {

                                    my.Surname_CompanyName ="TBA";
                                }
                                try
                                {
                                    my.Nationality = workSheet.Cells[rowIterator, 8].Value.ToString();
                                }
                                catch (Exception)
                                {

                                    my.Nationality ="TBA";
                                }
                                try
                                {
                                    my.Resident = workSheet.Cells[rowIterator, 8].Value.ToString();
                                }
                                catch (Exception)
                                {

                                my.Resident ="TBA";
                            }
                                my.DateofBirth_Incorporation = DateTime.Now;

                                my.Gender = "O";
                                my.Initials = "TBA";
                            //contact
                            try
                            {
                                my.Address1 = workSheet.Cells[rowIterator, 7].Value.ToString();
                            }
                            catch (Exception)
                            {

                                my.Address1 ="TBA";
                            }
                                my.Address2 ="TBA";
                                my.Address3 ="TBA";
                                my.Town ="TBA";
                                my.PostCode ="TBA";
                                my.FaxNumber ="TBA";
                            try
                            {
                                my.TelephoneNumber = workSheet.Cells[rowIterator, 14].Value.ToString();
                            }
                            catch (Exception)
                            {

                                my.TelephoneNumber ="TBA";

                            }
                            try
                            {
                                my.Emailaddress = workSheet.Cells[rowIterator, 13].Value.ToString();
                            }
                            catch (Exception)
                            {

                                my.Emailaddress ="TBA";
                            }
                                //ident
                                my.ClientSuffix ="LI";
                                my.RNum ="TBA";
                            try
                            {
                                my.Identification = workSheet.Cells[rowIterator, 2].Value.ToString();
                            }
                            catch (Exception)
                            {

                                my.Identification ="TBA";
                            }

                            //banking
                            int? x = 100;
                              my.TaxBracket=x;
                                my.Bank ="TBA";
                                try
                                {
                                    my.Branch = workSheet.Cells[rowIterator, 11].Value.ToString();
                                }
                                catch (Exception)
                                {

                                    my.Branch ="TBA";
                                }
                                my.Bankname = "TBA";
                                my.BranchName ="TBA";
                            try
                            {
                                my.Accountnumber = workSheet.Cells[rowIterator, 12].Value.ToString();
                            }
                            catch (Exception)
                            {

                                my.Accountnumber ="TBA";
                            }
                                my.idtype = "TBA";
                                my.accountcategory ="TBA";
                            try
                            {
                                my.country=workSheet.Cells[rowIterator, 8].Value.ToString();
                            }
                            catch (Exception)
                            {

                                my.country ="TBA";
                            }
                                my.currency ="US";
                                my.divpayee ="TBA";
                                my.divbank ="TBA";
                                my.divbankcode="TBA";
                            try
                            {
                                my.divbranch = workSheet.Cells[rowIterator, 11].Value.ToString();
                            }
                            catch (Exception)
                            {

                                my.divbranch="TBA";
                            }
                            try
                            {
                                my.divbranchcode =workSheet.Cells[rowIterator, 11].Value.ToString();
                            }
                            catch (Exception)
                            {

                                my.divbranchcode="TBA";
                            }
                            try
                            {
                                my.divacc=workSheet.Cells[rowIterator, 12].Value.ToString();
                            }
                            catch (Exception)
                            {

                                my.divacc="TBA";
                            }
                                my.divaccounttype="TBA";
                                my.dividnumber="TBA";

                                my.idtype2="0";

                                my.Date_Created = DateTime.Now;
                            try
                            {
                                my.MobileNumber=workSheet.Cells[rowIterator, 14].Value.ToString();
                            }
                            catch (Exception)
                            {

                                my.MobileNumber="TBA";
                            }
                                my.mobile_money="TBA";
                                my.depname="TBA";
                                my.depcode="TBA";

                            //Insert Blank Row in Table  
                            code22 = "BEAR";
                            my.currency = "USD";

                            try
                            {
                                my.Address3 = workSheet.Cells[rowIterator, 3].Value.ToString();
                            }
                            catch (Exception)
                            {

                                my.Address3 = "";
                            }

                            //if (IsAddressAvailable("http://192.168.3.245:165/EscrowSoapWebService.asmx") == true)
                            //{
                            //    WebReference.EscrowServiceSoap escorw = new WebReference.EscrowServiceSoap();
                            //    var edc = escorw.Createnewaccount("",code22, my.Surname_CompanyName, my.Middlename, my.OtherNames, my.Initials, my.Title, my.country, my.Town, my.TelephoneNumber, my.MobileNumber,
                            //   my.currency, my.idtype, my.Identification, my.Nationality, my.DateofBirth_Incorporation.ToString(), my.Gender, my.Address1, my.Address2, my.Address3, my.Address3, my.Emailaddress, my.divpayee, my.divaccounttype, my.divbankcode, my.divbranchcode, my.divacc, my.idtype2, my.dividnumber, my.Bank, my.Branch,
                            //   my.Accountnumber, my.mobile_money);
                            //    my.CDSC_Number = Regex.Replace(edc.ToString(), "<[^>]+>", string.Empty);


                            //}
                            //else
                            //{


                            //    WebReference2.EscrowServiceSoap escorw2 = new WebReference2.EscrowServiceSoap();
                            //    var edc = escorw2.Createnewaccount(code22, my.Surname_CompanyName, my.Middlename, my.OtherNames, my.Initials, my.Title, my.country, my.Town, my.TelephoneNumber, my.MobileNumber,
                            //   my.currency, my.idtype, my.Identification, my.Nationality, my.DateofBirth_Incorporation.ToString(), my.Gender, my.Address1, my.Address2, my.Address3, "", my.Emailaddress, my.divpayee, my.divaccounttype, my.divbankcode, my.divbranchcode, my.divacc, my.idtype2, my.dividnumber, my.Bank, my.Branch,
                            //   my.Accountnumber, my.mobile_money);
                            //    my.CDSC_Number = Regex.Replace(edc.ToString(), "<[^>]+>", string.Empty);

                            //}




                            db.Account_Creations.Add(my);

                            try
                            {
                                db.SaveChanges(WebSecurity.CurrentUserName.ToString());
                            }
                            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                            {

                                Exception raise = dbEx;
                                foreach (var validationErrors in dbEx.EntityValidationErrors)
                                {
                                    foreach (var validationError in validationErrors.ValidationErrors)
                                    {
                                        string message = string.Format("{0}:{1}",
                                            validationErrors.Entry.Entity.ToString(),
                                            validationError.ErrorMessage);
                                        // raise a new exception nesting
                                        // the current instance as InnerException
                                        raise = new InvalidOperationException(message, raise);
                                    }
                                }
                                throw raise;
                            }
                            db.Database.Connection.Close();
                            con2.Close();
                        }
                        }
                    }


                    //  System.IO.File.Delete(fileLocation);

               
                }
                else
                {
                   
                }
            ViewBag.Sucess = "Total of " + total + "records were uploaded";
             return View();
        }
            
        public ActionResult NMI(string id)
        {
            string s = id;
            System.Web.HttpContext.Current.Session["csd"] = s;
           
            return Redirect("~/Reporting/MatchedDealsN");
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
