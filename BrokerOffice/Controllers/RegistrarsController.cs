﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BrokerOffice.DAO;
using BrokerOffice.Models;
using BrokerOffice.DAO.security;

namespace BrokerOffice.Controllers
{// GET: Admin
 //[CustomAuthorize(RolesConfigKey = "RolesConfigKey")]
 // [CustomAuthorize(UsersConfigKey = "UsersConfigKey")]
    [CustomAuthorize(Roles = "Admin")]
    // [CustomAuthorize(Users = "1")]
    public class RegistrarsController : Controller
    {
        private SBoardContext db = new SBoardContext();

        // GET: Registrars
        public async Task<ActionResult> Index()
        {
            return View(await db.Registrar.ToListAsync());
        }

        // GET: Registrars/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Registrar registrar = await db.Registrar.FindAsync(id);
            if (registrar == null)
            {
                return HttpNotFound();
            }
            return View(registrar);
        }

        // GET: Registrars/Create
        public ActionResult Create()
        {
            ViewBag.Category = regionD();
            return View();
        }
        public List<SelectListItem> regionD()
        {
            var p = db.ParticipantTypes.ToList();


            List<SelectListItem> phy = new List<SelectListItem>();
            phy.Add(new SelectListItem { Text = "", Value = "" });
            foreach (var z in p)
            {

                phy.Add(new SelectListItem { Text = z.Type, Value = z.Type });

            }
            return phy;
        }
        public List<SelectListItem> regionD(string s,string m)
        {
            var p = db.ParticipantTypes.ToList();


            List<SelectListItem> phy = new List<SelectListItem>();
            phy.Add(new SelectListItem { Text = s, Value = m });
            foreach (var z in p)
            {

                phy.Add(new SelectListItem { Text = z.Type, Value = z.Type });

            }
            return phy;
        }
        // POST: Registrars/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "RegistrarID,ParticipantName,ParticipantCode,ParticipantType,Address,Country,Telephone,Mobile,ParticipantEmail,ContactPerson,MaxUsers")] Registrar registrar)
        {
            if (ModelState.IsValid)
            {
                db.Registrar.Add(registrar);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            string z = Request["ParticipantType"].ToString();
            ViewBag.Category = regionD(z,z);
            return View(registrar);
        }

        // GET: Registrars/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Registrar registrar = await db.Registrar.FindAsync(id);
            if (registrar == null)
            {
                return HttpNotFound();
            }

            ViewBag.Category = regionD(registrar.ParticipantType, registrar.ParticipantType);
            return View(registrar);
        }

        // POST: Registrars/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "RegistrarID,ParticipantName,ParticipantCode,ParticipantType,Address,Country,Telephone,Mobile,ParticipantEmail,ContactPerson,MaxUsers")] Registrar registrar)
        {
            if (ModelState.IsValid)
            {
                db.Entry(registrar).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.Category = regionD(registrar.ParticipantType, registrar.ParticipantType);
            return View(registrar);
        }

        // GET: Registrars/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Registrar registrar = await db.Registrar.FindAsync(id);
            if (registrar == null)
            {
                return HttpNotFound();
            }
            return View(registrar);
        }

        // POST: Registrars/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Registrar registrar = await db.Registrar.FindAsync(id);
            db.Registrar.Remove(registrar);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
