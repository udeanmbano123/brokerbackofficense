﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BrokerOffice.DAO;
using BrokerOffice.Models;
using BrokerOffice.DAO.security;
using WebMatrix.WebData;

namespace BrokerOffice.Controllers
{//[CustomAuthorize(RolesConfigKey = "RolesConfigKey")]
 // [CustomAuthorize(UsersConfigKey = "UsersConfigKey")]
    [CustomAuthorize(Roles = "Admin", NotifyUrl = "/UnauthorizedPage")]
    // [CustomAuthorize(Users = "1")]
    public class MessagesController : Controller
    {
        private SBoardContext db = new SBoardContext();

        // GET: Messages
        public async Task<ActionResult> Index()
        {
            return View(await db.Messagess.ToListAsync());
        }

        // GET: Messages/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Messages messages = await db.Messagess.FindAsync(id);
            if (messages == null)
            {
                return HttpNotFound();
            }
            return View(messages);
        }

        // GET: Messages/Create
        public ActionResult Create()
        {
            var list2 = db.Users.ToList();
            //Create List of SelectListItem
            List<SelectListItem> selectlist2 = new List<SelectListItem>();
            selectlist2.Add(new SelectListItem() { Text = "", Value = "" });
            foreach (var row in list2)
            {
                //Adding every record to list  
                selectlist2.Add(new SelectListItem { Text = row.Email, Value = row.UserId.ToString() });
            }

            ViewBag.Category = selectlist2;
            return View();
        }

        // POST: Messages/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "MessagesID,Subject,Description,User,UserID,fromUser,fromUserID,DateAdded")] Messages messages)
        {
            if (ModelState.IsValid)
            {
                messages.DateAdded = DateTime.Now;
                int myid = Convert.ToInt32(Request["User"].ToString());

                messages.UserID = myid.ToString();

                var tasksusers = db.Users.ToList().Where(a => a.UserId == myid);
                foreach (var c in tasksusers)
                {
                   messages.User = c.Email;
                }
                var users = db.Users.ToList().Where(a => a.Email == WebSecurity.CurrentUserName);

                foreach (var d in users)
                {
                   messages.fromUser = d.Email;
                   messages.fromUserID = d.UserId.ToString();
                }

                db.Messagess.Add(messages);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            var list2 = db.Users.ToList();
            //Create List of SelectListItem
            List<SelectListItem> selectlist2 = new List<SelectListItem>();
            selectlist2.Add(new SelectListItem() { Text = Request[""], Value = Request[""] });
            foreach (var row in list2)
            {
                //Adding every record to list  
                selectlist2.Add(new SelectListItem { Text = row.Email, Value = row.UserId.ToString() });
            }

            ViewBag.Category = selectlist2;
            return View(messages);
        }

        // GET: Messages/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Messages messages = await db.Messagess.FindAsync(id);

            var list2 = db.Users.ToList();
            //Create List of SelectListItem
            List<SelectListItem> selectlist2 = new List<SelectListItem>();
            selectlist2.Add(new SelectListItem() { Text = messages.User, Value = messages.UserID });
            foreach (var row in list2)
            {
                //Adding every record to list  
                selectlist2.Add(new SelectListItem { Text = row.Email, Value = row.UserId.ToString() });
            }

            ViewBag.Category = selectlist2;
            if (messages == null)
            {
                return HttpNotFound();
            }
            return View(messages);
        }

        // POST: Messages/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "MessagesID,Subject,Description,User,UserID,fromUser,fromUserID,DateAdded")] Messages messages)
        {
            if (ModelState.IsValid)
            {
                int myid = Convert.ToInt32(Request["User"].ToString());

                messages.UserID = myid.ToString();

                var tasksusers = db.Users.ToList().Where(a => a.UserId == myid);
                foreach (var c in tasksusers)
                {
                    messages.User = c.Email;
                }
                var users = db.Users.ToList().Where(a => a.Email == WebSecurity.CurrentUserName);

                foreach (var d in users)
                {
                    messages.fromUser = d.Email;
                    messages.fromUserID = d.UserId.ToString();
                }
                db.Entry(messages).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            var list2 = db.Users.ToList();
            //Create List of SelectListItem
            List<SelectListItem> selectlist2 = new List<SelectListItem>();
            selectlist2.Add(new SelectListItem() { Text = Request[""], Value = Request[""] });
            foreach (var row in list2)
            {
                //Adding every record to list  
                selectlist2.Add(new SelectListItem { Text = row.Email, Value = row.UserId.ToString() });
            }

            ViewBag.Category = selectlist2;
            return View(messages);
        }

        // GET: Messages/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Messages messages = await db.Messagess.FindAsync(id);
            if (messages == null)
            {
                return HttpNotFound();
            }
            return View(messages);
        }

        // POST: Messages/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Messages messages = await db.Messagess.FindAsync(id);
            db.Messagess.Remove(messages);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
