﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BrokerOffice.DAO;
using BrokerOffice.Models;

namespace BrokerOffice.Controllers
{
    public class FinancialAccountController : Controller
    {
        private SBoardContext db = new SBoardContext();
        public static tblFinancialAccount TempAccNo;

        // GET: FinancialAccount
        public async Task<ActionResult> Index()
        {
            //tblFinAccountGroup
            var tblFinancialAccounts = db.tblFinancialAccounts.Include(t => t.tblBalanceType).Include(t => t.tblFinAccountGroup).Include(t => t.tblFinAccountSubGroup).Include(t => t.tblFinancialAccountClassification);
            return View(await tblFinancialAccounts.ToListAsync());
        }

        // GET: FinancialAccount/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblFinancialAccount tblFinancialAccount = await db.tblFinancialAccounts.FindAsync(id);
            if (tblFinancialAccount == null)
            {
                return HttpNotFound();
            }
            return View(tblFinancialAccount);
        }

        // GET: FinancialAccount/Create
        //public ActionResult Create()
        //{
        //    ViewBag.BalanceTypeId = new SelectList(db.tblBalanceTypes, "BalanceTypeId", "BalanceTypeName");
        //    //ViewBag.FinAccountGroupId = new SelectList(db.tblFinAccountGroups, "FinAccountGroupId", "AccountTypeName");

        //    var oldList = new SelectList(db.tblFinAccountGroups, "FinAccountGroupId", "AccountTypeName");
        //    SelectListItem selListItem = new SelectListItem() { Value = "null", Text = "" };
        //    ViewBag.FinAccountGroupId = AddFirstItem(oldList, selListItem);

        //    ViewBag.FinAccountSubGroupId = new SelectList(db.tblFinAccountSubGroups, "FinAccountSubGroupId", "GroupName");
        //    ViewBag.FinancialAccountClassificationId = new SelectList(db.tblFinancialAccountClassifications, "FinancialAccountClassificationId", "ClassificationName");
        //    return View();
        //}

        //[HttpPost]
        //[ActionName("Create")]
        public ActionResult Create(int? groupId)
        {
            var selectedBalance = db.tblFinAccountGroups.Where(u => u.FinAccountGroupId == groupId).Select(u =>u.BalanceTypeId).FirstOrDefault() ;
            
            ViewBag.BalanceTypeId = new SelectList(db.tblBalanceTypes, "BalanceTypeId", "BalanceTypeName", selectedBalance);
            var oldList = new SelectList(db.tblFinAccountGroups, "FinAccountGroupId", "AccountTypeName",groupId);
            SelectListItem selListItem = new SelectListItem() { Value = "null", Text = "" };
            ViewBag.FinAccountGroupId = AddFirstItem(oldList, selListItem);

             var subs = db.tblFinAccountSubGroups.Where(y => y.FinAccountGroupId == groupId);
            ViewBag.FinAccountSubGroupId = new SelectList(subs, "FinAccountSubGroupId", "GroupName");
            ViewBag.FinancialAccountClassificationId = new SelectList(db.tblFinancialAccountClassifications, "FinancialAccountClassificationId", "ClassificationName");
            int grpId = Convert.ToInt32(groupId);
            //Session["AccountNumber"] = getNextAccountNo(grpId);
            ViewBag.AccountNumber = getNextAccountNo(grpId,1);
            return View();
        }

        // POST: FinancialAccount/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "FinancialAccountId,AccountName,Description,AccountNumber,SubAccountNumber,FinancialAccountClassificationId,FinAccountSubGroupId,FinAccountGroupId,BalanceTypeId,CreatedDate,CreatedBy,ModifiedDate,IsActive,OtherDetails")] tblFinancialAccount tblFinancialAccount)
        {
            if (ModelState.IsValid)
            {
                var existing = db.tblFinancialAccounts.Select(y => y.AccountNumber).ToList();

               
               
                int grpId = Convert.ToInt32(tblFinancialAccount.FinAccountGroupId);

                tblFinancialAccount.AccountNumber = getNextAccountNo(grpId, tblFinancialAccount.AccountNumber);
                tblFinancialAccount.BalanceTypeId = db.tblFinAccountGroups.Where(u => u.FinAccountGroupId == grpId).Select(v => v.BalanceTypeId).FirstOrDefault();
                db.tblFinancialAccounts.Add(tblFinancialAccount);
                await db.SaveChangesAsync();
                //return RedirectToAction("Index");
                //if (!existing.Contains(tblFinancialAccount.AccountNumber))
                //{

                //    if (tblFinancialAccount.BalanceTypeId == 1 && (tblFinancialAccount.FinAccountGroupId == 1 || tblFinancialAccount.FinAccountGroupId == 5))
                //    {
                //        db.tblFinancialAccounts.Add(tblFinancialAccount);
                //        await db.SaveChangesAsync();
                //        return RedirectToAction("Index");
                //    }
                //    else if (tblFinancialAccount.BalanceTypeId == 2 && (tblFinancialAccount.FinAccountGroupId == 2 || tblFinancialAccount.FinAccountGroupId == 3 || tblFinancialAccount.FinAccountGroupId == 4))
                //    {
                //        db.tblFinancialAccounts.Add(tblFinancialAccount);
                //        await db.SaveChangesAsync();
                //        return RedirectToAction("Index");
                //    }
                //    else
                //    {
                //        System.Web.HttpContext.Current.Session["NOT"] = "Please review your account creation options and try again";

                //    }
                //}
                //System.Web.HttpContext.Current.Session["NOT"] = "Account number already exists please try again";

                System.Web.HttpContext.Current.Session["NOT"] = "Account created successfully";
               // Response.Redirect(Request.RawUrl,false);
                return RedirectToAction("Index");


            }

            ViewBag.BalanceTypeId = new SelectList(db.tblBalanceTypes, "BalanceTypeId", "BalanceTypeName", tblFinancialAccount.BalanceTypeId);
            ViewBag.FinAccountGroupId = new SelectList(db.tblFinAccountGroups, "FinAccountGroupId", "AccountTypeName", tblFinancialAccount.FinAccountGroupId);
            ViewBag.FinAccountSubGroupId = new SelectList(db.tblFinAccountSubGroups, "FinAccountSubGroupId", "GroupName", tblFinancialAccount.FinAccountSubGroupId);
            ViewBag.FinancialAccountClassificationId = new SelectList(db.tblFinancialAccountClassifications, "FinancialAccountClassificationId", "ClassificationName", tblFinancialAccount.FinancialAccountClassificationId);
            return View(tblFinancialAccount);
        }

        // GET: FinancialAccount/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblFinancialAccount tblFinancialAccount = await db.tblFinancialAccounts.FindAsync(id);
            if (tblFinancialAccount == null)
            {
                return HttpNotFound();
            }
            TempAccNo = tblFinancialAccount;
            ViewBag.BalanceTypeId = new SelectList(db.tblBalanceTypes, "BalanceTypeId", "BalanceTypeName", tblFinancialAccount.BalanceTypeId);
            ViewBag.FinAccountGroupId = new SelectList(db.tblFinAccountGroups, "FinAccountGroupId", "AccountTypeName", tblFinancialAccount.FinAccountGroupId);
            ViewBag.FinAccountSubGroupId = new SelectList(db.tblFinAccountSubGroups, "FinAccountSubGroupId", "GroupName", tblFinancialAccount.FinAccountSubGroupId);
            ViewBag.FinancialAccountClassificationId = new SelectList(db.tblFinancialAccountClassifications, "FinancialAccountClassificationId", "ClassificationName", tblFinancialAccount.FinancialAccountClassificationId);
            return View(tblFinancialAccount);
        }

        // POST: FinancialAccount/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "FinancialAccountId,AccountName,Description,AccountNumber,SubAccountNumber,FinancialAccountClassificationId,FinAccountSubGroupId,FinAccountGroupId,BalanceTypeId,CreatedDate,CreatedBy,ModifiedDate,IsActive,OtherDetails")] tblFinancialAccount tblFinancialAccount)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tblFinancialAccount).State = EntityState.Modified;

                var accs = db.tblFinancialAccounts.Where(a => a.FinancialAccountId == tblFinancialAccount.FinancialAccountId).FirstOrDefault();

                //accs.AccountNumber = TempAccNo.AccountNumber;
                

                if (TempAccNo.AccountNumber !=0)
                {
                    tblFinancialAccount.AccountNumber = TempAccNo.AccountNumber;
                    tblFinancialAccount.BalanceTypeId = TempAccNo.BalanceTypeId;
                    tblFinancialAccount.FinAccountGroupId = TempAccNo.FinAccountGroupId;
                    tblFinancialAccount.FinAccountSubGroupId = TempAccNo.FinAccountSubGroupId;
                }
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.BalanceTypeId = new SelectList(db.tblBalanceTypes, "BalanceTypeId", "BalanceTypeName", tblFinancialAccount.BalanceTypeId);
            ViewBag.FinAccountGroupId = new SelectList(db.tblFinAccountGroups, "FinAccountGroupId", "AccountTypeName", tblFinancialAccount.FinAccountGroupId);
            ViewBag.FinAccountSubGroupId = new SelectList(db.tblFinAccountSubGroups, "FinAccountSubGroupId", "GroupName", tblFinancialAccount.FinAccountSubGroupId);
            ViewBag.FinancialAccountClassificationId = new SelectList(db.tblFinancialAccountClassifications, "FinancialAccountClassificationId", "ClassificationName", tblFinancialAccount.FinancialAccountClassificationId);
            return View(tblFinancialAccount);
        }

        // GET: FinancialAccount/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblFinancialAccount tblFinancialAccount = await db.tblFinancialAccounts.FindAsync(id);
            if (tblFinancialAccount == null)
            {
                return HttpNotFound();
            }
            return View(tblFinancialAccount);
        }

        // POST: FinancialAccount/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            tblFinancialAccount tblFinancialAccount = await db.tblFinancialAccounts.FindAsync(id);
            db.tblFinancialAccounts.Remove(tblFinancialAccount);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private int getNextAccountNo(int groupId, int accNo)
        {
            int generatedAccount = accNo;

            List<int> acceptableList = new List<int>();

            try
            {
                var unavailableList = db.tblFinancialAccounts.Select(v => v.AccountNumber).ToList();
                var availableNumbers = db.tblFinAccountGroups.Where(u => u.FinAccountGroupId == groupId).Select(x => new { x.FirstNumber, x.LastNumber }).FirstOrDefault();
                int min = availableNumbers.FirstNumber;
                int max = availableNumbers.LastNumber;

                List<int> foundAccountIDs = new List<int>();
                for (int i = min; i < max; i++)
                {
                    if (!unavailableList.Contains(i))
                    {
                        foundAccountIDs.Add(i);
                    }
                }

                generatedAccount = foundAccountIDs[0];
            }
            catch (Exception)
            {

            }
          

            return generatedAccount;



        }

        public static SelectList AddFirstItem(SelectList origList, SelectListItem firstItem)
        {
            List<SelectListItem> newList = origList.ToList();
            newList.Insert(0, firstItem);

            var selectedItem = newList.FirstOrDefault(item => item.Selected);
            var selectedItemValue = String.Empty;
            if (selectedItem != null)
            {
                selectedItemValue = selectedItem.Value;
            }

            return new SelectList(newList, "Value", "Text", selectedItemValue);
        }
    }
}
