﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BrokerOffice.DAO;
using BrokerOffice.Models;
using BrokerOffice.DAO.security;
using PagedList;

namespace BrokerOffice.Controllers
{// GET: Admin
 //[CustomAuthorize(RolesConfigKey = "RolesConfigKey")]
 // [CustomAuthorize(UsersConfigKey = "UsersConfigKey")]
    [CustomAuthorize(Roles = "Admin")]
    // [CustomAuthorize(Users = "1")]
    public class StockPricesController : Controller
    {
        private SBoardContext db = new SBoardContext();

        // GET: StockPrices
        public async Task<ActionResult> Index(string sortOrder, int? page)
        {
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "stock" : "";
            ViewBag.LastSortParm = String.IsNullOrEmpty(sortOrder) ? "last" : "";
            ViewBag.DateSortParm = String.IsNullOrEmpty(sortOrder) ? "date" : "";
            var per = from s in db.StockPrices.Include(s => s.StockSecurities)
                      select s;
            switch (sortOrder)
            {
                case "stock":
                    per = per.OrderByDescending(s => s.StockSecurities.StockISIN);
                    break;

                case "last":
                    per = per.OrderByDescending(s => s.LastTradePrice);
                    break;
                case "date":
                    per = per.OrderByDescending(s => s.daterecorded);
                    break;
                default:
                    per = per.OrderBy(s => s.StockPricesID);
                    break;
            }
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(per.ToPagedList(pageNumber, pageSize));
           // var stockPrices = db.StockPrices.Include(s => s.StockSecurities);
           // return View(await stockPrices.ToListAsync());
        }

        // GET: StockPrices/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StockPrices stockPrices = await db.StockPrices.FindAsync(id);
            if (stockPrices == null)
            {
                return HttpNotFound();
            }
            return View(stockPrices);
        }

        // GET: StockPrices/Create
        public ActionResult Create()
        {
            ViewBag.StockSecuritiesID = new SelectList(db.StockSecuritiess, "StockSecuritiesID", "StockISIN");
            var list = db.TradingPlatforms.ToList();
            //Create List of SelectListItem
            List<SelectListItem> selectlist = new List<SelectListItem>();
            selectlist.Add(new SelectListItem() { Text = "", Value = "" });
            foreach (var row in list)
            {
                //Adding every record to list  
                selectlist.Add(new SelectListItem { Text = row.Name, Value = row.Code.ToString() });
            }


            var list2 = db.TradingBoards.ToList();
            //Create List of SelectListItem
            List<SelectListItem> selectlist2 = new List<SelectListItem>();
            selectlist2.Add(new SelectListItem() { Text = Request[""], Value = Request[""] });
            foreach (var row in list2)
            {
                //Adding every record to list  
                selectlist2.Add(new SelectListItem { Text = row.BoardName, Value = row.BoardName.ToString() });
            }

            ViewBag.Category = selectlist2;
            ViewBag.Role = selectlist;

            return View();
        }

        // POST: StockPrices/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "StockPricesID,StockSecuritiesID,LastTradePrice,Highest_Trade_Price,Lowest_Trade_Price,daterecorded,stockisin,TP,TPBoard")] StockPrices stockPrices)
        {
          
            if (ModelState.IsValid)
            {
                var uz = db.StockSecuritiess.ToList().Where(a => a.StockSecuritiesID == stockPrices.StockSecuritiesID);
                foreach(var c in uz)
                {
                    stockPrices.stockisin = c.StockISIN;
                }
                db.StockPrices.Add(stockPrices);
                await db.SaveChangesAsync();
                System.Web.HttpContext.Current.Session["NOT"] = "You have successfully added the  Stock Price";

                return RedirectToAction("Index");
            }

            ViewBag.StockSecuritiesID = new SelectList(db.StockSecuritiess, "StockSecuritiesID", "StockISIN", stockPrices.StockSecuritiesID);
            var list = db.TradingPlatforms.ToList();
            //Create List of SelectListItem
            List<SelectListItem> selectlist = new List<SelectListItem>();
            selectlist.Add(new SelectListItem() { Text = Request["TP"], Value =Request["TP"] });
            foreach (var row in list)
            {
                //Adding every record to list  
                selectlist.Add(new SelectListItem { Text = row.Name, Value = row.Code.ToString() });
            }
            int d = 0;
            var my = db.TradingPlatforms.ToList().Where(a => a.Code == Request["TP"].ToString().Replace(",", ""));
            foreach(var s in my)
            {
                d = s.TradingPlatformID;
            }
            var list2 = db.TradingPlatforms.ToList().Where(a => a.TradingPlatformID==d);
            //Create List of SelectListItem
            List<SelectListItem> selectlist2 = new List<SelectListItem>();
            selectlist2.Add(new SelectListItem() { Text = "", Value = "" });
            foreach (var row in list2)
            {
                //Adding every record to list  
                selectlist2.Add(new SelectListItem { Text = row.TradeBoard, Value = row.TradeBoard.ToString() });
            }

            ViewBag.Category = selectlist2;
            ViewBag.Role = selectlist;
            return View(stockPrices);
        }

        // GET: StockPrices/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StockPrices stockPrices = await db.StockPrices.FindAsync(id);
            if (stockPrices == null)
            {
                return HttpNotFound();
            }
            ViewBag.StockSecuritiesID = new SelectList(db.StockSecuritiess, "StockSecuritiesID", "StockISIN", stockPrices.StockSecuritiesID);
            var list = db.TradingPlatforms.ToList();
            //Create List of SelectListItem
            List<SelectListItem> selectlist = new List<SelectListItem>();
            selectlist.Add(new SelectListItem() { Text = stockPrices.TP, Value = stockPrices.TP });
            selectlist.Add(new SelectListItem() { Text = "", Value = "" });
            foreach (var row in list)
            {
                //Adding every record to list  
                selectlist.Add(new SelectListItem { Text = row.Name, Value = row.Code.ToString() });
            }


            var list2 = db.TradingBoards.ToList();
            //Create List of SelectListItem
            List<SelectListItem> selectlist2 = new List<SelectListItem>();
            selectlist2.Add(new SelectListItem() { Text = stockPrices.TPBoard, Value = stockPrices.TPBoard });
            selectlist2.Add(new SelectListItem() { Text = "", Value = "" });
            foreach (var row in list2)
            {
                //Adding every record to list  
                selectlist2.Add(new SelectListItem { Text = row.BoardName, Value = row.BoardName.ToString() });
            }

            ViewBag.Category = selectlist2;
            ViewBag.Role = selectlist;
            return View(stockPrices);
        }

        // POST: StockPrices/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "StockPricesID,StockSecuritiesID,LastTradePrice,Highest_Trade_Price,Lowest_Trade_Price,daterecorded,stockisin,TP,TPBoard")] StockPrices stockPrices)
        {
            if (ModelState.IsValid)
            {
                var uz = db.StockSecuritiess.ToList().Where(a => a.StockSecuritiesID == stockPrices.StockSecuritiesID);
                foreach (var c in uz)
                {
                    stockPrices.stockisin = c.StockISIN;
                }
                db.Entry(stockPrices).State = EntityState.Modified;
                await db.SaveChangesAsync();
                System.Web.HttpContext.Current.Session["NOT"] = "You have successfully updated the Stock Price";

                return RedirectToAction("Index");
            }
            ViewBag.StockSecuritiesID = new SelectList(db.StockSecuritiess, "StockSecuritiesID", "StockISIN", stockPrices.StockSecuritiesID);

            var list = db.TradingPlatforms.ToList();
            //Create List of SelectListItem
            List<SelectListItem> selectlist = new List<SelectListItem>();
            selectlist.Add(new SelectListItem() { Text = Request["TP"], Value = Request["TP"] });
            foreach (var row in list)
            {
                //Adding every record to list  
                selectlist.Add(new SelectListItem { Text = row.Name, Value = row.Code.ToString() });
            }

            int d = 0;
            var my = db.TradingPlatforms.ToList().Where(a => a.Code == Request["TP"].ToString().Replace(",", ""));
            foreach (var s in my)
            {
                d = s.TradingPlatformID;
            }
            var list2 = db.TradingPlatforms.ToList().Where(a => a.TradingPlatformID == d);
            //Create List of SelectListItem
            List<SelectListItem> selectlist2 = new List<SelectListItem>();
            selectlist2.Add(new SelectListItem() { Text = "", Value = "" });
            foreach (var row in list2)
            {
                //Adding every record to list  
                selectlist2.Add(new SelectListItem { Text = row.TradeBoard, Value = row.TradeBoard.ToString() });
            }


            ViewBag.Category = selectlist2;
            ViewBag.Role = selectlist;
            return View(stockPrices);
        }

        // GET: StockPrices/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StockPrices stockPrices = await db.StockPrices.FindAsync(id);
            if (stockPrices == null)
            {
                return HttpNotFound();
            }
            return View(stockPrices);
        }

        // POST: StockPrices/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            StockPrices stockPrices = await db.StockPrices.FindAsync(id);
            db.StockPrices.Remove(stockPrices);
            await db.SaveChangesAsync();
            System.Web.HttpContext.Current.Session["NOT"] = "You have successfully deleted the Stock Price";

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
