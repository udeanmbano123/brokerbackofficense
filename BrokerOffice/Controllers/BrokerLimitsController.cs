﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BrokerOffice.DAO;
using BrokerOffice.Models;
using BrokerOffice.DAO.security;
using RestSharp;

namespace BrokerOffice.Controllers
{
    [CustomAuthorize(Roles = "Admin,BrokerAdmin")]

    public class BrokerLimitsController : Controller
    {
        private SBoardContext db = new SBoardContext();

        // GET: BrokerLimits
        public async Task<ActionResult> Index()
        {
            return View(await db.BrokerLimits.ToListAsync());
        }

        // GET: BrokerLimits/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BrokerLimit brokerLimit = await db.BrokerLimits.FindAsync(id);
            if (brokerLimit == null)
            {
                return HttpNotFound();
            }
            return View(brokerLimit);
        }

        // GET: BrokerLimits/Create
        public ActionResult Create()
        {
            var zs = 0;
            try
            {
                zs = db.BrokerLimits.ToList().Count();

            }
            catch (Exception)
            {

                zs = 0;
            }
            if (zs>0)
            {
                System.Web.HttpContext.Current.Session["NOT"] = "You may set only one limit";
              return  Redirect("~/BrokerLimits/Index");
            }
            ViewBag.Stat = regionS();
            return View();
        }

        // POST: BrokerLimits/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "BrokerLimitID,BuyLimit,SellLimit,reaction")] BrokerLimit brokerLimit)
        {
            if (ModelState.IsValid)
            {
                db.BrokerLimits.Add(brokerLimit);
                await db.SaveChangesAsync();
                System.Web.HttpContext.Current.Session["NOT"] = "You have successfully set Broker Limit";
                //add limit
                var client = new RestClient("http://localhost/BrokerService");
                var request = new RestRequest("BuySell/{a}/{b}/{c}/{d}", Method.POST);
                request.AddUrlSegment("a",brokerLimit.BuyLimit.ToString());
                request.AddUrlSegment("b", brokerLimit.SellLimit.ToString());
                request.AddUrlSegment("c", brokerLimit.reaction.Replace(" ","-"));
                request.AddUrlSegment("d", "EFES");
                IRestResponse response = client.Execute(request);
                //validate = response.Content;
                return RedirectToAction("Index");
            }
            ViewBag.Stat = regionS(brokerLimit.reaction,brokerLimit.reaction);
            return View(brokerLimit);
        }

        // GET: BrokerLimits/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BrokerLimit brokerLimit = await db.BrokerLimits.FindAsync(id);
            if (brokerLimit == null)
            {
                return HttpNotFound();
            }
            ViewBag.Stat = regionS(brokerLimit.reaction, brokerLimit.reaction);

            return View(brokerLimit);
        }

        // POST: BrokerLimits/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "BrokerLimitID,BuyLimit,SellLimit,reaction")] BrokerLimit brokerLimit)
        {
            if (ModelState.IsValid)
            {
                db.Entry(brokerLimit).State = EntityState.Modified;
                await db.SaveChangesAsync();
                var client = new RestClient("http://localhost/BrokerService");
                var request = new RestRequest("BuySellU/{a}/{b}/{c}/{d}", Method.POST);
                request.AddUrlSegment("a", brokerLimit.BuyLimit.ToString());
                request.AddUrlSegment("b", brokerLimit.SellLimit.ToString());
                request.AddUrlSegment("c", brokerLimit.reaction.Replace(" ", "-"));
                request.AddUrlSegment("d", "EFES");
                IRestResponse response = client.Execute(request);
                //validate = response.Content;
                System.Web.HttpContext.Current.Session["NOT"] = "You have successfully updated Broker Limit";
                return RedirectToAction("Index");
            }
            ViewBag.Stat = regionS(brokerLimit.reaction, brokerLimit.reaction);

            return View(brokerLimit);
        }

        // GET: BrokerLimits/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BrokerLimit brokerLimit = await db.BrokerLimits.FindAsync(id);
            if (brokerLimit == null)
            {
                return HttpNotFound();
            }
            return View(brokerLimit);
        }

        // POST: BrokerLimits/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            BrokerLimit brokerLimit = await db.BrokerLimits.FindAsync(id);
            db.BrokerLimits.Remove(brokerLimit);
            await db.SaveChangesAsync();
            System.Web.HttpContext.Current.Session["NOT"] = "You have successfully deketed Broker Limit";
           return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        public List<SelectListItem> regionS()
        {


            List<SelectListItem> phy = new List<SelectListItem>();
           phy.Add(new SelectListItem { Text = "Drop Transaction", Value = "Drop Transaction" });
            phy.Add(new SelectListItem { Text = "Authorise Transaction", Value = "Authorise Transaction" });
            phy.Add(new SelectListItem { Text = "Half Settlement", Value = "Half Settlement" });
            phy.Add(new SelectListItem { Text = "Freeze Account", Value = "Freeze Account" });
            phy.Add(new SelectListItem { Text = "Send Email But Proceed", Value = "Send Email But Proceed" });

            return phy;
        }
        public List<SelectListItem> regionS(string m, string n)
        {


            List<SelectListItem> phy = new List<SelectListItem>();
            phy.Add(new SelectListItem { Text = m, Value = n });
            phy.Add(new SelectListItem { Text = "Drop Transaction", Value = "Drop Transaction" });
            phy.Add(new SelectListItem { Text = "Authorise Transaction", Value = "Authorise Transaction" });
            phy.Add(new SelectListItem { Text = "Half Settlement", Value = "Half Settlement" });
            phy.Add(new SelectListItem { Text = "Freeze Account", Value = "Freeze Account" });
            phy.Add(new SelectListItem { Text = "Send Email But Proceed", Value = "Send Email But Proceed" });

            return phy;
        }
    }
}
