﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BrokerOffice.DAO;
using BrokerOffice.Models;
using PagedList;
using System.Data.OleDb;
using OfficeOpenXml;
using WebMatrix.WebData;
using System.Xml;
using BrokerOffice.DAO.security;
using System.IO;
using System.Globalization;
using System.Transactions;
using System.Data.Entity.Migrations;
using BrokerOffice.DealNotes;
using BrokerOffice.DealClass;

namespace BrokerOffice.Controllers
{
    [CustomAuthorize(Roles = "User,Approver,BrokerAdmin,BrokerAdmin2", NotifyUrl = " /UnauthorizedPage")]
    public class OrdersController : Controller
    {
        private SBoardContext db = new SBoardContext();

        // GET: Orders
        public async Task<ActionResult> Index(string sortOrder, int? page)
        {


            ViewBag.CustomerSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.DateSortParm = String.IsNullOrEmpty(sortOrder) ? "date" : "";
            var per = from s in db.Order_Lives
                      select s;
            switch (sortOrder)
            {
                case "name_desc":
                    per = per.OrderByDescending(s => s.CDS_AC_No);
                    break;

                case "date":
                    per = per.OrderBy(s => s.Create_date);
                    break;

                default:
                    per = per.OrderBy(s => s.OrderNo);
                    break;
            }
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(per.ToPagedList(pageNumber, pageSize));
           // return View(await db.Order_Lives.ToListAsync());
        }

        // GET: Orders/Details/5
        public async Task<ActionResult> Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order_Live order_Live = await db.Order_Lives.FindAsync(id);
            if (order_Live == null)
            {
                return HttpNotFound();
            }
            return View(order_Live);
        }

      

        // GET: Orders/Delete/5
        public async Task<ActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order_Live order_Live = await db.Order_Lives.FindAsync(id);
            if (order_Live == null)
            {
                return HttpNotFound();
            }
            return View(order_Live);
        }

        // POST: Orders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(long id)
        {
            Order_Live order_Live = await db.Order_Lives.FindAsync(id);
            db.Order_Lives.Remove(order_Live);
            await db.SaveChangesAsync();
            System.Web.HttpContext.Current.Session["NOT"] = "You have successfully deleted the Order";

            return RedirectToAction("Index");
        }

        public ActionResult UploadExcel()
        {

            //ViewBag.Sucess = "";
            return View();
        }
        
       
        public ActionResult Upload(FormCollection formCollection)
        {
           int total = 0;

            var chk = 0;
            var reason = "";
            string fileExtension=System.IO.Path.GetExtension(Request.Files["FileUpload"].FileName);
           var ff= Request.Form["DD"].ToString();
            if (ff==null || ff=="")
            {
                System.Web.HttpContext.Current.Session["NOT"] = "THe upload date is required";

                return View("UploadExcel");

            }

           try
            {
                 chk = db.UplodedDealss.ToList().Where(a => a.fileName == Request.Files["FileUpload"].FileName).Count();

            }
            catch (Exception)
            {

                chk = 0;
            }
            if (chk>0)
            {
                System.Web.HttpContext.Current.Session["NOT"] = "File with name has already been uploaded " + Request.Files["FileUpload"].FileName;

                return View("UploadExcel");
            }
            try
            {

                if (fileExtension == ".xls" || fileExtension == ".xlsx" || fileExtension == ".csv")
                {

                    // Create a folder in App_Data named ExcelFiles because you need to save the file temporarily location and getting data from there. 
                    string fileLocation = Server.MapPath("~/File/" + Request.Files["FileUpload"].FileName);

                    if (System.IO.File.Exists(fileLocation))
                        System.IO.File.Delete(fileLocation);

                    Request.Files["FileUpload"].SaveAs(fileLocation);
                    string excelConnectionString = string.Empty;

                    excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileLocation +
                                            ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                    //connection String for xls file format.
                    if (fileExtension == ".xls")
                    {
                        excelConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + fileLocation +
                                                ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
                    }
                    //connection String for xlsx file format.
                    else if (fileExtension == ".xlsx")
                    {

                        excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileLocation +
                                                ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                    }
                    else if (fileExtension == ".txt")
                    {
                        excelConnectionString = "Provider = Microsoft.Jet.OLEDB.4.0; Data Source=" + fileLocation +
                                                ";Extended Properties=\"text;HDR=Yes;FMT=Delimited\"";
                    }
                    else if (fileExtension == ".csv")
                    {
                           string fileName = Path.Combine(Server.MapPath("~/CSV"), Guid.NewGuid().ToString() + ".csv");

                        //    //save file in the path
                        //    Request.Files["FileUpload"].SaveAs(fileName);

                        //    //read all the lines in the csv
                        //    string[] Lines = System.IO.File.ReadAllLines(fileName);
                        //    //declare array field for all rows with data to be uploaded
                        //    string[] Fields;

                        //    //skip the first line with the header
                        //    Lines = Lines.Skip(1).ToArray();
                        //    //create a generic list of customers of the model class customer
                        //    List<UplodedDeals> customers = new List<UplodedDeals>();
                        //    foreach (var line in Lines)
                        //    {
                        //        //declare fields to the line split
                        //        Fields = line.Split(new char[] { ',' });



                        //        //validate all fields from csv
                        //        if (Fields != null)
                        //        {
                        //            string ss = "";
                        //            //DateTime parsed = DateTime.ParseExact(ss,
                        //            //                              "ddd MMM d HH:mm:ss  yyyy",
                        //            //                              CultureInfo.InvariantCulture);
                        //            if (chkclient(Fields[4].Replace("\"", "").ToString()) == false)
                        //            {
                        //                ss = "EXCEPTION";
                        //            }
                        //            else
                        //            {
                        //                ss = WebSecurity.CurrentUserName;

                        //            }
                        //            DateTime parsed = DateTime.Now;
                        //            //load customer model with values from csv
                        //            customers.Add(
                        //                new UplodedDeals
                        //                {
                        //                    Exchange = Fields[0].Replace("\"", "").ToString(),
                        //                    Market = Fields[1].Replace("\"", "").ToString(),
                        //                    Symbol = Fields[2].Replace("\"", "").ToString(),
                        //                    Trader = Fields[3].Replace("\"", "").ToString(),
                        //                    Client = Fields[4].Replace("\"", "").ToString(),
                        //                    Broker = Fields[5].Replace("\"", "").ToString(),
                        //                    OrderNumber = Fields[6].Replace("\"", "").ToString(),
                        //                    Price = Fields[7].Replace("\"", "").ToString(),
                        //                    Volume = Fields[8].Replace("\"", "").ToString(),
                        //                    TicketNumber = Fields[9].Replace("\"", "").ToString(),
                        //                    ExecutionDateTime = Convert.ToDateTime(ff),

                        //                    BuySell = Fields[11].Replace("\"", "").ToString(),
                        //                    ShortSell = Fields[12].Replace("\"", "").ToString(),
                        //                    fileName = Request.Files["FileUpload"].FileName,
                        //                    UploadedBy = ss,
                        //                });
                        //        }
                        //    }
                        //    int pc = 0;
                        //    // Save changes to the DataContext and save to the database
                        //    using (SBoardContext dc = new SBoardContext())
                        //    {
                        //        //for each record in the generic customer list store in the DataContext dbset
                        //        foreach (var i in customers)
                        //        {


                        //            if (ModelState.IsValid)
                        //            {
                        //                //Add customer object to dbset Customers from DataContext
                        //                dc.UplodedDealss.Add(i);
                        //            }

                        //            pc++;

                        //        }
                        //        //save the changes made to the DataContext
                        //        dc.SaveChanges(WebSecurity.CurrentUserName);
                        //        System.Web.HttpContext.Current.Session["NOT"] = "You have successfully uploaded " + pc + " deals";
                        try
                        {
                            System.IO.File.Delete(fileName);
                            System.IO.File.Delete(fileLocation);

                        }
                        catch (Exception)
                        {


                        }
                        System.Web.HttpContext.Current.Session["NOT"] = "Only xls,xlsx may be uploaded";

                        return View("UploadExcel");
                        }
                    
                    //Create Conneon to Excel work book and add oledb namespace
                    OleDbConnection excelConnection = new OleDbConnection(excelConnectionString);

                    excelConnection.Open();
                    DataTable dt = new DataTable();

                    dt = excelConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    if (dt == null)
                    {
                        return null;
                    }

                    String[] excelSheets = new String[dt.Rows.Count];
                    int t = 0;
                    //excel data saves in temp file here.
                    foreach (DataRow row in dt.Rows)
                    {
                        excelSheets[t] = row["TABLE_NAME"].ToString();
                        t++;
                    }
                    OleDbConnection excelConnection1 = new OleDbConnection(excelConnectionString);
                    DataSet ds = new DataSet();

                    string query = string.Format("Select * from [{0}]", excelSheets[0]);
                    using (OleDbDataAdapter dataAdapter = new OleDbDataAdapter(query, excelConnection1))
                    {
                        dataAdapter.Fill(ds);
                    }


                    if (Request != null)
                    {
                        HttpPostedFileBase file = Request.Files["FileUpload"];
                        if ((file != null) && (file.ContentLength != 0) && !string.IsNullOrEmpty(file.FileName))
                        {
                            string fileName = file.FileName;
                            string fileContentType = file.ContentType;
                            byte[] fileBytes = new byte[file.ContentLength];
                            var data = file.InputStream.Read(fileBytes, 0, Convert.ToInt32(file.ContentLength));
                            int xx = 0;
                            int cp= maxmD();
                            using (var package = new ExcelPackage(file.InputStream))
                            {
                                var currentSheet = package.Workbook.Worksheets;
                                var workSheet = currentSheet.First();
                                var noOfCol = workSheet.Dimension.End.Column;
                                var noOfRow = workSheet.Dimension.End.Row;
                                int n = noOfRow;
                                total = n - 1;
                                for (int rowIterator = 2; rowIterator < (n + 1); rowIterator++)
                                {
                                    UplodedDeals my = new UplodedDeals();
                                    my.Exchange = workSheet.Cells[rowIterator, 1].Value.ToString();
                                    my.Market = workSheet.Cells[rowIterator, 2].Value.ToString();
                                    my.Symbol = rebursesec(workSheet.Cells[rowIterator, 3].Value.ToString());
                                    my.Trader = workSheet.Cells[rowIterator, 4].Value.ToString();
                                    my.Client = workSheet.Cells[rowIterator, 5].Value.ToString().Replace(" ", "");
                                    my.Broker = workSheet.Cells[rowIterator, 6].Value.ToString();
                                    my.OrderNumber = workSheet.Cells[rowIterator, 7].Value.ToString();
                                    my.Price = workSheet.Cells[rowIterator, 8].Value.ToString();
                                    my.Volume = workSheet.Cells[rowIterator, 9].Value.ToString();
                                    my.TicketNumberN = workSheet.Cells[rowIterator, 10].Value.ToString();
                                    var sk = cp += 1;
                                    my.TicketNumber =(sk).ToString()+"U";
                                    my.ExecutionDateTime = Convert.ToDateTime(ff);
                                    my.BuySell = workSheet.Cells[rowIterator, 12].Value.ToString();
                                    my.ShortSell = workSheet.Cells[rowIterator, 13].Value.ToString();
                                    
                                     my.fileName = fileName;
                    
                                    if (chkclient(my.Client.Replace(" ","")) == false)
                                    {
                                        my.UploadedBy = "EXCEPTION";
                                        reason = "The ATP/CSD number in the file is not registered in Broker Office "+ my.Client.Replace(" ", "");
                                        break;
                                    }
                                    else
                                    {
                                        my.UploadedBy = WebSecurity.CurrentUserName;
                                    }
                                    decimal r=0;
                                    bool isNumericT = decimal.TryParse(my.Price, out r);

                                    if (isNumericT == false)
                                    {
                                        my.UploadedBy = "EXCEPTION";
                                        reason = "There is a row with a distorted price figure in the file";
                                        break;
                                    }
                                    decimal z = 0;
                                    bool isNumericTV = decimal.TryParse(my.Volume, out z);
                                    if (isNumericTV == false)
                                    {
                                        my.UploadedBy = "EXCEPTION";
                                        reason = "There is a row with a distorted volume figure in the file";
                                        break;
                                    }

                                    if (buysell(my.BuySell)==false)
                                    {
                                        my.UploadedBy = "EXCEPTION";
                                        reason = "BuySell column has wrong value";
                                        break;
                                    }
                                    if (my.UploadedBy=="EXCEPTION")
                                    {
                                        xx += 1;
                                    }
                                    db.UplodedDealss.Add(my);
                                   
                                }
                                if (reason=="")
                                {
                             db.SaveChanges(WebSecurity.CurrentUserName.ToString());

                                System.Web.HttpContext.Current.Session["NOT"] = "You have successfully uploaded " + (n-1).ToString() + " deals and " + (xx).ToString() + " exceptions.";

                                }else
                                {
                                    System.Web.HttpContext.Current.Session["NOT"] =reason;

                                }


                                try
                                {

                                }
                                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                                {

                                    Exception raise = dbEx;
                                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                                    {
                                        foreach (var validationError in validationErrors.ValidationErrors)
                                        {
                                            string message = string.Format("{0}:{1}",
                                                validationErrors.Entry.Entity.ToString(),
                                                validationError.ErrorMessage);
                                            // raise a new exception nesting
                                            // the current instance as InnerException
                                            raise = new InvalidOperationException(message, raise);
                                        }
                                    }
                                    throw raise;
                                }

                            }
                        }
                    }




                }
                else
                {

                }

            }
            catch (Exception)
            {
                System.Web.HttpContext.Current.Session["NOT"] = "File was not uploaded";


            }

            try
            {
                var fileLocation = Server.MapPath("~/File/" + Request.Files["FileUpload"].FileName);
                System.IO.File.Delete(fileLocation);
            }
            catch (Exception)
            {

               
            }
            return View("UploadExcel");
        }

        public bool buysell(string s)
        {
            bool me = false;
            if (s.ToLower() == "buy")
            {
                me = true;
            }

            if (s.ToLower() == "sell")
            {
                me = true;
            }

            return me;
        }
        public string rebursesec(string s)
        {
            string sem = s.ToLower().Replace(" ","");
            string two = "";
            var cd = db.para_issuers.ToList().Where(a=>a.Company.ToLower().Replace(" ", "")==sem);

            foreach (var p in cd)
            {
                two = p.ISIN;
            }

            if (two=="")
            {
                two = sem;
            }

            return two;
        }

        public int maxmD()
        {
            var max = 0;

            try
            {
                max = db.UplodedDealss.ToList().Max(a => a.UplodedDealsID);

            }
            catch (Exception)
            {

                max = 0;
            }
           // max = max + 1;

            //string maxw = max;

            return max;
        }
        public bool chkclient(string m)
        {
            bool tru = true;
            var ff = 0;

            try
            {
                ff = db.Account_Creations.ToList().Where(a => a.CDSC_Number == m).Count();

            }
            catch (Exception)
            {

                ff = 0;
                tru = false;
            }
            if (ff==0)
            {
                tru = false;
            }

            return tru;

        }
        public bool chkOrders(string m)
        {
            bool tru = true;
            var ff = 0;

            try
            {
                ff = db.UplodedDealss.ToList().Where(a => a.OrderNumber.ToLower().Replace(" ","")== m.ToLower().Replace(" ", "")).Count();

            }
            catch (Exception)
            {

                ff = 0;
                tru = false;
            }
            if (ff == 0)
            {
                tru = false;
            }

            return tru;

        }

        public DateTime AddBusinessDays(DateTime dateTime, int nDays)
        {
            var wholeWeeks = nDays / 5; //since nDays does not include weekdays every week is considered as 5 days
            var absDays = Math.Abs(nDays);
            var remaining = absDays % 5; //results in the number remaining days to add or substract excluding the whole weeks
            var direction = nDays / absDays;//results in 1 if nDays is posisive or -1 if it's negative
            while (dateTime.DayOfWeek == DayOfWeek.Saturday || dateTime.DayOfWeek == DayOfWeek.Sunday)
                dateTime = dateTime.AddDays(direction); //If we are already in a weekend, get out of it
            while (remaining-- > 0)
            {//add remaining days...
                dateTime = dateTime.AddDays(direction);
                if (dateTime.DayOfWeek == DayOfWeek.Saturday)
                    dateTime = dateTime.AddDays(direction * 2);//...skipping weekends
            }
            return dateTime.AddDays(wholeWeeks * 7); //Finally add the whole weeks as 7 days, thus skipping the weekends without checking for DayOfWeek
        }
        public string GetNames(string acc)
        {
            var c = db.Account_Creations.ToList().Where(a => a.CDSC_Number == acc);
            string nme = "";
            foreach (var p in c)
            {
                nme = p.Surname_CompanyName + " " + p.OtherNames;
            }
            return nme;
        }
        public Boolean isTaxable(string s)
        {
            Boolean test = false;
            string nm = "";
            var pace = db.Account_Creations.ToList().Where(a => a.CDSC_Number == s);
            foreach (var z in pace)
            {
                nm = z.ClientType;
            }
            if (nm == "Taxable")
            {
                test = true;
            }
            return test;
        }
        

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
