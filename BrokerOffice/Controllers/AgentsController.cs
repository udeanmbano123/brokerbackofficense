﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BrokerOffice.DAO;
using BrokerOffice.Models;
using BrokerOffice.DAO.security;
using RestSharp;
using Newtonsoft.Json;
using BrokerOffice.CDSMOdel;
using System.Data.Entity.Migrations;

namespace BrokerOffice.Controllers
{
    [CustomAuthorize(Roles = "Admin", NotifyUrl = "/UnauthorizedPage")]
    // [CustomAuthorize(Users = "1,2")]
    public class AgentsController : Controller
    {
        private SBoardContext db = new SBoardContext();

        // GET: Agents
        public async Task<ActionResult> Index()
        {
            return View(await db.Agents.ToListAsync());
        }

        // GET: Agents/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Agent agent = await db.Agents.FindAsync(id);
            if (agent == null)
            {
                return HttpNotFound();
            }
            return View(agent);
        }

        // GET: Agents/Create
        public ActionResult Create()
        {
            //get list from api
            var list3 = "";
            var client = new RestClient("http://localhost/BrokerService");
            var request = new RestRequest("BrokersList", Method.GET);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            List<Broker> dataList = JsonConvert.DeserializeObject<List<Broker>>(validate);


            var dbsel = from s in dataList
                        select s;

            ////Create List of SelectListItem
            List<SelectListItem> selectlist = new List<SelectListItem>();
            selectlist.Add(new SelectListItem() { Text = "", Value = "" });
            foreach (var row in dbsel)
            {
                //Adding every record to list  

                selectlist.Add(new SelectListItem { Text = row.Company_name, Value = row.Company_name.ToString()});
            }
            ViewBag.Role = selectlist;
            System.Web.HttpContext.Current.Session["CDS"] = ViewBag.Role;
            return View();
        }

        // POST: Agents/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "AgentID,AgentCode,AgentName,PracticingID,Email,address,mobilenumber,telephone,status,AgentType")] Agent agent)
        {
            agent.status = "Temp";
            var list3 = "";
            if (agent.AgentType == "Link Securities Dealer")
            {
  var client = new RestClient("http://localhost/BrokerService");
            var request = new RestRequest("Broker/{v}", Method.GET);
            request.AddUrlSegment("v",agent.AgentName);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            List<Broker> dataList = JsonConvert.DeserializeObject<List<Broker>>(validate);


            var dbsel = from s in dataList
                        select s;
            foreach(var p in dbsel)
            {
                agent.AgentCode = p.Company_Code;
            }
            }
          
            int c = db.Agents.ToList().Where(a => a.AgentCode == agent.AgentCode).Count();
            if (ModelState.IsValid && c<=0)
            {
                db.Agents.Add(agent);
                await db.SaveChangesAsync();
                System.Web.HttpContext.Current.Session["NOT"] = "You have successfully added the Agent";
                               return RedirectToAction("Index");
            }

            if (c > 0)
            {
                var mod = ModelState.First(d => d.Key == "AgentName");  // this
                mod.Value.Errors.Add("Duplicate Agents i.e Brokers are not allowed");
            }

            ViewBag.Role = (IEnumerable<SelectListItem>)System.Web.HttpContext.Current.Session["CDS"];
            return View(agent);
        }

        // GET: Agents/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Agent agent = await db.Agents.FindAsync(id);
            if (agent == null)
            {
                return HttpNotFound();
            }
            var client = new RestClient("http://localhost/BrokerService");
            var request = new RestRequest("BrokersList", Method.GET);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            List<Broker> dataList = JsonConvert.DeserializeObject<List<Broker>>(validate);


            var dbsel = from s in dataList
                        select s;

            ////Create List of SelectListItem
            List<SelectListItem> selectlist = new List<SelectListItem>();
            selectlist.Add(new SelectListItem() { Text = "", Value = "" });
            foreach (var row in dbsel)
            {
                //Adding every record to list  

                selectlist.Add(new SelectListItem { Text = row.Company_name, Value = row.Company_name.ToString() });
            }
            ViewBag.Role = selectlist;
            System.Web.HttpContext.Current.Session["CDS"] = ViewBag.Role;
            ViewBag.Pay = agent.AgentType;
            return View(agent);
        }

        // POST: Agents/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "AgentID,AgentCode,AgentName,PracticingID,Email,address,mobilenumber,telephone,status,AgentType")] Agent agent)
        {
            if (agent.AgentType == "Link Securities Dealer")
            {
                var client = new RestClient("http://localhost/BrokerService");
                var request = new RestRequest("Broker/{v}", Method.GET);
                request.AddUrlSegment("v", agent.AgentName);
                IRestResponse response = client.Execute(request);
                string validate = response.Content;
                List<Broker> dataList = JsonConvert.DeserializeObject<List<Broker>>(validate);


                var dbsel = from s in dataList
                            select s;
                foreach (var p in dbsel)
                {
                    agent.AgentCode = p.Company_Code;
                }
            }
            int c = db.Agents.ToList().Where(a => a.AgentCode == agent.AgentCode).Count();
           
            if (ModelState.IsValid && c<=1)
            {
                db.Agents.AddOrUpdate(agent);
                await db.SaveChangesAsync();
                System.Web.HttpContext.Current.Session["NOT"] = "You have successfully updated the Agent";

                return RedirectToAction("Index");
            }
            if (c > 1)
            {
                var mod = ModelState.First(d => d.Key == "AgentName");  // this
                mod.Value.Errors.Add("Duplicate Agents i.e Brokers are not allowed");
            }
            ViewBag.Role = "";
            var client2 = new RestClient("http://localhost/BrokerService");
            var request2 = new RestRequest("BrokersList", Method.GET);
            IRestResponse response2 = client2.Execute(request2);
            string validate2 = response2.Content;
            List<Broker> dataList2 = JsonConvert.DeserializeObject<List<Broker>>(validate2);


            var dbsel2 = from s in dataList2
                         select s;

            ////Create List of SelectListItem
            List<SelectListItem> selectlist = new List<SelectListItem>();
            selectlist.Add(new SelectListItem() { Text = agent.AgentName, Value = agent.AgentName });
            foreach (var row in dbsel2)
            {
                //Adding every record to list  

                selectlist.Add(new SelectListItem { Text = row.Company_name, Value = row.Company_name.ToString() });
            }
            ViewBag.Role = selectlist;
            return View(agent);
        }

        // GET: Agents/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Agent agent = await db.Agents.FindAsync(id);
            if (agent == null)
            {
                return HttpNotFound();
            }
            return View(agent);
        }

        // POST: Agents/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Agent agent = await db.Agents.FindAsync(id);
            db.Agents.Remove(agent);
            await db.SaveChangesAsync();
            System.Web.HttpContext.Current.Session["NOT"] = "You have successfully deleted the Agent";

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
