﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BrokerOffice.DAO;
using BrokerOffice.DealClass;
using BrokerOffice.DAO.security;
using PagedList;
using System.Data.Entity.Migrations;
using WebMatrix.WebData;

namespace BrokerOffice.Controllers
{
    [CustomAuthorize(Roles = "User,Approver,BrokerAdmin", NotifyUrl = " /UnauthorizedPage")]
        public class BrokerNotesController : Controller
    {
        private SBoardContext db = new SBoardContext();

        // GET: BrokerNotes
        public async Task<ActionResult> Index(string sortOrder, string currentFilter, string
 searchString, string Date1String, string Date2String, int? page)
        {
            var per = from s in db.DealerDGs2
                      select s;
            try
            {

                ViewBag.CurrentSort = sortOrder;
                ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "accountname1" : "";
                ViewBag.DealSortParm = String.IsNullOrEmpty(sortOrder) ? "deal" : "";
                ViewBag.NameSortParam2 = String.IsNullOrEmpty(sortOrder) ? "accountname2" : "";
                ViewBag.DateSortParm = String.IsNullOrEmpty(sortOrder) ? "dateposted" : "";
                ViewBag.CDSCSortParm = String.IsNullOrEmpty(sortOrder) ? "cdsc" : "";
                ViewBag.CDSCSortParm2 = String.IsNullOrEmpty(sortOrder) ? "cdsc2" : "";
                per = from s in db.DealerDGs2
                      select s;
                if (searchString != null)
                {
                    page = 1;
                }
                else
                {
                    searchString = currentFilter;
                }
                ViewBag.CurrentFilter = searchString;
                if (!String.IsNullOrEmpty(searchString))
                {
                    per = per.Where(s =>
                    (s.AccountName1).ToUpper().Contains(searchString.ToUpper())
                     ||
                      (s.AccountName2).ToUpper().Contains(searchString.ToUpper())
                      ||
                       (s.Account1).ToUpper().Contains(searchString.ToUpper())
                       ||
                     (s.Account2).ToUpper().Contains(searchString.ToUpper())
                     ||
                    (s.Deal).ToUpper().Contains(searchString.ToUpper())
               );
                }

                try
                {
                    Date1String = Request.QueryString["Date1String"].ToString();
                    Date2String = Request.QueryString["Date2String"].ToString();
                    if (!String.IsNullOrEmpty(Date1String) && !String.IsNullOrEmpty(Date2String))
                    {
                        System.Web.HttpContext.Current.Session["Date1"] = Date1String;
                        System.Web.HttpContext.Current.Session["Date2"] = Date2String;
                        ViewBag.dt1 = Date1String;
                        ViewBag.dt2 = Date2String;
                    }
                }
                catch (Exception)
                {


                }
                try
                {
                    if (System.Web.HttpContext.Current.Session["Date1"].ToString() != "" && System.Web.HttpContext.Current.Session["Date2"].ToString() != "")
                    {

                        Date1String = System.Web.HttpContext.Current.Session["Date1"].ToString();
                        Date2String = System.Web.HttpContext.Current.Session["Date2"].ToString();
                        ViewBag.dt1 = Date1String;
                        ViewBag.dt2 = Date2String;
                    }
                }
                catch (Exception)
                {


                }
                if (!String.IsNullOrEmpty(Date1String) && !String.IsNullOrEmpty(Date2String) && String.IsNullOrEmpty(searchString))
                {
                    System.Web.HttpContext.Current.Session["Date1"] = Date1String;
                    System.Web.HttpContext.Current.Session["Date2"] = Date2String;
                    DateTime date = Convert.ToDateTime(Date1String);
                    DateTime date2 = Convert.ToDateTime(Date2String);
                    DateTime date3 = date2.AddDays(1);
                    per = from s in db.DealerDGs2

                          where (s.DatePosted > date && s.DatePosted < date3)
                          select s;

                    var cc = per.Count();
                }
                ViewBag.dt1 = Date1String;
                ViewBag.dt2 = Date2String;

                switch (sortOrder)
                {

                    case "accountname1":
                        per = per.OrderByDescending(s => s.AccountName1);
                        break;
                    case "accountname2":
                        per = per.OrderBy(s => s.AccountName2);
                        break;
                    case "deal":
                        per = per.OrderBy(s => s.Deal);
                        break;
                    case "dateposted":
                        per = per.OrderBy(s => s.DatePosted);
                        break;
                    case "cdsc":
                        per = per.OrderBy(s => s.Account1);
                        break;
                    case "cdsc2":
                        per = per.OrderBy(s => s.Account2);
                        break;
                    default:
                        per = per.OrderBy(s => s.ID);
                        break;
                }



            }
            catch (Exception)
            {


            }
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            per = per.OrderByDescending(a=>a.DatePosted);
            return View(per.ToPagedList(pageNumber, pageSize));
        }

        // GET: BrokerNotes/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DealerDG2 dealerDG2 = await db.DealerDGs2.FindAsync(id);
            if (dealerDG2 == null)
            {
                return HttpNotFound();
            }
            return View(dealerDG2);
        }

        // GET: BrokerNotes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: BrokerNotes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,Security,Deal,Quantity,Price,Account1,Account2,DatePosted,SettlementDate,Ack,AccountName1,AccountName2,SecurityName,Exchange,Printed,Status,OldDeal")] DealerDG2 dealerDG2)
        {
            if (ModelState.IsValid)
            {
                db.DealerDGs2.Add(dealerDG2);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(dealerDG2);
        }

        // GET: BrokerNotes/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DealerDG2 dealerDG2 = await db.DealerDGs2.FindAsync(id);
            if (dealerDG2 == null)
            {
                return HttpNotFound();
            }
            ViewBag.Exchange = regionD(dealerDG2.Exchange, dealerDG2.Exchange);

            return View(dealerDG2);
        }

        // POST: BrokerNotes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,Security,Deal,Quantity,Price,Account1,Account2,DatePosted,SettlementDate,Ack,AccountName1,AccountName2,SecurityName,Exchange,Printed,Status,OldDeal,PostDate,SettDate")] DealerDG2 dealerDG2)
        {
            if (ModelState.IsValid)
            {
                dealerDG2.Status = "TOUPDATE";
                db.Entry(dealerDG2).State = EntityState.Modified;
                await db.SaveChangesAsync(WebSecurity.CurrentUserName);
                System.Web.HttpContext.Current.Session["NOT"] = "Broker Note updated successfully waiting authorization,:"+dealerDG2.Deal;

                return RedirectToAction("Index");
            }
            ViewBag.Exchange = regionD(dealerDG2.Exchange, dealerDG2.Exchange);

            return View(dealerDG2);
        }

        // GET: BrokerNotes/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DealerDG2 dealerDG2 = await db.DealerDGs2.FindAsync(id);
            if (dealerDG2 == null)
            {
                return HttpNotFound();
            }
            return View(dealerDG2);
        }

        // POST: BrokerNotes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            DealerDG2 dealerDG2 = await db.DealerDGs2.FindAsync(id);
            dealerDG2.Status = "DELETE";
            db.DealerDGs2.AddOrUpdate(dealerDG2);
            await db.SaveChangesAsync(WebSecurity.CurrentUserName);
            System.Web.HttpContext.Current.Session["NOT"] = "Broker Note updated successfully waiting authorization for removal,:" + dealerDG2.Deal;

            return RedirectToAction("Index");
        }

        public async Task<ActionResult> Print(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DealerDG2 dealerDG2 = await db.DealerDGs2.FindAsync(id);
            if (dealerDG2 == null)
            {
                return HttpNotFound();
            }
            return View(dealerDG2);
        }

        // POST: BrokerNotes/Delete/5
        [HttpPost, ActionName("Print")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> PrintConfirmed(int id)
        {
            DealerDG2 dealerDG2 = await db.DealerDGs2.FindAsync(id);
            dealerDG2.Printed = true;
            dealerDG2.Status = "TOPRINT";
            db.DealerDGs2.AddOrUpdate(dealerDG2);
            await db.SaveChangesAsync(WebSecurity.CurrentUserName);
            System.Web.HttpContext.Current.Session["NOT"] = "Broker Note has been marked as printed successfully waiting authorization,:" + dealerDG2.Deal;

            return RedirectToAction("Index");
        }


        public async Task<ActionResult> UnPrint(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DealerDG2 dealerDG2 = await db.DealerDGs2.FindAsync(id);
            if (dealerDG2 == null)
            {
                return HttpNotFound();
            }
            return View(dealerDG2);
        }

        // POST: BrokerNotes/Delete/5
        [HttpPost, ActionName("UnPrint")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> UnPrintConfirmed(int id)
        {
            DealerDG2 dealerDG2 = await db.DealerDGs2.FindAsync(id);
            dealerDG2.Printed = false;
            dealerDG2.Status = "TOPRINTU";
            db.DealerDGs2.AddOrUpdate(dealerDG2);
            await db.SaveChangesAsync(WebSecurity.CurrentUserName);
            System.Web.HttpContext.Current.Session["NOT"] = "Broker Note has been marked as printed successfully waiting authorization,:" + dealerDG2.Deal;

            return RedirectToAction("Index");
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        public List<SelectListItem> regionD()
        {


            List<SelectListItem> phy = new List<SelectListItem>();
            var p = db.TradingPlatforms.ToList();
            foreach (var z in p)
            {
                phy.Add(new SelectListItem { Text = z.Name, Value = z.Name });

            }
            return phy;
        }
        public List<SelectListItem> regionD(string m, string n)
        {


            List<SelectListItem> phy = new List<SelectListItem>();
            var p = db.TradingPlatforms.ToList();
            phy.Add(new SelectListItem { Text = m, Value = n });

            foreach (var z in p)
            {
                phy.Add(new SelectListItem { Text = z.Name, Value = z.Name });

            }
            return phy;
        }
    }
}
