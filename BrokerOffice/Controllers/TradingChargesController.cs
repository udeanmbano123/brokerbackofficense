﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BrokerOffice.DAO;
using BrokerOffice.Models;
using BrokerOffice.DAO.security;
using PagedList;

namespace BrokerOffice.Controllers
{// GET: Admin
 //[CustomAuthorize(RolesConfigKey = "RolesConfigKey")]
 // [CustomAuthorize(UsersConfigKey = "UsersConfigKey")]
    [CustomAuthorize(Roles = "Admin,User,Approver,BrokerAdmin")]
    // [CustomAuthorize(Users = "1")]
    public class TradingChargesController : Controller
    {
        private SBoardContext db = new SBoardContext();

        // GET: TradingCharges
        public async Task<ActionResult> Index(string sortOrder, int? page)
        {
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.CodeSortParm = String.IsNullOrEmpty(sortOrder) ? "code" : "";
            ViewBag.StockSortParm = String.IsNullOrEmpty(sortOrder) ? "stock" : "";
            var per = from s in db.TradingChargess
                      select s;
            switch (sortOrder)
            {
                case "name_desc":
                    per = per.OrderByDescending(s => s.ChargeName);
                    break;

                case "code":
                    per = per.OrderBy(s => s.chargecode);
                    break;
                case "stock":
                    per = per.OrderBy(s => s.stockisin);
                    break;
                default:
                    per = per.OrderBy(s => s.TradingChargesID);
                    break;
            }
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(per.ToPagedList(pageNumber, pageSize));
            //return View(await db.TradingChargess.ToListAsync());
        }

        // GET: TradingCharges/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TradingCharges tradingCharges = await db.TradingChargess.FindAsync(id);
            if (tradingCharges == null)
            {
                return HttpNotFound();
            }
            return View(tradingCharges);
        }

        // GET: TradingCharges/Create
        public ActionResult Create()
        {
            ViewBag.StockType = Registrars();
           
            var list4 = db.Accounts_Masters.ToList();
            //Create List of SelectListItem
            List<SelectListItem> selectlist4 = new List<SelectListItem>();
            selectlist4.Add(new SelectListItem() { Text ="", Value = "" });
            foreach (var row in list4)
            {
                //Adding every record to list  
                selectlist4.Add(new SelectListItem { Text = row.AccountName, Value = row.AccountNumber.ToString() });
            }

            ViewBag.Chart = selectlist4;
            ViewBag.Fre = Frequency();
            return View();
        }

        // POST: TradingCharges/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "TradingChargesID,stockisin,StockSecuritieesID,chargecode,chargedescription,tradeside,ChargedAs,chargevalue,chargefrequency,ChargeName,chartaccount,chartaccountcode")] TradingCharges tradingCharges)
        {
        
            ViewBag.StockType = RegistrarsM(tradingCharges.stockisin, tradingCharges.stockisin);
            if (ModelState.IsValid)
            {
     
                
                string acc = Request["chartaccount"].ToString();
                int accr = Convert.ToInt32(acc);
                var brs = db.Accounts_Masters.ToList().Where(a => a.AccountNumber == accr);

                string rm2 = "";
                string code2 = "";
                foreach (var q in brs)
                {
                    tradingCharges.chartaccount = q.AccountName;
                    tradingCharges.chargeaccountcode = q.AccountNumber;
                }

                db.TradingChargess.Add(tradingCharges);
                await db.SaveChangesAsync();
                System.Web.HttpContext.Current.Session["NOT"] = "You have successfully added the Trading Charge";

                return RedirectToAction("Index");
            }

     
            var list4 = db.Accounts_Masters.ToList();
            //Create List of SelectListItem
            List<SelectListItem> selectlist4 = new List<SelectListItem>();
            selectlist4.Add(new SelectListItem() { Text = Request["chartaccount"], Value = Request["chartaccount"] });
            foreach (var row in list4)
            {
                //Adding every record to list  
                selectlist4.Add(new SelectListItem { Text = row.AccountName, Value = row.AccountNumber.ToString() });
            }

            ViewBag.Chart = selectlist4;
            ViewBag.Fre = FrequencyM(tradingCharges.chargefrequency,tradingCharges.chargefrequency);
            return View(tradingCharges);
        }

        // GET: TradingCharges/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TradingCharges tradingCharges = await db.TradingChargess.FindAsync(id);
            if (tradingCharges == null)
            {
                return HttpNotFound();
            }
            ViewBag.StockType = RegistrarsM(tradingCharges.stockisin,tradingCharges.stockisin);
            ViewBag.Fre = FrequencyM(tradingCharges.chargefrequency, tradingCharges.chargefrequency);


            var list4 = db.Accounts_Masters.ToList();
            //Create List of SelectListItem
            List<SelectListItem> selectlist4 = new List<SelectListItem>();
            selectlist4.Add(new SelectListItem() { Text = tradingCharges.chartaccount, Value = tradingCharges.chargeaccountcode.ToString()});
            foreach (var row in list4)
            {
                //Adding every record to list  
                selectlist4.Add(new SelectListItem { Text = row.AccountName, Value = row.AccountNumber.ToString() });
            }

            ViewBag.Chart = selectlist4;
            return View(tradingCharges);
        }

        // POST: TradingCharges/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "TradingChargesID,stockisin,StockSecuritieesID,chargecode,chargedescription,tradeside,ChargedAs,chargevalue,chargefrequency,ChargeName,chartaccount,chartaccountcode")] TradingCharges tradingCharges)
        {
            int cs = 0;
            int accr = 0;
            ViewBag.StockType = RegistrarsM(tradingCharges.stockisin, tradingCharges.stockisin);
      
            if (ModelState.IsValid)
            {
               
                tradingCharges.StockSecuritieesID = 0;
                
                string acc = Request["chartaccount"].ToString();
                try
                {
                    accr = Convert.ToInt32(acc);
                }
                catch (Exception)
                {

                    accr = 0;
                }
                var brs = db.Accounts_Masters.ToList().Where(a => a.AccountNumber == accr);
                try
                {
                    cs = Convert.ToInt32(acc);
                }
                catch (Exception)
                {
                    cs = 0;
                }

                string rm2 = "";
                string code2 = "";
                if (cs==0)
                {
 foreach (var q in brs)
                {
                    tradingCharges.chartaccount= q.AccountName;
                    tradingCharges.chargeaccountcode = q.AccountNumber;
                }
                }else
                {
                    var brsz = db.Accounts_Masters.ToList().Where(a => a.AccountNumber == cs);
                    foreach (var q in brsz)
                    {
                        tradingCharges.chartaccount = q.AccountName;
                        tradingCharges.chargeaccountcode = q.AccountNumber;
                    }
                }
               

                db.Entry(tradingCharges).State = EntityState.Modified;
                await db.SaveChangesAsync();
                System.Web.HttpContext.Current.Session["NOT"] = "You have successfully updated the Trading Charge";

                return RedirectToAction("Index");
            }
 

            var list4 = db.Accounts_Masters.ToList();
            //Create List of SelectListItem
            List<SelectListItem> selectlist4 = new List<SelectListItem>();
            selectlist4.Add(new SelectListItem() { Text = Request["chartaccount"], Value = Request["chartaccount"] });
            foreach (var row in list4)
            {
                //Adding every record to list  
                selectlist4.Add(new SelectListItem { Text = row.AccountName, Value = row.AccountNumber.ToString() });
            }
            ViewBag.Fre = FrequencyM(tradingCharges.chargefrequency, tradingCharges.chargefrequency);

            ViewBag.Chart = selectlist4;
            return View(tradingCharges);
        }

        // GET: TradingCharges/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TradingCharges tradingCharges = await db.TradingChargess.FindAsync(id);
            if (tradingCharges == null)
            {
                return HttpNotFound();
            }
            return View(tradingCharges);
        }

        // POST: TradingCharges/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            TradingCharges tradingCharges = await db.TradingChargess.FindAsync(id);
            db.TradingChargess.Remove(tradingCharges);
            await db.SaveChangesAsync();
            System.Web.HttpContext.Current.Session["NOT"] = "You have successfully deleted the Trading Charge";

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public List<SelectListItem> Registrars()
        {

            List<SelectListItem> phy = new List<SelectListItem>();
            var query = from u in db.para_issuers select u;
          
                    phy.Add(new SelectListItem { Text = "EQUITIES", Value = "EQUITIES" });
             
            return phy;
        }
        public List<SelectListItem> RegistrarsM(string s, string v1)
        {

            List<SelectListItem> phy = new List<SelectListItem>();
            phy.Add(new SelectListItem { Text = s, Value = v1 });
           
            phy.Add(new SelectListItem { Text = "EQUITIES", Value = "EQUITIES" });
            
            return phy;
        }

        public List<SelectListItem> RegistrarsT()
        {

            List<SelectListItem> phy = new List<SelectListItem>();
          
                    phy.Add(new SelectListItem { Text = "", Value ="" });
            
            return phy;
        }
        public List<SelectListItem> RegistrarsTT(string v)
        {

            List<SelectListItem> phy = new List<SelectListItem>();

            phy.Add(new SelectListItem { Text =v, Value = v });

            return phy;
        }
        public List<SelectListItem> RegistrarsT(string s)
        {
            int my = Convert.ToInt32(s);

            List<SelectListItem> phy = new List<SelectListItem>();

            var query = from u in db.para_issuers where u.para_issuerID==my select u;
            if (query.Count() > 0)
            {
                foreach (var v in query)
                {
                    phy.Add(new SelectListItem { Text = v.Company, Value = v.Company.ToString() });
                }
            }
            return phy;
        }

        [HttpPost]
        public JsonResult AjaxMethod(string type, int value)
        {
            Depository model = new Depository();
            switch (type)
            {
                case "stockisin":
                    ViewBag.TradingName = RegistrarsT(value.ToString());
                    break;
             
            }
            return Json(ViewBag.TradingName);
        }

        public List<SelectListItem> Frequency()
        {
          

            List<SelectListItem> phy = new List<SelectListItem>();

            var query = from u in db.Frequenciess select u;
            if (query.Count() > 0)
            {
                foreach (var v in query)
                {
                    phy.Add(new SelectListItem { Text = v.FrequencyName, Value = v.FrequencyName.ToString() });
                }
            }
            return phy;
        }
        public List<SelectListItem> FrequencyM(string s,string v1)
        {
           

            List<SelectListItem> phy = new List<SelectListItem>();
                 phy.Add(new SelectListItem { Text = s, Value = v1});
               
            var query = from u in db.Frequenciess select u;
            if (query.Count() > 0)
            {
                foreach (var v in query)
                {
                    phy.Add(new SelectListItem { Text = v.FrequencyName, Value = v.FrequencyName.ToString() });
                }
            }
            return phy;
        }
    }
}
