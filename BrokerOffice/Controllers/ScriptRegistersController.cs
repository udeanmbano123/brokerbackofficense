﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BrokerOffice.DAO;
using BrokerOffice.Models;
using BrokerOffice.DAO.security;
using WebMatrix.WebData;

namespace BrokerOffice.Controllers
{
    [CustomAuthorize(Roles = "User,Approver,BrokerAdmin,BrokerAdmin2", NotifyUrl = " /UnauthorizedPage")]

    public class ScriptRegistersController : Controller
    {
        private SBoardContext db = new SBoardContext();

        // GET: ScriptRegisters
        public async Task<ActionResult> Index()
        {
            ViewBag.csd = System.Web.HttpContext.Current.Session["csd"].ToString();

            ViewBag.nme = System.Web.HttpContext.Current.Session["nme"].ToString();
            string nme= System.Web.HttpContext.Current.Session["csd"].ToString();


            return View(await db.ScriptRegisters.Where(a=>a.ClientNumber==nme).ToListAsync());
        }

        // GET: ScriptRegisters/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ScriptRegister scriptRegister = await db.ScriptRegisters.FindAsync(id);
            if (scriptRegister == null)
            {
                return HttpNotFound();
            }
            return View(scriptRegister);
        }

      

        // GET: ScriptRegisters/Create
        public ActionResult Create()
        {
            ViewBag.csd = System.Web.HttpContext.Current.Session["csd"].ToString();

            ViewBag.nme = System.Web.HttpContext.Current.Session["nme"].ToString();


            ViewBag.Exchange=regionD();
            ViewBag.Counter = regionE();
            ViewBag.Stat = regionS();
            return View();
        }
        public List<SelectListItem> regionD()
        {


            List<SelectListItem> phy = new List<SelectListItem>();
            var p = db.TradingPlatforms.ToList();
            foreach (var z in p)
            {
                phy.Add(new SelectListItem { Text = z.Name, Value = z.Name });

            }
            return phy;
        }
        public List<SelectListItem> regionE()
        {


            List<SelectListItem> phy = new List<SelectListItem>();
            var p = db.para_issuers.ToList();
            foreach (var z in p)
            {
                phy.Add(new SelectListItem { Text = z.ISIN, Value = z.ISIN });

            }
            return phy;
        }
        public List<SelectListItem> regionD(string m, string n)
        {


            List<SelectListItem> phy = new List<SelectListItem>();
            var p = db.TradingPlatforms.ToList();
            phy.Add(new SelectListItem { Text = m, Value = n });

            foreach (var z in p)
            {
                phy.Add(new SelectListItem { Text = z.Name, Value = z.Name });

            }
            return phy;
        }
        public List<SelectListItem> regionE(string m,string n)
        {


            List<SelectListItem> phy = new List<SelectListItem>();
            var p = db.para_issuers.ToList();
            phy.Add(new SelectListItem { Text = m, Value = n });

            foreach (var z in p)
            {
                phy.Add(new SelectListItem { Text = z.ISIN, Value = z.ISIN });

            }
            return phy;
        }
        public List<SelectListItem> regionS()
        {


            List<SelectListItem> phy = new List<SelectListItem>();
            var p = db.para_issuers.ToList();
            phy.Add(new SelectListItem { Text = "Received from Client", Value = "Received from Client" });
            phy.Add(new SelectListItem { Text = "Submitted to Tsec", Value = "Submitted to Tsec" });
            phy.Add(new SelectListItem { Text = "Dematerialized", Value = "Dematerialized" });

            return phy;
        }
        public List<SelectListItem> regionS(string m, string n)
        {


            List<SelectListItem> phy = new List<SelectListItem>();
            var p = db.para_issuers.ToList();
            phy.Add(new SelectListItem { Text = m, Value = n });
            phy.Add(new SelectListItem { Text = "Received from Client", Value = "Received from Client" });
            phy.Add(new SelectListItem { Text = "Submitted to Tsec", Value = "Submitted to Tsec" });
            phy.Add(new SelectListItem { Text = "Dematerialized", Value = "Dematerialized" });

            return phy;
        }
        // POST: ScriptRegisters/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ScriptRegisterID,Exchange,ClientNames,ClientNumber,Counter,CertificateNumber,NumberShares,Status")] ScriptRegister scriptRegister)
        {
            if (ModelState.IsValid)
            {
                db.ScriptRegisters.Add(scriptRegister);
                await db.SaveChangesAsync(WebSecurity.CurrentUserName);
                return RedirectToAction("Index");
            }
            ViewBag.Exchange = regionD(scriptRegister.Exchange, scriptRegister.Exchange);
            ViewBag.Counter = regionE(scriptRegister.Counter, scriptRegister.Counter);
            ViewBag.Stat = regionS(scriptRegister.Status, scriptRegister.Status);
            return View(scriptRegister);
        }

        // GET: ScriptRegisters/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ScriptRegister scriptRegister = await db.ScriptRegisters.FindAsync(id);
            if (scriptRegister == null)
            {
                return HttpNotFound();
            }
            ViewBag.Exchange = regionD(scriptRegister.Exchange, scriptRegister.Exchange);
            ViewBag.Counter = regionE(scriptRegister.Counter, scriptRegister.Counter);
            ViewBag.Stat = regionS(scriptRegister.Status, scriptRegister.Status);
            return View(scriptRegister);
        }

        // POST: ScriptRegisters/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ScriptRegisterID,Exchange,ClientNames,ClientNumber,Counter,CertificateNumber,NumberShares,Status")] ScriptRegister scriptRegister)
        {
            if (ModelState.IsValid)
            {
                db.Entry(scriptRegister).State = EntityState.Modified;
                await db.SaveChangesAsync(WebSecurity.CurrentUserName);
                return RedirectToAction("Index");
            }
            ViewBag.Exchange = regionD(scriptRegister.Exchange, scriptRegister.Exchange);
            ViewBag.Counter = regionE(scriptRegister.Counter, scriptRegister.Counter);
            ViewBag.Stat = regionS(scriptRegister.Status, scriptRegister.Status);
            return View(scriptRegister);
        }

        // GET: ScriptRegisters/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ScriptRegister scriptRegister = await db.ScriptRegisters.FindAsync(id);
            if (scriptRegister == null)
            {
                return HttpNotFound();
            }
            return View(scriptRegister);
        }

        // POST: ScriptRegisters/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            ScriptRegister scriptRegister = await db.ScriptRegisters.FindAsync(id);
            db.ScriptRegisters.Remove(scriptRegister);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
