﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BrokerOffice.DAO;
using BrokerOffice.Models;

namespace BrokerOffice.Controllers
{
    public class BatchController : Controller
    {
        private SBoardContext db = new SBoardContext();

        // GET: Batch
        public async Task<ActionResult> Index()
        {
            return View(await db.Batches.ToListAsync());
        }

        // GET: Batch/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Batch batch = await db.Batches.FindAsync(id);
            if (batch == null)
            {
                return HttpNotFound();
            }
            return View(batch);
        }

        

        // GET: Batch/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Batch batch = await db.Batches.FindAsync(id);
            if (batch == null)
            {
                return HttpNotFound();
            }
            return View(batch);
        }

        // POST: Batch/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Batch batch = await db.Batches.FindAsync(id);
            db.Batches.Remove(batch);
            await db.SaveChangesAsync();

            System.Web.HttpContext.Current.Session["NOT"] = "You have successfully deleted the Batch";

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
