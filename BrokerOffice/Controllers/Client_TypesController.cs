﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BrokerOffice.DAO;
using BrokerOffice.Models;
using BrokerOffice.DAO.security;
using System.Data.Entity.Migrations;
using WebMatrix.WebData;

namespace BrokerOffice.Controllers
{// GET: Admin
 //[CustomAuthorize(RolesConfigKey = "RolesConfigKey")]
 // [CustomAuthorize(UsersConfigKey = "UsersConfigKey")]
    [CustomAuthorize(Roles = "Admin")]
    // [CustomAuthorize(Users = "1")]
    public class Client_TypesController : Controller
    {
        private SBoardContext db = new SBoardContext();

        // GET: Client_Types
        public async Task<ActionResult> Index()
        {
            return View(await db.Client_Types.ToListAsync());
        }

        // GET: Client_Types/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Client_Types client_Types = await db.Client_Types.FindAsync(id);
            if (client_Types == null)
            {
                return HttpNotFound();
            }
            return View(client_Types);
        }

        // GET: Client_Types/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Client_Types/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Client_TypesID,ClientType,TaxCode,Rate")] Client_Types client_Types)
        {
            if (ModelState.IsValid)
            {
                db.Client_Types.Add(client_Types);
                await db.SaveChangesAsync(WebSecurity.CurrentUserName);
                System.Web.HttpContext.Current.Session["NOT"] = "You have successfully added the Client type";

                return RedirectToAction("Index");
            }

            return View(client_Types);
        }

        // GET: Client_Types/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Client_Types client_Types = await db.Client_Types.FindAsync(id);
            if (client_Types == null)
            {
                return HttpNotFound();
            }
            return View(client_Types);
        }

        // POST: Client_Types/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Client_TypesID,ClientType,TaxCode,Rate")] Client_Types client_Types)
        {
            if (ModelState.IsValid)
            {
                db.Entry(client_Types).State = EntityState.Modified;
                //db.Client_Types.AddOrUpdate(client_Types);
                await db.SaveChangesAsync(WebSecurity.CurrentUserName);
                System.Web.HttpContext.Current.Session["NOT"] = "You have successfully updated the Client";

                return RedirectToAction("Index");
            }
            return View(client_Types);
        }

        // GET: Client_Types/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Client_Types client_Types = await db.Client_Types.FindAsync(id);
            if (client_Types == null)
            {
                return HttpNotFound();
            }
            return View(client_Types);
        }

        // POST: Client_Types/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Client_Types client_Types = await db.Client_Types.FindAsync(id);
            db.Client_Types.Remove(client_Types);
            await db.SaveChangesAsync();
            System.Web.HttpContext.Current.Session["NOT"] = "You have successfully deleted the Client type";

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
