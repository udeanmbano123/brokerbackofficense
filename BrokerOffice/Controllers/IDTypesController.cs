﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BrokerOffice.DAO;
using BrokerOffice.Models;
using BrokerOffice.DAO.security;

namespace BrokerOffice.Controllers
{// GET: Admin
 //[CustomAuthorize(RolesConfigKey = "RolesConfigKey")]
 // [CustomAuthorize(UsersConfigKey = "UsersConfigKey")]
    [CustomAuthorize(Roles = "Admin")]
    // [CustomAuthorize(Users = "1")]
    public class IDTypesController : Controller
    {
        private SBoardContext db = new SBoardContext();

        // GET: IDTypes
        public async Task<ActionResult> Index()
        {
            return View(await db.IDTypess.ToListAsync());
        }

        // GET: IDTypes/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IDTypes iDTypes = await db.IDTypess.FindAsync(id);
            if (iDTypes == null)
            {
                return HttpNotFound();
            }
            return View(iDTypes);
        }

        // GET: IDTypes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: IDTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "IDTypesID,idname")] IDTypes iDTypes)
        {
            if (ModelState.IsValid)
            {
                db.IDTypess.Add(iDTypes);
                await db.SaveChangesAsync();
                System.Web.HttpContext.Current.Session["NOT"] = "You have successfully added the ID Type";

                return RedirectToAction("Index");
            }

            return View(iDTypes);
        }

        // GET: IDTypes/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IDTypes iDTypes = await db.IDTypess.FindAsync(id);
            if (iDTypes == null)
            {
                return HttpNotFound();
            }
            return View(iDTypes);
        }

        // POST: IDTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "IDTypesID,idname")] IDTypes iDTypes)
        {
            if (ModelState.IsValid)
            {
                db.Entry(iDTypes).State = EntityState.Modified;
                await db.SaveChangesAsync();
                System.Web.HttpContext.Current.Session["NOT"] = "You have successfully updated the ID type";

                return RedirectToAction("Index");
            }
            return View(iDTypes);
        }

        // GET: IDTypes/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IDTypes iDTypes = await db.IDTypess.FindAsync(id);
            if (iDTypes == null)
            {
                return HttpNotFound();
            }
            return View(iDTypes);
        }

        // POST: IDTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            IDTypes iDTypes = await db.IDTypess.FindAsync(id);
            db.IDTypess.Remove(iDTypes);
            await db.SaveChangesAsync();
            System.Web.HttpContext.Current.Session["NOT"] = "You have successfully deleted the ID Type";

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
