﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;

using BrokerOffice.DAO.security;
using BrokerOffice.DAO;
using BrokerOffice.Models;
using PagedList;

namespace BrokerOffice.Controllers
{ //[CustomAuthorize(RolesConfigKey = "RolesConfigKey")]
  // [CustomAuthorize(UsersConfigKey = "UsersConfigKey")]
    [CustomAuthorize(Roles = "Admin")]
    // [CustomAuthorize(Users = "1")]
    public class glyphiconsController : Controller
    {
        private SBoardContext db = new SBoardContext();

        // GET: glyphicons
        public async Task<ActionResult> Index(string sortOrder, int? page)
        {
      
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            var users = from s in db.glyhicons
                      select s;

            switch (sortOrder)
            {
                case "name_desc":
                    users = users.OrderByDescending(s => s.glyphiconname);
                    break;
                              

                default:
                    users = users.OrderBy(s => s.glyphiconID);
                    break;
            }
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(users.ToPagedList(pageNumber, pageSize));
           // return View(await db.glyhicons.ToListAsync());
        }

        // GET: glyphicons/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            glyphicon glyphicon = await db.glyhicons.FindAsync(id);
            if (glyphicon == null)
            {
                return HttpNotFound();
            }
            return View(glyphicon);
        }

        // GET: glyphicons/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: glyphicons/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "glyphiconID,glyphiconname")] glyphicon glyphicon)
        {
            if (ModelState.IsValid)
            {
                db.glyhicons.Add(glyphicon);
                await db.SaveChangesAsync();
                System.Web.HttpContext.Current.Session["NOT"] = "You have successfully added the glyphicon";

                return RedirectToAction("Index");
            }

            return View(glyphicon);
        }

        // GET: glyphicons/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            glyphicon glyphicon = await db.glyhicons.FindAsync(id);
            if (glyphicon == null)
            {
                return HttpNotFound();
            }
            return View(glyphicon);
        }

        // POST: glyphicons/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "glyphiconID,glyphiconname")] glyphicon glyphicon)
        {
            if (ModelState.IsValid)
            {
                db.Entry(glyphicon).State = EntityState.Modified;
                await db.SaveChangesAsync();
                System.Web.HttpContext.Current.Session["NOT"] = "You have successfully updated the glyphicon";

                return RedirectToAction("Index");
            }
            return View(glyphicon);
        }

        // GET: glyphicons/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            glyphicon glyphicon = await db.glyhicons.FindAsync(id);
            if (glyphicon == null)
            {
                return HttpNotFound();
            }
            return View(glyphicon);
        }

        // POST: glyphicons/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            glyphicon glyphicon = await db.glyhicons.FindAsync(id);
            db.glyhicons.Remove(glyphicon);
            await db.SaveChangesAsync();
            System.Web.HttpContext.Current.Session["NOT"] = "You have successfully deleted the glyphicon";

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
