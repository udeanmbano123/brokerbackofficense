﻿using BrokerOffice.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BrokerOffice.Controllers
{
    public class DownloadController : Controller
    {
        SBoardContext db = new SBoardContext();
        // GET: Download
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult DownloadFile(string id)
        {
            int my = Convert.ToInt32(id);
            var dd = db.Accounts_Documentss.ToList().Where(a => a.Accounts_DocumentsID == my);
            foreach (var c in dd)
            {


                var cd = new System.Net.Mime.ContentDisposition { FileName = c.Name, Inline = false };
                byte[] arr = c.Data;

                Response.ContentType = c.ContentType;
                Response.AddHeader("content-disposition", cd.ToString());
                Response.Buffer = true;
                Response.Clear();
                Response.BinaryWrite(arr);
                Response.End();
            }
            return new FileStreamResult(Response.OutputStream, Response.ContentType);
        }
    }
}