﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BrokerOffice.DAO;
using BrokerOffice.Models;
using BrokerOffice.DAO.security;
using System.Data.Entity.Migrations;
using WebMatrix.WebData;
using PagedList;

namespace BrokerOffice.Controllers
{
    [CustomAuthorize(Roles = "User", NotifyUrl = "/UnauthorizedPage")]
    // [CustomAuthorize(Users = "1,2")]
    public class ClientPortfoliosController : Controller
    {
        private SBoardContext db = new SBoardContext();

        // GET: ClientPortfolios
        public async Task<ActionResult> Index(string sortOrder, int? page)
        {
            ViewBag.StockSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.HoldingsSortParm = String.IsNullOrEmpty(sortOrder) ? "module" : "";
            var per = from s in db.ClientPortfolioss
                      select s;
            switch (sortOrder)
            {
                case "name_desc":
                    per = per.OrderByDescending(s => s.Stock);
                    break;

                case "module":
                    per = per.OrderBy(s => s.Holdings);
                    break;

                default:
                    per = per.OrderBy(s => s.ClientPortfoliosID);
                    break;
            }
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(per.ToPagedList(pageNumber, pageSize));
          //  return View(await db.ClientPortfolioss.ToListAsync());
        }

        // GET: ClientPortfolios/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ClientPortfolios clientPortfolios = await db.ClientPortfolioss.FindAsync(id);
            if (clientPortfolios == null)
            {
                return HttpNotFound();
            }
            return View(clientPortfolios);
        }

        // GET: ClientPortfolios/Create
        public ActionResult Create()
        {
            var list4 = db.StockSecuritiess.ToList();
            //Create List of SelectListItem
            List<SelectListItem> selectlist4 = new List<SelectListItem>();
            selectlist4.Add(new SelectListItem() { Text = "", Value = "" });
            foreach (var row in list4)
            {
                //Adding every record to list  
                selectlist4.Add(new SelectListItem { Text = row.StockISIN, Value = row.StockISIN  });
            }
            ViewBag.Dlyp = selectlist4;

            var list5 = db.Account_Creations.ToList();
            //Create List of SelectListItem
            List<SelectListItem> selectlist5 = new List<SelectListItem>();
            selectlist5.Add(new SelectListItem() { Text = "", Value = "" });
            foreach (var row in list5)
            {
                //Adding every record to list  
                selectlist5.Add(new SelectListItem { Text = "Names: "+row.OtherNames+","+row.Surname_CompanyName+" Customer Number"+row.CDSC_Number, Value = row.CDSC_Number });
            }
            ViewBag.Clyp = selectlist5;
            return View();
        }

        // POST: ClientPortfolios/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ClientPortfoliosID,ClientNumber,Stock,Holdings,Stockf,Clientf")] ClientPortfolios clientPortfolios)
        {
            //boolean to determine to sum up holdings lol
            bool holdistrue = false;
            int holdistrueid = 0;
            decimal currh = 0;
            if (ModelState.IsValid)
            {
                string fellow = Request["Stock"].ToString();
                string fellow2 = Request["ClientNumber"].ToString();
                var br = db.StockSecuritiess.ToList().Where(a => a.StockISIN == fellow);
                var br2 = db.Account_Creations.ToList().Where(a => a.CDSC_Number == fellow2);
                string rm = "";
                string code = "";
                string rm2 = "";
                string code2 = "";
                foreach (var q in br)
                {
                    rm = q.StockISIN;
                    code = q.Issuer;
                }
                clientPortfolios.Stock = rm;
                clientPortfolios.Stockf = code;

                foreach(var c in br2)
                {
                    rm2 = "Names: " + c.OtherNames + "," + c.Surname_CompanyName + " Customer Number" + c.CDSC_Number;
                    code2 = c.CDSC_Number;
                }
                clientPortfolios.Clientf = rm2;
                clientPortfolios.ClientNumber = code2;

                var checkn = db.ClientPortfolioss.ToList().Where(a => a.ClientNumber == clientPortfolios.ClientNumber && a.Stock == clientPortfolios.Stock && a.Stockf == clientPortfolios.Stockf);
                foreach(var c in checkn)
                {
                    holdistrue = true;
                    holdistrueid = c.ClientPortfoliosID;
                    currh = c.Holdings;
                }

                if(holdistrue==true && holdistrueid != null)
                {
                    //update the customers holdings
                    var my = db.ClientPortfolioss.Find(Convert.ToInt32(holdistrueid));

                    my.Holdings = currh + clientPortfolios.Holdings;
                    //Update Row in Table  
                    db.ClientPortfolioss.AddOrUpdate(my);
                    System.Web.HttpContext.Current.Session["NOT"] = "You have successfully added the Portfolio";

                    db.SaveChanges(WebSecurity.CurrentUserName.ToString());
                }
                else
                {
                 db.ClientPortfolioss.Add(clientPortfolios);
                await db.SaveChangesAsync(WebSecurity.CurrentUserName.ToString());
                }
            
                return RedirectToAction("Index");
            }
            var list4 = db.StockSecuritiess.ToList();
            //Create List of SelectListItem
            List<SelectListItem> selectlist4 = new List<SelectListItem>();
            selectlist4.Add(new SelectListItem() { Text = "", Value = "" });
            foreach (var row in list4)
            {
                //Adding every record to list  
                selectlist4.Add(new SelectListItem { Text = row.StockISIN, Value = row.StockISIN});
            }
            ViewBag.Dlyp = selectlist4;
            var list5 = db.Account_Creations.ToList();
            //Create List of SelectListItem
            List<SelectListItem> selectlist5 = new List<SelectListItem>();
            selectlist5.Add(new SelectListItem() { Text = "", Value = "" });
            foreach (var row in list5)
            {
                //Adding every record to list  
                selectlist5.Add(new SelectListItem { Text ="Names: " + row.OtherNames + "," + row.Surname_CompanyName + " Customer Number" + row.CDSC_Number, Value = row.CDSC_Number });
            }
            ViewBag.Clyp = selectlist5;
            return View(clientPortfolios);
        }

        // GET: ClientPortfolios/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ClientPortfolios clientPortfolios = await db.ClientPortfolioss.FindAsync(id);
            if (clientPortfolios == null)
            {
                return HttpNotFound();
            }
            var list4 = db.StockSecuritiess.ToList();
            //Create List of SelectListItem
            List<SelectListItem> selectlist4 = new List<SelectListItem>();
            selectlist4.Add(new SelectListItem() { Text = clientPortfolios.Stockf, Value = clientPortfolios.Stock});
            foreach (var row in list4)
            {
                //Adding every record to list  
                selectlist4.Add(new SelectListItem { Text = row.StockISIN, Value = row.StockISIN });
            }
            ViewBag.Dlyp = selectlist4;
            var list5 = db.Account_Creations.ToList();
            //Create List of SelectListItem
            List<SelectListItem> selectlist5 = new List<SelectListItem>();
            selectlist5.Add(new SelectListItem() { Text = clientPortfolios.Clientf, Value =clientPortfolios.ClientNumber});
            foreach (var row in list5)
            {
                //Adding every record to list  
                selectlist5.Add(new SelectListItem { Text="Names: " + row.OtherNames + "," + row.Surname_CompanyName + " Customer Number" + row.CDSC_Number, Value = row.CDSC_Number });
            }
            ViewBag.Clyp = selectlist5;
            return View(clientPortfolios);
        }

        // POST: ClientPortfolios/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        /// <summary>
        /// 
        /// </summary>
        /// <param name="clientPortfolios"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ClientPortfoliosID,ClientNumber,Stock,Holdings,Stockf,Clientf")] ClientPortfolios clientPortfolios)
        {
            if (ModelState.IsValid)
            {
                string fellow = Request["Stock"].ToString();
                string fellow2 = Request["ClientNumber"].ToString();
                var br = db.StockSecuritiess.ToList().Where(a => a.StockISIN == fellow);
                var br2 = db.Account_Creations.ToList().Where(a => a.CDSC_Number == fellow2);
                string rm = "";
                string code = "";
                string rm2 = "";
                string code2 = "";
                foreach (var q in br)
                {
                    rm = q.StockISIN;
                    code =  q.Issuer;
                }
                clientPortfolios.Stock = rm;
                clientPortfolios.Stockf = code;

                foreach(var c in br2)
                {
                    rm2 = "Names: " + c.OtherNames + "," + c.Surname_CompanyName + " Customer Number" + c.CDSC_Number;
                    code2 = c.CDSC_Number;
                }
                clientPortfolios.Clientf = rm2;
                clientPortfolios.ClientNumber = code2;
                db.Entry(clientPortfolios).State = EntityState.Modified;
                await db.SaveChangesAsync();
                System.Web.HttpContext.Current.Session["NOT"] = "You have successfully updated the Portfolio";

                return RedirectToAction("Index");
            }
            var list4 = db.StockSecuritiess.ToList();
            //Create List of SelectListItem
            List<SelectListItem> selectlist4 = new List<SelectListItem>();
            selectlist4.Add(new SelectListItem() { Text = clientPortfolios.Stock, Value = clientPortfolios.Stock });
            foreach (var row in list4)
            {
                //Adding every record to list  
                selectlist4.Add(new SelectListItem { Text = row.StockISIN , Value = row.StockISIN });
            }
            ViewBag.Dlyp = selectlist4;
            var list5 = db.Account_Creations.ToList();
            //Create List of SelectListItem
            List<SelectListItem> selectlist5 = new List<SelectListItem>();
            selectlist5.Add(new SelectListItem() { Text = clientPortfolios.Clientf, Value =clientPortfolios.ClientNumber });
            foreach (var row in list5)
            {
                //Adding every record to list  
                selectlist5.Add(new SelectListItem { Text ="Names: "+row.OtherNames+","+row.Surname_CompanyName+" Customer Number"+row.CDSC_Number, Value = row.CDSC_Number });
            }
            ViewBag.Clyp = selectlist5;
            return View(clientPortfolios);
        }

        // GET: ClientPortfolios/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ClientPortfolios clientPortfolios = await db.ClientPortfolioss.FindAsync(id);
            if (clientPortfolios == null)
            {
                return HttpNotFound();
            }
            return View(clientPortfolios);
        }

        // POST: ClientPortfolios/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            ClientPortfolios clientPortfolios = await db.ClientPortfolioss.FindAsync(id);
            db.ClientPortfolioss.Remove(clientPortfolios);
            await db.SaveChangesAsync();
            System.Web.HttpContext.Current.Session["NOT"] = "You have successfully deleted the Portfolio";

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
