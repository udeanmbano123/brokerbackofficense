﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BrokerOffice.DAO;
using BrokerOffice.Models;
using BrokerOffice.DAO.security;
using PagedList;

namespace BrokerOffice.Controllers
{//[CustomAuthorize(RolesConfigKey = "RolesConfigKey")]
 // [CustomAuthorize(UsersConfigKey = "UsersConfigKey")]
    [CustomAuthorize(Roles = "User,Approver,BrokerAdmin,BrokerAdmin2", NotifyUrl = " /UnauthorizedPage")]
    // [CustomAuthorize(Users = "1")]
    public class Accounts_MasterController : Controller
    {
        private SBoardContext db = new SBoardContext();

        // GET: Accounts_Master
        public async Task<ActionResult> Index(string sortOrder, int? page)
        {
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.AccountNumberSortParm = String.IsNullOrEmpty(sortOrder) ? "account" : "";
            ViewBag.GroupNameSortParm = String.IsNullOrEmpty(sortOrder) ? "group" : "";
            var per = from s in db.Accounts_Masters
                      select s;
            switch (sortOrder)
            {
                case "name_desc":
                    per = per.OrderByDescending(s => s.AccountName);
                    break;
                case "account":
                    per = per.OrderBy(s => s.AccountNumber);
                    break;
                case "group":
                    per = per.OrderBy(s => s.GroupName);
                    break;
                
                default:
                    per = per.OrderBy(s => s.Accounts_MasterID);
                    break;
            }
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(per.ToPagedList(pageNumber, pageSize));
           // return View(await db.Accounts_Masters.ToListAsync());
        }

        // GET: Accounts_Master/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Accounts_Master accounts_Master = await db.Accounts_Masters.FindAsync(id);
            if (accounts_Master == null)
            {
                return HttpNotFound();
            }
            return View(accounts_Master);
        }

        
        // GET: Accounts_Master/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Accounts_Master accounts_Master = await db.Accounts_Masters.FindAsync(id);
            if (accounts_Master == null)
            {
                return HttpNotFound();
            }
            return View(accounts_Master);
        }

        // POST: Accounts_Master/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Accounts_Master accounts_Master = await db.Accounts_Masters.FindAsync(id);
            db.Accounts_Masters.Remove(accounts_Master);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
