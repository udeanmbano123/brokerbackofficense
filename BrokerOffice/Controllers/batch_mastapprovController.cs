﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BrokerOffice.DAO;
using BrokerOffice.Models;
using WebMatrix.WebData;
using RestSharp;
using Newtonsoft.Json;
using PagedList;
using BrokerOffice.DAO.security;
using System.Data.Entity.Migrations;

namespace BrokerOffice.Controllers
{// [CustomAuthorize(RolesConfigKey = "RolesConfigKey")]
 // [CustomAuthorize(UsersConfigKey = "UsersConfigKey")]
    [CustomAuthorize(Roles = "Approver,BrokerAdmin", NotifyUrl = "/UnauthorizedPage")]
    // [CustomAuthorize(Users = "1,2")]
    public class batch_mastapprovController : Controller
    {
        private SBoardContext db = new SBoardContext();

        // GET: batch_mast
        public async Task<ActionResult> Index(string sortOrder, int? page)
        {
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.ModuleSortParm = String.IsNullOrEmpty(sortOrder) ? "module" : "";
            var per = from s in db.batch_mast
                      where s.OldBatchRef=="SUBMITTED"
                      select s;
            switch (sortOrder)
            {
                case "name_desc":
                    per = per.OrderByDescending(s => s.batch_no);
                    break;

                case "module":
                    per = per.OrderBy(s => s.datecreated);
                    break;

                default:
                    per = per.OrderBy(s => s.ID);
                    break;
            }
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(per.ToPagedList(pageNumber, pageSize));

        }
        // GET: Batch/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            batch_mast batch_mast = await db.batch_mast.FindAsync(id);
            if (batch_mast == null)
            {
                return HttpNotFound();
            }
            return View(batch_mast);
        }
        // GET: batch_mast/Create
        public ActionResult Create()
        {
            ViewBag.AgentList = Agent();
            ViewBag.Company = Company();

            return View();
        }

        // POST: batch_mast/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,batch_no,batch_type,app_shares,app_cash,status,Company,Agent,BrokerBatchRef,Verify,CreatedBy,apps,branch,datecreated,PayMode,BoxNo,OldBatchRef,Source,WebResponse")] batch_mast batch_mast)
        {
           // batch_mast.app_cash = 0;
          //  batch_mast.app_shares = 0;
            batch_mast.batch_type = "BBO";
            batch_mast.status = "C";
            batch_mast.BrokerBatchRef = batch_mast.Agent + "//" + batch_mast.batch_no;
            batch_mast.Verify = false;
            batch_mast.CreatedBy = WebSecurity.CurrentUserName;
            if (batch_mast.CreatedBy == "")
            {
                batch_mast.CreatedBy = "LoggedInUser";
            }
            batch_mast.branch = "N/A";
            batch_mast.datecreated = DateTime.Now;
            batch_mast.PayMode = "N/A";
            batch_mast.BoxNo = 0;
            batch_mast.OldBatchRef = "N/A";
            batch_mast.Source = "BBO";
            batch_mast.ID = 0;

            //check if number exists
            var pz = db.batch_mast.ToList().Where(a => a.batch_no == batch_mast.batch_no).Count();

            if (ModelState.IsValid && pz == 0)
            {


                //send to IPO lols
                //webservice

                db.batch_mast.Add(batch_mast);
                System.Web.HttpContext.Current.Session["NOT"] = "Batch created successsfully";

                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            if (pz > 0)
            {
                var mod = ModelState.First(c => c.Key == "batch_no");  // this
                mod.Value.Errors.Add("Duplicate batch numbers are not allowed");
            }
            ViewBag.AgentList = Agent();
            ViewBag.Company = Company();
            return View(batch_mast);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public List<SelectListItem> Agent()
        {

            var p = db.Users.ToList().Where(a => a.Email == WebSecurity.CurrentUserName);
            string mm = "";
            foreach (var z in p)
            {
                mm = z.BrokerCode;
            }
            var client = new RestClient("http://localhost/BrokerService");
            var request = new RestRequest("IPAgents", Method.GET);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            List<IPAgents> dataList = JsonConvert.DeserializeObject<List<IPAgents>>(validate);


            var dbsel = from s in dataList
                        where s.code==mm
                        select s;

            List<SelectListItem> phy = new List<SelectListItem>();

            var query = from u in dbsel select u;
            if (query.Count() > 0)
            {
                foreach (var v in query)
                {
                    phy.Add(new SelectListItem { Text = v.desc, Value = v.code });
                }
            }
            return phy;
        }

        public List<SelectListItem> Company()
        {

          
            var client = new RestClient("http://localhost/BrokerService");
            var request = new RestRequest("IPCompany", Method.GET);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            List<IPCompany> dataList = JsonConvert.DeserializeObject<List<IPCompany>>(validate);


            var dbsel = from s in dataList
               
                        select s;

            List<SelectListItem> phy = new List<SelectListItem>();

            var query = from u in dbsel select u;
            if (query.Count() > 0)
            {
                foreach (var v in query)
                {
                    phy.Add(new SelectListItem { Text = v.company_name, Value = v.company });
                }
            }
            return phy;
        }
        public async Task<ActionResult> SubmitBatch(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            batch_mast batch = await db.batch_mast.FindAsync(id);
            if (batch == null)
            {
                return HttpNotFound();
            }
            return View(batch);
        }
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            batch_mast batch_mast = await db.batch_mast.FindAsync(id);
            if (batch_mast == null)
            {
                return HttpNotFound();
            }
            if (batch_mast.OldBatchRef=="SUBMITTED")
            {
                System.Web.HttpContext.Current.Session["NOT"] = "The batch has been committed";

                return RedirectToAction("Index");
            }

            
            return View(batch_mast);
        }

        // POST: batch_mast1/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,batch_no,batch_type,app_shares,app_cash,status,Company,Agent,BrokerBatchRef,Verify,CreatedBy,apps,branch,datecreated,PayMode,BoxNo,OldBatchRef,Source,WebResponse")] batch_mast batch_mast)
        {
            if (ModelState.IsValid)
            {
                db.Entry(batch_mast).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(batch_mast);
        }
        // POST: Batch/Delete/5
        [HttpPost, ActionName("SubmitBatch")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SubmitBatchConfirmed(int id)
        {
            batch_mast batch = await db.batch_mast.FindAsync(id);

            var client = new RestClient("http://localhost/BrokerService");
            var request = new RestRequest("IPPPCompany/{f}", Method.GET);
            request.AddUrlSegment("f", batch.Company);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;

            var Jsonobject = JsonConvert.DeserializeObject<string>(validate);
            validate = Jsonobject.ToString();

            decimal minshares = Convert.ToDecimal(batch.app_shares);
            decimal appshr = Convert.ToDecimal(validate);
            //batch applications
            var e= db.BatchApplications.ToList().Where(a => a.BatchNo.ToString() == batch.batch_no).Count();
            var f= db.BatchApplications.ToList().Where(a => a.BatchNo.ToString() == batch.batch_no).Sum(a=>a.numberofshares);
            //batch sares
            if (minshares < appshr)
            {
                System.Web.HttpContext.Current.Session["NOT"] = "The minimum required shares for the batch " + appshr;

            }
            else if (Convert.ToDecimal(f) != batch.app_shares)
            {
                System.Web.HttpContext.Current.Session["NOT"] = "The batch application share total of " + f + " does not match with batch share total of " + batch.app_shares;

            }
            else if (e != Convert.ToInt32(batch.apps))
            {
                System.Web.HttpContext.Current.Session["NOT"] = "The batch application total of " + e + " does not match with batch header total of" + batch.apps + " applications";

            }
            else
            {
                var client2 = new RestClient("http://localhost/BrokerService");
                var request2 = new RestRequest("Batchn/{batchno}/{shrs}/{amt}/{comp}/{agg}/{user}/{numapps}", Method.POST);
                request2.AddUrlSegment("batchno", batch.batch_no.ToString().Replace(" ", string.Empty).Replace(":", string.Empty));
                request2.AddUrlSegment("shrs", batch.app_shares.ToString().Replace(" ", string.Empty).Replace(":", string.Empty));
                request2.AddUrlSegment("amt", batch.app_cash.ToString().Replace(" ", string.Empty).Replace(":", string.Empty));
                request2.AddUrlSegment("comp", batch.Company.ToString().Replace(" ", string.Empty).Replace(":", string.Empty));
                request2.AddUrlSegment("agg", batch.Agent.ToString().Replace(" ", string.Empty).Replace(":", string.Empty));
                request2.AddUrlSegment("user", batch.CreatedBy.ToString().Replace(" ", string.Empty).Replace(":", string.Empty));
                request2.AddUrlSegment("numapps", batch.apps.ToString().Replace(" ", string.Empty).Replace(":", string.Empty));
                IRestResponse response2 = client2.Execute(request2);
                string validate2 = response2.Content;
                var Jsonobject2 = JsonConvert.DeserializeObject<string>(validate2);

                validate2 = Jsonobject2.ToString();
                batch.WebResponse = validate2;
                batch.OldBatchRef = "AUTHORISED";
                batch.PayMode = WebSecurity.CurrentUserName;
                batch.dateauth = DateTime.Now;
                db.batch_mast.AddOrUpdate(batch);
                await db.SaveChangesAsync();
                if (validate2 == "The batch was successfully created.")
                {


                    //credit client accounts
                    var cs = db.BatchApplications.ToList().Where(a => a.BatchNo.ToString() == batch.batch_no);
                    foreach (var d in cs)
                    {
                        //add to trans
                        BrokerOffice.Models.Trans my = new BrokerOffice.Models.Trans();
                        my.Account = d.Clientnumber;
                        my.Category = "Batch Application Capture";
                        my.Credit = d.ClientAmount;
                        my.Debit = 0;
                        my.Narration = "Batch Application Number "+ batch.batch_no;
                        my.Reference_Number = batch.BrokerBatchRef;
                        my.TrxnDate = DateTime.Now;
                        my.Post = DateTime.Now;
                        my.Type = "Batch Application Capture";
                        my.PostedBy = WebSecurity.CurrentUserName;
                        db.Transs.Add(my);

                        //Update table  
                        db.SaveChanges(WebSecurity.CurrentUserName.ToString());
                    }
                    validate2 = "The batch was successfully submitted.";

                    var p = db.BatchApplications.ToList().Where(a => a.BatchNo.ToString() == batch.batch_no);

                    foreach (var d in p)
                    {
                        var client3 = new RestClient("http://localhost/BrokerService");
                        var request3 = new RestRequest("Batchnapp/{batchno}/{shrs}/{amt}/{comp}/{agg}/{user}/{cds}/{idno}", Method.POST);
                        request3.AddUrlSegment("batchno", batch.batch_no.ToString().Replace(" ", string.Empty).Replace(":", string.Empty));
                        request3.AddUrlSegment("shrs", d.numberofshares.ToString().Replace(" ", string.Empty).Replace(":", string.Empty));
                        request3.AddUrlSegment("amt", d.ClientAmount.ToString().Replace(" ", string.Empty).Replace(":", string.Empty));
                        request3.AddUrlSegment("comp", batch.Company.ToString().Replace(" ", string.Empty).Replace(":", string.Empty));
                        request3.AddUrlSegment("agg", batch.Agent.ToString().Replace(" ", string.Empty).Replace(":", string.Empty));
                        request3.AddUrlSegment("user", batch.CreatedBy.ToString().Replace(" ", string.Empty).Replace(":", string.Empty));
                        request3.AddUrlSegment("cds", d.Clientnumber.ToString().Replace(" ", string.Empty).Replace(":", string.Empty));
                        request3.AddUrlSegment("idno", d.BatchApplicationID.ToString().Replace(" ", string.Empty).Replace(":", string.Empty));
                        IRestResponse response3 = client3.Execute(request3);
                        string validate3 = response3.Content;
                        var Jsonobject3 = JsonConvert.DeserializeObject<string>(validate3);

                        validate3 = Jsonobject3.ToString();
                        //batch.WebResponse = validate2;
                    }
                }
                System.Web.HttpContext.Current.Session["NOT"] = validate2.Replace("."," ") + " and authorized.";

            }

            return RedirectToAction("Index");
        }

        // POST: Batch/Delete/5
       
        public async Task<ActionResult> RejectBatch(int id)
        {
            batch_mast batch = await db.batch_mast.FindAsync(id);

            var client = new RestClient("http://localhost/BrokerService");
            var request = new RestRequest("IPPPCompany/{f}", Method.GET);
            request.AddUrlSegment("f", batch.Company);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;

            var Jsonobject = JsonConvert.DeserializeObject<string>(validate);
            validate = Jsonobject.ToString();

            decimal minshares = Convert.ToDecimal(batch.app_shares);
            decimal appshr = Convert.ToDecimal(validate);
            //batch applications
            var e = db.BatchApplications.ToList().Where(a => a.BatchNo.ToString() == batch.batch_no).Count();
            var f = db.BatchApplications.ToList().Where(a => a.BatchNo.ToString() == batch.batch_no).Sum(a => a.numberofshares);
            //batch sares
            if (minshares < appshr)
            {
                System.Web.HttpContext.Current.Session["NOT"] = "The minimum required shares for the batch " + appshr;

            }
            else if (Convert.ToDecimal(f) != batch.app_shares)
            {
                System.Web.HttpContext.Current.Session["NOT"] = "The batch application share total of " + f + " does not match with batch share total of " + batch.app_shares;

            }
            else if (e != Convert.ToInt32(batch.apps))
            {
                System.Web.HttpContext.Current.Session["NOT"] = "The batch application total of " + e + " does not match with batch header total of" + batch.apps + " applications";

            }
            else
            {
         
                batch.OldBatchRef = "REJECTED";
                batch.status = "C";
                batch.PayMode = WebSecurity.CurrentUserName;
                batch.dateauth = DateTime.Now;
                db.batch_mast.AddOrUpdate(batch);
                await db.SaveChangesAsync();
           
                System.Web.HttpContext.Current.Session["NOT"] = "The batch was rejected.";

            }

            return RedirectToAction("Index");
        }

        public async Task<ActionResult> Apps(string id)
        {
            System.Web.HttpContext.Current.Session["BA"] = id;

            return Redirect("~/BatchApplicationsApprov/Index");
        }
    }
}
