﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BrokerOffice.DAO;
using BrokerOffice.Models;
using BrokerOffice.DAO.security;

namespace BrokerOffice.Controllers
{// GET: Admin
 //[CustomAuthorize(RolesConfigKey = "RolesConfigKey")]
 // [CustomAuthorize(UsersConfigKey = "UsersConfigKey")]
    [CustomAuthorize(Roles = "Admin")]
    // [CustomAuthorize(Users = "1")]
    public class CompanySecuritiesController : Controller
    {
        private SBoardContext db = new SBoardContext();

        // GET: CompanySecurities
        public async Task<ActionResult> Index()
        {

           ViewBag.Name= System.Web.HttpContext.Current.Session["Name"].ToString();
            ViewBag.ID=System.Web.HttpContext.Current.Session["ID"].ToString();
            int my = Convert.ToInt32(ViewBag.ID);
            return View(await db.compsecs.Where(a=>a.CompanyID==my).ToListAsync());
        }

        // GET: CompanySecurities/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CompanySecurity companySecurity = await db.compsecs.FindAsync(id);
            if (companySecurity == null)
            {
                return HttpNotFound();
            }
            return View(companySecurity);
        }

        // GET: CompanySecurities/Create
        public ActionResult Create()
        {
            ViewBag.Name = System.Web.HttpContext.Current.Session["Name"].ToString();
            ViewBag.ID = System.Web.HttpContext.Current.Session["ID"].ToString();

            return View();
        }

        // POST: CompanySecurities/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,Security_Type,Description,CompanyName,CompanyID")] CompanySecurity companySecurity)
        {
            if (ModelState.IsValid)
            {
                db.compsecs.Add(companySecurity);
                await db.SaveChangesAsync();
                System.Web.HttpContext.Current.Session["NOT"] = "You have successfully added the Company security";

                return RedirectToAction("Index");
            }
            ViewBag.Name = System.Web.HttpContext.Current.Session["Name"].ToString();
            ViewBag.ID = System.Web.HttpContext.Current.Session["ID"].ToString();

            return View(companySecurity);
        }

        // GET: CompanySecurities/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CompanySecurity companySecurity = await db.compsecs.FindAsync(id);
            if (companySecurity == null)
            {
                return HttpNotFound();
            }
            return View(companySecurity);
        }

        // POST: CompanySecurities/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,Security_Type,Description,CompanyName,CompanyID")] CompanySecurity companySecurity)
        {
            if (ModelState.IsValid)
            {
                db.Entry(companySecurity).State = EntityState.Modified;
                await db.SaveChangesAsync();
                System.Web.HttpContext.Current.Session["NOT"] = "You have successfully updated the company security";

                return RedirectToAction("Index");
            }
            return View(companySecurity);
        }

        // GET: CompanySecurities/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CompanySecurity companySecurity = await db.compsecs.FindAsync(id);
            if (companySecurity == null)
            {
                return HttpNotFound();
            }
            return View(companySecurity);
        }

        // POST: CompanySecurities/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            CompanySecurity companySecurity = await db.compsecs.FindAsync(id);
            db.compsecs.Remove(companySecurity);
            await db.SaveChangesAsync();
            System.Web.HttpContext.Current.Session["NOT"] = "You have successfully deleted the company security";

            return RedirectToAction("Index");
        }

        public async Task<ActionResult> SecList(int id)
        {

            try
            {
        
                var c = db.para_issuers.ToList().Where(a => a.para_issuerID == id);
                foreach (var d in c)
                {
                    System.Web.HttpContext.Current.Session["Name"] = d.Company;
                    System.Web.HttpContext.Current.Session["ID"] = d.para_issuerID;
                   
                }

                    return RedirectToAction("Index");

                
            }
            catch (Exception)
            {
                System.Web.HttpContext.Current.Session["NOT"] = "Issuer does not exsist";

                return Redirect("~/para_issuer/Index");
            }
            return View();
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
