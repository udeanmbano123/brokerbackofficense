﻿using BrokerOffice.DAO;
using BrokerOffice.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebMatrix.WebData;

namespace BrokerOffice.Accounting
{
    public partial class TransJ : System.Web.UI.Page
    {
        public string id = "";
        public string ff = "";
        private SBoardContext db = new SBoardContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            id = Request.QueryString["id"];
            ff = Request.QueryString["ff"].ToString();
            if (IsPostBack == false)
            {
                int n = 0;
                string batchno = Request.QueryString["id"].ToString();
                var pc = db.BatchHeaders.ToList().Where(a => a.BatchNo == batchno);
                foreach (var z in pc)
                {
                    bnumber.Text = z.BatchNo;
                    btran.Text = z.NumberOfTrxns.ToString();
                    btotal.Text = z.Amount.ToString();
                    n = z.NumberOfTrxns;
                    ViewState["trp"] = z.BatchType;
                }

                var p = 0;
                p = db.BatchTransactions.ToList().Where(a => a.BatchNo == batchno).Count();
                ViewState["tr"] = "";
                //if (p == n)
                //{
                //    System.Web.HttpContext.Current.Session["NOT"] = "You have reached the batch transaction limit";

                //    Response.Redirect("~/BatchTransactions/Index");

                //}

            }

            try
            {
                BindGrid();
            }
            catch (Exception)
            {

                
            }
        }

        public void BindGrid()
        {
             string Batch = System.Web.HttpContext.Current.Session["Batch"].ToString();
            // check batch status
            var pp = db.BatchTransactions.ToList().Where(a => a.BatchNo==Batch);
            ASPxGridView1.DataSource = pp;
            ASPxGridView1.DataBind();
        }


        protected void drpCredit_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtCrAcc.Text=drpCredit.SelectedItem.Value.ToString();
        }

        protected void Debit_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string dr = Debit.SelectedValue.ToString();

                string cr = Credit.SelectedValue.ToString();

                if (dr == "Client" && cr == "Client")
                {
                    msgbox("Client transactions are not allowed");
                    return;
                }

            }
            catch (Exception)
            {

                
            }
            drpDebit.DataSource = GetDataSource();
            drpDebit.DataBind();
            
        }
        private DataTable GetDataSource()
        {
           //Request.QueryString["drid"]
            //DataTable defined
            var dtSource = new DataTable();
            dtSource.Columns.Add("id", typeof(string));
            dtSource.Columns.Add("FullNames", typeof(string));
            string x = "";
            
            string name = "";
            int num = 0;

            try
            {
                num = Convert.ToInt32(drpCredit.SelectedItem.Value);
            }
            catch (Exception)
            {

                num = 0;
            }
            string pia = "";

            try
            {
                pia = drpCredit.SelectedItem.Value.ToString();
            }
            catch (Exception)
            {

                pia ="0";
            }
            if (Submit.Text == "Update")
            {
                int d = Convert.ToInt32(ViewState["tr"].ToString());
                var pcc = db.BatchTransactions.ToList().Where(a => a.ID == d);
                foreach (var f in pcc)
                {

                    dtSource.Rows.Add(f.AccountD, f.AccountNameD);

                }
            }
            //Fill rows
            if (Debit.SelectedIndex == 0)
            {
                dtSource.Rows.Add("1", "Please Select a group");
                var dd = from c in db.Accounts_Masters
                     where c.AccountNumber!=num
                         select c;
                foreach (var p in dd)
                {
                 
                    dtSource.Rows.Add(p.AccountNumber, p.AccountName);
                }

            }
            else if(Debit.SelectedIndex == 1)
            {
                dtSource.Rows.Add("1", "Please Select a client");
                var dd = from c in db.Account_Creations
                         where c.StatuSActive == true && c.CDSC_Number!=pia
                         select c;
                foreach (var p in dd)
                {
                    if (p.OtherNames == null)
                    {
                        name = p.Surname_CompanyName + " ," + p.CDSC_Number;
                    }
                    else if (p.OtherNames == "")
                    {
                        name = p.Surname_CompanyName + " ," + p.CDSC_Number;
                    }
                    else
                    {
                        name = p.OtherNames + " " + p.Surname_CompanyName + " ," + p.CDSC_Number;
                    }

                    dtSource.Rows.Add(p.CDSC_Number, name);
                }

            }//Fill rows



            return dtSource;
        }

        private DataTable GetDataSource2()
        {
            //DataTable defined
            var dtSource = new DataTable();
            dtSource.Columns.Add("id", typeof(string));
            dtSource.Columns.Add("FullNames", typeof(string));
            string x = "";

            string name = "";
            int num = 0;

            try
            {
                num = Convert.ToInt32(drpDebit.SelectedItem.Value);
            }
            catch (Exception)
            {

                num = 0;
            }
            string pia = "";
            try
            {
                pia = drpDebit.SelectedItem.Value.ToString();
            }
            catch (Exception)
            {

                pia = "0";
            }
            //Fill rows
            if (Submit.Text == "Update")
            {
                int d = Convert.ToInt32(ViewState["tr"].ToString());
                var pcc = db.BatchTransactions.ToList().Where(a => a.ID == d);
                foreach (var f in pcc)
                {

                    dtSource.Rows.Add(f.Account, f.AccountName);

                }
            }
            if (Credit.SelectedIndex == 0)
            {
                dtSource.Rows.Add("1", "Please Select a group");
                var dd = from c in db.Accounts_Masters
                         where c.AccountNumber!=num
                         select c;
                foreach (var p in dd)
                {

                    dtSource.Rows.Add(p.AccountNumber, p.AccountName);
                }

            }
            else if (Credit.SelectedIndex == 1)
            {
                dtSource.Rows.Add("1", "Please Select a client");
                var dd = from c in db.Account_Creations
                         where c.StatuSActive == true && c.CDSC_Number!=pia
                         select c;
                foreach (var p in dd)
                {
                    if (p.OtherNames == null)
                    {
                        name = p.Surname_CompanyName + " ," + p.CDSC_Number;
                    }
                    else if (p.OtherNames == "")
                    {
                        name = p.Surname_CompanyName + " ," + p.CDSC_Number;
                    }
                    else
                    {
                        name = p.OtherNames + " " + p.Surname_CompanyName + " ," + p.CDSC_Number;
                    }

                    dtSource.Rows.Add(p.CDSC_Number, name);
                }

            }//Fill rows



            return dtSource;
        }

        public void msgbox(string strMessage)
        {
            string strScript = "<script language=JavaScript>";
            strScript += "window.alert(\"" + strMessage + "\");";
            strScript += "</script>";
            System.Web.UI.WebControls.Label lbl = new System.Web.UI.WebControls.Label();
            lbl.Text = strScript;
            Page.Controls.Add(lbl);
        }
        protected void Credit_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string dr = Debit.SelectedValue.ToString();
                string cr = Credit.SelectedValue.ToString();

                if (dr == "Client" && cr == "Client")
                {
                    msgbox("Client transactions are not allowed");
                    return;
                }
            }
            catch (Exception)
            {

             
            }
            drpCredit.DataSource = GetDataSource2();
            drpCredit.DataBind();
        }

        protected void Submit_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(drpDebit.SelectedItem.Text))
            {
                msgbox("An account must be selected for the Debit Account");
                return;
            }
            else if (String.IsNullOrEmpty(drpCredit.SelectedItem.Text))
            {
                msgbox("An account must be selected for the Credit Account");
                return;
            }

            if (drpDebit.SelectedItem.Text== "Please Select a group" || drpDebit.SelectedItem.Text == "Please Select a client")
            {
                msgbox("An account must be selected for the Debit Account");
                return;
            }else if (drpCredit.SelectedItem.Text == "Please Select a group" || drpCredit.SelectedItem.Text == "Please Select a client")
            {
                msgbox("An account must be selected for the Credit Account");
                return;
            }
            else if (txtnarr.Text=="")
            {
                msgbox("Reference is required");
                return;
             
            }
            else if (ASPxDateEdit1.Text == "")
            {
                msgbox("Transaction date is required");
                return;

            }
            //check existing sum
          decimal? cr = 0;
            decimal  dbr = 0;

            try
            {
                cr = db.BatchTransactions.ToList().Where(a=>a.BatchNo==bnumber.Text).Sum(a => a.Amount);

            }
            catch (Exception)
            {

                cr = 0;
            }

            
            var settotal = Convert.ToDecimal(btotal.Text);

            var nw = Convert.ToDecimal(txtamt.Text);

            if (((cr + nw)) > settotal && Submit.Text=="Submit")
            {
                msgbox("The amount will exceed the batch total");
                return;
            }

            decimal n;
            bool isNumericT = decimal.TryParse(txtamt.Text, out n);

            if (isNumericT == false)
            {
                msgbox("The amount has to be a figure");
                txtamt.Focus();
                return;
            }
            //if (RadioButtonList4.SelectedIndex<0)
            //{
            //    msgbox("The type of transaction is required");
            //    return;
            //}
            if (RadioButtonList1.SelectedIndex < 0)
            {
                msgbox("The payment type is required");
                return;
            }

            if (Submit.Text=="Submit")
            {
                //submit
                BrokerOffice.Models.BatchTransaction my = new BrokerOffice.Models.BatchTransaction();
                //Debit
                my.Account = txtCrAcc.Text;
                my.Reference = txtnarr.Text;
                my.Amount = Convert.ToDecimal(txtamt.Text);
                my.TrxnDate = Convert.ToDateTime(ASPxDateEdit1.Text);
                my.AccountName = drpCredit.SelectedItem.Text.ToString();
                my.BatchNo = bnumber.Text;
                my.BankID = ViewState["trp"].ToString();
                my.Description =my.Reference;
                my.BankAccName = RadioButtonList1.SelectedItem.Text;
                my.AccountD= txtDbAcc.Text;
                my.AccountNameD = drpDebit.SelectedItem.Text.ToString();
                my.trantype = Debit.SelectedValue.ToString();
                my.trantypeD= Credit.SelectedValue.ToString();
                my.Commit = false;
                my.filename = "None";
                db.BatchTransactions.Add(my);      
                //Update table  
                db.SaveChanges(WebSecurity.CurrentUserName.ToString());
                msgbox("You have successfully added the batch transaction");
                txtDbAcc.Text = "";
                txtCrAcc.Text = "";
                txtnarr.Text = "";
                txtamt.Text = "";
                drpCredit.SelectedIndex = 0;

                drpDebit.SelectedIndex = 0;
                Credit.SelectedIndex = 0;

               Debit.SelectedIndex = 0;
            }
            else if (Submit.Text=="Update")
            {
                Submit.Text = "Submit";
                //
                var crid = Convert.ToInt32(CrId.Text);
                var drid = Convert.ToInt32(DrId.Text);

               

                BatchTransaction my = db.BatchTransactions.Find(crid);
                //Debimy.i
                my.Account = txtCrAcc.Text;
                my.Reference = txtnarr.Text;
                my.Amount = Convert.ToDecimal(txtamt.Text);
                my.TrxnDate = Convert.ToDateTime(ASPxDateEdit1.Text);
                my.AccountName = drpCredit.SelectedItem.Text.ToString();
                my.BatchNo = bnumber.Text;
                my.Description = my.Reference;
                my.BankAccName = RadioButtonList1.SelectedItem.Text;
                my.BankID = ViewState["trp"].ToString();
                my.AccountD = txtDbAcc.Text;
                my.AccountNameD = drpDebit.SelectedItem.Text.ToString();
                my.trantype = Debit.SelectedValue.ToString();
                my.trantypeD = Credit.SelectedValue.ToString();
                my.Commit = false;
                my.filename = "None";
                db.BatchTransactions.AddOrUpdate(my);
                //Update table  
                db.SaveChanges(WebSecurity.CurrentUserName.ToString());
                Submit.Text = "Submit";
                msgbox("You have successfully updated the batch transaction");
                txtDbAcc.Text = "";
                txtCrAcc.Text = "";
                txtnarr.Text = "";
                txtamt.Text = "";
                drpCredit.SelectedIndex = 0;

                drpDebit.SelectedIndex = 0;
                Credit.SelectedIndex = 0;

                Debit.SelectedIndex = 0;

                try
                {
                    ASPxGridView1.Selection.UnselectAll();

                }
                catch (Exception)
                {

                  
                }
                try
                {
                    ASPxGridView1.Selection.CancelSelection();

                }
                catch (Exception)
                {

                    
                }

                Submit.Text = "Submit";
            }
            BindGrid();
            
            //Clear();
        }
        protected void Clear()
        {
            if(Submit.Text=="Submit")
            {
     System.Web.HttpContext.Current.Session["NOT"] = "";

            }else
            {
                System.Web.HttpContext.Current.Session["NOT"] = "";

            }

            Response.Redirect("~/BatchHeaders/Index");
        }

        protected void dxAction_Click(object sender, EventArgs e)
        {
            Label44.Text = "Action";
            TabName.Value = Label44.Text;
            return;
        }

        protected void dxPersonal_Click(object sender, EventArgs e)
        {
            Label44.Text = "personal";
            TabName.Value = Label44.Text;
            return;
        }

        protected void drpDebit_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtDbAcc.Text = drpDebit.SelectedItem.Value.ToString();
        }

        protected void ASPxGridView1_SelectionChanged(object sender, EventArgs e)
        {
            drpDebit.SelectedIndex = 0;
            drpCredit.SelectedIndex = 0;
            object key = null;
            for (int i = 0; i < ASPxGridView1.VisibleRowCount; i++)
            {
                if (ASPxGridView1.Selection.IsRowSelected(i))
                {
                    key = ASPxGridView1.GetRowValues(i, "ID");

                }

            }

            int my = Convert.ToInt32(key);

            //Credit.SelectedValue = z.
            //Request.QueryString["crid"]
            //Debit.SelectedValue = Request.QueryString["drtype"].ToString();

            string batchno = my.ToString();
            string batchnos = Request.QueryString["id"].ToString();
            ViewState["tr"]=batchno;
            var pc = db.BatchHeaders.ToList().Where(a => a.BatchNo == batchnos);
                foreach (var z in pc)
                {
                    bnumber.Text = z.BatchNo;
                    btran.Text = z.NumberOfTrxns.ToString();
                    btotal.Text = z.Amount.ToString();

                }
                int d = Convert.ToInt32(batchno);
                var pcc = db.BatchTransactions.ToList().Where(a => a.ID == d);
                foreach (var f in pcc)
                {
                    CrId.Text = f.ID.ToString();
                    DrId.Text = f.ID.ToString();
                    Credit.SelectedValue = f.trantype;
                    Debit.SelectedValue = f.trantypeD;
                    txtnarr.Text = f.Reference;
                    ASPxDateEdit1.Date = Convert.ToDateTime(f.TrxnDate.ToString());
                    txtamt.Text = f.Amount.ToString();
                if (f.BankID=="Cash")
                {
                    RadioButtonList4.SelectedIndex = 0;
                }else
                {
                    RadioButtonList4.SelectedIndex = 1;
                }

                if (f.BankAccName == "Receipt")
                {
                    RadioButtonList1.SelectedIndex = 0;
                }
                else
                {
                    RadioButtonList1.SelectedIndex = 1;
                }

                }
                Submit.Text = "Update";
                try
                {
                    string dr = Debit.SelectedValue.ToString();

                    string cr = Credit.SelectedValue.ToString();

                    if (dr == "Client" && cr == "Client")
                    {
                        msgbox("Client transactions are not allowed");
                        return;
                    }

                }
                catch (Exception)
                {


                }
                drpDebit.DataSource = GetDataSource();
                drpDebit.DataBind();


                try
                {
                    string dr = Debit.SelectedValue.ToString();
                    string cr = Credit.SelectedValue.ToString();


                }
                catch (Exception)
                {


                }
                drpCredit.DataSource = GetDataSource2();
                drpCredit.DataBind();
                txtDbAcc.Text = drpDebit.SelectedItem.Value.ToString();
                txtCrAcc.Text = drpCredit.SelectedItem.Value.ToString();
            
    
            }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Clear();
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            string Batch = System.Web.HttpContext.Current.Session["Batch"].ToString();

            Response.Redirect("~/BatchTransactions/Commit?batch="+ Batch +"");
        }

        protected void ASPxGridView1_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
        {
            try
            {
                int i = Convert.ToInt32(e.Keys[ASPxGridView1.KeyFieldName].ToString());
                string c = i.ToString();

                BatchTransaction user = db.BatchTransactions.Find(i);
                db.BatchTransactions.Remove(user);
                db.SaveChanges(WebSecurity.CurrentUserName);
                BindGrid();
                msgbox("You have removed the batch transaction");
            }
            catch (Exception)
            {


            }
            finally
            {
                e.Cancel = true;
            }
        }
    }
}