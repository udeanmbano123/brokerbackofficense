﻿<%@ Page Language="C#" MaintainScrollPositionOnPostback="true" MasterPageFile="~/Site1.Master"  AutoEventWireup="true" CodeBehind="Withdrawals.aspx.cs" Inherits="BrokerOffice.Accounting.Withdrawals" %>
<%@ Register Assembly="DevExpress.Web.v18.2, Version=18.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>


<asp:content id="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   
    <div>


  <div class="panel panel-default" style=" padding: 10px; margin: 10px">
    <div class="panel-heading" style="background-color:#428bca">
        <i style="color:white">Withdrawals</i>
    </div>
      <div class="form-group">
          <div class="col md-4">
              <asp:Label ID="Label6" CssClass="control-label col-md-4" runat="server" Text="Search Customer"></asp:Label>
              <asp:TextBox ID="txtSearch" CssClass="form-control" Width="150px" PlaceHolder="Search name......." runat="server"></asp:TextBox>
              <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-primary" Text="Search" OnClick="btnSearch_Click" />
          </div>
                             <asp:GridView ID="GridView1" CssClass="table table-striped" runat="server"   EmptyDataText="No records has been added."
    AutoGenerateSelectButton="true" onselectedindexchanged="GridView1_SelectedIndexChanged"
    AllowPaging="True" PageSize="5" OnPageIndexChanging="grdData_PageIndexChanging"
    >   <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />

</asp:GridView>

      <br />
        <div id="Tabs" role="tabpanel">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li class="active"><a href="#personal" aria-controls="personal" role="tab" data-toggle="tab">
                    Search Customer </a></li>
                <li ><a href="#Action" aria-controls="Action" role="tab" data-toggle="tab">
               Withdrawal Details</a></li>
          
            </ul>
            <!-- Tab panes -->
            <div class="tab-content" style="padding-top: 20px">
                  <div role="tabpanel" class="tab-pane active" id="personal">
                               <table class="table table-striped">
                                      <asp:Label ID="txtFkey" runat="server" Visible="False" Text=""></asp:Label>
<asp:Label ID="txtID" Visible="false" runat="server" Text="Label"></asp:Label>
                                    <tr>
                                <td>
                                    <asp:Label ID="Label3" CssClass="control-label col-md-6" runat="server" Text="Customer Number"></asp:Label>
                                      <br />
                                    <asp:TextBox ID="txtCustomerNumber" Width="150px" CssClass="form-control" runat="server" ReadOnly="True"></asp:TextBox>
                                 </td>
                                       <td>
                                           <asp:Label ID="Label4" CssClass="control-label col-md-6" runat="server" Text="Customer Names"></asp:Label>
                                             <br />
                                           <asp:TextBox ID="txtCustomerNames" Width="150px" CssClass="form-control" runat="server" ReadOnly="True"></asp:TextBox>
                                       </td>

                                       <td>
                                           &nbsp;</td>
                                   </tr>

                                   
                             </table>
            <dx style="float:right"><dx:ASPxButton ID="dxAction" runat="server" CssClass="btn btn-primary" Theme="Moderno" Text="Next" OnClick="dxAction_Click"  ></dx:ASPxButton></dx>
                                      
                     </div>
                <div role="tabpanel" class="tab-pane" id="Action">
                  <table class="table table-striped">
           
             
                      <tr>
                          <td>
                              <asp:Label ID="Label1" CssClass="control-label col-md-4"  runat="server" Text="Date"></asp:Label>
                                        <br />
                              <dx:ASPxDateEdit ID="txtDate" Width="150px" CssClass="form-control" runat="server"></dx:ASPxDateEdit>
                          </td>
                          <td>
                              <asp:Label ID="Label2" CssClass="control-label col-md-4" runat="server" Text="Amount"></asp:Label>
                                        <br />
                              <dx:ASPxTextBox ID="txtAmt" Width="150px" CssClass="form-control" runat="server" ></dx:ASPxTextBox>
                          </td>
                          <td>
                              <asp:Label ID="Label7" CssClass="control-label col-md-4" runat="server" Text="Description"></asp:Label>
                                        <br />
                              <dx:ASPxTextBox ID="txtDescr" Width="150px" CssClass="form-control" runat="server" ></dx:ASPxTextBox>

                          </td>
                      </tr>

                      <tr>
                          <td>
<asp:Button runat="server" CssClass="btn btn-primary" ID="Submit" Text="Submit" OnClick="Submit_Click" ></asp:Button>
                          </td>
              
                               <td>
                                           <asp:HiddenField ID="TabName" runat="server" />
                                    <asp:Label ID="Label44" runat="server" Visible="false" Text=""></asp:Label>
                                     
                                       </td>
                          
                      </tr>
                  </table>
                      <dx style="float:right"><dx:ASPxButton ID="dxPersonal" runat="server" CssClass="btn btn-primary" Theme="Moderno" Text="Previous" OnClick="dxPersonal_Click" ></dx:ASPxButton></dx>
              
                </div>
     
      </div>
        </div>
    </div>
      </div>
  
    </div>
     <script type="text/javascript">
$(function () {
    var tabName = $("[id*=TabName]").val() != "" ? $("[id*=TabName]").val() : "personal";
    $('#Tabs a[href="#' + tabName + '"]').tab('show');
    $("#Tabs a").click(function () {
        $("[id*=TabName]").val($(this).attr("href").replace("#", ""));
    });
});
</script>

</asp:content>
