﻿<%@ Page Language="C#" MaintainScrollPositionOnPostback="true" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Trans.aspx.cs" Inherits="BrokerOffice.Accounting.Trans" %>
<%@ Register Assembly="DevExpress.Web.v18.2, Version=18.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>


<asp:content id="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   
    <div>


  <div class="panel panel-default" style=" padding: 10px; margin: 10px">
    <div class="panel-heading" style="background-color:#428bca">
        <i style="color:white">Trans</i>
    </div>

      <br />
        <div id="Tabs" role="tabpanel">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li class="active"><a href="#personal" aria-controls="personal" role="tab" data-toggle="tab">
                    Trans</a></li>
                <li ><a href="#Action" aria-controls="Action" role="tab" data-toggle="tab">
                    Details </a></li>
          
            </ul>
            <!-- Tab panes -->
            <div class="tab-content" style="padding-top: 20px">
                  <div role="tabpanel" class="tab-pane active" id="personal">
                               <table class="table table-striped">
                                      <asp:Label ID="txtFkey" runat="server" Visible="False" Text=""></asp:Label>
<asp:Label ID="txtID" Visible="false" runat="server" Text="Label"></asp:Label>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label1" runat="server" Text="Debit Account"></asp:Label>
                                        <asp:RadioButtonList ID="Debit" AutoPostBack="true" runat="server" RepeatDirection="Horizontal" OnSelectedIndexChanged="Debit_SelectedIndexChanged">
                                             <asp:ListItem Text="GL" Value="GL"></asp:ListItem>
                                             <asp:ListItem Text="Client" Value="Client"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                    <td>
                                        <asp:Label ID="Label2" runat="server" Text="Credit Account"></asp:Label>
                                        <asp:RadioButtonList ID="Credit" AutoPostBack="true" runat="server" RepeatDirection="Horizontal" OnSelectedIndexChanged="Credit_SelectedIndexChanged">
                                                <asp:ListItem Text="GL" Value="GL"></asp:ListItem>
                                             <asp:ListItem Text="Client" Value="Client"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>                              

                                </tr>
                                   <tr>
                                       <td>
                                           <asp:Label ID="Label3" runat="server" Text="Select Debit Account"></asp:Label>
                                            <dx:ASPxComboBox ID="drpDebit" runat="server" ValueType="System.String" OnSelectedIndexChanged="drpDebit_SelectedIndexChanged" AutoPostBack="true"  TextField="FullNames" Theme="Glass" ValueField="id" CssClass="form-control" ></dx:ASPxComboBox>
                                       
                                       </td>
                                       <td>
                                           <asp:Label ID="Label4" runat="server" Text="Select Credit Account"></asp:Label>
                                             <dx:ASPxComboBox ID="drpCredit" runat="server" ValueType="System.String" OnSelectedIndexChanged="drpCredit_SelectedIndexChanged" AutoPostBack="true"  TextField="FullNames" Theme="Glass" ValueField="id" CssClass="form-control" ></dx:ASPxComboBox>
                                                                                </td>
                                   </tr>
                                   <tr>
                                       <td>
                                           <asp:Label ID="Label5" runat="server" Text="Debit Account Number"></asp:Label>
                                           <asp:TextBox ID="txtDbAcc" CssClass="form-control"  runat="server"></asp:TextBox>
                                       </td>
                                       <td>
                                           <asp:Label ID="Label7" runat="server" Text="Credit Account Number"></asp:Label>
                                           <asp:TextBox ID="txtCrAcc" CssClass="form-control"  runat="server"></asp:TextBox>
                                       </td>
                                   </tr>
                             </table>
                          <dx style="float:right"><dx:ASPxButton ID="dxAction" runat="server" CssClass="btn btn-primary" Theme="Moderno" Text="Next" OnClick="dxAction_Click"  ></dx:ASPxButton></dx>
              
                </div>
                <div role="tabpanel" class="tab-pane" id="Action">
                  <table class="table table-striped">

                     <tr>
                        <td>
                             <asp:Label ID="Label9" runat="server" Text="Reference"></asp:Label>
                             <asp:TextBox ID="txtnarr" CssClass="form-control"  runat="server"></asp:TextBox>
                         </td>
                         <td>
                             <asp:Label ID="Label10" runat="server" Text="Amount($)"></asp:Label>
                             <asp:TextBox ID="txtamt" CssClass="form-control" runat="server"></asp:TextBox>
                         </td>
                         <td>
                                  <asp:Label ID="Label6" runat="server" Text="Select Transaction Date"></asp:Label>
                             <dx:ASPxDateEdit ID="ASPxDateEdit1" runat="server" Theme="BlackGlass"></dx:ASPxDateEdit>
                         </td>
                     </tr>
                      <tr>
                          <td>
                   <asp:Button runat="server" CssClass="btn btn-primary" ID="Submit" Text="Submit" OnClick="Submit_Click"  ></asp:Button>
                          </td>
              
                               <td>
                                           <asp:HiddenField ID="TabName" runat="server" />
                                    <asp:Label ID="Label44" runat="server" Visible="false" Text=""></asp:Label>
                                     
                                       </td>
                          
                      </tr>
                  </table>
                      <dx style="float:right"><dx:ASPxButton ID="dxPersonal" runat="server" CssClass="btn btn-primary" Theme="Moderno" Text="Previous" OnClick="dxPersonal_Click" ></dx:ASPxButton></dx>
              
                </div>
      
            </div>
        </div>
    </div>
      
    </div>
      <script type="text/javascript">
$(function () {
    var tabName = $("[id*=TabName]").val() != "" ? $("[id*=TabName]").val() : "personal";
    $('#Tabs a[href="#' + tabName + '"]').tab('show');
    $("#Tabs a").click(function () {
        $("[id*=TabName]").val($(this).attr("href").replace("#", ""));
    });
});
</script>

</asp:content>