﻿<%@ Page Language="C#" MaintainScrollPositionOnPostback="true" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="TransJ.aspx.cs" Inherits="BrokerOffice.Accounting.TransJ" %>
<%@ Register Assembly="DevExpress.Web.v18.2, Version=18.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>


<asp:content id="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type = "text/javascript">
function DisableButtons() {
    var inputs = document.getElementsByTagName("INPUT");
    for (var i in inputs) {
        if (inputs[i].type == "button" || inputs[i].type == "submit") {
            inputs[i].disabled = true;
        }
    }
}
window.onbeforeunload = DisableButtons;
</script>
    <div>


  <div class="panel panel-default" style=" padding: 10px; margin: 10px">
    <div class="panel-heading" style="background-color:#428bca">
        <i style="color:white">CashBook Transactions</i>
    </div>

      <br />
        <div id="Tabs" role="tabpanel">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li class="active"><a href="#personal" aria-controls="personal" role="tab" data-toggle="tab">
                    Trans</a></li>
                <li ><a href="#Action" aria-controls="Action" role="tab" data-toggle="tab">
                     </a></li>
          
            </ul>
            <!-- Tab panes -->
            <div class="tab-content" style="padding-top: 20px">
                  <div role="tabpanel" class="tab-pane active" id="personal">
                               <table class="table table-striped">
                                      <asp:Label ID="txtFkey" runat="server" Visible="False" Text=""></asp:Label>
<asp:Label ID="txtID" Visible="false" runat="server" Text="Label"></asp:Label>
                              <tr>
                                  <td>
                                      <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Batch Number"></dx:ASPxLabel>
                                  </td>
                                  <td>
                                      <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="Batch Transactions"></dx:ASPxLabel>
                                  </td>
                                  <td>
                                      <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="Batch Total"></dx:ASPxLabel>
                                  </td>
                              </tr>
                                    <tr>
                                  <td>
                                      <dx:ASPxTextBox ID="bnumber" runat="server" CssClass="form-control" ReadOnly="True"></dx:ASPxTextBox>
                                  </td>
                                  <td>
                                      <dx:ASPxTextBox ID="btran" runat="server" CssClass="form-control" ReadOnly="True"></dx:ASPxTextBox>
                                  </td> <td>
                                      <dx:ASPxTextBox ID="btotal" runat="server" CssClass="form-control"  ReadOnly="True"></dx:ASPxTextBox>
                                  </td>
                              </tr>
                                  
                                     <tr>

                                    <td>
                                        <asp:Label ID="Label1" runat="server" Text="Debit Account"></asp:Label>
                                        <asp:RadioButtonList ID="Debit" AutoPostBack="true" runat="server" RepeatDirection="Horizontal" OnSelectedIndexChanged="Debit_SelectedIndexChanged">
                                             <asp:ListItem Text="GL" Value="GL"></asp:ListItem>
                                             <asp:ListItem Text="Client" Value="Client"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                    <td>
                                        <asp:Label ID="Label2" runat="server" Text="Credit Account"></asp:Label>
                                        <asp:RadioButtonList ID="Credit" AutoPostBack="true" runat="server" RepeatDirection="Horizontal" OnSelectedIndexChanged="Credit_SelectedIndexChanged">
                                                <asp:ListItem Text="GL" Value="GL"></asp:ListItem>
                                             <asp:ListItem Text="Client" Value="Client"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>                              
                                              <td>
                                  <asp:Label ID="Label8" runat="server" Text="Type" Visible="False"></asp:Label>
                          <asp:RadioButtonList ID="RadioButtonList4" runat="server" RepeatDirection="Horizontal" AutoPostBack="True"  Font-Bold="True" Height="22px" Width="173px" Font-Size="Small" Visible="False">
                         <asp:ListItem Text="Receipt" Value="Receipt">Receipt</asp:ListItem>                 
                         <asp:ListItem Text="Payment" Value="Payment">Payment</asp:ListItem> 

                                       
                                      </asp:RadioButtonList>
                              </td>
                                </tr>
                                   <tr>
                                       <td>
                                           <asp:Label ID="Label3" runat="server" Text="Select Debit Account"></asp:Label>
                                            <dx:ASPxComboBox ID="drpDebit" runat="server" ValueType="System.String" OnSelectedIndexChanged="drpDebit_SelectedIndexChanged" AutoPostBack="true"  TextField="FullNames" Theme="Glass" ValueField="id" CssClass="form-control" ></dx:ASPxComboBox>
                                       
                                       </td>
                                       <td>
                                           <asp:Label ID="Label4" runat="server" Text="Select Credit Account"></asp:Label>
                                             <dx:ASPxComboBox ID="drpCredit" runat="server" ValueType="System.String" OnSelectedIndexChanged="drpCredit_SelectedIndexChanged" AutoPostBack="true"  TextField="FullNames" Theme="Glass" ValueField="id" CssClass="form-control" ></dx:ASPxComboBox>
                                                                                </td>
                                   </tr>
                                   <tr>
                                       <td>
                                           <asp:Label ID="Label5" runat="server" Text="Debit Account Number"></asp:Label>
                                           <asp:TextBox ID="txtDbAcc" CssClass="form-control"  runat="server" ReadOnly="True"></asp:TextBox>
                                       </td>
                                       <td>
                                           <asp:Label ID="Label7" runat="server" Text="Credit Account Number"></asp:Label>
                                           <asp:TextBox ID="txtCrAcc" CssClass="form-control"  runat="server" ReadOnly="True"></asp:TextBox>
                                       </td>
                                        <td>
                                  <asp:Label ID="Label11" runat="server" Text="Payment Type"></asp:Label>
                                                   <asp:RadioButtonList ID="RadioButtonList1" runat="server" RepeatDirection="Horizontal" AutoPostBack="True"  Font-Bold="True" Height="22px" Width="173px" Font-Size="Small">
                         <asp:ListItem Text="Cash" Value="Cash">Cash</asp:ListItem>                 
                         <asp:ListItem Text="Bank" Value="Bank">Bank</asp:ListItem> 

                                       
                                      </asp:RadioButtonList>    

                              </td>
                                   </tr>
                                   
                     <tr>
                        <td>
                                                 <asp:Label ID="Label9" runat="server" Text="Reference"></asp:Label>
                             <asp:TextBox ID="txtnarr" CssClass="form-control"  runat="server"></asp:TextBox>
                         </td>
                         <td>
                             <asp:Label ID="Label10" runat="server" Text="Amount($)"></asp:Label>
                             <asp:TextBox ID="txtamt" CssClass="form-control" runat="server"></asp:TextBox>
                         </td>
                         <td>
                                  <asp:Label ID="Label6" runat="server" Text="Select Transaction Date"></asp:Label>
                             <dx:ASPxDateEdit ID="ASPxDateEdit1" runat="server" CssClass="form-control" Theme="BlackGlass"></dx:ASPxDateEdit>
                         </td>
                         </tr>
                                   <tr>
                    
                         
                                       <td></td>
                     </tr>
                      <tr>
                          <td>
                                  
 <asp:Button runat="server" CssClass="btn btn-primary" ID="Submit" Text="Submit" OnClick="Submit_Click" style="height:35px;width:auto"  ></asp:Button>
                           
                       
                                     </td>
                          <td>
 <asp:Button runat="server" CssClass="btn btn-primary" ID="Button1" Text="Back" style="height:35px;width:auto" OnClick="Button1_Click" ></asp:Button>
                       
                        
                       </td>
                          <td>
                              &nbsp;</td>
              
                               <td>
                                           <asp:HiddenField ID="TabName" runat="server" />
                                    <asp:Label ID="Label44" runat="server" Visible="false" Text=""></asp:Label>
                                   <dx:ASPxLabel ID="CrId" runat="server" Visible="false" Text="ASPxLabel"></dx:ASPxLabel>
                                   <dx:ASPxLabel ID="DrId" runat="server" Visible="false" Text="ASPxLabel"></dx:ASPxLabel>
                               </td>
                          
                      </tr>
                             </table>
                          <dx style="float:right"><dx:ASPxButton ID="dxAction" runat="server" CssClass="btn btn-primary" Theme="Moderno" Text="Next" OnClick="dxAction_Click" Visible="False"  ></dx:ASPxButton></dx>
              
                </div>
                <div role="tabpanel" class="tab-pane" id="Action">
                  <table class="table table-striped">

                  </table>
                      <dx style="float:right"><dx:ASPxButton ID="dxPersonal" runat="server" CssClass="btn btn-primary" Theme="Moderno" Text="Previous" OnClick="dxPersonal_Click" Visible="False" ></dx:ASPxButton>
                       
                  
                          </dx>
              
                </div>
      
            </div>
        </div>
    </div>
        
<asp:Button runat="server" CssClass="btn btn-primary" ID="Button2" Text="Process Batch" style="height:35px;width:auto" OnClientClick="return confirm('Are you sure want to send batch for authorization?');" OnClick="Button2_Click"></asp:Button>
                       
                  
    <dx:ASPxGridView ID="ASPxGridView1" KeyFieldName="ID"  AutoGenerateColumns="false"  runat="server" EnableCallBacks="False"  Theme="BlackGlass" OnRowDeleting="ASPxGridView1_RowDeleting"  OnSelectionChanged="ASPxGridView1_SelectionChanged" Visible="true" EnablePagingGestures="True" Width="960px" style="margin-right: 0px" Font-Size="X-Small"  >
                        <SettingsPager AlwaysShowPager="True" PageSize="30" EnableAdaptivity="True">
                            <PageSizeItemSettings Visible="True">
                            </PageSizeItemSettings>
                        </SettingsPager>
                        <SettingsBehavior  AllowSelectSingleRowOnly="True"   ProcessSelectionChangedOnServer="True" AllowSelectByRowClick="True" ConfirmDelete="True" />
                          <SettingsCommandButton>
                              <NewButton ButtonType="Button" Text="Audit">
                              </NewButton>
                              <CancelButton ButtonType="Button" Text="Audit">
                              </CancelButton>
                              <EditButton ButtonType="Button"  Text="Audit">
                              </EditButton>
                            <DeleteButton   ButtonType="Button" Text="Delete">
                            </DeleteButton>
                            <SelectButton ButtonType="Button">
                            </SelectButton>
                        </SettingsCommandButton>
                        <SettingsText CommandDelete="Are you sure you want to delete the batch transaction ?" ConfirmDelete="Are you sure you want to delete the batch transaction ?" />
        <Columns>

            <dx:GridViewCommandColumn ShowSelectCheckbox="True" Caption="Select"  VisibleIndex="0">
                </dx:GridViewCommandColumn>
                      <dx:GridViewDataTextColumn Width="150px" FieldName="ID" ReadOnly="True" VisibleIndex="1">
            
                </dx:GridViewDataTextColumn>
                           <dx:GridViewDataTextColumn Width="150px" FieldName="TrxnDate" ReadOnly="True" VisibleIndex="2">
            
                </dx:GridViewDataTextColumn>
                     <dx:GridViewDataTextColumn Width="150px"  Caption="Credit" FieldName="Account" VisibleIndex="3">
                </dx:GridViewDataTextColumn>
                  <dx:GridViewDataTextColumn Width="150px" Caption="Credit Name" FieldName="AccountName" VisibleIndex="4">
                </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Width="150px" Caption="Debit" FieldName="AccountD" VisibleIndex="5">
                </dx:GridViewDataTextColumn>
                                       
                                   <dx:GridViewDataTextColumn Width="150px" Caption="Debit Name" FieldName="AccountNameD" VisibleIndex="6">
                </dx:GridViewDataTextColumn>
                                       
                              <dx:GridViewDataTextColumn Width="150px" FieldName="BatchNo" VisibleIndex="7">
                </dx:GridViewDataTextColumn>
                                         
                              <dx:GridViewDataTextColumn Width="150px" FieldName="Amount" VisibleIndex="8">
                </dx:GridViewDataTextColumn>
                                            
            <dx:GridViewCommandColumn  ShowDeleteButton="True"  VisibleIndex="10">
                </dx:GridViewCommandColumn>
            
            </Columns>
                       

                    </dx:ASPxGridView>
         </div>
      <script type="text/javascript">
$(function () {
    var tabName = $("[id*=TabName]").val() != "" ? $("[id*=TabName]").val() : "personal";
    $('#Tabs a[href="#' + tabName + '"]').tab('show');
    $("#Tabs a").click(function () {
        $("[id*=TabName]").val($(this).attr("href").replace("#", ""));
    });
});
</script>

</asp:content>