﻿<%@ Page Language="C#" MaintainScrollPositionOnPostback="true" MasterPageFile="~/Site1.Master"  AutoEventWireup="true" CodeBehind="Account_Master.aspx.cs" Inherits="BrokerOffice.Accounting.Account_Master" %>
<%@ Register Assembly="DevExpress.Web.v18.2, Version=18.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>


<asp:content id="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   
    <div>


  <div class="panel panel-default" style=" padding: 10px; margin: 10px">
    <div class="panel-heading" style="background-color:#428bca">
        <i style="color:white">Accounts Master</i>
    </div>
        <div id="Tabs" role="tabpanel">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li class="active"><a href="#personal" aria-controls="personal" role="tab" data-toggle="tab">
                   Accounts Master</a></li>
             
          
            </ul>
            <!-- Tab panes -->
            <div class="tab-content" style="padding-top: 20px">
                  <div role="tabpanel" class="tab-pane active" id="personal">
                               <table class="table table-striped">
                                     <asp:Label ID="txtFkey" runat="server" Visible="False" Text=""></asp:Label>
                        
                                   <tr>
                                       <td>
                                           <asp:Label ID="Label1" CssClass="control-label col-md-6" runat="server" Text="Select GL Group"></asp:Label>
                                       </td>
                                       <td>
                                           <asp:DropDownList ID="drGroup" CssClass="form-control" runat="server" AutoPostBack="True" OnSelectedIndexChanged="drGroup_SelectedIndexChanged"></asp:DropDownList>
                                       </td>
                                   </tr>
                                   <tr>
                                       <td>
                                           <asp:Label ID="Label2" CssClass="control-label col-md-2" runat="server" Text="Account Number"></asp:Label>
                                           <asp:TextBox ID="accnum" ReadOnly="true" CssClass="form-control" runat="server"></asp:TextBox>
                                       </td>
                                       <td>
                                           <asp:Label ID="Label3" CssClass="control-label col-md-2" runat="server" Text="Account Name"></asp:Label>
                                           <asp:TextBox ID="txtAccname" CssClass="form-control" runat="server"></asp:TextBox>
                                       </td>
                                       <td>
                                           <asp:Label ID="Label4" CssClass="control-label col-md-2" runat="server" Text="Description"></asp:Label>
                                           <asp:TextBox TextMode="MultiLine" CssClass="form-control" ID="descr" runat="server"></asp:TextBox>
                                       </td>
                                   </tr>
                                   <tr>
                                       <td>
                                            <asp:Label ID="Label5" CssClass="control-label col-md-6" runat="server" Text="FinancialStatement"></asp:Label>
                                           <asp:DropDownList ID="DropDownList2" CssClass="form-control"  runat="server"></asp:DropDownList>
                                       </td>
                                       <td>
                                                  <asp:Label ID="Label6" CssClass="control-label col-md-6" runat="server" Text="Report Category"></asp:Label>
                                           <asp:DropDownList ID="DropDownList3" CssClass="form-control" runat="server" style="height: 22px"></asp:DropDownList>
                                       </td>
                                       <td>
                                                   <asp:Label ID="Label7" CssClass="control-label col-md-6" runat="server" Text="Debit/Credit"></asp:Label>
                                          
                                           <asp:DropDownList CssClass="form-control" ID="DropDownList1" runat="server">
                                               <asp:ListItem Value="Credit" Text="Credit"></asp:ListItem>
                                               <asp:ListItem Value="Debit" Text="Debit"></asp:ListItem>
                                           </asp:DropDownList>
                                       </td>
                                   </tr>
                              <tr>
                          <td>
<asp:Button ID="Submit" runat="server" CssClass="btn btn-primary" Text="Submit" OnClick="Submit_Click" ></asp:Button>
                          </td>
              
                               <td>
                                           <asp:HiddenField ID="TabName" runat="server" />

                                       </td>
                          
                      </tr>
                  </table>
                </div>
      
            </div>
        </div>
    </div>
      
    </div>
    <script type="text/javascript">
$(function () {
    var tabName = $("[id*=TabName]").val() != "" ? $("[id*=TabName]").val() : "personal";
    $('#Tabs a[href="#' + tabName + '"]').tab('show');
    $("#Tabs a").click(function () {
        $("[id*=TabName]").val($(this).attr("href").replace("#", ""));
    });
});
</script>

</asp:content>