﻿using BrokerOffice.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebMatrix.WebData;

namespace BrokerOffice.Accounting
{
    public partial class Withdrawals : System.Web.UI.Page
    {
        private SBoardContext db = new SBoardContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.IsPostBack)
            {
                TabName.Value = Request.Form[TabName.UniqueID];

            }
            else
            {
               // loadCustomers();


               
            }
        }
        protected void grdData_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            loadCustomers();
        }
        public void loadCustomers()
        {
            var action = from v in db.Account_Creations
                         let CDSNUMBER = v.CDSC_Number
                         let Surname = v.Surname_CompanyName
                         let Firstname = v.OtherNames

                         select new { v.ID_, CDSNUMBER, Surname, Firstname };

            GridView1.DataSource = action.ToList();
            GridView1.DataBind();
        }
        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //     public int ClientPortfoliosID { get; set; }
            //public string ClientNumber { get; set; }
            //public string ClientNames { get; set; }
            //public double clientholdings { get; set; }
            String pathname = GridView1.SelectedRow.Cells[1].Text;
            txtID.Text = pathname;
            txtCustomerNumber.Text = GridView1.SelectedRow.Cells[2].Text;
            txtCustomerNames.Text = GridView1.SelectedRow.Cells[3].Text + " " + GridView1.SelectedRow.Cells[4].Text;
       


        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (txtSearch.Text != "")
            {
                var action = from v in db.Account_Creations 
                             let CDSNUMBER = v.CDSC_Number
                             let Surname = v.Surname_CompanyName
                             let Firstname = v.OtherNames
                           
                             where (v.Surname_CompanyName + v.OtherNames + "" + v.CDSC_Number).Contains(txtSearch.Text)
                             select new { v.ID_, CDSNUMBER, Surname, Firstname };

                GridView1.DataSource = action.ToList();
                GridView1.DataBind();
            }
            else
            {
                loadCustomers();
            }

        }
        public void msgbox(string strMessage)
        {
            string strScript = "<script language=JavaScript>";
            strScript += "window.alert(\"" + strMessage + "\");";
            strScript += "</script>";
            System.Web.UI.WebControls.Label lbl = new System.Web.UI.WebControls.Label();
            lbl.Text = strScript;
            Page.Controls.Add(lbl);
        }
        protected void Submit_Click(object sender, EventArgs e)
        {
            decimal n;
            bool isNumericT = decimal.TryParse(txtAmt.Text, out n);

            if (isNumericT == false)
            {
                msgbox("The amount has to be a figure");
                txtAmt.Focus();
                return;
            }
            else if (txtDate.Text == "")
            {
                msgbox("Date has to be selected");
                txtDate.Focus();
                return;
            }
            else if (txtDescr.Text == "")
            {
                msgbox("Description has to be selected");
                txtDescr.Focus();
                return;
            }

            var acc = db.Accounts_Masters.ToList().Where(a => a.AccountName.Contains("Cash Deposits"));

              int num =0;
          foreach(var c in acc)
            {
                num = c.AccountNumber;
            }
            var acc2 = db.Accounts_Masters.ToList().Where(a => a.AccountName.Contains("Accounts Receivable"));

            int num2 = 0;
            foreach (var c in acc2)
            {
                num2 = c.AccountNumber;
            }


            //submit
            BrokerOffice.Models.Trans my = new BrokerOffice.Models.Trans();
            //Debit
            my.Account = txtCustomerNumber.Text;
            my.Category = "Withdrawals";
            my.Credit = 0;
            my.Debit = Convert.ToDecimal(txtAmt.Text);
            my.Narration = txtDescr.Text;
            my.Reference_Number = "Withdrawals";
            my.TrxnDate = txtDate.Date;
            my.Post = DateTime.Now;
            my.Type = "Withdrawals";
            my.PostedBy = WebSecurity.CurrentUserName;
            db.Transs.Add(my);

            //Update table  
            db.SaveChanges(WebSecurity.CurrentUserName.ToString());
            // Credit//
            my.Account = num.ToString();
            my.Category = "Withdrawals";
            my.Credit = Convert.ToDecimal(txtAmt.Text);
            my.Debit = 0;
            my.Narration = txtDescr.Text;
            my.Reference_Number = "Withdrawals";
            my.TrxnDate = txtDate.Date;
            my.Post = DateTime.Now;
            my.Type = "Withdrawals";
            my.PostedBy = WebSecurity.CurrentUserName;
            db.Transs.Add(my);

            //Update table  
            db.SaveChanges(WebSecurity.CurrentUserName.ToString());
            //doublle entry
            // Credit//
            my.Account = num2.ToString();
            my.Category = "Withdrawals";
            my.Credit = 0;
            my.Debit = Convert.ToDecimal(txtAmt.Text);
            my.Narration = txtDescr.Text;
            my.Reference_Number = "Withdrawals";
            my.TrxnDate = txtDate.Date;
            my.Post = DateTime.Now;
            my.Type = "Withdrawals";
            my.PostedBy = WebSecurity.CurrentUserName;
            db.Transs.Add(my);

            //Update table  
            db.SaveChanges(WebSecurity.CurrentUserName.ToString());
            System.Web.HttpContext.Current.Session["NOT"] = "You have successfully added the withdrawal";
            Response.Redirect("~/TransRecords/Index");
            //Update reduce the batch
        }
        protected void Clear()
        {
            Response.Redirect("~/TransRecords/Index");
        }

        protected void dxAction_Click(object sender, EventArgs e)
        {
            Label44.Text = "Action";
            TabName.Value = Label44.Text;
            return;
        }

        protected void dxPersonal_Click(object sender, EventArgs e)
        {
            Label44.Text = "personal";
            TabName.Value = Label44.Text;
            return;
        }
    }
}