﻿using BrokerOffice.DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebMatrix.WebData;

namespace BrokerOffice.Accounting
{
    public partial class Trans : System.Web.UI.Page
    {
        public string id = "";
        private SBoardContext db = new SBoardContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            id = Request.QueryString["id"];

        }

     

        protected void drpCredit_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtCrAcc.Text=drpCredit.SelectedItem.Value.ToString();
        }

        protected void Debit_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string dr = Debit.SelectedValue.ToString();

                string cr = Credit.SelectedValue.ToString();

                if (dr == "Client" && cr == "Client")
                {
                    msgbox("Client transactions are not allowed");
                    return;
                }

            }
            catch (Exception)
            {

                
            }
            drpDebit.DataSource = GetDataSource();
            drpDebit.DataBind();
            
        }
        private DataTable GetDataSource()
        {
            //DataTable defined
            var dtSource = new DataTable();
            dtSource.Columns.Add("id", typeof(string));
            dtSource.Columns.Add("FullNames", typeof(string));
            string x = "";
            
            string name = "";
            int num = 0;

            try
            {
                num = Convert.ToInt32(drpCredit.SelectedItem.Value);
            }
            catch (Exception)
            {

                num = 0;
            }
            string pia = "";

            try
            {
                pia = drpCredit.SelectedItem.Value.ToString();
            }
            catch (Exception)
            {

                pia ="0";
            }
            //Fill rows
            if (Debit.SelectedIndex == 0)
            {
                dtSource.Rows.Add("0", "Please Select a group");
                var dd = from c in db.Accounts_Masters
                     where c.AccountNumber!=num
                         select c;
                foreach (var p in dd)
                {
                 
                    dtSource.Rows.Add(p.AccountNumber, p.AccountName);
                }

            }
            else if(Debit.SelectedIndex == 1)
            {
                dtSource.Rows.Add("0", "Please Select a client");
                var dd = from c in db.Account_Creations
                         where c.StatuSActive == true && c.CDSC_Number!=pia
                         select c;
                foreach (var p in dd)
                {
                    if (p.OtherNames == null)
                    {
                        name = p.Surname_CompanyName + " ," + p.CDSC_Number;
                    }
                    else if (p.OtherNames == "")
                    {
                        name = p.Surname_CompanyName + " ," + p.CDSC_Number;
                    }
                    else
                    {
                        name = p.OtherNames + " " + p.Surname_CompanyName + " ," + p.CDSC_Number;
                    }

                    dtSource.Rows.Add(p.CDSC_Number, name);
                }

            }//Fill rows



            return dtSource;
        }

        private DataTable GetDataSource2()
        {
            //DataTable defined
            var dtSource = new DataTable();
            dtSource.Columns.Add("id", typeof(string));
            dtSource.Columns.Add("FullNames", typeof(string));
            string x = "";

            string name = "";
            int num = 0;

            try
            {
                num = Convert.ToInt32(drpDebit.SelectedItem.Value);
            }
            catch (Exception)
            {

                num = 0;
            }
            string pia = "";
            try
            {
                pia = drpDebit.SelectedItem.Value.ToString();
            }
            catch (Exception)
            {

                pia = "0";
            }
            //Fill rows
            if (Credit.SelectedIndex == 0)
            {
                dtSource.Rows.Add("0", "Please Select a group");
                var dd = from c in db.Accounts_Masters
                         where c.AccountNumber!=num
                         select c;
                foreach (var p in dd)
                {

                    dtSource.Rows.Add(p.AccountNumber, p.AccountName);
                }

            }
            else if (Credit.SelectedIndex == 1)
            {
                dtSource.Rows.Add("0", "Please Select a client");
                var dd = from c in db.Account_Creations
                         where c.StatuSActive == true && c.CDSC_Number!=pia
                         select c;
                foreach (var p in dd)
                {
                    if (p.OtherNames == null)
                    {
                        name = p.Surname_CompanyName + " ," + p.CDSC_Number;
                    }
                    else if (p.OtherNames == "")
                    {
                        name = p.Surname_CompanyName + " ," + p.CDSC_Number;
                    }
                    else
                    {
                        name = p.OtherNames + " " + p.Surname_CompanyName + " ," + p.CDSC_Number;
                    }

                    dtSource.Rows.Add(p.CDSC_Number, name);
                }

            }//Fill rows



            return dtSource;
        }

        public void msgbox(string strMessage)
        {
            string strScript = "<script language=JavaScript>";
            strScript += "window.alert(\"" + strMessage + "\");";
            strScript += "</script>";
            System.Web.UI.WebControls.Label lbl = new System.Web.UI.WebControls.Label();
            lbl.Text = strScript;
            Page.Controls.Add(lbl);
        }
        protected void Credit_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string dr = Debit.SelectedValue.ToString();
                string cr = Credit.SelectedValue.ToString();

                if (dr == "Client" && cr == "Client")
                {
                    msgbox("Client transactions are not allowed");
                    return;
                }
            }
            catch (Exception)
            {

             
            }
            drpCredit.DataSource = GetDataSource2();
            drpCredit.DataBind();
        }

        protected void Submit_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(drpDebit.SelectedItem.Text))
            {
                msgbox("An account must be selected for the Debit Account");
                return;
            }
            else if (String.IsNullOrEmpty(drpCredit.SelectedItem.Text))
            {
                msgbox("An account must be selected for the Credit Account");
                return;
            }

            if (drpDebit.SelectedItem.Text== "Please Select a group" || drpDebit.SelectedItem.Text == "Please Select a client")
            {
                msgbox("An account must be selected for the Debit Account");
                return;
            }else if (drpCredit.SelectedItem.Text == "Please Select a group" || drpCredit.SelectedItem.Text == "Please Select a client")
            {
                msgbox("An account must be selected for the Credit Account");
                return;
            }
            else if (txtnarr.Text=="")
            {
                msgbox("Reference is required");
                return;
             
            }
            else if (ASPxDateEdit1.Text == "")
            {
                msgbox("Transaction date is required");
                return;

            }


            decimal n;
            bool isNumericT = decimal.TryParse(txtamt.Text, out n);

            if (isNumericT == false)
            {
                msgbox("The amount has to be a figure");
                txtamt.Focus();
                return;
            }

            int maxx = 0;

            try
            {
                maxx = db.Transs.ToList().Max(a => a.TransID);
                maxx += 1;
            }
            catch (Exception)
            {

           
            }

            //submit
            Submit.Enabled = false;
            BrokerOffice.Models.Trans my = new BrokerOffice.Models.Trans();
            //Debit
            my.Account = txtDbAcc.Text;
            my.Category = Debit.SelectedValue.ToString();
            my.Credit = 0;
            my.Debit = Convert.ToDecimal(txtamt.Text);
            my.Narration = txtnarr.Text;
            my.Reference_Number = maxx.ToString();
            my.TrxnDate = Convert.ToDateTime(ASPxDateEdit1.Text);
            my.Post = Convert.ToDateTime(ASPxDateEdit1.Text);
            my.Type ="Journl Entry";
            my.PostedBy = WebSecurity.CurrentUserName;
            db.Transs.Add(my);

            //Update table  
            db.SaveChanges(WebSecurity.CurrentUserName.ToString());
            // Credit//
            my.Account = txtCrAcc.Text;
            my.Category = Credit.SelectedValue.ToString();
            my.Credit = Convert.ToDecimal(txtamt.Text);
            my.Debit = 0;
            my.Narration = txtnarr.Text;
            my.Reference_Number = maxx.ToString();
            my.TrxnDate = Convert.ToDateTime(ASPxDateEdit1.Text);
            my.Post = Convert.ToDateTime(ASPxDateEdit1.Text);
            my.Type = "Journl Entry";
            my.PostedBy = WebSecurity.CurrentUserName;
            db.Transs.Add(my);

            //Update table  
            db.SaveChanges(WebSecurity.CurrentUserName.ToString());

            //Update reduce the batch

            if (Credit.SelectedValue =="Client")
            {
                string nme = "Accounts recievables";
                var acc = db.Accounts_Masters.ToList().Where(a => a.AccountName.ToLower().Replace(" ","").Contains(nme.ToLower().Replace(" ", "")));

            int num = 0;
            foreach (var c in acc)
            {
                num = c.AccountNumber;
            }


            //submit
            BrokerOffice.Models.Trans my2 = new BrokerOffice.Models.Trans();
            //Debit
            my2.Account = num.ToString();
            my2.Category = "Journl Entry";
            my2.Credit = Convert.ToDecimal(txtamt.Text);
                my2.Debit = 0;
            my2.Narration = txtnarr.Text;
            my2.Reference_Number = maxx.ToString();
                my2.TrxnDate = Convert.ToDateTime(ASPxDateEdit1.Text);
                my2.Post = Convert.ToDateTime(ASPxDateEdit1.Text);
                my2.Type = "Journl Entry";
                my2.PostedBy = WebSecurity.CurrentUserName;
                db.Transs.Add(my2);

            //Update table  
            db.SaveChanges(WebSecurity.CurrentUserName.ToString());

        }else if (Debit.SelectedValue == "Client")
            {
                string nme = "Accounts payables";
              
                var acc = db.Accounts_Masters.ToList().Where(a => a.AccountName.ToLower().Replace(" ", "").Contains(nme.ToLower().Replace(" ", "")));

                int num = 0;
                foreach (var c in acc)
                {
                    num = c.AccountNumber;
                }


                //submit
                BrokerOffice.Models.Trans my2 = new BrokerOffice.Models.Trans();
                //Debit
                my2.Account = num.ToString();
                my2.Category = "Journl Entry";
                my2.Credit = 0;
                my2.Debit = Convert.ToDecimal(txtamt.Text);
                my2.Narration = txtnarr.Text;
                my2.Reference_Number=maxx.ToString();
                my2.TrxnDate = Convert.ToDateTime(ASPxDateEdit1.Text);
                my2.Post = Convert.ToDateTime(ASPxDateEdit1.Text);
                my2.Type = "Journl Entry";

                db.Transs.Add(my2);

                //Update table  
                db.SaveChanges(WebSecurity.CurrentUserName.ToString());
            }
           Clear();
        }
        protected void Clear()
        {
            System.Web.HttpContext.Current.Session["NOT"] = "You have successfully added the transaction";
           
            Response.Redirect("~/TransRecords/Index");
        }

        protected void dxAction_Click(object sender, EventArgs e)
        {
            Label44.Text = "Action";
            TabName.Value = Label44.Text;
            return;
        }

        protected void dxPersonal_Click(object sender, EventArgs e)
        {
            Label44.Text = "personal";
            TabName.Value = Label44.Text;
            return;
        }

        protected void drpDebit_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtDbAcc.Text = drpDebit.SelectedItem.Value.ToString();
        }
    }
}