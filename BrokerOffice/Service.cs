﻿using BrokerOffice.DAO;
using BrokerOffice.DealNotes;
using BrokerOffice.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Sockets;
using System.Threading.Tasks;
using System.Web;
using WebMatrix.WebData;

namespace BrokerOffice
{
    public class Service
    {
        public Service()
        {

        }
        public int mynum = 0;
        public int mynum2 = 0;
        private SBoardContext db = new SBoardContext();

       public int trades()
        {


            try
            {

                List<Tbl_MatchedDeals> dataList = JsonConvert.DeserializeObject<List<Tbl_MatchedDeals>>(SendSMS());


                //   GridView1.DataSource = DerializeDataTable(responJsonText);
                var dbsel = from s in dataList
                            join v in db.Account_Creations on s.Buyer equals v.CDSC_Number
                            let Deal = s.Deal
                            let BuyCompany = s.BuyCompany
                            let SellCompany = s.SellCompany
                            let Buyer = s.Buyer
                            let Seller = s.Seller
                            let Quantity = s.Quantity
                            let Trade = s.Trade
                            let DealPrice = s.DealPrice
                            select new { Deal, BuyCompany, SellCompany, Buyer, Seller, Quantity, Trade, DealPrice };
                var dbsel2 = from s in dataList
                             join v in db.Account_Creations on s.Seller equals v.CDSC_Number

                             let Deal = s.Deal
                             let BuyCompany = s.BuyCompany
                             let SellCompany = s.SellCompany
                             let Buyer = s.Buyer
                             let Seller = s.Seller
                             let Quantity = s.Quantity
                             let Trade = s.Trade
                             let DealPrice = s.DealPrice
                             select new { Deal, BuyCompany, SellCompany, Buyer, Seller, Quantity, Trade, DealPrice };

                //mynum= dbsel.Concat(dbsel2).ToList().Count();

                var xy = dbsel.Concat(dbsel2).ToList().Distinct();
                int counter = 0;

                foreach (var c in xy)
                {
                    counter++;
                }


                mynum = counter;

                return counter;
            }
            catch (Exception)
            {

                return 0;
            }
           // tradesnum(counter);
        }

        public int tradesnum()
        {
            //this.mynum = x;

            return this.mynum;
        }
        public int settledtradesnum()
        {
            //

            return this.mynum2;
        }
        public string SendSMS()
        {
            string SmsStatusMsg = string.Empty;
           
                //Sending SMS To User
                string URL = "";
               
                    URL = "http://localhost/BrokerService/api/Deals/Deals";
                var client = new RestClient(URL);
                var request = new RestRequest("", Method.GET);
                IRestResponse response = client.Execute(request);
                SmsStatusMsg = response.Content;

                return SmsStatusMsg;

        }

        public string SendSMS2()
        {
            string SmsStatusMsg = string.Empty;
           
                string URL = "";

                    URL = "http://localhost/BrokerService/api/Deals/SettledDeals";
                var client = new RestClient(URL);
                var request = new RestRequest("", Method.GET);
                IRestResponse response = client.Execute(request);
                SmsStatusMsg = response.Content;
                return SmsStatusMsg;

        }
        public int settledtrades()
        {

            string nme = "";
            var users = db.Users.ToList().Where(a => a.Email == WebSecurity.CurrentUserName);

            foreach (var dd in users)
            {
                nme = dd.BrokerCode;
            }

            try
            {
                List<DealClass.Dealer> dataList = JsonConvert.DeserializeObject<List<DealClass.Dealer>>(SendSMS2());


                //   GridView1.DataSource = DerializeDataTable(responJsonText);
                var dbsel = from s in dataList
                            join v in db.Account_Creations on s.Account1 equals v.CDSC_Number
                            let Security = s.Security
                            let Deal = s.Deal
                            let Quantity = s.Quantity
                            let Price = s.Price
                            let BuyerClient = s.Account1
                            let SellerClient = s.Account2
                            let DatePosted = s.DatePosted
                            let SettlementDate = s.SettlementDate
                            where s.Ack == "SETTLED" && v.Broker == nme

                            select s;
                var dbsel2 = from s in dataList
                             join v in db.Account_Creations on s.Account2 equals v.CDSC_Number
                             let Security = s.Security
                             let Deal = s.Deal
                             let Quantity = s.Quantity
                             let Price = s.Price
                             let BuyerClient = s.Account1
                             let SellerClient = s.Account2
                             let DatePosted = s.DatePosted
                             let SettlementDate = s.SettlementDate
                             where s.Ack == "SETTLED" && v.Broker ==nme

                             select s;

                var xz = dbsel.Concat(dbsel2).ToList();
                int counter = 0;

                foreach (var c in xz)
                {
                    counter++;
                }

                return counter;

            }
            catch (Exception)
            {

                return 0;
            }
        }
                
        public void accountsupdate()
        {
            var c = db.Users.ToList().Where(a => a.Email == WebSecurity.CurrentUserName);
            string vx = "";
            foreach (var d in c)
            {
                vx = d.BrokerCode;
            }
            var client = new RestClient("http://localhost/BrokerService");
            var request = new RestRequest("denied/{s}", Method.GET);
            request.AddUrlSegment("s", vx);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            List<Accounts_Audit> dataList = JsonConvert.DeserializeObject<List<Accounts_Audit>>(validate);


            var pers = from s in dataList
                       select s;
            string zee = "";
            foreach (var d in pers)
            {
                reoveaccount(d.CDS_Number);

            }
           // updatebilling();
           // trades2();
        }
       
        public void reoveaccount(string s)
        {
            int my = 0;
            var c = db.Account_Creations.ToList().Where(a => a.CDSC_Number == s);
            foreach (var p in c)
            {
                my = p.ID_;
            }

            try
            {
                Account_Creation account_Creation = db.Account_Creations.Find(my);
                account_Creation.StatuSActive = true;
                db.Account_Creations.AddOrUpdate(account_Creation);
                db.SaveChangesAsync();
            }
            catch (Exception)
            {


            }
        }
                 
        

    }

}

