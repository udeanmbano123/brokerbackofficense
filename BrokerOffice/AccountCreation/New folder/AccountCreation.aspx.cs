﻿using BrokerOffice.DAO;
using BrokerOffice.Models;
using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.ServiceModel;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebMatrix.WebData;
using Database = WebMatrix.Data.Database;
namespace BrokerOffice.AccountCreation
{
    public partial class AccountCreation : System.Web.UI.Page
    {

        public string id = "";

        private SBoardContext db = new SBoardContext();
        protected void Page_Load(object sender, EventArgs e)
        {


            id = Request.QueryString["id"];

            loadMemberGrid();
            if (this.IsPostBack == true)
            {

                TabName.Value = Request.Form[TabName.UniqueID];

            }
            else if (IsPostBack == false) {
                System.Web.HttpContext.Current.Session["Tab"] = "";
                ClearMember();
                LoadBank();
                LoadID();
                loadClientTypes();
                LoadCurrencies();
                LoadCountry();
                Label19.Visible = false;
                txtReg.Visible = false;
                Label20.Visible = false;
                txtID.Visible = false;
                Label34.Visible = false;
                Label22.Visible = false;
                cmbBank.Visible = false;
                Label23.Visible = false;
                cmbBranch.Visible = false;
                Label24.Visible = false;
                txtAcc.Visible = false;

                Label11.Visible = false;
                idPay.Visible = false;
                lblMobi.Visible = false;
                mobilewallet.Visible = false;
                Label46.Visible = false;
                Label47.Visible = false;
                Label48.Visible = false;
                Label49.Visible = false;
                if (id == " " || id == null)
                {

                }
                else
                {
                    getDetails(id);
                }
                //ASPxComboBox1.DataSource = GetDataSource();
                //ASPxComboBox1.DataBind();
            }


        }
        private DataTable GetDataSource()
        {
            //DataTable defined
            var dtSource = new DataTable();
            dtSource.Columns.Add("ID", typeof(string));
            dtSource.Columns.Add("FullNames", typeof(string));
            var dd = db.Account_Creations.ToList();
            foreach (var fg in dd)
            {
                dtSource.Rows.Add(fg.CDSC_Number, fg.Surname_CompanyName + fg.OtherNames);

            }

            return dtSource;
        }


        protected void getDetails(string x)
        {

            cmbBank.Items.Clear();
            cmbBank.DataSource = null;
            cmbBranch.Items.Clear();
            cmbBranch.DataSource = null;
            if (x != "")
            {
                btnSubmit.Text = "Update";
                txtFkey.Text = x;

                var acc = db.Account_Creations.ToList().Where(a => a.ID_ == Convert.ToInt32(x));

                foreach (var my in acc)
                {

                    drpTitle.SelectedValue = my.Title;
                    drpTitle.SelectedItem.Text = my.Title;
                    txtFirstName.Text = my.OtherNames;
                    txtMiddle.Text = my.Middlename;
                    txtSurname.Text = my.Surname_CompanyName;
                    txtResident.Items.Insert(0, new ListItem(my.Resident, my.Resident));


                    txtDOB.Date = Convert.ToDateTime(my.DateofBirth_Incorporation);

                    RadioButtonList1.SelectedValue = my.Gender;

                    //contact
                    addr1.Text = my.Address1;
                    addr2.Text = my.Address2;
                    addr3.Text = my.Address3;


                    //ident
                    //my.ClientSuffix = drpSuffix.SelectedValue.ToString();
                    txtReg.Text = my.RNum;
                    txtID.Text = my.Identification;
                    drpTax.Enabled = false;
                    txtCDS.Text = my.CDSC_Number;
                    txtCompanyName.Text = my.Surname_CompanyName;
                    //banking
                    Nationality.Items.Insert(0, new ListItem(my.Nationality.ToString(), my.Nationality.ToString()));


                    drpTax.Text = my.TaxBracket.ToString();
                    cmbBank.Items.Insert(0, new ListItem(my.Bankname, my.Bank));
                    txtTown.Text = my.Town;
                    txtTel.Text = my.TelephoneNumber;
                    txtEmail.Text = my.Emailaddress;
                    cmbBranch.Items.Insert(0, new ListItem(my.BranchName, my.Branch));
                    txtAcc.Text = my.Accountnumber;
                    drpID.Items.Insert(0, new ListItem(my.idtype, my.idtype));
                    RadioButtonList2.SelectedValue = my.accountcategory;
                    RadioButtonList4.SelectedValue = my.MNO_;
                    RadioButtonList3.SelectedValue = my.manBank;

                    drpCountry.Items.Insert(0, new ListItem(my.country, my.country));

                    dep.Items.Insert(0, new ListItem(my.depname, my.depcode));
                    // my.Date_Created = Convert.ToDateTime(txtDOB.Text);
                    txtMobile.Text = my.MobileNumber;
                    idPay.SelectedValue = my.mobile_money;
                    mobilewallet.Text = my.mobilewalletnumber;
                    txtResident.Items.Insert(0, new ListItem(my.ClientSuffix, my.ClientSuffix));

                    if (my.mobile_money == "ONE WALLET")
                    {
                        lblMobi.Visible = true;
                        mobilewallet.Visible = true;
                    }
                    else
                    {
                        lblMobi.Visible = false;
                        mobilewallet.Visible = false;
                    }
                }
                if (RadioButtonList2.SelectedIndex == 1)
                {
                    Label19.Visible = true;
                    txtReg.Visible = true;
                    Label20.Visible = false;
                    txtID.Visible = false;
                    Label65.Visible = true;
                    txtCompanyName.Visible = true;
                    //greying of other fields
                    drpTitle.Visible = false;
                    txtFirstName.Visible = false;
                    txtMiddle.Visible = false;
                    txtSurname.Visible = false;
                    Label52.Visible = false;
                    Label26.Visible = false;
                    Label27.Visible = false;
                    Label28.Visible = false;
                    Label33.Visible = false;
                    Label1.Visible = false;
                    Label2.Visible = false;
                    Label4.Visible = false;
                    Label3.Visible = false;
                    Label6.Visible = false;
                    Label45.Visible = true;

                    RadioButtonList1.Visible = false;

                    Label49.Visible = false;
                    Label17.Visible = false;
                    txtDOB.Visible = false;
                }
                else if (RadioButtonList2.SelectedIndex == 0)
                {
                    Label19.Visible = false;
                    txtReg.Visible = false;
                    Label20.Visible = true;
                    txtID.Visible = true;
                    Label65.Visible = false;
                    txtCompanyName.Visible = false;
                    //greying of other fields
                    drpTitle.Visible = true;
                    txtFirstName.Visible = true;
                    txtMiddle.Visible = true;
                    txtSurname.Visible = true;
                    Label52.Visible = true;
                    Label26.Visible = true;
                    Label27.Visible = true;
                    Label28.Visible = true;
                    Label33.Visible = true;
                    Label1.Visible = true;
                    Label2.Visible = true;
                    Label4.Visible = true;
                    Label3.Visible = true;
                    Label6.Visible = true;
                    RadioButtonList1.Visible = true;
                    Label49.Visible = true;
                    Label45.Visible = false;
                    Label17.Visible = false;
                    txtDOB.Visible = false;
                }
                else if (RadioButtonList2.SelectedIndex == 2)
                {
                    Label19.Visible = true;
                    txtReg.Visible = true;
                    Label20.Visible = false;
                    txtID.Visible = false;
                    Label65.Visible = true;
                    txtCompanyName.Visible = true;
                    //greying of other fields
                    drpTitle.Visible = false;
                    txtFirstName.Visible = false;
                    txtMiddle.Visible = false;
                    txtSurname.Visible = false;
                    Label52.Visible = false;
                    Label26.Visible = false;
                    Label27.Visible = false;
                    Label28.Visible = false;
                    Label33.Visible = false;
                    Label1.Visible = false;
                    Label2.Visible = false;
                    Label4.Visible = false;
                    Label3.Visible = false;
                    Label6.Visible = false;
                    Label45.Visible = true;

                    RadioButtonList1.Visible = false;
                    Label49.Visible = false;
                    Label17.Visible = true;
                    txtDOB.Visible = true;
                }
                if (RadioButtonList4.SelectedIndex == 1)
                {
                    Label80.Visible = true;
                    txtCDS.Visible = true;
                }
                else if (RadioButtonList4.SelectedIndex == 0)
                {
                    Label80.Visible = false;
                    txtCDS.Visible = false;
                }
                if (RadioButtonList3.SelectedIndex == 0)
                {
                    Label34.Visible = true;
                    Label22.Visible = true;
                    cmbBank.Visible = true;
                    Label23.Visible = true;
                    cmbBranch.Visible = true;
                    Label24.Visible = true;
                    txtAcc.Visible = true;

                    Label11.Visible = false;
                    idPay.Visible = false;
                    lblMobi.Visible = false;
                    mobilewallet.Visible = false;

                    //
                    Label46.Visible = true;
                    Label47.Visible = true;
                    Label48.Visible = true;

                }
                else if (RadioButtonList3.SelectedIndex == 1)
                {
                    Label34.Visible = false;
                    Label22.Visible = false;
                    cmbBank.Visible = false;
                    Label23.Visible = false;
                    cmbBranch.Visible = false;
                    Label24.Visible = false;
                    txtAcc.Visible = false;

                    Label11.Visible = true;
                    idPay.Visible = true;
                    lblMobi.Visible = true;
                    mobilewallet.Visible = true;
                    Label46.Visible = false;
                    Label47.Visible = false;
                    Label48.Visible = false;
                }
                LoadBank2();
                LoadID2();
                loadClientTypes2();
                LoadCurrencies2();
                LoadCountry2();
            }

            return;
        }
        protected void LoadBank2()
        {

            var ds = from c in db.Banks
                     select new

                     {

                         Bankcode = c.bank,

                         Bankname = c.bank_name,

                     };
            cmbBank.DataSource = ds.ToList();
            cmbBank.DataTextField = "Bankname";
            // text field name of table dispalyed in dropdown
            cmbBank.DataValueField = "Bankcode"; // to retrive specific  textfield name 
                                                 //assigning datasource to the dropdownlist
            cmbBank.DataBind(); //binding dropdownlist
                                // allocate.Items.Insert(0, "Select");


            // allocate.Items.Insert(0, "Select");


            var ds3 = from c in db.TradingPlatforms
                      select new

                      {

                          Bankcode = c.Code,

                          Bankname = c.Name,

                      };
            dep.DataSource = ds3.ToList();
            dep.DataTextField = "Bankname";
            // text field name of table dispalyed in dropdown
            dep.DataValueField = "Bankcode"; // to retrive specific  textfield name 
                                             //assigning datasource to the dropdownlist
            dep.DataBind(); //binding dropdownlist
                            // allocate.Items.Insert(0, "Select");

        }
        protected void LoadBank()
        {
            cmbBank.Items.Clear();
            cmbBank.DataSource = null;
            var ds = from c in db.Banks
                     select new

                     {

                         Bankcode = c.bank,

                         Bankname = c.bank_name,

                     };
            cmbBank.DataSource = ds.ToList();
            cmbBank.DataTextField = "Bankname";
            // text field name of table dispalyed in dropdown
            cmbBank.DataValueField = "Bankcode"; // to retrive specific  textfield name 
                                                 //assigning datasource to the dropdownlist
            cmbBank.DataBind(); //binding dropdownlist
                                // allocate.Items.Insert(0, "Select");
            cmbBank.Items.Insert(0, new ListItem("Please Select a bank", "0"));


            dep.Items.Clear();
            dep.DataSource = null;
            var ds3 = from c in db.TradingPlatforms
                      select new

                      {

                          Bankcode = c.Code,

                          Bankname = c.Name,

                      };
            dep.DataSource = ds3.ToList();
            dep.DataTextField = "Bankname";
            // text field name of table dispalyed in dropdown
            dep.DataValueField = "Bankcode"; // to retrive specific  textfield name 
                                             //assigning datasource to the dropdownlist
            dep.DataBind(); //binding dropdownlist
                            // allocate.Items.Insert(0, "Select");
            dep.Items.Insert(0, new ListItem("Please Select a Depository", "0"));
        }

        protected void txtID_TextChanged(object sender, EventArgs e)
        {


            int my = db.Account_Creations.ToList().Where(a => a.Identification == txtID.Text).Count();

            if (my > 0)
            {
                msgbox("ID Number exists");
                txtID.Text = "";
            }
            txtID.Focus();
            return;
        }
        public void msgbox(string strMessage)
        {
            string strScript = "<script language=JavaScript>";
            strScript += "window.alert(\"" + strMessage + "\");";
            strScript += "</script>";
            System.Web.UI.WebControls.Label lbl = new System.Web.UI.WebControls.Label();
            lbl.Text = strScript;
            Page.Controls.Add(lbl);
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {

            if (RadioButtonList4.SelectedIndex < 0)
            {

                msgbox("NEW or EXISTING account must be selected");
                return;
            }
            if (RadioButtonList4.SelectedIndex == 1 && txtCDS.Text == "")
            {

                msgbox("ATP/CDS is required");
                return;
            }
            if (RadioButtonList2.SelectedIndex == 1 )
            {
                txtSurname.Text = txtCompanyName.Text;
            }
            if (RadioButtonList2.SelectedIndex == 2)
            {
                txtSurname.Text = txtCompanyName.Text;
            }

            if (RadioButtonList2.SelectedIndex == 2 && txtCompanyName.Text == "")
            {
                msgbox("Corporate name is required !");
                return;
            }
            if (RadioButtonList2.SelectedIndex == 1 && txtCompanyName.Text == "")
            {
                msgbox("Joint name is required !");
                return;
            }
            if (RadioButtonList2.SelectedIndex == 0 && txtFirstName.Text == "")
            {
                msgbox("First name is required !");
                return;
            } else if (RadioButtonList2.SelectedIndex == 0 && txtSurname.Text == "")
            {
                msgbox("Surname is required !");
                return;
            }
            else if (txtDOB.Text == "" && RadioButtonList2.SelectedIndex == 0)
            {
                msgbox("Date of birth is required !");
                return;
            }
            else if (Nationality.SelectedItem.Text == "Please Select a nationality")
            {
                msgbox("Nationality is required !");
                return;
            }
            else if (addr1.Text == "")
            {
                msgbox("Address is required !");
                return;
            }
            else if (txtTown.Text == "")
            {
                msgbox("Town is required !");
                return;
            }
            else if (txtEmail.Text == "")
            {
                msgbox("Email is required !");
                return;
            }
            else if (txtMobile.Text == "")
            {
                msgbox("Mobile is required !");
                return;
            }
            else if (drpCountry.SelectedItem.Text == "Please Select a country")
            {
                msgbox("Country is required !");
                return;
            }
            else if (RadioButtonList1.SelectedIndex < 0 && RadioButtonList2.SelectedIndex == 0)
            {
                msgbox("Gender is required !");
                return;
            }
            else if (RadioButtonList2.SelectedIndex < 0)
            {
                msgbox("Individual,Corporate and Joint is required !");
                return;
            }

            else if (cmbBank.SelectedItem.Text == "Please Select a bank")
            {
                msgbox("Select a Bank is required !");
                return;
            }
           
            else if (drpID.SelectedItem.Text == "Please Select an ID type")

            {
                msgbox("Select an id type !");
                return;
            }
            else if (drpCountry.SelectedItem.Text == "Please Select a country")
            {
                msgbox("Country is required !");
                return;
            }

            else if (txtTown.Text == "")
            {
                msgbox("Select town!");
                return;
            }
            else if (txtMobile.Text == "")
            {
                msgbox("Please enter mobile number!");
                return;
            }
            else if (txtAcc.Text == "")
            {
                msgbox("Account number is required!");
                return;
            }
            if (txtResident.SelectedItem.Text == "Please select a resident type")
            {
                msgbox("Resident type is required!");
                return;
            }
            if (RadioButtonList2.SelectedIndex == 1 && txtReg.Text == "")
            {
                msgbox("Corporate registration number is required!");
                return;
            }

            if (RadioButtonList2.SelectedIndex == 2 && txtReg.Text == "")
            {
                msgbox("Joint registration number is required!");
                return;
            }

            if (RadioButtonList2.SelectedIndex == 0 && txtID.Text == "")
            {
                msgbox("ID Number is required!");
                return;
            }



            if (RadioButtonList2.SelectedIndex == 0 && txtReg.Text != "")
            {
                txtID.Text = "0";
            }

            if (RadioButtonList2.SelectedIndex == 1 && txtID.Text != "")
            {
                txtReg.Text = "0";
            }



            if (btnSubmit.Text == "Submit")
            {
                Account_CreationPending my = new Account_CreationPending();
                //generate CDSNUMBEr
                var con2 = Database.Open("SBoardConnection");
                var audit = "select max(ID_) as ID from Account_Creation";
                var list13 = con2.Query(audit).ToList();
                int code25 = 0;
                try
                {
                    foreach (var row in list13)
                    {
                        code25 = row.ID;
                    }

                }

                catch (Exception f)
                {
                    code25 = 0;
                }

                int size = 5;


                var agent = db.Users.ToList().Where(a => a.Email == WebSecurity.CurrentUserName);
                string code22 = "";
                foreach (var row in agent)
                {
                    code22 = row.BrokerCode;
                }
                string concode25 = Convert.ToString((code25 + 1));
                string cdsNo = concode25.PadLeft(10, '0') + code22 + concode25;
                my.CDSC_Number = cdsNo;
                my.Title = drpTitle.SelectedValue.ToString();
                my.OtherNames = txtFirstName.Text;

                my.Middlename = txtMiddle.Text;
                my.Surname_CompanyName = txtSurname.Text;
                my.Nationality = Nationality.SelectedItem.Text;
                my.Resident = txtResident.SelectedItem.Text;

                try
                {
                    my.DateofBirth_Incorporation = Convert.ToDateTime(txtDOB.Text);

                }
                catch (Exception)
                {

                    my.DateofBirth_Incorporation = DateTime.Now;
                }
                if (my.DateofBirth_Incorporation == null)
                {
                    my.DateofBirth_Incorporation = DateTime.Now;
                }
                my.Gender = RadioButtonList1.SelectedValue.ToString();
                my.Initials = "N";
                //contact
                my.Address1 = addr1.Text;
                my.Address2 = addr2.Text;
                my.Address3 = addr3.Text;
                my.Town = txtTown.Text;
                my.PostCode = "N";
                my.FaxNumber = "N";
                my.TelephoneNumber = txtTel.Text;
                my.Emailaddress = txtEmail.Text;
                //ident
                my.ClientSuffix = txtResident.SelectedValue.ToString();
                my.RNum = txtReg.Text;
                my.Identification = txtID.Text;

                if (my.Identification == "")
                {
                    my.Identification = txtReg.Text;
                }
                if (my.OtherNames == "")
                {
                    my.OtherNames = txtSurname.Text;
                }
                if (my.Gender == "")
                {
                    my.Gender = "M";
                }
                //banking
                try
                {
                    my.TaxBracket = Convert.ToDecimal(drpTax.Text);
                }
                catch (Exception)
                {

                    my.TaxBracket = 0;
                }
            
            try
            {
                my.Bank = cmbBank.SelectedValue.ToString();
            }
            catch (Exception)
            {

                my.Bank = "Not selected";
            }

            try
            {
                my.Branch = cmbBranch.SelectedValue.ToString();
            }
            catch (Exception)
            {

                my.Branch = "Not selected";
            }
                try
                {
                    my.Bankname = cmbBank.SelectedItem.Text;
                }
                catch (Exception)
                {

                    my.Bankname = "N/A";
                }
                try
                {
                    my.BranchName = cmbBranch.SelectedItem.Text;
                }
                catch (Exception)
                {

                    my.BranchName = "N/A";
                }
                my.Accountnumber = txtAcc.Text;
                my.idtype = drpID.SelectedItem.Text;
                my.accountcategory = RadioButtonList2.SelectedValue.ToString();
                my.country = drpCountry.SelectedValue.ToString();
                my.currency = "USD";
                my.divpayee = "N";
                my.divbank = "N";
                my.divbankcode = "N";
                my.divbranch = "N";
                my.divbranchcode = "N";
                my.divacc = "N";
                my.divaccounttype = "N";
                my.dividnumber = "N";

                my.idtype2 = "0";

                my.Date_Created = DateTime.Now;
                my.MobileNumber = txtMobile.Text;
                my.depname = dep.SelectedItem.Text;
                my.depcode = dep.SelectedValue.ToString();
                my.manAccount = "N/A";
                my.manBank = RadioButtonList3.SelectedItem.Value;
                my.manBankCode = "N/A";
                my.manBranch = "N/A";
                my.manBranchCode = "N/A";
                my.manNames = "N/A";
                my.manAddress = "N/A";
                my.mobilewalletnumber = mobilewallet.Text;
                my.Accountnumber = txtAcc.Text;
                //Insert Blank Row in Table  
                my.CreatedBy = WebSecurity.CurrentUserName.ToString();
                my.Broker = code22;
                my.CDSC_Number = "PENDING";
                my.update_type = "INSERT";
              

                if (my.mobile_money == "")
                {
                    my.mobile_money = "0";
                }

                if (RadioButtonList4.SelectedIndex == 1 && txtCDS.Text != "")
                {
                    my.CDSC_Number = txtCDS.Text;
                }

                my.MNO_ = RadioButtonList4.SelectedItem.Value;
                if (my.DateofBirth_Incorporation == null)
                {
                    my.DateofBirth_Incorporation = DateTime.Now;
                }

                if (my.Identification == "")
                {
                    my.Identification = my.RNum;
                }
                //finaaly save
                my.mobilewalletnumber = mobilewallet.Text;
                try
                {
                    my.mobile_money = idPay.SelectedItem.Value;
                }
                catch (Exception)
                {

                    my.mobile_money = "NONE";
                }
                db.Account_CreationPendingss.Add(my);

                //Update table 
                db.SaveChanges(WebSecurity.CurrentUserName.ToString());

                int max = db.Account_CreationPendingss.ToList().Max(a => a.ID_);
                UpdateMember(max.ToString());

                //db.Database.ExecuteSqlCommand("Exec NewAccounts");
                System.Web.HttpContext.Current.Session["NOT"] = "You have successfully added the client waiting for authorizaion";
                Response.Redirect("~/Account_Creation/Index");


            }
            else if (btnSubmit.Text == "Update")
            {
                var p = db.Account_Creations.ToList().Where(a => a.ID_ == Convert.ToInt32(txtFkey.Text));

                string cc = "";
                foreach (var d in p)
                {
                    cc = d.CDSC_Number;
                }

                var ps = db.Account_CreationPendingss.ToList().Where(a => a.CDSC_Number == cc);

                int f = 0;

                foreach (var q in ps)
                {
                    f = q.ID_;
                }
                var my = db.Account_CreationPendingss.Find(f);
                my.Title = drpTitle.SelectedValue.ToString();
                my.OtherNames = txtFirstName.Text;
                my.Middlename = txtMiddle.Text;
                my.Surname_CompanyName = txtSurname.Text;
                my.Nationality = Nationality.SelectedItem.Text;
                my.Resident = txtResident.SelectedItem.Text;
                try
                {
                    my.DateofBirth_Incorporation = Convert.ToDateTime(txtDOB.Text);

                }
                catch (Exception)
                {

                    my.DateofBirth_Incorporation = DateTime.Now;
                }
                if (my.DateofBirth_Incorporation == null)
                {
                    my.DateofBirth_Incorporation = DateTime.Now;
                }
                my.Gender = RadioButtonList1.SelectedValue.ToString();
                my.Initials = "N";
                //contact
                my.Address1 = addr1.Text;
                my.Address2 = addr2.Text;
                my.Address3 = addr3.Text;


                //ident
                my.ClientSuffix = txtResident.SelectedValue.ToString();
                my.RNum = txtReg.Text;
                my.Identification = txtID.Text;
                if (my.Identification == "")
                {
                    my.Identification = txtReg.Text;
                }
                if (my.OtherNames == "")
                {
                    my.OtherNames = txtSurname.Text;
                }
                if (my.Gender == "")
                {
                    my.Gender = "M";
                }
                //banking
                try
                {
                    my.TaxBracket = Convert.ToDecimal(drpTax.Text);
                }
                catch (Exception)
                {

                    my.TaxBracket = 0;
                }
                try
                {
                    my.Bank = cmbBank.SelectedValue.ToString();
                }
                catch (Exception)
                {

                    my.Bank = "Not selected";
                }
                try
                {
                    my.Branch = cmbBranch.SelectedValue.ToString();
                }
                catch (Exception)
                {

                    my.Branch = "Not selected";
                }
                try
                {
                    my.Bankname = cmbBank.SelectedItem.Text;
                }
                catch (Exception)
                {

                    my.Bankname = "N/A";
                }
                try
                {
                    my.BranchName = cmbBranch.SelectedItem.Text;
                }
                catch (Exception)
                {

                    my.BranchName = "N/A";
                }
                my.Accountnumber = txtAcc.Text;
                my.idtype = drpID.SelectedItem.Text;
                my.accountcategory = RadioButtonList2.SelectedValue.ToString();
                my.country = drpCountry.SelectedValue.ToString();
                my.currency = "USD";
                my.divpayee = "N";
                my.divbank = "N";
                my.divbankcode = "N";
                my.divbranch = "N";
                my.divbranchcode = "N";
                my.divacc = "N";
                my.divaccounttype = "N";
                my.Town = txtTown.Text;
                my.PostCode = "N";
                my.FaxNumber = "N";
                my.TelephoneNumber = txtTel.Text;
                my.Emailaddress = txtEmail.Text;
                my.MobileNumber = txtMobile.Text;
                //Update Row in Table  
                my.dividnumber = "N";
                try
                {
                    my.mobile_money = idPay.SelectedItem.Value;
                }
                catch (Exception)
                {

                    my.mobile_money = "NONE";
                }
                my.idtype2 = "0";
                my.depname = dep.SelectedItem.Text;
                my.depcode = dep.SelectedValue.ToString();
                my.update_type = "TOUPDATE";
                my.ModifiedBy = WebSecurity.CurrentUserName.ToString();
                my.Accountnumber = txtAcc.Text;
                if (RadioButtonList4.SelectedIndex == 1 && txtCDS.Text != "")
                {
                    my.CDSC_Number = txtCDS.Text;
                }

                my.MNO_ = RadioButtonList4.SelectedItem.Value;
                my.manAccount = "N/A";
                my.manBank = RadioButtonList3.SelectedItem.Value;
                my.manBankCode = "N/A";
                my.manBranch = "N/A";
                my.manBranchCode = "N/A";
                my.manNames = "N/A";
                my.manAddress = "N/A";
                if (my.Identification == "")
                {
                    my.Identification = my.RNum;
                }
                my.mobilewalletnumber = mobilewallet.Text;
                try
                {
                    my.mobile_money = idPay.SelectedItem.Value;
                }
                catch (Exception)
                {

                    my.mobile_money = "NONE";
                }
                db.Account_CreationPendingss.AddOrUpdate(my);
                db.SaveChanges(WebSecurity.CurrentUserName.ToString());

                UpdateMember(my.CDSC_Number);

                System.Web.HttpContext.Current.Session["NOT"] = my.CDSC_Number + "You have successfully updated the client waiting for authorization";
                Response.Redirect("~/Account_Creation/Index");
                string strscript = "<script>alert('You have successfully updated the client');window.location.href='../Account_Creation/Index'</script>";

                //if (!ClientScript.IsClientScriptBlockRegistered("clientscript"))
                //{
                //    ClientScript.RegisterStartupScript(this.GetType(), "clientscript", strscript);
                //}

            }

        }

        protected void Clear()
        {
            Response.Redirect("~/Account_Creation/Index");
        }
        public string CreateRandomPassword(int size)
        {
            string allowedChars = "";
            allowedChars += "1,2,3,4,5,6,7,8,9,0";
            char[] sep = { ',' };
            string[] arr = allowedChars.Split(sep);
            string passwordString = "";
            string password = "";
            string temp = "";
            Random rand = new Random();
            for (int i = 0; i < size; i++)
            {
                temp = arr[rand.Next(0, arr.Length)];
                passwordString += temp;
            }
            password = passwordString;
            return password;
        }
        protected void cmbBank_SelectedIndexChanged(object sender, EventArgs e)
        {


            cmbBranch.Items.Clear();
            cmbBranch.DataSource = null;
            var ds = from c in db.Branches
                     where c.bank == cmbBank.SelectedValue.ToString()
                     select new

                     {

                         branchcode = c.branch,

                         branchname = c.branch_name,

                     };
            cmbBranch.DataSource = ds.ToList();
            cmbBranch.DataTextField = "branchname";
            // text field name of table dispalyed in dropdown
            cmbBranch.DataValueField = "branchcode"; // to retrive specific  textfield name 
            //assigning datasource to the dropdownlist
            cmbBranch.DataBind(); //binding dropdownlist
                                  // allocate.Items.Insert(0, "Select");
            cmbBranch.Items.Insert(0, new ListItem("Please Select a branch", "0"));
        }

        protected void divbank_SelectedIndexChanged(object sender, EventArgs e)
        {
       
    }
        protected void LoadID()
        {
            drpID.Items.Clear();
            drpID.DataSource = null;
            var ds = from c in db.IDTypess
                     select new

                     {

                         Bankcode = c.idname,

                         Bankname = c.idname,

                     };
            drpID.DataSource = ds.ToList();
            drpID.DataTextField = "Bankname";
            // text field name of table dispalyed in dropdown
            drpID.DataValueField = "Bankcode"; // to retrive specific  textfield name 
                                               //assigning datasource to the dropdownlist
            drpID.DataBind(); //binding dropdownlist
                              // allocate.Items.Insert(0, "Select");
            drpID.Items.Insert(0, new ListItem("Please Select an ID type", "0"));


            JIDTYPE.Items.Clear();
            JIDTYPE.DataSource = null;
            var dss = from c in db.IDTypess
                     select new

                     {

                         Bankcode = c.idname,

                         Bankname = c.idname,

                     };
            JIDTYPE.DataSource = dss.ToList();
            JIDTYPE.DataTextField = "Bankname";
            // text field name of table dispalyed in dropdown
            JIDTYPE.DataValueField = "Bankcode"; // to retrive specific  textfield name 
                                                 //assigning datasource to the dropdownlist
            JIDTYPE.DataBind(); //binding dropdownlist
                              // allocate.Items.Insert(0, "Select");
            JIDTYPE.Items.Insert(0, new ListItem("Please Select an ID type", "0"));

        }
        protected void LoadID2()
        {
            drpID.Items.Clear();
            drpID.DataSource = null;
            var ds = from c in db.IDTypess
                     select new

                     {

                         Bankcode = c.idname,

                         Bankname = c.idname,

                     };
            drpID.DataSource = ds.ToList();
            drpID.DataTextField = "Bankname";
            // text field name of table dispalyed in dropdown
            drpID.DataValueField = "Bankcode"; // to retrive specific  textfield name 
                                               //assigning datasource to the dropdownlist
            drpID.DataBind(); //binding dropdownlist
                            // allocate.Items.Insert(0, "Select");

        }

        protected void loadClientTypes2()
        {
           
                               // allocate.Items.Insert(0, "Select");

            txtResident.Items.Clear();
            txtResident.DataSource = null;
            var dssw = from c in db.Client_Types
                      select new

                      {

                          ID = c.ClientType,

                          ISIN = c.ClientType,

                      };
            txtResident.DataSource = dssw.ToList();
            txtResident.DataTextField = "ISIN";
            // text field name of table dispalyed in dropdown
            txtResident.DataValueField = "ID"; // to retrive specific  textfield name 
                                               //assigning datasource to the dropdownlist
            txtResident.DataBind(); //binding dropdownlist
                                    // allocate.Items.Insert(0, "Select");

          
                                   // allocate.Items.Insert(0, "Select");

          

        }
        protected void loadClientTypes()
        {
          
            txtResident.Items.Clear();
            txtResident.DataSource = null;
            var dssw = from c in db.Client_Types
                     select new

                     {

                         ID = c.ClientType,

                         ISIN = c.ClientType,

                     };
            txtResident.DataSource = dssw.ToList();
            txtResident.DataTextField = "ISIN";
            // text field name of table dispalyed in dropdown
            txtResident.DataValueField = "ID"; // to retrive specific  textfield name 
                                               //assigning datasource to the dropdownlist
            txtResident.DataBind(); //binding dropdownlist
                                    // allocate.Items.Insert(0, "Select");

            txtResident.Items.Insert(0, new ListItem("Please Select a resident type", "0"));

               }
        protected void LoadCurrencies2()
        {
            JNationality.Items.Clear();
            JNationality.DataSource = null;
            var dssz = from c in db.Countries
                       select new

                       {

                           Code = c.fnam,

                           CurrencyName = c.fnam,

                       };
            JNationality.DataSource = dssz.ToList();
            JNationality.DataTextField = "CurrencyName";
            // text field name of table dispalyed in dropdown
            JNationality.DataValueField = "Code"; // to retrive specific  textfield name 
                                                  //assigning datasource to the dropdownlist
            JNationality.DataBind(); //binding dropdownlist
                                     // allocate.Items.Insert(0, "Select");

            JNationality.Items.Insert(0, new ListItem("Please Select a nationality", "0"));




        }
        protected void LoadCurrencies()
        {

            var dssz = from c in db.Countries
                       select new

                       {

                           Code = c.fnam,

                           CurrencyName = c.fnam,

                       };
            JNationality.DataSource = dssz.ToList();
            JNationality.DataTextField = "CurrencyName";
            // text field name of table dispalyed in dropdown
            JNationality.DataValueField = "Code"; // to retrive specific  textfield name 
                                                  //assigning datasource to the dropdownlist
            JNationality.DataBind(); //binding dropdownlist
                                     // allocate.Items.Insert(0, "Select");

        }

        protected void LoadCountry2()
        {
            var ds = from c in db.Countries
                     select new

                     {

                         Code = c.fnam,

                         CurrencyName = c.fnam,

                     };
            drpCountry.DataSource = ds.ToList();
            drpCountry.DataTextField = "CurrencyName";
            // text field name of table dispalyed in dropdown
            drpCountry.DataValueField = "Code"; // to retrive specific  textfield name 
                                                //assigning datasource to the dropdownlist
            drpCountry.DataBind(); //binding dropdownlist
                                   // allocate.Items.Insert(0, "Select");

            var dss = from c in db.Countries
                     select new

                     {

                         Code = c.fnam,

                         CurrencyName = c.fnam,

                     };
            Nationality.DataSource = dss.ToList();
            Nationality.DataTextField = "CurrencyName";
            // text field name of table dispalyed in dropdown
            Nationality.DataValueField = "Code"; // to retrive specific  textfield name 
                                                //assigning datasource to the dropdownlist
            Nationality.DataBind(); //binding dropdownlist
                                   // allocate.Items.Insert(0, "Select");



        }
        protected void LoadCountry()
        {
            drpCountry.Items.Clear();
            drpCountry.DataSource = null;
            var ds = from c in db.Countries
                     select new

                     {

                         Code = c.fnam,

                         CurrencyName = c.fnam,

                     };
            drpCountry.DataSource = ds.ToList();
            drpCountry.DataTextField = "CurrencyName";
            // text field name of table dispalyed in dropdown
            drpCountry.DataValueField = "Code"; // to retrive specific  textfield name 
                                                //assigning datasource to the dropdownlist
            drpCountry.DataBind(); //binding dropdownlist
                                   // allocate.Items.Insert(0, "Select");

            drpCountry.Items.Insert(0, new ListItem("Please Select a country", "0"));


            Nationality.Items.Clear();
            Nationality.DataSource = null;
            var dss = from c in db.Countries
                     select new

                     {

                         Code = c.fnam,

                         CurrencyName = c.fnam,

                     };
            Nationality.DataSource = dss.ToList();
            Nationality.DataTextField = "CurrencyName";
            // text field name of table dispalyed in dropdown
            Nationality.DataValueField = "Code"; // to retrive specific  textfield name 
                                                 //assigning datasource to the dropdownlist
            Nationality.DataBind(); //binding dropdownlist
                                    // allocate.Items.Insert(0, "Select");

            Nationality.Items.Insert(0, new ListItem("Please Select a nationality", "0"));

            JNationality.Items.Clear();
            JNationality.DataSource = null;
            var dssz = from c in db.Countries
                      select new

                      {

                          Code = c.fnam,

                          CurrencyName = c.fnam,

                      };
            JNationality.DataSource = dssz.ToList();
            JNationality.DataTextField = "CurrencyName";
            // text field name of table dispalyed in dropdown
            JNationality.DataValueField = "Code"; // to retrive specific  textfield name 
                                                 //assigning datasource to the dropdownlist
            JNationality.DataBind(); //binding dropdownlist
                                    // allocate.Items.Insert(0, "Select");

            JNationality.Items.Insert(0, new ListItem("Please Select a nationality", "0"));

        }

        protected void RadioButtonList2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (RadioButtonList2.SelectedIndex==1)
            {
                Label19.Visible = true;
                txtReg.Visible = true;
                Label20.Visible = false;
                txtID.Visible = false;
                Label65.Visible =true;
                txtCompanyName.Visible = true;
                //greying of other fields
                drpTitle.Visible = false;
                txtFirstName.Visible =false;
                txtMiddle.Visible = false;
                txtSurname.Visible = false;
                Label52.Visible = false;
                Label26.Visible = false;
                Label27.Visible = false;
                Label28.Visible = false;
                Label33.Visible = false;
                Label1.Visible = false;
                Label2.Visible = false;
                Label4.Visible = false;
                Label3.Visible = false;
                Label6.Visible = false;
                Label45.Visible = true;

                RadioButtonList1.Visible = false;

                Label49.Visible = false;
                Label17.Visible = false;
                txtDOB.Visible = false;
            }
            else if (RadioButtonList2.SelectedIndex == 0)
            {
                Label19.Visible =false;
                txtReg.Visible = false;
                Label20.Visible =true;
                txtID.Visible = true;
                Label65.Visible = false;
                txtCompanyName.Visible = false;
                //greying of other fields
                drpTitle.Visible = true;
                 txtFirstName.Visible = true;
                txtMiddle.Visible = true;
                txtSurname.Visible = true;
                Label52.Visible = true;
                Label26.Visible = true;
                Label27.Visible = true;
                Label28.Visible = true;
                Label33.Visible = true;
                Label1.Visible = true;
                Label2.Visible = true;
                Label4.Visible = true;
                Label3.Visible = true;
                Label6.Visible = true;
                RadioButtonList1.Visible = true;
                Label49.Visible = true;
                Label45.Visible = false;
                Label17.Visible = false;
                txtDOB.Visible = false;
            }
            else if (RadioButtonList2.SelectedIndex == 2)
            {
                Label19.Visible = true;
                txtReg.Visible = true;
                Label20.Visible = false;
                txtID.Visible = false;
                Label65.Visible = true;
                txtCompanyName.Visible = true;
                //greying of other fields
                drpTitle.Visible = false;
                txtFirstName.Visible = false;
                txtMiddle.Visible = false;
                txtSurname.Visible = false;
                Label52.Visible = false;
                Label26.Visible = false;
                Label27.Visible = false;
                Label28.Visible = false;
                Label33.Visible = false;
                Label1.Visible = false;
                Label2.Visible = false;
                Label4.Visible = false;
                Label3.Visible = false;
                Label6.Visible = false;
                Label45.Visible = true;

                RadioButtonList1.Visible = false;
                Label49.Visible = false;
                Label17.Visible = true;
                txtDOB.Visible = true;
            }
        }
        public bool IsAddressAvailable(string address)
        {
            try
            {
                System.Net.WebClient client = new WebClient();
                client.DownloadData(address);
                return true;
            }
            catch
            {
                return false;
            }
        }

     

        protected void upload2_Click(object sender, EventArgs e)
        {
            if (FileUpload.PostedFile.FileName != null)
            {
                Accounts_Documents my = new Accounts_Documents();
                my.Name = FileUpload.PostedFile.FileName;
                my.ContentType = System.IO.Path.GetExtension(FileUpload.PostedFile.FileName);
                my.doc_generated = "";
                Stream fs = FileUpload.PostedFile.InputStream;
                BinaryReader br = new BinaryReader(fs);
                byte[] bytes = br.ReadBytes((Int32)fs.Length);
                my.Data = bytes;
                db.Accounts_Documentss.AddOrUpdate(my);
                db.SaveChanges(WebSecurity.CurrentUserName);
                loadMemberGrid();
            }else
            {
                msgbox("Select the file first for upload");
                Label44.Text = "trading";
                TabName.Value = Label44.Text;
                return;
            }
        }
        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("Local IP Address Not Found!");
        }

        protected void ASPxButton1_Click(object sender, EventArgs e)
        {
            Label44.Text = "contact";
            if (RadioButtonList2.SelectedIndex == 1)
            {
                Label44.Text = "member";
            }
            TabName.Value = Label44.Text;
            return;
        }

        protected void dxPersonal_Click(object sender, EventArgs e)
        {
            Label44.Text = "personal";
            if (RadioButtonList2.SelectedIndex == 1)
            {
                Label44.Text = "identification";
            }
            TabName.Value = Label44.Text;
            return;
        }

        protected void dxIdentiication_Click(object sender, EventArgs e)
        {
            Label44.Text = "identification";
            TabName.Value = Label44.Text;
            return;
        }

        protected void dxContact_Click(object sender, EventArgs e)
        {
            Label44.Text = "contact";
            if (RadioButtonList2.SelectedIndex == 1)
            {
                Label44.Text = "member";
            }
            TabName.Value = Label44.Text;
            return;
        }

        protected void dxBanking_Click(object sender, EventArgs e)
        {
            Label44.Text = "banking";
            TabName.Value = Label44.Text;
            return;
        }

        protected void dxIdentificationBack_Click(object sender, EventArgs e)
        {
            Label44.Text = "identification";
            TabName.Value = Label44.Text;
            return;
        }

        protected void dxTrading_Click(object sender, EventArgs e)
        {
            Label44.Text = "trading";
         
            TabName.Value = Label44.Text;
            return;
        }

        protected void dxBankingBack_Click(object sender, EventArgs e)
        {
            Label44.Text = "banking";
            TabName.Value = Label44.Text;
            return;
        }

        protected void manBank_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        protected void idPay_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }


        //protected void ASPxGridView1_SelectionChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        object key = null;
        //        for (int i = 0; i < ASPxGridView1.VisibleRowCount; i++)
        //        {
        //            if (ASPxGridView1.Selection.IsRowSelected(i))
        //            {
        //                key = ASPxGridView1.GetRowValues(i, "Accounts_DocumentsID");

        //            }

        //        }


        //        ASPxGridView1.Selection.UnselectRowByKey(key);
               

        //    }
        //    catch (Exception)
        //    {


        //    }


        //}

        protected void ASPxGridView1_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
        {
            try
            {
                int i = Convert.ToInt32(e.Keys[ASPxGridView1.KeyFieldName].ToString());
                string c = i.ToString();

                Accounts_Documents user = db.Accounts_Documentss.Find(i);
                db.Accounts_Documentss.Remove(user);
                db.SaveChanges(WebSecurity.CurrentUserName);
                loadMemberGrid();
            }
            catch (Exception)
            {


            }
            finally
            {
                e.Cancel = true;
            }
        }

        protected void ClearMember()
        {
            var remove = db.Accounts_Documentss.ToList().Where(a => string.IsNullOrEmpty(a.doc_generated));
            int count = db.Accounts_Documentss.ToList().Where(a => string.IsNullOrEmpty(a.doc_generated)).Count();
            foreach (var detail in remove)
            {
               Accounts_Documents user = db.Accounts_Documentss.Find(detail.Accounts_DocumentsID);
                db.Accounts_Documentss.Remove(user);
                db.SaveChanges(WebSecurity.CurrentUserName);
            }
            var removes = db.Accounts_Joints.ToList().Where(a => string.IsNullOrEmpty(a.CDSNo));
            int counts = db.Accounts_Joints.ToList().Where(a => string.IsNullOrEmpty(a.CDSNo)).Count();
            foreach (var detail in removes)
            {
                Accounts_Joint user = db.Accounts_Joints.Find(detail.id);
                db.Accounts_Joints.Remove(user);
                db.SaveChanges(WebSecurity.CurrentUserName);
            }



        }
        protected void loadMemberGrid()
        {
            ASPxGridView1.DataSource = null;
            ASPxGridView1.DataBind();
            ASPxGridView2.DataSource = null;
            ASPxGridView2.DataBind();
            if (id == "")
            {
                var mem = (from p in db.Accounts_Documentss
                           where string.IsNullOrEmpty(p.doc_generated)
                           select new { p.Accounts_DocumentsID, p.Name, p.doc_generated, p.ContentType }).ToList();

                ASPxGridView1.DataSource = mem.ToList();
                ASPxGridView1.DataBind();

                var mem2 = (from p in db.Accounts_Joints
                           where string.IsNullOrEmpty(p.CDSNo)
                           select new { p.id, p.Surname, p.Forenames, p.IDType,p.IDNo,p.Nationality,p.DateOfBirth,p.Gender,p.CDSNo,p.email }).ToList();

                ASPxGridView2.DataSource = mem2.ToList();
                ASPxGridView2.DataBind();


                
            }
            else
            {
                int my = Convert.ToInt32(id);
                string nme = "";
                var c = db.Account_Creations.ToList().Where(a => a.ID_ == my);
                foreach (var e in c)
                {
                    nme = e.CDSC_Number;
                }
                var mem = (from p in db.Accounts_Documentss
                           where p.doc_generated == nme || string.IsNullOrEmpty(p.doc_generated)
                           select new { p.Accounts_DocumentsID, p.Name, p.doc_generated, p.ContentType }).ToList();

                ASPxGridView1.DataSource = mem.ToList();
                ASPxGridView1.DataBind();

                var mem2 = (from p in db.Accounts_Joints
                            where p.CDSNo == nme || string.IsNullOrEmpty(p.CDSNo)
                            select new { p.id, p.Surname, p.Forenames, p.IDType, p.IDNo, p.Nationality, p.DateOfBirth, p.Gender, p.CDSNo, p.email }).ToList();

                ASPxGridView2.DataSource = mem2.ToList();
                ASPxGridView2.DataBind();
            }

            
        }


        protected void UpdateMember(string c)
        {

            
        

            List<Accounts_Documents> results = (from p in db.Accounts_Documentss
                                         where string.IsNullOrEmpty(p.doc_generated)
                                         select p).ToList();
WebReference.EscrowServiceSoap escorw = new WebReference.EscrowServiceSoap();
           foreach (Accounts_Documents p in results)
           {               p.doc_generated = c;
             //var edc2 = escorw.SubmitDocuments(p.doc_generated,Convert.ToBase64String(p.Data),p.Name,p.ContentType);

     }

            List<Accounts_Joint> results2 = (from p in db.Accounts_Joints
                                                where string.IsNullOrEmpty(p.CDSNo)
                                                select p).ToList();
            foreach (Accounts_Joint p in results2)
            {
                p.CDSNo = c;
                //var edc2 = escorw.SubmitDocuments(p.doc_generated,Convert.ToBase64String(p.Data),p.Name,p.ContentType);

            }

            db.SaveChanges(WebSecurity.CurrentUserName);
        }


    
        //protected void ASPxComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    var c = db.Account_Creations.ToList().Where(a => a.CDSC_Number == ASPxComboBox1.SelectedItem.Value.ToString());

        //    foreach(var p in c)
        //    {
        //        dividpayee.Text = p.Identification;
               
        //        //manBranch.Items.Insert(0, new ListItem(p.BranchCode,p.BranchName));
        //        //manAccount.Text = p.Accountnumber;
        //        //manBank.Items.Insert(0, new ListItem(p.Bankcode,p.Bankname));
        //        //manNames.Text = p.Surname_CompanyName + " " + p.OtherNames;
        //        //manAddress.Text = p.Address1;
               
        //    }
        //    //msgbox(ASPxComboBox1.SelectedItem.Value.ToString());
        //}

        protected void RadioButtonList3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (RadioButtonList3.SelectedIndex == 0)
           {
                Label34.Visible = true;
                Label22.Visible = true;
                cmbBank.Visible = true;
                Label23.Visible = true;
                cmbBranch.Visible = true;
                Label24.Visible = true;
                txtAcc.Visible = true;

                Label11.Visible = false;
                idPay.Visible = false;
                lblMobi.Visible = false;
                mobilewallet.Visible = false;

                //
                Label46.Visible = true;
                Label47.Visible = true;
                Label48.Visible = true;

           }
          else if (RadioButtonList3.SelectedIndex == 1)
            {
                Label34.Visible = false;
                Label22.Visible = false;
                cmbBank.Visible = false;
                Label23.Visible = false;
                cmbBranch.Visible = false;
                Label24.Visible = false;
                txtAcc.Visible = false;

                Label11.Visible = true;
                idPay.Visible = true;
                lblMobi.Visible = true;
                mobilewallet.Visible = true;
                Label46.Visible = false;
                Label47.Visible = false;
                Label48.Visible = false;
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {

        }

        protected void txtResident_SelectedIndexChanged(object sender, EventArgs e)
        {
            drpTax.Visible = true;
            var p = db.Client_Types.ToList().Where(a => a.ClientType == txtResident.SelectedItem.Text);

            foreach(var c in p){
                drpTax.Text = c.Rate.ToString();
            }

            drpTax.Enabled = false;
        }

        protected void drpSuffix_SelectedIndexChanged(object sender, EventArgs e)
        {
            //drpTax.Visible = true;
            //var p = db.Client_Types.ToList().Where(a => a.ClientType == drpSuffix.SelectedItem.Text);

            //foreach (var c in p)
            //{
            //    drpTax.Text = c.Rate.ToString();
            //}

            //drpTax.Visible = false;
        }

        protected void RadioButtonList4_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (RadioButtonList4.SelectedIndex == 1)
            {
                Label80.Visible = true;
                txtCDS.Visible = true;
            }else if(RadioButtonList4.SelectedIndex == 0)
            {
                Label80.Visible = false;
                txtCDS.Visible = false;
            }
        }

        protected void ASPxGridView2_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                object key = null;
                for (int i = 0; i < ASPxGridView2.VisibleRowCount; i++)
                {
                    if (ASPxGridView2.Selection.IsRowSelected(i))
                    {
                        key = ASPxGridView2.GetRowValues(i, "id");

                    }

                }

                int my = Convert.ToInt32(key);
                var c = db.Accounts_Joints.ToList().Where(a => a.id == my);

                foreach (var p in c)
                {
                    JForenames.Text = p.Forenames;
                    JSurname.Text = p.Surname;
                    JDOB.Date =Convert.ToDateTime(p.DateOfBirth);
                    JGender.SelectedValue = p.Gender;
                    JIDNO.Text = p.IDNo;
                    JIDTYPE.SelectedValue = p.IDType;
                    JEmail.Text = p.email;
                    JNationality.Items.Clear();
                    JNationality.Items.Insert(0, new ListItem(p.Nationality.ToString(), p.Nationality.ToString()));
                    LoadCurrencies();
                }
                Label59.Text = my.ToString();
                ASPxButton2.Text = "Update";

            }
            catch (Exception)
            {


            }
        }

        protected void ASPxGridView2_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
        {
            try
            {
                int i = Convert.ToInt32(e.Keys[ASPxGridView2.KeyFieldName].ToString());
                string c = i.ToString();

                Accounts_Joint user = db.Accounts_Joints.Find(i);
                db.Accounts_Joints.Remove(user);
                db.SaveChanges(WebSecurity.CurrentUserName);
                loadMemberGrid();
            }
            catch (Exception)
            {


            }
            finally
            {
                e.Cancel = true;
            }
        }

        protected void ASPxButton2_Click(object sender, EventArgs e)
        {
            if (JDOB.Text=="")
            {
                msgbox("Date Of Birth is required");
                return;
            }
            if (ASPxButton2.Text=="Add")
            {
 Accounts_Joint p = new Accounts_Joint();
              p.Forenames=JForenames.Text;
            p.Surname=JSurname.Text ;
                 p.DateOfBirth=JDOB.Date;
              p.Gender=JGender.SelectedValue;
            p.IDNo=JIDNO.Text;
            p.IDType=JIDTYPE.SelectedValue;
            p.email=JEmail.Text;
           p.Nationality=JNationality.SelectedItem.Value;
            db.Accounts_Joints.AddOrUpdate(p);
            db.SaveChanges(WebSecurity.CurrentUserName);
                JForenames.Text = "";
                JSurname.Text = "";
                JDOB.Text = "";
                JIDNO.Text = "";
                JEmail.Text = "";
  LoadCurrencies2();
                loadMemberGrid();
              
            }
            else if (ASPxButton2.Text == "Update")
            {
                int key = Convert.ToInt32(Label59.Text);

                Accounts_Joint p = db.Accounts_Joints.Find(key);
                p.Forenames = JForenames.Text;
                p.Surname = JSurname.Text;
                p.DateOfBirth = JDOB.Date;
                p.Gender = JGender.SelectedValue;
                p.IDNo = JIDNO.Text;
                p.IDType = JIDTYPE.SelectedValue;
                p.email = JEmail.Text;
                p.Nationality = JNationality.SelectedItem.Value;
                db.Accounts_Joints.AddOrUpdate(p);
                db.SaveChanges(WebSecurity.CurrentUserName);

                ASPxButton2.Text = "Add";
               JForenames.Text="";
               JSurname.Text = "";
               JDOB.Text = "";
               JIDNO.Text = "";
                  JEmail.Text = "";
                LoadCurrencies2();
                loadMemberGrid();

            }
           
        }

        protected void ASPxButton3_Click(object sender, EventArgs e)
        {
            Label44.Text = "personal";
           
            TabName.Value = Label44.Text;
            return;
        }

        protected void ASPxButton4_Click(object sender, EventArgs e)
        {
            Label44.Text = "trading";
            if (RadioButtonList2.SelectedIndex == 1)
            {
                Label44.Text = "contact";
            }
            TabName.Value = Label44.Text;
            return;
        }

        protected void ASPxButton1_Init(object sender, EventArgs e)
        {
            ASPxButton button = (ASPxButton)sender;
            GridViewDataItemTemplateContainer container = (GridViewDataItemTemplateContainer)button.NamingContainer;


            try
            {
                button.ClientSideEvents.Click = string.Format("function(s, e) {{ window.location = 'FileDownload.ashx?id={0}'; }}", container.KeyValue);

            }
            catch (Exception)
            {

            
            }
            
            if (Label44.Text== "trading")
            {
               Label44.Text = "trading";

            TabName.Value = Label44.Text;
            }
    
        }

        protected void txtCDS_TextChanged(object sender, EventArgs e)
        {
            int my = 0;
            try
            {
                my = db.Account_CreationPendingss.ToList().Where(a => a.CDSC_Number == txtCDS.Text).Count();

            }
            catch (Exception)
            {

                my = 0;
            }
            if (my > 0)
            {
                msgbox("The ATP/CSD Number already exsists");
                txtCDS.Text = "";
                return;
            }
        }



        //private bool FileExists(object key)
        //{
        //    return !string.IsNullOrEmpty(Product.GetData().First(p => p.ProductID.Equals(key)).ImagePath);
        //}
    }
    }
