﻿<%@ Page Language="C#" MaintainScrollPositionOnPostback="true" MasterPageFile="~/Site1.Master"  AutoEventWireup="true" CodeBehind="AccountCreation.aspx.cs" Inherits="BrokerOffice.AccountCreation.AccountCreation" %>

<%@ Register Assembly="DevExpress.Web.v18.2, Version=18.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>


<asp:content id="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <div>
 <div class="panel panel-default" style=" padding: 10px; margin: 10px">
    <div class="panel-heading" style="background-color:#428bca">
        <i style="color:white">Account Creation</i>
    </div>
        <div id="Tabs" role="tabpanel">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li class="active"><a href="#personal" aria-controls="personal" role="tab" data-toggle="tab">
                    Personal </a></li>
                <li><a href="#member" aria-controls="member" role="tab" data-toggle="tab"><asp:Label ID="Label49" runat="server" Text="Joint Members"></asp:Label></a></li>
                
                <li><a href="#contact" aria-controls="contact" role="tab" data-toggle="tab">Contact Details</a></li>
                <li><a href="#identification" aria-controls="identification" role="tab" data-toggle="tab">Identification Details</a></li>
                 <li><a href="#banking" aria-controls="banking" role="tab" data-toggle="tab">Banking Details</a></li>
                 <li><a href="#trading" aria-controls="banking" role="tab" data-toggle="tab">Uploads</a></li>

            </ul>
            <!-- Tab panes -->
            <div class="tab-content" style="padding-top: 20px">
                <div role="tabpanel" class="tab-pane active" id="personal">
              

                   
                  <table class="table table-striped" >
                      <tr>
                               <td>
                                 

                                 
                          <asp:Label ID="Label41" runat="server" Text="" Font-Bold="True">Select Exchange</asp:Label><asp:Label ID="Label14" runat="server" Text="*" ForeColor="Red" Width="5px"></asp:Label>
                             
                                <asp:DropDownList ID="dep"  CssClass="form-control" runat="server" Width="150px">
                         
                              </asp:DropDownList> 

                                   
                          </td> 
                          <td>
             <asp:Label ID="Label79"  runat="server" Text="" Font-Size="Small" Font-Bold="True">Account Type</asp:Label><asp:Label ID="Label18" runat="server" Text="*" ForeColor="Red" Width="5px"></asp:Label>
                    
                                      <asp:RadioButtonList ID="RadioButtonList4" runat="server" RepeatDirection="Horizontal" AutoPostBack="True" OnSelectedIndexChanged="RadioButtonList4_SelectedIndexChanged" Font-Bold="True" Height="22px" Width="173px" Font-Size="Small">
                                          <asp:ListItem Text="NEW" Value="GLOBAL"></asp:ListItem> 
                                          <asp:ListItem Text="EXISTING" Value="LOCAL"></asp:ListItem>
                                       
                                      </asp:RadioButtonList>
                                  </td>
                             <td>
                                 <asp:Label ID="Label80" runat="server" Text="ATP/CSD Number" Visible="False" Font-Bold="True"></asp:Label>
                         
                        <asp:TextBox ID="txtCDS" CssClass="form-control" runat="server"   Width="150px" Visible="False" AutoPostBack="True" OnTextChanged="txtCDS_TextChanged"></asp:TextBox>
                              
                     </td>
                          

                           
                        
                      </tr>
                      <tr>
                          <asp:Label ID="txtFkey" runat="server" Visible="False" Text=""></asp:Label>
                          <td>
                                       <asp:Label ID="Label31"  runat="server" Text="" Font-Size="Small" Font-Bold="True">Category</asp:Label>
                    <asp:Label ID="Label25" runat="server" Text="*" ForeColor="Red" Width="5px"></asp:Label>
                                      <asp:RadioButtonList ID="RadioButtonList2" runat="server" RepeatDirection="Horizontal" AutoPostBack="True" OnSelectedIndexChanged="RadioButtonList2_SelectedIndexChanged" Font-Bold="True" Height="26px" Width="302px" Font-Size="Small">
                                      <asp:ListItem Text="INDIVIDUAL" Value="LE"></asp:ListItem> 
                                          <asp:ListItem Text="JOINT" Value="LJ"></asp:ListItem>
                                            <asp:ListItem Text="CORPORATE" Value="LI"></asp:ListItem>
                                          
                                     </asp:RadioButtonList>
                                  </td>
                       <td>
                          <asp:Label ID="Label1"  runat="server" Text="Title" Font-Bold="True"></asp:Label>
                       
                           <asp:Label ID="Label67" runat="server" Text="" ForeColor="#CC0000"></asp:Label>     <asp:Label ID="Label52" runat="server" Text="*" ForeColor="#CC0000"></asp:Label><asp:DropDownList ID="drpTitle" CssClass="form-control" runat="server" Width="150px">
                              <asp:ListItem Text="Select Title" Value="Select Title"></asp:ListItem>
                                    <asp:ListItem Text="Mr" Value="Mr"></asp:ListItem>
                              <asp:ListItem Text="Mrs" Value="Mrs"></asp:ListItem>
                              <asp:ListItem Text="Miss" Value="Miss"></asp:ListItem>
                              <asp:ListItem Text="Dr" Value="Dr"></asp:ListItem>
                              <asp:ListItem Text="Rev" Value="Rev"></asp:ListItem>
                          </asp:DropDownList>
                      </td>
                                                                                     
                      <td>
                          <asp:Label ID="Label2"  runat="server" Text="" Font-Bold="True">First</asp:Label><asp:Label ID="Label26" runat="server" Text="*" ForeColor="Red" Width="5px"></asp:Label>
                          
                          <asp:TextBox ID="txtFirstName" CssClass="form-control" runat="server" Width="150px"></asp:TextBox>
                                       <asp:Label ID="Label65"   runat="server" Text="Name" Visible="False" Font-Bold="True"></asp:Label>
                         
 <asp:TextBox ID="txtCompanyName" TextMode="MultiLine" CssClass="form-control" runat="server" AutoPostBack="True"  Width="150px" Visible="False"></asp:TextBox>
                           
                      </td>
                      
                     
                      </tr> 
                      <tr>
<td>
                          <asp:Label ID="Label4"  runat="server" Text="Middle" Font-Bold="True"></asp:Label>
                        
                          <asp:TextBox ID="txtMiddle" CssClass="form-control" runat="server" Width="150px"></asp:TextBox>
                      </td>
                  <td>
                        <asp:Label ID="Label3"  runat="server" Text="" Font-Bold="True">Surname</asp:Label><asp:Label ID="Label27" runat="server" Text="*" ForeColor="Red" Width="5px"></asp:Label>
             
                          <asp:TextBox ID="txtSurname" CssClass="form-control" runat="server" Width="150px"></asp:TextBox>
                      </td>
                <td>
 
                          <asp:Label ID="Label17"  runat="server" Text="" Font-Bold="True">Date Of Birth/Incorporation</asp:Label><asp:Label ID="Label28" runat="server" Text="*" ForeColor="Red" Width="5px"></asp:Label>
  <dx:ASPxDateEdit ID="txtDOB"  CssClass="form-control" runat="server" Width="150px"></dx:ASPxDateEdit>
                         
   </td>
                                    
                      </tr>
                     
                      <tr>
                                 <td>
   <asp:Label ID="Label6"  runat="server" Text="" Font-Bold="True">Gender</asp:Label><asp:Label ID="Label33" runat="server" Text="*" ForeColor="Red" Width="5px"></asp:Label>
                                                   
                      <asp:RadioButtonList ID="RadioButtonList1" CssClass="form-control" style="border:none" width="150px" runat="server" RepeatDirection="Horizontal">
                                     <asp:ListItem Text="MALE" Value="MALE"></asp:ListItem>
                                                    <asp:ListItem Text="FEMALE" Value="FEMALE"></asp:ListItem>
                          </asp:RadioButtonList>
                      </td>
                     <td>
                       
                     </td>
                       
                      </tr>
                  </table>
                         
                    <dx style="float:right"><dx:ASPxButton ID="ASPxButton1" runat="server" CssClass="btn btn-primary" Theme="Moderno" Text="Next" OnClick="ASPxButton1_Click"></dx:ASPxButton></dx>
                </div>
                <div role="tabpanel" class="tab-pane" id="member">




    
                               <table class="table table-striped">
                     
                                     <tr> <td>
                                             <asp:Label ID="Label51" runat="server" Text="Forenames"></asp:Label>
                                             <asp:TextBox ID="JForenames" CssClass="form-control" runat="server" Width="150px"></asp:TextBox>
                                         </td>
                                         <td>
                                             <asp:Label ID="Label50" runat="server" Text="Surname"></asp:Label>
                                             <asp:TextBox ID="JSurname" CssClass="form-control" runat="server" Width="150px"></asp:TextBox>
                                         </td>
                                        
                                         <td>
                                             <asp:Label ID="Label53" runat="server" Text="DateOfBirth"></asp:Label>
                                             <dx:ASPxDateEdit ID="JDOB" CssClass="form-control" runat="server" Width="150px"></dx:ASPxDateEdit>
                                         </td>

                                     </tr>
                                   <tr>
                                       <td>
                                                <asp:Label ID="Label54" runat="server" Text="Gender"></asp:Label>
                          <asp:RadioButtonList ID="JGender" CssClass="form-control" width="150px" style="border:none" runat="server" RepeatDirection="Horizontal">
                                     <asp:ListItem Text="MALE" Value="MALE"></asp:ListItem>
                                                    <asp:ListItem Text="FEMALE" Value="FEMALE"></asp:ListItem>
                          </asp:RadioButtonList>
                                       </td>
                                       <td>
                                        <asp:Label ID="Label57" runat="server" Text="ID TYPE"></asp:Label>
                                            <asp:DropDownList ID="JIDTYPE" CssClass="form-control" runat="server" Width="150px">
                             

                          </asp:DropDownList>
                                      </td>
                                       <td>
                                                <asp:Label ID="Label55" runat="server" Text="IDNO"></asp:Label>
                                           <asp:TextBox ID="JIDNO" CssClass="form-control" runat="server" Width="150px"></asp:TextBox>
                                        
                                       </td>
                                       
                                   </tr>
                                   <tr>
                                       
                                       <td>
                                                <asp:Label ID="Label58" runat="server" Text="Email"></asp:Label>
                                           <asp:TextBox ID="JEmail" CssClass="form-control" runat="server" Width="150px"></asp:TextBox>
                                        
                                       </td>
                                      <td>
                                                <asp:Label ID="Label56" runat="server" Text="Nationality"></asp:Label>
                                            <asp:DropDownList ID="JNationality" CssClass="form-control" runat="server" Width="150px"></asp:DropDownList>
                                       
                                       </td>
                                       <td>
                                           <asp:Label ID="Label59" runat="server" Visible="false" Text=""></asp:Label>
                                           <br /> <br />
                                           <dx:ASPxButton ID="ASPxButton2" CssClass="btn btn-primary" runat="server" Text="Add" Height="23px" Theme="Glass" Width="70px" OnClick="ASPxButton2_Click"></dx:ASPxButton>
                                       </td>
                                   </tr>
                   
                               </table>
                             <dx style="float:right">
                    <dx:ASPxButton ID="ASPxButton3" runat="server" CssClass="btn btn-primary" Theme="Moderno" Text="Previous" OnClick="ASPxButton3_Click" ></dx:ASPxButton>
                    <dx:ASPxButton ID="ASPxButton4" runat="server" Theme="Moderno" CssClass="btn btn-primary" Text="Next" OnClick="ASPxButton4_Click"></dx:ASPxButton>


                </dx>
                     <br />
                     <br />
                     
                        <div class="row" style="margin-left:0px;float:left">
                    <dx:ASPxGridView ID="ASPxGridView2" KeyFieldName="id"  AutoGenerateColumns="false"  runat="server" CssClass="table table-reponsive" OnSelectionChanged="ASPxGridView2_SelectionChanged" EnableCallBacks="False" Theme="Glass" OnRowDeleting="ASPxGridView2_RowDeleting"  >
                        <SettingsPager AlwaysShowPager="True" PageSize="5">
                        </SettingsPager>
                        <SettingsBehavior AllowSelectSingleRowOnly="True"   ProcessSelectionChangedOnServer="True" AllowSelectByRowClick="True" ConfirmDelete="True" />
                          

                        <SettingsCommandButton>
                            <DeleteButton   ButtonType="Button">
                            </DeleteButton>
                            <SelectButton ButtonType="Button">
                            </SelectButton>
                        </SettingsCommandButton>
                       <Columns>
                <dx:GridViewCommandColumn ShowSelectCheckbox="True" Caption="Select"  VisibleIndex="0">
                </dx:GridViewCommandColumn>
                <dx:GridViewDataTextColumn FieldName="id" ReadOnly="True" VisibleIndex="1">
                    <EditFormSettings Visible="False" />
                </dx:GridViewDataTextColumn>
                     <dx:GridViewDataTextColumn FieldName="Surname" VisibleIndex="2">
                </dx:GridViewDataTextColumn>
                  <dx:GridViewDataTextColumn FieldName="Forenames" VisibleIndex="3">
                </dx:GridViewDataTextColumn>
                          
                             <dx:GridViewDataTextColumn FieldName="IDNo" VisibleIndex="4">
                </dx:GridViewDataTextColumn>
                           <dx:GridViewDataTextColumn FieldName="Nationality" VisibleIndex="5">
                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="DateOfBirth" VisibleIndex="6">
                </dx:GridViewDataTextColumn>
                             <dx:GridViewDataTextColumn FieldName="Gender" VisibleIndex="7">
                </dx:GridViewDataTextColumn>
                           
          <dx:GridViewDataTextColumn FieldName="email" VisibleIndex="8">
                </dx:GridViewDataTextColumn>
                            <dx:GridViewCommandColumn  ShowDeleteButton="True"  VisibleIndex="9">
                </dx:GridViewCommandColumn>
                              

            </Columns>

                      

                       

                    </dx:ASPxGridView>
                   
                                 </div>
                     
                 
                   </div>
                <div role="tabpanel" class="tab-pane" id="contact">
                    
                     <table class="table table-striped">
                      <tr>
                       <td>
                           <asp:Label ID="Label5"  runat="server" Text="" Font-Bold="True">Address Line 1</asp:Label><asp:Label ID="Label35" runat="server" Text="*" ForeColor="Red" Width="5px"></asp:Label>
         <asp:TextBox ID="addr1" runat="server"  CssClass="form-control" Visible="True" TextMode="MultiLine" Width="150px" ></asp:TextBox>
                                                   
                      </td>
                     <td>
                         <asp:Label ID="Label9" runat="server" Text="Address Line 2" Font-Bold="True"></asp:Label>
                         
                          <asp:TextBox ID="addr2" runat="server" CssClass="form-control"  Visible="True" TextMode="MultiLine" Width="150px" ></asp:TextBox>
                      </td>
                      <td>
                                  <asp:Label ID="Label10" runat="server" Text="Address Line 3" Font-Bold="True"></asp:Label>
                        
                           <asp:TextBox ID="addr3" runat="server" CssClass="form-control"  Visible="True" TextMode="MultiLine" Width="150px" ></asp:TextBox>
                      </td>
                         
                      </tr>
                         <tr>
                              <td>
 <asp:Label ID="Label12" runat="server" Text="" Font-Bold="True">Town</asp:Label><asp:Label ID="Label36" runat="server" Text="*" ForeColor="Red" Width="5px"></asp:Label>
                           
                          <asp:TextBox ID="txtTown" CssClass="form-control" runat="server" TextMode="MultiLine" Width="150px" ></asp:TextBox>
                          </td>
             <td>
                          
                         <asp:Label ID="Label32"  runat="server" Text="" Font-Bold="True">Country</asp:Label><asp:Label ID="Label37" runat="server" Text="*" ForeColor="Red" Width="5px"></asp:Label>
    
     <asp:DropDownList ID="drpCountry" CssClass="form-control" runat="server" Width="150px">
                         
                          </asp:DropDownList>
                             </td>
   
                             <td>
                                  <asp:Label ID="Label7" runat="server" Text="" Font-Bold="True">Nationality</asp:Label><asp:Label ID="Label38" runat="server" Text="*" ForeColor="Red" Width="5px"></asp:Label>
                            
                                 <asp:DropDownList ID="Nationality" CssClass="form-control" runat="server" Width="150px"></asp:DropDownList>
                         
                       
                     
                             </td>
                          
    
 
                                              
                      </tr>
                         <tr>                  
           <td>
                          <asp:Label ID="Label29" runat="server" Text="" Font-Bold="True">MobileNumber</asp:Label><asp:Label ID="Label39" runat="server" Text="*" ForeColor="Red" Width="5px"></asp:Label>
                <asp:TextBox ID="txtMobile" CssClass="form-control" runat="server" Width="150px"></asp:TextBox>
                     
                             </td>  
                           <td>
                          <asp:Label ID="Label15"  runat="server" Text="Telephone" Font-Bold="True"></asp:Label>
     <asp:TextBox ID="txtTel" CssClass="form-control" runat="server" Width="150px"></asp:TextBox>
                      </td>
                                             <td>
                          <asp:Label ID="Label16"  runat="server" Text="" Font-Bold="True">EmailAddress</asp:Label><asp:Label ID="Label40" runat="server" Text="*" ForeColor="Red" Width="5px"></asp:Label>
      <asp:TextBox ID="txtEmail" CssClass="form-control" runat="server" Width="150px"></asp:TextBox>
                      </td>
                             </tr>
                                                                  
                  
                  </table>
                        
                <dx style="float:right">
                    <dx:ASPxButton ID="dxPersonal" runat="server" CssClass="btn btn-primary" Theme="Moderno" Text="Previous" OnClick="dxPersonal_Click" ></dx:ASPxButton>
                  
                      <dx:ASPxButton ID="dxIdentiication" runat="server" CssClass="btn btn-primary" Theme="Moderno" Text="Next" OnClick="dxIdentiication_Click"></dx:ASPxButton>


                </dx>
               
                </div>
                   <div role="tabpanel" class="tab-pane" id="identification">
                 

                                     <table class="table table-striped">
                              <tr>
                                  
                                  <td>
                                 
                                  </td>
                                   <td>
                                 
                                  </td>
                                   <td>
                                 
                                  </td>
                              </tr>           
                      <tr>
                            <td>
                 
                          <asp:Label ID="Label8"  runat="server" Text="" Font-Bold="True">Client Type</asp:Label><asp:Label ID="Label42" runat="server" Text="*" ForeColor="Red" Width="5px"></asp:Label>
                        
                           <asp:DropDownList ID="txtResident" CssClass="form-control" runat="server" Width="150px" AutoPostBack="True" OnSelectedIndexChanged="txtResident_SelectedIndexChanged"></asp:DropDownList>
                      </td>
                          
                                 <td>
                          <asp:Label ID="Label30"  runat="server" Text="" Font-Bold="True">IDType</asp:Label><asp:Label ID="Label43" runat="server" Text="*" ForeColor="Red" Width="5px"></asp:Label>
                 
                                     <asp:DropDownList ID="drpID" CssClass="form-control" runat="server" Width="150px">
                             

                          </asp:DropDownList>
                      </td>
                           <td>
                                <asp:Label ID="Label19"  runat="server" Text="" Font-Bold="True">RegistrationNumber</asp:Label><asp:Label ID="Label45" runat="server" Text="*" ForeColor="Red" Width="5px"></asp:Label>
                                
                          <asp:TextBox ID="txtReg" CssClass="form-control" runat="server" Width="150px"></asp:TextBox>
                                
                                <asp:Label ID="Label20"  runat="server" Text="ID" Font-Bold="True"></asp:Label>

                           <asp:TextBox ID="txtID" CssClass="form-control" runat="server" AutoPostBack="True" OnTextChanged="txtID_TextChanged" Width="150px"></asp:TextBox>
                                </td>
                    

                          </tr>
                                    
                            <tr> <td>
                          <asp:Label ID="Label21"  runat="server" Text="Tax" Font-Bold="True"></asp:Label>
                           <asp:TextBox ID="drpTax" CssClass="form-control" runat="server" Width="150px" />
                           
                      </td>
                              
                                <td>
                                   

                                </td>
                                <td></td>
                             </tr>
                             
                                         </table>
                           
                       
                          <dx style="float:right">
                    <dx:ASPxButton ID="dxContact" runat="server" CssClass="btn btn-primary" Theme="Moderno" Text="Previous" OnClick="dxContact_Click" ></dx:ASPxButton>
                    <dx:ASPxButton ID="dxBanking" runat="server" CssClass="btn btn-primary" Theme="Moderno" Text="Next" OnClick="dxBanking_Click"></dx:ASPxButton>


                </dx>
                </div>
                <div role="tabpanel" class="tab-pane" id="banking">
   


                               <table class="table table-striped">
                                            <tr>

                                       <td>
                                           <asp:Label ID="Label13" CssClass="control-label" runat="server" Text="Payment Method" Font-Bold="True"></asp:Label>
                                                  <asp:RadioButtonList ID="RadioButtonList3" runat="server" RepeatDirection="Horizontal" AutoPostBack="True" OnSelectedIndexChanged="RadioButtonList3_SelectedIndexChanged" Font-Bold="True" Height="26px" Width="237px" Font-Size="Small">
                                           <asp:ListItem Text="BANK" Value="BANK"></asp:ListItem>
                                           <asp:ListItem Text="MOBILE" Value="MOBILE"></asp:ListItem>
                                      </asp:RadioButtonList>
                                       </td>
                                       <td>

                                       </td>
                                       <td>

                                       </td>
                                       <td>

                                       </td>
                                   </tr>
                                   <tr>

                                       <td>
                                           <asp:Label ID="Label34" CssClass="control-label" runat="server" Text="1. Settlement Details" Font-Bold="True"></asp:Label>
                                       </td>
                                       <td>

                                       </td>
                                       <td>

                                       </td>
                                       <td>

                                       </td>
                                   </tr>
                      <tr>
<td>
                          <asp:Label ID="Label22"  runat="server" Text="Bank" Font-Bold="True"></asp:Label>

                              <asp:Label ID="Label46" runat="server" Text="*" ForeColor="Red" Width="5px"></asp:Label> <asp:DropDownList ID="cmbBank" CssClass="form-control" runat="server" AutoPostBack="True" OnSelectedIndexChanged="cmbBank_SelectedIndexChanged" Width="150px"></asp:DropDownList>
                      </td>
                          <td>
                          <asp:Label ID="Label23"  runat="server" Text="Branch" Font-Bold="True"></asp:Label>
                  
                           <asp:Label ID="Label47" runat="server" Text="*" ForeColor="Red" Width="5px"></asp:Label></asp:Label><asp:DropDownList ID="cmbBranch" CssClass="form-control" runat="server" Width="150px" ></asp:DropDownList>
                        </td>
                      <td>

                  <asp:Label ID="Label24" runat="server" Text="AccountNumber" Font-Bold="True"></asp:Label>
                  
                <asp:Label ID="Label48" runat="server" Text="*" ForeColor="Red" Width="5px"></asp:Label> <asp:TextBox ID="txtAcc" runat="server"  CssClass="form-control" Visible="True" Width="150px"></asp:TextBox>
                   
                       </td>
   <td>

                      
                   
                                       </td>
                          </tr>
                                      <tr>
                                       <td>
                                           <asp:Label ID="Label11" CssClass="control-label" runat="server" Text="2. Mobile Details" Font-Bold="True"></asp:Label>
                                       </td>
                                       <td>

                                       </td>
                                       <td>

                                       </td>
                                       <td>

                                       </td>
                                   </tr>
                   <tr>
                            <td>
                                <asp:RadioButtonList ID="idPay" runat="server" Width="450px" RepeatDirection="Horizontal" OnSelectedIndexChanged="idPay_SelectedIndexChanged" Font-Size="Small">
                                     <asp:ListItem Text="ECOCASH" Value="ECOCASH" ></asp:ListItem>
                                   
                                     <asp:ListItem Text="NETCASH" Value="NETCASH" ></asp:ListItem>
                                     <asp:ListItem Text="TELECASH" Value="TELECASH" ></asp:ListItem>
                                    <asp:ListItem Text="ONE WALLET" Value="ONE WALLET" ></asp:ListItem>
                                    
                                </asp:RadioButtonList>
                          </td>
                      <td>
                                 
                                  </td>
                                   <td>
                                 
                                  </td>
                       
                                       
                                      
<//tr>
                                      <tr>
                                          <td >
  <asp:Label ID="lblMobi" runat="server" Text="Mobile Wallet Number" Visible="False" Font-Bold="True"></asp:Label>
                    
 <asp:TextBox ID="mobilewallet" runat="server"  CssClass="form-control" Visible="False" Width="150px" ></asp:TextBox>
                              
                     
                          </td>
     

                                       <td>
                                 
                                  </td>
                                   <td>
                                 
                                  </td>
                                   </tr>
                             
                                  
                                 
                                 

                                         </table>
              
                             <dx style="float:right">
                    <dx:ASPxButton ID="dxIdentificationBack" runat="server" CssClass="btn btn-primary" Theme="Moderno" Text="Previous" OnClick="dxIdentificationBack_Click" ></dx:ASPxButton>
                    <dx:ASPxButton ID="dxTrading" runat="server" Theme="Moderno" CssClass="btn btn-primary" Text="Next" OnClick="dxTrading_Click"></dx:ASPxButton>


                </dx>
                </div>
                 
                   <div role="tabpanel" class="tab-pane" id="trading">
 

    
                               <table class="table table-striped">
                     
                                     <tr>
                                       <td class="auto-style1">
                                           <strong>Attachments</strong></td>
                                       <td>

                                       </td>
                                       <td>

                                       </td>
                                     
                                   </tr>
                            <tr>
                                
                                <td>
                                <br />
                                <asp:FileUpload ID="FileUpload" runat="server" />
                                    <asp:Button ID="upload2" CssClass="btn btn-primary" runat="server" Text="Upload" OnClick="upload2_Click" />
                                </td>
                                <td>
                                <br />
      <asp:Button ID="Button1" CssClass="btn btn-primary" runat="server" Text="Download" OnClick="Button1_Click" Visible="False"  />
                              
                                </td>
                                <td></td>
                           
                             </tr>
<tr>
                                 
                                       <td>
                                           <asp:HiddenField ID="TabName" runat="server" />
                                           <asp:Label ID="Label44" runat="server" Visible="false" Text=""></asp:Label>
                                           <asp:Label ID="Label78" runat="server" Text="Label" Visible="False"></asp:Label>
                                       </td> 
    <td >
                                      <asp:Button ID="btnSubmit" CssClass="btn btn-primary" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
                                  </td>
    <td>
                                 
                                  </td>
                                
                                   </tr>
                                         </table>
                        <h6>To download the file first select the row  then click download</h6>
                    <dx:ASPxGridView ID="ASPxGridView1" KeyFieldName="Accounts_DocumentsID"  AutoGenerateColumns="false"  runat="server" CssClass="nav-tabs" EnableCallBacks="False" Theme="Glass" OnRowDeleting="ASPxGridView1_RowDeleting"  Width="615px">
                        <SettingsPager AlwaysShowPager="True" PageSize="5">
                        </SettingsPager>
                        <SettingsBehavior AllowSelectSingleRowOnly="True"   ProcessSelectionChangedOnServer="True" AllowSelectByRowClick="True" ConfirmDelete="True" />

                    

                        <SettingsCommandButton>
                            <DeleteButton   ButtonType="Button">
                            </DeleteButton>
                            <SelectButton ButtonType="Button">
                            </SelectButton>
                        </SettingsCommandButton>
                       <Columns>
                                           <dx:GridViewCommandColumn ShowSelectCheckbox="True" Caption="Select"  VisibleIndex="0">
                </dx:GridViewCommandColumn>
                <dx:GridViewDataTextColumn FieldName="Accounts_DocumentsID" ReadOnly="True" VisibleIndex="1">
            
                </dx:GridViewDataTextColumn>
                     <dx:GridViewDataTextColumn FieldName="doc_generated" VisibleIndex="2">
                </dx:GridViewDataTextColumn>
                  <dx:GridViewDataTextColumn FieldName="Name" VisibleIndex="3">
                </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="ContentType" VisibleIndex="4">
                </dx:GridViewDataTextColumn>
                                          <dx:GridViewCommandColumn  ShowDeleteButton="True"  VisibleIndex="5">
                </dx:GridViewCommandColumn>
            <dx:GridViewDataColumn  Caption="Downloads" VisibleIndex="6">
                    <DataItemTemplate>
                        <dx:ASPxButton ID="ASPxButton1" runat="server"  OnInit="ASPxButton1_Init" 
                            AutoPostBack="False" RenderMode="Link" Text="Download">
                           <Image IconID="actions_download_16x16" />
                        </dx:ASPxButton>
                    </DataItemTemplate>
                </dx:GridViewDataColumn>
            </Columns>

                      

                       

                    </dx:ASPxGridView>
                          
                       </div>
                   

                </div>
            </div>
        </div>
        </div>

     <script type="text/javascript">
$(function () {
    var tabName = $("[id*=TabName]").val() != "" ? $("[id*=TabName]").val() : "personal";
    $('#Tabs a[href="#' + tabName + '"]').tab('show');
    $("#Tabs a").click(function () {
        $("[id*=TabName]").val($(this).attr("href").replace("#", ""));
    });
});
</script>

</asp:content>