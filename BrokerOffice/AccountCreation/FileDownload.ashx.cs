﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Linq;
using BrokerOffice.DAO;

namespace BrokerOffice.AccountCreation
{
    /// <summary>
    /// Summary description for FileDownload1
    /// </summary>
    public class FileDownload1 : IHttpHandler
    {
        SBoardContext db = new SBoardContext();
        public void ProcessRequest(HttpContext context)
        {
            string id = context.Request["id"];
            int my = Convert.ToInt32(id);
            var dd = db.Accounts_Documentss.ToList().Where(a => a.Accounts_DocumentsID==my);
            foreach (var c in dd)
            {

                ExportToResponse(context, c.Data, c.Name + id, c.ContentType, false);
            }


        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }


        public void ExportToResponse(HttpContext context, byte[] content, string fileName, string fileType, bool inline)
        {
            context.Response.Clear();
            context.Response.ContentType = "application/" + fileType;
            context.Response.AddHeader("Content-Disposition", string.Format("{0}; filename={1}.{2}", inline ? "Inline" : "Attachment", fileName, fileType));
            context.Response.AddHeader("Content-Length", content.Length.ToString());
            //Response.ContentEncoding = System.Text.Encoding.Default;
            context.Response.BinaryWrite(content);
            //context.Response.Flush();
            //context.Response.Close();
            context.Response.End();
        }
    }
}