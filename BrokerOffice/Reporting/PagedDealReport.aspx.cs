﻿using BrokerOffice.DAO;
using DevExpress.XtraReports.UI;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Text;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BrokerOffice.Reporting
{
    public partial class PagedDealReport: System.Web.UI.Page
    {

        SBoardContext db = new SBoardContext();
       
        //public string account = "";
        //public string orderno= "";

        protected void Page_Init(object sender, EventArgs e)
        {
            //passreport parameters

            
            //account = Request.QueryString["Account"];
            //orderno = Request.QueryString["OrderNo"];

            if (this.IsPostBack)
            {
                //TabName.Value = Request.Form[TabName.UniqueID];

            }

            else if (IsPostBack == false)
            {

                try
                {
                    PagedDeal2 report = new PagedDeal2();
               
                    report.Parameters["Type"].Value = HttpContext.Current.Session["Type"].ToString();
                    report.Parameters["Trade"].Value = HttpContext.Current.Session["Trade"].ToString();
                    report.Parameters["Trade2"].Value = HttpContext.Current.Session["Trade2"].ToString();
                    if (HttpContext.Current.Session["Type"].ToString() != "BUY")
                    {
                        report.Parameters["Stamp"].Value = "Stamp duty";
                    }else
                    {
                        report.Parameters["Stamp"].Value = "Zim";
                    }
                    foreach (XRControlStyle style in report.StyleSheet)
                    {
                        style.Font = new Font(FontCollection.Families[0], 8F, GraphicsUnit.Point);
                    }
                    report.xrLabel6.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);

                    report.xrLabel22.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);

                    report.xrLabel15.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);

                    report.xrLabel2.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);

                    report.xrLabel1.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);

                    report.xrLabel11.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                    report.xrLabel23.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);


                    report.xrLabel18.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                    report.xrLabel46.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                    report.xrLabel21.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                    report.xrLabel3.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                    report.xrLabel31.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                    report.xrLabel34.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                    report.xrLabel35.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                    report.xrLabel5.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);


                    report.xrLabel36.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                    report.xrLabel26.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                  report.xrLabel33.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                    report.xrLabel13.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                    report.xrLabel42.Font = new Font(FontCollection.Families[0], 8F, GraphicsUnit.Point);
                    report.xrLabel44.Font = new Font(FontCollection.Families[0], 8F, GraphicsUnit.Point);



                    report.xrLabel22.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                    report.xrLabel17.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                    report.xrLabel41.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                    //report.xrTableCell1.Font = new Font(FontCollection.Families[0], 8F, GraphicsUnit.Point);
                  //  report.xrTableCell3.Font = new Font(FontCollection.Families[0], 8F, GraphicsUnit.Point);
                    ASPxDocumentViewer1.Report = report;

                    
                }
                catch (Exception)
                {

                    Response.Redirect("~/Account");
                }
            }
        }
        static PrivateFontCollection fontCollection;
        public static FontCollection FontCollection
        {
            get
            {
                if (fontCollection == null)
                {
                    fontCollection = new PrivateFontCollection();
                    fontCollection.AddFontFile(HttpContext.Current.Server.MapPath("~/Fonts/Gotham-Light.ttf"));
                }
                return fontCollection;
            }
        }
        protected void ASPxButton1_Click(object sender, EventArgs e)
        {
            

            try
            {

                string mac = HttpContext.Current.Session["Type"].ToString();
                DateTime dte = Convert.ToDateTime(HttpContext.Current.Session["Trade"].ToString());
                DateTime dte2 = Convert.ToDateTime(HttpContext.Current.Session["Trade2"].ToString());
                var c = db.DealerDGs.ToList().Where(a => Convert.ToDateTime(a.DatePosted) >=dte && Convert.ToDateTime(a.DatePosted) < dte2.AddDays(1));
                var d = db.DealerDGs.ToList().Where(a => Convert.ToDateTime(a.DatePosted) > dte && Convert.ToDateTime(a.DatePosted) < dte2.AddDays(1)).Count();
                if (mac == "BUY")
                {
                    foreach (var f in c)
                    {
                        XtraDeal report = new XtraDeal();
                        report.Parameters["Account"].Value = f.Account1;

                        report.Parameters["OrderNo"].Value =f.Deal;
                        report.Parameters["SID"].Value =f.Deal;
                        report.Parameters["QTY"].Value = f.Quantity;
                        report.Parameters["Dte"].Value = f.DatePosted;
                        report.Parameters["Max"].Value = f.Price;

                     
                        var client = new RestClient("http://localhost/BrokerService");
                        var request = new RestRequest("Brokerfull/{v}", Method.GET);
                        request.AddUrlSegment("v",f.Security);
                        IRestResponse response = client.Execute(request);
                        string validate = response.Content;

                        List<DealClass.DealFull> dataList = JsonConvert.DeserializeObject<List<DealClass.DealFull>>(validate);

                        foreach (var pp in dataList)
                        {
                            report.Parameters["Broker"].Value = f.Security;
                            report.Parameters["BrokerName"].Value = pp.fnam.ToUpper();
                        }
                        string z =f.Account1;
                        string cf = "";
                        var p = db.Account_Creations.ToList().Where(a => a.CDSC_Number == z);
                        foreach (var v in p)
                        {
                            cf = v.Surname_CompanyName + " " + v.OtherNames;
                            report.Parameters["Address"].Value = v.depname;
                        }
                        report.Parameters["TYPO"].Value = "BUY";

                        
                            //report.Parameters["Stamp"].Value = "Stamp duty";
                      
                            report.Parameters["Stamp"].Value = "Zim";
                        
                        report.Parameters["TYPO2"].Value = "PURCHASE";

                        report.xrLabel7.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);

                        report.xrLabel7.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);

                        report.xrLabel22.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);

                        report.xrLabel15.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);

                        report.xrLabel2.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);

                        report.xrLabel1.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);

                        report.xrLabel11.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                        report.xrLabel33.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);


                        report.xrLabel18.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                        report.xrLabel46.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                        report.xrLabel21.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                        report.xrLabel3.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                        report.xrLabel31.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                        report.xrLabel34.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                        report.xrLabel35.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);


                        report.xrLabel36.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                        report.xrLabel26.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                        report.xrLabel42.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                        report.xrLabel13.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);

                        report.xrLabel42.Font = new Font(FontCollection.Families[0], 8F, GraphicsUnit.Point);
                        report.xrLabel44.Font = new Font(FontCollection.Families[0], 8F, GraphicsUnit.Point);


                        report.xrLabel22.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                        report.xrLabel17.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                        report.xrLabel41.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                        //report.xrLabel40.Font = new Font(FontCollection.Families[0], 8F, GraphicsUnit.Point);
                       // report.xrTableCell1.Font = new Font(FontCollection.Families[0], 8F, GraphicsUnit.Point);
                        //report.xrTableCell3.Font = new Font(FontCollection.Families[0], 8F, GraphicsUnit.Point);
                        //ASPxDocumentViewer1.Report = report;
                        // Create a new memory stream and export the report into it as PDF.
                        MemoryStream mem = new MemoryStream();
                        report.ExportToPdf(mem);

                        // Create a new attachment and put the PDF report into it.
                        mem.Seek(0, System.IO.SeekOrigin.Begin);
                        Attachment att = new Attachment(mem, "BrokerNote" + DateTime.Now.ToString("dd-MMM-yyyy") + ".pdf", "application/pdf");
                        sendM(att, z, cf);
                        // Close the memory stream.
                        mem.Close();
                       
                    }

                }
                else
                {
                    foreach (var f in c)
                    {

                        XtraDeal report = new XtraDeal();
                        report.Parameters["Account"].Value = f.Account1;

                        report.Parameters["OrderNo"].Value = f.Deal;
                        report.Parameters["SID"].Value = f.Deal;
                        report.Parameters["QTY"].Value = f.Quantity;
                        report.Parameters["Dte"].Value = f.DatePosted;
                        report.Parameters["Max"].Value = f.Price;

                     
                        var client = new RestClient("http://localhost/BrokerService");
                        var request = new RestRequest("Brokerfull/{v}", Method.GET);
                        request.AddUrlSegment("v",f.Security);
                        IRestResponse response = client.Execute(request);
                        string validate = response.Content;

                        List<DealClass.DealFull> dataList = JsonConvert.DeserializeObject<List<DealClass.DealFull>>(validate);

                        foreach (var pp in dataList)
                        {
                            report.Parameters["Broker"].Value = f.Security;
                            report.Parameters["BrokerName"].Value = pp.fnam.ToUpper();
                        }
                        string z = f.Account1;
                        string cf = "";
                        var p = db.Account_Creations.ToList().Where(a => a.CDSC_Number == z);
                        foreach (var v in p)
                        {
                            cf = v.Surname_CompanyName + " " + v.OtherNames;
                            report.Parameters["Address"].Value = v.depname;
                        }
                        report.Parameters["TYPO"].Value = "SELL";


                        report.Parameters["Stamp"].Value = "Stamp duty";

                        //report.Parameters["Stamp"].Value = "Zim";

                        report.Parameters["TYPO2"].Value = "SALE";

                        report.xrLabel7.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);

                        report.xrLabel7.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);

                        report.xrLabel22.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);

                        report.xrLabel15.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);

                        report.xrLabel2.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);

                        report.xrLabel1.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);

                        report.xrLabel11.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                        report.xrLabel33.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);


                        report.xrLabel18.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                        report.xrLabel46.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                        report.xrLabel21.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                        report.xrLabel3.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                        report.xrLabel31.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                        report.xrLabel34.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                        report.xrLabel35.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);


                        report.xrLabel36.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                        report.xrLabel26.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                        report.xrLabel42.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                        report.xrLabel13.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);


                        report.xrLabel22.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                        report.xrLabel17.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                        report.xrLabel41.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                        //report.xrLabel40.Font = new Font(FontCollection.Families[0], 8F, GraphicsUnit.Point);


                        //ASPxDocumentViewer1.Report = report;
                        // Create a new memory stream and export the report into it as PDF.
                        MemoryStream mem = new MemoryStream();
                        report.ExportToPdf(mem);

                        // Create a new attachment and put the PDF report into it.
                        mem.Seek(0, System.IO.SeekOrigin.Begin);
                        Attachment att = new Attachment(mem, "BrokerNote" + DateTime.Now.ToString("dd-MMM-yyyy") + ".pdf", "application/pdf");
                        sendM(att, z, cf);
                        // Close the memory stream.
                        mem.Close();
                    } 
                }

                msgbox("The report has been sent");
            }
            catch (Exception)
            {

                msgbox("Emailing report has failed");
            }
            return;
        }
        public void msgbox(string strMessage)
        {
            string strScript = "<script language=JavaScript>";
            strScript += "window.alert(\"" + strMessage + "\");";
            strScript += "</script>";
            System.Web.UI.WebControls.Label lbl = new System.Web.UI.WebControls.Label();
            lbl.Text = strScript;
            Page.Controls.Add(lbl);
        }

        public void sendM(Attachment att,string  acc,string accountname)
        {
            // Create a new message and attach the PDF report to it.

            // Send the e-mail message via the specified SMTP server.
            //SmtpClient smtp = new SmtpClient("smtp.somewhere.com");
            string nn = investormail(acc);
            if (nn != "")
            {


                SmtpClient clients = new SmtpClient();
                clients.Port = 587;
                clients.Host = "smtp.gmail.com";
                clients.EnableSsl = true;
                ServicePointManager.ServerCertificateValidationCallback =
    delegate (object s, X509Certificate certificate,
           X509Chain chain, SslPolicyErrors sslPolicyErrors)
    { return true; };
                clients.Timeout=0;
                clients.DeliveryMethod = SmtpDeliveryMethod.Network;
                clients.UseDefaultCredentials = false;
                clients.Credentials = new System.Net.NetworkCredential("edocwebapp@gmail.com", "escrow123456789");

                MailMessage mail = new MailMessage();
                mail.Attachments.Add(att);

                // Specify sender and recipient options for the e-mail message.
                mail.From = new MailAddress("brokeroffice@escrowgroup.org", "Broker Office");
                mail.To.Add(new MailAddress(investormail(acc), accountname));
                mail.CC.Add(new MailAddress(custodians(acc).Split(',')[1], custodians(acc).Split(',')[0]));
                mail.BodyEncoding = UTF8Encoding.UTF8;
                mail.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                // Specify other e-mail options.
                mail.Subject = "Broker Note";
                mail.Body = "This is a Broker Note sent by Broker Office.";

                clients.Send(mail);

            }

        }

        public string custodians(string acc)
        {
            string validate = "";
            var client = new RestClient("http://localhost/BrokerService");
            var request = new RestRequest("Custodian/{s}", Method.POST);
            request.AddUrlSegment("s", acc);
            IRestResponse response = client.Execute(request);
            validate = response.Content;

            var Jsonobject = JsonConvert.DeserializeObject<string>(validate);

            validate = Jsonobject.ToString();


            return validate;

         
        }
        public string investormail(string num)
        {
            var p = db.Account_CreationPendingss.ToList().Where(a => a.CDSC_Number == num);
            string mm = "";
            foreach (var d in p)
            {
                mm = d.Emailaddress;
            }
           
            return mm;
        }
    }
}