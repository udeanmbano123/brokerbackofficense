﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" MaintainScrollPositionOnPostback="true" Async="true" AutoEventWireup="true" CodeBehind="AuditMasterPage.aspx.cs" Inherits="BrokerOffice.Reporting.AuditMasterPage" %>


<%@ Register Assembly="DevExpress.XtraReports.v18.2.Web.WebForms, Version=18.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraReports.Web" TagPrefix="dx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="margin-left: 5%; margin-right: 10%; background: white" class="form-horizontal">
        <div class="panel-heading" style="background-color: #428bca">
            <l style="color: white">Audit Report</l>

        </div>
        <a class="btn btn-primary" href="../Reporting/AuditMaster">Back To Search

        </a>
    </div>
    <dx:ASPxDocumentViewer ID="ASPxDocumentViewer1" runat="server" ReportTypeName="BrokerOffice.Reporting.XtraAuditMaster" Theme="Moderno"></dx:ASPxDocumentViewer>
</asp:Content>
