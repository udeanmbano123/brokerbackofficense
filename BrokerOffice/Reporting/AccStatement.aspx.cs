﻿using BrokerOffice.DAO;
using BrokerOffice.Models;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebMatrix.WebData;

namespace BrokerOffice.Reporting
{
    public partial class AccStatement : System.Web.UI.Page
    {
        private SBoardContext db = new SBoardContext();
        public string date = "";

        public string date2 = "";

        public string account = "";

        protected void Page_Init(object sender, EventArgs e)
        {
            //passreport parameters

            date = Request.QueryString["From"];
            date2 = Request.QueryString["To"];
            account = Request.QueryString["Acco"];


            if (this.IsPostBack)
            {
                //TabName.Value = Request.Form[TabName.UniqueID];

            }

            else if (IsPostBack == false)
            {
                XtraAccountStatement report = new XtraAccountStatement();
                report.Parameters["fDate"].Value = date;
                report.Parameters["tDate"].Value = date2;
                report.Parameters["Acc"].Value = account;

                ASPxDocumentViewer1.Report = report;
            }
        }


    }
}