﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AccountAuditor.aspx.cs"  CodeBehind="AccountAuditor.aspx.cs"  MasterPageFile="~/Site1.Master"    Inherits="BrokerOffice.Reporting.AccountAuditor" %>

<%@ Register assembly="CrystalDecisions.Web, Version=13.0.3500.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" namespace="CrystalDecisions.Web" tagprefix="CR" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 

    <div style="margin-left: 5%; margin-right: 10%; background: white" class="form-horizontal">
        <div class="panel-heading" style="background-color: #428bca">
            
            <l style="color: white">Account Audit</l>

        </div>
        <a class="btn btn-primary" href="../FinancialAccounts/AuditTrail.aspx">Back To Search
        </a>

    </div>


    <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" AutoDataBind="true" />

  
</asp:Content>