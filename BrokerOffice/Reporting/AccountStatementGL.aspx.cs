﻿using BrokerOffice.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BrokerOffice.Reporting
{
    public partial class AccountStatementGL : System.Web.UI.Page
    {
        SBoardContext db = new SBoardContext();
        public string id = "";
        public string frmdate = "";
        public string todate = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            id = Request.QueryString["Account"];
            frmdate= Request.QueryString["Begin"];
            todate = Request.QueryString["End"];
            if (this.IsPostBack)
            {
                //TabName.Value = Request.Form[TabName.UniqueID];

            }

            else if (IsPostBack == false)
            {
                int ccd = 0;
                
               XtraAccStmt report = new XtraAccStmt();
                report.Parameters["Account"].Value = id;
                var c = db.Account_Creations.ToList().Where(a => a.CDSC_Number.ToString() == id);
                foreach(var p in c)
                {
                    report.Parameters["AccountName"].Value = p.Surname_CompanyName + " " + p.OtherNames;
                }
                var cc = db.Accounts_Masters.ToList().Where(a => a.AccountNumber.ToString() == id);
                try
                {
                    ccd = db.Accounts_Masters.ToList().Where(a => a.AccountNumber.ToString() == id).Count();

                }
                catch (Exception)
                {

                    ccd = 0;
                }
                if (ccd> 0)
                {
                 foreach (var p in cc)
                {
                    report.Parameters["AccountName"].Value = p.AccountName;
                }
               
                }
                 report.Parameters["FDate"].Value = frmdate;
                report.Parameters["TDate"].Value = todate;
                //report.Parameters["yourParameter2"].Value = secondValue;

                ASPxDocumentViewer1.Report = report;
            }
        }
    }
}