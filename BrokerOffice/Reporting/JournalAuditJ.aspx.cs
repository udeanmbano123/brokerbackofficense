﻿using BrokerOffice.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BrokerOffice.Reporting
{
    public partial class JournalAuditJ : System.Web.UI.Page
    {
        SBoardContext db = new SBoardContext();
        public string id = "";
        public string frmdate = "";
        public string todate = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            id = Request.QueryString["Account"];

            frmdate = Request.QueryString["From"];
            todate = Request.QueryString["To"];
            if (this.IsPostBack)
            {
                //TabName.Value = Request.Form[TabName.UniqueID];

            }
            else if (IsPostBack == false)
            {
                int ccd = 0;
                
               XtraFullJ report = new XtraFullJ();
               var cc = db.Accounts_Masters.ToList().Where(a => a.AccountNumber.ToString() == id);
                try
                {
                    ccd = db.Accounts_Masters.ToList().Where(a => a.AccountNumber.ToString() == id).Count();

                }
                catch (Exception)
                {

                    ccd = 0;
                }
                if (ccd > 0)
                {
                    foreach (var p in cc)
                    {     report.Parameters["Account"].Value = p.AccountNumber.ToString();
                   
                        report.Parameters["AccountName"].Value = p.AccountName;
                    }

                }
       

                report.Parameters["FDate"].Value = frmdate;
                report.Parameters["TDate"].Value = todate;
                //report.Parameters["yourParameter2"].Value = secondValue;

                ASPxDocumentViewer1.Report = report;
            }
        }
    }
}