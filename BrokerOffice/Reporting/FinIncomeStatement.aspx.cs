﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BrokerOffice.Reporting
{
    public partial class FinIncomeStatement : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

    

        public string id = "";
        public string id2 = "";
        protected void Page_Init(object sender, EventArgs e)
        {
            //passreport parameters

            id = Request.QueryString["date"];
            id2 = Request.QueryString["date2"];


            if (this.IsPostBack)
            {
                //TabName.Value = Request.Form[TabName.UniqueID];

            }

            else if (IsPostBack == false)
            {

                //xtraClientList report = new xtraClientList();

                XtraFinIncomeStatement report = new XtraFinIncomeStatement();
                report.Parameters["fDate"].Value = id;
                report.Parameters["tDate"].Value = id2;

                //report.Parameters["AccountNumber"].Value = "";
                //report.Parameters["AccountName"].Value = "";
                //report.Parameters["ReferenceNumber"].Value = "";
                //report.Parameters["Narration"].Value = "";
                //report.Parameters["Type"].Value = "";
                //report.Parameters["yourParameter2"].Value = secondValue;

                ASPxDocumentViewer1.Report = report;
            }
        }

    }
}