﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="NewDealReport.aspx.cs" Inherits="BrokerOffice.Reporting.NewDealReport" %>


<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=12.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

    <div style="margin-left: 5%; margin-right: 10%; background: white" class="form-horizontal">
        <div class="panel-heading" style="background-color: #428bca">
            <l style="color: white">Deal Note</l>

        </div>
        <a class="btn btn-primary" href="../Reporting/AccountAudit">Back To Search
        </a>

    </div>

    <div style="margin-left: 5%; margin-right: 10%; align-content:center; background: white" class="form-horizontal" >
        
        <rsweb:ReportViewer ID="ReportViewer1"  runat="server" Font-Names="Verdana" Font-Size="8pt" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="100%" BorderStyle="Groove" Height="100%" >
            
            <LocalReport ReportPath="Reporting\DealNote.rdlc">
            </LocalReport>
        </rsweb:ReportViewer>


    </div>
      
</asp:Content>
