﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.CrystalReports.Engine;
using BrokerOffice.DAO;

namespace BrokerOffice.Reporting
{
    public partial class GeneralLedgerDetailed : System.Web.UI.Page
    {
        ReportDocument cryRpt = new ReportDocument();

        private SBoardContext db = new SBoardContext();
        public string date = "";

        public string date2 = "";

        public string account = "";

        public string kk = "";

        protected void Page_Init(object sender, EventArgs e)
        {
            //passreport parameters

            date = Request.QueryString["From"];
            date2 = Request.QueryString["To"];
            account = Request.QueryString["Acco"];


            if (this.IsPostBack)
            {
                //TabName.Value = Request.Form[TabName.UniqueID];

            }

            kk = Server.MapPath("DetailedGeneralLedger.rpt");

            cryRpt.Load(kk);
            cryRpt.SetParameterValue("fDate", date);
            cryRpt.SetParameterValue("tDate", date2);

            CrystalReportViewer1.ReportSource = cryRpt;
        }

        protected void Page_Unload(object sender, EventArgs e)
        {
            cryRpt.Close();
            cryRpt.Dispose();
            GC.Collect();
        }

    }
}