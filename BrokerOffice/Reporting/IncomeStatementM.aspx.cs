﻿using DevExpress.XtraReports.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using BrokerOffice.DAO;

namespace BrokerOffice.Reporting
{
    public partial class IncomeStatementM : System.Web.UI.Page
    {
        public string id = "";
        public string from = "";
        public string to = "";
        private SBoardContext db = new SBoardContext();
        protected void Page_Init(object sender, EventArgs e)
        {
            //passreport parameters

            
            from = Request.QueryString["From"];
            to = Request.QueryString["To"];

            if (this.IsPostBack)
            {
                //TabName.Value = Request.Form[TabName.UniqueID];

            }

            else if (IsPostBack == false)
            {
            

                IncomesM report = new IncomesM();
                report.Parameters["TDate"].Value =to;

                report.Parameters["FDate"].Value = from;
                //report.Parameters["yourParameter2"].Value = secondValue;

                ASPxDocumentViewer1.Report = report;
            }
        }

        
    }
}