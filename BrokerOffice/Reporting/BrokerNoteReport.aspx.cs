﻿using BrokerOffice.DAO;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BrokerOffice.Reporting
{
    public partial class BrokerNoteReport: System.Web.UI.Page
    {

        SBoardContext db = new SBoardContext();
       
       public string account = "";
        public string choice= "";

        protected void Page_Init(object sender, EventArgs e)
        {
            //passreport parameters

            
            account = Request.QueryString["Account"];
            //orderno = Request.QueryString["OrderNo"];

            if (this.IsPostBack)
            {
                //TabName.Value = Request.Form[TabName.UniqueID];

            }

            else if (IsPostBack == false)
            {

                try
                {
                    string i = account;
                    var cpz = db.DealerDGs.ToList().Where(a => a.Deal == i);
                    foreach (var f in cpz)
                    {
                        string ac = "";
                        string typo = "";
                        string typo2 = "";
                        if (f.Account1 != "ZSE" || f.Account1 != "")
                        {
                            ac = f.Account1;
                            typo = "BUY";
                            typo2 = "PURCHASE";
                        }
                        if (f.Account2 != "ZSE" || f.Account2 != "")
                        {
                            ac = f.Account2;
                            typo = "SELL";
                            typo2 = "SALE";
                        }
                        XtraDeal report = new XtraDeal();
                        report.Parameters["Account"].Value = ac;

                        report.Parameters["OrderNo"].Value = f.Deal;
                        report.Parameters["SID"].Value = f.Deal;
                        report.Parameters["QTY"].Value = f.Quantity;
                        report.Parameters["Dte"].Value = f.PostDate;
                        report.Parameters["Max"].Value = f.Price;

                        report.Parameters["Broker"].Value = "AKRI";
                        report.Parameters["BrokerName"].Value = ("Akribos Securities").ToUpper();

                        string z = ac;
                        string cf = "";
                        var p = db.Account_Creations.ToList().Where(a => a.CDSC_Number == z);
                        foreach (var v in p)
                        {
                            cf = v.Surname_CompanyName + " " + v.OtherNames;
                            report.Parameters["Address"].Value = v.depname;
                        }
                        report.Parameters["TYPO"].Value = typo;

                        if (typo != "BUY")
                        {
                            report.Parameters["Stamp"].Value = "Stamp duty";
                        }
                        else
                        {
                            report.Parameters["Stamp"].Value = "Zim";
                        }
                        report.Parameters["TYPO2"].Value = typo2;
                        report.CreateDocument();
                        //report2
                        if (f.Account1 != "ZSE" && f.Account2 != "ZSE")
                        {
                            ac = f.Account1;
                            typo = "BUY";
                            typo2 = "PURCHASE";
                            XtraDeal report2 = new XtraDeal();
                            report2.Parameters["Account"].Value = ac;

                            report2.Parameters["OrderNo"].Value = f.Deal;
                            report2.Parameters["SID"].Value = f.Deal;
                            report2.Parameters["QTY"].Value = f.Quantity;
                            report2.Parameters["Dte"].Value = f.PostDate;
                            report2.Parameters["Max"].Value = f.Price;

                            report2.Parameters["Broker"].Value = "AKRI";
                            report2.Parameters["BrokerName"].Value = ("Akribos Securities").ToUpper();

                            z = ac;
                            cf = "";
                            p = db.Account_Creations.ToList().Where(a => a.CDSC_Number == z);
                            foreach (var v in p)
                            {
                                cf = v.Surname_CompanyName + " " + v.OtherNames;
                                report2.Parameters["Address"].Value = v.depname;
                            }
                            report2.Parameters["TYPO"].Value = typo;

                            if (typo != "BUY")
                            {
                                report2.Parameters["Stamp"].Value = "Stamp duty";
                            }
                            else
                            {
                                report2.Parameters["Stamp"].Value = "Zim";
                            }
                            report2.Parameters["TYPO2"].Value = typo2;
                            report2.CreateDocument();

                            int minPageCount = Math.Min(report.Pages.Count, report2.Pages.Count);
                            for (int g = 0; g < minPageCount; g++)
                            {
                                report.Pages.Insert(g * 2 + 1, report2.Pages[g]);
                            }
                            if (report2.Pages.Count != minPageCount)
                            {
                                for (int g = minPageCount; g < report2.Pages.Count; g++)
                                {
                                    report.Pages.Add(report2.Pages[g]);
                                }
                            }

                            // Reset all page numbers in the resulting document. 
                            report.PrintingSystem.ContinuousPageNumbering = true;
                        }

                        ASPxDocumentViewer1.Report = report;
                    }
                }
                catch (Exception)
                {
                    Response.Redirect("~/Account");
                }
            }
        }
        
        public void msgbox(string strMessage)
        {
            string strScript = "<script language=JavaScript>";
            strScript += "window.alert(\"" + strMessage + "\");";
            strScript += "</script>";
            System.Web.UI.WebControls.Label lbl = new System.Web.UI.WebControls.Label();
            lbl.Text = strScript;
            Page.Controls.Add(lbl);
        }

       
    }
}