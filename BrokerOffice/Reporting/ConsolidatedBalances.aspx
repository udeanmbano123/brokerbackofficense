﻿<%@ Page Language="C#" MaintainScrollPositionOnPostback="true" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="ConsolidatedBalances.aspx.cs" Inherits="BrokerOffice.Reporting.ConsolidatedBalances" %>

<%@ Register Assembly="DevExpress.XtraReports.v18.2.Web.WebForms, Version=18.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraReports.Web" TagPrefix="dx" %>


<asp:content id="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="margin-left:5%;margin-right:10%;background:white" class="form-horizontal">
    <div class="panel-heading" style="background-color:#428bca">
        <l style="color:white">Consolidated Balances</l>
       
    </div> 
        </div>
        <dx:ASPxDocumentViewer ID="ASPxDocumentViewer1" runat="server" ReportTypeName="BrokerOffice.Reporting.XtraConsolidateBalances" Theme="Moderno"></dx:ASPxDocumentViewer>
   </asp:content>
