﻿using BrokerOffice.DAO;
using BrokerOffice.Models;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebMatrix.WebData;
using CrystalDecisions.CrystalReports.Engine;

namespace BrokerOffice.Reporting
{
    public partial class FinTrialBalance : System.Web.UI.Page
    {
        ReportDocument cryRpt = new ReportDocument();

        private SBoardContext db = new SBoardContext();
        public string date = "";

        public string date2 = "";

        public string id = "";

        public string kk = "";

        protected void Page_Init(object sender, EventArgs e)
        {
            //passreport parameters
            id = Request.QueryString["id"];
            //date = Request.QueryString["From"];
            //date2 = Request.QueryString["To"];
            //account = Request.QueryString["Acco"];

            


            if (this.IsPostBack)
            {
                //TabName.Value = Request.Form[TabName.UniqueID];

            }

            kk = Server.MapPath("rptTrialBalance.rpt");

            cryRpt.Load(kk);
            cryRpt.SetParameterValue("tDate", id);

            CrystalReportViewer1.ReportSource = cryRpt;
        }

        protected void Page_Unload(object sender, EventArgs e)
        {
            cryRpt.Close();
            cryRpt.Dispose();
            GC.Collect();
        }
    }
}