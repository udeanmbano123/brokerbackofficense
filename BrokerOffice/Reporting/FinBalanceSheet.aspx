﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" MaintainScrollPositionOnPostback="true" AutoEventWireup="true" CodeBehind="FinBalanceSheet.aspx.cs" Inherits="BrokerOffice.Reporting.FinBalanceSheet" %>

<%@ Register Assembly="DevExpress.XtraReports.v18.2.Web.WebForms, Version=18.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraReports.Web" TagPrefix="dx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="margin-left: 5%; margin-right: 10%; background: white" class="form-horizontal">
        <div class="panel-heading" style="background-color: #428bca">
            <l style="color: white">Balance Sheet</l>

        </div>
        <a class="btn btn-primary" href="../Reporting/FinBalanceSheet">Back To Search

        </a>
    </div>
    <dx:ASPxDocumentViewer ID="ASPxDocumentViewer1" runat="server" ReportTypeName="BrokerOffice.Reporting.XtraFinBalanceSheet" Theme="Moderno"></dx:ASPxDocumentViewer>
</asp:Content>
