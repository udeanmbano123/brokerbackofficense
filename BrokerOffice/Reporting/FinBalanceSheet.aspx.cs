﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BrokerOffice.Reporting
{
    public partial class FinBalanceSheet : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }



        public string id = "";
        public string id2 = "";
        protected void Page_Init(object sender, EventArgs e)
        {
            //passreport parameters

           
            id2 = Request.QueryString["id"];


            if (this.IsPostBack)
            {
                //TabName.Value = Request.Form[TabName.UniqueID];

            }

            else if (IsPostBack == false)
            {

                //xtraClientList report = new xtraClientList();

                XtraFinBalanceSheet report = new XtraFinBalanceSheet();
                var firstDate = "1 Jan " + DateTime.Now.Year;
                report.Parameters["fDate"].Value = Convert.ToDateTime(firstDate) ;
                report.Parameters["tDate"].Value = id2;

              

                ASPxDocumentViewer1.Report = report;
            }
        }
    }
}