﻿<%@ Page Language="C#" MaintainScrollPositionOnPostback="true" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="CompanyEarners.aspx.cs" Inherits="BrokerOffice.Reporting.CompanyEarners" %>

<%@ Register Assembly="DevExpress.Web.v18.2, Version=18.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.XtraReports.v18.2.Web.WebForms, Version=18.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraReports.Web" TagPrefix="dx" %>


<asp:content id="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="margin-left:5%;margin-right:10%;background:white" class="form-horizontal">
    <div class="panel-heading" style="background-color:#428bca">
        <l style="color:white">Top 20 Company Earners</l>
       
    </div>
        <dx:ASPxRadioButtonList ID="dxBuySell" runat="server" RepeatDirection="Horizontal" Theme="Metropolis">
               <Items>
                <dx:ListEditItem Text="Buy" Value="Buy" />
                <dx:ListEditItem Text="Sell" Value="Sell" />
                   <dx:ListEditItem Text="Consolidated" Value="Consolidated" />
            </Items>
        </dx:ASPxRadioButtonList>
       <dx:ASPxRadioButtonList ID="dxRange" AutoPostBack="true" runat="server" RepeatDirection="Horizontal" Theme="Metropolis" OnSelectedIndexChanged="dxRange_SelectedIndexChanged">
               <Items>
                      <dx:ListEditItem Text="No Range" Value="No Range" />
                <dx:ListEditItem Text="Range" Value="Range" />
           
            </Items>
        </dx:ASPxRadioButtonList>
        <table>
            <tr>
                <td>
                    <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="From"></dx:ASPxLabel>
  <dx:ASPxDateEdit ID="dtDate" runat="server" Theme="Moderno"></dx:ASPxDateEdit>
                </td>
                <td>
                       <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="To"></dx:ASPxLabel>
 <dx:ASPxDateEdit ID="dxDate2" runat="server" Theme="Moderno"></dx:ASPxDateEdit>
                </td>
            </tr>
        </table>
      
           
        <dx:ASPxButton ID="btSubmit" runat="server" Text="Submit" OnClick="btSubmit_Click" Theme="Moderno"></dx:ASPxButton>

     </div>
    </asp:content>
