﻿using DevExpress.XtraReports.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

namespace BrokerOffice.Reporting
{
    public partial class DrillDownReport : System.Web.UI.Page
    {

        public string id = "";
        public string gen = "";
        protected void Page_Init(object sender, EventArgs e)
        {
            //passreport parameters
            string nm = Request.QueryString["TDate"].ToString();
            id = nm;
            gen = Request.QueryString["General"];

            if (this.IsPostBack)
            {
                //TabName.Value = Request.Form[TabName.UniqueID];

            }
            else if (gen == "Equity")
            {
                Response.Redirect("~/Reporting/DrillDownReport2.aspx?TDate=" + id + "&General=" + gen);
            }  else if (IsPostBack == false && gen == "Equity")
            {
                Response.Redirect("~/Reporting/DrillDownReport2.aspx?TDate='"+ id +"'&General='"+gen);
             }
            else if (IsPostBack == false && gen != "Equity")
            {

                XtraDrill report = new XtraDrill();
                report.Parameters["TDate"].Value = id;
                report.Parameters["Ledger"].Value = gen;
                ASPxDocumentViewer1.Report = report;
            }
          


        }
    }
}