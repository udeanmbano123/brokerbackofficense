﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BrokerOffice.Reporting
{
    public partial class ClientListTop : System.Web.UI.Page
    {
        public string id = "";
        public string id2 = "";
        protected void Page_Init(object sender, EventArgs e)
        {
            //passreport parameters

            id = Request.QueryString["From"];
            id2 = Request.QueryString["To"];


            if (this.IsPostBack)
            {
                //TabName.Value = Request.Form[TabName.UniqueID];

            }

            else if (IsPostBack == false)
            {

                XtraTop20 report = new XtraTop20();
                report.Parameters["FDate"].Value = id;
                report.Parameters["TDate"].Value = id2;
                //report.Parameters["yourParameter2"].Value = secondValue;

                ASPxDocumentViewer1.Report = report;
            }
        }


    }
}