﻿using BrokerOffice.ATS;
using BrokerOffice.DAO;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebMatrix.WebData;

namespace BrokerOffice.Reporting
{
    public partial class OrderBookReport : System.Web.UI.Page
    {
        private SBoardContext db = new SBoardContext();
        public string date = "";
        public string date2 = "";
        public string broker = "";
        protected void Page_Init(object sender, EventArgs e)
        {
            //passreport parameters

            date = Request.QueryString["date"];
            date2= Request.QueryString["date2"];
          

            if (this.IsPostBack)
            {
                //TabName.Value = Request.Form[TabName.UniqueID];

            }

            else if (IsPostBack == false)
            {
                //loaad orders fro webservice
                var c = db.Users.ToList().Where(a => a.Email == WebSecurity.CurrentUserName.ToString());
                foreach(var d in c)
                {
                    broker = d.BrokerCode;
                }
                int z = 0;

                try
                {
                    z = db.TblDealss.ToList().Where(a=>a.BrokerCode==broker).Max(a => a.RefID);

                }
                catch (Exception)
                {

                    z = 0;
                }
               

                
                XtraOrderBook report = new XtraOrderBook();
                report.Parameters["FDate"].Value =date;
                report.Parameters["TDate"].Value = date2;
                report.Parameters["Broker"].Value = broker;
                //report.Parameters["yourParameter2"].Value = secondValue;

                ASPxDocumentViewer1.Report = report;
            }
        }


    }
}