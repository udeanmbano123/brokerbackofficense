﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BrokerOffice.Reporting
{
    public partial class CompanyReportConsolidatedRange : System.Web.UI.Page
    {
        public string date1 = "";
        public string date2 = "";
        public string type = "";
        protected void Page_Init(object sender, EventArgs e)
        {
            //passreport parameters

            date1 = Request.QueryString["date1"];
            date2 = Request.QueryString["date2"];
            type = Request.QueryString["Type"];

            if (this.IsPostBack)
            {
                //TabName.Value = Request.Form[TabName.UniqueID];

            }

            else if (IsPostBack == false)
            {

              XtraCompanyRangeConsolidated report = new XtraCompanyRangeConsolidated();
                report.Parameters["SelDate"].Value =date1;
                report.Parameters["SelDate2"].Value = date2;
                report.Parameters["Type"].Value = type;
                //report.Parameters["yourParameter2"].Value = secondValue;

                ASPxDocumentViewer1.Report = report;
            }
        }


    }
}