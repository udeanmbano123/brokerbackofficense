﻿using BrokerOffice.DAO;
using BrokerOffice.Models;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebMatrix.WebData;

namespace BrokerOffice.Reporting
{
    public partial class BatchReconciliation : System.Web.UI.Page
    {
        private SBoardContext db = new SBoardContext();
      

        public string broker = "";

        protected void Page_Init(object sender, EventArgs e)
        {
            //passreport parameters

            broker = Request.QueryString["Acc"];
          

            if (this.IsPostBack)
            {
                //TabName.Value = Request.Form[TabName.UniqueID];

            }

            else if (IsPostBack == false)
            {
                XtraChargeB report = new XtraChargeB();
                report.Parameters["Acc"].Value = broker;
                ASPxDocumentViewer1.Report = report;
            }
        }


    }
}