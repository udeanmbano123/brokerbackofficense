﻿using BrokerOffice.DAO;
using BrokerOffice.Models;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebMatrix.WebData;

namespace BrokerOffice.Reporting
{
    public partial class BatchReconciliations : System.Web.UI.Page
    {
        private SBoardContext db = new SBoardContext();
      

        public string broker = "";
        public string d1 = "";
        public string d2 = "";

        protected void Page_Init(object sender, EventArgs e)
        {
            //passreport parameters

            broker = Request.QueryString["acc"];
          d1= Request.QueryString["date"];
            d2 = Request.QueryString["date2"];

            if (this.IsPostBack)
            {
                //TabName.Value = Request.Form[TabName.UniqueID];

            }

            else if (IsPostBack == false)
            {
                XtraChargeBC report = new XtraChargeBC();
                report.Parameters["Acc"].Value = broker;
                report.Parameters["FDate"].Value = d1;
                report.Parameters["TDate"].Value = d2;
                ASPxDocumentViewer1.Report = report;
            }
        }


    }
}