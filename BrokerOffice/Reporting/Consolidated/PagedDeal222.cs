﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Drawing.Text;
using System.Web;

namespace BrokerOffice.Reporting.Consolidated
{
    public partial class PagedDeal222 : DevExpress.XtraReports.UI.XtraReport
    {
        public PagedDeal222()
        {
            InitializeComponent();
        }
        static PrivateFontCollection fontCollection;
        public static FontCollection FontCollection
        {
            get
            {
                if (fontCollection == null)
                {
                    fontCollection = new PrivateFontCollection();
                    fontCollection.AddFontFile(HttpContext.Current.Server.MapPath("~/Fonts/Gotham-Light.ttf"));
                }
                return fontCollection;
            }
        }
        private void PagedDeal222_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            PrivateFontCollection fontColl = new PrivateFontCollection();
            fontColl.AddFontFile(HttpContext.Current.Server.MapPath("~/Fonts/Gotham-Light.ttf"));

            PagedDeal222 report = (PagedDeal222)sender;
            foreach (Band b in report.Bands)
            {
                foreach (XRControl c in b.Controls)
                {
                    c.Font = new Font(FontCollection.Families[0], 8F, GraphicsUnit.Point);//new Font(fontColl.Families[0], c.Font.Size, c.Font.Style);
                }
            }


            report.xrLabel3.Font = new Font(FontCollection.Families[0], 8F, GraphicsUnit.Point);

            report.xrLabel5.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);

            report.xrLabel7.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);

            report.xrLabel7.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);

            report.xrLabel22.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);

            report.xrLabel15.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);

            report.xrLabel2.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);

            report.xrLabel1.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);

            report.xrLabel11.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);


            report.xrLabel18.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
            report.xrLabel46.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
            report.xrLabel21.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
            report.xrLabel3.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
            report.xrLabel31.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
            report.xrLabel34.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
            report.xrLabel35.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);


            report.xrLabel36.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
            report.xrLabel26.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
            report.xrLabel42.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
            report.xrLabel13.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
            report.xrLabel10.Font = new Font(FontCollection.Families[0], 8F, GraphicsUnit.Point);
            report.xrLabel43.Font = new Font(FontCollection.Families[0], 8F, GraphicsUnit.Point);
            report.xrLabel45.Font = new Font(FontCollection.Families[0], 8F, GraphicsUnit.Point);
            report.xrLabel33.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
            //report.xrLabel39.Font = new Font(FontCollection.Families[0], 8F, GraphicsUnit.Point);
            report.xrLabel25.Font = new Font(FontCollection.Families[0], 8F, GraphicsUnit.Point);
            report.xrLabel40.Font = new Font(FontCollection.Families[0], 8F, GraphicsUnit.Point);
            report.xrLabel22.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
            report.xrLabel17.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
            report.xrLabel41.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
            report.xrLabel16.Font = new Font(FontCollection.Families[0], 8F, GraphicsUnit.Point);


        }
    }
}
