﻿using BrokerOffice.DAO;
using BrokerOffice.Models;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebMatrix.WebData;
using CrystalDecisions.CrystalReports.Engine;

namespace BrokerOffice.Reporting
{
    public partial class AccStatesReport : System.Web.UI.Page
    {
        ReportDocument cryRpt = new ReportDocument();

        private SBoardContext db = new SBoardContext();
        public string date = "";

        public string date2 = "";

        public string account = "";

        public string kk = "";

        protected void Page_Init(object sender, EventArgs e)
        {
            //passreport parameters

            date = Request.QueryString["From"];
            date2 = Request.QueryString["To"];
            account = Request.QueryString["Acco"];

            var checkBalType = db.tblFinancialAccounts.Where(acc => acc.AccountNumber.ToString() == account).Select(y => y.FinAccountGroupId).FirstOrDefault();
            var balType = db.tblFinAccountGroups.Where(xy => xy.FinAccountGroupId == checkBalType).Select(v=>v.BalanceTypeId). FirstOrDefault();
            

            if (this.IsPostBack)
            {
                //TabName.Value = Request.Form[TabName.UniqueID];

            }

            if (account=="1100")
            {
                if (balType ==2)
                {
                    kk = Server.MapPath("AccountStatementClients.rpt");

                    cryRpt.Load(kk);
                    cryRpt.SetParameterValue("fDate", date);
                    cryRpt.SetParameterValue("tDate", date2);
                    cryRpt.SetParameterValue("Acc", account);
                }
                else
                {
                    kk = Server.MapPath("AccountStatementClientsDr.rpt");

                    cryRpt.Load(kk);
                    cryRpt.SetParameterValue("fDate", date);
                    cryRpt.SetParameterValue("tDate", date2);
                    cryRpt.SetParameterValue("Acc", account);
                }

               
            }
            else
            {
                if (balType == 1)
                {
                    kk = Server.MapPath("AccountStatementDr.rpt");

                    cryRpt.Load(kk);
                    cryRpt.SetParameterValue("fDate", date);
                    cryRpt.SetParameterValue("tDate", date2);
                    cryRpt.SetParameterValue("Acc", account);
                }
                else
                {
                    kk = Server.MapPath("AccountStatementCr.rpt");

                    cryRpt.Load(kk);
                    cryRpt.SetParameterValue("fDate", date);
                    cryRpt.SetParameterValue("tDate", date2);
                    cryRpt.SetParameterValue("Acc", account);
                }
            }

           
           
            CrystalReportViewer1.ReportSource = cryRpt;
        }

        protected void Page_Unload(object sender, EventArgs e)
        {
            cryRpt.Close();
            cryRpt.Dispose();
            GC.Collect();
        }
    }
}