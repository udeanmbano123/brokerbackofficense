﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BrokerOffice.Reporting
{
    public partial class DepositoryReport : System.Web.UI.Page
    {
        public string id = "";
        protected void Page_Init(object sender, EventArgs e)
        {
            //passreport parameters

            id = Request.QueryString["id"];
    


            if (this.IsPostBack)
            {
                //TabName.Value = Request.Form[TabName.UniqueID];

            }

            else if (IsPostBack == false)
            {

                XtraDepository report = new XtraDepository();
                report.Parameters["Script"].Value = id;
                 ASPxDocumentViewer1.Report = report;
            }
        }


    }
}