﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GeneralLedger.aspx.cs" MasterPageFile="~/Site1.Master" Inherits="BrokerOffice.Reporting.GeneralLedger"  %>


<%@ Register assembly="CrystalDecisions.Web, Version=13.0.3500.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" namespace="CrystalDecisions.Web" tagprefix="CR" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <%-- <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>--%>

    <div style="margin-left: 5%; margin-right: 10%; background: white" class="form-horizontal">
        <div class="panel-heading" style="background-color: #428bca">
            
            <l style="color: white">Account Statement</l>

        </div>
        <a class="btn btn-primary" href="../FinancialAccounts/SummarizedGeneralLedger.aspx">Back To Search
        </a>

    </div>

    <div>
        <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" AutoDataBind="true" />
    </div>
    

  
</asp:Content>