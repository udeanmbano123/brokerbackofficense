﻿<%@ Page Language="C#" MaintainScrollPositionOnPostback="true" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="AccAudit.aspx.cs" Inherits="BrokerOffice.Reporting.AccAudit" %>

<%--<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=12.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>--%>

<%@ Register Assembly="DevExpress.XtraReports.v18.2.Web.WebForms, Version=18.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraReports.Web" TagPrefix="dx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <%-- <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>--%>

    <div style="margin-left: 5%; margin-right: 10%; background: white" class="form-horizontal">
        <div class="panel-heading" style="background-color: #428bca">
            <l style="color: white">Account Audit</l>

        </div>
        <a class="btn btn-primary" href="../Reporting/AccountAudit">Back To Search
        </a>

    </div>

   <%-- <div style="margin-left: 5%; margin-right: 10%; align-content:center; background: white" class="form-horizontal" >
        
        <rsweb:ReportViewer ID="ReportViewer1"  runat="server" Font-Names="Verdana" Font-Size="8pt" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="100%" BorderStyle="Groove" Height="100%" >
            
            <LocalReport ReportPath="Reporting\ChargesReport.rdlc">
            </LocalReport>
        </rsweb:ReportViewer>
    </div>--%>
       <dx:ASPxDocumentViewer ID="ASPxDocumentViewer1" runat="server" ReportTypeName="BrokerOffice.Reporting.XtraAudits" Theme="Moderno"></dx:ASPxDocumentViewer>
</asp:Content>
