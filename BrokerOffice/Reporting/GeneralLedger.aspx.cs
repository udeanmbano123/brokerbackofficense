﻿using BrokerOffice.DAO;
using BrokerOffice.Models;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebMatrix.WebData;
using CrystalDecisions.CrystalReports.Engine;

namespace BrokerOffice.Reporting
{
    public partial class GeneralLedger : System.Web.UI.Page
    {

        ReportDocument cryRpt = new ReportDocument();

        private SBoardContext db = new SBoardContext();
        public string date = "";

        public string date2 = "";

        public string account = "";

        public string kk = "";

        protected void Page_Init(object sender, EventArgs e)
        {
            //passreport parameters

            date = Request.QueryString["From"];
            date2 = Request.QueryString["To"];
            account = Request.QueryString["Acco"];


            if (this.IsPostBack)
            {
                //TabName.Value = Request.Form[TabName.UniqueID];

            }

            kk = Server.MapPath("SummarizedGeneralLedger.rpt");

            cryRpt.Load(kk);
            cryRpt.SetParameterValue("fDate", date);
            cryRpt.SetParameterValue("tDate", date2);
            
            CrystalReportViewer1.ReportSource = cryRpt;
        }

        protected void Page_Unload(object sender, EventArgs e)
        {
            cryRpt.Close();
            cryRpt.Dispose();
            GC.Collect();
        }
    }
}