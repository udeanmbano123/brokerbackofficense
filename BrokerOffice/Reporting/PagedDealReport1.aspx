﻿<%@ Page Language="C#" MaintainScrollPositionOnPostback="true" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="PagedDealReport1.aspx.cs" Inherits="BrokerOffice.Reporting.PagedDealReport1" %>

<%@ Register Assembly="DevExpress.Web.v18.2, Version=18.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.XtraReports.v18.2.Web.WebForms, Version=18.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraReports.Web" TagPrefix="dx" %>


<asp:content id="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="margin-left:5%;margin-right:10%;background:white" class="form-horizontal">
    <div class="panel-heading" style="background-color:#428bca">
        <l style="color:white">Day Order Purchase Single Broker Note</l>
       
    </div> 
          <a class="btn btn-primary" href="../PrimaryMarket/DealSelector.aspx">
   Back To Search

</a>
    <dx:ASPxButton ID="ASPxButton1" CssClass="btn btn-primary" runat="server" Text="Email Note" OnClick="ASPxButton1_Click"></dx:ASPxButton>
         
          </div>
        <dx:ASPxDocumentViewer ID="ASPxDocumentViewer1" runat="server" ReportTypeName="BrokerOffice.Reporting.Consolidated.PagedDeal22" Theme="Moderno"></dx:ASPxDocumentViewer>
   </asp:content>
