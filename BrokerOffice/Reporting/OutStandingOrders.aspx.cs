﻿using BrokerOffice.DAO;
using BrokerOffice.Models;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebMatrix.WebData;

namespace BrokerOffice.Reporting
{
    public partial class OutStandingOrders : System.Web.UI.Page
    {
        private SBoardContext db = new SBoardContext();
        public string date = "";

        public string date2 = "";

        public string broker = "";

        protected void Page_Init(object sender, EventArgs e)
        {
            //passreport parameters

            date = Request.QueryString["date"];
            date2 = Request.QueryString["date2"];
      

            if (this.IsPostBack)
            {
                //TabName.Value = Request.Form[TabName.UniqueID];

            }

            else if (IsPostBack == false)
            {
                var c = db.Users.ToList().Where(a => a.Email == WebSecurity.CurrentUserName.ToString());
                foreach (var d in c)
                {
                    broker = d.BrokerCode;
                }
                int z = 0;

                var client = new RestClient("http://localhost/BrokerService");
                var request = new RestRequest("Outstanding/{s}", Method.GET);
                request.AddUrlSegment("s", broker);
                IRestResponse response = client.Execute(request);
                string validate = response.Content;
                List<Order_Lives> dataList = JsonConvert.DeserializeObject<List<Order_Lives>>(validate);

                //db.Database.ExecuteSqlCommand("Truncate table Order_Lives");
                //check if orders are still outstanding
                checkOrders();
                var per = from s in dataList
                          select s;
                foreach (var k in per)
                {
                   

                        Order_Lives my = new Order_Lives();
                        my.OrderNo = k.OrderNo;
                        my.OrderType = k.OrderType;
                        my.Company = k.Company;
                        my.SecurityType = k.SecurityType;
                        my.CDS_AC_No = k.CDS_AC_No;
                        my.Broker_Code = k.Broker_Code;
                        my.Client_Type =k.Client_Type;
                        my.Tax = k.Tax;
                        my.Shareholder = k.Shareholder;
                        my.ClientName =k.ClientName;
                        my.TotalShareHolding = k.TotalShareHolding;
                        my.OrderStatus = k.OrderStatus;
                        my.Create_date = k.Create_date;
                        my.Deal_Begin_Date = k.Deal_Begin_Date;
                        my.Expiry_Date = k.Expiry_Date;
                        my.Quantity = k.Quantity;
                        my.BasePrice =k.BasePrice;
                        my.AvailableShares = k.AvailableShares;
                        my.OrderPref = k.OrderPref;
                        my.OrderAttribute = k.OrderAttribute;
                        my.Marketboard = k.Marketboard;
                        my.TimeInForce =k.TimeInForce;
                        my.OrderQualifier =k.OrderQualifier;
                        my.BrokerRef = k.BrokerRef;

                        my.ContraBrokerId = k.ContraBrokerId;
                        my.MaxPrice = k.MaxPrice;
                        my.MiniPrice = k.MiniPrice;
                        my.Flag_oldorder = k.Flag_oldorder;
                        my.OrderNumber = k.OrderNumber;
                        my.Currency = k.Currency;
                        my.FOK = k.FOK;
                        my.Affirmation = k.Affirmation;
                        db.Order_Livess.AddOrUpdate(my);
                        db.SaveChanges(WebSecurity.CurrentUserName.ToString());
                    }
                

                XtraOutstanding report = new XtraOutstanding();
                report.Parameters["FDate"].Value = date;
                report.Parameters["TDate"].Value =date2;
                report.Parameters["Broker"].Value = broker;
                ASPxDocumentViewer1.Report = report;
            }
        }

        public void checkOrders()
        {
            var dd = db.Order_Livess.ToList();

            foreach(var b in dd)
            {

                performdeletion(b.OrderNo, b.Order_LivesID);

            }
        }

        public void performdeletion(long orderNo,int theid)
        {
            string validate = "";
            var client = new RestClient("http://localhost/BrokerService");
            var request = new RestRequest("CheckOrder/{s}", Method.POST);
            request.AddUrlSegment("s", orderNo.ToString());
            IRestResponse response = client.Execute(request);
            validate = response.Content;


            var Jsonobject = JsonConvert.DeserializeObject<string>(validate);

            validate = Jsonobject.ToString();

            if (validate == "1")
            {
                Order_Lives user = db.Order_Livess.Find(theid);
                db.Order_Livess.Remove(user);
                db.SaveChanges(WebSecurity.CurrentUserName);

            }
        }

    }
}