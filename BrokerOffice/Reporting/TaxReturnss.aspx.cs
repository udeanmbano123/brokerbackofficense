﻿using BrokerOffice.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BrokerOffice.Reporting
{
    public partial class TaxReturnns : System.Web.UI.Page
    {
        SBoardContext db = new SBoardContext();
        public string frmdate = "";
        public string todate = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            frmdate= Request.QueryString["Begin"];
            todate = Request.QueryString["End"];
            if (this.IsPostBack)
            {
                //TabName.Value = Request.Form[TabName.UniqueID];

            }
            else if (IsPostBack == false)
            {
                int ccd = 0;
                
               XtraTaxReturns report = new XtraTaxReturns();
                 report.Parameters["FDate"].Value = frmdate;
                report.Parameters["TDate"].Value = todate;
                ASPxDocumentViewer1.Report = report;
            }
        }
    }
}