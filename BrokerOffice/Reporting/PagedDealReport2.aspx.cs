﻿using BrokerOffice.DAO;
using BrokerOffice.Reporting.Consolidated;
using DevExpress.XtraReports.UI;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Text;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BrokerOffice.Reporting
{
    public partial class PagedDealReport2: System.Web.UI.Page
    {

        SBoardContext db = new SBoardContext();
       
        //public string account = "";
        //public string orderno= "";

        protected void Page_Init(object sender, EventArgs e)
        {
            //passreport parameters

            
            //account = Request.QueryString["Account"];
            //orderno = Request.QueryString["OrderNo"];

            if (this.IsPostBack)
            {
                //TabName.Value = Request.Form[TabName.UniqueID];

            }

            else if (IsPostBack == false)
            {

                try
                {
                    PagedDeal222 report = new PagedDeal222();
                    report.Parameters["AccName"].Value =HttpContext.Current.Session["AccName"].ToString();
                    report.Parameters["AccNum"].Value =HttpContext.Current.Session["AccNum"].ToString();
                    report.Parameters["AccQ"].Value =HttpContext.Current.Session["AccQ"].ToString();
                    report.Parameters["AccBR"].Value = HttpContext.Current.Session["AccBR"].ToString();
                    report.Parameters["DTePost"].Value =HttpContext.Current.Session["DTePost"].ToString();
                    report.Parameters["Price"].Value = HttpContext.Current.Session["Price"].ToString();
                    report.Parameters["sec"].Value = HttpContext.Current.Session["Sec"].ToString();
                    foreach (XRControlStyle style in report.StyleSheet)
                    {
                        style.Font = new Font(FontCollection.Families[0], 8F, GraphicsUnit.Point);
                    }
                    report.xrLabel3.Font = new Font(FontCollection.Families[0], 8F, GraphicsUnit.Point);

                    report.xrLabel5.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);

                    report.xrLabel7.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);

                    report.xrLabel7.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);

                    report.xrLabel22.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);

                    report.xrLabel15.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);

                    report.xrLabel2.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);

                    report.xrLabel1.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);

                    report.xrLabel11.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);


                    report.xrLabel18.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                    report.xrLabel46.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                    report.xrLabel21.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                    report.xrLabel3.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                    report.xrLabel31.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                    report.xrLabel34.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                    report.xrLabel35.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);


                    report.xrLabel36.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                    report.xrLabel26.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                    report.xrLabel42.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                    report.xrLabel13.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                    report.xrLabel10.Font = new Font(FontCollection.Families[0], 8F, GraphicsUnit.Point);
                    report.xrLabel43.Font = new Font(FontCollection.Families[0], 8F, GraphicsUnit.Point);
                    report.xrLabel45.Font = new Font(FontCollection.Families[0], 8F, GraphicsUnit.Point);
                    report.xrLabel33.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                    //report.xrLabel39.Font = new Font(FontCollection.Families[0], 8F, GraphicsUnit.Point);
                    report.xrLabel25.Font = new Font(FontCollection.Families[0], 8F, GraphicsUnit.Point);
                    report.xrLabel40.Font = new Font(FontCollection.Families[0], 8F, GraphicsUnit.Point);
                    report.xrLabel22.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                    report.xrLabel17.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                    report.xrLabel41.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                    report.xrLabel16.Font = new Font(FontCollection.Families[0], 8F, GraphicsUnit.Point);
                    ASPxDocumentViewer1.Report = report;
                }
                catch (Exception)
                {

                    Response.Redirect("~/Account");
                }
            }
        }
        static PrivateFontCollection fontCollection;
        public static FontCollection FontCollection
        {
            get
            {
                if (fontCollection == null)
                {
                    fontCollection = new PrivateFontCollection();
                    fontCollection.AddFontFile(HttpContext.Current.Server.MapPath("~/Fonts/Gotham-Light.ttf"));
                }
                return fontCollection;
            }
        }
        public void sendM(Attachment att, string acc, string accountname)
        {
            // Create a new message and attach the PDF report to it.
            string nn = investormail(acc);
            if (nn != "")
            {
                // Send the e-mail message via the specified SMTP server.
                //SmtpClient smtp = new SmtpClient("smtp.somewhere.com");
                SmtpClient clients = new SmtpClient();
                clients.Port = 587;
                clients.Host = "smtp.gmail.com";
                clients.EnableSsl = true;
                ServicePointManager.ServerCertificateValidationCallback =
    delegate (object s, X509Certificate certificate,
           X509Chain chain, SslPolicyErrors sslPolicyErrors)
    { return true; };
                clients.Timeout = 0;
                clients.DeliveryMethod = SmtpDeliveryMethod.Network;
                clients.UseDefaultCredentials = false;
                clients.Credentials = new System.Net.NetworkCredential("edocwebapp@gmail.com", "escrow123456789");

                MailMessage mail = new MailMessage();
                mail.Attachments.Add(att);

                // Specify sender and recipient options for the e-mail message.
                mail.From = new MailAddress("brokeroffice@escrowgroup.org", "Broker Office");
                mail.To.Add(new MailAddress(investormail(acc), accountname));
                mail.CC.Add(new MailAddress(custodians(acc).Split(',')[1], custodians(acc).Split(',')[0]));
                mail.BodyEncoding = UTF8Encoding.UTF8;
                mail.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                // Specify other e-mail options.
                mail.Subject = "Broker Note";
                mail.Body = "This is a test e-mail message sent by an application.";

                clients.Send(mail);

            }

        }
        public void msgbox(string strMessage)
        {
            string strScript = "<script language=JavaScript>";
            strScript += "window.alert(\"" + strMessage + "\");";
            strScript += "</script>";
            System.Web.UI.WebControls.Label lbl = new System.Web.UI.WebControls.Label();
            lbl.Text = strScript;
            Page.Controls.Add(lbl);
        }

        public string custodians(string acc)
        {
            string validate = "";
            var client = new RestClient("http://localhost/BrokerService");
            var request = new RestRequest("Custodian/{s}", Method.POST);
            request.AddUrlSegment("s", acc);
            IRestResponse response = client.Execute(request);
            validate = response.Content;

            var Jsonobject = JsonConvert.DeserializeObject<string>(validate);

            validate = Jsonobject.ToString();


            return validate;


        }
        public string investormail(string num)
        {
            var p = db.Account_CreationPendingss.ToList().Where(a => a.CDSC_Number == num);
            string mm = "";
            foreach (var d in p)
            {
                mm = d.Emailaddress;
            }
            return mm;
        }

        protected void ASPxButton1_Click(object sender, EventArgs e)
        {
            try
            {

                PagedDeal222 report = new PagedDeal222();
                report.Parameters["AccName"].Value = HttpContext.Current.Session["AccName"].ToString();
                report.Parameters["AccNum"].Value = HttpContext.Current.Session["AccNum"].ToString();
                report.Parameters["AccQ"].Value = HttpContext.Current.Session["AccQ"].ToString();
                report.Parameters["AccBR"].Value = HttpContext.Current.Session["AccBR"].ToString();
                report.Parameters["DTePost"].Value = HttpContext.Current.Session["DTePost"].ToString();
                report.Parameters["Price"].Value = HttpContext.Current.Session["Price"].ToString();
                report.Parameters["sec"].Value = HttpContext.Current.Session["Sec"].ToString();
                foreach (XRControlStyle style in report.StyleSheet)
                {
                    style.Font = new Font(FontCollection.Families[0], 8F, GraphicsUnit.Point);
                }
                report.xrLabel3.Font = new Font(FontCollection.Families[0], 8F, GraphicsUnit.Point);

                report.xrLabel5.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);


                report.xrLabel7.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);

                report.xrLabel7.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);

                report.xrLabel22.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);

                report.xrLabel15.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);

                report.xrLabel2.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);

                report.xrLabel1.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);

                report.xrLabel11.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);


                report.xrLabel18.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                report.xrLabel46.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                report.xrLabel21.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                report.xrLabel3.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                report.xrLabel31.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                report.xrLabel34.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                report.xrLabel35.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                report.xrLabel16.Font = new Font(FontCollection.Families[0], 8F, GraphicsUnit.Point);

                report.xrLabel36.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                report.xrLabel26.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                report.xrLabel42.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                report.xrLabel13.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                report.xrLabel10.Font = new Font(FontCollection.Families[0], 8F, GraphicsUnit.Point);
                report.xrLabel43.Font = new Font(FontCollection.Families[0], 8F, GraphicsUnit.Point);
                report.xrLabel45.Font = new Font(FontCollection.Families[0], 8F, GraphicsUnit.Point);
                report.xrLabel33.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                //report.xrLabel39.Font = new Font(FontCollection.Families[0], 8F, GraphicsUnit.Point);
                report.xrLabel25.Font = new Font(FontCollection.Families[0], 8F, GraphicsUnit.Point);
                report.xrLabel40.Font = new Font(FontCollection.Families[0], 8F, GraphicsUnit.Point);
                report.xrLabel22.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                report.xrLabel17.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);
                report.xrLabel41.Font = new Font(FontCollection.Families[0], 8F, FontStyle.Bold, GraphicsUnit.Point);


                //ASPxDocumentViewer1.Report = report;
                // Create a new memory stream and export the report into it as PDF.
                MemoryStream mem = new MemoryStream();
                report.ExportToPdf(mem);

                // Create a new attachment and put the PDF report into it.
                mem.Seek(0, System.IO.SeekOrigin.Begin);
                Attachment att = new Attachment(mem, "BrokerNote" + DateTime.Now.ToString("dd-MMM-yyyy") + ".pdf", "application/pdf");
                DateTime dte = Convert.ToDateTime(HttpContext.Current.Session["DTePost"].ToString());
                string accnum = HttpContext.Current.Session["AccNum"].ToString();
                string accname = HttpContext.Current.Session["AccName"].ToString();
                var c = db.Account_CreationPendingss.ToList().Where(a => a.CDSC_Number == accnum).Take(1);

                sendM(att, accnum, accname);
                // Close the memory stream.
                mem.Close();
                msgbox("Report has been sent");
            
    }
            catch (Exception)
            {
                msgbox("Emailing report has failed");
            }
        }
    }
}