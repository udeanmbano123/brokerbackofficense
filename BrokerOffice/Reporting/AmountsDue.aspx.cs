﻿using BrokerOffice.DAO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebMatrix.WebData;

namespace BrokerOffice.Reporting
{
    public partial class AmountsDue : System.Web.UI.Page
    {
        private SBoardContext db = new SBoardContext();
        protected void Page_Init(object sender, EventArgs e)
        {
            //passreport parameters




            if (this.IsPostBack)
            {
                //TabName.Value = Request.Form[TabName.UniqueID];

            }else
            {
                ASPxLabel2.Visible = false;
                dxDate2.Visible = false;
            }


        }

        public string SendSMS2()
        {
            string SmsStatusMsg = string.Empty;
            try
            {
                //Sending SMS To User
                WebClient client = new WebClient();
                string URL = "";
               
                    URL = "http://192.168.3.245/BrokerService/api/Deals/AmountsDueRange?date1=" + dtDate.Date.ToString("yyyy-MM-dd") + "&date2=" + dxDate2.Date.ToString("yyyy-MM-dd") + "";
                
              
                SmsStatusMsg = client.DownloadString(URL);
                if (SmsStatusMsg.Contains("<br>"))
                {
                    SmsStatusMsg = SmsStatusMsg.Replace("<br>", ", ");
                }

            }
            catch (WebException e1)
            {
                SmsStatusMsg = e1.Message;
            }
            catch (Exception e2)
            {
                SmsStatusMsg = e2.Message;
            }
            return SmsStatusMsg;

        }
        public string SendSMS()
        {
            string SmsStatusMsg = string.Empty;
            try
            {
                //Sending SMS To User
                WebClient client = new WebClient();
                string URL = "";
               
                 URL = "http://192.168.3.245/BrokerService/api/Deals/AmountsDue?date=" + dtDate.Date.ToString("yyyy-MM-dd") + "";
              

                SmsStatusMsg = client.DownloadString(URL);
                if (SmsStatusMsg.Contains("<br>"))
                {
                    SmsStatusMsg = SmsStatusMsg.Replace("<br>", ", ");
                }

            }
            catch (WebException e1)
            {
                SmsStatusMsg = e1.Message;
            }
            catch (Exception e2)
            {
                SmsStatusMsg = e2.Message;
            }
            return SmsStatusMsg;

        }
        public void msgbox(string strMessage)
        {
            string strScript = "<script language=JavaScript>";
            strScript += "window.alert(\"" + strMessage + "\");";
            strScript += "</script>";
            System.Web.UI.WebControls.Label lbl = new System.Web.UI.WebControls.Label();
            lbl.Text = strScript;
            Page.Controls.Add(lbl);
        }
        protected void btSubmit_Click(object sender, EventArgs e)
        {

            if (dtDate.Date.ToString() == "0001/01/01 12:00:00 AM")
            {
                msgbox("Date must be selected");
                dtDate.Focus();
                return;
            }
            List<BrokerOffice.Models.ClientsDue> dataList;



            try
            {
                if (dxRange.SelectedIndex == 1)
                {

                    if (dtDate.Date.ToString() == "0001/01/01 12:00:00 AM")
                    {
                        msgbox("Date From must be selected");
                        dtDate.Focus();
                        return;
                    }

                    if (dxDate2.Date.ToString() == "0001/01/01 12:00:00 AM")
                    {
                        msgbox("Date To must be selected");
                        dxDate2.Focus();
                        return;
                    }
                    dataList = JsonConvert.DeserializeObject<List<BrokerOffice.Models.ClientsDue>>(SendSMS2());
                }
                else
                {
                    dataList = JsonConvert.DeserializeObject<List<BrokerOffice.Models.ClientsDue>>(SendSMS());
                }
                   


                //truncate table
                string constr2 = ConfigurationManager.ConnectionStrings["SBoardConnection"].ToString(); // connection string
                bool check;
                string col1Value = "";
                using (SqlConnection con2 = new SqlConnection(constr2))
                {
                    con2.Open();
                    SqlCommand cmd =
                        new SqlCommand("Truncate Table ClientsDue", con2);
                    SqlDataReader dr = cmd.ExecuteReader();
                    check = dr.HasRows;

                    con2.Close();
                }


                //   GridView1.DataSource = DerializeDataTable(responJsonText);
                var dbsel = dataList.ToList();

                foreach (var c in dbsel)
                {
                    BrokerOffice.Models.ClientsDue com = new BrokerOffice.Models.ClientsDue();
                    com.names = c.names;
                    com.accountnumber = c.accountnumber;
                    com.amounts = c.amounts;

                    db.ClientsDues.Add(com);

                    //Update table  
                    db.SaveChanges(WebSecurity.CurrentUserName.ToString());
                }
                if (dxRange.SelectedIndex == 1)
                {
                    Response.Redirect("~/Reporting/AmountsDueReportRange.aspx?date1=" + dtDate.Date.ToString("yyyy-MM-dd") + "&date2=" + dxDate2.Date.ToString("yyyy-MM-dd"));

                }
                else
                {
                    Response.Redirect("~/Reporting/AmountsDueReport.aspx?date=" + dtDate.Date.ToString("yyyy-MM-dd"));

                }

            }
            catch (Exception)
            {

                msgbox("No internet connection");
            }
        }
             public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("Local IP Address Not Found!");
        }


        protected void dxRange_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (dxRange.SelectedIndex == 1)
            {
                ASPxLabel2.Visible = true;
                dxDate2.Visible = true;
            }
            else if (dxRange.SelectedIndex == 0)
            {
                ASPxLabel2.Visible = false;
                dxDate2.Visible = false;
            }

        }
    }
    }


