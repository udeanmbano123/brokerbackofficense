using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;


namespace BrokerOffice.Models
{

    [TrackChanges]
    public partial class para_bank
    {
        [Key]
        public int id { get; set; }

      
     
        [DisplayName("BankCode")]
        public string bank { get; set; }
 
        [DisplayName("Bank Name")]
        public string bank_name { get; set; }

 


        public bool? eft { get; set; }
    }
}
