﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BrokerOffice.Models
{
    [TrackChanges]
    public class glyphicon
    {
        public int glyphiconID { get; set; }
        public string glyphiconname { get; set; }
    }
}