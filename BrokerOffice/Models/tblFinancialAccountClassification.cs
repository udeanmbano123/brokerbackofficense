namespace BrokerOffice.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tblFinancialAccountClassification")]
    public partial class tblFinancialAccountClassification
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tblFinancialAccountClassification()
        {
            tblFinancialAccounts = new HashSet<tblFinancialAccount>();
        }

        [Key]
        public int FinancialAccountClassificationId { get; set; }

        [StringLength(50)]
        public string ClassificationName { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblFinancialAccount> tblFinancialAccounts { get; set; }
    }
}
