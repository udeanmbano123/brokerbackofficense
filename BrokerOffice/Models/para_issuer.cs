using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;


namespace BrokerOffice.Models
{

    [TrackChanges]
    public  class para_issuer
    {
        [Key]
        public int? para_issuerID { get; set; }

   
      [Required]
        public string Company { get; set; }



        [DataType(DataType.Date)]
        [Display(Name = "Date Created")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MMM/yyyy}")]
        public DateTime? Date_created { get; set; }

        [Column(TypeName = "numeric")]
        [DefaultValue(0)]
        public decimal? Issued_shares { get; set; }

        public bool? Status { get; set; }


        [StringLength(50)]
        public string registrar { get; set; }

 
        [Required]
        [DisplayName("Address")]
        [DataType(DataType.MultilineText)]
        public string Add_1 { get; set; }

       

        [StringLength(50)]
        [Required]
        public string City { get; set; }

        [StringLength(50)]
        [Required]
        public string Country { get; set; }
        [Required]
        public string State { get; set; }
        [StringLength(50)]
        [Required]
        public string Contact_Person { get; set; }

        [StringLength(50)]
        public string Telephone { get; set; }

        [StringLength(50)]
        public string Cellphone { get; set; }


        [StringLength(12)]
        public string ISIN { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "DateListed")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MMM/yyyy}")]
        public DateTime? date_listed { get; set; }

      
        [StringLength(50)]
        public string email { get; set; }

        [StringLength(50)]
        public string website { get; set; }


        public int StateID { get; set; }
        public int CountryID { get; set; }

        public int CityID { get; set; }
    }
}
