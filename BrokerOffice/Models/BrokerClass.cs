﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BrokerOffice.Models
{
    public class BrokerClass
    {
        [Key]
        public int BrokerClassID { get; set; }
        public string BrokerName { get; set; }

        public string ATPCSD { get; set; }
    }
    public class marketwatch
    {
        public string company { get; set; }
        public int Volume { get; set; }
        public decimal Bid { get; set; }
        public int Volume_Sell { get; set; }
        public decimal Ask { get; set; }
        public string fnam { get; set; }
        public Nullable<double> Last_Traded_Price { get; set; }
        public decimal Lastmatched { get; set; }
        public decimal lastvolume { get; set; }
        public decimal TotalVolume { get; set; }
        public decimal Turnover { get; set; }
        public Nullable<double> Open { get; set; }
        public decimal High { get; set; }
        public decimal Low { get; set; }
        public Nullable<double> Average_Price { get; set; }
        public Nullable<double> Change { get; set; }
        public Nullable<double> percchange { get; set; }
        public string url { get; set; }
        public string url2 { get; set; }
    }
    public class Brokers
    {
        public string Name { get; set; }
        public string CDSNo { get; set; }
        public string Phone { get; set; }
        public string Broker { get; set; }
        public string IDNumber { get; set; }
        public string IDType { get; set; }
        public string AccountType { get; set; }
        public string TradingStatus { get; set; }
    }

    public class Securities
    {
        public string Fnam { get; set; }
        public string Company { get; set; }
    }
    public class Company
    {
        public string company { get; set; }
        public string fnam { get; set; }


    }

    public partial class Client_Companies
    {

        public string Company_name { get; set; }
        public string Company_Code { get; set; }
        public string Company_type { get; set; }
        public string AccountManager { get; set; }
        public string Account_Pass { get; set; }
        public string Adress_1 { get; set; }
        public string Adress_2 { get; set; }
        public string Adress_3 { get; set; }
        public string Adress_4 { get; set; }
        public string adress_5 { get; set; }
        public string contact_email { get; set; }
        public string contact_phone { get; set; }
        public string contact_person { get; set; }
        public string Job { get; set; }
        public Nullable<bool> status { get; set; }
        [Key]
        public int ID { get; set; }
        public string main_branch { get; set; }
        public string main_account { get; set; }
        public string trading_branch { get; set; }
        public string trading_account { get; set; }
        public string main_account_name { get; set; }
        public string trading_account_name { get; set; }
        public string trading_bank { get; set; }
        public string main_bank { get; set; }
        public Nullable<int> settlement_cycle { get; set; }
        public string main_branch_new { get; set; }
        public string trading_branch_new { get; set; }
        public string key_letter { get; set; }
        public string CDS_Number { get; set; }
        public string Broker_Custodian { get; set; }
    }
}