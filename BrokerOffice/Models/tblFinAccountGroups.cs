namespace BrokerOffice.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tblFinAccountGroups
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tblFinAccountGroups()
        {
            tblFinAccountSubGroups = new HashSet<tblFinAccountSubGroups>();
            tblFinancialAccounts = new HashSet<tblFinancialAccount>();
        }

        [Key]
        public int FinAccountGroupId { get; set; }

        [Required]
        [StringLength(50)]
        public string AccountTypeName { get; set; }

        public int BalanceTypeId { get; set; }

        public int FirstNumber { get; set; }

        public int LastNumber { get; set; }

        public virtual tblBalanceType tblBalanceType { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblFinAccountSubGroups> tblFinAccountSubGroups { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblFinancialAccount> tblFinancialAccounts { get; set; }
    }
}
