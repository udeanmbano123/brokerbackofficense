namespace BrokerOffice.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tblBalanceType")]
    public partial class tblBalanceType
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tblBalanceType()
        {
            tblFinAccountGroups = new HashSet<tblFinAccountGroups>();
            tblFinancialAccounts = new HashSet<tblFinancialAccount>();
        }

        [Key]
        public int BalanceTypeId { get; set; }

        [Required]
        [StringLength(50)]
        public string BalanceTypeName { get; set; }

        [Required]
        [StringLength(10)]
        public string Abbreviation { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblFinAccountGroups> tblFinAccountGroups { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblFinancialAccount> tblFinancialAccounts { get; set; }
    }
}
