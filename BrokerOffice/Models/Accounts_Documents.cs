﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BrokerOffice.Models
{
    [TrackChanges]
    public class Accounts_Documents
    {
        [Key]
      public int id { get; set; }
     public string doc_generated { get; set; }
     public string Name { get; set; }
     public string ContentType { get; set; }
     public byte[] Data { get; set; }
    }
}