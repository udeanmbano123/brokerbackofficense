namespace BrokerOffice.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tblClientsAccount")]
    public partial class tblClientsAccount
    {
        [Key]
        public int ClientAccountId { get; set; }

        public int AccountCreationId { get; set; }

        public int FinancialAccountId { get; set; }

        [StringLength(50)]
        public string ClientNumber { get; set; }

        [StringLength(50)]
        public string OtherDetails { get; set; }

        public virtual tblFinancialAccount tblFinancialAccount { get; set; }
    }
}
