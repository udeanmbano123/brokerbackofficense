namespace BrokerOffice.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tblFinancialTransactions
    {
        [Key]
        public int FinancialTransactionId { get; set; }

        public int? TransactionTypeId { get; set; }

        public int? TransationCategoryId { get; set; }

        [Column(TypeName = "date")]
        public DateTime TrxnDate { get; set; }

        [Column(TypeName = "money")]
        public decimal? Amount { get; set; }

        [StringLength(50)]
        public string Description { get; set; }

        [StringLength(50)]
        public string BatchReference { get; set; }

        [StringLength(50)]
        public string Reference { get; set; }

        [StringLength(50)]
        public string CreatedBy { get; set; }

        [Column(TypeName = "date")]
        public DateTime? CreatedDate { get; set; }

        [Column(TypeName = "money")]
        public decimal Debit { get; set; }

        [Column(TypeName = "money")]
        public decimal Credit { get; set; }

        public string DrAccount { get; set; }

        public string CrAccount { get; set; }

        public bool? IsClientAccount { get; set; }

        [Column(TypeName = "date")]
        public DateTime? ModifiedDate { get; set; }

        public string SubAccount { get; set; }

        public string ClientAccount { get; set; }
    }
}
