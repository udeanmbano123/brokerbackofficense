namespace BrokerOffice.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tblFinancialAccount")]
    public partial class tblFinancialAccount
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tblFinancialAccount()
        {
            tblClientsAccounts = new HashSet<tblClientsAccount>();
        }

        [Key]
        public int FinancialAccountId { get; set; }

        [Required]
        [StringLength(50)]
        public string AccountName { get; set; }

        [StringLength(50)]
        public string Description { get; set; }

        public int AccountNumber { get; set; }

        public int? SubAccountNumber { get; set; }

        public int? FinancialAccountClassificationId { get; set; }

        public int? FinAccountSubGroupId { get; set; }

        public int? FinAccountGroupId { get; set; }

        public int? BalanceTypeId { get; set; }

        [Column(TypeName = "date")]
        public DateTime? CreatedDate { get; set; }

        [StringLength(50)]
        public string CreatedBy { get; set; }

        [Column(TypeName = "date")]
        public DateTime? ModifiedDate { get; set; }

        public bool? IsActive { get; set; }

        [StringLength(50)]
        public string OtherDetails { get; set; }

        public virtual tblBalanceType tblBalanceType { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblClientsAccount> tblClientsAccounts { get; set; }

        public virtual tblFinAccountGroups tblFinAccountGroup { get; set; }

        public virtual tblFinAccountSubGroups tblFinAccountSubGroup { get; set; }

        public virtual tblFinancialAccountClassification tblFinancialAccountClassification { get; set; }
    }
}
