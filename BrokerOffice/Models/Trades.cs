﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BrokerOffice.Models
{
    public class Trades
    {
        [Key]
        public int TradeID { get; set; }
        public DateTime tradedate { get; set; }
        public string buyercds { get; set; }
        public string sellercds { get; set; }
        public string securitytype { get; set; }
        public string issuer { get; set; }
        public string isin { get; set; }

        public decimal tradeprice { get; set; }
        public int tradequantity{get;set;}
        public string brokerrole { get; set; }
        public string buyorderref { get; set; }
        public string sellorderref { get; set; }

    }
}