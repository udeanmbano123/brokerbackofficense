namespace BrokerOffice.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tblTransactionTypes
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tblTransactionTypes()
        {
            tblFinancialTransactions = new HashSet<tblFinancialTransactions>();
        }

        [Key]
        public int TransactionTypeId { get; set; }

        [Required]
        [StringLength(50)]
        public string TransactionTypeName { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblFinancialTransactions> tblFinancialTransactions { get; set; }
    }
}
