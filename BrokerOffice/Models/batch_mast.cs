﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BrokerOffice.Models
{
    public  class batch_mast
    {

        [Key]
        public int ID { get; set; }
        [Required]
        [DisplayName("Batch Number")]
        public string batch_no { get; set; }
        public string batch_type { get; set; }
        [Required]
        [DisplayName("Shares")]
        public Nullable<decimal> app_shares { get; set; }
        [Required]
        [DisplayName("Amount")]
        public Nullable<decimal> app_cash { get; set; }
     
        public string status { get; set; }
       [Required]
        public string Company { get; set; }
        [Required]
        public string Agent { get; set; }
        public string BrokerBatchRef { get; set; }
        public Nullable<bool> Verify { get; set; }
        [Required]
         public string CreatedBy { get; set; }
        [Required]
        [DisplayName("Applications")]
        public string apps { get; set; }
        public string branch { get; set; }
        public Nullable<System.DateTime> datecreated { get; set; }
        [DisplayName("AuthorisedBy")]
        public string PayMode { get; set; }
        [DisplayName("AuthorizedDate")]
        public Nullable<System.DateTime> dateauth { get; set; }
        public Nullable<decimal> BoxNo { get; set; }
        public string OldBatchRef { get; set; }
        public string Source { get; set; }
        public string WebResponse { get; set; }
    }
}