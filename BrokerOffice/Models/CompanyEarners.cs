﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BrokerOffice.Models
{
    public class CompanyEarners
    {
        [Key]
        public int id { get; set; }
        public string companyname { get; set; }
        public string brokername { get; set; }
        public decimal consideration { get; set; }
        public decimal commission { get; set; }
        public string tradeType { get; set; }
    }
}