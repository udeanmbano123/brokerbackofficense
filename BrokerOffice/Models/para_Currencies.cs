﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BrokerOffice.Models
{
    [TrackChanges]
    public class para_Currencies
    {
        [Key]
        public int id { get; set; }
        public string CurrencyCode { get; set; }
        public string CurrencyName { get; set; }
        public string CurrencySymbol { get; set; }
        public string InternationalSTD { get; set; }
        public string CurrencyStatus { get; set; }
        public string Country { get; set; }
        public string Pref { get; set; }
      
    }
}