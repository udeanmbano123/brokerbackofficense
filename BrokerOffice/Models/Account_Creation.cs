using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BrokerOffice.Models
{

    [TrackChanges]
    public class Account_Creation
    {
        [Key]
        public int ID_ { get; set; }

        public string RecordType { get; set; }

        [StringLength(255)]
        public string ClientSuffix { get; set; }

        [StringLength(255)]
        public string JointAcc { get; set; }

        [StringLength(255)]
        public string Identification { get; set; }

        [StringLength(255)]
        public string Title { get; set; }

        [StringLength(255)]
        public string Initials { get; set; }

        [StringLength(255)]
        public string OtherNames { get; set; }

        [StringLength(255)]
        public string Surname_CompanyName { get; set; }

        [StringLength(255)]
        public string Address1 { get; set; }

        [StringLength(255)]
        public string Address2 { get; set; }

        [StringLength(255)]
        public string Address3 { get; set; }

        [StringLength(255)]
        public string Town { get; set; }

        [StringLength(255)]
        public string PostCode { get; set; }


        [StringLength(255)]
        public string FaxNumber { get; set; }

        [StringLength(255)]
        public string Emailaddress { get; set; }

        [Column(TypeName = "date")]

        public DateTime? DateofBirth_Incorporation { get; set; }

        [StringLength(255)]
        public string Gender { get; set; }

        [StringLength(255)]
        public string Nationality { get; set; }

        [StringLength(255)]
        public string Resident { get; set; }

        [DisplayName("Tax")]
        public decimal? TaxBracket { get; set; }

        [StringLength(255)]
        public string TelephoneNumber { get; set; }

        [StringLength(255)]
        public string Bank { get; set; }

        [StringLength(255)]
        public string Branch { get; set; }





        [StringLength(255)]
        public string ClientType { get; set; }



        [StringLength(255)]
        [DisplayName("CDS Number")]
        public string CDSC_Number { get; set; }

        public int? Notification_Sent { get; set; }

        [StringLength(255)]
        public string Callback_Endpoint { get; set; }

        [StringLength(255)]
        [DisplayName("Account Type")]
        public string MNO_ { get; set; }

        public DateTime? Date_Created { get; set; }

        public int? File_Sent_DirectDebt { get; set; }

        public int? File_Sent_AccountCr { get; set; }

        [StringLength(255)]
        public string PIN_No { get; set; }

        [StringLength(255)]
        public string Middlename { get; set; }

        [StringLength(255)]
        [DisplayName("Registration Number")]
        public string RNum { get; set; }
        [StringLength(100)]
        public string MobileNumber { get; set; }

        public string Bankname { get; set; }

        public string Bankcode { get; set; }
        public string BranchName { get; set; }
        public string BranchCode { get; set; }
        [StringLength(255)]
        public string Accountnumber { get; set; }
        public string idtype { get; set; }
        public string accountcategory { get; set; }
        public string country { get; set; }
        public string currency { get; set; }
        public string divpayee { get; set; }
        public string divbank { get; set; }
        public string divbankcode { get; set; }
        public string divbranch { get; set; }
        public string divbranchcode { get; set; }
        public string divacc { get; set; }
        public string divaccounttype
        {
            get; set;
        }
        public string idtype2 { get; set; }
        public string dividnumber { get; set; }
        public string mobile_money { get; set; }
        [DisplayName("Depository Name")]
        public string depname { get; set; }
        [DisplayName("Depository Code")]
        public string depcode { get; set; }


        public string manBank { get; set; }
        public string manBankCode { get; set; }

        public string manBranch { get; set;}
        public string manBranchCode { get; set; }

        public string manAccount{get;set;}

        public string manNames { get; set;}

        public string manAddress { get; set; }

        public string mobilewalletnumber { get; set; }

        public string Broker { get; set; }

        public string CreatedBy { get; set; }

        public Boolean StatuSActive { get; set; }
    }

    [TrackChanges]
    public class Account_CreationPending
    {
        [Key]
        public int ID_ { get; set; }

        public string RecordType { get; set; }

        [StringLength(255)]
        public string ClientSuffix { get; set; }

        [StringLength(255)]
        public string JointAcc { get; set; }

        [StringLength(255)]
        public string Identification { get; set; }

        [StringLength(255)]
        public string Title { get; set; }

        [StringLength(255)]
        public string Initials { get; set; }

        [StringLength(255)]
        public string OtherNames { get; set; }

        [StringLength(255)]
        [DisplayName("Surname/Company")]
        public string Surname_CompanyName { get; set; }

        [StringLength(255)]
        public string Address1 { get; set; }

        [StringLength(255)]
        public string Address2 { get; set; }

        [StringLength(255)]
        public string Address3 { get; set; }

        [StringLength(255)]
        public string Town { get; set; }

        [StringLength(255)]
        public string PostCode { get; set; }


        [StringLength(255)]
        public string FaxNumber { get; set; }

        [StringLength(255)]
        public string Emailaddress { get; set; }

        [Column(TypeName = "date")]
      
        public DateTime? DateofBirth_Incorporation { get; set; }

        [StringLength(255)]
        public string Gender { get; set; }

        [StringLength(255)]
        public string Nationality { get; set; }

        [StringLength(255)]
        public string Resident { get; set; }

        [DisplayName("Tax")]
        public decimal? TaxBracket { get; set; }

        [StringLength(255)]
        public string TelephoneNumber { get; set; }

        [StringLength(255)]
        public string Bank { get; set; }

        [StringLength(255)]
        public string Branch { get; set; }





        [StringLength(255)]
        public string ClientType { get; set; }



        [StringLength(255)]
        [DisplayName("CDS Number")]
        public string CDSC_Number { get; set; }

        public int? Notification_Sent { get; set; }

        [StringLength(255)]
        public string Callback_Endpoint { get; set; }

        [StringLength(255)]
        [DisplayName("Account Type")]
        public string MNO_ { get; set; }

        public DateTime? Date_Created { get; set; }

        public int? File_Sent_DirectDebt { get; set; }

        public int? File_Sent_AccountCr { get; set; }

        [StringLength(255)]
        public string PIN_No { get; set; }

        [StringLength(255)]
        public string Middlename { get; set; }
        [DisplayName("Registration Number")]
        [StringLength(255)]
        public string RNum { get; set; }
        [StringLength(100)]
        public string MobileNumber { get; set; }

        public string Bankname { get; set; }

        public string Bankcode { get; set; }
        public string BranchName { get; set; }
        public string BranchCode { get; set; }
        [StringLength(255)]
        public string Accountnumber { get; set; }
        public string idtype { get; set; }
        public string accountcategory { get; set; }
        public string country { get; set; }
        public string currency { get; set; }
        public string divpayee { get; set; }
        public string divbank { get; set; }
        public string divbankcode { get; set; }
        public string divbranch { get; set; }
        public string divbranchcode { get; set; }
        public string divacc { get; set; }
        public string divaccounttype
        {
            get; set;
        }
        public string idtype2 { get; set; }
        public string dividnumber { get; set; }
        public string mobile_money { get; set; }

        public string depname { get; set; }
        public string depcode { get; set; }

 

        public string manBank { get; set; }
        public string manBankCode { get; set; }

        public string manBranch { get; set; }
        public string manBranchCode { get; set; }

        public string manAccount { get; set; }

        public string manNames { get; set; }

        public string manAddress { get; set; }

        public string mobilewalletnumber { get; set; }

        public string Broker { get; set; }

        public string update_type { get; set; }

        public string ModifiedBy {get; set; }

        public string CreatedBy { get; set; }
        public Boolean StatuSActive { get; set; }
    }

    public partial class para_Billing
    {
        public string ChargeCode { get; set; }
        public string ChargeName { get; set; }
        public Nullable<double> PercentageOrValue { get; set; }
        public string ApplyTo { get; set; }
        public string Indicator { get; set; }
        public int ID { get; set; }
    }

    public partial class Accounts_Joint
    {
        [Key]
        public int id { get; set; }
        public string Surname { get; set; }
        public string Forenames { get; set; }
        public string IDNo { get; set; }
        public string IDType { get; set; }
        public string Nationality { get; set; }
        public Nullable<System.DateTime> DateOfBirth { get; set; }
        public string Gender { get; set; }
        public string CDSNo { get; set; }
        public string email { get; set; }
   }
}
