﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BrokerOffice.Models
{
    public class region
    {
        [Key]
      public int id { get; set; }
      public string name { get; set; }
     public string code { get; set; }
     public string country_id { get; set; }
    }
}