﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BrokerOffice.Models
{
    [TrackChanges]
    public class Accounts_Master
    {
        [Key]
        public int Accounts_MasterID { get; set; }
        [DisplayName("Account Name")]
        public string AccountName { get; set; }
       
        [DisplayName("Account Number")]
        public int AccountNumber { get; set; }

        public int? GL_GroupID { get; set; }
       
         public string GroupName { get; set; }
        [DataType(DataType.MultilineText)]
        [DisplayName("Description")]
        public string Description { get; set; }

        public string Code { get; set; }
        public string FinancialStatement { get; set; }

        public string GeneralLedgerAccount { get; set; }

        public string DebitOrCredit { get; set; }
    }
    [TrackChanges]
    public class FinancialStatements
    {
        [Key]
        public int FinancialStatementsID { get; set; }
        public string Name { get; set; }
    }

    [TrackChanges]
    public class GeneralLedgerAcc
    {
        [Key]
        public int GeneralLedgerAccID { get; set; }
        public string Name { get; set; }
    }

    [TrackChanges]
    public  class Accounts_Documents
    {
        [Key]
       public int Accounts_DocumentsID { get; set; }
        public string doc_generated { get; set; }

        [StringLength(255)]
        public string Name { get; set; }

        [StringLength(255)]
        public string ContentType { get; set; }

        public byte[] Data { get; set; }
    }

    public class ACN
    {
        
        public string AccountName { get; set; }

        [DisplayName("Account Number")]
        public int AccountNumber { get; set; }
        public string GroupName { get; set; }

    }
}