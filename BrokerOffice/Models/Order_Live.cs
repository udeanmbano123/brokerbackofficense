using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BrokerOffice.Models
{

    [TrackChanges]
    public class Order_Live
    {
        [Key]
        public long OrderNo { get; set; }

      
        [DisplayName("Order Type")]
        public string OrderType { get; set; }

      
        [DisplayName("Company")]
        public string Company { get; set; }

     
        [DisplayName("Security type")]
        public string SecurityType { get; set; }

   
        [DisplayName("Customer Number")]
        public string CDS_AC_No { get; set; }

        
        public string Broker_Code { get; set; }

  
        public string Client_Type { get; set; }
        [DefaultValue(0)]
        public decimal? Tax { get; set; }
        
        public string Shareholder { get; set; }


        public string ClientName { get; set; }

        public decimal? TotalShareHolding { get; set; }

        
        public string OrderStatus { get; set; }

        public DateTime? Create_date { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]

        public DateTime? Deal_Begin_Date { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]

        public DateTime? Expiry_Date { get; set; }
       
        public int? Quantity { get; set; }

        [Column(TypeName = "money")]
        
        public decimal? BasePrice { get; set; }

        public decimal? AvailableShares { get; set; }

     
        public string OrderPref { get; set; }

       
        public string OrderAttribute { get; set; }

      
        public string Marketboard { get; set; }

  
        public string TimeInForce { get; set; }

     
        public string OrderQualifier { get; set; }

       
        public string BrokerRef { get; set; }

     
        public string ContraBrokerId { get; set; }

        [Column(TypeName = "money")]
        [DefaultValue(0)]
        public decimal? MaxPrice { get; set; }

        [Column(TypeName = "money")]
        [DefaultValue(0)]
        public decimal? MiniPrice { get; set; }

        public bool? Flag_oldorder { get; set; }

      
        public string OrderNumber { get; set; }

     
        public string Currency { get; set; }

        public int CompanyID { get; set; }
        public string CompanyISIN { get; set; }

        public string TP { get; set; }
        public string TPBoard { get; set; }

        public string mobile { get; set; }

        public string PostedBy { get; set; }

        public string WebServiceID { get; set; }

    }
    public class Pre_Order
    {
        [Key]
        public int ID { get; set; }
        public long OrderNo { get; set; }
        public string OrderType { get; set; }
        public string Company { get; set; }
        public string SecurityType { get; set; }
        public string CDS_AC_No { get; set; }
        public string Broker_Code { get; set; }
        public string Client_Type { get; set; }
        public Nullable<decimal> Tax { get; set; }
        public string Shareholder { get; set; }
        public string ClientName { get; set; }
        public Nullable<int> TotalShareHolding { get; set; }
        public string OrderStatus { get; set; }
        public Nullable<System.DateTime> Create_date { get; set; }
        public Nullable<System.DateTime> Deal_Begin_Date { get; set; }
        public Nullable<System.DateTime> Expiry_Date { get; set; }
        public Nullable<int> Quantity { get; set; }
        public Nullable<decimal> BasePrice { get; set; }
        public Nullable<int> AvailableShares { get; set; }
        public string OrderPref { get; set; }
        public string OrderAttribute { get; set; }
        public string Marketboard { get; set; }
        public string TimeInForce { get; set; }
        public string OrderQualifier { get; set; }
        public string BrokerRef { get; set; }
        public string ContraBrokerId { get; set; }
        public Nullable<decimal> MaxPrice { get; set; }
        public Nullable<decimal> MiniPrice { get; set; }
        public Nullable<bool> Flag_oldorder { get; set; }
        public string OrderNumber { get; set; }
        public string Currency { get; set; }
        public Nullable<bool> FOK { get; set; }
        public Nullable<bool> Affirmation { get; set; }
        public string trading_platform { get; set; }
    }
    [TrackChanges]
    public class BrokerLimit
    {
        [Key]
        public int BrokerLimitID { get; set; }
        [Required]
        [Column(TypeName = "money")]
        public decimal BuyLimit { get; set; }
        [Required]
        [Column(TypeName = "money")]
        public decimal SellLimit { get; set; }
        [Required]
        public string reaction { get; set; }

    }
}
