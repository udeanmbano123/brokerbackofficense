﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace BrokerOffice.Models
{
    public class notification
    {
        public int id { get; set; }
        [DisplayName("session User")]
        public int userid { get; set; }
        [DisplayName("Notification Type")]
        public string notification_type { get; set; }
        [DisplayName("Alert Message")]
        public string notification_data { get; set; }
        [DisplayName("Current Read")]
        public DateTime? timestamp { get; set; }
        [DisplayName("Last Viewed")]
        public string last_read { get; set; }
        [DisplayName("From System")]
        public string fromsysytem { get; set; }
    }
}