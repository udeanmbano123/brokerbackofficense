﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BrokerOffice.Models
{
    [TrackChanges]
    [Table("Modules")]
    public class Modules
    {
        [Key]
        public int ModulesID { get; set; }
        [Required]
        public string ModulesName { get; set; }
        [Required]
        public int? RoleID { get; set; }

        public string glyphicon { get; set; }

        [DisplayName("ControllerName")]
        public string ControllerName { get; set; }

        [DisplayName("ActionName")]
        public string ViewName { get; set; }
        [Required]
        [DisplayName("Alias")]
        public string Name { get; set; }
        public Boolean IsWebForm { get; set; }
        [DisplayName("WebFormUrl")]
        public string webFormUrl { get; set; }
        [ForeignKey("RoleID")]
        public virtual Role Roles { get; set; }
        [DisplayName("Ranking")]
        public int MenuRank { get; set; }
    }
}