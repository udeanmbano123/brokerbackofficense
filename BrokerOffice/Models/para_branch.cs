using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BrokerOffice.Models
{
    [TrackChanges]
    public partial class para_branch
    {

        [Key]
        public int id { get; set; }
     
        [StringLength(50)]
        [DisplayName("Bank")]
        public string bank { get; set; }

        [StringLength(50)]
        [DisplayName("BranchCode")]
        public string branch { get; set; }
        [StringLength(50)]
        [DisplayName("Branch Name")]
        public string branch_name { get; set; }

       
    }
}
