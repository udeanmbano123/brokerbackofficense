﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BrokerOffice.Models
{
    [TrackChanges]
    public class GL_Group
    {
        [Key]
        public int GL_GroupID { get; set; }
        [Display(Name = "Group Name")]
        [Required]
        public string GroupName { get; set; }
        [Display(Name = "Starting Account")]
        [Required]
        public int StartingAccount { get; set; }
        [Display(Name = "Ending Account")]
        [Required]
        public int EndingAccount { get; set; }

    }

    public class div_types
    {
        [Key]
        public int div_typesID { get; set; }
    
        public string DivType { get; set; }
      
      
        
        public string DiviTypeName { get; set; }
    }
}