﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BrokerOffice.Models
{
    [TrackChanges]
    public class ActionFormcs
    {
        [Key]
        public int ActionFormID { get; set; }
        public int ClientPortfoliosID { get; set; }
        [DefaultValue("None")]
        public string ClientNumber{get;set;}
        [DefaultValue("None")]
        public string ClientNames { get; set; }
        [DefaultValue(0)]
        public double clientholdings { get; set; }
       
        public DateTime? Date { get; set; }
        [DefaultValue("None")]
        public string hour { get; set; }
        [DefaultValue("None")]
        public string minutes { get; set; }
        [DefaultValue("None")]
        public string setting { get; set; }
        [DefaultValue("None")]
        public string contactnotes { get; set; }
        [DefaultValue("None")]
        public string adviceclient { get; set; }
        [DefaultValue("None")]
        public string contacttype { get; set; }
    }
    public class TR
    {
        [Column(TypeName = "money")]
        public Decimal Mo { get; set; }
    }
    public class TRP
    {
        public int Mo { get; set; }
    }
}