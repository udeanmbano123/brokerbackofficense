namespace BrokerOffice.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tblFinAccountSubGroups
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tblFinAccountSubGroups()
        {
            tblFinancialAccounts = new HashSet<tblFinancialAccount>();
        }

        [Key]
        public int FinAccountSubGroupId { get; set; }

        public int FinAccountGroupId { get; set; }

        [Required]
        [StringLength(50)]
        public string GroupName { get; set; }

        public virtual tblFinAccountGroups tblFinAccountGroup { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblFinancialAccount> tblFinancialAccounts { get; set; }
    }
}
