﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BrokerOffice.Models
{
    [TrackChanges]
    public class Agent
    {
        [Key]
        public int AgentID { get; set; }
        [DisplayName("AgentCode")]
        
        public string AgentCode { get; set; }
        [DisplayName("AgentName")]
        [Required]
        public string AgentName { get; set; }
        [DisplayName("PracticingID")]
        public string PracticingID { get; set; }
        [Required(ErrorMessage = "eMail is required")]
        [StringLength(35, ErrorMessage = "eMail Length Should be less than 35")]
        [RegularExpression(@"^[\w-]+(\.[\w-]+)*@([a-z0-9-]+(\.[a-z0-9-]+)*?\.[a-z]{2,6}|(\d{1,3}\.){3}\d{1,3})(:\d{4})?$", ErrorMessage = "eMail is not in proper format")]
        [DataType(DataType.EmailAddress)]
        [DisplayName("EmailAddress")]
        public String Email { get; set; }
        [DisplayName("AgentAddress")]
        [DataType(DataType.MultilineText)]
        [Required]
        public string address { get; set; }
        [DisplayName("MobileNumber")]
        public string mobilenumber { get; set; }
        [DisplayName("TelephoneNumber")]
        public string telephone{get;set;}

        [DefaultValue("Temp")]
        public string status { get; set; }

        [Required]
        public string AgentType { get; set; }
 
    }
}