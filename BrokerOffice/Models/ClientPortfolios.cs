﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BrokerOffice.Models
{
    [TrackChanges]
    public class ClientPortfolios
    {
        [Key]
        public int ClientPortfoliosID { get; set; }

        [Required]
        [DisplayName("Client Number")]
        public string ClientNumber { get; set; }
        [Required]
        [DisplayName("Stock")]
        public string Stock { get; set; }
        [Required]
       [DisplayName("Holdings")]
        public decimal Holdings { get; set; }
        [DisplayName("Stock")]
        public string Stockf { get; set; }
        [DisplayName("clientf")]
        public string Clientf { get; set; }
    }
}