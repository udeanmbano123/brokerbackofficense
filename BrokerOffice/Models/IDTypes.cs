﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BrokerOffice.Models
{
    public class IDTypes
    {
        [Key]
        public int IDTypesID { get; set; }
        public string idname { get; set; }
    }
}