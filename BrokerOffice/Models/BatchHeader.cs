﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BrokerOffice.Models
{
    public class BatchHeader
    {
        [Key]
        public int id { get; set; }
        public string BatchNo { get; set; }
        public string BatchType { get; set; }
        [Column(TypeName = "money")]
      
        [DisplayName("Amount")]
        public decimal Amount { get; set; }
        [DisplayName("Number Of Transactions")]
        
        public int NumberOfTrxns { get; set; }
        public string CreatedBy { get; set; }
        public DateTime ? DateCreated { get; set; }
        public bool Status { get; set; }
        public bool Auth { get; set; }
        public string BatchName { get; set; }
     
        public DateTime BatchDate { get; set; }
    }
    public class BatchTransaction
    {
        [Key]
        public int ID { get; set; }
        public DateTime TrxnDate { get; set; }
 
        public string Reference { get; set; }
        public string Description { get; set; }
        [Column(TypeName = "money")]
        [Required]
        public decimal ?Amount { get; set; }
        public string BankID { get; set; }
        public string BankAccName { get; set; }
        public string BatchNo { get; set; }
        [DisplayName("Credit Account Name")]
       public string AccountName { get; set; }
        [DisplayName("Credit Account")]
        public string Account { get; set; }
        [DisplayName("Debit Accunt Name")]
        public string AccountNameD { get; set; }
        [DisplayName("Debit Account")]
        public string AccountD { get; set; }
        public bool Commit { get; set; }
        public bool Auth { get; set; }
        public string  trantype {get;set;}
        public string trantypeD { get; set; }
        public string filename { get; set; }
    }
}
