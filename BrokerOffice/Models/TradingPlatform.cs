﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BrokerOffice.Models
{
    public class TradingPlatform
    {
        [Key]
        public int TradingPlatformID { get; set; }
        [Required]
        public string Code { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Country { get; set; }
        [Required]
        public string City { get; set; }
        [DataType(DataType.MultilineText)]
       [Required]
        public string Address { get; set; }

        public string PhoneNumber { get; set; }
        [DisplayName("Contact Person")]
        public string ContactPerson { get; set; }
        
        [Display(Name = "Province")]
        public string State { get; set; }
        public int StateID { get; set; }
        public int CountryID { get; set; }

        public int CityID { get; set; }

        public string Depository { get; set; }
        public string TradeBoard { get; set; }
        public int? TradingBoardID { get; set; }
        [ForeignKey("TradingBoardID")]
       public virtual TradingBoard TradingBoards { get; set; }

    }
}
