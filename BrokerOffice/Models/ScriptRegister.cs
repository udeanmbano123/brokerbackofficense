﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BrokerOffice.Models
{
    public class ScriptRegister
    {
        [Key]
        public int ScriptRegisterID { get; set; }
        [Required]
        public string Exchange { get; set; }
        [Required]
        public string ClientNames { get; set; }
        [Required]
        public string ClientNumber { get; set; }
        [Required]
        public string Counter { get; set; }
        [Required]
        public string CertificateNumber { get; set; }
        [Required]
        public decimal NumberShares { get; set; }
        
        public string Status { get; set; }


    }
}