﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BrokerOffice.Models
{
    public class WindowsService
    {

        public int WindowsServiceID { get; set; }

        public string AccountName { get; set; }

        public string AccountNumber { get; set; }

        public string Action { get; set; }
    }
}