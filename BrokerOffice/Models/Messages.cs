﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BrokerOffice.Models
{
    public class Messages
    {
        [Key]
        public int MessagesID { get; set; }
        [DisplayName("Subject")]
        public string Subject { get; set; }
        [DisplayName("Messages")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }
        [DisplayName("ToUser")]
        public string User { get; set; }
        public string UserID { get; set; }
        public string fromUser { get; set; }
        public string fromUserID { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MMM/yyyy}")]
        [Required]
        public DateTime DateAdded { get; set; }
    }
}