﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BrokerOffice.DealClass
{
    public class tblMatchedOrders
    {
        [Column(TypeName = "numeric")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [StringLength(50)]
        public string TimeStamp { get; set; }

        [Column(TypeName = "money")]
        public decimal? TradePrice { get; set; }

        public double? TradeQty { get; set; }

        [StringLength(50)]
        public string ReportID { get; set; }

        [StringLength(50)]
        public string Side1 { get; set; }

        [StringLength(50)]
        public string OrderID1 { get; set; }

        [StringLength(50)]
        public string PartyID1 { get; set; }

        [StringLength(50)]
        public string Source1 { get; set; }

        [StringLength(50)]
        public string Role1 { get; set; }

        [StringLength(50)]
        public string Account1 { get; set; }

        [StringLength(50)]
        public string Side2 { get; set; }

        [StringLength(50)]
        public string OrderID2 { get; set; }

        [StringLength(50)]
        public string PartyID2 { get; set; }

        [StringLength(50)]
        public string Source2 { get; set; }

        [StringLength(50)]
        public string Role2 { get; set; }

        [StringLength(50)]
        public string Account2 { get; set; }

        [StringLength(50)]
        public string BankSent { get; set; }

        [StringLength(50)]
        public string BankRef { get; set; }

        [StringLength(50)]
        public string AccRef1 { get; set; }

        [StringLength(50)]
        public string AccRef2 { get; set; }

        [StringLength(50)]
        public string Broker1 { get; set; }

        [StringLength(50)]
        public string Broker2 { get; set; }

        [StringLength(50)]
        public string Ack { get; set; }

        [StringLength(500)]
        public string Error_details { get; set; }

        [StringLength(50)]
        public string CommonRef { get; set; }

        [Column(TypeName = "date")]
        public DateTime? Date_posted { get; set; }

        public int? SettlementCycle { get; set; }

        [Column(TypeName = "date")]
        public DateTime? SettlementDate { get; set; }

        [StringLength(50)]
        public string Company { get; set; }

        [StringLength(50)]
        public string BSCREF { get; set; }

        [StringLength(50)]
        public string SSCREF { get; set; }

        [StringLength(50)]
        public string TRAREF { get; set; }

        public bool? AckSent { get; set; }
    }

    public class Dealer
    {
      public string Security {get;set;}
        public string Deal {get;set;}
            public string Quantity {get;set;}
           public string Price {get;set;}
         public string Account1 {get;set;}
        public string Account2 {get;set;}
          public string DatePosted {get;set;}
        public string SettlementDate {get;set;}

        
        public string Ack { get; set; }
    }
    [TrackChanges]
    public class DealerDG
    {
        [Key]
        public int ID { get; set; }
        public string Security { get; set; }
        public string Deal { get; set; }
        public string Quantity { get; set; }
        public string Price { get; set; }
        public string Account1 { get; set; }
        public string Account2 { get; set; }
        public string DatePosted { get; set; }
        public string SettlementDate { get; set; }

        public string Ack { get; set; }

        public string AccountName1 { get; set; }
        public string AccountName2 { get; set; }

        public string SecurityName { get; set; }

        public string Exchange { get; set; }

        public Boolean Printed { get; set; }

        public string Status { get; set; }

        public DateTime ?SettDate { get; set; }
        public DateTime? PostDate { get; set; }
        public int? DealNumber { get; set; }
        public string OrderNumber { get; set; }
    }
    [TrackChanges]
    public class DealerDG2
    {
        [Key]
        public int ID { get; set; }
        public string Security { get; set; }
        public string Deal { get; set; }
        public string Quantity { get; set; }
        public string Price { get; set; }
        public string Account1 { get; set; }
        public string Account2 { get; set; }
        public DateTime DatePosted { get; set; }
        public string SettlementDate { get; set; }

        public string Ack { get; set; }

        public string AccountName1 { get; set; }
        public string AccountName2 { get; set; }

        public string SecurityName { get; set; }

        public string Exchange { get; set; }

        public Boolean Printed { get; set; }

        public string Status { get; set; }

        public string OldDeal { get; set; }

        public DateTime ?SettDate { get; set; }
        public DateTime? PostDate { get; set; }
        public string OrderNumber { get; set; }
    }

    public class DealFull
    {
     public string fnam { get; set; }
    public string Add_1 { get; set; }
      public string Add_2 { get; set; }
     public string Add_3 { get; set; }
       public string Add_4 { get; set; }
        
    }

    public class Consolidateds
    {

        public int Key { get; set; }
        public string AccountName { get; set; }
        public string ATPCSD { get; set; }
    public decimal TotalQuantity { get; set; }
    public decimal Price { get; set; }
    public decimal TotalBrokerAmount { get; set; }
    public DateTime? DatePosted { get; set; }
    public int Deals { get; set; }
        public string Security { get; set; }
    }

}