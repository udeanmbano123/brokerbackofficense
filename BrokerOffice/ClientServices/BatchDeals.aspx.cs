﻿using BrokerOffice.ATS;
using BrokerOffice.DAO;
using BrokerOffice.DealClass;
using BrokerOffice.DealNotes;
using BrokerOffice.Models;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebMatrix.WebData;
using System.Transactions;
using System.Collections;
using System.Text;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace BrokerOffice.ClientServices
{
    public partial class BatchDeals : System.Web.UI.Page
    {
        public string id = "";
        private SBoardContext db = new SBoardContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                try
                {
                    GetData();
                }
                catch (Exception)
                {

                   
                }
            }
            BindGrids();
            try
            {
                id = Request.QueryString["id"];
                if (this.IsPostBack)
                {

                }
                else
                {



                    if (id == " " || id == null)
                    {

                    }
                    else
                    {
                        // getDetails(id);
                    }
                }

            }
            catch (Exception)
            {
                //Response.Redirect("~/User/Index", false);        //write redirect
                //Context.ApplicationInstance.CompleteRequest(); // end response


            }

        }

        public void BindGrids()
        {
            string nme = "";
            var users = db.Users.ToList().Where(a => a.Email == WebSecurity.CurrentUserName);

            foreach (var dd in users)
            {
                nme = dd.BrokerCode;
            }
            if (ASPxDateEdit1.Text!="") {

                DateTime date = Convert.ToDateTime(ASPxDateEdit1.Text);
                DateTime date2 = Convert.ToDateTime(ASPxDateEdit2.Text);
                DateTime date3 = date2.AddDays(1);
                // select list
                var buy = from s in db.DealerDGs
                          join v in db.Account_Creations on s.Account2 equals v.CDSC_Number
                          let Security = s.Security
                          let Deal = s.Deal
                          let Quantity = s.Quantity
                          let Price = s.Price
                          let BuyerClient = s.AccountName1
                          let SellerClient = s.AccountName2
                          let DatePosted = s.DatePosted
                          let SettlementDate = s.SettlementDate
                          let fullNames = v.Surname_CompanyName + " " + v.OtherNames + " " + v.CDSC_Number
                          let Status = s.Status
                          where s.Status != "SETTLED"
                          select new { Deal, Security, Quantity, Price, BuyerClient, SellerClient, DatePosted, SettlementDate, Status };

                //gvAll.DataSource = buy.Where(a =>System.DateTime.Parse(a.DatePosted)>date && System.DateTime.Parse(a.DatePosted)<date3).ToList();
                gvAll.DataSource = GetEarners(date,date3);
                gvAll.DataBind();
            }
        }
        public List<BatchD> GetEarners(DateTime a,DateTime b)
        {
            //return listEmp.First(e => e.ID == id); 

            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["SBoardConnection"].ConnectionString;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = "Select [Security] As 'Security',Deal As 'Deal',Quantity as 'Quantity',Price as 'Price',AccountName1 as 'BuyerClient',AccountName2 as SellerClient,PostDate as 'DatePosted',SettDate as 'SettlementDate',DealerDG.[Status] as 'Status'  from DealerDG inner join  Account_Creation ON DealerDG.Account2=Account_Creation.CDSC_Number where CAST(PostDate As Date)>='" + a.ToString("dd-MMM-yyyy") + "' And CAST(PostDate As Date)<='"+ b.ToString("dd-MMM-yyyy") + "' And DealerDg.[Status] is null or DealerDg.[Status]='None' ";
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<BatchD>();
            while (reader.Read())
            {
                var accountDetails = new BatchD
                {

                    Security = reader.GetValue(0).ToString(),
                     Deal = reader.GetValue(1).ToString(),
                      Quantity = reader.GetValue(2).ToString(),
                       Price = reader.GetValue(3).ToString(),
                       BuyerClient = reader.GetValue(4).ToString(),
                       SellerClient = reader.GetValue(5).ToString(),
                       DatePosted = reader.GetValue(6).ToString(),
                       SettlementDate = reader.GetValue(7).ToString(),
                       Status = reader.GetValue(8).ToString(),
                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();

            return accDetails;
        }
        public void msgbox(string strMessage)
        {
            string strScript = "<script language=JavaScript>";
            strScript += "window.alert(\"" + strMessage + "\");";
            strScript += "</script>";
            System.Web.UI.WebControls.Label lbl = new System.Web.UI.WebControls.Label();
            lbl.Text = strScript;
            Page.Controls.Add(lbl);
        }
        public string GetNames(string acc)
        {
            var c = db.Account_Creations.ToList().Where(a => a.CDSC_Number == acc);
            string nme = "";
            foreach (var p in c)
            {
                nme = p.Surname_CompanyName + " " + p.OtherNames;
            }
            return nme;
        }
                        
        public  DateTime AddBusinessDays(DateTime dateTime, int nDays)
        {
            var wholeWeeks = nDays / 5; //since nDays does not include weekdays every week is considered as 5 days
            var absDays = Math.Abs(nDays);
            var remaining = absDays % 5; //results in the number remaining days to add or substract excluding the whole weeks
            var direction = nDays / absDays;//results in 1 if nDays is posisive or -1 if it's negative
            while (dateTime.DayOfWeek == DayOfWeek.Saturday || dateTime.DayOfWeek == DayOfWeek.Sunday)
                dateTime = dateTime.AddDays(direction); //If we are already in a weekend, get out of it
            while (remaining-- > 0)
            {//add remaining days...
                dateTime = dateTime.AddDays(direction);
                if (dateTime.DayOfWeek == DayOfWeek.Saturday)
                    dateTime = dateTime.AddDays(direction * 2);//...skipping weekends
            }
            return dateTime.AddDays(wholeWeeks * 7); //Finally add the whole weeks as 7 days, thus skipping the weekends without checking for DayOfWeek
        }

        protected void OnPaging(object sender, GridViewPageEventArgs e)
        {
            gvAll.PageIndex = e.NewPageIndex;
            gvAll.DataBind();
            BindGrids();
        }
        private void SetData()
        {
            int currentCount = 0;
            CheckBox chkAll = (CheckBox)gvAll.HeaderRow
                                    .Cells[0].FindControl("chkAll");
            chkAll.Checked = true;
            ArrayList arr = (ArrayList)ViewState["SelectedRecords"];
            for (int i = 0; i < gvAll.Rows.Count; i++)
            {
                CheckBox chk = (CheckBox)gvAll.Rows[i]
                                .Cells[0].FindControl("chk");
                if (chk != null)
                {
                    chk.Checked = arr.Contains(gvAll.DataKeys[i].Value);
                    if (!chk.Checked)
                        chkAll.Checked = false;
                    else
                        currentCount++;
                }
            }
            hfCount.Value = (arr.Count - currentCount).ToString();
        }
        private void GetData()
        {
            ArrayList arr;
            if (ViewState["SelectedRecords"] != null)
                arr = (ArrayList)ViewState["SelectedRecords"];
            else
                arr = new ArrayList();
            CheckBox chkAll = (CheckBox)gvAll.HeaderRow
                                .Cells[0].FindControl("chkAll");
            for (int i = 0; i < gvAll.Rows.Count; i++)
            {
                if (chkAll.Checked)
                {
                    if (!arr.Contains(gvAll.DataKeys[i].Value))
                    {
                        arr.Add(gvAll.DataKeys[i].Value);
                    }
                }
                else
                {
                    CheckBox chk = (CheckBox)gvAll.Rows[i]
                                       .Cells[0].FindControl("chk");
                    if (chk.Checked)
                    {
                        if (!arr.Contains(gvAll.DataKeys[i].Value))
                        {
                            arr.Add(gvAll.DataKeys[i].Value);
                        }
                    }
                    else
                    {
                        if (arr.Contains(gvAll.DataKeys[i].Value))
                        {
                            arr.Remove(gvAll.DataKeys[i].Value);
                        }
                    }
                }
            }
            ViewState["SelectedRecords"] = arr;
        }
        protected void ASPxButton1_Click(object sender, EventArgs e)
        {
            int count = 0;
            SetData();
            gvAll.AllowPaging = false;
            gvAll.DataBind();
            ArrayList arr = (ArrayList)ViewState["SelectedRecords"];
            count = arr.Count;
            for (int i = 0; i < gvAll.Rows.Count; i++)
            {
                if (arr.Contains(gvAll.DataKeys[i].Value))
                {
                    //DeleteRecord(gvAll.DataKeys[i].Value.ToString());
                    var dbc = db.DealerDGs.ToList().Where(a=>a.Deal== gvAll.DataKeys[i].Value.ToString());
                    foreach (var c in dbc)
                    {
                        DealerDG my = db.DealerDGs.Find(c.ID);
                        my.Status = "SETTLED";
                        db.DealerDGs.AddOrUpdate(my);
                        db.SaveChanges(WebSecurity.CurrentUserName);
                    }
                    arr.Remove(gvAll.DataKeys[i].Value);
                }
            }
            ViewState["SelectedRecords"] = arr;
            hfCount.Value = "0";
            gvAll.AllowPaging = true;
            BindGrids();
            ShowMessage(count);
        }
        private void ShowMessage(int count)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<script type = 'text/javascript'>");
            sb.Append("alert('");
            sb.Append(count.ToString());
            sb.Append(" sell deals have been settled.');");
            sb.Append("</script>");
            ClientScript.RegisterStartupScript(this.GetType(),
                            "script", sb.ToString());
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (ASPxDateEdit1.Text == "" || ASPxDateEdit2.Text == "")
            {
                msgbox("Please select a date");
                return;
            }
            if (gvAll.Rows.Count==0)
            {
                msgbox("The grid must contain sell deals");
                return;
            }
            int count = 0;
            SetData();
            gvAll.AllowPaging = false;
            gvAll.DataBind();
            ArrayList arr = (ArrayList)ViewState["SelectedRecords"];
            count = arr.Count;
            for (int i = 0; i < gvAll.Rows.Count; i++)
            {
                if (arr.Contains(gvAll.DataKeys[i].Value))
                {
                    //DeleteRecord(gvAll.DataKeys[i].Value.ToString());
                    var dbc = db.DealerDGs.ToList().Where(a => a.Deal == gvAll.DataKeys[i].Value.ToString());
                    foreach (var c in dbc)
                    {
                        DealerDG my = db.DealerDGs.Find(c.ID);
                        my.Status = "SETTLED";
                        db.DealerDGs.AddOrUpdate(my);
                        db.SaveChanges(WebSecurity.CurrentUserName);
                    }
                    arr.Remove(gvAll.DataKeys[i].Value);
                }
            }
            ViewState["SelectedRecords"] = arr;
            hfCount.Value = "0";
            gvAll.AllowPaging = true;
            BindGrids();
            ShowMessage(count);

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (ASPxDateEdit1.Text == "" || ASPxDateEdit2.Text=="")
            {
                msgbox("Please select a date");
                return;
            }
            else
            {
                BindGrids();
            }

        }

        protected void gvAll_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "MouseEvents(this, event)");
                e.Row.Attributes.Add("onmouseout", "MouseEvents(this, event)");
            }
        }
    }

       
        
    }
    

