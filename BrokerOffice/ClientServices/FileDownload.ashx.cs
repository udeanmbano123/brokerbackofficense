﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Linq;
using BrokerOffice.DAO;
using System.IO;
using DevExpress.XtraReports.UI;

namespace BrokerOffice.ClientServices
{
    /// <summary>
    /// Summary description for FileDownload1
    /// </summary>
    public class FileDownload1 : IHttpHandler
    {
        SBoardContext db = new SBoardContext();
        public void ProcessRequest(HttpContext context)
        {
            var cb = 0;
            try
            {
                cb = db.TradingChargess.Where(a => a.chargeaccountcode.ToString() == "2003").ToList().Count();

            }
            catch (Exception)
            {

                cb = 0;
            }
            var date = "01-JAN-2017";
            var date2 = "31-DEC-2017";
            var dep = "pdf";
            //DeleteRecord(gvAll.DataKeys[i].Value.ToString());
            var dbccz = db.Accounts_Masters.ToList().Where(a => a.AccountNumber.ToString() == "2003");
            foreach (var c in dbccz)
            {
                if (cb == 0)
                {
                    BrokerOffice.Reporting.XtraAccStmt report1 = new BrokerOffice.Reporting.XtraAccStmt();
                    //parameters
                    var ccd = 0;
                    report1.Parameters["Account"].Value = c.AccountNumber;
                    report1.Parameters["AccountName"].Value = c.AccountName;

                    report1.Parameters["FDate"].Value = date;
                    report1.Parameters["TDate"].Value = date2;
                    //report.Parameters["yourParameter2"].Value = secondValue;
                    report1.CreateDocument();
                    // Reset all page numbers in the resulting document. 
                    report1.PrintingSystem.ContinuousPageNumbering = true;
                    string nme33 = date + "-" + date2;
                    // Create a Print Tool and show the Print Preview form. 
                    //report1.ExportToPdf();
                    ExportReport(context, report1, report1.Parameters["AccountName"].Value + nme33 + "." + dep, dep, false);
                }
                else
                {
                    BrokerOffice.Reporting.XtraTaxesF report1 = new BrokerOffice.Reporting.XtraTaxesF();
                    //parameters
                    var ccd = 0;
                    report1.Parameters["Account"].Value = c.AccountNumber;
                    report1.Parameters["AccountName"].Value = c.AccountName;
                    report1.Parameters["FDate"].Value = date;
                    report1.Parameters["TDate"].Value = date2;
                    //report.Parameters["yourParameter2"].Value = secondValue;
                    report1.CreateDocument();
                    // Reset all page numbers in the resulting document. 
                    report1.PrintingSystem.ContinuousPageNumbering = true;
                    string nme2 = date + "-" + date2; // Create a Print Tool and show the Print Preview form. 
                                                      //report1.ExportToPdf();
                    ExportReport(context, report1, report1.Parameters["AccountName"].Value + nme2 + "." + dep, dep, false);
                }
            }

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
     
        public void ExportReport(HttpContext context, XtraReport report, string fileName, string fileType, bool inline)
        {
            MemoryStream stream = new MemoryStream();

            context.Response.Clear();

            if (fileType == "xlsx")
                report.ExportToXlsx(stream);
            if (fileType == "pdf")
                report.ExportToPdf(stream);
            if (fileType == "rtf")
                report.ExportToRtf(stream);
            if (fileType == "csv")
                report.ExportToCsv(stream);


            context.Response.ContentType = "application/" + fileType;
            context.Response.AddHeader("Accept-Header", stream.Length.ToString());
            context.Response.AddHeader("Content-Disposition", (inline ? "Inline" : "Attachment") + "; filename=" + fileName + "." + fileType);
            context.Response.AddHeader("Content-Length", stream.Length.ToString());
            //Response.ContentEncoding = System.Text.Encoding.Default;
            context.Response.BinaryWrite(stream.ToArray());
            context.Response.End();

        }

        public void ExportToResponse(HttpContext context, byte[] content, string fileName, string fileType, bool inline)
        {
            context.Response.Clear();
            context.Response.ContentType = "application/" + fileType;
            context.Response.AddHeader("Content-Disposition", string.Format("{0}; filename={1}.{2}", inline ? "Inline" : "Attachment", fileName, fileType));
            context.Response.AddHeader("Content-Length", content.Length.ToString());
            //Response.ContentEncoding = System.Text.Encoding.Default;
            context.Response.BinaryWrite(content);
            //context.Response.Flush();
            //context.Response.Close();
            context.Response.End();
        }
    }
}