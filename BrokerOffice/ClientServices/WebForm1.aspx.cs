﻿using BrokerOffice.DAO;
using DevExpress.XtraReports.UI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BrokerOffice.ClientServices
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        private SBoardContext db = new SBoardContext();

        protected void Page_Load(object sender, EventArgs e)
        {
            Do();
        }
        public void Do()
        {
            var cb = 0;
            int mo = Convert.ToInt32(Request.QueryString["id"].ToString());
            try
            {
                cb = db.TradingChargess.Where(a => a.chargeaccountcode==mo ).ToList().Count();

            }
            catch (Exception)
            {

                throw;
            }
            var date = Request.QueryString["d1"].ToString();
                var date2 = Request.QueryString["d2"].ToString();
            var dep = Request.QueryString["dep"].ToString();
            //DeleteRecord(gvAll.DataKeys[i].Value.ToString());
            var dbccz = db.Accounts_Masters.ToList().Where(a => a.AccountNumber.ToString() == Request.QueryString["id"].ToString());
            foreach (var c in dbccz)
            {
                if (cb == 0)
                {
                    BrokerOffice.Reporting.XtraAccStmt report1 = new BrokerOffice.Reporting.XtraAccStmt();
                    //parameters
                    var ccd = 0;
                    report1.Parameters["Account"].Value = c.AccountNumber;
                    report1.Parameters["AccountName"].Value = c.AccountName;

                    report1.Parameters["FDate"].Value = date;
                    report1.Parameters["TDate"].Value = date2;
                    //report.Parameters["yourParameter2"].Value = secondValue;
                    report1.CreateDocument();
                    // Reset all page numbers in the resulting document. 
                    report1.PrintingSystem.ContinuousPageNumbering = true;
                    string nme33 = date + "-" + date2;
                    // Create a Print Tool and show the Print Preview form. 
                    //report1.ExportToPdf();
                    ExportReport(report1, report1.Parameters["AccountName"].Value + nme33 + "." + dep, dep, false);
                }

                else if (cb>0)
                {

                    
                        BrokerOffice.Reporting.XtraTaxesF report1 = new BrokerOffice.Reporting.XtraTaxesF();
                    //parameters
                    var ccd = 0;
                    report1.Parameters["Account"].Value = c.AccountNumber;
                    report1.Parameters["AccountName"].Value = c.AccountName;
                    report1.Parameters["FDate"].Value = date;
                    report1.Parameters["TDate"].Value = date2;
                    //report.Parameters["yourParameter2"].Value = secondValue;
                    report1.CreateDocument();
                    // Reset all page numbers in the resulting document. 
                    report1.PrintingSystem.ContinuousPageNumbering = true;
                    string nme2 = date + "-" + date2; // Create a Print Tool and show the Print Preview form. 
                                                                                                    //report1.ExportToPdf();
                    ExportReport(report1, report1.Parameters["AccountName"].Value + nme2 + "." + dep, dep, false);
                }
            }
            
        }
        public void ExportReport(XtraReport report, string fileName, string fileType, bool inline)
        {
            MemoryStream stream = new MemoryStream();

            Response.Clear();

            if (fileType == "Xlsx")
                report.ExportToXlsx(stream);
            if (fileType == "xls")
                report.ExportToXls(stream);
            if (fileType == "pdf")
                report.ExportToPdf(stream);
            if (fileType == "rtf")
                report.ExportToRtf(stream);
            if (fileType == "csv")
                report.ExportToCsv(stream);


            Response.ContentType = "application/" + fileType;
            Response.AddHeader("Accept-Header", stream.Length.ToString());
            Response.AddHeader("Content-Disposition", (inline ? "Inline" : "Attachment") + "; filename=" + fileName + "." + fileType);
            Response.AddHeader("Content-Length", stream.Length.ToString());
            //Response.ContentEncoding = System.Text.Encoding.Default;
            Response.BinaryWrite(stream.ToArray());
            Response.End();
        
        }

    }
}