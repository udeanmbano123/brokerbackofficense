﻿using BrokerOffice.DAO;
using BrokerOffice.DealClass;
using BrokerOffice.DealNotes;
using BrokerOffice.Models;
using BrokerOffice.Reporting;
using DevExpress.XtraReports.UI;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.Linq.SqlClient;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebMatrix.WebData;


namespace BrokerOffice.ClientServices
{
    public partial class Audit : System.Web.UI.Page
    {
        public string id = "";
        private SBoardContext db = new SBoardContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                id = Request.QueryString["id"];
                if (this.IsPostBack)
                {
                    // TabName.Value = Request.Form[TabName.UniqueID];
                    if (ASPxDateEdit1.Text != "" && ASPxDateEdit2.Text != "")
                    {
                        trades();
                    }
                }
                else
                {
                    //trades2();
                    // loadCustomers();
                    if (ASPxDateEdit1.Text != "" && ASPxDateEdit2.Text != "")
                    {
                        trades();

                    }

                    if (id == " " || id == null)
                    {

                    }
                    else
                    {
                        // getDetails(id);
                    }
                }
                //if (txtSearch.Text != "")
                //{
                //    //ASPxTextBox1.Text = "";
                //    trades();
                //}

            }
            catch (Exception)
            {
                Response.Redirect("~/User/Index", false);        //write redirect
                Context.ApplicationInstance.CompleteRequest(); // end response


            }

        }

        public bool IsAddressAvailable(string address)
        {
            try
            {
                System.Net.WebClient client = new WebClient();
                client.DownloadData(address);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public async void trades2()
        {


            var dd = db.Users.ToList().Where(a => a.Email == WebSecurity.CurrentUserName);

            string vx = "";

            foreach (var p in dd)
            {
                vx = p.BrokerCode;
            }
            string n = "";
            string responJsonText = "";
            var user = db.Account_Creations.ToList();
            string updatedjson = "";
            int d = 0;
            int x = 0;
            string carryjson = "";
            JArray v2 = null;

            Tbl_MatchedDeals c1 = null;
            var jsonObjects = new List<string>();
            var jsonObjectsArray = "";


            var client = new RestClient("http://localhost/BrokerService");
            var request = new RestRequest("Matcheds", Method.GET);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            List<ATS.Tbl_MatchedDeals> dataList = JsonConvert.DeserializeObject<List<ATS.Tbl_MatchedDeals>>(validate);
            //   GridView1.DataSource = DerializeDataTable(responJsonText);
            var dbsel = from s in dataList
                        join v in db.Account_Creations on s.Buyercdsno equals v.CDSC_Number
                        let SID = s.ID
                        let Deal = s.Deal
                        let BuyCompany = s.BuyCompany
                        let SellCompany = s.SellCompany
                        let Buyer = s.Buyercdsno
                        let Seller = s.Sellercdsno
                        let Quantity = s.Quantity
                        let Trade = s.Trade
                        let DealPrice = s.DealPrice
                        let DealFlag = s.DealFlag
                        let Instrument = s.Instrument
                        let affirmation = s.Affirmation
                        let buybroker = s.buybroker
                        let sellbroker = s.sellbroker
                        //where s.Buyercdsno==bss || s.Sellercdsno==bss
                        select new { SID, Deal, BuyCompany, SellCompany, Buyer, Seller, Quantity, Trade, DealPrice, DealFlag, Instrument, affirmation, buybroker, sellbroker };
            var dbsel2 = from s in dataList
                         join v in db.Account_Creations on s.Sellercdsno equals v.CDSC_Number
                         let SID = s.ID
                         let Deal = s.Deal
                         let BuyCompany = s.BuyCompany
                         let SellCompany = s.SellCompany
                         let Buyer = s.Buyercdsno
                         let Seller = s.Sellercdsno
                         let Quantity = s.Quantity
                         let Trade = s.Trade
                         let DealPrice = s.DealPrice
                         let DealFlag = s.DealFlag
                         let Instrument = s.Instrument
                         let affirmation = s.Affirmation
                         let buybroker = s.buybroker
                         let sellbroker = s.sellbroker
                         //where s.Buyercdsno==bss || s.Sellercdsno==bss
                         select new { SID, Deal, BuyCompany, SellCompany, Buyer, Seller, Quantity, Trade, DealPrice, DealFlag, Instrument, affirmation, buybroker, sellbroker };


            int my44 = dbsel.ToList().Count();
            int my45 = dbsel2.ToList().Count();
            int sel = 0;
            var bag = dbsel.ToList();
            var bag2 = dbsel2.ToList();
            foreach (var c in bag)
            {
                var ss = 0;

                try
                {
                    ss = db.TblDealss.ToList().Where(a => a.RefID == c.SID).Count();
                }
                catch (Exception)
                {

                    ss = 0;
                }


                if (ss == 0)
                {
                    try
                    {
                        sel = db.Account_Creations.ToList().Where(a => a.CDSC_Number == c.Seller).Count();


                    }
                    catch (Exception)
                    {

                        sel = 0;
                    }
                    if (sel == 0)
                    {
                        ATS.Tbl_MatchedDeals my = new ATS.Tbl_MatchedDeals();
                        my.Deal = c.Deal;
                        my.BuyCompany = c.BuyCompany;
                        my.SellCompany = c.SellCompany;
                        my.Buyercdsno = c.Buyer;
                        my.Sellercdsno = c.Seller;
                        my.Quantity = c.Quantity;
                        my.Trade = c.Trade;
                        my.DealPrice = c.DealPrice;
                        my.DealFlag = c.DealFlag;
                        my.Instrument = c.Instrument;
                        my.Affirmation = c.affirmation;
                        my.buybroker = c.buybroker;
                        my.sellbroker = c.sellbroker;
                        my.RefID = Convert.ToInt32(c.SID);
                        my.BrokerCode = vx;
                        db.TblDealss.Add(my);
                        db.SaveChanges(WebSecurity.CurrentUserName);
                    }
                    updateBuyTrade(c.Buyer, c.Seller, c.DealPrice.ToString(), Convert.ToInt32(c.SID), Convert.ToInt32(c.Quantity));

                }

            }


            //update all sell

            foreach (var c in bag2)
            {
                var ss = 0;
                try
                {
                    ss = db.TblDealss.ToList().Where(a => a.RefID == c.SID).Count();

                }
                catch (Exception)
                {

                    ss = 0;
                }

                if (ss == 0)
                {
                    ATS.Tbl_MatchedDeals my = new ATS.Tbl_MatchedDeals();
                    my.Deal = c.Deal;
                    my.BuyCompany = c.BuyCompany;
                    my.SellCompany = c.SellCompany;
                    my.Buyercdsno = c.Buyer;
                    my.Sellercdsno = c.Seller;
                    my.Quantity = c.Quantity;
                    my.Trade = c.Trade;
                    my.DealPrice = c.DealPrice;
                    my.DealFlag = c.DealFlag;
                    my.Instrument = c.Instrument;
                    my.Affirmation = c.affirmation;
                    my.buybroker = c.buybroker;
                    my.sellbroker = c.sellbroker;
                    my.RefID = Convert.ToInt32(c.SID);
                    my.BrokerCode = vx;
                    db.TblDealss.Add(my);
                    db.SaveChanges(WebSecurity.CurrentUserName);
                    updateSellTrade(c.Buyer, c.Seller, c.DealPrice.ToString(), Convert.ToInt32(c.SID), Convert.ToInt32(c.Quantity));
                }
            }




        }
        public async void trades()
        {
            var dd = db.Users.ToList().Where(a => a.Email == WebSecurity.CurrentUserName);

            string vx = "";

            foreach (var p in dd)
            {
                vx = p.BrokerCode;
            }
            DateTime dt = Convert.ToDateTime(ASPxDateEdit1.Date);
            DateTime dt2 = Convert.ToDateTime(ASPxDateEdit2.Date).AddDays(1);
         
            if (RadioButtonList5.SelectedValue=="False")
            {
                var dbsel = from s in db.DealerDGs
                            join v in db.Account_Creations on s.Account1 equals v.CDSC_Number
                            let DealNumber = s.DealNumber
                            let Security = s.Security
                            let Deal = s.Deal
                            let Quantity = s.Quantity
                            let Price = s.Price
                            let BuyerClient = s.AccountName1
                            let SellerClient = s.AccountName2
                            let DatePosted = s.PostDate
                            let SettlementDate = s.SettlementDate
                            let fullNames = v.Surname_CompanyName + " " + v.OtherNames + " " + v.CDSC_Number
                            let Audited = s.Printed
                            where DatePosted > dt && DatePosted <dt2 && Audited == false

                            select new { DealNumber, Security, Deal, Quantity, Price, BuyerClient, SellerClient, DatePosted, SettlementDate, Audited };

                var dbsel2 = from s in db.DealerDGs
                             join v in db.Account_Creations on s.Account2 equals v.CDSC_Number
                             let DealNumber = s.DealNumber
                             let Security = s.Security
                             let Deal = s.Deal
                             let Quantity = s.Quantity
                             let Price = s.Price
                             let BuyerClient = s.AccountName1
                             let SellerClient = s.AccountName2
                             let DatePosted = s.PostDate
                             let SettlementDate = s.SettlementDate
                             let fullNames = v.Surname_CompanyName + " " + v.OtherNames + " " + v.CDSC_Number
                             let Audited = s.Printed
                             where DatePosted >= dt && DatePosted < dt2 && Audited == false
                             select new { DealNumber, Security, Deal, Quantity, Price, BuyerClient, SellerClient, DatePosted, SettlementDate, Audited };

                var allresults = dbsel.Union(dbsel2).ToList().OrderByDescending(a => a.DatePosted);

                ASPxGridView1.DataSource = null;

                ASPxGridView1.DataSource = allresults;

                ASPxGridView1.DataBind();
                ASPxGridView1.Visible = true;
                ASPxGridView2.Visible = false;
            }
            else
            {
                var dbsel = from s in db.DealerDGs
                            join v in db.Account_Creations on s.Account1 equals v.CDSC_Number
                            let DealNumber = s.DealNumber
                            let Security = s.Security
                            let Deal = s.Deal
                            let Quantity = s.Quantity
                            let Price = s.Price
                            let BuyerClient = s.AccountName1
                            let SellerClient = s.AccountName2
                            let DatePosted = s.PostDate
                            let SettlementDate = s.SettlementDate
                            let fullNames = v.Surname_CompanyName + " " + v.OtherNames + " " + v.CDSC_Number
                            let Audited = s.Printed
                            where DatePosted > dt && DatePosted <dt2 && Audited == true

                            select new { DealNumber, Security, Deal, Quantity, Price, BuyerClient, SellerClient, DatePosted, SettlementDate, Audited };

                var dbsel2 = from s in db.DealerDGs
                             join v in db.Account_Creations on s.Account2 equals v.CDSC_Number
                             let DealNumber = s.DealNumber
                             let Security = s.Security
                             let Deal = s.Deal
                             let Quantity = s.Quantity
                             let Price = s.Price
                             let BuyerClient = s.AccountName1
                             let SellerClient = s.AccountName2
                             let DatePosted = s.PostDate
                             let SettlementDate = s.SettlementDate
                             let fullNames = v.Surname_CompanyName + " " + v.OtherNames + " " + v.CDSC_Number
                             let Audited = s.Printed
                             where DatePosted >= dt && DatePosted <dt2 && Audited == true
                             select new { DealNumber, Security, Deal, Quantity, Price, BuyerClient, SellerClient, DatePosted, SettlementDate, Audited };

                var allresults = dbsel.Union(dbsel2).ToList().OrderByDescending(a => a.DatePosted);

                ASPxGridView2.DataSource = null;

                ASPxGridView2.DataSource = allresults;

                ASPxGridView2.DataBind();
                ASPxGridView2.Visible = true;
                ASPxGridView1.Visible = false;
            }
           


        }
        public void postTradeCharges(string type, string b, string p, int d, string deal)
        {
            BrokerOffice.Models.Trans my2 = new BrokerOffice.Models.Trans();
            string maxid = deal;

            decimal maxprice = Convert.ToDecimal(p);
            decimal quantity = Convert.ToDecimal(d);
            int num = 0;
            //Cash Deposits
            var acc = db.Accounts_Masters.ToList().Where(a => a.AccountName.Contains("Accounts Receivable"));


            foreach (var c in acc)
            {
                num = c.AccountNumber;
            }
            ////trading charges
            var tr = db.TradingChargess.ToList();
            BrokerOffice.Models.Trans my3 = new BrokerOffice.Models.Trans();
            //Debit
            decimal total = 0;
            foreach (var q in tr)
            {
                if (type == "BUY")
                {
                    if (q.ChargedAs == "Flat")
                    {
                        my3.Account = q.chargeaccountcode.ToString();
                        my3.Category = "Order Posting";
                        my3.Credit = Convert.ToDecimal(q.chargevalue);
                        my3.Debit = 0;
                        my3.Narration = q.chartaccount;
                        my3.Reference_Number = maxid.ToString();
                        my3.TrxnDate = DateTime.Now;
                        my3.Post = DateTime.Now;
                        my3.Type = q.chartaccount;
                        my3.PostedBy = WebSecurity.CurrentUserName;
                        db.Transs.Add(my3);

                        //Update table  
                        db.SaveChanges(WebSecurity.CurrentUserName.ToString());
                        ////debit account number
                        //my3.Account = b;
                        //my3.Category = q.chartaccount;
                        //my3.Credit = 0;
                        //my3.Debit = Convert.ToDecimal(q.chargevalue);
                        //my3.Narration = q.chartaccount;
                        //my3.Reference_Number = maxid.ToString();
                        //my3.TrxnDate = DateTime.Now;
                        //my3.Post = DateTime.Now;
                        //my3.Type = q.chartaccount;
                        //my3.PostedBy = WebSecurity.CurrentUserName;
                        //db.Transs.Add(my3);

                        ////Update table  
                        //db.SaveChanges(WebSecurity.CurrentUserName.ToString());

                        //debit
                        my3.Account = num.ToString();
                        my3.Category = "Accounts Receivable";
                        my3.Credit = 0;
                        my3.Debit = Convert.ToDecimal(q.chargevalue);
                        my3.Narration = q.chartaccount;
                        my3.Reference_Number = maxid.ToString();
                        my3.TrxnDate = DateTime.Now;
                        my3.Post = DateTime.Now;
                        my3.Type = "Accounts Receivable";
                        my3.PostedBy = WebSecurity.CurrentUserName;
                        db.Transs.Add(my3);

                        //Update table  
                        db.SaveChanges(WebSecurity.CurrentUserName.ToString());
                        total += Convert.ToDecimal(q.chargevalue);
                    }
                    else if (q.ChargedAs == "Percentage")
                    {
                        my3.Account = q.chargeaccountcode.ToString();
                        my3.Category = "Order Posting";
                        my3.Credit = ((Convert.ToDecimal(q.chargevalue) / 100) * (maxprice * quantity));
                        my3.Debit = 0;
                        my3.Narration = q.chartaccount;
                        my3.Reference_Number = maxid.ToString();
                        my3.TrxnDate = DateTime.Now;
                        my3.Post = DateTime.Now;
                        my3.Type = q.chartaccount;
                        my3.PostedBy = WebSecurity.CurrentUserName;
                        db.Transs.Add(my3);

                        //Update table  
                        db.SaveChanges(WebSecurity.CurrentUserName.ToString());
                        //debit account number
                        //my3.Account = b;
                        //my3.Category = q.chartaccount;
                        //my3.Credit = 0;
                        //my3.Debit = ((Convert.ToDecimal(q.chargevalue) / 100) * (maxprice * quantity));
                        //my3.Narration = q.chartaccount;
                        //my3.Reference_Number = maxid.ToString();
                        //my3.TrxnDate = DateTime.Now;
                        //my3.Post = DateTime.Now;
                        //my3.Type = q.chartaccount;
                        //my3.PostedBy = WebSecurity.CurrentUserName;
                        //db.Transs.Add(my3);

                        ////Update table  
                        //db.SaveChanges(WebSecurity.CurrentUserName.ToString());

                        //debit cashdeposits
                        my3.Account = num.ToString();
                        my3.Category = "Accounts Receivable";
                        my3.Credit = 0;
                        my3.Debit = ((Convert.ToDecimal(q.chargevalue) / 100) * (maxprice * quantity));
                        my3.Narration = q.chartaccount;
                        my3.Reference_Number = maxid.ToString();
                        my3.TrxnDate = DateTime.Now;
                        my3.Post = DateTime.Now;
                        my3.Type = "Accounts Receivable";
                        my3.PostedBy = WebSecurity.CurrentUserName;
                        db.Transs.Add(my3);

                        //Update table  
                        db.SaveChanges(WebSecurity.CurrentUserName.ToString());
                        total += Convert.ToDecimal(q.chargevalue);
                    }
                }
                else
                {
                    if (q.ChargedAs == "Flat")
                    {
                        my3.Account = q.chargeaccountcode.ToString();
                        my3.Category = "Order Posting";
                        my3.Debit = Convert.ToDecimal(q.chargevalue);
                        my3.Credit = 0;
                        my3.Narration = q.chartaccount;
                        my3.Reference_Number = maxid.ToString();
                        my3.TrxnDate = DateTime.Now;
                        my3.Post = DateTime.Now;
                        my3.Type = q.chartaccount;
                        my3.PostedBy = WebSecurity.CurrentUserName;
                        db.Transs.Add(my3);

                        //Update table  
                        db.SaveChanges(WebSecurity.CurrentUserName.ToString());
                        //debit account number
                        //my3.Account = b;
                        //my3.Category = q.chartaccount;
                        //my3.Debit = 0;
                        //my3.Credit = Convert.ToDecimal(q.chargevalue);
                        //my3.Narration = q.chartaccount;
                        //my3.Reference_Number = maxid.ToString();
                        //my3.TrxnDate = DateTime.Now;
                        //my3.Post = DateTime.Now;
                        //my3.Type = q.chartaccount;
                        //my3.PostedBy = WebSecurity.CurrentUserName;
                        //db.Transs.Add(my3);

                        ////Update table  
                        //db.SaveChanges(WebSecurity.CurrentUserName.ToString());

                        //debit
                        my3.Account = num.ToString();
                        my3.Category = "Accounts Receivable";
                        my3.Debit = 0;
                        my3.Credit = Convert.ToDecimal(q.chargevalue);
                        my3.Narration = q.chartaccount;
                        my3.Reference_Number = maxid.ToString();
                        my3.TrxnDate = DateTime.Now;
                        my3.Post = DateTime.Now;
                        my3.Type = "Accounts Receivable";
                        my3.PostedBy = WebSecurity.CurrentUserName;
                        db.Transs.Add(my3);

                        //Update table  
                        db.SaveChanges(WebSecurity.CurrentUserName.ToString());
                        total += Convert.ToDecimal(q.chargevalue);
                    }
                    else if (q.ChargedAs == "Percentage")
                    {
                        my3.Account = q.chargeaccountcode.ToString();
                        my3.Category = "Order Posting";
                        my3.Debit = ((Convert.ToDecimal(q.chargevalue) / 100) * (maxprice * quantity));
                        my3.Credit = 0;
                        my3.Narration = q.chartaccount;
                        my3.Reference_Number = maxid.ToString();
                        my3.TrxnDate = DateTime.Now;
                        my3.Post = DateTime.Now;
                        my3.Type = q.chartaccount;
                        my3.PostedBy = WebSecurity.CurrentUserName;
                        db.Transs.Add(my3);

                        //Update table  
                        db.SaveChanges(WebSecurity.CurrentUserName.ToString());
                        //debit account number
                        //my3.Account = b;
                        //my3.Category = q.chartaccount;
                        //my3.Debit = 0;
                        //my3.Credit = ((Convert.ToDecimal(q.chargevalue) / 100) * (maxprice * quantity));
                        //my3.Narration = q.chartaccount;
                        //my3.Reference_Number = maxid.ToString();
                        //my3.TrxnDate = DateTime.Now;
                        //my3.Post = DateTime.Now;
                        //my3.Type = q.chartaccount;
                        //my3.PostedBy = WebSecurity.CurrentUserName;
                        //db.Transs.Add(my3);

                        ////Update table  
                        //db.SaveChanges(WebSecurity.CurrentUserName.ToString());

                        //debit cashdeposits
                        my3.Account = num.ToString();
                        my3.Category = "Accounts Receivable";
                        my3.Debit = 0;
                        my3.Credit = ((Convert.ToDecimal(q.chargevalue) / 100) * (maxprice * quantity));
                        my3.Narration = q.chartaccount;
                        my3.Reference_Number = maxid.ToString();
                        my3.TrxnDate = DateTime.Now;
                        my3.Post = DateTime.Now;
                        my3.Type = "Accounts Receivable";
                        my3.PostedBy = WebSecurity.CurrentUserName;
                        db.Transs.Add(my3);

                        //Update table  
                        db.SaveChanges(WebSecurity.CurrentUserName.ToString());
                        total += Convert.ToDecimal(q.chargevalue);
                    }
                }


            }

            if (type == "BUY")
            {
                //debit account number
                my2.Account = b;
                my2.Category = "Deal";
                my2.Credit = 0;
                my2.Debit = (maxprice * quantity) + total;
                my2.Narration = "Deal";
                my2.Reference_Number = maxid.ToString();
                my2.TrxnDate = DateTime.Now;
                my2.Post = DateTime.Now;
                my2.Type = "Purchase Deal";
                my2.PostedBy = WebSecurity.CurrentUserName;
                db.Transs.Add(my2);

                //Update table  
                db.SaveChanges(WebSecurity.CurrentUserName.ToString());
            }
            else if (type == "SELL")
            {
                //debit account number
                my2.Account = b;
                my2.Category = "Deal";
                my2.Credit = (maxprice * quantity) - total;
                my2.Debit = 0;
                my2.Narration = "Deal";
                my2.Reference_Number = maxid.ToString();
                my2.TrxnDate = DateTime.Now;
                my2.Post = DateTime.Now;
                my2.Type = "Sell Deal";
                my2.PostedBy = WebSecurity.CurrentUserName;
                db.Transs.Add(my2);

                //Update table  
                db.SaveChanges(WebSecurity.CurrentUserName.ToString());
            }








            //insert into chart of accounts

            //Liability account
            var liab = db.Accounts_Masters.ToList().Where(a => a.AccountName.Contains("Liability"));
            int lacc = 0;
            foreach (var c in liab)
            {
                lacc = c.AccountNumber;
            }

            //Debit
            my2.Account = lacc.ToString();
            my2.Category = "Liability";
            my2.Credit = (maxprice * quantity) + total;
            my2.Debit = 0;
            my2.Narration = "Deal";
            my2.Reference_Number = maxid.ToString();
            my2.TrxnDate = DateTime.Now;
            my2.Post = DateTime.Now;
            my2.Type = "Order Posting";
            my2.PostedBy = WebSecurity.CurrentUserName;
            db.Transs.Add(my2);

            //Update table  
            db.SaveChanges(WebSecurity.CurrentUserName.ToString());



            my2.Account = num.ToString();
            my2.Category = "Deal";
            my2.Credit = 0;
            my2.Debit = (maxprice * quantity) + total;
            my2.Narration = "Deal";
            my2.Reference_Number = maxid.ToString();
            my2.TrxnDate = DateTime.Now;
            my2.Post = DateTime.Now;
            my2.Type = "Order Posting";
            my2.PostedBy = WebSecurity.CurrentUserName;
            db.Transs.Add(my2);

            db.SaveChanges(WebSecurity.CurrentUserName.ToString());

        }


        public void updateSellTrade(string b, string s, string p, int d, int q)
        {




            postTradeCharges("SELL", s, p, q, d.ToString());

        }

        public void updateBuyTrade(string b, string s, string p, int d, int q)
        {

            postTradeCharges("BUY", b, p, q, d.ToString());

        }

        public DataTable DerializeDataTable(string data)
        {
            string json = data; //"data" should contain your JSON 
            var table = JsonConvert.DeserializeObject<DataTable>(json);
            return table;
        }
        public string checkNull(string s)
        {
            if (s == null)
            {
                return "0";
            }

            return s;

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (ASPxDateEdit1.Text == "")
            {
                msgbox("From date is required");
                return;
            }
            else if(ASPxDateEdit2.Text == "")
            {
                msgbox("To date is required");
                return;
            }
            else if (RadioButtonList5.SelectedIndex<0)
            {
                msgbox("Select Criteria");
                return;
            }
            else
            {
                trades();
            }

        }
        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {



        }
        public void msgbox(string strMessage)
        {
            string strScript = "<script language=JavaScript>";
            strScript += "window.alert(\"" + strMessage + "\");";
            strScript += "</script>";
            System.Web.UI.WebControls.Label lbl = new System.Web.UI.WebControls.Label();
            lbl.Text = strScript;
            Page.Controls.Add(lbl);
        }
        protected void grdData_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            ASPxGridView1.PageIndex = e.NewPageIndex;
            trades();
        }
        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("Local IP Address Not Found!");
        }

        protected void ASPxGridView1_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
        {
            try
            {

                string i = e.Keys[ASPxGridView1.KeyFieldName].ToString();
                var cpz = db.DealerDGs.ToList().Where(a => a.Deal == i);
                foreach (var f in cpz)
                {
                    string ac = "";
                    string typo = "";
                    string typo2 = "";
                    if (f.Account1 != "ZSE" || f.Account1 != "")
                    {
                        ac = f.Account1;
                        typo = "BUY";
                        typo2 = "PURCHASE";
                    }
                    if (f.Account2 != "ZSE" || f.Account2 != "")
                    {
                        ac = f.Account2;
                        typo = "SELL";
                        typo2 = "SALE";
                    }
                    XtraDeal report = new XtraDeal();
                    report.Parameters["Account"].Value = ac;

                    report.Parameters["OrderNo"].Value = f.Deal;
                    report.Parameters["SID"].Value = f.Deal;
                    report.Parameters["QTY"].Value = f.Quantity;
                    report.Parameters["Dte"].Value = f.PostDate;
                    report.Parameters["Max"].Value = f.Price;

                    report.Parameters["Broker"].Value = "AKRI";
                    report.Parameters["BrokerName"].Value = ("Akribos Securities").ToUpper();

                    string z = ac;
                    string cf = "";
                    var p = db.Account_Creations.ToList().Where(a => a.CDSC_Number == z);
                    foreach (var v in p)
                    {
                        cf = v.Surname_CompanyName + " " + v.OtherNames;
                        report.Parameters["Address"].Value = v.depname;
                    }
                    report.Parameters["TYPO"].Value = typo;

                    if (typo != "BUY")
                    {
                        report.Parameters["Stamp"].Value = "Stamp duty";
                    }
                    else
                    {
                        report.Parameters["Stamp"].Value = "Zim";
                    }
                    report.Parameters["TYPO2"].Value = typo2;
                    if (f.Account1 != "ZSE" && f.Account2 != "ZSE")
                    {
                        ac = f.Account1;
                        typo = "BUY";
                        typo2 = "PURCHASE";
                        XtraDeal report2 = new XtraDeal();
                        report2.Parameters["Account"].Value = ac;

                        report2.Parameters["OrderNo"].Value = f.Deal;
                        report2.Parameters["SID"].Value = f.Deal;
                        report2.Parameters["QTY"].Value = f.Quantity;
                        report2.Parameters["Dte"].Value = f.PostDate;
                        report2.Parameters["Max"].Value = f.Price;

                        report2.Parameters["Broker"].Value = "AKRI";
                        report2.Parameters["BrokerName"].Value = ("Akribos Securities").ToUpper();

                        z = ac;
                        cf = "";
                        p = db.Account_Creations.ToList().Where(a => a.CDSC_Number == z);
                        foreach (var v in p)
                        {
                            cf = v.Surname_CompanyName + " " + v.OtherNames;
                            report2.Parameters["Address"].Value = v.depname;
                        }
                        report2.Parameters["TYPO"].Value = typo;

                        if (typo != "BUY")
                        {
                            report2.Parameters["Stamp"].Value = "Stamp duty";
                        }
                        else
                        {
                            report2.Parameters["Stamp"].Value = "Zim";
                        }
                        report2.Parameters["TYPO2"].Value = typo2;
                        report2.CreateDocument();

                        int minPageCount = Math.Min(report.Pages.Count, report2.Pages.Count);
                        for (int g = 0; g < minPageCount; g++)
                        {
                            report.Pages.Insert(g * 2 + 1, report2.Pages[g]);
                        }
                        if (report2.Pages.Count != minPageCount)
                        {
                            for (int g = minPageCount; g < report2.Pages.Count; g++)
                            {
                                report.Pages.Add(report2.Pages[g]);
                            }
                        }

                        // Reset all page numbers in the resulting document. 
                        report.PrintingSystem.ContinuousPageNumbering = true;
                    }
                    //ASPxDocumentViewer1.Report = report;
                    // Create a new memory stream and export the report into it as PDF.
                    MemoryStream mem = new MemoryStream();
                    report.ExportToPdf(mem);

                    // Create a new attachment and put the PDF report into it.
                    mem.Seek(0, System.IO.SeekOrigin.Begin);
                    Attachment att = new Attachment(mem, "BrokerNote" + DateTime.Now.ToString("dd-MMM-yyyy") + ".pdf", "application/pdf");
                    sendM(att, z, cf);
                    // Close the memory stream.
                    mem.Close();
                    msgbox("Email was sent successfully");
                }
                }catch (Exception f)
            {

            
            }
            finally
            {
                e.Cancel = true;
            }

        }
        public void sendM(Attachment att, string acc, string accountname)
        {
            // Create a new message and attach the PDF report to it.

            // Send the e-mail message via the specified SMTP server.
            //SmtpClient smtp = new SmtpClient("smtp.somewhere.com");
            SmtpClient clients = new SmtpClient();
            clients.Port = 587;
            clients.Host = "smtp.gmail.com";
            clients.EnableSsl = true;
            ServicePointManager.ServerCertificateValidationCallback =
delegate (object s, X509Certificate certificate,
       X509Chain chain, SslPolicyErrors sslPolicyErrors)
{ return true; };
            clients.Timeout = 2000000;
            clients.DeliveryMethod = SmtpDeliveryMethod.Network;
            clients.UseDefaultCredentials = false;
            clients.Credentials = new System.Net.NetworkCredential("edocwebapp@gmail.com", "escrow123456789");

            MailMessage mail = new MailMessage();
            mail.Attachments.Add(att);

            // Specify sender and recipient options for the e-mail message.
            mail.From = new MailAddress("brokeroffice@escrowgroup.org", "Broker Office");
            try { 
            mail.To.Add(new MailAddress(investormail(acc), accountname));
        }
                    catch (Exception)
                    {
                     }
            try { 
            mail.CC.Add(new MailAddress(custodians(acc).Split(',')[1], custodians(acc).Split(',')[0]));
        }
                    catch (Exception)
                    {
                     }
            //mail.To.Add(new MailAddress("udeanmbano@gmail.com", accountname));
            //mail.To.Add(new MailAddress("memory@escrowgroup.org", accountname));
            mail.BodyEncoding = UTF8Encoding.UTF8;
            mail.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
            // Specify other e-mail options.
            mail.Subject = "AKRIBOS CAPITAL Broker Note";
            mail.Body = "Dear "+ accountname +"\n Please find attached your Broker Note";
            //mail.IsBodyHtml = true;
            clients.Send(mail);



        }
        public string custodians(string acc)
        {
            string validate = "";
            var client = new RestClient("http://localhost/BrokerService");
            var request = new RestRequest("Custodian/{s}", Method.POST);
            request.AddUrlSegment("s", acc);
            IRestResponse response = client.Execute(request);
            validate = response.Content;

            var Jsonobject = JsonConvert.DeserializeObject<string>(validate);

            validate = Jsonobject.ToString();


            return validate;


        }
        public string investormail(string num)
        {
            var p = db.Account_CreationPendingss.ToList().Where(a => a.CDSC_Number == num);
            string mm = "";
            foreach (var d in p)
            {
                mm = d.Emailaddress;
            }
            return mm;
        }

        protected void ASPxGridView1_CancelRowEditing(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
        {
           
        }

        protected void ASPxGridView1_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            try
            {
                 string i = e.Keys[ASPxGridView1.KeyFieldName].ToString();
                var cp = db.DealerDGs.ToList().Where(a=>a.Deal==i);
                foreach (var z in cp)
                {
                    var my = db.DealerDGs.Find(z.ID);
                    my.Printed = true;
                    db.DealerDGs.AddOrUpdate(my);
                    db.SaveChanges(WebSecurity.CurrentUserName);
                }
                var cpp = db.DealerDGs2.ToList().Where(a => a.Deal == i);
                foreach (var z in cpp)
                {
                    var my = db.DealerDGs2.Find(z.ID);
                    my.Printed = true;
                    db.DealerDGs2.AddOrUpdate(my);
                    db.SaveChanges(WebSecurity.CurrentUserName);
                }
                trades();
                msgbox("Deal "+i.ToString()+" has been audited.");

            }
            catch (Exception)
            {


            }
            finally
            {
                e.Cancel = true;
            }

        }

        protected void ASPxGridView1_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                object key = null;
                for (int i = 0; i < ASPxGridView1.VisibleRowCount; i++)
                {
                    if (ASPxGridView1.Selection.IsRowSelected(i))
                    {
                        key = ASPxGridView1.GetRowValues(i, "Deal");

                    }

                }

                string my = key.ToString();
                Label1.Text = my;                
            }
            catch (Exception)
            {


            }
        }

        protected void ASPxButton1_Click(object sender, EventArgs e)
        {
            if (Label1.Text=="")
            {
                msgbox("Select a record from the grid to view Broker note");
                return;
            }
            try
            {
                string i = Label1.Text;
                var cpz = db.DealerDGs.ToList().Where(a => a.Deal == i);
                foreach (var f in cpz)
                {
                    string ac = "";
                    string typo = "";
                    string typo2 = "";
                    if (f.Account1!= "ZSE")
                    {
                        ac = f.Account1;
                        typo = "BUY";
                        typo2 = "PURCHASE";
                    }
                    if (f.Account2!="ZSE")
                    {
                        ac = f.Account2;
                        typo = "SELL";
                        typo2 = "SALE";
                    }
                    XtraDeal report = new XtraDeal();
                    report.Parameters["Account"].Value = ac;

                    report.Parameters["OrderNo"].Value = f.Deal;
                    report.Parameters["SID"].Value = f.Deal;
                    report.Parameters["QTY"].Value = f.Quantity;
                    report.Parameters["Dte"].Value = f.PostDate;
                    report.Parameters["Max"].Value = f.Price;

                    report.Parameters["Broker"].Value = "AKRI";
                    report.Parameters["BrokerName"].Value = ("Akribos Securities").ToUpper();

                    string z = ac;
                    string cf = "";
                    var p = db.Account_Creations.ToList().Where(a => a.CDSC_Number == z);
                    foreach (var v in p)
                    {
                        cf = v.Surname_CompanyName + " " + v.OtherNames;
                        report.Parameters["Address"].Value = v.depname;
                    }
                    report.Parameters["TYPO"].Value = typo;

                    if (typo != "BUY")
                    {
                        report.Parameters["Stamp"].Value = "Stamp duty";
                    }
                    else
                    {
                        report.Parameters["Stamp"].Value = "Zim";
                    }
                    report.Parameters["TYPO2"].Value = typo2;
                    report.CreateDocument();
                    //report2
                    if (f.Account1!="ZSE" && f.Account2!="ZSE")
                    {
                        ac = f.Account1;
                        typo = "BUY";
                        typo2 = "PURCHASE";
                        XtraDeal report2 = new XtraDeal();
                        report2.Parameters["Account"].Value = ac;

                        report2.Parameters["OrderNo"].Value = f.Deal;
                        report2.Parameters["SID"].Value = f.Deal;
                        report2.Parameters["QTY"].Value = f.Quantity;
                        report2.Parameters["Dte"].Value = f.PostDate;
                        report2.Parameters["Max"].Value = f.Price;

                        report2.Parameters["Broker"].Value = "AKRI";
                        report2.Parameters["BrokerName"].Value = ("Akribos Securities").ToUpper();

                        z = ac;
                        cf = "";
                        p = db.Account_Creations.ToList().Where(a => a.CDSC_Number == z);
                        foreach (var v in p)
                        {
                            cf = v.Surname_CompanyName + " " + v.OtherNames;
                            report2.Parameters["Address"].Value = v.depname;
                        }
                        report2.Parameters["TYPO"].Value = typo;

                        if (typo != "BUY")
                        {
                            report2.Parameters["Stamp"].Value = "Stamp duty";
                        }
                        else
                        {
                            report2.Parameters["Stamp"].Value = "Zim";
                        }
                        report2.Parameters["TYPO2"].Value = typo2;
                        report2.CreateDocument();

                        int minPageCount = Math.Min(report.Pages.Count, report2.Pages.Count);
                        for (int g = 0; g < minPageCount; g++)
                        {
                            report.Pages.Insert(g * 2 + 1, report2.Pages[g]);
                        }
                        if (report2.Pages.Count != minPageCount)
                        {
                            for (int g = minPageCount; g < report2.Pages.Count; g++)
                            {
                                report.Pages.Add(report2.Pages[g]);
                            }
                        }

                        // Reset all page numbers in the resulting document. 
                        report.PrintingSystem.ContinuousPageNumbering = true;
                    }

                    ExportReport(report, "BrokerNote" + f.Account1 + ".pdf", "pdf", false);

                }
            }
            catch (Exception)
            {
               ;
            }
        }
        public void ExportReport(XtraReport report, string fileName, string fileType, bool inline)
        {
            MemoryStream stream = new MemoryStream();

            Response.Clear();

            if (fileType == "xls")
                report.ExportToXls(stream);
            if (fileType == "pdf")
                report.ExportToPdf(stream);
            if (fileType == "rtf")
                report.ExportToRtf(stream);
            if (fileType == "csv")
                report.ExportToCsv(stream);

            Response.ContentType = "application/" + fileType;
            Response.AddHeader("Accept-Header", stream.Length.ToString());
            Response.AddHeader("Content-Disposition", (inline ? "Inline" : "Attachment") + "; filename=" + fileName + "." + fileType);
            Response.AddHeader("Content-Length", stream.Length.ToString());
            //Response.ContentEncoding = System.Text.Encoding.Default;
            Response.BinaryWrite(stream.ToArray());
            Response.End();
        }

        protected void ASPxGridView2_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
        {
            try
            {

                string i = e.Keys[ASPxGridView2.KeyFieldName].ToString();
                var cpz = db.DealerDGs.ToList().Where(a => a.Deal == i);
                foreach (var f in cpz)
                {
                    string ac = "";
                    string typo = "";
                    string typo2 = "";
                    if (f.Account1 != "ZSE" || f.Account1 != "")
                    {
                        ac = f.Account1;
                        typo = "BUY";
                        typo2 = "PURCHASE";
                    }
                    if (f.Account2 != "ZSE" || f.Account2 != "")
                    {
                        ac = f.Account2;
                        typo = "SELL";
                        typo2 = "SALE";
                    }
                    XtraDeal report = new XtraDeal();
                    report.Parameters["Account"].Value = ac;

                    report.Parameters["OrderNo"].Value = f.Deal;
                    report.Parameters["SID"].Value = f.Deal;
                    report.Parameters["QTY"].Value = f.Quantity;
                    report.Parameters["Dte"].Value = f.PostDate;
                    report.Parameters["Max"].Value = f.Price;

                    report.Parameters["Broker"].Value = "AKRI";
                    report.Parameters["BrokerName"].Value = ("Akribos Securities").ToUpper();

                    string z = ac;
                    string cf = "";
                    var p = db.Account_Creations.ToList().Where(a => a.CDSC_Number == z);
                    foreach (var v in p)
                    {
                        cf = v.Surname_CompanyName + " " + v.OtherNames;
                        report.Parameters["Address"].Value = v.depname;
                    }
                    report.Parameters["TYPO"].Value = typo;

                    if (typo != "BUY")
                    {
                        report.Parameters["Stamp"].Value = "Stamp duty";
                    }
                    else
                    {
                        report.Parameters["Stamp"].Value = "Zim";
                    }
                    report.Parameters["TYPO2"].Value = typo2;
                    if (f.Account1 != "ZSE" && f.Account2 != "ZSE")
                    {
                        ac = f.Account1;
                        typo = "BUY";
                        typo2 = "PURCHASE";
                        XtraDeal report2 = new XtraDeal();
                        report2.Parameters["Account"].Value = ac;

                        report2.Parameters["OrderNo"].Value = f.Deal;
                        report2.Parameters["SID"].Value = f.Deal;
                        report2.Parameters["QTY"].Value = f.Quantity;
                        report2.Parameters["Dte"].Value = f.PostDate;
                        report2.Parameters["Max"].Value = f.Price;

                        report2.Parameters["Broker"].Value = "AKRI";
                        report2.Parameters["BrokerName"].Value = ("Akribos Securities").ToUpper();

                        z = ac;
                        cf = "";
                        p = db.Account_Creations.ToList().Where(a => a.CDSC_Number == z);
                        foreach (var v in p)
                        {
                            cf = v.Surname_CompanyName + " " + v.OtherNames;
                            report2.Parameters["Address"].Value = v.depname;
                        }
                        report2.Parameters["TYPO"].Value = typo;

                        if (typo != "BUY")
                        {
                            report2.Parameters["Stamp"].Value = "Stamp duty";
                        }
                        else
                        {
                            report2.Parameters["Stamp"].Value = "Zim";
                        }
                        report2.Parameters["TYPO2"].Value = typo2;
                        report2.CreateDocument();

                        int minPageCount = Math.Min(report.Pages.Count, report2.Pages.Count);
                        for (int g = 0; g < minPageCount; g++)
                        {
                            report.Pages.Insert(g * 2 + 1, report2.Pages[g]);
                        }
                        if (report2.Pages.Count != minPageCount)
                        {
                            for (int g = minPageCount; g < report2.Pages.Count; g++)
                            {
                                report.Pages.Add(report2.Pages[g]);
                            }
                        }

                        // Reset all page numbers in the resulting document. 
                        report.PrintingSystem.ContinuousPageNumbering = true;
                    }
                    //ASPxDocumentViewer1.Report = report;
                    // Create a new memory stream and export the report into it as PDF.
                    MemoryStream mem = new MemoryStream();
                    report.ExportToPdf(mem);

                    // Create a new attachment and put the PDF report into it.
                    mem.Seek(0, System.IO.SeekOrigin.Begin);
                    Attachment att = new Attachment(mem, "BrokerNote" + DateTime.Now.ToString("dd-MMM-yyyy") + ".pdf", "application/pdf");
                    sendM(att, z, cf);
                    // Close the memory stream.
                    mem.Close();
                    msgbox("Email was sent successfully");
                }
            }
            catch (Exception f)
            {


            }
            finally
            {
                e.Cancel = true;
            }

        }

        protected void ASPxGridView2_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {

        }

        protected void ASPxGridView2_SelectionChanged(object sender, EventArgs e)
        {

            try
            {
                object key = null;
                for (int i = 0; i < ASPxGridView2.VisibleRowCount; i++)
                {
                    if (ASPxGridView2.Selection.IsRowSelected(i))
                    {
                        key = ASPxGridView2.GetRowValues(i, "Deal");

                    }

                }

                string my = key.ToString();
                Label1.Text = my;
            }
            catch (Exception)
            {


            }
        }
    }
}
