﻿<%@ Page Language="C#" MaintainScrollPositionOnPostback="true" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="OrderPosting.aspx.cs" Inherits="BrokerOffice.ClientServices.OrderPosting" %>

<%@ Register Assembly="DevExpress.Web.v18.2, Version=18.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.XtraReports.v18.2.Web.WebForms, Version=18.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraReports.Web" TagPrefix="dx" %>


<asp:content id="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div>


  <div class="panel panel-default" style=" padding: 10px; margin: 10px">
    <div class="panel-heading" style="background-color:#428bca">
        <i style="color:white">Order Posting</i>
    </div>
        <div id="Tabs" role="tabpanel">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li class="active"><a href="#personal" aria-controls="personal" role="tab" data-toggle="tab">
                    </a></li>
                <li ></li>
          
            </ul>
            <!-- Tab panes -->
            <div class="tab-content" style="padding-top: 20px">
                  <div role="tabpanel" style="background-color:#BFF794"  class="tab-pane active" id="personal">

                               <table class="table table-responsive">
                                   <tr>      <td>
                                           <asp:Label ID="Label2"  runat="server" Text="Select Exchange " Font-Bold="True"></asp:Label>
                                           <br />
                                          <asp:DropDownList CssClass="form-control" AutoPostBack="false"  ID="drpTP" runat="server"  Width="250px"></asp:DropDownList>
                                       </td>
                                      <td>
                                   <asp:Label runat="server"  Text="Order Type" Font-Bold="True"></asp:Label>
                                                <br />
                                <asp:DropDownList id="drporder" CssClass="form-control"  runat="server" Width="250px" OnSelectedIndexChanged="drporder_SelectedIndexChanged" >
      <asp:ListItem Text="Buy" Value="Buy"></asp:ListItem>
      <asp:ListItem Text="Sell" Value="Sell"></asp:ListItem>
                                </asp:DropDownList>
                                          <script src="../Scripts/jquery-1.10.2.min.js"></script>
  
<script type="text/javascript">
    $(function () {
        $("#<%= drporder.ClientID%>").change(function () {
            var selectedText = $(this).find("option:selected").text();
            var selectedValue = $(this).val();
            var defvalue ="<%= this.Page.IsPostBack %>";
            
            if (selectedValue == "Buy") {
                $('#' + personal.id).css({ "background-color": "#BFF794" });
            }else if (selectedValue == "Sell") {
                $('#' + personal.id).css({ "background-color": "#F9E3CA" });
            }
        });
    });
</script>
<script type='text/javascript'>
    $(document).ready(function(){
   var shipList = document.getElementById('<%=drporder.ClientID%>');
    var shipCost = shipList.options[shipList.selectedIndex].value;

    if (shipCost == "Buy") {
        $('#' + personal.id).css({ "background-color": "#BFF794" });
    } else if (shipCost == "Sell") {
        $('#' + personal.id).css({ "background-color": "#F9E3CA" });
    }
    });
    
</script>

                                      </td>
                                 
                                      <td>
                                   <asp:Label runat="server"  Text="Security" Font-Bold="True"></asp:Label>
                                          <br />
                                        <asp:DropDownList CssClass="form-control" ID="Company" runat="server" Width="250px" OnSelectedIndexChanged="Company_SelectedIndexChanged" AutoPostBack="false"></asp:DropDownList>
                                    
                                    </td>
                                   </tr>
                                <tr>
                                    <asp:Label ID="txtFkey" runat="server" Visible="False" Text=""></asp:Label>
                                  
<td>
<asp:Label runat="server"  Text="Investor" Font-Bold="True"></asp:Label>
                                         <br />
                                       <dx:ASPxComboBox ID="drpname" runat="server" ValueType="System.String" OnSelectedIndexChanged="drpname_SelectedIndexChanged" AutoPostBack="true" Height="17px" TextField="FullNames" Theme="BlackGlass" ValueField="id" Width="250px"></dx:ASPxComboBox>
                                         
                                    </td>
                                    <td>
<asp:Label runat="server"  Text="Client Name" Font-Bold="True"></asp:Label>
                                        <br />
<asp:TextBox id="Shareholder" ReadOnly="true" CssClass="form-control" runat="server"  OnTextChanged="Shareholder_TextChanged" Width="250px"></asp:TextBox>
    
                                   </td>
                                             <td>
                                                    <asp:Label runat="server"  Text="FOK" Font-Bold="True"></asp:Label>
                                                   <br />  
                                              <asp:DropDownList ID="DropDownList2" CssClass="form-control" runat="server" Width="250px">
                                                  <asp:ListItem Text="--Select FOK--" Value="--Select Order Qualifier--"></asp:ListItem>
                                                  <asp:ListItem Text="Yes" Value="True"></asp:ListItem>
                                                   <asp:ListItem Text="No" Value="False"></asp:ListItem>
                                                  
                                                    </asp:DropDownList>
                                      

                                         </td>

                                </tr>
                                     <tr>
                                   
                                    <td>
<asp:Label runat="server"  Text="Quantity" Font-Bold="True"></asp:Label>  
                                   <br />
<asp:TextBox id="qty" CssClass="form-control" runat="server" Width="250px" AutoPostBack="True" OnTextChanged="qty_TextChanged"></asp:TextBox>
                                     
                                    </td>
                                     <td>
                                             <asp:Label runat="server"  Text="Price" Font-Bold="True"></asp:Label>
                                        <br />
<asp:TextBox id="txtbase" CssClass="form-control" runat="server" Width="250px" AutoPostBack="True" OnTextChanged="txtbase_TextChanged"></asp:TextBox>
                       
                                    </td>
                                         <td>
                                                    <asp:Label runat="server"  Text="Order Qualifier" Font-Bold="True"></asp:Label>
                                                   <br />  
                                              <asp:DropDownList ID="DropDownList1" CssClass="form-control" runat="server" Width="250px">
                                                  <asp:ListItem Text="--Select Order Qualifier--" Value="--Select Order Qualifier--"></asp:ListItem>
                                                  <asp:ListItem Text="DayOrder" Value="DayOrder"></asp:ListItem>
                                                   <asp:ListItem Text="GTC" Value="GTC"></asp:ListItem>
                                                   <asp:ListItem Text="GTD" Value="GTD"></asp:ListItem>
                                                   <asp:ListItem Text="FOK" Value="FOK"></asp:ListItem>
                                                    </asp:DropDownList>
                                      

                                         </td>
                                </tr>
                                   <tr>
                                       <td>
                                                  <asp:Label runat="server"  Text="Broker/Fund Manager" Font-Bold="True"></asp:Label>
                                                   <br /> 
                                             <asp:DropDownList ID="DropDownList3" CssClass="form-control" runat="server" Width="250px" OnSelectedIndexChanged="DropDownList3_SelectedIndexChanged">
                                                  <asp:ListItem Text="--Select Broker--" Value="--Select Broker--"></asp:ListItem>
                                                   </asp:DropDownList>
                                       </td>
                                       <td>
                                            <asp:Label runat="server"  Text="Order Date" Font-Bold="True"></asp:Label>
                                                   <br /> 
                                           <dx:ASPxDateEdit CssClass="form-control" ID="ASPxDateEdit1" runat="server"  Theme="Moderno" width="250px" Height="39px"></dx:ASPxDateEdit>
                                       </td>
                                       <td>

                                       </td>
                                   </tr>
                                     <tr>
                                   
                                    <td>
                                        <br /><asp:Button ID="btnInvoice" runat="server" CssClass="btn btn-primary" Text="Print Quotation" OnClick="btnInvoice_Click"  ></asp:Button>
                      
<asp:Button ID="Submit" runat="server" CssClass="btn btn-primary" Text="Submit" OnClick="Submit_Click" ></asp:Button>
              </td>
                                   <td>
                                       <br />
                                     
                                    </td>
                                         <td>
                                <br />
                       
                                         </td>
                                </tr>
                             </table>
                            <dx style="float:right"><dx:ASPxButton ID="dxAction" runat="server" CssClass="btn btn-primary" Theme="Moderno" Text="Next" OnClick="dxAction_Click" Visible="False" ></dx:ASPxButton></dx>
              
                </div>
                <div role="tabpanel" class="tab-pane" id="Action">
                  <table class="table table-striped">
                      <tr>
                          <td>
                                    <asp:Label runat="server" CssClass="control-label col-md-2" Visible="false" Text="SecurityType "></asp:Label>
                                        </td>
                          <td>
<asp:Label runat="server" CssClass="control-label col-md-2" Text=" OrderPref " Visible="False"></asp:Label>  </td>
                          <td>
<asp:Label runat="server" CssClass="control-label col-md-2" Text="Marketboard" Visible="False"></asp:Label> 
                              <br />
                          </td>
                      </tr>
                             <tr>
                          <td>
                          <asp:DropDownList id="drpSecurity" CssClass="form-control" Visible="false" runat="server" Width="150px" />
    
                                   </td>
                          <td>
<asp:DropDownList id="ordepref" CssClass="form-control" runat="server" Width="150px" Visible="False">
    <asp:ListItem>L</asp:ListItem>
</asp:DropDownList>
                                <br />
                          </td>
                          <td>
                              <asp:DropDownList ID="mrktBoard" runat="server" CssClass="form-control"  runat="server" Width="150px" Visible="False">

                                  <asp:ListItem>Normal Board</asp:ListItem>
                                  <asp:ListItem></asp:ListItem>
                              </asp:DropDownList>

                              <br />
                          </td>
                      </tr>
                      <tr>
                          <td>
<asp:Label runat="server" CssClass="control-label col-md-2" Text="Currency($)" Visible="False"></asp:Label>  
                              <br /> 

<asp:DropDownList id="drpCurr" CssClass="form-control" Visible="False" runat="server" Width="150px"></asp:DropDownList>

                              <br />
                          </td>
                          <td>
                              <br />

                          </td>  
                          <td>
                              <br />
                           </td>
                      </tr>
                      <tr>
                    
                           <td>
                               <br />
                           </td>
                          <td>
                              <br />
                          </td>
                          <td></td>
                      </tr>
                    
                             
                      <tr>
                          
                          <td>
                             <asp:Label runat="server" Text="Available Securities" Font-Bold="True" Visible="False"></asp:Label>
                                                                             <br />
<asp:TextBox id="availableshrs" CssClass="form-control" runat="server"  Width="250px" Visible="False"></asp:TextBox>
                                    </td>
                 <td>
                     &nbsp;</td>
                               <td>
                                           <asp:HiddenField ID="TabName" runat="server" />
                                   <asp:Label ID="Label1" runat="server" Visibility="hidden" ></asp:Label>
                                         <asp:Label ID="Label44" runat="server" Visible="false" Text=""></asp:Label>
                                     
                                       </td>
                          
                      </tr>
                  </table>
                      <dx style="float:right"><dx:ASPxButton ID="dxPersonal" runat="server" CssClass="btn btn-primary" Theme="Moderno" Text="Previous" OnClick="dxPersonal_Click"></dx:ASPxButton></dx>
              
                </div>
      
            </div>
        </div>
    </div>
        <dx:ASPxPopupControl ID="ASPxPopupControl1" runat="server" FooterText="Search Customer" HeaderText="Search Customer" Width="1184px" Modal="True" ScrollBars="Both" AllowDragging="True" ShowPageScrollbarWhenModal="True" Top="15" CloseAction="CloseButton">

             <ContentCollection>
                <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
           <iframe name="myIframe" src="" id="myIframe"  width="1184px" height="800px" runat =server></iframe>

                      </dx:PopupControlContentControl>
            </ContentCollection>
     
            
      </dx:ASPxPopupControl>
    </div>
      <script type="text/javascript">
$(function () {
    var tabName = $("[id*=TabName]").val() != "" ? $("[id*=TabName]").val() : "personal";
    $('#Tabs a[href="#' + tabName + '"]').tab('show');
    $("#Tabs a").click(function () {
        $("[id*=TabName]").val($(this).attr("href").replace("#", ""));
    });
});
</script>

</asp:content>
