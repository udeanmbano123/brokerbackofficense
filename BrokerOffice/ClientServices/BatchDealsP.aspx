﻿<%@ Page Language="C#" Async="true"   AutoEventWireup="true" MaintainScrollPositionOnPostback="true" MasterPageFile="~/Site1.Master" CodeBehind="BatchDealsP.aspx.cs" Inherits="BrokerOffice.ClientServices.BatchDealsP" %>
<%@ Register Assembly="DevExpress.Web.v18.2, Version=18.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.XtraReports.v18.2.Web.WebForms, Version=18.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraReports.Web" TagPrefix="dx" %>


<asp:content id="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type = "text/javascript">
    function ConfirmDelete()
    {
       var count = document.getElementById("<%=hfCount.ClientID %>").value;
       var gv = document.getElementById("<%=gvAll.ClientID%>");
       var chk = gv.getElementsByTagName("input");
       for(var i=0;i<chk.length;i++)
       {
            if(chk[i].checked && chk[i].id.indexOf("chkAll") == -1)
            {
                count++;
            }
       }
       if(count == 0)
       {
            alert("No records to print.");
            return false;
       }
       else
       {
            return confirm("Do you want to print " + count + " the General ledger statements.");
       }
    }
       function Check_Click(objRef) {
           //Get the Row based on checkbox
           var row = objRef.parentNode.parentNode;
           if (objRef.checked) {
               //If checked change color to Aqua
               row.style.backgroundColor = "aqua";
           }
           else {
               //If not checked change back to original color
               if (row.rowIndex % 2 == 0) {
                   //Alternating Row Color
                   row.style.backgroundColor = "#C2D69B";
               }
               else {
                   row.style.backgroundColor = "white";
               }
           }

           //Get the reference of GridView
           var GridView = row.parentNode;

           //Get all input elements in Gridview
           var inputList = GridView.getElementsByTagName("input");

           for (var i = 0; i < inputList.length; i++) {
               //The First element is the Header Checkbox
               var headerCheckBox = inputList[0];

               //Based on all or none checkboxes
               //are checked check/uncheck Header Checkbox
               var checked = true;
               if (inputList[i].type == "checkbox" && inputList[i] != headerCheckBox) {
                   if (!inputList[i].checked) {
                       checked = false;
                       break;
                   }
               }
           }
           headerCheckBox.checked = checked;

       }
       function checkAll(objRef) {
           var GridView = objRef.parentNode.parentNode.parentNode;
           var inputList = GridView.getElementsByTagName("input");
           for (var i = 0; i < inputList.length; i++) {
               //Get the Cell To find out ColumnIndex
               var row = inputList[i].parentNode.parentNode;
               if (inputList[i].type == "checkbox" && objRef != inputList[i]) {
                   if (objRef.checked) {
                       //If the header checkbox is checked
                       //check all checkboxes
                       //and highlight all rows
                       row.style.backgroundColor = "aqua";
                       inputList[i].checked = true;
                   }
                   else {
                       //If the header checkbox is checked
                       //uncheck all checkboxes
                       //and change rowcolor back to original
                       if (row.rowIndex % 2 == 0) {
                           //Alternating Row Color
                           row.style.backgroundColor = "#C2D69B";
                       }
                       else {
                           row.style.backgroundColor = "white";
                       }
                       inputList[i].checked = false;
                   }
               }
           }
       }
       function MouseEvents(objRef, evt) {
           var checkbox = objRef.getElementsByTagName("input")[0];
           if (evt.type == "mouseover") {
               objRef.style.backgroundColor = "orange";
           }
           else {
               if (checkbox.checked) {
                   objRef.style.backgroundColor = "aqua";
               }
               else if (evt.type == "mouseout") {
                   if (objRef.rowIndex % 2 == 0) {
                       //Alternating Row Color
                       objRef.style.backgroundColor = "#C2D69B";
                   }
                   else {
                       objRef.style.backgroundColor = "white";
                   }
               }
           }
       }
</script>
  <div style="background:white" class="row">
    <div class="panel-heading" style="background-color:#428bca">
        <i style="color:white">General Ledger Exporter</i>
       
    </div> 

<table>

    <tr>
        <td>
<dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="Range" Theme="Glass"></dx:ASPxLabel>

        </td>
           <td>
                   <asp:RadioButtonList ID="JGender" runat="server" RepeatDirection="Horizontal" Height="20px" Width="227px" Font-Size="Small">
                                     <asp:ListItem Text="A-F" Value="A-F"></asp:ListItem>
                                                    <asp:ListItem Text="G-L" Value="G-L"></asp:ListItem>

                          <asp:ListItem Text="M-R" Value="M-R"></asp:ListItem>
                          <asp:ListItem Text="S-Z" Value="S-Z"></asp:ListItem>

                          </asp:RadioButtonList>
           </td>
         <td>
            <dx:ASPxButton ID="btnSearch" runat="server" CssClass="btn btn-primary" Text="Search" OnClick="btnSearch_Click" style="height: 23px"></dx:ASPxButton>
       </td>
    </tr>
    <tr>
       
       
         <td>
 
                
        </td>
        <td>
 <dx:ASPxTextBox ID="txtSearch" Visible="false" runat="server" CssClass="form-control" Width="250px"></dx:ASPxTextBox>
          
        </td>
    </tr>
    </table>
    <br />

      <table>
  <tr>
 
          <td>
              <asp:GridView ID="gvAll" CssClass="table table-striped" runat="server"
    AutoGenerateColumns = "false" Font-Names = "Arial"
    Font-Size = "11pt" AlternatingRowStyle-BackColor ="#AFEEEE" 
    HeaderStyle-BackColor = "#FFFFFF" AllowPaging ="true"  
    OnPageIndexChanging = "OnPaging" DataKeyNames="AccountNumber"
    PageSize = "100" OnRowDataBound="gvAll_RowDataBound" Width="744px" AllowCustomPaging="True" OnSelectedIndexChanged="gvAll_SelectedIndexChanged" >
   <Columns>
    <asp:TemplateField>
        <HeaderTemplate>
            <asp:CheckBox ID="chkAll" runat="server"
             onclick = "checkAll(this);" />
        </HeaderTemplate>
        <ItemTemplate>
            <asp:CheckBox ID="chk" runat="server"
             onclick = "Check_Click(this)"/>
        </ItemTemplate>
    </asp:TemplateField>
       <asp:BoundField ItemStyle-Width = "150px" DataField = "AccountNumber"
       HeaderText = "Account Number"/>
      <asp:BoundField ItemStyle-Width = "150px" DataField = "AccountName"
       HeaderText = "Account Name"/>    
    <asp:BoundField ItemStyle-Width = "150px" DataField = "GroupName"
       HeaderText = "GroupName"/>
            </Columns>
   <AlternatingRowStyle BackColor="#C2D69B"  />
</asp:GridView>
          
          
      </td>
       
  </tr>
          </table>
      <table>
    <tr>  <td>
             <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="From"></dx:ASPxLabel>
             <dx:ASPxDateEdit ID="ASPxDateEdit1" runat="server" Theme="BlackGlass"></dx:ASPxDateEdit>
                        </td>
                        <td>
                            <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="To"></dx:ASPxLabel>
                            <dx:ASPxDateEdit ID="ASPxDateEdit2" runat="server" Theme="BlackGlass"></dx:ASPxDateEdit>
                        </td>
        <td>
                <asp:DropDownList ID="dep"  CssClass="form-control" runat="server" Width="289px">
                     <asp:ListItem Text="Portable Document Format(pdf)" Value="pdf"></asp:ListItem>
                     <asp:ListItem Text="Excel Workbook(Xlsx)" Value="xlsx"></asp:ListItem>
                         <asp:ListItem Text="Excel Workbook(xls)" Value="xls"></asp:ListItem>
                   <asp:ListItem Text="Rich Text Format" Value="rtf"></asp:ListItem>
                    <asp:ListItem Text="Comma Separated Value(csv)" Value="csv"></asp:ListItem>
                </asp:DropDownList> 

        </td>
        <td>
             <dx:ASPxLabel ID="ASPxLabel4" runat="server" Text=""></dx:ASPxLabel>
     <asp:Button ID="Button1"  OnClientClick = "return ConfirmDelete();"  CssClass="btn btn-primary" runat="server" Text="Print Ledgers" Click="ASPxButton1_Click" OnClick="Button1_Click"></asp:Button>
             <asp:Button ID="Button2" runat="server" OnClick="Button2_Click" Text="Button" Visible="False" />
        </td>
          <br />
            <asp:HiddenField ID="hfCount" runat="server" Value = "0" />
   
        
    </tr>

      


      

   </table>
  

</asp:content>
<asp:Content ID="Content2" runat="server" contentplaceholderid="head">
        <!-- Bootstrap Core CSS -->
    <link href="<%= Page.ResolveUrl("~/vendor/bootstrap/css/bootstrap.min.css")%>" rel="stylesheet">
                                                                                            <!-- MetisMenu CSS -->
                                         <%--                                                   <link href="<%=baseUrl%>/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">--%>
    <!-- Custom CSS -->
    <link href="<%= Page.ResolveUrl("~/dist/css/sb-admin-2.css")%>" rel="stylesheet">
    <link href="<%= Page.ResolveUrl("~/vendor/morrisjs/morris.css")%>" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="<%= Page.ResolveUrl("~/Content/font-awesome.min.css")%>" rel="stylesheet" />
    <link href="<%= Page.ResolveUrl("~/Content/font-awesome.css")%>" rel="stylesheet" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script src="<%= Page.ResolveUrl("~/Scripts/jquery-1.10.2.min.js")%>"></script>
         <style type="text/css">

        .nav-tabs a, .nav-tabs a:hover, .nav-tabs a:focus
        {
            outline: 0;
        }
    </style>
</asp:Content>
