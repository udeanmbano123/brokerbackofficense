﻿using BrokerOffice.ATS;
using BrokerOffice.DAO;
using BrokerOffice.DealClass;
using BrokerOffice.DealNotes;
using BrokerOffice.Models;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebMatrix.WebData;
using System.Transactions;
using System.Collections;
using System.Text;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.IO;
using DevExpress.XtraReports.UI;
using System.Net;
using Microsoft.Win32;
using Ionic.Zip;

namespace BrokerOffice.ClientServices
{
    public partial class BatchDealsP : System.Web.UI.Page
    {
        public string id = "";
        private SBoardContext db = new SBoardContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                try
                {
                    GetData();
                }
                catch (Exception)
                {


                }
            }
            try
            {
                BindGrids();
            }
            catch (Exception)
            {

            }
            try
            {
                id = Request.QueryString["id"];
                if (this.IsPostBack)
                {

                }
                else
                {



                    if (id == " " || id == null)
                    {

                    }
                    else
                    {
                        // getDetails(id);
                    }
                }

            }
            catch (Exception)
            {
                //Response.Redirect("~/User/Index", false);        //write redirect
                //Context.ApplicationInstance.CompleteRequest(); // end response


            }

        }

        public void BindGrids()
        {
            string nme = "";
            var users = db.Users.ToList().Where(a => a.Email == WebSecurity.CurrentUserName);

            foreach (var dd in users)
            {
                nme = dd.BrokerCode;
            }
            string nm = JGender.SelectedValue.ToString();
            var l1 = nm.Split('-')[0];
            var l2 = nm.Split('-')[1];
            //gvAll.DataSource = buy.Where(a =>System.DateTime.Parse(a.DatePosted)>date && System.DateTime.Parse(a.DatePosted)<date3).ToList();


            char n1 = l1[0];
            char n2 = l2[0];
            int count = 0;
            List<ACN> neme = new List<ACN>();
            for (char c = n1; c <= n2; c++)
            {
                count = count + 1;
                var ab = db.Accounts_Masters.ToList().Where(a => a.AccountName.StartsWith(c.ToString()));

                foreach (var p in ab) {
                    neme.Add(new Models.ACN() { AccountName = p.AccountName, AccountNumber = p.AccountNumber, GroupName = p.GroupName });
                }

            }
            gvAll.DataSource = neme.OrderBy(a => a.AccountName).ToList();

            // gvAll.DataSource = fn.ToList();
            gvAll.DataBind();
        }

        public List<BatchD> GetEarners(DateTime a, DateTime b)
        {
            //return listEmp.First(e => e.ID == id); 

            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["SBoardConnection"].ConnectionString;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = "Select [Security] As 'Security',Deal As 'Deal',Quantity as 'Quantity',Price as 'Price',AccountName1 as 'BuyerClient',AccountName2 as SellerClient,PostDate as 'DatePosted',SettDate as 'SettlementDate',DealerDG.[Status] as 'Status'  from DealerDG inner join  Account_Creation ON DealerDG.Account2=Account_Creation.CDSC_Number where CAST(PostDate As Date)>='" + a.ToString("dd-MMM-yyyy") + "' And CAST(PostDate As Date)<='" + b.ToString("dd-MMM-yyyy") + "' And DealerDg.[Status] is null or DealerDg.[Status]='None' ";
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<BatchD>();
            while (reader.Read())
            {
                var accountDetails = new BatchD
                {

                    Security = reader.GetValue(0).ToString(),
                    Deal = reader.GetValue(1).ToString(),
                    Quantity = reader.GetValue(2).ToString(),
                    Price = reader.GetValue(3).ToString(),
                    BuyerClient = reader.GetValue(4).ToString(),
                    SellerClient = reader.GetValue(5).ToString(),
                    DatePosted = reader.GetValue(6).ToString(),
                    SettlementDate = reader.GetValue(7).ToString(),
                    Status = reader.GetValue(8).ToString(),
                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();

            return accDetails;
        }
        public void msgbox(string strMessage)
        {
            string strScript = "<script language=JavaScript>";
            strScript += "window.alert(\"" + strMessage + "\");";
            strScript += "</script>";
            System.Web.UI.WebControls.Label lbl = new System.Web.UI.WebControls.Label();
            lbl.Text = strScript;
            Page.Controls.Add(lbl);
        }
        public string GetNames(string acc)
        {
            var c = db.Account_Creations.ToList().Where(a => a.CDSC_Number == acc);
            string nme = "";
            foreach (var p in c)
            {
                nme = p.Surname_CompanyName + " " + p.OtherNames;
            }
            return nme;
        }


        protected void OnPaging(object sender, GridViewPageEventArgs e)
        {
            gvAll.PageIndex = e.NewPageIndex;
            gvAll.DataBind();
            BindGrids();
        }
        private void SetData()
        {
            int currentCount = 0;
            CheckBox chkAll = (CheckBox)gvAll.HeaderRow
                                    .Cells[0].FindControl("chkAll");
            chkAll.Checked = true;
            ArrayList arr = (ArrayList)ViewState["SelectedRecords"];
            for (int i = 0; i < gvAll.Rows.Count; i++)
            {
                CheckBox chk = (CheckBox)gvAll.Rows[i]
                                .Cells[0].FindControl("chk");
                if (chk != null)
                {
                    chk.Checked = arr.Contains(gvAll.DataKeys[i].Value);
                    if (!chk.Checked)
                        chkAll.Checked = false;
                    else
                        currentCount++;
                }
            }
            hfCount.Value = (arr.Count - currentCount).ToString();
        }
        private void GetData()
        {
            ArrayList arr;
            if (ViewState["SelectedRecords"] != null)
                arr = (ArrayList)ViewState["SelectedRecords"];
            else
                arr = new ArrayList();
            CheckBox chkAll = (CheckBox)gvAll.HeaderRow
                                .Cells[0].FindControl("chkAll");
            for (int i = 0; i < gvAll.Rows.Count; i++)
            {
                if (chkAll.Checked)
                {
                    if (!arr.Contains(gvAll.DataKeys[i].Value))
                    {
                        arr.Add(gvAll.DataKeys[i].Value);
                    }
                }
                else
                {
                    CheckBox chk = (CheckBox)gvAll.Rows[i]
                                       .Cells[0].FindControl("chk");
                    if (chk.Checked)
                    {
                        if (!arr.Contains(gvAll.DataKeys[i].Value))
                        {
                            arr.Add(gvAll.DataKeys[i].Value);
                        }
                    }
                    else
                    {
                        if (arr.Contains(gvAll.DataKeys[i].Value))
                        {
                            arr.Remove(gvAll.DataKeys[i].Value);
                        }
                    }
                }
            }
            ViewState["SelectedRecords"] = arr;
        }
        protected void ASPxButton1_Click(object sender, EventArgs e)
        {
            int count = 0;
            SetData();
            gvAll.AllowPaging = false;
            gvAll.DataBind();
            ArrayList arr = (ArrayList)ViewState["SelectedRecords"];
            count = arr.Count;
            for (int i = 0; i < gvAll.Rows.Count; i++)
            {
                if (arr.Contains(gvAll.DataKeys[i].Value))
                {
                    //DeleteRecord(gvAll.DataKeys[i].Value.ToString());
                    var dbc = db.DealerDGs.ToList().Where(a => a.Deal == gvAll.DataKeys[i].Value.ToString());
                    foreach (var c in dbc)
                    {
                        DealerDG my = db.DealerDGs.Find(c.ID);
                        my.Status = "SETTLED";
                        db.DealerDGs.AddOrUpdate(my);
                        db.SaveChanges(WebSecurity.CurrentUserName);
                    }
                    arr.Remove(gvAll.DataKeys[i].Value);
                }
            }
            ViewState["SelectedRecords"] = arr;
            hfCount.Value = "0";
            gvAll.AllowPaging = true;
            BindGrids();
            ShowMessage(count);
        }
        private void ShowMessage(int count)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<script type = 'text/javascript'>");
            sb.Append("alert('");
            sb.Append(count.ToString());
            sb.Append(" General Ledgers have been printed.');");
            sb.Append("</script>");
            ClientScript.RegisterStartupScript(this.GetType(),
                            "script", sb.ToString());
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (ASPxDateEdit1.Text == "" || ASPxDateEdit2.Text == "")
            {
                msgbox("Please select from date and to date");
                return;
            }
            if (dep.SelectedValue == "Select format")
            {
                msgbox("Please select file format ");
                return;
            }
            if (gvAll.Rows.Count == 0)
            {
                msgbox("The grid must contain sell deals");
                return;
            }
            DateTime date = Convert.ToDateTime(ASPxDateEdit1.Text);
            DateTime date2 = Convert.ToDateTime(ASPxDateEdit2.Text);

            int count = 0;
            SetData();
            gvAll.AllowPaging = false;
            gvAll.DataBind();
            ArrayList arr = (ArrayList)ViewState["SelectedRecords"];
            count = arr.Count;
            
            string baseUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/');
            List<string> nm = new List<string>();
            for (int i = 0; i < gvAll.Rows.Count; i++)
            {
                if (arr.Contains(gvAll.DataKeys[i].Value))
                {
                    //HttpWebRequest request = (HttpWebRequest)WebRequest.Create(baseUrl+"/AccountCreation/Filedownload.ashx?id=1");
                    // HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                    //using (var client = new WebClient())
                    //{
                    //   client.Headers["User-Agent"] ="Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.85 Safari/537.36";
                    //    string desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);

                    //    client.DownloadFileAsync(new Uri(baseUrl+ "/ClientServices/WebForm1.aspx?id="+ gvAll.DataKeys[i].Value.ToString() + "&d1="+ date.ToString("dd-MMM-yyyy")+ "&d2=" + date2.ToString("dd-MMM-yyyy") + "&dep=" + dep.SelectedValue.ToString() + ""), mee);
                    //}
                    string mee = ne(gvAll.DataKeys[i].Value.ToString()) + gvAll.DataKeys[i].Value.ToString() + DateTime.Now.ToString("ddmmyyyyhhmmss") + "." + dep.SelectedValue.ToString();
                    var cb = 0;
                    int mo = Convert.ToInt32(gvAll.DataKeys[i].Value.ToString());
                    try
                    {
                        cb = db.TradingChargess.Where(a => a.chargeaccountcode == mo).ToList().Count();

                    }
                    catch (Exception)
                    {

                        throw;
                    }
                    //DeleteRecord(gvAll.DataKeys[i].Value.ToString());
                    var dbccz = db.Accounts_Masters.ToList().Where(a => a.AccountNumber.ToString() == gvAll.DataKeys[i].Value.ToString());
                    foreach (var c in dbccz)
                    {
                        if (c.AccountNumber>=6000 && c.AccountNumber<8000)
                        {
                            BrokerOffice.Reporting.XtraAccStmt2 report1 = new BrokerOffice.Reporting.XtraAccStmt2();
                            //parameters
                            var ccd = 0;
                            report1.Parameters["Account"].Value = c.AccountNumber;
                            report1.Parameters["AccountName"].Value = c.AccountName;

                            report1.Parameters["FDate"].Value = date;
                            report1.Parameters["TDate"].Value = date2;
                            //report.Parameters["yourParameter2"].Value = secondValue;
                            report1.CreateDocument();
                            // Reset all page numbers in the resulting document. 
                            report1.PrintingSystem.ContinuousPageNumbering = true;
                            string nme33 = date.ToString("dd-MMM-yyyy") + "-" + date2.ToString("dd-MMM-yyyy") + DateTime.Now.ToString("ddMMMyyyyhhmmss");
                            // Create a Print Tool and show the Print Preview form. 
                            //report1.ExportToPdf();
                            //ExportReport(report1, report1.Parameters["AccountName"].Value + nme33 + "." + dep.SelectedValue, dep.SelectedValue, false);
                            nm.Add(report1.Parameters["AccountName"].Value + nme33 + "." + dep.SelectedValue);
                            if (dep.SelectedValue == "xlsx")
                                report1.ExportToXlsx(Server.MapPath("~/File/" + report1.Parameters["AccountName"].Value + nme33 + "." + dep.SelectedValue));
                            if (dep.SelectedValue == "pdf")
                                report1.ExportToPdf(Server.MapPath("~/File/" + report1.Parameters["AccountName"].Value + nme33 + "." + dep.SelectedValue));
                            if (dep.SelectedValue == "rtf")
                                report1.ExportToRtf(Server.MapPath("~/File/" + report1.Parameters["AccountName"].Value + nme33 + "." + dep.SelectedValue));
                            if (dep.SelectedValue == "csv")
                                report1.ExportToCsv(Server.MapPath("~/File/" + report1.Parameters["AccountName"].Value + nme33 + "." + dep.SelectedValue));

                        }
                        else if (cb == 0 && !(c.AccountNumber >= 6000 && c.AccountNumber < 8000))
                        {
                            BrokerOffice.Reporting.XtraAccStmt report1 = new BrokerOffice.Reporting.XtraAccStmt();
                            //parameters
                            var ccd = 0;
                            report1.Parameters["Account"].Value = c.AccountNumber;
                            report1.Parameters["AccountName"].Value = c.AccountName;

                            report1.Parameters["FDate"].Value = date;
                            report1.Parameters["TDate"].Value = date2;
                            //report.Parameters["yourParameter2"].Value = secondValue;
                            report1.CreateDocument();
                            // Reset all page numbers in the resulting document. 
                            report1.PrintingSystem.ContinuousPageNumbering = true;
                            string nme33 = date.ToString("dd-MMM-yyyy") + "-" + date2.ToString("dd-MMM-yyyy") + DateTime.Now.ToString("ddMMMyyyyhhmmss");
                            // Create a Print Tool and show the Print Preview form. 
                            //report1.ExportToPdf();
                            //ExportReport(report1, report1.Parameters["AccountName"].Value + nme33 + "." + dep.SelectedValue, dep.SelectedValue, false);
                            nm.Add(report1.Parameters["AccountName"].Value + nme33 + "." + dep.SelectedValue);
                            if (dep.SelectedValue == "xlsx")
                                report1.ExportToXlsx(Server.MapPath("~/File/" + report1.Parameters["AccountName"].Value + nme33 + "." + dep.SelectedValue));
                            if (dep.SelectedValue == "pdf")
                                report1.ExportToPdf(Server.MapPath("~/File/" + report1.Parameters["AccountName"].Value + nme33 + "." + dep.SelectedValue));
                            if (dep.SelectedValue == "rtf")
                                report1.ExportToRtf(Server.MapPath("~/File/" + report1.Parameters["AccountName"].Value + nme33 + "." + dep.SelectedValue));
                            if (dep.SelectedValue == "csv")
                                report1.ExportToCsv(Server.MapPath("~/File/" + report1.Parameters["AccountName"].Value + nme33 + "." + dep.SelectedValue));

                        }
                        else if (cb > 0)
                        {


                            BrokerOffice.Reporting.XtraTaxesF report1 = new BrokerOffice.Reporting.XtraTaxesF();
                            //parameters
                            var ccd = 0;
                            report1.Parameters["Account"].Value = c.AccountNumber;
                            report1.Parameters["AccountName"].Value = c.AccountName;
                            report1.Parameters["FDate"].Value = date;
                            report1.Parameters["TDate"].Value = date2;
                            //report.Parameters["yourParameter2"].Value = secondValue;
                            report1.CreateDocument();
                            // Reset all page numbers in the resulting document. 
                            report1.PrintingSystem.ContinuousPageNumbering = true;
                            string nme2 = date.ToString("dd-MMM-yyyy") + "-" + date2.ToString("dd-MMM-yyyy")+DateTime.Now.ToString("ddMMMyyyyhhmmss"); // Create a Print Tool and show the Print Preview form.                                                                        //report1.ExportToPdf();
                            nm.Add(report1.Parameters["AccountName"].Value + nme2 + "." + dep.SelectedValue);                                                                                  //    ExportReport(report1, report1.Parameters["AccountName"].Value + nme2 + "." + dep.SelectedValue, dep.SelectedValue, false);
                            if (dep.SelectedValue == "xlsx")
                                report1.ExportToXlsx(Server.MapPath("~/File/" + report1.Parameters["AccountName"].Value + nme2 + "." + dep.SelectedValue));
                            if (dep.SelectedValue == "pdf")
                                report1.ExportToPdf(Server.MapPath("~/File/" + report1.Parameters["AccountName"].Value + nme2 + "." + dep.SelectedValue));
                            if (dep.SelectedValue == "rtf")
                                report1.ExportToRtf(Server.MapPath("~/File/" + report1.Parameters["AccountName"].Value + nme2 + "." + dep.SelectedValue));
                            if (dep.SelectedValue == "csv")
                                report1.ExportToCsv(Server.MapPath("~/File/" + report1.Parameters["AccountName"].Value + nme2 + "." + dep.SelectedValue));

                        }
                    }
                    arr.Remove(gvAll.DataKeys[i].Value);
                }
            }
    
            ViewState["SelectedRecords"] = arr;
            hfCount.Value = "0";
            gvAll.AllowPaging = true;
            BindGrids();
            //ShowMessage(count);
            var nmw = nm.ToList().Distinct();
            FnN(nmw,count);
            ShowMessage(count);
        }
        public string GetDownloadFolderPath()
        {
            return Registry.GetValue(@"HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders", "{374DE290-123F-4565-9164-39C4925E467B}", String.Empty).ToString();
        }
        public string ne(string acc){
            string acn = "";
            var cp = db.Accounts_Masters.ToList().Where(a=>a.AccountNumber.ToString()==acc);
            foreach (var z in cp)
            {
                acn = z.AccountName;
            }
        return acn;
            }
        public void ExportReport(XtraReport report, string fileName, string fileType, bool inline)
        {
            MemoryStream stream = new MemoryStream();

            Response.Clear();
            Response.ClearContent();
            Response.ClearHeaders();

            if (fileType == "xlsx")
                report.ExportToXlsx(stream);
            if (fileType == "pdf")
                report.ExportToPdf(stream);
            if (fileType == "rtf")
                report.ExportToRtf(stream);
            if (fileType == "csv")
                report.ExportToCsv(stream);
           

            Response.ContentType = "application/" + fileType;
            Response.AddHeader("Accept-Header", stream.Length.ToString());
            Response.AddHeader("Content-Disposition", (inline ? "Inline" : "Attachment") + "; filename=" + fileName + "." + fileType);
            Response.AddHeader("Content-Length", stream.Length.ToString());
            //Response.ContentEncoding = System.Text.Encoding.Default;
            Response.BinaryWrite(stream.ToArray());
            Response.Flush();
            Response.Close();

        }


        protected void btnSearch_Click(object sender, EventArgs e)
        {
            //if (ASPxDateEdit1.Text == "" || ASPxDateEdit2.Text == "")
            //{
            //    msgbox("Please select a date");
            //    return;
            //}
            //else
            if (JGender.SelectedValue == "")
            {
                msgbox("Please select a range");
                return;
            }
            else
            {
                BindGrids();
            }

        }

        protected void gvAll_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "MouseEvents(this, event)");
                e.Row.Attributes.Add("onmouseout", "MouseEvents(this, event)");
            }
        }

        protected void gvAll_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        public void FnN(dynamic nm,int count)
        {

            string path = Server.MapPath("~/File/");//Location for inside Test Folder  
          
            string[] Filenames = Directory.GetFiles(path);
            using (ZipFile zip = new ZipFile())
            {
                zip.AlternateEncodingUsage = ZipOption.AsNecessary;
                foreach (var p in nm)
                {

                    zip.AddFile(Server.MapPath("~/File/"+ p.ToString()), "General Ledger Statements downloaded by"+WebSecurity.CurrentUserName);

                }
                Response.Clear();
                Response.BufferOutput = false;
                string zipName = String.Format("Zip_{0}.zip", DateTime.Now.ToString("yyyy-MMM-dd-HHmmss"));
                Response.ContentType = "application/zip";
                Response.AddHeader("content-disposition", "attachment; filename=" + zipName);
                zip.Save(Response.OutputStream);
                BindGrids();
                ShowMessage(count);
                Response.End();
            }
        }
        protected void Button2_Click(object sender, EventArgs e)
        {
            string path = Server.MapPath("~/File/");//Location for inside Test Folder  
            List<string> nm = new List<string>();
            nm.Add("XtraDeal.pdf");
            nm.Add("XtraDeal 2.pdf");
            string[] Filenames = Directory.GetFiles(path);
            using (ZipFile zip = new ZipFile())
            {
                zip.AlternateEncodingUsage = ZipOption.AsNecessary;
                zip.AddDirectoryByName("Files");
                foreach (var p in nm)
                {
                   
                         zip.AddFile(Server.MapPath("~/File/Project/"+p.ToString()), "Files");
                    
                }
                Response.Clear();
                Response.BufferOutput = false;
                string zipName = String.Format("Zip_{0}.zip", DateTime.Now.ToString("yyyy-MMM-dd-HHmmss"));
                Response.ContentType = "application/zip";
                Response.AddHeader("content-disposition", "attachment; filename=" + zipName);
                zip.Save(Response.OutputStream);
                Response.End(); 
            }
            
            }
        }
    }

    

       
        
    
    

