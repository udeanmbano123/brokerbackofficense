﻿
<%@ Page Language="C#" MaintainScrollPositionOnPostback="true" MasterPageFile="~/Site1.Master" Async="true"    AutoEventWireup="true" CodeBehind="Audit.aspx.cs" Inherits="BrokerOffice.ClientServices.Audit" %>
<%@ Register Assembly="DevExpress.Web.v18.2, Version=18.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>


<asp:content id="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
  <div style="background:white" class="col-md-12">
    <div class="panel-heading" style="background-color:#428bca">
        <i style="color:white">Broker Note Audit</i>
       
    </div> 
      <table class="table table-responsive" style="margin-left:0%;">
          <tr>
         <td>
             <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="From"></dx:ASPxLabel>
             <dx:ASPxDateEdit ID="ASPxDateEdit1" runat="server" Theme="BlackGlass"></dx:ASPxDateEdit>
                        </td>
                        <td>
                            <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="To"></dx:ASPxLabel>
                            <dx:ASPxDateEdit ID="ASPxDateEdit2" runat="server" Theme="BlackGlass"></dx:ASPxDateEdit>
                        </td>
              <td>
                 <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Criteria"></dx:ASPxLabel>
                         
                  <asp:RadioButtonList ID="RadioButtonList5" runat="server"  RepeatDirection="Horizontal">
                    <asp:ListItem Text="Audited" Value="True"></asp:ListItem>
                     <asp:ListItem Text="Non-Audited" Value="False"></asp:ListItem>
                </asp:RadioButtonList>
              </td>
       
         <td>
<dx:ASPxButton ID="ASPxButton2" runat="server" CssClass="btn btn-primary" Text="Search" OnClick="btnSearch_Click" style="height: 23px"></dx:ASPxButton>
        
                
        </td>
        <td>
            &nbsp;</td>
    </tr>

                </table>
   
      <div class="col-md-10">

 
    <dx:ASPxGridView ID="ASPxGridView1" KeyFieldName="Deal"  AutoGenerateColumns="false"  runat="server" EnableCallBacks="False" CssClass="form-control" Theme="BlackGlass" OnCancelRowEditing="ASPxGridView1_CancelRowEditing" OnRowDeleting="ASPxGridView1_RowDeleting" OnRowUpdating="ASPxGridView1_RowUpdating" OnSelectionChanged="ASPxGridView1_SelectionChanged" Visible="False" EnablePagingGestures="True" Width="355px" style="margin-right: 0px" Font-Size="X-Small"  >
                        <SettingsPager AlwaysShowPager="True" PageSize="30" EnableAdaptivity="True">
                            <PageSizeItemSettings Visible="True">
                            </PageSizeItemSettings>
                        </SettingsPager>
                        <SettingsBehavior  AllowSelectSingleRowOnly="True"   ProcessSelectionChangedOnServer="True" AllowSelectByRowClick="True" ConfirmDelete="True" />
                          <SettingsCommandButton>
                              <NewButton ButtonType="Button" Text="Audit">
                              </NewButton>
                              <CancelButton ButtonType="Button" Text="Audit">
                              </CancelButton>
                              <EditButton ButtonType="Button"  Text="Audit">
                              </EditButton>
                            <DeleteButton   ButtonType="Button" Text="Email">
                            </DeleteButton>
                            <SelectButton ButtonType="Button">
                            </SelectButton>
                        </SettingsCommandButton>
                        <SettingsText CommandDelete="Are you sure you want to email Broker Note ?" ConfirmDelete="Are you sure you want to email Broker Note ?" />
        <Columns>

            <dx:GridViewCommandColumn ShowSelectCheckbox="True" Caption="Select"  VisibleIndex="0">
                </dx:GridViewCommandColumn>
                      <dx:GridViewDataTextColumn Width="100px" FieldName="DealNumber" ReadOnly="True" VisibleIndex="1">
            
                </dx:GridViewDataTextColumn>
                           <dx:GridViewDataTextColumn Width="100px" FieldName="Security" ReadOnly="True" VisibleIndex="2">
            
                </dx:GridViewDataTextColumn>
                     <dx:GridViewDataTextColumn Width="100px" FieldName="Deal" VisibleIndex="3">
                </dx:GridViewDataTextColumn>
                  <dx:GridViewDataTextColumn Width="100px" FieldName="Quantity" VisibleIndex="4">
                </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Width="100px" FieldName="Price" VisibleIndex="5">
                </dx:GridViewDataTextColumn>
                                       
                                   <dx:GridViewDataTextColumn Width="100px" FieldName="BuyerClient" VisibleIndex="6">
                </dx:GridViewDataTextColumn>
                                       
                              <dx:GridViewDataTextColumn Width="100px" FieldName="SellerClient" VisibleIndex="7">
                </dx:GridViewDataTextColumn>
                                         
                              <dx:GridViewDataTextColumn Width="100px" FieldName="DatePosted" VisibleIndex="8">
                </dx:GridViewDataTextColumn>
                                          
                              <dx:GridViewDataTextColumn Width="100px" FieldName="SettlementDate" VisibleIndex="9">
                </dx:GridViewDataTextColumn>
             <dx:GridViewDataTextColumn Width="100px" FieldName="Audited" VisibleIndex="10">
                </dx:GridViewDataTextColumn>
                       
            <dx:GridViewCommandColumn  ShowEditButton="True"  VisibleIndex="11">
                </dx:GridViewCommandColumn>
             <dx:GridViewDataColumn Caption="Commands" VisibleIndex="12">
                    <DataItemTemplate>
                        <table>
                            <tr>
                                <td>
                                    <dx:ASPxButton ID="ASPxButton1" runat="server" Theme="BlackGlass" Font-Size="X-Small" Width="100px" OnClick="ASPxButton1_Click" Text="Download Deal Note">
    </dx:ASPxButton>
                                </td>
                               
                            </tr>
                        </table>
                    </DataItemTemplate>             
              </dx:GridViewDataColumn>
            </Columns>
                       

                    </dx:ASPxGridView>
               </div>
      <div class="col-md-12">

      
    <dx:ASPxGridView ID="ASPxGridView2" KeyFieldName="Deal"  AutoGenerateColumns="false"  runat="server"  EnableCallBacks="False" Theme="BlackGlass" OnCancelRowEditing="ASPxGridView1_CancelRowEditing" OnRowDeleting="ASPxGridView2_RowDeleting" OnRowUpdating="ASPxGridView2_RowUpdating" OnSelectionChanged="ASPxGridView2_SelectionChanged" Visible="False" CssClass="form-control" Width="960px" Font-Size="X-Small" style="margin-right: 0px"   >
                        <SettingsPager Mode="ShowPager" AlwaysShowPager="True" PageSize="30">
                            <PageSizeItemSettings Visible="True">
                            </PageSizeItemSettings>
                        </SettingsPager>
                        <SettingsBehavior AllowSelectSingleRowOnly="True"   ProcessSelectionChangedOnServer="True" AllowSelectByRowClick="True" ConfirmDelete="True" />
                          <SettingsCommandButton>
                              <NewButton ButtonType="Button" Text="Audit">
                              </NewButton>
                              <CancelButton ButtonType="Button" Text="Audit">
                              </CancelButton>
                              <EditButton ButtonType="Button"  Text="Audit">
                              </EditButton>
                            <DeleteButton   ButtonType="Button" Text="Email">
                            </DeleteButton>
                            <SelectButton ButtonType="Button">
                            </SelectButton>
                        </SettingsCommandButton>
                        <SettingsText CommandDelete="Are you sure you want to email Broker Note ?" ConfirmDelete="Are you sure you want to email Broker Note ?" />
        <Columns>

            <dx:GridViewCommandColumn ShowSelectCheckbox="True" Caption="Select"  VisibleIndex="0">
                </dx:GridViewCommandColumn>
                      <dx:GridViewDataTextColumn Width="100px" FieldName="DealNumber" ReadOnly="True" VisibleIndex="1">
            
                </dx:GridViewDataTextColumn>
                           <dx:GridViewDataTextColumn Width="100px" FieldName="Security" ReadOnly="True" VisibleIndex="2">
            
                </dx:GridViewDataTextColumn>
                     <dx:GridViewDataTextColumn Width="100px" FieldName="Deal" VisibleIndex="3">
                </dx:GridViewDataTextColumn>
                  <dx:GridViewDataTextColumn Width="100px" FieldName="Quantity" VisibleIndex="4">
                </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Width="100px" FieldName="Price" VisibleIndex="5">
                </dx:GridViewDataTextColumn>
                                       
                                   <dx:GridViewDataTextColumn Width="100px" FieldName="BuyerClient" VisibleIndex="6">
                </dx:GridViewDataTextColumn>
                                       
                              <dx:GridViewDataTextColumn Width="100px" FieldName="SellerClient" VisibleIndex="7">
                </dx:GridViewDataTextColumn>
                                         
                              <dx:GridViewDataTextColumn Width="100px" FieldName="DatePosted" VisibleIndex="8">
                </dx:GridViewDataTextColumn>
                                          
                              <dx:GridViewDataTextColumn Width="100px" FieldName="SettlementDate" VisibleIndex="9">
                </dx:GridViewDataTextColumn>
             <dx:GridViewDataTextColumn Width="100px" FieldName="Audited" VisibleIndex="9">
                </dx:GridViewDataTextColumn>
                            <dx:GridViewCommandColumn  ShowDeleteButton="True"  VisibleIndex="11">
                </dx:GridViewCommandColumn>
                  <dx:GridViewDataColumn Caption="Commands" VisibleIndex="12">
                    <DataItemTemplate>
                        <table>
                            <tr>
                                <td>
                                    <dx:ASPxButton ID="ASPxButton1" runat="server" Theme="BlackGlass" Font-Size="X-Small" Width="100px" OnClick="ASPxButton1_Click" Text="Download Deal Note">
    </dx:ASPxButton>
                                </td>
                               
                            </tr>
                        </table>
                    </DataItemTemplate>             
              </dx:GridViewDataColumn>
            </Columns>
                       

                    </dx:ASPxGridView>
         
         
  
          </div>
     
 
    </div>
    
    <script type="text/javascript">
$(function () {
    var tabName = $("[id*=TabName]").val() != "" ? $("[id*=TabName]").val() : "personal";
    $('#Tabs a[href="#' + tabName + '"]').tab('show');
    $("#Tabs a").click(function () {
        $("[id*=TabName]").val($(this).attr("href").replace("#", ""));
    });
});
</script>

    <asp:Label ID="Label1" runat="server" Visible="False"></asp:Label>
     

</asp:content>