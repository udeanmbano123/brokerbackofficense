﻿using BrokerOffice.DAO;
using BrokerOffice.Models;
using BrokerOffice.Reporting;
using DevExpress.Web;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebMatrix.WebData;

namespace BrokerOffice.ClientServices
{
    public partial class OrderPosting : System.Web.UI.Page
    {
        public string id = "";
        
        private SBoardContext db = new SBoardContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            id = Request.QueryString["id"];
          
            if (this.IsPostBack)
            {

                TabName.Value = Request.Form[TabName.UniqueID];


            }
            else
            {
               
                LoadNames();
                drpname.DataSource = GetDataSource();
                drpname.DataBind();
                LoadCurrencies();
                LoadIssuer();
                loadClientTypes();
               loadTP();
                if (id == " " || id == null)
                {

                }
                else
                {
                    getDetails(id);
                }
            }
        }

        private DataTable GetDataSource()
        {
            //DataTable defined
            var dtSource = new DataTable();
            dtSource.Columns.Add("id", typeof(string));
            dtSource.Columns.Add("FullNames", typeof(string));
            string x = "";

            try
            {
                x= Request.QueryString["id"];
            }
            catch (Exception)
            {

                x = "";
            }

            string name = "";
            
            //Fill rows
            if (x!=null)
            {
                long? byy = Convert.ToInt32(x);
                var d = from c in db.Order_Lives
                        where c.OrderNo==byy
                        select c;

                foreach (var e in d)
                {
                    dtSource.Rows.Add(e.Shareholder, e.ClientName);
                }



            }
            else
            {
                dtSource.Rows.Add("1", "Select Investor");
                var dd = from c in db.Account_Creations
                        where c.StatuSActive == true
                        select c;
                foreach (var p in dd)
                {
                    if (p.OtherNames == null)
                    {
                        name = p.Surname_CompanyName + " ," + p.CDSC_Number;
                    }
                    else if (p.OtherNames == "")
                    {
                        name =p.Surname_CompanyName + " ," + p.CDSC_Number;
                    }
                    else
                    {
                        name = p.OtherNames + " " + p.Surname_CompanyName + " ," + p.CDSC_Number;
                    }

                    dtSource.Rows.Add(p.CDSC_Number,name);
                }

            }//Fill rows



            return dtSource;
        }
        protected void getDetails(string x)
        {
            drpname.Items.Clear();
            drpname.DataSource = null;
            drpname.Items.Clear();
            drpname.DataSource = null;

            drpCurr.Items.Clear();
            drpCurr.DataSource = null;
            drpCurr.Items.Clear();
            drpCurr.DataSource = null;
            string per = "";
            if (x != "")
            {
                Submit.Text = "Update";
                txtFkey.Text = x;

                var acc = db.Order_Lives.ToList().Where(a => a.OrderNo == Convert.ToInt32(x));

                foreach (var my in acc)
                {
                

                    drporder.Items.Insert(0, new ListItem(my.OrderType.ToString(), my.OrderType));
                    
                     Shareholder.Text=my.Shareholder;
                    //my.ClientName = drpname.SelectedItem.Text;
                  
                    qty.Text=my.Quantity.ToString() ;
                     txtbase.Text=my.BasePrice.ToString();
                   availableshrs.Text=my.AvailableShares.ToString();
                ordepref.Text= my.OrderPref  ;
               
                    //drpCurr.SelectedValue.ToString();
                    var curr = db.para_Currencies.ToList().Where(a=>a.CurrencyCode==my.Currency);
                    foreach(var c in curr)
                    {
                        per = c.CurrencyName;
                    }
                    drpCurr.Items.Insert(0, new ListItem(per, my.Currency));
                    drpSecurity.Items.Insert(0, new ListItem(my.SecurityType, my.SecurityType));
            
                    drpTP.Items.Insert(0, new ListItem(my.TP, my.TP));
                    Company.Items.Insert(0, new ListItem(my.Company, my.CompanyISIN.ToString()));
            
                }
                LoadNames2();
                LoadCurrencies2();
                LoadIssuer2();
                loadClientTypes2();
            }

            return;
        }
        protected void loadTP()
        {
            drpTP.Items.Clear();
            drpTP.DataSource = null;
            var ds = from c in db.TradingPlatforms
                     select new

                     {

                         ID = c.Name,

                         ISIN = c.Name,

                     };
            drpTP.DataSource = ds.ToList();
            drpTP.DataTextField = "ISIN";
            // text field name of table dispalyed in dropdown
            drpTP.DataValueField = "ID"; // to retrive specific  textfield name 
                                         //assigning datasource to the dropdownlist
            drpTP.DataBind(); //binding dropdownlist
                              // allocate.Items.Insert(0, "Select");
            drpTP.Items.Insert(0, new ListItem("Please select an Exchange", "0"));

        }
        protected void loadClientTypes2()
        {
            
           

        }
        protected void loadClientTypes()
        {
           

        }
        protected void LoadIssuer2()
        {
            Company.Items.Clear();
            Company.DataSource = null;
            var ds = from c in db.para_issuers
                     select new

                     {

                         ID = c.ISIN,

                         ISIN = c.ISIN

                     };
            Company.DataSource = ds.ToList();
            Company.DataTextField = "ISIN";
            // text field name of table dispalyed in dropdown
            Company.DataValueField = "ID"; // to retrive specific  textfield name 
                                           //assigning datasource to the dropdownlist
            Company.DataBind(); //binding dropdownlist
                                // allocate.Items.Insert(0, "Select");

           

        }
        protected void LoadIssuer()
        {
            Company.Items.Clear();
            Company.DataSource = null;
            var ds = from c in db.para_issuers
                     select new

                     {

                         ID = c.ISIN,

                         ISIN =c.ISIN,

                     };
            Company.DataSource = ds.ToList();
            Company.DataTextField = "ISIN";
            // text field name of table dispalyed in dropdown
            Company.DataValueField = "ID"; // to retrive specific  textfield name 
                                           //assigning datasource to the dropdownlist
            Company.DataBind(); //binding dropdownlist
                                // allocate.Items.Insert(0, "Select");
         
                Company.Items.Insert(0, new ListItem("Please select a company", "0"));
          

        }
        protected void LoadNames2()
        {
            
           
        }
        protected void LoadNames()
        {
           
        }
        protected void LoadCurrencies2()
        {
            drpCurr.Items.Clear();
            drpCurr.DataSource = null;
            var ds = from c in db.para_Currencies
                     select new

                     {

                         Code = c.CurrencyCode,

                         CurrencyName = c.CurrencyName,

                     };
            drpCurr.DataSource = ds.ToList();
            drpCurr.DataTextField = "CurrencyName";
            // text field name of table dispalyed in dropdown
            drpCurr.DataValueField = "Code"; // to retrive specific  textfield name 
                                             //assigning datasource to the dropdownlist
            drpCurr.DataBind(); //binding dropdownlist
                                // allocate.Items.Insert(0, "Select");

          

        }
        protected void LoadCurrencies()
        {
            drpCurr.Items.Clear();
            drpCurr.DataSource = null;
            var ds = from c in db.para_Currencies
                     select new

                     {

                         Code = c.CurrencyCode,

                         CurrencyName = c.CurrencyName,

                     };
            drpCurr.DataSource = ds.ToList();
            drpCurr.DataTextField = "CurrencyName";
            // text field name of table dispalyed in dropdown
            drpCurr.DataValueField = "Code"; // to retrive specific  textfield name 
                                             //assigning datasource to the dropdownlist
            drpCurr.DataBind(); //binding dropdownlist
                                // allocate.Items.Insert(0, "Select");
            
                drpCurr.Items.Insert(0, new ListItem("Please Select a currency", "0"));
            

        }
        public bool IsAddressAvailable(string address)
        {
            try
            {
                System.Net.WebClient client = new WebClient();
                client.DownloadData(address);
                return true;
            }
            catch
            {
                return false;
            }
        }
        protected void Submit_Click(object sender, EventArgs e)
        {

            if (drpname.SelectedItem.Text == "Select Investor")
            {
                msgbox("Select an Investor");
                return;
            }
            else if (Company.SelectedItem.Text == "Please select a company")
            {
                msgbox("Select a company");
                return;
            }
            
                    else if(qty.Text=="")
            {
                msgbox("Select a quantity");
                return;
            }
            else if (drpTP.SelectedItem.Text == "Please select an Exchange")
            {
                msgbox("Please select an Exchange");
                return;
            }
            else if (DropDownList1.SelectedItem.Text=="--Select Order Qualifier--")
            {
                msgbox("Please select Order qualifier");
                return;
            }else if(DropDownList2.SelectedItem.Text== "--Select FOK--")
            {
                msgbox("Please speciify if OK or not");
                return;
            }
            else if(ASPxDateEdit1.Text=="")
            {
                msgbox("Please select Order Date");
                return;
            }
            DateTime dte = DateTime.Now;
            DateTime dte2 = Convert.ToDateTime(ASPxDateEdit1.Text);
            if (dte2>dte)
            {
                msgbox("Please select a date that is not a future a date");
                return;
            }
            availableshrs.Text = "0";


            if (Submit.Text == "Submit")
            {

                    Order_Live my = new Order_Live();

                    my.OrderType = drporder.SelectedValue.ToString();
                    my.Currency = "USD";
                    my.SecurityType = drpSecurity.SelectedItem.Text;
                    my.Client_Type = "None";
                    my.Tax = 0;

                    string newd = (drpname.SelectedItem.Text).Split(',')[0];
                    my.ClientName = newd;
                    my.CDS_AC_No = drpname.SelectedItem.Value.ToString();
                    my.Shareholder = my.CDS_AC_No;
                    my.TotalShareHolding = 0;
                    DateTime? my11 = Convert.ToDateTime(DateTime.Now);
                    DateTime? my2 = Convert.ToDateTime(DateTime.Now);
                    my.Deal_Begin_Date = my11;
                    my.Expiry_Date = my2;
                    my.Quantity = Convert.ToInt32(qty.Text);
                    if (txtbase.Text == "" || txtbase.Text == null)
                    {
                        txtbase.Text = "0";
                    }
                    decimal n;
                    bool isNumericT = decimal.TryParse(txtbase.Text, out n);

                    if (isNumericT == false)
                    {
                        txtbase.Text = "0";
                    }
                    my.BasePrice = Convert.ToDecimal(txtbase.Text);
                    my.AvailableShares = Convert.ToDecimal(availableshrs.Text);
                    my.OrderPref = ordepref.Text;
                    my.OrderAttribute = "";
                    my.Marketboard = mrktBoard.Text;

                   
                    string vx = DropDownList3.SelectedItem.Text;
                    
                    my.MiniPrice = 0;
                    my.Company = Company.SelectedItem.Text;
                    my.CompanyISIN = Company.SelectedValue.ToString();
                    drpCurr.SelectedValue.ToString();
                    my.TP = drpTP.SelectedItem.Text;
                    my.TPBoard = "None";
                my.Create_date = Convert.ToDateTime(ASPxDateEdit1.Text);
                    my.mobile = "None";
                    my.PostedBy = WebSecurity.CurrentUserName;
                    my.Broker_Code = vx;
            if (DropDownList3.SelectedItem.Text == "" || DropDownList3.SelectedItem.Text == "--Select Broker--")
                {
                    my.Broker_Code = "NSE";
                }
                my.TimeInForce = "0";
                    my.CompanyID = 0;
                try
                {
                    my.Flag_oldorder = Convert.ToBoolean(DropDownList2.SelectedValue.ToString());

                }
                catch (Exception)
                {

                    my.Flag_oldorder = false;
                }
                my.OrderQualifier = DropDownList1.SelectedValue.ToString();
                    my.TimeInForce = DropDownList1.SelectedValue.ToString();
                    var edc = "";

                    string validate = "";

                
                    long max = 0;
                    try
                    {
                         max = db.Order_Lives.ToList().Max(a => a.OrderNo);
                    }
                    catch (Exception)
                    {

                        max = 1;
                    }
                    my.OrderNumber = max.ToString();
                   
                    my.OrderStatus = "Successful";
                    my.WebServiceID = "Internal";
                    my.PostedBy = WebSecurity.CurrentUserName.ToString();
                    my.ContraBrokerId = my.Broker_Code;
                    my.BrokerRef = my.Broker_Code;
                    my.Create_date = Convert.ToDateTime(ASPxDateEdit1.Text);
                    db.Order_Lives.AddOrUpdate(my);
                    db.SaveChanges(WebSecurity.CurrentUserName.ToString());
                    System.Web.HttpContext.Current.Session["NOT"] = "Order number "+ max.ToString() +" has been processed sucessfully";
                    //clearing fields
                    drpTP.SelectedIndex = 0;
                    drporder.SelectedIndex = 0;
                    Company.SelectedIndex = 0;
                    drpname.SelectedIndex = 0;
                    DropDownList2.SelectedIndex = 0;
                    DropDownList1.SelectedIndex = 0;
                    Shareholder.Text = "";
                    qty.Text = "";
                    txtbase.Text = "";
                
                System.Web.HttpContext.Current.Session["NOT"] = "You have successfully added the order";
                Response.Redirect("~/Orders/Index");



                //var edc = escorw.Createnewaccount(code22, my.Surname_CompanyName, my.Middlename, my.OtherNames, my.Initials, my.Title, my.country, my.Town, my.TelephoneNumber, my.MobileNumber,
                //my.currency, my.idtype, my.Identification, my.Nationality, my.DateofBirth_Incorporation.ToString(), my.Gender, my.Address1, my.Address2, my.Address3, "", my.Emailaddress, my.divpayee, my.divaccounttype, my.divbankcode, my.divbranchcode, my.divacc, my.idtype2, my.dividnumber, my.Bank, my.Branch,
                //my.Accountnumber, my.mobile_money);
                //System.Web.HttpContext.Current.Session["NOT"] = "You have successfully added the order";
                //Response.Redirect("~/Orders/Index");
                //msgbox(edc);
            }
            else if (Submit.Text == "Update")
            {
                var c = db.Users.ToList().Where(a => a.Email == WebSecurity.CurrentUserName);
                string vx = "";
                foreach (var d in c)
                {
                    vx = d.BrokerCode;
                }

                var my = db.Order_Lives.Find(Convert.ToInt32(txtFkey.Text));
                my.OrderType = drporder.SelectedValue.ToString();
                my.Currency = drpCurr.SelectedValue.ToString();
                my.Company = drpname.SelectedItem.Text;
 
                my.SecurityType = drpSecurity.SelectedItem.Text;
                my.Client_Type ="None";
                my.Tax = 0;
                my.Shareholder = Shareholder.Text;
                string newd = (drpname.SelectedItem.Text).Split(',')[0];
                my.ClientName = newd;
                my.CDS_AC_No = drpname.SelectedItem.Value.ToString();
                my.Shareholder = my.CDS_AC_No;
                my.TotalShareHolding = 0;
              
                my.Quantity = Convert.ToInt32(qty.Text);
                if (txtbase.Text == "" || txtbase.Text == null)
                {
                    txtbase.Text = "0";
                }
                decimal n;
                bool isNumericT = decimal.TryParse(txtbase.Text, out n);

                if (isNumericT == false)
                {
                    txtbase.Text = "0";
                }
                my.BasePrice = Convert.ToDecimal(txtbase.Text);
                my.AvailableShares = Convert.ToDecimal(availableshrs.Text);
                my.OrderPref = ordepref.Text;
                my.OrderAttribute = "";
                my.Marketboard = mrktBoard.Text;
                
                my.MiniPrice = 0;
                my.Currency=drpCurr.SelectedValue.ToString();
                my.Company = Company.SelectedItem.Text;
               my.CompanyISIN =Company.SelectedItem.Text;
                my.TP = drpTP.SelectedItem.Text;
                my.TPBoard = "None";
                my.mobile = "None";
                //my.PostedBy = WebSecurity.CurrentUserName;
                my.OrderStatus = "Active";
                my.Broker_Code = vx;
                my.TimeInForce = "0";
                my.CompanyID =0;
                my.Flag_oldorder = false;
               /// my.Create_date = DateTime.Now;

                string validate = "";
                try
                {
                    DateTime? dt1 = my.Deal_Begin_Date;
                    DateTime? dt2 = my.Expiry_Date;
                    string dd1 = dt1.Value.Year.ToString() + "-" + dt1.Value.Month.ToString() + "-" + dt1.Value.Day.ToString();
                    string dd2 = dt2.Value.Year.ToString() + "-" + dt2.Value.Month.ToString() + "-" + dt2.Value.Day.ToString();

                    string com = drpname.SelectedItem.Text.ToString().Replace(" ", string.Empty).Replace(":", string.Empty);
                    string com2 = my.Shareholder.ToString().Replace(" ", string.Empty).Replace(":", string.Empty);
                    string com3 = my.ClientName.ToString().Replace(" ", string.Empty).Replace(":", string.Empty);
                    string base1 = my.BasePrice.ToString().Replace(".", "T");
                        string max = my.MaxPrice.ToString().Replace(".", "T");
                    string mini = my.MiniPrice.ToString().Replace(".", "T");
                    string tax = my.Tax.ToString().Replace(".", "T");

                    //edc = escorw.ReceiveUSSDSellOrder(my.CDS_AC_No, my.Quantity.ToString(), my.mobile, my.Company, "BBO1", "finsec2016.es!@", "BBO", "English", my.MaxPrice.ToString());

                    var client = new RestClient("http://192.168.3.245/BrokerService");
                    var request = new RestRequest("OrderUpdate/{ordertype}/{CDS_Number}/{NoofNotes}/{TelephoneSelorder}/{Company}/{UserName}/{PassWord}/{MNO_}/{BasePrice}/{BeginDate}/{ExpiryDate}/{BrokerCode}/{MaxPrice}/{MiniPrice}/{shareholder}/{client}/{orderNo}/{curr}/{tax}", Method.PUT);
                    request.AddUrlSegment("ordertype", my.OrderType.ToString().Replace(" ", string.Empty).Replace(":", string.Empty));
                    request.AddUrlSegment("CDS_Number", my.CDS_AC_No.ToString().Replace(" ", string.Empty).Replace(":", string.Empty));
                    request.AddUrlSegment("NoofNotes", my.Quantity.ToString().Replace(" ", string.Empty).Replace(":", string.Empty));
                    request.AddUrlSegment("TelephoneSelorder", my.mobile.ToString().Replace(" ", string.Empty).Replace(":", string.Empty));
                    request.AddUrlSegment("Company",com);
                    request.AddUrlSegment("UserName", "BBO1");
                    request.AddUrlSegment("PassWord", "finsec2016");
                    request.AddUrlSegment("MNO_", "BBO");
                    request.AddUrlSegment("BasePrice", base1);
                    request.AddUrlSegment("BeginDate", dd1.ToString().Replace(" ", string.Empty).Replace(":", string.Empty));
                    request.AddUrlSegment("ExpiryDate", dd2.ToString().Replace(" ", string.Empty).Replace(":", string.Empty));
                    request.AddUrlSegment("BrokerCode", vx);
                    request.AddUrlSegment("MaxPrice", max);
                    request.AddUrlSegment("MiniPrice",mini);
                    request.AddUrlSegment("shareholder",com2);
                    request.AddUrlSegment("client", com3);
                    request.AddUrlSegment("orderNo",my.OrderNumber);
                    request.AddUrlSegment("curr",my.Currency);
                    request.AddUrlSegment("tax", tax);
                    IRestResponse response = client.Execute(request);
                    validate = response.Content;
                    //Post to Webservice



                }
                catch (Exception)
                {

                    msgbox("No internet connection to webservice");
                }


                var Jsonobject = JsonConvert.DeserializeObject<string>(validate);

                validate = Jsonobject.ToString();


                //Update Row in Table  
                if (validate != "Matched")
                {
                   
                    my.PostedBy = WebSecurity.CurrentUserName.ToString();
                    my.ContraBrokerId = my.Broker_Code;
                    db.Order_Lives.AddOrUpdate(my);
                db.SaveChanges(WebSecurity.CurrentUserName.ToString());
                System.Web.HttpContext.Current.Session["NOT"] = "You have successfully updated the order";
               // Response.Redirect("~/Orders/Index");
                }
                else if(validate == "0"){
                    System.Web.HttpContext.Current.Session["NOT"] = "Update failed";

                }
                else if (validate == "Matched")
                {
                    System.Web.HttpContext.Current.Session["NOT"] = "Order cannot be updated it has been processed";
                   // Response.Redirect("~/Orders/Index");

                }

            }
        }
        protected void Clear()
        {
            Response.Redirect("~/Orders/Index");
        }
        public void msgbox(string strMessage)
        {
            string strScript = "<script language=JavaScript>";
            strScript += "window.alert(\"" + strMessage + "\");";
            strScript += "</script>";
            System.Web.UI.WebControls.Label lbl = new System.Web.UI.WebControls.Label();
            lbl.Text = strScript;
            Page.Controls.Add(lbl);
        }

        protected void drpname_SelectedIndexChanged(object sender, EventArgs e)
        {
            Shareholder.Text = drpname.SelectedItem.Text;

            //var hold = db.ClientPortfolioss.ToList().Where(a => a.ClientNumber == drpname.SelectedItem.Value.ToString()).Sum(a=>a.Holdings);

            //if (hold.ToString()==null)
            //{
            //    hold = 0;
            // }    

            //load brokers
            DropDownList3.Items.Clear();
            DropDownList3.DataSource = null;
            var pc = db.Account_Creations.ToList().Where(a=>a.CDSC_Number== drpname.SelectedItem.Value.ToString());
            var pp = db.BrokerClasss.ToList().Where(a => a.ATPCSD == drpname.SelectedItem.Value.ToString());
            List<string> mm = new List<string>();
            foreach (var n in pc)
            {
                mm.Add(n.manAccount);
            }
            foreach(var d in pp)
            {
                mm.Add(d.BrokerName);
            }
            DropDownList3.DataSource = mm.ToList();
            DropDownList3.DataBind();
        }

        protected void drpTP_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        protected void btnInvoice_Click(object sender, EventArgs e)
        {
            if (txtbase.Text =="")
            {
                msgbox("The base price must be selected");
                return;
            }
            else if (qty.Text=="")
            {
                msgbox("The quantity must be selected");
                return;
            }

          
            //string chargedAs = "";

            //decimal chargevalue = 0;
            //decimal total = 0;
            decimal qty2 = Convert.ToDecimal(qty.Text);
            decimal maxprice = Convert.ToDecimal(txtbase.Text);

            //var tr = db.TradingChargess.ToList().Where(a=>a.tradeside==drporder.SelectedValue.ToString());

            //foreach(var c in tr)
            //{
            //    chargedAs = c.ChargedAs;
            //    chargevalue = c.chargevalue;
            //}

            //if (chargedAs == "Percentage")
            //{
            //    total = (maxprice * qty2) + ((chargevalue / 100) * (maxprice * qty2));
            //}
            //else if (chargedAs == "Flat")
            //{
            //    total = (maxprice * qty2) + chargevalue;
            //}
            //ASPxDocumentViewer1.ReportTypeName = "";
            //ASPxDocumentViewer1.ReportTypeName = "BrokerOffice.Reporting.XtraQuotation";
            //XtraQuotation report = new XtraQuotation();
            //report.Parameters["Account"].Value = drpname.SelectedValue.ToString();

            //report.Parameters["Max"].Value = maxprice.ToString();

            //report.Parameters["Quantity"].Value = qty2.ToString();

            //report.RequestParameters = false;

            //ASPxDocumentViewer1.Report = report;

            //report.Parameters["yourParameter2"].Value = secondValue;


            //report.Parameters["yourParameter2"].Value = secondValue;
            myIframe.Src = "~/Reporting/Quotation.aspx#ASPxDocumentViewer1?Account=" + drpname.SelectedItem.Value.ToString() + "&max=" + maxprice.ToString()+ "&qty="+ qty2.ToString()+"&Board=" + drpTP.SelectedItem.Text +"&Security=" + Company.SelectedItem.Text+"&Type=" + drporder.SelectedItem.Text;
 
             ASPxPopupControl1.PopupHorizontalAlign = PopupHorizontalAlign.WindowCenter;
            ASPxPopupControl1.PopupVerticalAlign = PopupVerticalAlign.WindowCenter;
            ASPxPopupControl1.AllowDragging = true;
            ASPxPopupControl1.ShowCloseButton = true;
            ASPxPopupControl1.ShowOnPageLoad = true;
            ASPxPopupControl1.Modal = true;
            ASPxPopupControl1.ShowCloseButton = true;
            ASPxPopupControl1.ShowRefreshButton = true;
            ASPxPopupControl1.ShowShadow = true;
   
        }
        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("Local IP Address Not Found!");
        }

        protected void dxAction_Click(object sender, EventArgs e)
        {
            Label44.Text = "Action";
            TabName.Value = Label44.Text;
            return;
        }

        protected void dxPersonal_Click(object sender, EventArgs e)
        {
            Label44.Text = "personal";
            TabName.Value = Label44.Text;
            return;
        }

    
        protected void qty_TextChanged(object sender, EventArgs e)
        {
            int s;
            bool isNumericS = int.TryParse(qty.Text, out s);

            if (isNumericS == false)
            {
                qty.Text = "0";
                msgbox("Must be integer i.e 8");
            }
        }

        protected void txtbase_TextChanged(object sender, EventArgs e)
        {
            decimal s;
            bool isNumericS = decimal.TryParse(txtbase.Text, out s);

            if (isNumericS == false)
            {
                txtbase.Text = "0";
                msgbox("Must be numeric");
            }
        }

        protected void Company_SelectedIndexChanged(object sender, EventArgs e)
        {
            drpSecurity.Items.Clear();
            drpSecurity.DataSource = null;
            var ds = from c in db.compsecs
                     select new

                     {

                         ID = c.CompanyName,

                         ISIN = c.Security_Type

                     };
            drpSecurity.DataSource = ds.ToList();
            drpSecurity.DataTextField = "ISIN";
            // text field name of table dispalyed in dropdown
            drpSecurity.DataValueField = "ID"; // to retrive specific  textfield name 
                                               //assigning datasource to the dropdownlist
            drpSecurity.DataBind(); //binding dropdownlist
                                // allocate.Items.Insert(0, "Select");

    
        }

        protected void drpTPB_SelectedIndexChanged(object sender, EventArgs e)
        {


          

           
        }

        protected void Shareholder_TextChanged(object sender, EventArgs e)
        {

        }

        protected void drporder_SelectedIndexChanged(object sender, EventArgs e)
        {
            Label1.Text = "Blue";
        }

        protected void DropDownList3_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}