﻿using BrokerOffice.ATS;
using BrokerOffice.DAO;
using BrokerOffice.DealClass;
using BrokerOffice.DealNotes;
using BrokerOffice.Models;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebMatrix.WebData;
using System.Transactions;
namespace BrokerOffice.ClientServices
{
    public partial class Matching : System.Web.UI.Page
    {
        public string id = "";
        private SBoardContext db = new SBoardContext();
        protected void Page_Load(object sender, EventArgs e)
        {

            BindGrids();
            try
            {
                id = Request.QueryString["id"];
                if (this.IsPostBack)
                {

                }
                else
                {



                    if (id == " " || id == null)
                    {

                    }
                    else
                    {
                        // getDetails(id);
                    }
                }

            }
            catch (Exception)
            {
              
            }

        }

        public void BindGrids()
        {
            // select list
            var buy = from s in db.Order_Lives
                      join p in db.Account_Creations on s.CDS_AC_No equals p.CDSC_Number
                      where s.OrderType == "Buy" && s.WebServiceID == "Internal"

                      let OrderNo = s.OrderNo
                      let Type = s.OrderType
                      let Company = s.Company
                      let AccountName = p.Surname_CompanyName + " " + p.OtherNames
                      let Price = s.BasePrice
                      let Quantity = s.Quantity
                      let DatePosted = s.Create_date
                      let Exchange = s.TP
                      select new { OrderNo, Type, Company, AccountName, Price, Quantity, DatePosted, Exchange };

            var sell = from s in db.Order_Lives
                       join p in db.Account_Creations on s.CDS_AC_No equals p.CDSC_Number
                       where s.OrderType == "Sell" && s.WebServiceID == "Internal"

                       let OrderNo = s.OrderNo
                       let Type = s.OrderType
                       let Company = s.Company
                       let AccountName = p.Surname_CompanyName + " " + p.OtherNames
                       let Price = s.BasePrice
                       let Quantity = s.Quantity
                       let DatePosted = s.Create_date
                       let Exchange = s.TP
                       select new { OrderNo, Type, Company, AccountName, Price, Quantity, DatePosted, Exchange };


            ASPxGridView1.DataSource = buy.ToList();
            ASPxGridView1.DataBind();
            ASPxGridView2.DataSource = null;
            ASPxGridView2.DataSource = sell.ToList();
            ASPxGridView2.DataBind();
        }
        public void msgbox(string strMessage)
        {
            string strScript = "<script language=JavaScript>";
            strScript += "window.alert(\"" + strMessage + "\");";
            strScript += "</script>";
            System.Web.UI.WebControls.Label lbl = new System.Web.UI.WebControls.Label();
            lbl.Text = strScript;
            Page.Controls.Add(lbl);
        }
        public Decimal? postT(string max, string trade)
        {
            string vf = "";
            var sf = db.Database.SqlQuery<TR>("select top 1 IsNull(sum([BuyCharges]+[SellCharges]),0) as 'Mo' from transactioncharges where transcode='" + max + "' and   REPLACE(LOWER(ChargeCode),' ','')=REPLACE(LOWER((select  ChargeCode from tradingcharges where chargeaccountcode=CAST('" + trade + "' as nvarchar))),' ','')");
            foreach (var p in sf)
            {
                vf = p.Mo.ToString();
            }


            return decimal.Parse(vf, CultureInfo.InvariantCulture);
        }


        public string GetNames(string acc)
        {
            var c = db.Account_Creations.ToList().Where(a => a.CDSC_Number == acc);
            string nme = "";
            foreach (var p in c)
            {
                nme = p.Surname_CompanyName + " " + p.OtherNames;
            }
            return nme;
        }

        protected void ASPxButton1_Click(object sender, EventArgs e)
        {

        }

        protected void ASPxGridView1_SelectionChanged(object sender, EventArgs e)
        {

            object key = null;
            for (int i = 0; i < ASPxGridView1.VisibleRowCount; i++)
            {
                if (ASPxGridView1.Selection.IsRowSelected(i))
                {
                    key = ASPxGridView1.GetRowValues(i, "OrderNo");
                    Label1.Text = ASPxGridView1.GetRowValues(i, "OrderNo").ToString();
                }

            }




        }

        protected void ASPxGridView2_SelectionChanged(object sender, EventArgs e)
        {


            object key = null;
            for (int i = 0; i < ASPxGridView2.VisibleRowCount; i++)
            {
                if (ASPxGridView2.Selection.IsRowSelected(i))
                {
                    key = ASPxGridView2.GetRowValues(i, "OrderNo");
                    Label2.Text = ASPxGridView2.GetRowValues(i, "OrderNo").ToString();
                }

            }






        }

        
        public bool chkclient(string m)
        {
            bool tru = true;
            var ff = 0;

            try
            {
                ff = db.Account_Creations.ToList().Where(a => a.CDSC_Number == m).Count();

            }
            catch (Exception)
            {

                ff = 0;
                tru = false;
            }
            if (m.ToLower() == "NSE")
            {
                tru = false;
            }
            if (m.ToLower() == "finsec")
            {
                tru = false;
            }

            return tru;

        }
        public Decimal Buy(string max)
        {
            string vf = "";
            var sf = db.Database.SqlQuery<TR>("SELECT (select cast(Price as numeric(18,4))*cast(Quantity as  numeric(18,4)) from DealerDg where Deal='" + max + "')+sum(BuyCharges) as 'Mo'  FROM TransactionCharges where transcode = '" + max + "'");
            foreach (var p in sf)
            {
                vf = p.Mo.ToString();
            }
            return decimal.Parse(vf, CultureInfo.InvariantCulture);
        }
        public Decimal Sell(string max)
        {
            string vf = "";
            var sf = db.Database.SqlQuery<TR>("SELECT (select cast(Price as numeric(18,4))*cast(Quantity as  numeric(18,4)) from DealerDg where Deal='" + max + "')-sum(SellCharges) as 'Mo'  FROM TransactionCharges where transcode = '" + max + "'");
            foreach (var p in sf)
            {
                vf = p.Mo.ToString();
            }

            return decimal.Parse(vf, CultureInfo.InvariantCulture);
        }
        public Decimal BR(string max)
        {
            string vf = "";
            var sf = db.Database.SqlQuery<TR>("SELECT top 1 (select cast(Price as numeric(18,4))*cast(Quantity as  numeric(18,4)) from DealerDg where Deal='" + max + "') as 'Mo'  FROM TransactionCharges where transcode='" + max + "'");
            foreach (var p in sf)
            {
                vf = p.Mo.ToString();
            }


            return decimal.Parse(vf, CultureInfo.InvariantCulture);
        }
        public  DateTime AddBusinessDays(DateTime dateTime, int nDays)
        {
            var wholeWeeks = nDays / 5; //since nDays does not include weekdays every week is considered as 5 days
            var absDays = Math.Abs(nDays);
            var remaining = absDays % 5; //results in the number remaining days to add or substract excluding the whole weeks
            var direction = nDays / absDays;//results in 1 if nDays is posisive or -1 if it's negative
            while (dateTime.DayOfWeek == DayOfWeek.Saturday || dateTime.DayOfWeek == DayOfWeek.Sunday)
                dateTime = dateTime.AddDays(direction); //If we are already in a weekend, get out of it
            while (remaining-- > 0)
            {//add remaining days...
                dateTime = dateTime.AddDays(direction);
                if (dateTime.DayOfWeek == DayOfWeek.Saturday)
                    dateTime = dateTime.AddDays(direction * 2);//...skipping weekends
            }
            return dateTime.AddDays(wholeWeeks * 7); //Finally add the whole weeks as 7 days, thus skipping the weekends without checking for DayOfWeek
        }
        public void postTransCharge(decimal? p, decimal? k, string c, string transcode, string st, string vv)
        {
            string buy = "";
            string buyaacount = "";
            var pk = db.WindowsServices.ToList().Where(a => a.Action == "BUY");

            foreach (var t in pk)
            {
                buy = t.AccountName;
                buyaacount = t.AccountNumber;
            }

            var dz = db.TradingChargess.ToList().Where(a => a.ChargeName.ToLower().Replace(" ", "") == c.ToLower().Replace(" ", ""));
            double pin = Math.Round(Convert.ToDouble((p + k)),4);
            foreach (var q in dz)
            {

                Trans my3 = new Trans();

                my3.Account = q.chargeaccountcode.ToString();
                my3.Category = "Order Posting";
                my3.Credit = postT(transcode, q.chargeaccountcode.ToString());
                my3.Debit = 0;
                my3.Narration = vv;
                my3.Reference_Number = transcode;
                my3.TrxnDate = Convert.ToDateTime(st);
                my3.Post = Convert.ToDateTime(st);
                my3.Type = q.chartaccount;
                my3.PostedBy = "Background Service";
                db.Transs.Add(my3);
                db.SaveChanges();
            }


        }


        public Boolean isTaxable(string s)
        {
            Boolean test = false;
            string nm = "";
            var pace = db.Account_Creations.ToList().Where(a=>a.CDSC_Number==s);
            foreach (var z in pace)
            {
                nm = z.ClientType;
            }
            if (nm=="Taxable")
            {
                test = true;
            }
            return test;
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                string nc = "";
                string mm = Label2.Text;
                string mm1 = Label1.Text;
                if (mm1 != "")
                {
                    nc = mm1;
                }
                if (mm != "")
                {
                    nc = mm;
                }

                int my = Convert.ToInt32(nc);
                // a.OrderNo.ToString() == );
                var c = db.Order_Lives.Find(my);
                db.Order_Lives.Remove(c);
                db.SaveChanges();

            }
            catch (Exception)
            {

                msgbox("The order was not deleted");
                return;
            }
            BindGrids();
            msgbox("The order has been deleted");
            return;
        }

        //Post to the new Accounting Model

        private void postNewAccountingTrans(string transcode)
        {
            var postedTrans = db.Transs.Where(a => a.Reference_Number == transcode && a.Account !="4001" && a.Account != "2003").ToList();

            var clientAcc = postedTrans.Where(ab => ab.Account.Length > 5 && ab.Narration.Contains("Buy") || ab.Narration.Contains("Sell")).Select(y =>y.Account).FirstOrDefault();


            try
            {
                //Debit
                foreach (var post in postedTrans)
                {
                    tblFinancialTransactions financialTransaction = new tblFinancialTransactions();

                    financialTransaction.TrxnDate = post.TrxnDate;
                    financialTransaction.TransactionTypeId = 1;
                    financialTransaction.Description = post.Narration;
                    financialTransaction.SubAccount = getSubAccount(post.Account);
                    financialTransaction.Reference = post.Reference_Number;
                    financialTransaction.IsClientAccount = true;
                    financialTransaction.Debit = Convert.ToDecimal(post.Debit);
                    financialTransaction.Credit = Convert.ToDecimal(post.Credit);
                    financialTransaction.DrAccount = post.Account;
                    financialTransaction.CrAccount = "1100";
                    financialTransaction.CreatedDate = DateTime.Now;
                    financialTransaction.CreatedBy = WebSecurity.CurrentUserName.ToString();
                    financialTransaction.ClientAccount = clientAcc;
                    db.tblFinancialTransactions.Add(financialTransaction);


                }

                //Credit
                foreach (var post in postedTrans)
                {
                    tblFinancialTransactions financialTransaction = new tblFinancialTransactions();

                    financialTransaction.TrxnDate = post.TrxnDate;
                    financialTransaction.TransactionTypeId = 1;
                    financialTransaction.Description = post.Narration;
                    financialTransaction.SubAccount = getSubAccount(post.Account);
                    financialTransaction.Reference = post.Reference_Number;
                    financialTransaction.IsClientAccount = true;
                    financialTransaction.Debit = Convert.ToDecimal(post.Credit);
                    financialTransaction.Credit = Convert.ToDecimal(post.Debit);
                    financialTransaction.DrAccount = "1100";
                    financialTransaction.CrAccount = post.Account;
                    financialTransaction.CreatedDate = DateTime.Now;
                    financialTransaction.CreatedBy = WebSecurity.CurrentUserName.ToString();
                    financialTransaction.ClientAccount = clientAcc;
                    db.tblFinancialTransactions.Add(financialTransaction);
                }
            }
            catch (Exception)
            {

                
            }
            db.SaveChanges(WebSecurity.CurrentUserName.ToString());
        }

        private string getSubAccount(string account)
        {
            string theAccount = string.Empty;
            var storedAccount = db.tblFinancialAccounts.Where(a => a.AccountName == account).Select(ay => ay.AccountNumber).FirstOrDefault();
            theAccount = storedAccount.ToString();
            return theAccount;
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            try
            {

           
            string myRefCode = string.Empty;
            //check if two orders have equal Deals
            int? qty = 0; int? qty2 = 0;
            decimal? buys = 0; decimal? sells = 0;
            string buyc = ""; string sellc = ""; string buyer = ""; string seller = "";
            string sec = "";
            string sec2 = "";
            string orderno = "";
         


            var c = db.Order_Lives.ToList().Where(a => a.OrderNo.ToString() == Label1.Text);
            foreach (var z in c)
            {
                qty = z.Quantity;
                buys = z.BasePrice;
                buyc = z.Company;
                buyer = z.CDS_AC_No;
                sec = z.Company;
                orderno = z.OrderNumber;
            }
            //long sell = Convert.ToInt64(ASPxLabel5.Text);
            var d = db.Order_Lives.ToList().Where(a => a.OrderNo.ToString() == Label2.Text);
            foreach (var f in d)
            {
                qty2 = f.Quantity;
                sells = f.BasePrice;
                sellc = f.Company;
                seller = f.CDS_AC_No;
                sec = f.Company;
                orderno = f.OrderNumber;
            }
            decimal? b = 0;
            try
            {
                b = buys * Convert.ToDecimal(qty);
            }
            catch (Exception)
            {

                b = 0;
            }
            decimal? s = 0;

            try
            {
                s = (decimal.Parse(sells.ToString(), CultureInfo.InvariantCulture)) * (decimal.Parse(qty2.ToString(), CultureInfo.InvariantCulture));

            }
            catch (Exception)
            {

                s = 0;
            }
            DateTime sex = DateTime.Now;
            if (d.Count() > 0 && c.Count() > 0)
            {
                msgbox("You can only select one order from a single grid at a time");
                return;
            }
            else if (d.Count() > 0 || c.Count() > 0)
            {

                if (d.Count() <= 0)
                {
                    //seller = "NSE";
                    qty2 = qty;
                    sells = buys;
                    sellc = buyc;
                    b = buys * Convert.ToDecimal(qty2);
                    sec = buyc;
                }

                if (c.Count() <= 0)
                {
                    //buyer = "NSE";
                    qty = qty2;
                    buys = sells;
                    buyc = sellc;
                    sec = sellc;
                    b = buys * Convert.ToDecimal(qty);


                }
                if (buyer == "")
                {
                    buyer = "NSE";
                }

                if (seller == "")
                {
                    seller = "NSE";
                }
              //starting transactions
                using (TransactionScope transactionScope = new TransactionScope())
                {
                    try
                    {
                        var max = 0;

                        try
                        {
                            max = db.DealerDGs.ToList().Max(a => a.ID);

                        }
                        catch (Exception)
                        {

                            max = 0;
                        }
                        max = max + 1;

                        string maxw = max.ToString() + "Z";

                        myRefCode = maxw;
                        //insert transaction charges
                        var p = db.TradingChargess.ToList();
                        foreach (var z in p)
                        {
                            TransactionCharges myyc = new TransactionCharges();

                            myyc.Transcode = maxw;
                            myyc.ChargeCode = z.chargecode;
                            if (z.chargecode.ToLower().Replace(" ", "") == ("Capital gains with holding Tax").ToLower().Replace(" ", ""))
                            {

                                myyc.BuyCharges = 0;
                            }
                            else
                            {
                                if (z.chargecode.ToLower().Replace(" ", "") == ("Capital gains with holding Tax").ToLower().Replace(" ", ""))
                                {
                                    myyc.BuyCharges = 0;
                                }
                                else
                                {
                                    myyc.BuyCharges = b * Convert.ToDecimal((z.chargevalue / 100));

                                }



                            }
                            if (z.chargecode.ToLower().Replace(" ", "") == ("Stamp duty").ToLower().Replace(" ", ""))
                            {
                                myyc.SellCharges = 0;

                            }
                            else
                            {
                                if (isTaxable(seller) == false && z.chargecode.ToLower().Replace(" ", "") == ("Capital gains with holding Tax").ToLower().Replace(" ", ""))
                                {
                                    myyc.SellCharges = 0;
                                }
                                else
                                {

                                    myyc.SellCharges = b * Convert.ToDecimal((z.chargevalue / 100));
                                }
                                //myyc.SellCharges = Convert.ToDecimal(Math.Round(Convert.ToDouble(b * Convert.ToDecimal(z.chargevalue/100)),4));

                            }
                            string dgg = qty + " " + sec + " @ " + b;
                            myyc.Date = sex;
                          
                            if (chkclient(buyer) == true && chkclient(seller) == false)
                            {
                                myyc.SellCharges = 0;
                            }

                            if (chkclient(seller) == true && chkclient(buyer) == false)
                            {
                                myyc.BuyCharges = 0;
                            }
                            db.TransactionCharges.Add(myyc);
                            db.SaveChanges();
                            postTransCharge(myyc.BuyCharges, myyc.SellCharges, myyc.ChargeCode, myyc.Transcode, myyc.Date.ToString(), dgg);

                            
                        }
                        var str = db.Users.ToList().Where(a => a.Email == WebSecurity.CurrentUserName);
                        string nme = "";
                        foreach (var t in str)
                        {
                            nme = t.BrokerCode;
                        }
                        //Insert tblMatcheddeals
                        ATS.Tbl_MatchedDeals my = new ATS.Tbl_MatchedDeals();
                        my.Deal = maxw;
                        my.BuyCompany = buyc;
                        my.SellCompany = sellc;
                        my.Buyercdsno = buyer;
                        my.Sellercdsno = seller;
                        my.Quantity = qty;
                        my.Trade = sex;
                        my.DealPrice = buys;
                        my.DealFlag = "C";
                        my.Instrument = "NULL";
                        my.Affirmation = false;
                        my.buybroker = GetNames(buyer);
                        my.sellbroker = GetNames(seller);
                        try
                        {
                            my.RefID = Convert.ToInt32(orderno);
                        }
                        catch (Exception)
                        {

                            my.RefID = 0;
                        }
                        my.BrokerCode = nme;
                        my.exchange = "NSE";
                        db.TblDealss.Add(my);
                        db.SaveChanges();

                        var mmksc = db.DealerDGs.ToList().Max(a => a.DealNumber);
                        if (mmksc==null)
                        {
                            mmksc = 0;
                        }
                        mmksc = mmksc + 1;
                        //insert into DealerDG
                        DealerDG myy = new DealerDG();
                        myy.Account1 = buyer;
                        myy.Account2 = seller;
                        myy.AccountName1 = GetNames(buyer);
                        myy.AccountName2 = GetNames(seller);
                        myy.Ack = "SETTLED";
                        myy.DatePosted = sex.ToString();
                        myy.SettlementDate = AddBusinessDays(sex, 3).ToString();
                        myy.Deal = maxw;
                        myy.Price = buys.ToString();
                        myy.Quantity = qty.ToString();
                        myy.Security = sec;
                        myy.SettlementDate = AddBusinessDays(sex, 3).ToString();
                        myy.SecurityName = sec;
                        myy.Exchange = "NSE";
                        myy.PostDate = Convert.ToDateTime(sex.ToString());
                        myy.DealNumber = mmksc;
                        myy.OrderNumber = orderno;
                        db.DealerDGs.Add(myy);
                        db.SaveChanges();
                        var mmk = "";
                        var mmks = db.DealerDGs.ToList().Where(a => a.Deal == myy.Deal);
                        foreach (var r in mmks)
                        {
                            mmk = r.DealNumber.ToString();
                        }

                        //Dealer2
                        DealerDG2 myyz = new DealerDG2();
                        myyz.Account1 = buyer;
                        myyz.Account2 = seller;
                        myyz.AccountName1 = GetNames(buyer);
                        myyz.AccountName2 = GetNames(seller);
                        myyz.Ack = "SETTLED";
                        myyz.DatePosted = Convert.ToDateTime(sex.ToString());
                        myyz.SettlementDate = AddBusinessDays(sex, 3).ToString();
                        myyz.Deal = maxw;
                        myyz.Price = buys.ToString();
                        myyz.Quantity = qty.ToString();
                        myyz.Security = sec;
                        myyz.SettlementDate = AddBusinessDays(sex, 3).ToString();
                        myyz.SecurityName = sec;
                        myyz.Exchange = "NSE";
                        myyz.OldDeal = mmk.ToString();
                        myyz.PostDate = Convert.ToDateTime(sex.ToString());
                        myyz.OrderNumber = orderno;
                        db.DealerDGs2.Add(myyz);
                        db.SaveChanges();

                        //Post to accounts
                        string desc = qty + " " + sec + " @ " + b;
                        decimal? total = 0;
                        decimal? total2 = 0;
                        //insert charge to Trans
                        var zzz = db.TransactionCharges.ToList().Where(a => a.Transcode == maxw);
                        foreach (var f in zzz)
                        {

                            total += f.BuyCharges;


                            total2 += f.SellCharges;




                        }
                        string buy = "";
                        string buyaacount = "";
                        string sell = "";
                        string sellacount = "";
                        string buyA = "";
                        string buyaacountA = "";
                        string sellA = "";
                        string sellacountA = "";
                        var pk = db.WindowsServices.ToList().Where(a => a.Action == "BUY");
                        foreach (var ppc in pk)
                        {
                            buy = ppc.AccountName;
                            buyaacount = ppc.AccountNumber;
                        }
                        var tk = db.WindowsServices.ToList().Where(a => a.Action == "SELL");
                        foreach (var u in tk)
                        {
                            sell = u.AccountName;
                            sellacount = u.AccountNumber;
                        }
                        var pkA = db.WindowsServices.ToList().Where(a => a.Action == "BUYA");
                        foreach (var ppc in pkA)
                        {
                            buyA = ppc.AccountName;
                            buyaacountA = ppc.AccountNumber;
                        }
                        var tkA = db.WindowsServices.ToList().Where(a => a.Action == "SELLA");
                        foreach (var u in tkA)
                        {
                            sellA = u.AccountName;
                            sellacountA = u.AccountNumber;
                        }

                        decimal deals = 0;
                        decimal? bb = 0;
                        decimal? ss = 0;
  deals = (decimal.Parse(buys.ToString(), CultureInfo.InvariantCulture)) * (decimal.Parse(qty.ToString(), CultureInfo.InvariantCulture));
                        bb = b + total;
                        ss = b - total2;
                        
                        if (chkclient(buyer) == true)
                        {
                            Trans my2 = new Trans();


                            //debit account number

                            my2.Account = buyer;


                            my2.Category = "Purchase " + qty + " " + sec + " @ " + buys;
                            my2.Credit = 0;
                            my2.Debit = Buy(maxw);
                            my2.Narration = "Purchase " + qty + " " + sec + " @ " + buys;
                            my2.Reference_Number = maxw;
                            my2.TrxnDate = Convert.ToDateTime(sex);
                            my2.Post = Convert.ToDateTime(sex);
                            my2.Type = "Purchase " + qty + " " + sec + " @ " + buys;
                            my2.PostedBy = "Service/Total Buy Charges" + total.ToString() + "," + deals;
                            db.Transs.Add(my2);

                            //Update table  
                            db.SaveChanges();

                            my2.Account = buyaacount;
                            my2.Category = "Purchase " + qty + " " + sec + " @ " + buys;
                            my2.Credit = 0;
                            my2.Debit = Buy(maxw);
                            my2.Narration = "Purchase " + qty + " " + sec + " @ " + buys;
                            my2.Reference_Number = maxw;
                            my2.TrxnDate = Convert.ToDateTime(sex);
                            my2.Post = Convert.ToDateTime(sex);
                            my2.Type = "Purchase " + qty + " " + sec + " @ " + buys;
                            my2.PostedBy = "Service /Total Buy Charges" + total.ToString() + "," + deals;
                            db.Transs.Add(my2);


                            db.SaveChanges();
                            //credit bramount
                            my2.Account = buyaacountA;
                            my2.Category = "Purchase " + qty + " " + sec + " @ " + buys;
                            my2.Credit = BR(maxw);
                            my2.Debit = 0;
                            my2.Narration = "Purchase " + qty + " " + sec + " @ " + buys;
                            my2.Reference_Number = maxw;
                            my2.TrxnDate = Convert.ToDateTime(sex);
                            my2.Post = Convert.ToDateTime(sex);
                            my2.Type = "Purchase " + qty + " " + sec + " @ " + buys;
                            my2.PostedBy = "Service /Total Buy Charges" + total.ToString() + "," + deals;
                            db.Transs.Add(my2);


                            db.SaveChanges();
                        }

                        //oppose buyaccount

                        //debit account number
                        if (chkclient(seller) == true)
                        {
                            Trans my4 = new Trans();

                            //debit account number

                            my4.Account = seller;

                            if (my4.Account == "")
                            {
                                my4.Account = "NSE";
                            }
                            my4.Category = "Sell " + qty + " " + sec + " @ " + buys;
                            my4.Credit = Sell(maxw);
                            my4.Debit = 0;
                            my4.Narration = "Sell " + qty + " " + sec + " @ " + buys;
                            my4.Reference_Number = maxw;
                            my4.TrxnDate = Convert.ToDateTime(sex);
                            my4.Post = Convert.ToDateTime(sex);
                            // my2.Type = "Sell Deal";
                            my4.Type = "Sell " + qty + " " + sec + " @ " + buys;
                            my4.PostedBy = "Service/Total Sell Charges" + total2.ToString() + "," + deals;
                            db.Transs.Add(my4);
                            db.SaveChanges();
                            ////accounts payable

                            my4.Account = sellacount;
                            my4.Category = "Sell " + qty + " " + sec + " @ " + buys;
                            my4.Credit = Sell(maxw);
                            my4.Debit = 0;
                            my4.Narration = "Sell " + qty + " " + sec + " @ " + buys;
                            my4.Reference_Number = maxw;
                            my4.TrxnDate = Convert.ToDateTime(sex);
                            my4.Post = Convert.ToDateTime(sex);
                            // my2.Type = "Sell Deal";
                            my4.Type = "Sell " + qty + " " + sec + " @ " + buys;
                            my4.PostedBy = "Service/Total Sell Charges" + total2.ToString() + "," + deals;
                            db.Transs.Add(my4);
                            db.SaveChanges();
                            //credit bramount

                            my4.Account = sellacountA;
                            my4.Category = "Sell " + qty + " " + sec + " @ " + buys;
                            my4.Credit = 0;
                            my4.Debit = BR(maxw);
                            my4.Narration = "Sell " + qty + " " + sec + " @ " + buys;
                            my4.Reference_Number = maxw;
                            my4.TrxnDate = Convert.ToDateTime(sex);
                            my4.Post = Convert.ToDateTime(sex);
                            // my2.Type = "Sell Deal";
                            my4.Type = "Sell " + qty + " " + sec + " @ " + buys;
                            my4.PostedBy = "Service/Total Sell Charges" + total2.ToString() + "," + deals;
                            db.Transs.Add(my4);
                            db.SaveChanges();


                        }

                        //Update Status to Matched
                        if (c.Count() > 0)
                        {
                            Order_Live omy = db.Order_Lives.Find(Convert.ToInt32(Label1.Text));
                            omy.WebServiceID = "Matched";
                            omy.OrderAttribute = "Matched";
                            db.Order_Lives.AddOrUpdate(omy);
                            db.SaveChanges();
                        }

                        if (d.Count() > 0)
                        {
                            Order_Live omyc = db.Order_Lives.Find(Convert.ToInt32(Label2.Text));
                            omyc.WebServiceID = "Matched";
                            omyc.OrderAttribute = "Matched";
                            db.Order_Lives.AddOrUpdate(omyc);
                            db.SaveChanges();
                        }

                        Label1.Text = "";
                        Label2.Text = "";
                        transactionScope.Complete();

                    }
                    catch (TransactionException ex)
                    {
                        transactionScope.Dispose();
                        msgbox("Transaction Exception Occured");
                    }
                }
                BindGrids();
                msgbox("The Deal Note has been generated for the order");
                postNewAccountingTrans(myRefCode);
                return;
            }

            

            else
            {
                // b == s && buyc == sellc
                msgbox("Select one record from either grid that is a Buy or Sell");
                return;
            }


            }
            catch (Exception)
            {


            }
        }
    }
    }

