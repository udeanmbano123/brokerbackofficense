﻿<%@ Page Language="C#" Async="true"   AutoEventWireup="true" MaintainScrollPositionOnPostback="true" MasterPageFile="~/Site1.Master" CodeBehind="BatchDeals.aspx.cs" Inherits="BrokerOffice.ClientServices.BatchDeals" %>
<%@ Register Assembly="DevExpress.Web.v18.2, Version=18.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.XtraReports.v18.2.Web.WebForms, Version=18.2.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraReports.Web" TagPrefix="dx" %>


<asp:content id="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <script type = "text/javascript">
    function ConfirmDelete()
    {
       var count = document.getElementById("<%=hfCount.ClientID %>").value;
       var gv = document.getElementById("<%=gvAll.ClientID%>");
       var chk = gv.getElementsByTagName("input");
       for(var i=0;i<chk.length;i++)
       {
            if(chk[i].checked && chk[i].id.indexOf("chkAll") == -1)
            {
                count++;
            }
       }
       if(count == 0)
       {
            alert("No records to delete.");
            return false;
       }
       else
       {
            return confirm("Do you want to settle " + count + " deals.");
       }
    }
       function Check_Click(objRef) {
           //Get the Row based on checkbox
           var row = objRef.parentNode.parentNode;
           if (objRef.checked) {
               //If checked change color to Aqua
               row.style.backgroundColor = "aqua";
           }
           else {
               //If not checked change back to original color
               if (row.rowIndex % 2 == 0) {
                   //Alternating Row Color
                   row.style.backgroundColor = "#C2D69B";
               }
               else {
                   row.style.backgroundColor = "white";
               }
           }

           //Get the reference of GridView
           var GridView = row.parentNode;

           //Get all input elements in Gridview
           var inputList = GridView.getElementsByTagName("input");

           for (var i = 0; i < inputList.length; i++) {
               //The First element is the Header Checkbox
               var headerCheckBox = inputList[0];

               //Based on all or none checkboxes
               //are checked check/uncheck Header Checkbox
               var checked = true;
               if (inputList[i].type == "checkbox" && inputList[i] != headerCheckBox) {
                   if (!inputList[i].checked) {
                       checked = false;
                       break;
                   }
               }
           }
           headerCheckBox.checked = checked;

       }
       function checkAll(objRef) {
           var GridView = objRef.parentNode.parentNode.parentNode;
           var inputList = GridView.getElementsByTagName("input");
           for (var i = 0; i < inputList.length; i++) {
               //Get the Cell To find out ColumnIndex
               var row = inputList[i].parentNode.parentNode;
               if (inputList[i].type == "checkbox" && objRef != inputList[i]) {
                   if (objRef.checked) {
                       //If the header checkbox is checked
                       //check all checkboxes
                       //and highlight all rows
                       row.style.backgroundColor = "aqua";
                       inputList[i].checked = true;
                   }
                   else {
                       //If the header checkbox is checked
                       //uncheck all checkboxes
                       //and change rowcolor back to original
                       if (row.rowIndex % 2 == 0) {
                           //Alternating Row Color
                           row.style.backgroundColor = "#C2D69B";
                       }
                       else {
                           row.style.backgroundColor = "white";
                       }
                       inputList[i].checked = false;
                   }
               }
           }
       }
       function MouseEvents(objRef, evt) {
           var checkbox = objRef.getElementsByTagName("input")[0];
           if (evt.type == "mouseover") {
               objRef.style.backgroundColor = "orange";
           }
           else {
               if (checkbox.checked) {
                   objRef.style.backgroundColor = "aqua";
               }
               else if (evt.type == "mouseout") {
                   if (objRef.rowIndex % 2 == 0) {
                       //Alternating Row Color
                       objRef.style.backgroundColor = "#C2D69B";
                   }
                   else {
                       objRef.style.backgroundColor = "white";
                   }
               }
           }
       }
</script>
  <div style="background:white" class="row">
    <div class="panel-heading" style="background-color:#428bca">
        <i style="color:white">Sell Deal Batch Processing</i>
       
    </div> 

<table>

    <tr>
        <td>
<dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="Buy Orders" Theme="Glass"></dx:ASPxLabel>

        </td>
           <td></td>
         <td>
              &nbsp;</td>
    </tr>
    <tr>
         <td>
             <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="From"></dx:ASPxLabel>
             <dx:ASPxDateEdit ID="ASPxDateEdit1" runat="server" Theme="BlackGlass"></dx:ASPxDateEdit>
                        </td>
                        <td>
                            <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="To"></dx:ASPxLabel>
                            <dx:ASPxDateEdit ID="ASPxDateEdit2" runat="server" Theme="BlackGlass"></dx:ASPxDateEdit>
                        </td>
       
         <td>
<dx:ASPxButton ID="btnSearch" runat="server" CssClass="btn btn-primary" Text="Search" OnClick="btnSearch_Click" style="height: 23px"></dx:ASPxButton>
        
                
        </td>
        <td>
 <dx:ASPxTextBox ID="txtSearch" Visible="false" runat="server" CssClass="form-control" Width="250px"></dx:ASPxTextBox>
          
        </td>
    </tr>
    </table>
    <br />

      <table>
  <tr>
 
          <td>
              <asp:GridView ID="gvAll" CssClass="table table-striped" runat="server"
    AutoGenerateColumns = "false" Font-Names = "Arial"
    Font-Size = "11pt" AlternatingRowStyle-BackColor ="#AFEEEE" 
    HeaderStyle-BackColor = "#FFFFFF" AllowPaging ="true"  
    OnPageIndexChanging = "OnPaging" DataKeyNames="Deal"
    PageSize = "10" OnRowDataBound="gvAll_RowDataBound" >
   <Columns>
    <asp:TemplateField>
        <HeaderTemplate>
            <asp:CheckBox ID="chkAll" runat="server"
             onclick = "checkAll(this);" />
        </HeaderTemplate>
        <ItemTemplate>
            <asp:CheckBox ID="chk" runat="server"
             onclick = "Check_Click(this)"/>
        </ItemTemplate>
    </asp:TemplateField>
       <asp:BoundField ItemStyle-Width = "150px" DataField = "Deal"
       HeaderText = "Deal"/>
      <asp:BoundField ItemStyle-Width = "150px" DataField = "Security"
       HeaderText = "Security"/>
    
    <asp:BoundField ItemStyle-Width = "150px" DataField = "Quantity"
       HeaderText = "Quantity"/>
         <asp:BoundField ItemStyle-Width = "150px" DataField = "Price"
       HeaderText = "Price"/>
    <asp:BoundField ItemStyle-Width = "150px" DataField = "BuyerClient"
       HeaderText = "BuyerClient"/>
    <asp:BoundField ItemStyle-Width = "150px" DataField = "SellerClient"
       HeaderText = "SellerClient"/>
         <asp:BoundField ItemStyle-Width = "150px" DataField = "DatePosted"
       HeaderText = "DatePosted"/>
    <asp:BoundField ItemStyle-Width = "150px" DataField = "SettlementDate"
       HeaderText = "SettlementDate"/>
    <asp:BoundField ItemStyle-Width = "150px" DataField = "Status"
       HeaderText = "Status"/>
   </Columns>
   <AlternatingRowStyle BackColor="#C2D69B"  />
</asp:GridView>
          
          
      </td>
       
  </tr>
          </table>
      <table>
    <tr><td>
            <br />
            <asp:HiddenField ID="hfCount" runat="server" Value = "0" />
    <asp:Button ID="Button1"  OnClientClick = "return ConfirmDelete();"  CssClass="btn btn-primary" runat="server" Text="Settle Sell Deals" Click="ASPxButton1_Click" OnClick="Button1_Click"></asp:Button>
        </td>
        <td>
            

        </td>
        
    </tr>

      


      

   </table>
  

</asp:content>