﻿using BrokerOffice.DAO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebMatrix.WebData;

namespace BrokerOffice.MarketWatch
{
    public partial class marketwatcher : System.Web.UI.Page
    {
        private SBoardContext db = new SBoardContext();

        public SqlConnection myConnection = new SqlConnection();
        public SqlDataAdapter adp;
        public SqlCommand cmd;
        public static string nm="";
        protected void Page_Load(object sender, EventArgs e)
        {
            loadmarketwatch();
            loadmarketwatchZSE();
            ASPxLabel1.Text = "Market Status: "+ marketstatus();
            if (!this.IsPostBack)
            {
                Timer2.Enabled = true;
                Timer2.Interval = 15000;
                RadStl.SelectedValue = "ZSE";
                if (RadStl.SelectedValue == "ZSE")
                {

                    Panel3.Visible = true;
                    Panel2.Visible = false;
                    Panel1.Visible = false;
                    Panel4.Visible = false;
                }
                else if (RadStl.SelectedValue == "FINSEC")
                {
                    Panel3.Visible = false;
                    Panel1.Visible = true;
                    Panel4.Visible = false;
                }
            }
        }

        public string marketstatus()
        {

            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["ATS"].ConnectionString;

            DataSet ds = new DataSet();
            int i = 0;
            cmd = new SqlCommand("select * from marketstatus", myConnection);
            adp = new SqlDataAdapter(cmd);
            adp.Fill(ds, "stats");
            string stat = "";
            foreach (DataRow myRow in ds.Tables[0].Rows)
            {
                stat = myRow["Market Status"].ToString();
           
            }
            return stat;
        }

        public void loadmarketwatch()
        {
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["ATS"].ConnectionString;

            DataSet ds = new DataSet();
            int i = 0;
            cmd = new SqlCommand("select *, (select bestprice from companyprices where company=marketwatcher.company) as [Last Traded Price], isnull((select top 1 dealprice from tbl_matcheddeals where BuyCompany=marketwatcher.company  order by id desc),0) as [Lastmatched],isnull((select top 1 Quantity from tbl_matcheddeals where BuyCompany=marketwatcher.company  order by id desc),0) as [lastvolume], isnull((select sum(Quantity) from tbl_matcheddeals where BuyCompany=marketwatcher.company And trade=convert(date,getdate())),0) as [TotalVolume] ,isnull((select sum(Quantity*DealPrice) from tbl_matcheddeals where BuyCompany=marketwatcher.company And trade=convert(date,getdate())),0) as [Turnover], (select top 1 OpeningPrice from CompanyPrices where company=marketwatcher.company) as [Open],isnull((select  max(DealPrice) from tbl_matcheddeals where BuyCompany= marketwatcher.company and trade=convert(date,getdate())),0) as [High],isnull((select  min(DealPrice) from tbl_matcheddeals where BuyCompany= marketwatcher.company and trade=convert(date,getdate())),0) as [Low], (select top 1 BestPrice from CompanyPrices where company=marketwatcher.company) as [Average Price],(select top 1 bestprice from CompanyPrices where company=marketwatcher.company) - (select top 1 OpeningPrice from CompanyPrices where company=marketwatcher.company) as [Change],((select top 1 bestprice from CompanyPrices where company=marketwatcher.company) - (select top 1 OpeningPrice from CompanyPrices where company=marketwatcher.company))/(select top 1 openingprice from CompanyPrices where company=marketwatcher.company)*100 as [percchange], case when (select top 1 BestPrice from CompanyPrices where company=marketwatcher.company)>(select top 1 OpeningPrice  from CompanyPrices where company=marketwatcher.company) then '~/images/up.png'  when (select top 1 BestPrice from CompanyPrices where company=marketwatcher.company)<(select top 1 OpeningPrice  from CompanyPrices where company=marketwatcher.company) then '~/images/down.png' else '~/images/nochange.png' end  as url, '~/images/yellow.gif' as url2 from marketwatcher", myConnection);
            adp = new SqlDataAdapter(cmd);
            adp.Fill(ds, "stats");
            try
            {
                GridView1.DataSource = ds;
                GridView1.DataBind();
                GridView4.DataSource = ds;
                GridView4.DataBind();
            }
            catch (Exception)
            {

               
            }
            var p = db.Users.ToList().Where(a => a.Email == WebSecurity.CurrentUserName);
            string vx = "";
            foreach (var c in p)
            {
                nm = c.role;
            }
            ASPxLabel1.Text = "Market Status: " + marketstatus();
            
        }

        public void loadmarketwatchZSE()
        {
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["connpath"].ConnectionString;

            DataSet ds = new DataSet();
            int i = 0;
            cmd = new SqlCommand("SELECT [Ticker],[ISIN],[Best_Ask],[Best_bid],[Current_price]  FROM [CDS_ROUTER].[dbo].[ZSE_market_data]", myConnection);
            adp = new SqlDataAdapter(cmd);
            adp.Fill(ds, "stats");
            try
            {
                GridView4.DataSource = ds;
                GridView4.DataBind();
            }
            catch (Exception)
            {


            }
          
        }

        public void loadbuys(String company)
        {
            var p = db.Users.ToList().Where(a => a.Email == WebSecurity.CurrentUserName);
            string vx = "";
            foreach (var c in p)
            {
               vx = c.BrokerCode;
            }
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["ATS"].ConnectionString;

            DataSet ds = new DataSet();
            int i = 0;
            cmd = new SqlCommand("select Company, Quantity, BasePrice, right(OrderPref,5) as OrderPref, Timeinforce AS [TIF], FOK as [Block], case broker_code when '" + vx + "' then  '~/images/yellow.gif' else NULL end as [mine] from (select case when baseprice=0 then 1 else 2 end as PriceRow, Company, Quantity, Baseprice, OrderPref, Timeinforce, FOK, orderdate, broker_code  from Livetradingmaster where ordertype='BUY') j where j.company='" + company + "' order by BASEPRICE desc, j.orderdate asc", myConnection);
            adp = new SqlDataAdapter(cmd);
            adp.Fill(ds, "stats");
            try
            {
                GridView2.DataSource = ds;
                GridView2.DataBind();

            }
            catch (Exception)
            {


            }
            //msgbox(company);
        }
        public void msgbox(string strMessage)
        {
            string strScript = "<script language=JavaScript>";
            strScript += "window.alert(\"" + strMessage + "\");";
            strScript += "</script>";
            System.Web.UI.WebControls.Label lbl = new System.Web.UI.WebControls.Label();
            lbl.Text = strScript;
            Page.Controls.Add(lbl);
        }

        public void loadsells(String company)
        {
            var p = db.Users.ToList().Where(a => a.Email == WebSecurity.CurrentUserName);
            string vx = "";
            foreach (var c in p)
            {
                vx = c.BrokerCode;
            }
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["ATS"].ConnectionString;

            DataSet ds = new DataSet();
            int i = 0;
            cmd = new SqlCommand("select Company, Quantity, BasePrice, right(OrderPref,5) as OrderPref, Timeinforce AS [TIF], FOK as [Block], case broker_code when '" + vx + "' then  '~/images/yellow.gif' else NULL end as [mine] from (select case when baseprice=0 then 1 else 2 end as PriceRow, Company, Quantity, Baseprice, OrderPref, Timeinforce, FOK, orderdate, broker_code  from Livetradingmaster where ordertype='SELL') j where j.company='" + company + "' order by BASEPRICE asc, j.orderdate asc", myConnection);
            adp = new SqlDataAdapter(cmd);
            adp.Fill(ds, "stats");
            try
            {
                GridView3.DataSource = ds;
                GridView3.DataBind();

            }
            catch (Exception)
            {


            }
            //msgbox(company);
        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Label5.Text = GridView1.SelectedValue.ToString();
            loadbuys(GridView1.SelectedValue.ToString());
            loadsells(GridView1.SelectedValue.ToString());
            Panel2.Visible = true;
        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }
        
        protected void RadStl_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (RadStl.SelectedValue == "ZSE")
            {
                //msgbox("ZSE");
                Panel3.Visible = true;
                Panel2.Visible = false;
                Panel1.Visible = false;
                Panel4.Visible = false;
            }
            else if (RadStl.SelectedValue == "FINSEC")
            {
                //msgbox("FIN");
                Panel3.Visible = false;
                Panel1.Visible = true;
                Panel4.Visible = false;
            }
        }

        protected void Timer2_Tick(object sender, EventArgs e)
        {

            if (RadStl.SelectedValue == "ZSE")
            {

                Panel3.Visible = true;
                Panel2.Visible = false;
                Panel1.Visible = false;
               
                try
                {
                    Label9.Text = GridView4.SelectedValue.ToString();
                    loadbuysN(GridView4.SelectedValue.ToString());
                    loadsellsN(GridView4.SelectedValue.ToString());
                    Panel4.Visible = true;
                }
                catch (Exception)
                {

                    loadmarketwatchZSE();
                }
            }
            else if (RadStl.SelectedValue == "FINSEC")
            {
                Panel3.Visible = false;
                Panel1.Visible = true;
                try
                {
                    Label5.Text = GridView1.SelectedValue.ToString();
                    loadbuys(GridView1.SelectedValue.ToString());
                    loadsells(GridView1.SelectedValue.ToString());
                    Panel2.Visible = true;
                }
                catch (Exception)
                {

                    loadmarketwatch();
                }
                
            }


        }

        protected void GridView4_SelectedIndexChanged(object sender, EventArgs e)
        {
            Label9.Text = GridView4.SelectedValue.ToString();
            loadbuysN(GridView4.SelectedValue.ToString());
            loadsellsN(GridView4.SelectedValue.ToString());
            Panel4.Visible = true;
        }
        public void loadsellsN(String company)
        {
            var p = db.Users.ToList().Where(a => a.Email == WebSecurity.CurrentUserName);
            string vx = "";
            foreach (var c in p)
            {
                vx = c.BrokerCode;
            }
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["SBoardConnection"].ConnectionString;

            DataSet ds = new DataSet();
            int i = 0;
            cmd = new SqlCommand("select *, case broker_code when '" + vx + "' then  '~/images/yellow.gif' else NULL end as [mine]  from Pre_Order where Company='" + company + "' and ordertype='SELL' and CAST(Create_date as date)=CAST('" + DateTime.Now.ToString("dd-MMM-yyyy") + "' as date)", myConnection);
            adp = new SqlDataAdapter(cmd);
            adp.Fill(ds, "stats");
            try
            {
                GridView6.DataSource = ds;
                GridView6.DataBind();

            }
            catch (Exception)
            {


            }
            //msgbox(company);
        }

        public void loadbuysN(String company)
        {
            var p = db.Users.ToList().Where(a => a.Email == WebSecurity.CurrentUserName);
            string vx = "";
            foreach (var c in p)
            {
                vx = c.BrokerCode;
            }
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["SBoardConnection"].ConnectionString;

            DataSet ds = new DataSet();
            int i = 0;
            cmd = new SqlCommand("select *, case broker_code when '" + vx + "' then  '~/images/yellow.gif' else NULL end as [mine]  from Pre_Order where Company='"+ company + "' and ordertype='BUY' and CAST(Create_date as date)=CAST('" + DateTime.Now.ToString("dd-MMM-yyyy") + "' as date)", myConnection);
            adp = new SqlDataAdapter(cmd);
            adp.Fill(ds, "stats");
            try
            {
                GridView5.DataSource = ds;
                GridView5.DataBind();

            }
            catch (Exception)
            {


            }
            //msgbox(company);
        }

  

        protected void GridView6_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView6.PageIndex = e.NewPageIndex;
            Label9.Text = GridView4.SelectedValue.ToString();
            loadsellsN(GridView4.SelectedValue.ToString());
        }

        protected void GridView5_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView5.PageIndex = e.NewPageIndex;
            Label9.Text = GridView4.SelectedValue.ToString();
            loadbuysN(GridView4.SelectedValue.ToString());

        }
    }
}